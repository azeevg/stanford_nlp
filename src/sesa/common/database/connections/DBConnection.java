package sesa.common.database.connections;

import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.ParsedRelation;
import sesa.common.database.entity.IDBEntity;
import sesa.common.features.Option;
import sesa.common.utility.logging.LoggingUtility;
import sesa.tools.statistics.RawSemanticGroup;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract class for working with Database
 * ��������� �������, ������� � ����������� ����� ���� ����������� � �����������
 */

@Deprecated
public abstract class DBConnection extends LoggingUtility {
    protected boolean isConnected = true;
    protected String host;
    protected int port;
    protected String name;
    protected String user;
    protected String password;

    protected DBConnection(String host, int port, String name, String user, String password) {
        this.host = host;
        this.port = port;
        this.name = name;
        this.user = user;
        this.password = password;
    }

    public final boolean isConnected() {
        return isConnected;
    }

    /**
     * ���������� ������� ����� � ������� yyyy-mm-dd hh:mm:ss
     * @return current time in format yyyy-mm-dd hh:mm:ss
     */
    protected final Timestamp getNow() {
        java.util.Date now = new java.util.Date();
        return new java.sql.Timestamp(now.getTime());
    }

    public abstract DBConnection useConnection(int nodeIndex);

    public abstract boolean tableExists(String collectionName);

    public abstract boolean insert(String collectionName, IDBEntity document);

    public abstract boolean copyByCategory(String collectionNameSrc, String collectionNameDest, String categories);

    public abstract boolean insert(String collectionName, IDBEntity document, Map<String, String> additional);

    public abstract boolean update(String collectionName, IDBEntity document);

    public abstract long count(String collectionName, Map<String, Object> condition);

    public abstract long count(String collectionName);

    public abstract boolean contains(String collectionName, String id);

    public abstract Option<IDBEntity> get(String collectionName, String id);

    public abstract boolean remove(String collectionName, String id);

    public abstract List<IDBEntity> select(String collectionName, long from, long size);

    public abstract IDBEntity selectById(String collectionName, String id);

    public abstract List<IDBEntity> select(String collectionName);

    public abstract boolean create(String collectionName);

    public abstract boolean truncate(String collectionName);

    public abstract boolean drop(String collectionName);

    public abstract boolean insertObject(String collectionName, String title, String hash, byte[] data);

    public abstract boolean insertObject(String collectionName, int title, int hash, String data);

    public abstract InputStream readObject(String collectionName, String id);

    public abstract HashMap<Integer, Tree> getCachedTrees(String collectionName, int titleHash);

    public abstract HashMap<Integer, Tree> getCachedTreesObjects(String collectionName, String title);

    public abstract HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> getCachedRelations(String collectionName, String title);

    public abstract void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, int position, int groupHash, int groupId);

    public abstract void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, String pos, int index, String groupId, int sentenceId, int sentenceService, int prevDelimiter, int sentenceSize);

  /*  public final DBIterator getIterator(String collectionName) {
        return new DBIterator(collectionName, this);
    }*/

    public abstract int getCounter(String key);

    public abstract void setCounter(String key, int value);


    public abstract void writeErrorDocument(String key, String type, String message);

    public abstract List<String> readParsingErrors();

    public abstract Collection<RawSemanticGroup> getSemanticGroups(String collectionName);
}
