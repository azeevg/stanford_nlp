package sesa.common.database.entity.dictionary;

import java.util.List;

/**
 * This entity work with table "terms_comp_definition" and "terms_comp_link"
 * Created by Ira on 13.05.2016.
 */
public class TermDefinitionEntity {

    private int idDefinition;
    private int idTerm;
    private String definition;
    private List<Integer> links; // list of links for another terms

    public TermDefinitionEntity() {
    }

    public TermDefinitionEntity(int idDefinition, int idTerm, String definition) {
        this.idDefinition = idDefinition;
        this.idTerm = idTerm;
        this.definition = definition;
    }

    public int getIdDefinition() {
        return idDefinition;
    }

    public void setIdDefinition(int idDefinition) {
        this.idDefinition = idDefinition;
    }

    public int getIdTerm() {
        return idTerm;
    }

    public void setIdTerm(int idTerm) {
        this.idTerm = idTerm;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public List<Integer> getLinks() {
        return links;
    }

    public void setLinks(List<Integer> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "TermDefinitionEntity{" +
                "idDefinition=" + idDefinition +
                ", idTerm=" + idTerm +
                ", definition='" + definition + '\'' +
                ", links=" + links +
                '}';
    }
}
