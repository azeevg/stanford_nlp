package sesa.tools.semantic.linkBuilder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.dao.dictionary.TermLinksDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.database.entity.SemanticGroupEntity;

import java.sql.SQLException;
import java.util.*;

/**
 * Build semantic graph based on links like "See...
 */
public class LinkStructureBuilder {
    public final Logger log = LogManager.getLogger(LinkStructureBuilder.class);

    private Map<String, Integer> allTerms;
    private TermDefinitionDao definitionDao;
    private TermDao dao = new TermDao();
    private TermLinksDao linksDao = new TermLinksDao();
    private LinkStructureDao linkStructureDao = new LinkStructureDao();

    public LinkStructureBuilder() throws SQLException, ClassNotFoundException {
        this.allTerms = dao.getAllTerms();
        this.definitionDao = new TermDefinitionDao();
        //  separateTerms();
    }

    /**
     * SemanticGroup which found here dependence on links like "See.."
     * Find dependency between term, link and definition, where that link we found
     * Save result to db
     *
     * @param collectionName
     */
    public void findSemanticGroupEntityAndSaveToDB(String collectionName) {
        Collection<Integer> idTerms = allTerms.values();
        List<SemanticGroupEntity> semanticGroupEntities = new ArrayList<SemanticGroupEntity>();
        int size = idTerms.size();
        int i = 0;
        for (Integer id : idTerms) {
            System.out.println(i + " in " + size);
            List<TermDefinitionEntity> entityList = definitionDao.getTermDefinitionByTermID(id);
            if (entityList.size() != 1) {
                continue;
            } else {
                SemanticGroupEntity dto = new SemanticGroupEntity();
                dto.setIdTerm(id);
                dto.setSentenceId(entityList.get(0).getIdDefinition());
                List<Integer> links = linksDao.getLinksByIdDef(entityList.get(0).getIdDefinition());
                if (links.size() > 0) {
                    dto.setTermLink(links);
                } else {
                    continue;
                }
                semanticGroupEntities.add(dto);
                linkStructureDao.addToLinkStructure(dto, collectionName);
            }
            i++;
        }
    }

    public void builder() throws SQLException, ClassNotFoundException {
        List<Integer> idTermsNotContainLinks = getIdTermsNotContainLinks();
        List<Integer> uniqueTerms = new ArrayList<Integer>();
        int i = 0;
        int size = idTermsNotContainLinks.size();
        for (Integer id : idTermsNotContainLinks) {
            log.info(i + " in " + size);
            if (!findTermsInDefinition(id, idTermsNotContainLinks)) {
                uniqueTerms.add(id);
            }
            i++;
        }

        System.out.println("result:" + uniqueTerms.size());
        FileReaderWriter.write("unique.txt", uniqueTerms);

    }

    private List<Integer> getIdTermsNotContainLinks() throws SQLException, ClassNotFoundException {
        TermLinksDao dao = new TermLinksDao();
        List<Integer> idTerms = dao.getTermsNotContainLink();
        return idTerms;
    }

    @Deprecated
    public boolean findTermsInDefinition(int idTerm, List<Integer> terms) {
        String key = dao.getTermById(idTerm).getHeadWord();
        List<TermDefinitionEntity> definitionEntities = definitionDao.getNotNUllTermDefinitionByTermID(idTerm);
        for (TermDefinitionEntity entity : definitionEntities) {
            String def = entity.getDefinition();
            //  SemanticGroupEntity dto = new SemanticGroupEntity();
            //  dto.setSentenceId(entity.getIdDefinition());
            //   dto.setIdTerm(idTerm);

            for (Integer id : terms) {
                String term = dao.getTermById(id).getHeadWord();
                if (term.equals(key)) continue;
                if (def.contains(term)) {
                    log.info(def);
                    log.info(term);
                    //    semanticGroupEntities.add()
                    return true;
                }


//                if(term.equals("u") || term.equals("1")) continue;
//                if(def.contains(term)){
//                    System.out.println(def);
//                    System.out.println(term);
//
//                //    semanticGroupEntities.add()
//                    return true;
//                }
            }

        }
        return false;
    }

    public void separateTerms(Map<String, Integer> terms,
                              Map<String, Integer> collocationTerms,
                              Map<String, Integer> nonCollocationsTerms) {
        for (String term : terms.keySet()) {
            if (term.split(" ").length > 1) {
                collocationTerms.put(term, terms.get(term));
            } else {
                nonCollocationsTerms.put(term, terms.get(term));
            }
        }
    }
}
