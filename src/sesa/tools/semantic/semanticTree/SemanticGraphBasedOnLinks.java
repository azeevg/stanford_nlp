package sesa.tools.semantic.semanticTree;

import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.tools.semantic.VisualizationGraph;
import sesa.common.database.entity.SemanticGroupEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static sesa.common.utility.strings.StringUtility.emptyNChars;

/**
 * <b>Semantic graph based on links like "See some_think"</b>
 * Some terms definition contain link like "See.."
 * Class has methods for build graph based on that information
 * Information about links is stored in db.table = "link_comp_structure"
 * Created by Ira on 04.06.2016.
 */
public class SemanticGraphBasedOnLinks {
    private LinkStructureDao dao = new LinkStructureDao();
    private TermDao termDao = new TermDao();
    private List<Integer> listId = new ArrayList<Integer>();
    private final int MAX = 5; // max depth for build graph
    private String collectionName = "link_comp_structure";
    Set<Integer> vertex = new HashSet<Integer>();
    List<SemanticEdge> edge = new ArrayList<SemanticEdge>();
    //  int id = 19546;
    private VisualizationGraph vis = new VisualizationGraph();

    public SemanticGraphBasedOnLinks() throws SQLException, ClassNotFoundException {
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public void buildGraph(int id) {
        System.out.println(dao.getListSemanticGroupDtoByTermID(id, collectionName));
        System.out.println("-----------------------");
        SemanticNode node = new SemanticNode();
        node.setIdTerm(id);

        node = readSemanticStructure(node, 0);
        printGraph(node, 0);
        vis.printEdgesToFile("data/graph_edges.txt", edge);
        vis.printVertexToFile("data/graph_vertex.txt", vertex);
    }

    public void buildAndPrintGraph(){

    }

    public void buildGraph(String term) {
        int id = termDao.getTermByName(term).getId();
        System.out.println(dao.getListSemanticGroupDtoByTermID(id, collectionName));
        System.out.println("-----------------------");
        SemanticNode node = new SemanticNode();
        node.setIdTerm(id);
        node = readSemanticStructure(node, 0);
        printGraph(node, 0);
        vis.printEdgesToFile("data/graph_edges.txt", edge);
        vis.printVertexToFile("data/graph_vertex.txt", vertex);
    }


    private SemanticNode readSemanticStructure(SemanticNode parentNode, int i) {
        SemanticGroupEntity dto = dao.getListSemanticGroupDtoByTermID(parentNode.getIdTerm(), collectionName);
        vertex.add(parentNode.getIdTerm());
        if (listId.contains(parentNode.getIdTerm())) {
            return parentNode;
        } else {
            listId.add(parentNode.getIdTerm());
        }
        if (i > MAX) {
            return parentNode;
        }
        if (dto.getTermLink().size() == 0) {
            System.out.println("###:" + parentNode.getIdTerm());
        }

        for (int linkId : dto.getTermLink()) {
            SemanticNode child = new SemanticNode(linkId);
            parentNode.addChild(child);
            vertex.add(linkId);
            edge.add(new SemanticEdge(parentNode.getIdTerm(), linkId));
            readSemanticStructure(child, i + 1);
        }

        return parentNode;

    }

    public void printGraph(SemanticNode parentNode, int i) {
        int tmp = 3;
        if (parentNode.getChildList().size() > 0 && i == 0) {
            System.out.println(emptyNChars(i * tmp) + "i:" + i + "  parent: " + termDao.getTermById(parentNode.getIdTerm()).getHeadWord());
        }

        for (SemanticNode child : parentNode.getChildList()) {
            System.out.println(emptyNChars((i + 1) * tmp) + "i:" + (i + 1) + "  " + termDao.getTermById(child.getIdTerm()).getHeadWord());
            printGraph(child, i + 1);
        }
    }
}
