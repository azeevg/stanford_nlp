package sesa.common.model.processing.support;

public enum RelationMatchType {
  NotMatched,
  Matched,
  PartiallyMatched
}
