package sesa.tools.statistics;


import org.apache.commons.math3.analysis.ParametricUnivariateFunction;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.fitting.CurveFitter;
import org.apache.commons.math3.fitting.PolynomialFitter;
import org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer;
import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.commons.math3.stat.regression.UpdatingMultipleLinearRegression;
import org.jfree.ui.RefineryUtilities;
import sesa.common.builder.BuilderException;
import sesa.common.configuration.Categories;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.features.Pair;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.dictionary.DictionaryUtils;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.builder.DictionaryDocumentBuilder;
import sesa.common.utility.vector.DocumentUtils;

import java.sql.SQLException;
import java.util.*;

public class EntropyCalculator extends Application {
    private DocumentsDao documentsDao = new DocumentsDao();
    private static final int STARTING_X_INDEX = 1;
    private String[] categoryNames = Categories.ALL_CATEGORIES;
    private double[][] data = {{0.1, 0.2}, {338.8, 337.4}, {118.1, 118.2},
            {888.0, 884.6}, {9.2, 10.1}, {228.1, 226.5}, {668.5, 666.3}, {998.5, 996.3},
            {449.1, 448.6}, {778.9, 777.0}, {559.2, 558.2}, {0.3, 0.4}, {0.1, 0.6}, {778.1, 775.5},
            {668.8, 666.9}, {339.3, 338.0}, {448.9, 447.5}, {10.8, 11.6}, {557.7, 556.0},
            {228.3, 228.1}, {998.0, 995.8}, {888.8, 887.6}, {119.6, 120.2}, {0.3, 0.3},
            {0.6, 0.3}, {557.6, 556.8}, {339.3, 339.1}, {888.0, 887.2}, {998.5, 999.0},
            {778.9, 779.0}, {10.2, 11.1}, {117.6, 118.3}, {228.9, 229.2}, {668.4, 669.1},
            {449.2, 448.9}, {0.2, 0.5}
    };

    public EntropyCalculator() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {


        calculateOverAllTFEntropy();
        //calculateOverAllStrictEntropy();
        //test();
        if (true)
            return true;
        //countOverAllStrictPower();


        //if (true)
        //  return true;
    /*Map<String, List<String>> all = new HashMap<String, List<String>>();
    all.put("d1", new LinkedList<String>(Arrays.asList("1","2","3")));
    all.put("d2", new LinkedList<String>(Arrays.asList("3","3","1")));
    all.put("d3", new LinkedList<String>(Arrays.asList("4","4","4")));
    all.put("d4", new LinkedList<String>(Arrays.asList("5","4","5")));*/


        String categoryName = Categories.ALGEBRA_CATEGORY;
        Map<String, List<String>> all = loadAllValidTokensByDocuments(categoryName);
        Map<String, Map<String, Double>> tfMap = getTFMap(all);
        Map<String, Double> avgTfMap = getAvgTfMap(all);

        Set<String> strictSet = DictionaryUtils.loadCategoryDictionary(/*dbController, */categoryName);
        Map<String, Double> strictMap = getStrictMap(all, strictSet);

        LinkedList<Pair<Double, Double>> tfSeriesData = getMapSeriesData(avgTfMap);
        LinkedList<Pair<Double, Double>> strictSeriesData = getMapSeriesData(strictMap);


        // getBestRegression(strictSeriesData);
        // if(true)
        //     return true;

        //getRegressionResult(strictSeriesData);
        //getCurveMapFitting(strictSeriesData);
        //getMapFitting(tfSeriesData);
        //getMapFitting(strictSeriesData);


    /*String[] categoryNames = Categories.ALL_CATEGORIES;
    for (String categoryName : categoryNames) {
      Map<String, List<String>> all = loadAllValidTokensByDocuments(categoryName);
      LoggingUtility.info("CATEGORY = " + categoryName);
      double tfEntropy = calculateTFEntropy(categoryName, all);
      LoggingUtility.info("TF ENTROPY = " + tfEntropy);
      double strictEntropy = calculateStrictEntropy(categoryName, all);
      LoggingUtility.info("STRICT ENTROPY = " + strictEntropy);
    } */


        //getFirstMapValues(strictMap);

        LinkedList<LinkedList<Pair<Double, Double>>> seriesDataList = new LinkedList<LinkedList<Pair<Double, Double>>>();

        LinkedList<Pair<Double, Double>> lgSeriesData = new LinkedList<Pair<Double, Double>>();
        LinkedList<Pair<Double, Double>> cuttedSeriesData = new LinkedList<Pair<Double, Double>>();
        for (Pair<Double, Double> pair : strictSeriesData) {
            Double right = pair.getRight();
            //System.out.println(pair.getRight());
            boolean first = true;
            double shift = 0;
            if (right > 1e-4) {
                if (first) {
                    shift = log10(pair.getRight());
                    first = false;
                }
                double lgLeft = log10(pair.getLeft());
                double lgRight = log10(pair.getRight());
                lgSeriesData.add(new Pair(lgLeft, (-0.7003088621286712) * lgLeft + (-1.709480161849656)));
                cuttedSeriesData.add(new Pair(lgLeft, lgRight));
            }
        }

        seriesDataList.add(lgSeriesData);
        seriesDataList.add(cuttedSeriesData);

        final Chart demo = new Chart("Ранговое распределение лексем. Закон Ципфа.", seriesDataList, "Раговое распределение лексем. Закон Ципфа.", "lg(Ранг лексемы)", "lg(p)");

        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

        return true;
    }


    public void test() {

        double result = 0;
        double limit = 1e6;
        for (int i = 1; i < limit; i++) {
            double part = 1d / i;
            result += log2(part) * part;
        }

        LoggingUtility.info("RANK RESULT = " + result);

        for (int i = 1; i < limit; i++) {
            double part = 1d / limit;
            result += log2(part) * part;

        }

        LoggingUtility.info("UNIFORM RESULT = " + result);


    }

    public void getBestRegression(LinkedList<Pair<Double, Double>> strictSeriesData) {


        double limit = 1e-5;
        double best = Double.MAX_VALUE;

        for (int i = 1; i < 1000; i++) {
            try {
                final SimpleRegression regression = new SimpleRegression(true);
                final UpdatingMultipleLinearRegression iface = regression;
                for (Pair<Double, Double> pair : strictSeriesData) {
                    Double right = pair.getRight();
                    if (right > limit * i) {
                        //System.out.println(pair.getLeft());
                        iface.addObservation(new double[]{log10(pair.getLeft())}, log10(right));
                    }
                }
                final RegressionResults fullReg = iface.regress();


                double diff = Math.abs(Math.abs(0.89) - Math.abs(fullReg.getParameterEstimate(1)));
                LoggingUtility.info("INTERCEPT  = " + fullReg.getParameterEstimate(0));
                LoggingUtility.info("SLOPE = " + fullReg.getParameterEstimate(1));
                LoggingUtility.info("DIFF = " + diff);
                LoggingUtility.info(i + "");


                System.out.println("DIFF = " + diff);
                if (diff < best)
                    best = diff;
            } catch (Exception e) {
                break;
            }
        }

        System.out.println("BEST = " + best);

    }

    private void calculateOverAllTFEntropy() throws SQLException, ClassNotFoundException {
        for (String thisCategoryName : categoryNames) {
            Map<String, List<String>> thisAll = loadAllValidTokensByDocuments(thisCategoryName);
            Map<String, Double> thisTFMap = getAvgTfMap(thisAll);
            LoggingUtility.info("D = " + thisCategoryName + " S = " + thisTFMap.size());
            for (String thatCategoryName : categoryNames) {
                if (thisCategoryName.equals(thatCategoryName)) {
                    double entropy = countEntropy(thisTFMap, thisTFMap.keySet());
                    LoggingUtility.info("C = " + thatCategoryName + "E = " + entropy);
                } else {
                    Map<String, List<String>> thatAll = loadAllValidTokensByDocuments(thatCategoryName);
                    Map<String, Double> thatTFMap = getAvgTfMap(thatAll);
                    double entropy = countEntropy(thatTFMap, thisTFMap.keySet());
                    LoggingUtility.info("C = " + thatCategoryName + " E = " + entropy);
                }
                System.gc();
            }
        }
    }

    private void calculateOverAllStrictEntropy() throws SQLException, ClassNotFoundException {
        for (String thisCategoryName : categoryNames) {
            Map<String, List<String>> thisAll = loadAllValidTokensByDocuments(thisCategoryName);
            Set<String> thisStrictSet = DictionaryUtils.loadCategoryDictionary(/*dbController,*/ thisCategoryName);
            Map<String, Double> thisStrictMap = getStrictMap(thisAll, thisStrictSet);
      /*LoggingUtility.info("D = " + thisCategoryName + " S = " + thisStrictMap.size());
      double entropy = countEntropy(thisStrictMap, thisStrictMap.keySet());
      LoggingUtility.info("C = " + thisCategoryName + "E = " + entropy);*/
            for (String thatCategoryName : categoryNames) {
                if (thisCategoryName.equals(thatCategoryName)) {

                    double entropy = countEntropy(thisStrictMap, thisStrictMap.keySet());
                    LoggingUtility.info("C = " + thatCategoryName + "E = " + entropy);
                } else {

                    Map<String, List<String>> thatAll = loadAllValidTokensByDocuments(thatCategoryName);
                    Set<String> thatStrictSet = DictionaryUtils.loadCategoryDictionary(/*dbController,*/ thatCategoryName);
                    Map<String, Map<String, Double>> tfMap = getTFMap(thatAll);

                    LoggingUtility.info("UNIQUE MAP SIZE = " + tfMap.size());
                    double zeroValue = 1d / tfMap.size();
                    System.out.println("ZERO VALUE = " + zeroValue);
                    Map<String, Double> thatStrictMap = getStrictMap(thatAll, thatStrictSet);
                    double entropy = countEntropy(thatStrictMap, thisStrictMap.keySet(), zeroValue);
                    LoggingUtility.info("C = " + thatCategoryName + " E = " + entropy);
                }
                System.gc();
            }
        }
    }

    private void countKernelPower() {

    }




    public void getRegressionResult(LinkedList<Pair<Double, Double>> series) {
//    final SimpleRegression noIntRegression = new SimpleRegression(true);
//    for (Pair<Double, Double> pair : series) {
//      noIntRegression.addData(Math.log(pair.getLeft()), Math.log(pair.getRight()));
//    }
//
//   final RegressionResults results =  noIntRegression.regress();
//    results.
//
//    LoggingUtility.info("SLOPE = "  + results.g);
//    /*double[] estimates = results.getParameterEstimates();

//    System.out.println("BEST FITTING: ");
//    for(double e : estimates)
//      System.out.println(e);*/


        final SimpleRegression regression = new SimpleRegression(true);
        final UpdatingMultipleLinearRegression iface = regression;
        for (Pair<Double, Double> pair : series) {
            Double right = pair.getRight();
            if (right > 1e-4) {
                System.out.println(pair.getLeft());
                iface.addObservation(new double[]{log10(pair.getLeft())}, log10(right));
            }
        }

        final RegressionResults fullReg = iface.regress();
        System.out.println("INTERCEPT = ");
        System.out.println("INTERCEPT = " + fullReg.getParameterEstimate(0));
        System.out.println("SLOPE = " + fullReg.getParameterEstimate(1));

    }

    private double log10(double x) {
        return Math.log(x) / Math.log(10);
    }


    public void getMapFitting(LinkedList<Pair<Double, Double>> series) {
        final LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
        final PolynomialFitter fitter = new PolynomialFitter(optimizer);


        for (Pair<Double, Double> pair : series) {
            fitter.addObservedPoint(pair.getLeft(), pair.getRight());
        }

        final double[] best = fitter.fit(new double[]{-1e-20, 3e15});


        System.out.println("BEST FITTING: ");
        for (double b : best)
            System.out.println(b);

    }


    public void getCurveMapFitting(LinkedList<Pair<Double, Double>> series) {
        LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
        CurveFitter<ParametricUnivariateFunction> fitter = new CurveFitter<ParametricUnivariateFunction>(optimizer);

        for (Pair<Double, Double> pair : series) {
            fitter.addObservedPoint(pair.getLeft(), pair.getRight());
        }

        ParametricUnivariateFunction sif = new SimpleInverseFunction();
        final double[] best = fitter.fit(sif, new double[]{1e-1, 1e-1});
        System.out.println("BEST FITTING: ");
        for (double b : best)
            System.out.println(b);


    }


    private static class SimpleInverseFunction implements ParametricUnivariateFunction {

        public double value(double x, double... parameters) {
            return parameters[0] / x + (parameters.length < 2 ? 0 : parameters[1]);
        }

        public double[] gradient(double x, double... doubles) {
            double[] gradientVector = new double[doubles.length];
            gradientVector[0] = 1 / x;
            if (doubles.length >= 2) {
                gradientVector[1] = 1;
            }
            return gradientVector;
        }
    }

    public void getFitting() {

        final RealDistribution rng = new UniformRealDistribution(-100, 100);
        rng.reseedRandomGenerator(64925784252L);

        final LevenbergMarquardtOptimizer optim = new LevenbergMarquardtOptimizer();
        final PolynomialFitter fitter = new PolynomialFitter(optim);

        final double[] coeff = {12.9, -3.4, 2.1}; // 12.9 - 3.4 x + 2.1 x^2
        final PolynomialFunction f = new PolynomialFunction(coeff);

        // Collect data from a known polynomial.
        for (int i = 0; i < 100; i++) {
            final double x = rng.sample();
            fitter.addObservedPoint(x, f.value(x));
        }

        // Start fit from initial guesses that are far from the optimal values.
        final double[] best = fitter.fit(new double[]{-1e-20, 3e15, -5e25});
        for (double b : best)
            System.out.println(b);


    }

    private PolynomialFunction buildRandomPolynomial(int degree, Random randomizer) {
        final double[] coefficients = new double[degree + 1];
        for (int i = 0; i <= degree; ++i) {
            coefficients[i] = randomizer.nextGaussian();
        }
        return new PolynomialFunction(coefficients);
    }

    public void getFirstMapValues(Map<String, Double> map) {
        ValueComparator bvc = new ValueComparator(map);
        TreeMap<String, Double> sorted_map = new TreeMap<String, Double>(bvc);
        sorted_map.putAll(map);
        //ArrayList<String> array  = new ArrayList<String>(sorted_map.keySet());
        List<String> first = new LinkedList<String>();
        int counter = 0;
        for (String key : sorted_map.keySet()) {
            if (counter++ > 100)
                break;
            //first.add(key);
            // LoggingUtility.info("KEY = " + key + " VALUE = " + map.get(key));
        }

        //System.out.println("SUBMAP = " + first);
        //LoggingUtility.info("SUBMAP = " + first);
    }


    class ValueComparator implements Comparator<String> {


        Map<String, Double> base;

        public ValueComparator(Map<String, Double> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.
        public int compare(String a, String b) {
            if (base.get(a) <= base.get(b)) {
                return -1;
            } else {
                return 1;
            } // returning 0 would merge keys
        }
    }

    public LinkedList<Pair<Double, Double>> getMapSeriesData(Map<String, Double> map) {

        LinkedList<Pair<Double, Double>> data = new LinkedList<Pair<Double, Double>>();
        Collection<Double> values = map.values();
        List<Double> ordered = new ArrayList<Double>(values);
        Comparator comparator = Collections.reverseOrder();
        Collections.sort(ordered, comparator);
        double counter = STARTING_X_INDEX;
        for (Double value : ordered) {
            data.add(new Pair<Double, Double>(counter, value));
            counter++;
        }
        return data;
    }

    public Pair<Double, Double> p(double l, double r) {
        return new Pair<Double, Double>(l, r);
    }

    public double calculateTFEntropy(String categoryName, Map<String, List<String>> all) {
        //Map<String, Map<String, Double>> tfMap = getTFMap(all);
        Map<String, Double> avgTfMap = getAvgTfMap(all);
        validateMap(avgTfMap);
        return countEntropy(avgTfMap);
    }

    public double calculateStrictEntropy(String categoryName, Map<String, List<String>> all) throws SQLException, ClassNotFoundException {
        Set<String> strictSet = DictionaryUtils.loadCategoryDictionary(/*dbController,*/ categoryName);
        Map<String, Double> strictMap = getStrictMap(all, strictSet);
        validateMap(strictMap);
        return countEntropy(strictMap);
    }


    public Map<String, Double> getStrictMap(Map<String, List<String>> all, Set<String> strictSet) {
        Map<String, Double> strictMap = new HashMap<String, Double>();
        Set<String> docs = all.keySet();
        for (String doc : docs) {
            List<String> docTokens = all.get(doc);
            for (String token : docTokens) {
                if (strictSet.contains(token)) {
                    if (!strictMap.containsKey(token)) {
                        strictMap.put(token, 1d);
                    } else {
                        double value = strictMap.get(token);
                        strictMap.put(token, value + 1);
                    }
                }
            }
        }


        //LoggingUtility.info("" + strictMap.get("iypt"));

        double count = 0;
        for (String elem : strictMap.keySet())
            count += strictMap.get(elem);

        Map<String, Double> resultMap = new HashMap<String, Double>();
        for (String elem : strictMap.keySet()) {
            double value = strictMap.get(elem);
            resultMap.put(elem, value / count);
        }

        return resultMap;
    }

    public void validateMap(Map<String, Double> map) {
        double result = 0;
        for (String token : map.keySet()) {
            result += map.get(token);
        }

        LoggingUtility.info("VALIDATION = " + result);
        if (Math.abs(result - 1) > 1e-8) {

            LoggingUtility.info("VALIDATION FAILED!!!!");
        }
    }


    public Map<String, Double> getAvgTfMap(Map<String, List<String>> all) {
        double docsCount = all.keySet().size();
        Map<String, Map<String, Double>> tfMap = getTFMap(all);
        Map<String, Double> avgTfMap = new HashMap<String, Double>();

        for (String token : tfMap.keySet()) {
            Map<String, Double> docs = tfMap.get(token);
            double result = 0;
            for (String doc : docs.keySet()) {
                result += docs.get(doc);
            }
            result = result / docsCount;
            avgTfMap.put(token, result);
        }

        return avgTfMap;
    }

    public Map<String, Map<String, Double>> getTFMap(Map<String, List<String>> all) {
        Map<String, Map<String, Double>> tfMap = new HashMap<String, Map<String, Double>>();
        Set<String> docs = all.keySet();
        for (String doc : docs) {
            List<String> docTokens = all.get(doc);
            double docSize = docTokens.size();
            for (String token : docTokens) {
                if (!tfMap.containsKey(token)) {
                    Map<String, Double> docMap = new HashMap<String, Double>();
                    docMap.put(doc, 1d / docSize);
                    tfMap.put(token, docMap);
                } else {
                    Map<String, Double> docMap = tfMap.get(token);
                    if (!docMap.containsKey(doc)) {
                        docMap.put(doc, 1d / docSize);
                    } else {
                        double value = docMap.get(doc);
                        docMap.put(doc, value + 1d / docSize);
                    }
                }
            }
        }
        return tfMap;
    }

    public double countEntropy(Map<String, Double> probabilityMap, Set<String> dictionary) {
        double result = 0;
        for (String token : dictionary) {
            if (probabilityMap.containsKey(token)) {
                double probability = probabilityMap.get(token);
                result += probability * log2(probability);
            }
        }

        if (result != 0)
            return (-1) * result;
        else
            return Double.MAX_VALUE;
    }


    public double countEntropy(Map<String, Double> probabilityMap, Set<String> dictionary, double zeroValue) {
        double result = 0;
        for (String token : dictionary) {
            if (probabilityMap.containsKey(token)) {
                double probability = probabilityMap.get(token);
                result += probability * log2(probability);
            } else {
                double probability = zeroValue;
                result += probability * log2(probability);
                LoggingUtility.info("RESULT = " + result);
            }
        }
        return -result;
    }


    public double countEntropy(Map<String, Double> probabilityMap) {
        double result = 0;
        for (String token : probabilityMap.keySet()) {
            double probability = probabilityMap.get(token);
            result += probability * log2(probability);
        }

        if (result != 0)
            return (-1) * result;
        else
            return Double.MAX_VALUE;
    }

    private static double log2(double x) {
        // Math.log is base e, natural log, ln
        return Math.log(x) / Math.log(2);
    }

    public Map<String, List<String>> loadAllValidTokensByDocuments(String categoryName) throws SQLException, ClassNotFoundException {
        Map<String, List<String>> tokens = new HashMap<String, List<String>>();

        String categoryCollection = Categories.buildCategoryHtmlCollectionName(categoryName);
        IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryCollection);//dbController.getDocumentsCollectionIterator(categoryCollection);

        while (iterator.hasNext()) {
            IDBEntity doc = iterator.next().get();
            Map<String, Object> map = doc.toMap();
            String id = doc.getID();


            String text = (String) map.get("w_filtered_text");
            IDocument document = null;
            DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
            try{
                document = documentBuilder.buildDocument(id, text, null);
            }catch (BuilderException e){
                e.printStackTrace();
                //TODO: add log
            }
          //  IDocument document = DictionaryDocumentBuilder.buildDocument(id, text, null);
            List<SentenceTree> sentences = document.getSentences();
            List<String> documentTokens = DocumentUtils.getTokens(sentences);
            tokens.put(id, documentTokens);
        }
        return tokens;
    }

    public void countOverAllStrictPower() throws SQLException, ClassNotFoundException {
        //Map<String, List<String>> tokens = new HashMap<String, List<String>>();

        int overallCount = 0;
        for (String categoryName : categoryNames) {
            Set<String> strictSet = DictionaryUtils.loadCategoryDictionary(/*dbController,*/ categoryName);
            String categoryCollection = Categories.buildCategoryHtmlCollectionName(categoryName);
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryCollection);//dbController.getDocumentsCollectionIterator(categoryCollection);


            int count = 0;
            HashSet<String> docs = new HashSet<String>();
            while (iterator.hasNext()) {
                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String id = doc.getID();


                String text = (String) map.get("w_filtered_text");
                IDocument document = null;
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
                try{
                    document = documentBuilder.buildDocument(id, text, null);
                }catch (BuilderException e){
                    e.printStackTrace();
                    //TODO: add log
                }
               // IDocument document = DictionaryDocumentBuilder.buildDocument(id, text, null);
                List<SentenceTree> sentences = document.getSentences();
                Set<String> documentTokens = DocumentUtils.getUniqueTokens(sentences);
                for (String token : documentTokens) {
                    if (strictSet.contains(token)) {
                        if (!docs.contains(id)) {
                            docs.add(id);
                            count++;
                        }
                    }
                }
            }
            LoggingUtility.info("POWER IN " + categoryName + " = " + count);
            overallCount += count;
        }
        LoggingUtility.info("POWER ALL = " + overallCount);
        //return tokens;
    }
}


