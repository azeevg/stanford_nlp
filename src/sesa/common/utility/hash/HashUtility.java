package sesa.common.utility.hash;

import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.strings.StringUtility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;
import java.util.TreeSet;

public class HashUtility extends LoggingUtility {

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    /**
     * US Secure Hash Algorithm
     * @param text
     * @return ���������� ��� ������, ����������� �� ��������� US Secure Hash Algorithm 1
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    private static String getSHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        md.update(text.getBytes("UTF-8"), 0, text.length());
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    private static Set<String> sortSet(Set<String> set) {
        Set<String> sortedSet = new TreeSet<String>();
        for (String item : set)
            sortedSet.add(item);

        return sortedSet;
    }

    public static String generateHash(String string) {
        try {
            return getSHA1(StringUtility.getHash(string));
        } catch (NoSuchAlgorithmException e) {
            error("Can't find SHA1 implementation");
        } catch (UnsupportedEncodingException e) {
            error("Unsupported encoding");
        }

        return "";
    }
}
