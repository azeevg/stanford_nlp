package sesa.common.utility.dictionary;

/**
 * Created by gleb on 08.05.17.
 */


import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.entity.dictionary.TermEntity;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Replace all references like "See smth." in DEF in terms_comp_definition by corresponding definition of "smth".
 */
public class DefinitionReferencesResolver {
    private final TermDao termDao;
    private final TermDefinitionDao termDefinitionDao;

    public DefinitionReferencesResolver() throws SQLException, ClassNotFoundException {
        this.termDao = new TermDao();
        this.termDefinitionDao = new TermDefinitionDao();
    }

    public List<Integer> collectAllReferences() {
        return termDefinitionDao.getAllReferences();
    }

    public List<String> getCorrespondingStringDefs(int defId) {
        String referencingDef = termDefinitionDao.getDefinitionByDefinitionId(defId);
        referencingDef = referencingDef.substring(4, referencingDef.length() - 1).trim();
        TermEntity termEntity = termDao.getTermByName(referencingDef);
        int correspondingTerm = 0;

        if (termEntity != null)
            correspondingTerm = termEntity.getId();

        if (correspondingTerm == 0)
            correspondingTerm = termDao.getTermByNameLike(referencingDef);

        return termDefinitionDao.getDefsByTermId(correspondingTerm);
    }


    public boolean replaceReference(int destId, int sourceId) {
        return termDefinitionDao.replaceReferences(destId, sourceId);
    }

    public int replaceAllReferences() {
        List<Integer> references = collectAllReferences();
        final int[] replacedRefs = {0};

        if (references.size() == 0)
            return 0;

        references.forEach(ref -> {
            List<String> correspondingDefs = getCorrespondingStringDefs(ref);
//            String def = termDefinitionDao.getDefinitionByDefinitionId(ref);
            List<String> defs = termDefinitionDao.getNotNUllTermDefinitionByTermID(ref)
                    .stream().map(TermDefinitionEntity::getDefinition)
                    .collect(Collectors.toList());
            final int r = ref;
            correspondingDefs = correspondingDefs.stream()
                    .filter(s -> {
                        if (s.startsWith("See ")) {
                            String substr = s.substring(4, s.length() - 1).trim();
                            String term = termDao.getTermById(termDefinitionDao.getTermIdByDefId(r)).getHeadWord();
                            boolean b = term.equals(substr)
                                    || (term.contains(substr) && (1.0 * substr.length() / term.length()) > 0.5);

                            return !s.equals("???") && !defs.contains(s) && !b;
                        } else {
                            return !s.equals("???") && !defs.contains(s);
                        }
//                        return !s.equals("???") && !defs.contains(s);

                    })
                    .collect(Collectors.toList());


            if (!correspondingDefs.isEmpty()) {
                int termId = termDefinitionDao.getTermIdByDefId(ref);
                termDefinitionDao.deleteDef(ref);

                correspondingDefs.forEach(correspondingDef -> {
                    TermDefinitionEntity entity = new TermDefinitionEntity(0, termId, correspondingDef);
                    termDefinitionDao.addToTermDefinitionCollections(entity);
                });
                System.out.println("Reference " + ref + " resolved by " + correspondingDefs.size() + " definition.");
                replacedRefs[0]++;
            }
        });

        System.out.println(replacedRefs[0] + " of " + references.size() + " references (\"See ...\") are replaced.");
        return replacedRefs[0];
    }

}
