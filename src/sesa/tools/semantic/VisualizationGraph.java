package sesa.tools.semantic;

import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.utility.FileReaderWriter;
import sesa.tools.semantic.semanticTree.SemanticEdge;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Visualization graphs, save information about graph vertex and edges to files to show in different program
 * Created by Ira on 11.06.2016.
 */
public class VisualizationGraph {
    private static final String TITLE_VERTEX_FILE = "Id;Label";
    private static final String TITLE_EDGE_FILE = "Source;Target";
    private LinkStructureDao dao = new LinkStructureDao();
    private TermDao termDao = new TermDao();

    public VisualizationGraph() throws SQLException, ClassNotFoundException {
    }

    public void printVertexToFile(String fileName) {
        Set<Integer> set = dao.getAllVertex("auto_comp_reference");
        printVertexToFile(fileName, set);
    }

    public void printVertexToFile(String fileName, Set<Integer> set) {
        Map<Integer, String> map = termDao.constructMapTermByIds(set);
        FileReaderWriter.write(fileName, TITLE_VERTEX_FILE);
        for (Integer id : map.keySet()) {
            String name = map.get(id);
            String s = id + ";" + name;
            FileReaderWriter.write(fileName, s);
        }
    }

    public void printEdgesToFile(String fileName) {
        List<SemanticEdge> semanticEdges = dao.getAllEdges("auto_comp_reference");
        printEdgesToFile(fileName, semanticEdges);
    }

    public void printEdgesToFile(String fileName, List<SemanticEdge> list) {
        FileReaderWriter.write(fileName, TITLE_EDGE_FILE);
        for (SemanticEdge dto : list) {
            String s = dto.getSource() + ";" + dto.getTarget();
            FileReaderWriter.write(fileName, s);
        }
    }
}
