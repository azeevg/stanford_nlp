package sesa.common.model.processing.sentence;


import java.util.LinkedList;
import java.util.List;

/**
 *
 * This class represents a list of parse trees for one sentence
 */
public class SentenceForest {

    private List<SentenceTree> trees; /* Sentence variants */

    public List<SentenceTree> getTrees() {
        return trees;
    }
    public SentenceForest() {

        this.trees = new LinkedList<SentenceTree>();
    }

    public void addTree(SentenceTree tree) {
        trees.add(tree);
    }
}
