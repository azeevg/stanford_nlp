package sesa.tools.filters.WIKI.category;

import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.Application;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CategoryFilter extends Application {

    private int counter = 0;
    private DocumentsDao documentsDao = new DocumentsDao();

    public CategoryFilter() throws SQLException, ClassNotFoundException {
    }

    protected boolean body() throws SQLException, ClassNotFoundException {

        Pattern catPattern = buildWrapperPattern("\\[\\[Category:", "\\]\\]");
        documentsDao.truncateDocumentCollection(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
       // dbController.removeCategorizedDocumentWIKI(); // dbConnection.truncate(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
        documentsDao.truncateOrCreateDocumentCollection(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
     //   dbController.truncateOrCreate(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);



        IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_WIKI);
                //dbController.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_WIKI);
        iterator = new DBReportingIterator(iterator, "Extracting categories");
        while (iterator.hasNext()) {

            IDBEntity entity = iterator.next().get();
            List<String> categories = new LinkedList<String>();
            Map<String, Object> map = entity.toMap();

            String text = (String) map.get("text");
            Matcher m = catPattern.matcher(text);

            while (m.find()) {
                categories.add(m.group(1));
            }

            map.put("cat", categories);
            documentsDao.addToDocumentsCollection(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI, entity);
            //dbController.insertToDocumentsCollection(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI, entity);

            if (counter++ % 10000 == 0)
                System.gc();
        }

        return false;
    }

    private Pattern buildWrapperPattern(String startWrapSymbol, String endWrapSymbol) {
        return Pattern.compile(buildDoubleSidedWrapperString(startWrapSymbol, endWrapSymbol));
    }

    private String buildDoubleSidedWrapperString(String startWrapSymbols, String endWrapSymbols) {
        return startWrapSymbols + "([^" + endWrapSymbols + "\\r\\n]*)" + endWrapSymbols;
    }
}
