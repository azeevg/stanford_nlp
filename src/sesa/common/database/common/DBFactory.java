package sesa.common.database.common;

import sesa.common.database.connections.DBConnection;
import sesa.common.database.connections.DBShardedConnection;
import sesa.common.database.connections.DBSingleConnection;

// TODO: delete

@Deprecated
public class DBFactory {

    public static DBConnection createConnection(String dbHosts, int dbPort, String dbName, String dbUser, String dbPassword) {
        if (dbHosts.contains(";"))
            return new DBShardedConnection(dbHosts, dbPort, dbName, dbUser, dbPassword);
        return new DBSingleConnection(dbHosts, dbPort, dbName, dbUser, dbPassword);
    }
}
