package edu.spbstu.egor;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.trees.EnglishGrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TypedDependency;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.semantics.SemanticRelation;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Egor Gorbunov on 29.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class GrammaticalStructureTest {
    private static final String TREES_TEX_FILENAME = "./egor_out/tex/k_trees.tex";
//    private static final String TREES_TEX_FILENAME = "./out/Gleb_tree_1.txt";
    private static final String SENTENCES_FILENAME = "./egor_in/wrong_text/text1.txt";
    private static final int K = 1;

    private static Grammar grammar = new Grammar();
    private static ParserFactory parserFactory = new ParserFactory(grammar);
    private static SentenceGetter sentenceGetter = new SentenceGetter(SENTENCES_FILENAME);
    private static TreeGetter treeGetter = new TreeGetter();
    private static TreeUtils treeUtils = new TreeUtils(grammar.op);

    private static PrintWriter texTreesWriter;

    static {
        treeGetter.setPparser(parserFactory.getMyParser());
        treeGetter.setDparser(parserFactory.getDependencyParser());

        try {
            texTreesWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(new File(TREES_TEX_FILENAME))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        texTreesWriter.println("\\input{./env.tex}");
        texTreesWriter.println("\\begin{document}");
    }
    public static void main(String[] args) throws Exception {

        grammar.op.testOptions.verbose = false;
        System.out.println(sentenceGetter.getText());

        Pattern pattern = Pattern.compile(".*(subj|obj|pred).*");

        // Sesa Grammatical Structure
        StanfordRelationsBuilder relationsBuilder = new StanfordRelationsBuilder(grammar);
        ArrayList<ArrayList<SemanticRelation>> relations = relationsBuilder.getGrammaticalTextStructure(sentenceGetter);


        // Stanford NLP Grammatical Structure
        System.out.println("");
        for (List<? extends HasWord> sentence : sentenceGetter) {
            System.out.println("SENTENCE: " + SentenceGetter.sentenceToString(sentence));

            ArrayList<Tree> parseTrees = treeGetter.getKBestParseTrees(sentence, K);
            for (Tree parseTree : parseTrees) {
                parseTree = treeUtils.transformTreeToStandardShape(parseTree);

                treeUtils.preprocessedTikzPrint(parseTree, texTreesWriter);
                texTreesWriter.print('\n');

                System.out.println("SESA:");
                ArrayList<SemanticRelation> sesaGs = relationsBuilder.getGrammaticalTextStructure(sentence, parseTree);
                for (SemanticRelation sr : sesaGs) {
                    //if (pattern.matcher(sr.getRelationName()).matches())
                    System.out.println(sr);
                }

                System.out.println("STANFORD:");
                GrammaticalStructure stanfGs = new EnglishGrammaticalStructure(parseTree);

                List<TypedDependency> filteredDependencies = new ArrayList<TypedDependency>();

                for (TypedDependency td : stanfGs.typedDependencies(false)) {
                    //if (pattern.matcher(td.reln().getShortName()).matches())
                    filteredDependencies.add(td);
                }

                EnglishGrammaticalStructure.printDependencies(stanfGs, filteredDependencies, parseTree, false, false);
            }

            //break; //TODO: DELETE
        }

        texTreesWriter.println("\\end{document}");
        texTreesWriter.flush();
        texTreesWriter.close();
    }

    private static void printSesaGrammaticalStructure(ArrayList<SemanticRelation> relations) {

    }

    private static void printStanfordGrammaticalStructure(List<TypedDependency> dependencies) {

    }
}
