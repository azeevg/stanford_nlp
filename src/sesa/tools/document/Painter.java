package sesa.tools.document;

import edu.stanford.nlp.io.IOUtils;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.configuration.Categories;
import sesa.common.dataModule.ReadTreeDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.model.processing.document.IDocument;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.vector.DocumentUtils;
import sesa.tools.cache.CacheUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.Arrays;


public class Painter extends Application {

    private static final String COLORED_DIR_PATH = "colored_pages";

    String categoryName = Categories.COMPUTING_CATEGORY;
    //  String title = "Algorithm";
    String title = "Ada (programming language)";

    @Override
    protected boolean body() {
        try {
            generateColoredDocument(categoryName, title);
        } catch (IOException | ClassNotFoundException | SQLException e) {
            LoggingUtility.info(Arrays.toString(e.getStackTrace()));
        }
        return false;
    }

    public void generateColoredDocument(String categoryName, String title) throws IOException, SQLException, ClassNotFoundException {

        ObjectInputStream inputStream = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz");

        StanfordDataModule dataModule = new ReadTreeDataModule("");
        DocumentBuilder stanfordBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
        IDocument stanfordDocument = DocumentUtils.loadDocumentByTitle(categoryName, title, stanfordBuilder);
        CacheUtils.generatePaintedDocument(stanfordDocument);
      /*List<SentenceTree> sentences = stanfordDocument.getSentences();

      String paintedHtml = DocumentUtils.buildPaintedHtml(sentences);

      String id = stanfordDocument.getId();
      String filePath = configuration.getProperty(COLORED_DIR_PATH) + "/" + id + dataModule.getClass().getSimpleName() + ".html";
      FileUtility.writeAllTextToFile(filePath, paintedHtml);*/

        //LoggingUtility.info("Generated html colored file = " + filePath);
        // LoggingUtility.info("Done...");
    }


}
