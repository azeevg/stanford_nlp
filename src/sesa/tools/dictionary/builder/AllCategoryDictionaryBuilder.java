package sesa.tools.dictionary.builder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.builder.BuilderException;
import sesa.common.builder.DictionaryDocumentBuilder;
import sesa.common.configuration.Categories;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.parsing.XMLDocument;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.vector.DocumentUtils;
import sesa.tools.dictionary.AllCatDictionaryBuilder;

import java.sql.SQLException;
import java.util.*;


/**
 * Created by Ira on 11.03.2016.
 */
//TODO: Change AllCatDictionaryBuilder
public class AllCategoryDictionaryBuilder extends Application {

    public final Logger log = LogManager.getLogger(AllCatDictionaryBuilder.class);
    private String[] categoryNames = Categories.TEST_CATEGORIES;
    private DocumentsDao documentsDao = new DocumentsDao();

    public AllCategoryDictionaryBuilder() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        Map<String, Double> dictionary = prepareDictionary().getTokens();
        Map<String, Object> entity = new HashMap<String, Object>();
        entity.put("dictionary", dictionary);
        System.out.println(entity);
       // saveDictionary(entity, Configuration.DB_COLLECTION_TEST_DICTIONARY);

        return true;
    }

    private Dictionary prepareDictionary() throws SQLException, ClassNotFoundException {
        Set<String> documentIdSet = new HashSet<String>();
        Dictionary dictionary = new Dictionary();

        for (String categoryName : categoryNames) {
            String categoryCollection = Categories.buildCategoryHtmlCollectionName(categoryName);
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryCollection);
            iterator = new DBReportingIterator(iterator, categoryCollection + " idf obtaining");

            while (iterator.hasNext()) {
                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String id = doc.getID();
                if (documentIdSet.contains(id)) {
                    continue;
                } else {
                    documentIdSet.add(id);
                }
                String text = (String) map.get("w_filtered_text");
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
                IDocument document = null;
                try {
                    document = documentBuilder.buildDocument(id, text, null);
                } catch (BuilderException e) {
                    log.error(e.getMessage());
                }
                List<SentenceTree> sentences = document.getSentences();
                Set<String> documentUniqueTokens = DocumentUtils.getUniqueTokens(sentences);

                //From complete set of tokens choose unique and calculate frequency of occurrence for every one
                dictionary.merge(documentUniqueTokens);
            }
        }

        return dictionary;
    }

    private void saveDictionary(Map<String, Object> entity, String collectionName) {
        documentsDao.truncateOrCreateDocumentCollection(collectionName);

        documentsDao.addToDocumentsCollection(collectionName, new XMLDocument(entity));

    }


}
