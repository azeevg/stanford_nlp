package sesa.common.database.dao.dictionary;

import sesa.common.database.dao.CommonDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by gleb on 06.05.17.
 */

public class DictionarySourceDao extends CommonDao {

    private static final String COLLECTION_NAME = "dictionary_source";

    public DictionarySourceDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public int getSourceId(String source) {
        int id = 0;

        try (Connection connection = cp.takeConnection()) {
            String query = "SELECT * FROM " + COLLECTION_NAME + " WHERE SRC = '" + source + "';";

            try (Statement st = connection.createStatement();
                 ResultSet rs = st.executeQuery(query)) {

                while (rs.next()) {
                    String src = rs.getString("SRC");

                    if (src.equals(source)) {
                        id = rs.getInt("ID");
                        break;
                    }
                }
            }

            if (id == 0) {
                query = "INSERT INTO " + COLLECTION_NAME + " (SRC) VALUES ('" + source + "');";

                try (Statement st = connection.createStatement()) {

                    st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                    ResultSet rs = st.getGeneratedKeys();

                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                    rs.close();
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return id;
    }

}
