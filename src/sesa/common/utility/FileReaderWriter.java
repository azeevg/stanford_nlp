package sesa.common.utility;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

/**
 * Created by Ira on 25.04.2016.
 */
// public class FileUtility extends LoggingUtility
    //deprecate
public class FileReaderWriter {
    public static final Logger log = LogManager.getLogger(FileReaderWriter.class);
    /**
     * Append to file text
     * @param fileName
     * @param text
     */
    public static void write(String fileName, String text) {
        try {
            FileWriter fileWriter = new FileWriter(fileName,true);
            BufferedWriter BWf = new BufferedWriter(fileWriter);
            BWf.write(text); // ������������ ������ � ����� �����
            BWf.newLine();     // ��������� �� ��������� ������
            BWf.close();
            fileWriter.close();

        } catch (IOException e) {
            log.error(e.getMessage());
        }


    }

    /**
     * Read list of stings from file to array
     * @param fileName
     * @return
     */
    public static ArrayList<String> read(String fileName){

        ArrayList<String> strings = new ArrayList<String>();
        try{
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                strings.add(in.nextLine());
            }
        }catch(FileNotFoundException e){
            log.error("File " + fileName + " not found.\n" + e.getMessage());
        }

        return strings;
    }


    /**
     *
     * @param fileName text contains list string in json format
     * @param key
     * @return
     */
    public static List<String> readJSONFromFile(String fileName, String key){
       // key = "dictionary_text";
        List<String> list = new ArrayList<String>();
        JSONParser parser = new JSONParser();

        try {
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                String jsonStr = in.nextLine();
                try {
                    Object obj = parser.parse(jsonStr);
                    org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
                    if (jsonObject.get(key) != null) {
                        list.addAll((List<String>)jsonObject.get(key));
                    }
                } catch (ParseException pe) {
                    pe.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

//    public static void write(String fileName, Collection<String> list) {
//        try {
//            FileWriter fileWriter = new FileWriter(fileName,true);
//            BufferedWriter BWf = new BufferedWriter(fileWriter);
//            for(String s:list){
//                BWf.write(s); // ������������ ������ � ����� �����
//                BWf.newLine();     // ��������� �� ��������� ������
//            }
//
//            BWf.close();
//            fileWriter.close();
//
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
//
//
//    }



    public static <T> void write(String fileName, Collection<T> list) {
        try {
            FileWriter fileWriter = new FileWriter(fileName,true);
            BufferedWriter BWf = new BufferedWriter(fileWriter);
            for(T s : list){
                BWf.write(s.toString()); // ������������ ������ � ����� �����
                BWf.newLine();     // ��������� �� ��������� ������
            }

            BWf.close();
            fileWriter.close();

        } catch (IOException e) {
            log.error(e.getMessage());
        }


    }
//
//
//    public static <T> ArrayList<T> read(String fileName){
//
//        ArrayList<T> strings = new ArrayList<T>();
//        try{
//            Scanner in = new Scanner(new File(fileName));
//            while (in.hasNextLine()) {
//                T t = (T)in.nextLine();
//                strings.add(t);
//            }
//        }catch(FileNotFoundException e){
//            log.error("File " + fileName + " not found.\n" + e.getMessage());
//        }
//
//        return strings;
//    }

    /**
     * Read list of stings from file to array
     * @param fileName
     * @return
     */
    public static ArrayList<Integer> readNumber(String fileName){

        ArrayList<Integer> strings = new ArrayList<Integer>();
        try{
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                strings.add(Integer.valueOf(in.nextLine()));
            }
        }catch(FileNotFoundException e){
            log.error("File " + fileName + " not found.\n" + e.getMessage());
        }

        return strings;
    }


}
