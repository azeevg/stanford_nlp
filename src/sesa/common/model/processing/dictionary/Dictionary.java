package sesa.common.model.processing.dictionary;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * This class represents a <br> dictionary </br>
 * Dictionary contains set of tokens and their weight
 */
public class Dictionary implements Iterable<String> {

    private HashMap<String, Double> tokens = new HashMap<String, Double>();

    /**
     * Get weight of word
     *  - if word does not contains in dictionary -> weight = 1
     *  - if word contains in dictionary          -> find weight word in dictionary
     * @param keyword  - word
     * @return weight of word
     */
    public double getWeight(String keyword) {
        if (!tokens.containsKey(keyword))
            return 1.0;
        return tokens.get(keyword);
    }

    public long getSize() {
        return tokens.size();
    }

    /**
     * From complete set of tokens choose unique
     * and
     * calculate frequency of occurrence for everyone
     * @param keywords complete set of tokens
     */
    public void merge(Set<String> keywords) {
        for (String keyword : keywords)
            if (tokens.containsKey(keyword))
                tokens.put(keyword, tokens.get(keyword) + 1.0);
            else
                tokens.put(keyword, 1.0);
    }

    public Iterator<String> iterator() {
        return tokens.keySet().iterator();
    }

    public void setTokens(HashMap<String, Double> tokens){
      this.tokens = tokens;
    }

    public HashMap<String, Double> getTokens() {
        return tokens;
    }
}