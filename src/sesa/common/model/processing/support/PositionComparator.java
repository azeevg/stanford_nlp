package sesa.common.model.processing.support;

import sesa.common.model.processing.sentence.SentenceToken;

import java.util.Comparator;

/**
 * Comparing the positions of tokens in a sentence
 */
public class PositionComparator implements Comparator<SentenceToken> {
    public int compare(SentenceToken o1, SentenceToken o2) {
        return o1.getSentencePosition() - o2.getSentencePosition();
    }
}
