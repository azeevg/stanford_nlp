package sesa.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.configuration.Configuration;
import sesa.common.database.entity.DBEntity;
import sesa.common.database.entity.IDBEntity;
import sesa.common.utility.json.JSONUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Ira on 19.02.2016.
 */
public class DocumentsDao extends CommonDao {
    public final Logger log = LogManager.getLogger(DocumentsDao.class);

    public DocumentsDao() throws SQLException, ClassNotFoundException {
        super();
    }

    /**
     * Create new table with column: id,imported,data
     *
     * @param tableName
     * @return
     */
    public boolean createDocumentCollectionTable(String tableName) {
        PreparedStatement ps = null;
        Connection connection = null;
        boolean isCreated = false;
        try {
            connection = cp.takeConnection();
            String query = "CREATE TABLE `" + tableName + "` (" +
                    "`id` VARCHAR(40)  NOT NULL, " +
                    "`imported` TIMESTAMP  NOT NULL DEFAULT NOW(), " +
                    "`data` LONGTEXT  NOT NULL, " +
                    "" +
                    "PRIMARY KEY (`id`)) " +
                    "ENGINE = MyISAM " +
                    "CHARACTER SET utf8 COLLATE utf8_general_ci";

            ps = connection.prepareStatement(query);
            ps.execute();
            isCreated = true;
        } catch (SQLException e) {
            log.debug("Can't create collection: " + tableName);
            log.error("Exception: " + e.getMessage());
        } finally {
            cp.closeConnection(connection, ps);
        }
        return isCreated;
    }

    public IDBEntity getDocumentById(String collectionName, String id) {
        IDBEntity result = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE id = ?");
            ps.setString(1, id);
            ps.execute();
            resultSet = ps.getResultSet();

            if (resultSet.next())
                result = new DBEntity((String) resultSet.getObject("id"), (String) resultSet.getObject("data"));

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return result;
    }

    /**
     * Insert to table the document which like wiki
     *
     * @param collectionName
     * @param document
     * @return
     */
    public boolean addToDocumentsCollection(String collectionName, IDBEntity document) {
        PreparedStatement ps = null;
        Connection connection = null;

        boolean isInserted = false;
        try {
            connection = cp.takeConnection();
            String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO `" + collectionName + "` (id, data, imported) VALUES(?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, document.getID());
            ps.setString(2, data);
            ps.setTimestamp(3, getNow());
            ps.execute();
            isInserted = true;
        } catch (SQLException e) {
            log.error(e.getMessage());
            log.debug("Can't insert record: " + document.getID());
        } finally {
            cp.closeConnection(connection, ps);
        }


        return isInserted;
    }

    /**
     * Get list of documents from table: "collectionName"
     *
     * @param collectionName
     * @param from
     * @param size
     * @return
     */
    public List<IDBEntity> getDocuments(String collectionName, long from, long size) {
        List<IDBEntity> resultList = new LinkedList<IDBEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` LIMIT ?,?");
            ps.setLong(1, from);
            ps.setLong(2, size);
            ps.execute();
            resultSet = ps.getResultSet();
            while (resultSet.next())
                resultList.add(new DBEntity((String) resultSet.getObject("id"), (String) resultSet.getObject("data")));

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
            resultList = null;
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return resultList;
    }

    public List<IDBEntity> getDocuments(String collectionName) {
        return getDocuments(collectionName, 0, countTotalOfRow(collectionName));
    }

    public boolean addByCategoryDocuments(String categories) {
        return copyByCategory(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI, Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI, categories);
    }

    //copyByCategory
    protected boolean copyByCategory(String srcCollectionName, String destCollectionName, String regexpExpression) {
        Connection connection = null;
        PreparedStatement s = null;
        boolean isCopy = false;
        try {
            connection = cp.takeConnection();
            String query = "INSERT INTO " + destCollectionName + " SELECT * FROM " + srcCollectionName + " WHERE cat REGEXP ?";
            s = connection.prepareStatement(query);
            s.setString(1, regexpExpression);
            s.execute();
            isCopy = true;

        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            cp.closeConnection(connection, s);
        }

        return isCopy;
    }


    public void truncateOrCreateDocumentCollection(String collectionName) {
        if (isTableExists(collectionName))
            truncateDocumentCollection(collectionName);
        else
            createDocumentCollectionTable(collectionName);
    }


    public int fullTextSearch(String searchString, String collectionName) {
        int numberOfFound = 0;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            String query = "SELECT COUNT(*) as cnt FROM " + collectionName + " WHERE MATCH (`data`) AGAINST ('\"" + searchString + "\"' IN BOOLEAN MODE )";
            // String query = "SELECT COUNT(*) as cnt FROM " + collectionName + " WHERE MATCH (`data`) AGAINST ('" + searchString + "')";

            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            if (resultSet.next())
                numberOfFound = resultSet.getInt("cnt");

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return numberOfFound;
    }


    public Set<String> fullTextSearch2(List<String> searchStrings, String collectionName, int start, int end) {
        int numberOfFound = 0;
        Set<String> res = new HashSet<String>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            for (int i = start; i < end; i++) {

                String searchString = searchStrings.get(i);
                String query = "SELECT COUNT(*) as cnt FROM " + collectionName +
                        " WHERE MATCH (`data`) AGAINST ('\"" + searchString + "\"' IN BOOLEAN MODE )";

                ps = connection.prepareStatement(query);
                ps.execute();
                resultSet = ps.getResultSet();

                if (resultSet.next()) {
                    numberOfFound = resultSet.getInt("cnt");
                    if (numberOfFound > 0) {
                        log.info("group - count: " + searchString + " : " + numberOfFound);
                        res.add(searchString);
                    }
                }
            }
        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return res;
    }
}
