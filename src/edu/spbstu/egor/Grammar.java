package edu.spbstu.egor;

import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.util.Index;

/**
 * Created by Egor Gorbunov on 29.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class Grammar {
    public static class Const {
        public static final String NOUN_TAG = "NN";
        public static final String COLLOCATION_TAG = "COLLOCATION";
        public static final String NUOUN_PHRASE_TAG = "NP";
    }

    private static final String SERIALIZED_GRAMMAR_FILENAME = "./data/grammar/englishFactored.ser.gz";

   // public final LexicalizedParser lexicalizedParser;
    public LexicalizedParser lexicalizedParser;

    public ParserData pd;
    public Options op;
    public BinaryGrammar bg;
    public UnaryGrammar ug;
    public BaseLexicon lex;


    public DependencyGrammar dg;

    public Index<String> stateIndex;
    public Index<String> wordIndex;
    public Index<String> tagIndex;

    public Grammar() {
        lexicalizedParser = new LexicalizedParser(SERIALIZED_GRAMMAR_FILENAME);

        pd = lexicalizedParser.getPD();
        bg = pd.bg;
        ug = pd.ug;

        lex = (BaseLexicon) pd.lex;
        wordIndex = pd.wordIndex;
        tagIndex = pd.tagIndex;
        stateIndex = pd.stateIndex;
        dg = pd.dg;
        dg.setLexicon(lex);
        op = lexicalizedParser.getOp();
    }

    /**
     * Irina Daniel
     * @param lexicalizedParser
     */
    public Grammar(LexicalizedParser lexicalizedParser){
        this.lexicalizedParser = lexicalizedParser;
        pd = lexicalizedParser.getPD();
        bg = pd.bg;
        ug = pd.ug;

        lex = (BaseLexicon) pd.lex;
        wordIndex = pd.wordIndex;
        tagIndex = pd.tagIndex;
        stateIndex = pd.stateIndex;
        dg = pd.dg;
        System.out.println("dg:" + dg);
        dg.setLexicon(lex);
        op = lexicalizedParser.getOp();
    }


    /**
     *  WARNING: wordIndex and tagIndex must be shared by all BaseLexicon and current Grammar class
     *
     *  That method extends Grammar by one word with given tag and given count.
     *  It works really simple: given (word, tag) pair is added to grammar as if
     *  during train phase that word occurred only with that tag {@code cnt} times!
     *
     *  So as a result in grammar next things will be changed:
     *  <ul>
     *      <li>Total count of seen words will be increased by {@code cnt}</li>
     *      <li>Total count of seen words, which occurred with tag {@code tag} will be increased by {@code cnt}</li>
     *      <li>New word {@code word} will be added to word index</li>
     *      <li>Number of times given word occurred at all will be set to {@code cnt}</li>
     *      <li>Number of times given word occurred with given tag will be set to {@code cnt}</li>
     *  </ul>
     */
    public void addWord(String word, String tag, int cnt) {
        addWordNoRefresh(word, tag, cnt);
        int wid = wordIndex.indexOf(word);
        refresh();
        assert lex.isKnown(word);
        assert lex.isKnown(wid);
    }

    /**
     * Same as {@code addWord} method, but no refresh is made after adding word to index.
     * Use that method if you want to add a lot of words to grammar, but don't forget to
     * class {@code Grammar.refresh()} method for grammar to work correctly with newly added
     * words
     */
    public void addWordNoRefresh(String word, String tag, int cnt) {
        if (!tagIndex.contains(tag))
            throw new IllegalArgumentException("Tag index must contain given tag!");
        if (wordIndex.contains(word)){
            throw new IllegalArgumentException("Word already is in word index!");
        }


        wordIndex.add(word);

        int wid = wordIndex.indexOf(word);
        int tid = tagIndex.indexOf(tag);


        IntTaggedWord itw = new IntTaggedWord(wid, tid);
        lex.seenCounter.incrementCount(itw, cnt);
        itw = new IntTaggedWord(wid, IntTaggedWord.ANY_TAG_INT);
        lex.seenCounter.incrementCount(itw, cnt);

        assert lex.seenCounter.getCount(itw) == cnt;

        itw = new IntTaggedWord(IntTaggedWord.ANY_WORD_INT, tid);
        lex.seenCounter.incrementCount(itw, cnt);

    }

    /**
     * Adds given tag to state index and tag index
     * @param tag
     */
    public void addNewTag(String tag) {
        tagIndex.add(tag);
        stateIndex.add(tag);

    }

    /**
     * Adds given unary rule to grammar
     *
     * ALERT 1: if any state (parent or child) not added to state index and tag index (i.e. method {@code addNewTag} not
     * called) method will throw {@code IllegalArgumentException}
     *
     * ALERT 2: {@code UnaryGrammar} instance, which is provided by parser data must be shared between every object,
     * which have a link to it.
     *
     * ALERT 3: {@code refresh()} method of cur. class MUST be invoked after.
     */
    public void addNewUnaryRule(String parent, String child, float score) {
        if (!stateIndex.contains(parent) || !stateIndex.contains(child))
            throw new IllegalArgumentException();

        UnaryRule ur = new UnaryRule(stateIndex.indexOf(parent), stateIndex.indexOf(child), score);

        ug.addCoreRule(ur);
    }

    /**
     * That method must be used if you made any changes in grammar with any of next methods:
     * <ul>
     *     <li>{@code addWordNoRefresh(String word, String tag, int cnt)}</li>
     * </ul>
     */
    public void refresh() {
        lex.initRulesWithWord();

        ug.refresh();
        bg.refresh();
    }
}
