package sesa.common.builder;

import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.document.CategorizedDocument;

import java.util.Set;

public class CategorizedBuilder extends BasicBuilder {

  private Set<String> categoryDictionary;

  public CategorizedBuilder(String text, String id, Set<String> categoryDictionary) throws BuilderException {
    super(text, id);
    this.categoryDictionary = categoryDictionary;
  }

  @Override
  public IDocument buildDocument() {
    return new CategorizedDocument(id, title,  model, categoryDictionary);
  }
}
