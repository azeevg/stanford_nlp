package sesa.tools.semantic.linkBuilder;

/**
 * Created by Ira on 07.06.2016.
 */

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchedCollocation;
import edu.spbstu.appmath.collocmatch.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.utility.FileReaderWriter;
import sesa.tools.semantic.AutoReference;
import sesa.common.database.entity.SemanticGroupEntity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * If term definition contains another terms => it's new link
 * We find newLink only for unique term which does not contain links like See ...
 * Separate found terms and collocations in definition
 */
public class AutoGenerateReference {

    public final Logger log = LogManager.getLogger(AutoGenerateReference.class);
    private final String COLLOCATION_SPECIAL_WORD = "_COLLOCATION_";
    private CollocationDictionary collocationDictionary;
    private Map<String, Integer> allTerms;
    private TermDao termDao = new TermDao();
    private TermDefinitionDao termDefinitionDao = new TermDefinitionDao();
    private LinkStructureDao linkStructureDao = new LinkStructureDao();
    private List<String> words = new ArrayList<String>();
    private String collectionName = "auto_comp_reference";


    public AutoGenerateReference() throws IOException, SQLException, ClassNotFoundException {
        collocationDictionary = new CollocationDictionary(new String[]{"./data/collocations/allCollocations.txt"});
        allTerms = termDao.getAllTerms();
        initWords();
    }


    public void run(List<Integer> listId) {
        for (int id : listId) {
            AutoReference dto = findLinksInDefinition(id);
            saveToDataBase(dto);
        }
    }

    public void saveToDataBase(AutoReference dto) {
        SemanticGroupEntity semanticGroupEntity = convert(dto);
        if (semanticGroupEntity != null) {
            System.out.println(semanticGroupEntity.toString());
            linkStructureDao.addToLinkStructure(semanticGroupEntity, collectionName);
        }
    }

    public AutoReference findLinksInDefinition(int idTerm) {
        log.info("find links for term:" + idTerm);
        TermDefinitionEntity entity = termDefinitionDao.getTermDefinitionByTermID(idTerm).get(0);
        String def = entity.getDefinition();
        log.info("begin: " + def);
        AutoReference dto = new AutoReference();
        dto.setIdTerm(idTerm);
        dto.setTermDefinition(def);
        dto.setIdDefinition(entity.getIdDefinition());
        replaceCollocationInText(dto);
        log.info("replaceCollocation: " + dto.getTermDefinition());
        replaceTermInText(dto);
        log.info("replaceTerm: " + dto.getTermDefinition());
        // System.out.println(dto.toString());
        return dto;

    }

    public AutoReference findLinksInDefinition(String term) {
        int idTerm = termDao.getTermByName(term).getId();
        log.info("find links for term with id:" + idTerm);
        TermDefinitionEntity entity = termDefinitionDao.getTermDefinitionByTermID(idTerm).get(0);
        String def = entity.getDefinition();
        log.info("begin: " + def);
        AutoReference dto = new AutoReference();
        dto.setIdTerm(idTerm);
        dto.setTermDefinition(def);
        dto.setIdDefinition(entity.getIdDefinition());
        replaceCollocationInText(dto);
        log.info("replaceCollocation: " + dto.getTermDefinition());
        replaceTermInText(dto);
        log.info("replaceTerm: " + dto.getTermDefinition());
        System.out.println(dto.toString());
        return dto;

    }

    private void replaceTermInText(AutoReference dto) {
        List<String> linkTerms = new ArrayList<String>();
        for (String term : words) {
            for (String d : dto.getTermDefinition().split(",| ")) {
                if (d.length() == 0) continue;
                if (d.equals("1")) continue;
                if (d.equals(term)) {
                    linkTerms.add(term);
                }
            }
        }

        dto.setTermLink(linkTerms);
    }

    private void replaceCollocationInText(AutoReference dto) {

        List<String> linkTerms = new ArrayList<String>();
        Match curMatch = null;
        String resultStr = null;

        try {
            curMatch = collocationDictionary.searchAllCollocations(dto.getTermDefinition());
        } catch (Exception e) {
            FileReaderWriter.write("errorAutoRef.txt", String.valueOf(dto.getIdTerm()));
            return;
            // e.printStackTrace();
        }
        if (curMatch == null) {
            return;
        }
        for (MatchedCollocation m : curMatch.getMatchedCollocations()) {
            linkTerms.add(m.getCollocationStr());
        }
        Sentence sentence = curMatch.replaceCollocations(COLLOCATION_SPECIAL_WORD);
        dto.setTermDefinition(sentence.toString());
        dto.setTermLink(linkTerms);

    }

    // list of terms contain one word
    public void initWords() {
        for (String w : allTerms.keySet()) {
            w = w.trim();
            if (w.split(" ").length == 1) {
                words.add(w);
            }
        }

    }

    // TODO: send to new class
    public SemanticGroupEntity convert(AutoReference dto) {
        List<Integer> idLinks = new ArrayList<Integer>();
        SemanticGroupEntity semDto = new SemanticGroupEntity();
        semDto.setIdTerm(dto.getIdTerm());
        if (dto.getTermLink().size() == 0) {
            return null;
        }
        System.out.println(dto.toString());
        for (String termLink : dto.getTermLink()) {
            int id = 0;
            if(allTerms.get(termLink) == null){
                id = termDao.getTermByName(termLink).getId();
            }else{
                id = allTerms.get(termLink);
            }
            idLinks.add(id);
            //idLinks.add(allTerms.get(termLink));

        }
//        if (idLinks.size() == 0) {
//            FileReaderWriter.write("idNullLinks.txt", String.valueOf(dto.getIdTerm()));
//        }
        semDto.setTermLink(idLinks);
        semDto.setSentenceId(dto.getIdDefinition());
        return semDto;

    }

}
