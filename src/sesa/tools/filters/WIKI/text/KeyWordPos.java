package sesa.tools.filters.WIKI.text;

public class KeyWordPos implements Comparable<KeyWordPos> {
    private int startPos;
    private int endPos;
    private boolean notWellFormed = false;
    private KeySymbols symbols;
    private char[] sSymbols;
    private char[] eSymbols;

    public KeyWordPos(int startPos, KeySymbols keySymbols) {
        this.startPos = startPos;
        this.symbols = keySymbols;
    }

    public KeyWordPos(int startPos, int endPos) {
        this.startPos = startPos;
        this.endPos = endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }

    public int getStartPos() {
        return startPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public char[] getStartSyms() {
        return sSymbols;
    }

    public char[] getEndSyms() {
        return eSymbols;
    }

    public KeySymbols getSymbols() {
        return symbols;
    }

    public void setStartSyms(char[] sSyms) {
        this.sSymbols = sSyms;
    }

    public void setEndSyms(char[] eSyms) {
        this.eSymbols = eSyms;
    }

    public void setNotWellFormed() {
        notWellFormed = true;
    }

    public int compareTo(KeyWordPos o) {
        return startPos - o.startPos;
    }

    public boolean isNotWellFormed() {
        return notWellFormed;
    }

    public void printKeyWord(String input) {

        if (sSymbols == null)
            System.out.println("nulled start");
        else if (eSymbols == null)
            System.out.println("nulled end");
        else
            System.out.println(new String(sSymbols) + input.substring(startPos, endPos) + new String(eSymbols));
    }

}
