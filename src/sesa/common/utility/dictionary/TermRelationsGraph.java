package sesa.common.utility.dictionary;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.min;

/**
 * Created by gleb on 02.06.17.
 */

public class TermRelationsGraph {
    private final Map<Integer, Node> nodes;

    public TermRelationsGraph(List<Integer> ids, Map<Integer, Map<String, Integer>> relations) {
        this.nodes = new HashMap<>();

        if (relations.isEmpty()) {
            return;
        }

        System.out.println("Nodes number: " + ids.size());
        System.out.println("Relations number: " + relations.size());

        ids.forEach(id -> nodes.put(id, new Node(id)));
        relations.forEach((id, relatedTerms) -> nodes.get(id).initRelatedNodes(nodes, relations.get(id)));
    }

    public int getDistance(int id1, int id2) {
        if (id1 == id2)
            return 0;

        return nodes.get(id1).getDistanceTo(nodes.get(id2));
    }

    public List<Integer> getNodesIds() {
        List<Integer> nodesIds = new ArrayList<>(nodes.keySet());
        nodesIds.sort(Integer::compareTo);
        return nodesIds;
    }

    public int[][] getAllDistances() {
        List<Integer> ids = getNodesIds();
        int[][] distances = new int[ids.size()][ids.size()];

        for (int i = 0; i < distances.length; i++) {
            for (int j = 0; j < distances.length; j++) {
                distances[i][j] = 1_000_000;
            }
        }

        for (int i = 0; i < ids.size(); i++) {
            Node node = nodes.get(ids.get(i));
            distances[i][i] = 0;
            int finalI = i;
            node.getRelatedNodes().forEach(relatedNode -> {
                distances[finalI][ids.indexOf(relatedNode.getTermId())] = 1;
            });
        }

        for (int k = 0; k < ids.size(); k++) {
            if (k % 1000 == 0)
                System.out.println(k);
            for (int i = 0; i < ids.size(); i++) {
                for (int j = 0; j < ids.size(); j++) {
                    distances[i][j] = min(distances[i][j], distances[i][k] + distances[k][j]);
                }
            }
        }

        return distances;
    }

    class Node {
        private final Integer termId;
        private final Set<Node> relatedNodes;
        private final Set<String> undefinedTerms;

        Node(Integer termId) {
            this.termId = termId;
            this.relatedNodes = new HashSet<>();
            this.undefinedTerms = new HashSet<>();
        }

        public void initRelatedNodes(Map<Integer, Node> nodes, Map<String, Integer> relations) {
            relations.forEach((term, id) -> {
                if (id.equals(0))
                    undefinedTerms.add(term);
                else if (!termId.equals(id)) {
                    relatedNodes.add(nodes.get(id));
                }
            });
        }

        public Integer getTermId() {
            return termId;
        }

        public Set<Node> getRelatedNodes() {
            return relatedNodes;
        }

        public Set<String> getUndefinedTerms() {
            return undefinedTerms;
        }

        public boolean hasRelatedNodes() {
            return !relatedNodes.isEmpty();
        }

        public boolean hasUndefinedTerms() {
            return !undefinedTerms.isEmpty();
        }

        public boolean hasRelated(Node node) {
            return relatedNodes.contains(node);
        }

        public int getDistanceTo(Node node) {
            Set<Node> s = new HashSet<>();
            s.addAll(getRelatedNodes());
            s.remove(this);
            Set<Node> banned = new HashSet<>();
            banned.add(this);

            int d = 1;
            int count = s.size();

            while (true) {
                if (s.contains(node))
                    return d;

                d++;
                Set<Node> s1 = new HashSet<>();

                s.forEach(o -> s1.addAll(o.getRelatedNodes()));
                s.removeAll(banned);
                banned.addAll(s);
                s = s1;

                if (count == s.size())
                    return -1;

                count = s.size();
            }
        }

        private int getDistanceTo(Node node, int dist) {
            if (relatedNodes.isEmpty())
                return Integer.MAX_VALUE;
            if (relatedNodes.contains(node))
                return dist + 1;
            else
                return relatedNodes.stream()
                        .map(n -> n.getDistanceTo(node, dist))
                        .min(Comparator.comparingInt(Integer::intValue)).get() + 1;
        }
    }
}
