package grammarscope.parser;

import java.util.Collection;

import edu.stanford.nlp.trees.CoordinationTransformer;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.Filter;

public class GrammaticalStructureProxy extends GrammaticalStructure
{
	private static final long serialVersionUID = 1L;

	// actually called
	public GrammaticalStructureProxy(final Tree t, final Collection<GrammaticalRelation> relations, final Filter<String> puncFilter, final HeadFinder hf)
	{
		super(new CoordinationTransformer().transformTree(t), relations, hf, puncFilter);
	}

	// dummy : called by factory in factory base class constructor
	public GrammaticalStructureProxy(final Tree t, final Filter<String> puncFilter, final HeadFinder hf) throws RuntimeException
	{
		super(new CoordinationTransformer().transformTree(t), null, hf, puncFilter);
		throw new RuntimeException("GrammaticalStructureProxy dummy constructor called");
	}
}
