package sesa.common.utility.strings;

import edu.stanford.nlp.ling.Word;
import sesa.common.database.dao.dictionary.TermDao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.*;

public class StringUtility {
    /* HTML characters replacements */
    private static HashMap<String, Integer> htmlEntities = new HashMap<String, Integer>();

    /* Greek alphabet replacements */
    private static HashMap<String, String> greekAlphabet = new HashMap<String, String>();

    /* Normalized interface */

    private static abstract class Normalizer {
        protected String absolutelyDeniedCharacters = "<>\\|=‘/*[]{}~`!@#$%^&*\n\t†±‡¢¡µ¸¶£˻»¿˼·¤˽¼÷˾¥¦¾˺˿´º°½§˹¹²³«ˬˮ©¯®˭ª¨˥˦˧˨˩➤✔¬˪˫◮ ⎣�“✁⎢□■●⎝ℓ⎠×√•➢→ก❉〈〉∗‖Ƒ";
        protected String basicDeniedCharacters = absolutelyDeniedCharacters + "()_+,\"?№;:0123456789.";

        public String normalize(String s) {
            StringBuilder result = new StringBuilder(s.length());

            int pos;
            for (pos = 0; pos < s.length(); pos++)
                if (isValid(s.charAt(pos)))
                    result.append(s.charAt(pos));
            return result.toString();
        }

        protected abstract Boolean isValid(char ch);
    }

    /* Default normalizer */

    private static class DefaultNormalizer extends Normalizer {
        /* Characters, that denied for normalized titles and author keys (for matching) */
        private String deniedCharacters = basicDeniedCharacters + "' -";

        protected Boolean isValid(char ch) {
            return (deniedCharacters.indexOf(ch) == -1);
        }
    }

    /* Title normalizer */

    private static class TitleNormalizer extends Normalizer {
        protected Boolean isValid(char ch) {
            return (absolutelyDeniedCharacters.indexOf(ch) == -1);
        }
    }

    /* Author normalizer */

    private static class AuthorNormalizer extends Normalizer {
        /* Characters, that denied for normalized titles and author keys (for matching) */
        private String deniedCharacters = basicDeniedCharacters;

        protected Boolean isValid(char ch) {
            return (deniedCharacters.indexOf(ch) == -1);
        }
    }

    /* Normalizers */
    private static Normalizer defaultNormalizer;
    private static Normalizer authorNormalizer;
    private static Normalizer titleNormalizer;

    static {
        defaultNormalizer = new DefaultNormalizer();
        authorNormalizer = new AuthorNormalizer();
        titleNormalizer = new TitleNormalizer();

        /* HTML replacements */
        htmlEntities.put("nbsp", 160);
        htmlEntities.put("iexcl", 161);
        htmlEntities.put("cent", 162);
        htmlEntities.put("pound", 163);
        htmlEntities.put("curren", 164);
        htmlEntities.put("yen", 165);
        htmlEntities.put("brvbar", 166);
        htmlEntities.put("sect", 167);
        htmlEntities.put("uml", 168);
        htmlEntities.put("copy", 169);
        htmlEntities.put("ordf", 170);
        htmlEntities.put("laquo", 171);
        htmlEntities.put("not", 172);
        htmlEntities.put("shy", 173);
        htmlEntities.put("reg", 174);
        htmlEntities.put("macr", 175);
        htmlEntities.put("deg", 176);
        htmlEntities.put("plusmn", 177);
        htmlEntities.put("sup2", 178);
        htmlEntities.put("sup3", 179);
        htmlEntities.put("acute", 180);
        htmlEntities.put("micro", 181);
        htmlEntities.put("para", 182);
        htmlEntities.put("middot", 183);
        htmlEntities.put("cedil", 184);
        htmlEntities.put("sup1", 185);
        htmlEntities.put("ordm", 186);
        htmlEntities.put("raquo", 187);
        htmlEntities.put("frac14", 188);
        htmlEntities.put("frac12", 189);
        htmlEntities.put("frac34", 190);
        htmlEntities.put("iquest", 191);
        htmlEntities.put("agrave", 192);
        htmlEntities.put("aacute", 193);
        htmlEntities.put("acirc", 194);
        htmlEntities.put("atilde", 195);
        htmlEntities.put("auml", 196);
        htmlEntities.put("aring", 197);
        htmlEntities.put("aelig", 198);
        htmlEntities.put("ccedil", 199);
        htmlEntities.put("egrave", 200);
        htmlEntities.put("eacute", 201);
        htmlEntities.put("ecirc", 202);
        htmlEntities.put("euml", 203);
        htmlEntities.put("igrave", 204);
        htmlEntities.put("iacute", 205);
        htmlEntities.put("icirc", 206);
        htmlEntities.put("iuml", 207);
        htmlEntities.put("eth", 208);
        htmlEntities.put("ntilde", 209);
        htmlEntities.put("ograve", 210);
        htmlEntities.put("oacute", 211);
        htmlEntities.put("ocirc", 212);
        htmlEntities.put("otilde", 213);
        htmlEntities.put("ouml", 214);
        htmlEntities.put("times", 215);
        htmlEntities.put("oslash", 216);
        htmlEntities.put("ugrave", 217);
        htmlEntities.put("uacute", 218);
        htmlEntities.put("ucirc", 219);
        htmlEntities.put("uuml", 220);
        htmlEntities.put("yacute", 221);
        htmlEntities.put("thorn", 222);
        htmlEntities.put("szlig", 223);
        htmlEntities.put("agrave", 224);
        htmlEntities.put("aacute", 225);
        htmlEntities.put("acirc", 226);
        htmlEntities.put("atilde", 227);
        htmlEntities.put("auml", 228);
        htmlEntities.put("aring", 229);
        htmlEntities.put("aelig", 230);
        htmlEntities.put("ccedil", 231);
        htmlEntities.put("egrave", 232);
        htmlEntities.put("eacute", 233);
        htmlEntities.put("ecirc", 234);
        htmlEntities.put("euml", 235);
        htmlEntities.put("igrave", 236);
        htmlEntities.put("iacute", 237);
        htmlEntities.put("icirc", 238);
        htmlEntities.put("iuml", 239);
        htmlEntities.put("eth", 240);
        htmlEntities.put("ntilde", 241);
        htmlEntities.put("ograve", 242);
        htmlEntities.put("oacute", 243);
        htmlEntities.put("ocirc", 244);
        htmlEntities.put("otilde", 245);
        htmlEntities.put("ouml", 246);
        htmlEntities.put("divide", 247);
        htmlEntities.put("oslash", 248);
        htmlEntities.put("ugrave", 249);
        htmlEntities.put("uacute", 250);
        htmlEntities.put("ucirc", 251);
        htmlEntities.put("uuml", 252);
        htmlEntities.put("yacute", 253);
        htmlEntities.put("thorn", 254);
        htmlEntities.put("yuml", 255);
        htmlEntities.put("fnof", 402);
        htmlEntities.put("alpha", 945);
        htmlEntities.put("beta", 946);
        htmlEntities.put("gamma", 947);
        htmlEntities.put("delta", 948);
        htmlEntities.put("epsilon", 949);
        htmlEntities.put("zeta", 950);
        htmlEntities.put("eta", 951);
        htmlEntities.put("theta", 952);
        htmlEntities.put("iota", 953);
        htmlEntities.put("kappa", 954);
        htmlEntities.put("lambda", 955);
        htmlEntities.put("mu", 956);
        htmlEntities.put("nu", 957);
        htmlEntities.put("xi", 958);
        htmlEntities.put("omicron", 959);
        htmlEntities.put("pi", 960);
        htmlEntities.put("rho", 961);
        htmlEntities.put("sigmaf", 962);
        htmlEntities.put("sigma", 963);
        htmlEntities.put("tau", 964);
        htmlEntities.put("upsilon", 965);
        htmlEntities.put("phi", 966);
        htmlEntities.put("chi", 967);
        htmlEntities.put("psi", 968);
        htmlEntities.put("omega", 969);
        htmlEntities.put("thetasym", 977);
        htmlEntities.put("upsih", 978);
        htmlEntities.put("piv", 982);
        htmlEntities.put("bull", 8226);
        htmlEntities.put("hellip", 8230);
        htmlEntities.put("prime", 8242);
        htmlEntities.put("Prime", 8243);
        htmlEntities.put("oline", 8254);
        htmlEntities.put("frasl", 8260);
        htmlEntities.put("weierp", 8472);
        htmlEntities.put("image", 8465);
        htmlEntities.put("real", 8476);
        htmlEntities.put("trade", 8482);
        htmlEntities.put("alefsym", 8501);
        htmlEntities.put("larr", 8592);
        htmlEntities.put("uarr", 8593);
        htmlEntities.put("rarr", 8594);
        htmlEntities.put("darr", 8595);
        htmlEntities.put("harr", 8596);
        htmlEntities.put("crarr", 8629);
        htmlEntities.put("larr", 8656);
        htmlEntities.put("uarr", 8657);
        htmlEntities.put("rarr", 8658);
        htmlEntities.put("darr", 8659);
        htmlEntities.put("harr", 8660);
        htmlEntities.put("forall", 8704);
        htmlEntities.put("part", 8706);
        htmlEntities.put("exist", 8707);
        htmlEntities.put("empty", 8709);
        htmlEntities.put("nabla", 8711);
        htmlEntities.put("isin", 8712);
        htmlEntities.put("notin", 8713);
        htmlEntities.put("ni", 8715);
        htmlEntities.put("prod", 8719);
        htmlEntities.put("sum", 8721);
        htmlEntities.put("minus", 8722);
        htmlEntities.put("lowast", 8727);
        htmlEntities.put("radic", 8730);
        htmlEntities.put("prop", 8733);
        htmlEntities.put("infin", 8734);
        htmlEntities.put("ang", 8736);
        htmlEntities.put("and", 8743);
        htmlEntities.put("or", 8744);
        htmlEntities.put("cap", 8745);
        htmlEntities.put("cup", 8746);
        htmlEntities.put("int", 8747);
        htmlEntities.put("there4", 8756);
        htmlEntities.put("sim", 8764);
        htmlEntities.put("cong", 8773);
        htmlEntities.put("asymp", 8776);
        htmlEntities.put("ne", 8800);
        htmlEntities.put("equiv", 8801);
        htmlEntities.put("le", 8804);
        htmlEntities.put("ge", 8805);
        htmlEntities.put("sub", 8834);
        htmlEntities.put("sup", 8835);
        htmlEntities.put("nsub", 8836);
        htmlEntities.put("sube", 8838);
        htmlEntities.put("supe", 8839);
        htmlEntities.put("oplus", 8853);
        htmlEntities.put("otimes", 8855);
        htmlEntities.put("perp", 8869);
        htmlEntities.put("sdot", 8901);
        htmlEntities.put("lceil", 8968);
        htmlEntities.put("rceil", 8969);
        htmlEntities.put("lfloor", 8970);
        htmlEntities.put("rfloor", 8971);
        htmlEntities.put("lang", 9001);
        htmlEntities.put("rang", 9002);
        htmlEntities.put("loz", 9674);
        htmlEntities.put("spades", 9824);
        htmlEntities.put("clubs", 9827);
        htmlEntities.put("hearts", 9829);
        htmlEntities.put("diams", 9830);
        htmlEntities.put("quot", 34);
        htmlEntities.put("amp", 38);
        htmlEntities.put("lt", 60);
        htmlEntities.put("gt", 62);
        htmlEntities.put("OElig", 338);
        htmlEntities.put("oelig", 339);
        htmlEntities.put("scaron", 352);
        htmlEntities.put("yuml", 376);
        htmlEntities.put("circ", 710);
        htmlEntities.put("tilde", 732);
        htmlEntities.put("ensp", 8194);
        htmlEntities.put("emsp", 8195);
        htmlEntities.put("thinsp", 8201);
        htmlEntities.put("zwnj", 8204);
        htmlEntities.put("zwj", 8205);
        htmlEntities.put("lrm", 8206);
        htmlEntities.put("rlm", 8207);
        htmlEntities.put("ndash", 8211);
        htmlEntities.put("mdash", 8212);
        htmlEntities.put("lsquo", 8216);
        htmlEntities.put("rsquo", 8217);
        htmlEntities.put("sbquo", 8218);
        htmlEntities.put("ldquo", 8220);
        htmlEntities.put("rdquo", 8221);
        htmlEntities.put("bdquo", 8222);
        htmlEntities.put("dagger", 8224);
        htmlEntities.put("Dagger", 8225);
        htmlEntities.put("permil", 8240);
        htmlEntities.put("lsaquo", 8249);
        htmlEntities.put("rsaquo", 8250);
        htmlEntities.put("euro", 8364);

        /* Greek alphabet */
        greekAlphabet.put("α", "alpha");
        greekAlphabet.put("β", "beta");
        greekAlphabet.put("γ", "gamma");
        greekAlphabet.put("δ", "delta");
        greekAlphabet.put("ε", "epsilon");
        greekAlphabet.put("ζ", "zeta");
        greekAlphabet.put("η", "eta");
        greekAlphabet.put("θ", "theta");
        greekAlphabet.put("ι", "iota");
        greekAlphabet.put("κ", "kappa");
        greekAlphabet.put("λ", "lambda");
        greekAlphabet.put("μ", "mu");
        greekAlphabet.put("ν", "nu");
        greekAlphabet.put("ξ", "xi");
        greekAlphabet.put("ο", "omicron");
        greekAlphabet.put("π", "pi");
        greekAlphabet.put("ρ", "rho");
        greekAlphabet.put("σ", "sigma");
        greekAlphabet.put("τ", "tau");
        greekAlphabet.put("υ", "upsilon");
        greekAlphabet.put("φ", "phi");
        greekAlphabet.put("χ", "chi");
        greekAlphabet.put("ψ", "psi");
        greekAlphabet.put("ω", "omega");
        greekAlphabet.put("Γ", "gamma");
        greekAlphabet.put("Δ", "delta");
        greekAlphabet.put("Η", "eta");
        greekAlphabet.put("Θ", "theta");
        greekAlphabet.put("Λ", "iota");
        greekAlphabet.put("Ξ", "xi");
        greekAlphabet.put("Π", "pi");
        greekAlphabet.put("Σ", "sigma");
        greekAlphabet.put("Φ", "phi");
        greekAlphabet.put("Ψ", "psi");
        greekAlphabet.put("Ω", "omega");
    }

    /**
     * Turn any HTML escape entities in the string into
     * characters and return the resulting string.
     *
     * @param s String to be unescaped.
     * @return unescaped String.
     * @throws NullPointerException if s is null.
     * @since ostermillerutils 1.00.00
     */
    private static String unescapeHTML(String s) {
        StringBuilder result = new StringBuilder(s.length());
        int ampInd = s.indexOf("&");
        int lastEnd = 0;
        while (ampInd >= 0) {
            int nextAmp = s.indexOf("&", ampInd + 1);
            int nextSemi = s.indexOf(";", ampInd + 1);
            if (nextSemi != -1 && (nextAmp == -1 || nextSemi < nextAmp)) {
                int value = -1;
                String escape = s.substring(ampInd + 1, nextSemi);
                try {
                    if (escape.startsWith("#"))
                        value = Integer.parseInt(escape.substring(1), 10);
                    else if (htmlEntities.containsKey(escape.toLowerCase()))
                        value = htmlEntities.get(escape.toLowerCase());
                } catch (NumberFormatException ignored) {
                }
                result.append(s.substring(lastEnd, ampInd));
                lastEnd = nextSemi + 1;
                if (value >= 0 && value <= 0xffff)
                    result.append((char) value);
            }
            ampInd = nextAmp;
        }
        result.append(s.substring(lastEnd));
        return result.toString();
    }

    private static String unescapeUnicode(String s) {
        try {
            byte[] utf8 = s.getBytes("UTF-8");
            return new String(utf8, "UTF-8");
        } catch (UnsupportedEncodingException ignored) {
        }

        return s;
    }

    private static String unescapeGreek(String s) {
        String escapedString = "";
        int pos;
        for (pos = 0; pos < s.length(); pos++) {
            String ch = s.substring(pos, pos + 1);
            if (greekAlphabet.containsKey(ch))
                escapedString += greekAlphabet.get(ch);
            else
                escapedString += s.charAt(pos);
        }

        return escapedString;
    }

    public static String getHash(String s) {
        return (s == null) ? "" : defaultNormalizer.normalize(unescapeUnicode(unescapeGreek(unescapeHTML(s.toLowerCase())))).toLowerCase();
    }

    public static String normalizeString(String s) {
        return (s == null) ? "" : titleNormalizer.normalize(unescapeUnicode(unescapeHTML(s.replaceAll("`", "'"))));
    }

    public static String normalizeAuthorName(String s) {
        return (s == null) ? "" : authorNormalizer.normalize(unescapeUnicode(unescapeHTML(s)));
    }

    public static String escapeString(String s) {
        if (s == null)
            return "";
        String retvalue = s;
        if (s.indexOf("'") != -1) {
            StringBuffer hold = new StringBuffer();
            char c;

            for (int i = 0; i < s.length(); i++) {
                if ((c = s.charAt(i)) == '\'')
                    hold.append("''");
                else
                    hold.append(c);
            }

            retvalue = hold.toString();
        }

        return retvalue;
    }

    public static String join(Iterable<? extends Object> pColl, String separator) {
        Iterator<? extends Object> oIter;
        if (pColl == null || (!(oIter = pColl.iterator()).hasNext()))
            return "";
        StringBuilder oBuilder = new StringBuilder(String.valueOf(oIter.next()));
        while (oIter.hasNext())
            oBuilder.append(separator).append(oIter.next());
        return oBuilder.toString();
    }


    public static List<String> getListStringStartFromPoint(Set<String> allTerms) {
        List<String> terms = new ArrayList<String>();
        for (String s : allTerms) {
            if (s.trim().indexOf(".") == 0) {

                terms.add(s);
            }
        }
        return terms;
    }

    public static List<String> getListStringStartFromPoint() {
        try {
            List<String> list = new ArrayList<String>();
            Scanner in = new Scanner(new File("data/dictionary/utility/startPointTerm.txt"));
            while (in.hasNextLine()) {
                String str = in.nextLine();
                list.add(str);
            }
            return list;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String replacePoint(String text) throws SQLException, ClassNotFoundException {
        // System.out.println("replacePoint:" + text);
        TermDao dao = new TermDao();
        Map<String, Integer> map = dao.getAllTerms();
        // List<String> terms = StringUtility.getListStringStartFromPoint(map.keySet()); // get terms from db
        List<String> terms = StringUtility.getListStringStartFromPoint(); // get terms from file

        for (String term : terms) {
            if (text.contains(term)) {
                StringBuilder sb = new StringBuilder();
                sb.append("#");
                sb.append(term, 1, term.length());
                // res.add(sb.toString());
                text = text.replace(term, sb.toString());

            }
        }
        //   System.out.println("replace:" + text);
        return text;
    }

    public static List<String> convertSetToList(Set<String> terms) {
        List<String> titles_terms = new ArrayList<String>();
        for (String term : terms) {
            titles_terms.add(term);
        }
        return titles_terms;
    }


    public static String updateStringInBracket(String text) {

        text = text.trim();
        StringBuilder sb = new StringBuilder();
        if (text.indexOf("(") == 0) {
            int end = text.indexOf(")");
            for (int i = 0; i < end + 1; i++) {
                sb.append(text.charAt(i));
            }
            if (sb.length() < 40) {
                if (sb.toString().contains(".")) {
                    String s = sb.toString().replaceAll("\\.", "");
                    String result = s + "." + text.replace(sb.toString(), "");
                    return result;
                }
            }

        }
        return text;
    }

    public static List<Word> updateWordList(List<Word> inputWords) {
        List<Word> words = new ArrayList<Word>();
        // System.out.println("updateWordList:" + inputWords);

        if (inputWords.size() > 2) {
            String w1 = inputWords.get(inputWords.size() - 1).word();
            String w2 = inputWords.get(inputWords.size() - 2).word();

            if ((w1.equals(".")) && w2.equals(".")) {
                inputWords.remove(inputWords.size() - 1);
            }
        }


        for (Word word : inputWords) {
            if (word.toString().indexOf("#") == 0 && word.toString().length() > 1) {
                StringBuilder sb = new StringBuilder(".");
                sb.append(word.word(), 1, word.word().length());
                words.add(new Word(sb.toString()));
            } else {
                words.add(word);
            }
        }

        return words;
    }

    public static String emptyNChars(int i) {
        String s = "";
        for (int j = 0; j < i; j++) {
            s += " ";
        }
        return s;
    }
}