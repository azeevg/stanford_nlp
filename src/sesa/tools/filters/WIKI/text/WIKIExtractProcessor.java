package sesa.tools.filters.WIKI.text;

import sesa.common.database.entity.IDBEntity;
import java.util.*;

public class WIKIExtractProcessor {
    private IDBEntity doc;
    private Map<String, Object> docMap;
    private List<KeySymbols> allKeySymbols;
    private String title;
    private String input;

    public WIKIExtractProcessor(IDBEntity doc) {
        this.doc = doc;
        this.docMap = doc.toMap();
        this.title = (String) docMap.get("title");
        String text = (String) docMap.get("text");
        text = cleanWikiBulletLists(text);
        text = cleanWikiNumberedLists(text);
        this.input = wrapText(text);
    }

    public String wrapText(String input) {
        return "<#wrap>" + input + "</#wrap>";
    }

    public IDBEntity processDocument() throws WIKIExtractImportException {
        SortedSet<KeyWordPos> keyWordSet = findPageWordPositions();
        WIKIExtractor extractor = new WIKIExtractor(input, title);
        HashMap<String, Object> extracts = extractor.extract(keyWordSet);
        docMap.putAll(extracts);
        return doc;
    }

    private SortedSet<KeyWordPos> findPageWordPositions() throws WIKIExtractImportException {
        allKeySymbols = RuleInitializer.initializeKeySymbols();
        RuleInitializer.hashCache = new HashMap<String, LinkedList<Integer>>();
        SortedSet<KeyWordPos> keyWordSet = new TreeSet<KeyWordPos>();
        Stack<KeyWordPos> keyWordStack = new Stack<KeyWordPos>();

        char[] chs = input.toCharArray();
        for (int pos = 0; pos < chs.length; pos++) {
            char ch = chs[pos];
            try {
                for (KeySymbols keySymbols : allKeySymbols) {
                    if (keySymbols.isKeySymbol(ch)) {
                        KeyEntry entry = keySymbols.getKeyEntry(chs, pos);
                        if (entry.getType() != KeyType.NONE_TYPE) {

                            if (entry.getAmbiguityEntry() != null) {
                                KeyEntry resolvedEntry;
                                if ((resolvedEntry = resolveAmbiguity(entry, keyWordStack)) != null) {
                                    entry = resolvedEntry;
                                    keySymbols = resolvedEntry.getKeySymbols();
                                    entry.setType(KeyType.END_TYPE);
                                } else {
                                    entry.setType(KeyType.START_TYPE);
                                }
                            }

                            if (keySymbols.isParseInsideSkipped() && entry.getType() == KeyType.START_TYPE) {
                                KeyWordPos skipKeyWord = new KeyWordPos(pos + entry.getSymbols().length, keySymbols);
                                skipKeyWord.setStartSyms(entry.getSymbols());
                                KeyEntry endEntry = keySymbols.findSkipUntilPosition(chs, pos + entry.getSymbols().length);
                                int skipUntilPos = endEntry.getPosition() + endEntry.getSymbols().length - 1;
                                skipKeyWord.setEndPos(endEntry.getPosition() - 1);
                                skipKeyWord.setEndSyms(endEntry.getSymbols());
                                keyWordSet.add(skipKeyWord);
                                pos = skipUntilPos - 1;

                            } else {

                                int offset = entry.getSymbols().length - 1;
                                int startKeyWordPos = pos + offset + 1;

                                switch (entry.getType()) {
                                    case START_TYPE:
                                        KeyWordPos kwp = new KeyWordPos(startKeyWordPos, keySymbols);
                                        kwp.setStartSyms(entry.getSymbols());
                                        keyWordStack.push(kwp);
                                        break;

                                    case END_TYPE:
                                        processEndTag(keyWordStack, keyWordSet, keySymbols, pos, entry.getSymbols());
                                        break;

                                    case BOTH_TYPE:
                                        if (startSymbolsOnStackPosition(keyWordStack, keySymbols, entry.getSymbols()) == -1) {
                                            KeyWordPos bothKeywordPosition = new KeyWordPos(startKeyWordPos, keySymbols);
                                            bothKeywordPosition.setStartSyms(entry.getSymbols());
                                            keyWordStack.push(bothKeywordPosition);
                                        } else {
                                            processEndTag(keyWordStack, keyWordSet, keySymbols, pos, entry.getSymbols());
                                        }
                                        break;
                                }
                                pos += offset;
                            }
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                throw new WIKIExtractImportException(e.getMessage(), title);
            }
        }

        if (keyWordStack.size() > 0)
            throw new WIKIExtractImportException("Stack > 0", title);

        return keyWordSet;
    }

    private KeyEntry resolveAmbiguity(KeyEntry entry, Stack<KeyWordPos> keyWordStack) {

        KeyEntry[] variants = new KeyEntry[]{entry, entry.getAmbiguityEntry()};
        for (int i = keyWordStack.size() - 1; i >= 0; i--)
            for (KeyEntry variant : variants)
                if (keyWordStack.get(i).getSymbols().getKeyWord().equals(variant.getKeySymbols().getKeyWord()))
                    return variant;

        return null;
    }

    private void processEndTag(Stack<KeyWordPos> keyWordStack, SortedSet<KeyWordPos> keyWordSet, KeySymbols keySymbols, int pos, char[] syms) throws WIKIExtractImportException {
        int startStackPos = findNonWellFormedWordPositions(keyWordStack, keyWordSet, keySymbols, pos, syms);
        if (startStackPos == -1)
            return;

        if (keyWordStack.size() == 0) {
            throw new WIKIExtractImportException("Stack == 0", title);
        }
        KeyWordPos keyWordPos = keyWordStack.pop();
        keyWordPos.setEndSyms(syms);
        keyWordPos.setEndPos(pos);
        keyWordSet.add(keyWordPos);
    }

    private int startSymbolsOnStackPosition(Stack<KeyWordPos> stack, KeySymbols keySymbols, char[] symbols) {

        int stackSize = stack.size();
        LinkedList<Integer> hashes = RuleInitializer.getPossibleHashes(allKeySymbols, symbols);

        if (hashes.size() > 1) {
            for (int i = stackSize - 1; i >= 0; i--) {
                KeySymbols stackSymbols = stack.get(i).getSymbols();
                for (Integer hash : hashes) {
                    if (stackSymbols.getTagHash() == hash)
                        return i;
                }
            }

        } else {
            for (int i = stackSize - 1; i >= 0; i--) {
                KeySymbols stackSymbols = stack.get(i).getSymbols();
                if (stackSymbols.getTagHash() == keySymbols.getTagHash())
                    return i;
                else if (stackSymbols.getPriority() > keySymbols.getPriority())
                    break;
            }
        }
        return -1;
    }

    private int findNonWellFormedWordPositions(Stack<KeyWordPos> stack, SortedSet<KeyWordPos> keyWordSet, KeySymbols keySymbols, int pos, char[] symbols) {
        int startStackPos = startSymbolsOnStackPosition(stack, keySymbols, symbols);

        if (startStackPos == -1)
            return startStackPos;

        int stackSize = stack.size();
        for (int i = 0; i < stackSize - startStackPos - 1; i++) {

            KeyWordPos nwfPos = stack.pop();
            nwfPos.setNotWellFormed();
            nwfPos.setEndSyms(symbols);
            nwfPos.setEndPos(pos);
            keyWordSet.add(nwfPos);
        }
        return startStackPos;
    }

    private String cleanWikiBulletLists(String cleanedText) {
        return cleanedText.replaceAll("\\n\\*[^\\n]+", "");
    }

    private String cleanWikiNumberedLists(String cleanedText) {
        return cleanedText.replaceAll("\\n#[^\\n]+", "");
    }
}
