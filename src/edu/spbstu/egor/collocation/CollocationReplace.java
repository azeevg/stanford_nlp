package edu.spbstu.egor.collocation;

import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchBank;
import edu.spbstu.appmath.collocmatch.MatchedCollocation;
import edu.spbstu.egor.*;
import edu.stanford.nlp.trees.Tree;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Egor Gorbunov on 08.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class CollocationReplace {
    private static final Logger logger = Logger.getLogger(CollocationReplace.class.getSimpleName());

    private static final String COLLOCATION_SPECIAL_WORD = "_COLLOCATION_";
    private static final String MATCH_BANK_FILENAME = "./egor_in/colloc_sentences/sent_clear_1.txt";

    // settings
    private static final boolean DO_FILTER = true;
    private static final boolean DO_READ_BEST_TREES_FROM_FILE = true;
    private static final boolean DO_TREE_DUMP = true;
    private static final boolean DO_PDF_COMPILE = false;
    private static final boolean GET_BAD_COLLOCATION_PARSES = true;
    private static final boolean DO_TREE_COMPARE_AND_PRINT = false;

    private static MatchBank matchBank;
    private static Grammar grammar;
    private static TreeGetter treeGetter;
    private static TreeUtils treeUtils;

    private static void addSpecialCollocationWordToGrammar() {
        logger.info("Adding new special word to grammar: " + COLLOCATION_SPECIAL_WORD);
        grammar.addWord(COLLOCATION_SPECIAL_WORD, Grammar.Const.NOUN_TAG, 200);
    }

    private static void prepare() throws IOException {
        logger.info("Initializing grammar.");
        grammar = new Grammar();
        grammar.op.testOptions.verbose = false;

        matchBank = new MatchBank();

        logger.info("Reading sentences with matched collocations.");
        matchBank.read(new BufferedReader(new FileReader(new File(MATCH_BANK_FILENAME))));
        addSpecialCollocationWordToGrammar();

        ParserFactory parserFactory = new ParserFactory(grammar);
        treeGetter = new TreeGetter();
        treeGetter.setPparser(parserFactory.getMyParser());
        treeUtils = new TreeUtils(grammar.op);
    }

    public static void main(String[] args) throws IOException {
        prepare();

        Match[] matches = matchBank.getAsArray(3);

        logger.info("Sorting by sentence length for better clearness");
        Arrays.sort(matches, Match.BY_SENTENCE_LENGTH);

        Tree[] oldTrees;
        Tree[] newTrees;
        LinkedList<Tree> badCollocationParses = new LinkedList<Tree>();

        if (!DO_READ_BEST_TREES_FROM_FILE) {
            oldTrees = new Tree[matches.length];
            newTrees = new Tree[matches.length];

            logger.info("Starting tree parsing... Total sentence to be parsed: " + matches.length);
            for (int i = 0; i < oldTrees.length; ++i) {
                logger.info("progress: " + (i + 1) + " / " + matches.length);
                logger.info("Parsing sentence: " + matches[i].getSentence().str());
                oldTrees[i] = treeGetter.getBestParseTree(SentenceGetter.convert(matches[i].getSentence()));

                logger.info("Parsing sentence: " + matches[i].replaceCollocations(COLLOCATION_SPECIAL_WORD).str());
                newTrees[i] = treeGetter.getBestParseTree(SentenceGetter.convert(
                        matches[i].replaceCollocations(COLLOCATION_SPECIAL_WORD)
                ));

                Tree transformedOldTree = treeUtils.transformTreeToStandardShape(oldTrees[i]);
                if (!TreeUtils.isGoodCollocationParse(transformedOldTree, matches[i])) {
                    TreeUtils.colorCollocations(matches[i], transformedOldTree);
                    badCollocationParses.add(transformedOldTree);
                }

                replaceCollocationTag(newTrees[i], matches[i]);
            }

            if (DO_TREE_DUMP) {
                logger.info("Dumping trees to files...");
                BufferedWriter treeWriter = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("./egor_out/tree_dump/replace/old_trees.txt"), "UTF-8"));
                TreeUtils.dumpAllTrees(oldTrees, treeWriter);
                treeWriter.flush();
                treeWriter.close();

                treeWriter = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("./egor_out/tree_dump/replace/new_trees.txt"), "UTF-8"));
                TreeUtils.dumpAllTrees(newTrees, treeWriter);
                treeWriter.flush();
                treeWriter.close();
            }
        } else {
            // EGOR_INFO: be ware reading dumped trees, because they must correspond to sentences from MatchBank.
            logger.info("Reading best trees from file...");

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream("./egor_out/tree_dump/replace/old_trees.txt"), "UTF-8"));
            oldTrees = TreeUtils.readAllTrees(reader);

            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream("./egor_out/tree_dump/replace/new_trees.txt"), "UTF-8"));
            newTrees = TreeUtils.readAllTrees(reader);

            for (int i = 0; i < oldTrees.length; ++i) {
                Tree transformedOldTree = treeUtils.transformTreeToStandardShape(oldTrees[i]);
                if (!TreeUtils.isGoodCollocationParse(transformedOldTree, matches[i])) {
                    TreeUtils.colorCollocations(matches[i], transformedOldTree);
                    badCollocationParses.add(transformedOldTree);
                }
            }
        }

        if (DO_TREE_COMPARE_AND_PRINT) {
            logger.info("Transforming and comparing trees...");
            int changesDepths[] = new int[oldTrees.length];
            for (int i = 0; i < oldTrees.length; ++i) {
                oldTrees[i] = treeUtils.transformTreeToStandardShape(oldTrees[i]);
                newTrees[i] = treeUtils.transformTreeToStandardShape(newTrees[i]);

                changesDepths[i] = TreeUtils.compareTrees(oldTrees[i], newTrees[i]);
            }

            if (DO_FILTER) {
                logger.info("Filtering trees by max change depth...");
                List<Tree> finalOldTrees = new ArrayList<Tree>();
                List<Tree> finalNewTrees = new ArrayList<Tree>();

                for (int i = 0; i < changesDepths.length; ++i) {
                    if (changesDepths[i] > 2) {
                        finalOldTrees.add(oldTrees[i]);
                        finalNewTrees.add(newTrees[i]);
                    }
                }

                oldTrees = finalOldTrees.toArray(new Tree[finalOldTrees.size()]);
                newTrees = finalNewTrees.toArray(new Tree[finalNewTrees.size()]);
            }

            if (oldTrees.length == 0) {
                logger.info("No differences in parses =(");
                return;
            }

            logger.info("Writing trees to .tex files.");
            final int MAX_TREES_IN_FILE = 30;
            final String oldTreesFilePrefix = "./egor_out/tex/collocations/replace/old_trees_replace_";
            final String newTreesFilePrefix = "./egor_out/tex/collocations/replace/new_trees_replace_";

            treeUtils.printTreesToTexFiles(oldTrees, oldTreesFilePrefix, MAX_TREES_IN_FILE,
                    "Parse trees. Standard Stanford Parser Algorithm.");
            treeUtils.printTreesToTexFiles(newTrees, newTreesFilePrefix, MAX_TREES_IN_FILE,
                    "Parse trees. Technique: replace every collocation with special word _COLLOCATION_, which is always tagged as NN.");
        }

        if (GET_BAD_COLLOCATION_PARSES) {
            logger.info("Writing bad collocation parses to .tex files.");
            final String badCollocationParsesPrefix = "./egor_out/tex/collocations/replace/bad_parses/bad_parses_";

            treeUtils.printTreesToTexFiles(badCollocationParses, badCollocationParsesPrefix, 30,
                    "Parse trees. Trees, where collocations are not in same subtree.");
            logger.info("NUMBER OF BAD COLLOCATION PARSES = " + badCollocationParses.size() + " / " + oldTrees.length);
        }

        if (DO_PDF_COMPILE) {
            logger.info("Starting pdf compilation...");
            final String PDF_COMPILE_SCRIPT = "egor_out\\tex\\collocations\\replace\\pdfgen.bat";
            final String INPUT_TEX_FILES_FOLDER = "egor_out\\tex\\collocations\\replace";
            final String OUTPUT_PDF_FILES_FILDER = "egor_out\\tex\\collocations\\pdf_trees\\replace";

            Runtime.getRuntime().exec("cmd /c start " + PDF_COMPILE_SCRIPT + " " + INPUT_TEX_FILES_FOLDER
                    + " " + OUTPUT_PDF_FILES_FILDER);
        }

    }

    /**
     * EGOR_INFO: be ware, that method assumes that Tree.getLeaves() method return all leaves in right order (from
     * left to right), so initial sentence can be generated.
     */
    private static void replaceCollocationTag(Tree newTree, Match match) {
        int i = 0;
        MatchedCollocation[] collocations = match.getSortedCollocations();
        for (Tree t : newTree.getLeaves()) {
            if (t.label().value().equals(COLLOCATION_SPECIAL_WORD)) {
                t.label().setValue(collocations[i].getCollocationStr());
                logger.info("Replaced _COLLOCATION_ tag at: " + i + " to value = " + t.label().value());
                ++i;
            }
        }
    }
}
