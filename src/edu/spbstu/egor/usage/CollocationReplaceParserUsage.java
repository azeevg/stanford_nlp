package edu.spbstu.egor.usage;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.appmath.collocmatch.Sentence;
import edu.spbstu.egor.*;
import edu.stanford.nlp.parser.lexparser.ExhaustivePCFGParser;
import edu.stanford.nlp.trees.Tree;

import java.io.IOException;
import java.util.List;

/**
 * Created by Egor Gorbunov on 03.04.2016.
 * email: egor-mailbox@ya.ru
 */
public class CollocationReplaceParserUsage {

    public static void main(String[] args) throws IOException {
        // setting up stuff
        Grammar grammar = new Grammar();

        // parser factory used for easier access to parsers
        ParserFactory parserFactory = new ParserFactory(grammar);

        // reading collocations dictionary
        final String[] collocationDictFiles = { "./egor_in/collocations.txt" };
        CollocationDictionary collocationDictionary = new CollocationDictionary(collocationDictFiles);

        // creating parser
        ExhaustivePCFGParser parser =
                parserFactory.getCollocationReplaceParser(collocationDictionary);

        // creating tree getter (actually is just a wrapper...)
        TreeGetter tg = new TreeGetter();
        tg.setPparser(parser);

        // parsing and printing tree
        Sentence sentence = new Sentence("Ada programming language , algebra stubs .");
        Tree tree = tg.getBestParseTree(SentenceGetter.convert(sentence));

        // Tree Utils for tree printing
        TreeUtils treeUtils = new TreeUtils(grammar.op);

        // printing tree
        treeUtils.preprocessedPennPrint(tree);


    }
}
