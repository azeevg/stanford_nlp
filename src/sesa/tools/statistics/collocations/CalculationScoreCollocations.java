package sesa.tools.statistics.collocations;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.utility.FileReaderWriter;

import java.util.*;

/**
 * Created by Ira on 05.05.2016.
 */
// TODO ���������� � CollocationUtility
public class CalculationScoreCollocations {
    public static final Logger log = LogManager.getLogger(CalculationScoreCollocations.class);

    private List<String> strings;
    private List<String> collocations;


    public CalculationScoreCollocations(List<String> strings, String collocationFile) {
        this.strings = strings;
        initCollocations(collocationFile);
    }



    public HashMap<String,Integer> findScores(){
        HashMap<String,Integer> scoreMap = new HashMap<String,Integer>();
        for(String collocation:collocations){
            log.info("Find score for collocation:" + collocation);
            scoreMap.put(collocation, calcFrequencyForAllDictionary(collocation));
        }
        return scoreMap;
    }


    public int calcFrequencyForAllDictionary(String term){
        int resCount = 0;
        for(String text: strings){
          //  System.out.println(text);
            resCount += frequencyOfOccurrence(text,term);
        }
        return resCount;
    }
    /**
     * ������� ������� ������� � ������
     * @param text
     * @param term
     * @return
     */
    public int frequencyOfOccurrence(String text, String term){
        text = text.toLowerCase();
        term = term.toLowerCase();
        int count = 0;
        int termLength = term.length();
        if(text.contains(term)){
            count++;
            int index = text.indexOf(term);
            while(true){
                index = text.indexOf(term, index + termLength);
                if(index == -1){
                    return count;
                }
                count++;
            }
        }
        return count;
    }

    public int calcNumCollocationInSentence(String sentence) {
        int numCollocationsInSentence = 0;
        for (String collocation : collocations) {
            numCollocationsInSentence += frequencyOfOccurrence(sentence,collocation);
        }
        return numCollocationsInSentence;
    }

    public int calcNumCollocationInText(List<String> text) {
        int res = 0;
        for (String sentence : text) {
            res += calcNumCollocationInSentence(sentence);
        }
        return res;
    }


    public void initCollocations(String collocationFile){
       collocations = FileReaderWriter.read(collocationFile);
    }




}
