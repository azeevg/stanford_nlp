package sesa.common.model.processing.support;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 *   Класс позволяет хранить типизированные ошибки
 *   и комментарии к этим ошибкам
 */
public class RelationMistake {

    public enum RelationMistakeType {
        NotMatched,
        PartiallyMatched
    }

    public enum RelationMistakeSubType {
        Undefined,
        GroupNotMatched,
        SentenceLength,
        DelimiterNeighbor,
        ServiceTokens,
        SubjNeighborMistake,
        ObjNeighborMistake
    }


    private RelationMistakeType type;
    private String info;
    private Map<RelationMistakeSubType, String> subTypes = new HashMap<RelationMistakeSubType, String>();

    public RelationMistakeType getType() {
        return type;
    }

    public void setType(RelationMistakeType type) {
        this.type = type;
    }

    public Map<RelationMistakeSubType, String> getSubTypes() {
        return subTypes;
    }

    public void setSubTypes(HashMap<RelationMistakeSubType, String> subTypes) {
        this.subTypes = subTypes;
    }

    public void addSubType(RelationMistakeSubType subType) {
        subTypes.put(subType, "");
    }

    public void addSubType(RelationMistakeSubType subType, String info) {
        subTypes.put(subType, info);
    }

    public String getInfoString() {
        StringBuilder builder = new StringBuilder();
        builder.append(StringUtils.join(subTypes.values(), "<br/>").replaceAll("\\|", ""));
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(StringUtils.join(subTypes.values()));
        return builder.toString();
    }
}
