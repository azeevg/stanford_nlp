package sesa.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Created by Ira on 29.02.2016.
 */

/**
 * Class for work with table: "counter"
 */
public class CounterDao extends CommonDao {
    public final Logger log = LogManager.getLogger(CounterDao.class);
    public CounterDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public int getCounter(String key) {
        Statement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        int count = -1;
        try {
            connection = cp.takeConnection();
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            resultSet = statement.executeQuery("SELECT * FROM `counters` WHERE name = " + key);

            if (resultSet.next()) {
                count = resultSet.getInt("count");
            }
        } catch (SQLException e) {
            log.error("Can't select counter");
            log.error("Exception: " + e.getMessage());
        } finally {
            cp.closeConnection(connection, statement, resultSet);
        }

        return count;
    }

    /**
     * @param key
     * @param value
     */
    //
    // TODO ERROR - cp.closeConnection()
    public void setCounter(String key, int value) {

        PreparedStatement s = null;
        Connection connection = null;

        try {
            connection = cp.takeConnection();
            String query = "UPDATE `counters` SET `count`=" + value + " WHERE `name`='" + key + "'";
            s = connection.prepareStatement(query);
            s.execute();

        } catch (SQLException e) {
            // debug("Can't update counter");
            // debug("Exception: " + e.getMessage());
            //  LoggingUtility.info("SQL Exception: " + e.getMessage());
        } finally {
            cp.closeConnection(connection, s);
        }


    }
    public void increaseCounter(String key) {
        int current = getCounter(key);
        setCounter(key, current + 1);
    }
    public void resetCounter(String key) {
        setCounter(key, 0);
    }
}
