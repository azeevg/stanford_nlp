package sesa.common.database.connection;

import java.sql.SQLException;

/**
 * Created by Ira on 03.02.2016.
 */
public class ConnectionPoolSingleton {

    private static ConnectionPoolSingleton instance = null;

    private ConnectionPool connectionPool;

    private ConnectionPoolSingleton() throws SQLException, ClassNotFoundException {
        connectionPool = new ConnectionPool();
        connectionPool.initPoolData();
    }

    public static synchronized ConnectionPoolSingleton getInstance() throws SQLException, ClassNotFoundException {
        if(instance == null) {
            instance =  new ConnectionPoolSingleton();
        }
        return instance;
    }

    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }
}
