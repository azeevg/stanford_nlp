package sesa.tools.filters.WIKI.category;

import sesa.common.configuration.Configuration;
import sesa.common.database.common.*;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.ConfigUtility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ByCategoryFilter {

  //  private DBController dbController;
    private DocumentsDao documentsDao = new DocumentsDao();
    private ConfigUtility configuration;
    private String selectedCategory;

    private final static String CAT_LIST = "cat_list";

    public ByCategoryFilter(/*DBController dbController,*/ ConfigUtility configuration, String categoryName) throws SQLException, ClassNotFoundException {
     //   this.dbController = dbController;
        this.selectedCategory = categoryName;
        this.configuration = configuration;
    }

    public boolean filter() {
        Set<String> categories = new HashSet<String>();
        BufferedReader reader = null;

        try {
            String categoryFile = configuration.getProperty(CAT_LIST) + "_" + selectedCategory + ".txt";
            String categoryTable = Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI + "_" + selectedCategory;
            reader = new BufferedReader(new FileReader(categoryFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                if (text.length() > 0)
                    categories.add(text.trim());
            }

            reader.close();
            documentsDao.truncateOrCreateDocumentCollection(categoryTable);
           // dbController.truncateOrCreate(categoryTable);

            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
           // dbController.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
            iterator = new DBReportingIterator(iterator, "Filtering " + selectedCategory);
            while (iterator.hasNext()) {
                IDBEntity entity = iterator.next().get();
                Map<String, Object> map = entity.toMap();
                List<String> docCats = (List<String>) map.get("cat");
                for (String docCat : docCats) {
                    if (categories.contains(clearCategory(docCat))) {
                        documentsDao.addToDocumentsCollection(categoryTable, entity);
                       // dbController.insertToDocumentsCollection(categoryTable, entity);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    public String clearCategory(String category) {
        return category.replaceAll("\\|", "").trim();
    }

}
