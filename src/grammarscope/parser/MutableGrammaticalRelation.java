package grammarscope.parser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import edu.stanford.nlp.trees.tregex.TregexPattern;

/**
 * Mutable (editable) grammatical relation
 * 
 * @author Bernard Bou
 */
public class MutableGrammaticalRelation extends ProtoGrammaticalRelation
{
	private static final long serialVersionUID = 3L;

	/**
	 * Name
	 */
	private String theName;

	/**
	 * Description
	 */
	private String theDescription;

	/**
	 * Example, the way the relation in the example is formalized. E.g. pred((Reagan,died) pairs
	 */
	private String[][] theCodedExamples;

	/**
	 * Constructor
	 * 
	 * @param thisName
	 *            name
	 * @param thisShortName
	 *            short name
	 * @param thisLongName
	 *            long name
	 * @param thisParent
	 *            parent relation
	 * @param thisSourcePatternString
	 *            uncompiled source pattern
	 * @param theseTargetPatternStrings
	 *            uncompiled target patterns
	 */
	public MutableGrammaticalRelation(final String thisName, final String thisShortName, final String thisLongName, final ProtoGrammaticalRelation thisParent, final String thisSourcePatternString, final String[] theseTargetPatternStrings)
	{
		super(thisShortName, thisLongName, thisParent, thisSourcePatternString, theseTargetPatternStrings, null);
		this.theName = thisName;
	}

	/**
	 * Constructor
	 * 
	 * @param thisName
	 *            name
	 * @param thisShortName
	 *            short name
	 * @param thisLongName
	 *            long name
	 * @param thisParent
	 *            parent relation
	 * @param thisSourcePatternString
	 *            uncompiled source pattern
	 * @param theseTargetPatternStrings
	 *            uncompiled target patterns
	 * @param thisDescription
	 *            description
	 * @param theseCodedExamples
	 *            examples
	 */
	public MutableGrammaticalRelation(final String thisName, final String thisShortName, final String thisLongName, final ProtoGrammaticalRelation thisParent, final String thisSourcePatternString, final String[] theseTargetPatternStrings,
			final String thisDescription, final String[][] theseCodedExamples)
	{
		this(thisName, thisShortName, thisLongName, thisParent, thisSourcePatternString, theseTargetPatternStrings);
		this.theDescription = thisDescription;
		this.theCodedExamples = theseCodedExamples;
	}

	/**
	 * Constructor
	 * 
	 * @param thisName
	 *            name
	 * @param thisShortName
	 *            short name
	 * @param thisLongName
	 *            long name
	 * @param thisParent
	 *            parent relation
	 * @param thisSourcePattern
	 *            compiled source pattern
	 * @param theseTargetPatterns
	 *            compiled target patterns
	 * @param thisDescription
	 *            description
	 * @param theseCodedExamples
	 *            examples
	 */
	private MutableGrammaticalRelation(final String thisName, final String thisShortName, final String thisLongName, final ProtoGrammaticalRelation thisParent, final Pattern thisSourcePattern, final List<TregexPattern> theseTargetPatterns,
			final String thisDescription, final String[][] theseCodedExamples)
	{
		this(thisName, thisShortName, thisLongName, thisParent, (String) null, (String[]) null, thisDescription, theseCodedExamples);
		this.sourcePattern = thisSourcePattern;
		this.targetPatterns = theseTargetPatterns;
	}

	/**
	 * Clone
	 * 
	 * @return duplicated relation (unlinked from hierarchy)
	 */
	@Override
	public MutableGrammaticalRelation clone()
	{
		final MutableGrammaticalRelation thisNewRelation = new MutableGrammaticalRelation(this.theName, this.shortName, this.longName, this.parent, this.sourcePattern, this.targetPatterns, this.theDescription, this.theCodedExamples);
		thisNewRelation.setParent(this.parent);
		return thisNewRelation;
	}

	// A D D / R E M O V E

	// adds visibility
	/*
	 * (non-Javadoc)
	 * 
	 * @see grammarscope.parser.ProtoGrammaticalRelation#addChild(grammarscope.parser.ProtoGrammaticalRelation)
	 */
	@Override
	public void addChild(final ProtoGrammaticalRelation child)
	{
		super.addChild(child);
	}

	/**
	 * Remove child from children array
	 * 
	 * @param child
	 *            child to remove
	 */
	public void removeChild(final ProtoGrammaticalRelation child)
	{
		this.children.remove(child);
	}

	// T R E E
	/**
	 * Get parent relation
	 * 
	 * @return parent relation
	 */
	@Override
	public MutableGrammaticalRelation getParent()
	{
		return (MutableGrammaticalRelation) super.getParent();
	}

	/**
	 * Get child relations
	 * 
	 * @return children
	 */
	public Iterable<MutableGrammaticalRelation> getChildrenIterable()
	{
		return new Iterable<MutableGrammaticalRelation>()
		{
			public Iterator<MutableGrammaticalRelation> iterator()
			{
				return getChildrenIterator();
			}
		};
	}

	/**
	 * Get child iterator
	 * 
	 * @return child iterator
	 */
	public Iterator<MutableGrammaticalRelation> getChildrenIterator()
	{
		return new Iterator<MutableGrammaticalRelation>()
		{
			private final Iterator<? extends ProtoGrammaticalRelation> theIterator = getChildren().iterator();

			public boolean hasNext()
			{
				return this.theIterator.hasNext();
			}

			public MutableGrammaticalRelation next()
			{
				return (MutableGrammaticalRelation) this.theIterator.next();
			}

			public void remove()
			{
				this.theIterator.remove();
			}
		};
	}

	// A C C E S S

	/**
	 * Get id
	 * 
	 * @return id
	 */
	public String getId()
	{
		return this.shortName;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 */
	public String getName()
	{
		return this.theName;
	}

	/**
	 * Get description
	 * 
	 * @return description
	 */
	public String getDescription()
	{
		return this.theDescription;
	}

	/**
	 * Get coded examples
	 * 
	 * @return coded examples
	 */
	public String[][] getCodedExamples()
	{
		return this.theCodedExamples;
	}

	/**
	 * Set short name
	 * 
	 * @param thisShortName
	 *            short name
	 */
	public void setShortName(final String thisShortName)
	{
		this.shortName = thisShortName;
	}

	/**
	 * Set long name
	 * 
	 * @param thisLongName
	 *            long name
	 */
	public void setLongName(final String thisLongName)
	{
		this.longName = thisLongName;
	}

	/**
	 * Set specific
	 * 
	 * @param thisSpecific
	 *            specific
	 */
	public void setSpecific(final String thisSpecific)
	{
		this.specific = thisSpecific;
	}

	/**
	 * Set name
	 * 
	 * @param theName
	 *            name
	 */
	public void setName(final String theName)
	{
		this.theName = theName;
	}

	/**
	 * Set description
	 * 
	 * @param theDescription
	 *            description
	 */
	public void setDescription(final String theDescription)
	{
		this.theDescription = theDescription;
	}

	/**
	 * Set parents
	 * 
	 * @param thisParent
	 *            parent relation
	 */
	public void setParent(final ProtoGrammaticalRelation thisParent)
	{
		this.parent = thisParent;
	}

	/**
	 * Set children
	 * 
	 * @param theseChildren
	 *            children
	 */
	public void setChildren(final List<ProtoGrammaticalRelation> theseChildren)
	{
		this.children = theseChildren;
	}

	/**
	 * Set source pattern
	 * 
	 * @param thisSourcePattern
	 *            source pattern
	 */
	public void setSourcePattern(final Pattern thisSourcePattern)
	{
		this.sourcePattern = thisSourcePattern;
	}

	/**
	 * Set target patterns
	 * 
	 * @param theseTargetPatterns
	 *            compiled target patterns
	 */
	public void setTargetPatterns(final List<TregexPattern> theseTargetPatterns)
	{
		this.targetPatterns = theseTargetPatterns;
	}

	/**
	 * Set coded examples
	 * 
	 * @param theseCodedExamples
	 *            coded examples
	 */
	public void setCodedExamples(final String[][] theseCodedExamples)
	{
		this.theCodedExamples = theseCodedExamples;
	}

	// H E L P E R S

	/**
	 * Extract source pattern from target patterns
	 * 
	 * @param thesePatterns
	 *            target patterns
	 * @return source pattern
	 */
	static public Pattern getSourcePattern(final List<TregexPattern> thesePatterns)
	{
		final StringBuffer thisBuffer = new StringBuffer();
		for (final TregexPattern thisPattern : thesePatterns)
		{
			final String thisString = thisPattern.pattern();
			final int thisIndex = thisString.indexOf(" ");
			final String thisSource = thisIndex == -1 ? thisString : thisString.substring(0, thisIndex);
			if (thisBuffer.length() != 0)
			{
				thisBuffer.append("|");
			}
			thisBuffer.append(thisSource);
		}
		final Pattern thisPattern = Pattern.compile(thisBuffer.toString());
		return thisPattern;
	}

	/**
	 * Stringify target patterns
	 * 
	 * @param thesePatterns
	 *            patterns
	 * @return pattern strings
	 */
	static public String[] getTargetPatternStrings(final List<TregexPattern> thesePatterns)
	{
		final List<String> thisList = new ArrayList<String>();
		for (final TregexPattern thisPattern : thesePatterns)
		{
			thisList.add(thisPattern.pattern());
		}
		return thisList.toArray(new String[] {});
	}

	// S T R I N G

	/**
	 * Get tree name (this node with appended lineage)
	 * 
	 * @param down
	 *            parent-child order
	 * @param displayLongName
	 *            whether to display long names
	 * @return tree name string
	 */
	public String getTreeName(final boolean down, final boolean displayLongName)
	{
		final StringBuffer thisBuffer = new StringBuffer();
		thisBuffer.append(getId());
		final String thisLineage = getLineage(down, displayLongName);
		if (!thisLineage.isEmpty())
		{
			thisBuffer.insert(down ? 0 : thisBuffer.length(), " | ");
			thisBuffer.insert(down ? 0 : thisBuffer.length(), thisLineage);
		}
		return thisBuffer.toString();
	}

	/**
	 * Get up lineage (parents to root)
	 * 
	 * @param down
	 *            parent-child order
	 * @param displayLongName
	 *            whether to display long names
	 * @return lineage string
	 */
	public String getLineage(final boolean down, final boolean displayLongName)
	{
		final StringBuffer thisBuffer = new StringBuffer();
		boolean first = true;
		for (MutableGrammaticalRelation thisAncestorRelation = getParent(); thisAncestorRelation != null; thisAncestorRelation = thisAncestorRelation.getParent())
		{
			if (!first)
			{
				thisBuffer.insert(down ? 0 : thisBuffer.length(), " | ");
			}
			first = false;
			thisBuffer.insert(down ? 0 : thisBuffer.length(), displayLongName ? thisAncestorRelation.getLongName() : thisAncestorRelation.getId());
		}
		return thisBuffer.toString();
	}

	/**
	 * Stringify pattern
	 * 
	 * @param thisPattern
	 *            pattern to stringify
	 * @return pattern string
	 */
	static public String patternToString(final TregexPattern thisPattern)
	{
		final StringWriter thisStringWriter = new StringWriter();
		final PrintWriter thisWriter = new PrintWriter(thisStringWriter);
		thisPattern.prettyPrint(thisWriter);
		thisWriter.close();
		String thisResult = thisStringWriter.getBuffer().toString().trim();

		// remove windows eol
		thisResult = thisResult.replaceAll("\r\n", "\n");
		return thisResult;
	}

	public String[] getTargetPatternStrings()
	{
		final List<TregexPattern> theseTargetPatterns = getTargetPatterns();
		final String[] theseStrings = new String[theseTargetPatterns.size()];
		int i = 0;
		for (final TregexPattern thisPattern : getTargetPatterns())
		{
			theseStrings[i++] = thisPattern.pattern();
		}
		return theseStrings;
	}

	/**
	 * Walk relation tree
	 * 
	 * @param thisProcessor
	 *            node processor
	 */
	public void walkTree(final RelationProcessor thisProcessor)
	{
		thisProcessor.process(this);
		for (final MutableGrammaticalRelation thisChild : getChildrenIterable())
		{
			final MutableGrammaticalRelation thisRelation = thisChild;
			thisRelation.walkTree(thisProcessor);
		}
	}

	/**
	 * Dump
	 * 
	 * @param theseArgs
	 */
	public static void main(final String[] theseArgs)
	{
		final RelationModel thisModel = DefaultMutableGrammaticalRelations.makeDefaultModel();

		thisModel.theRoot.walkTree(new RelationProcessor()
		{
			public void process(final MutableGrammaticalRelation thisRelation)
			{
				for (final TregexPattern thisPattern : thisRelation.getTargetPatterns())
				{
					final String thisPrettyString = thisPattern.pattern();
					final String thisCode = MutableGrammaticalRelation.patternToString(thisPattern);
					System.out.println(thisRelation.getShortName());
					System.out.println(thisRelation.getName());
					System.out.println(" [" + thisPrettyString + "]");
					System.out.println(" [" + thisCode + "]");
				}
			}
		});
	}
}
