package sesa.common.dataModule;

import grammarscope.parser.Sentence;
import sesa.common.database.dao.TreeCacheDao;

import java.sql.SQLException;
import java.util.List;

/**
 * Class for entries parse trees to the database
 */
public class WriteTreeDataModule extends BasicDataModule {

    protected static String DEFAULT_CACHE_TABLE = "tree_cache";
    protected String collection;
    protected TreeCacheDao treeCacheDao;

    /**
     * Constructor
     * Initialize default table to save parse tree to database
     * @param truncate key  - truncate table or not
     */
    public WriteTreeDataModule(boolean truncate) throws SQLException, ClassNotFoundException {
        treeCacheDao = new TreeCacheDao();
        if (truncate) {
            treeCacheDao.truncateDocumentCollection(DEFAULT_CACHE_TABLE);
        }
        this.collection = DEFAULT_CACHE_TABLE;

    }

    public WriteTreeDataModule(boolean truncate, String collection) throws SQLException, ClassNotFoundException {
        treeCacheDao = new TreeCacheDao();
        if (truncate) {
            treeCacheDao.truncateDocumentCollection(collection);
        }

        this.collection = collection;

    }

    /**
     * Write parse tree to database
     * Parse tree extracted from global treeMap
     * TODO: delete params, because we will not use in method. treeMap contains parse tree
     * @param title
     * @param sentences
     */
    protected void writeTreeCache(String title, List<Sentence> sentences) {
        //System.out.println(this.collection);
        for (int key : treeMap.keySet()) {
            // int key = sentence.toString().hashCode();
            String penn = treeMap.get(key).toString();
            String zippedPenn = penn.replaceAll(" \\(", "(").replaceAll("\\) ", ")");
            treeCacheDao.addCacheTree(this.collection, title.hashCode(), key, zippedPenn);
            // controller.insertObject(this.collection, title.hashCode(), key, zippedPenn);
        }
    }


}
