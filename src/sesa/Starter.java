package sesa;

import sesa.application.*;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.application.TestSemanticGroup;
import sesa.tools.cache.CacheDocuments;
import sesa.tools.dictionary.*;
import sesa.tools.dictionary.builder.AllCategoryDictionaryBuilder;
import sesa.tools.dictionary.builder.AnalyzeDictionary;
import sesa.tools.dictionary.builder.DictionaryBuilder;
import sesa.tools.document.Painter;
import sesa.tools.filters.WIKI.category.ByCategoryMySqlFilter;
import sesa.tools.filters.WIKI.category.ByCategoryJavaFilter;
import sesa.tools.filters.WIKI.category.CategoryFilter;
import sesa.tools.filters.WIKI.html.HtmlFilter;
import sesa.tools.filters.WIKI.mixed.MixedFilter;
import sesa.tools.filters.WIKI.text.TextFilter;
import sesa.tools.importers.ImporterPUBMED;
import sesa.tools.importers.ImporterWIKI;
import sesa.tools.statistics.*;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Starter extends LoggingUtility {
    private static final Map<String, String> arguments = new HashMap<String, String>();
    private static Map<String, Application> applications = new HashMap<String, Application>();

    static {
        setLoggingLevel(LOGGING_LEVEL.LEVEL_DEBUG);

        /* Importers */
        try {
            applications.put("importer-pubmed", new ImporterPUBMED());
            applications.put("importer-wiki", new ImporterWIKI());

        /* DictionaryTerms builders */
            applications.put("dictionary-builder", new DictionaryBuilder());
            applications.put("sd-dict-builder", new SymDiffDictionaryBuilder());
            applications.put("ac-dict-builder", new AllCatDictionaryBuilder());
            applications.put("all-cat-dict-builder", new AllCategoryDictionaryBuilder());

        /* Cache builders */
            applications.put("cache", new CacheDocuments());


        /*Metrics */
            applications.put("cent-rad-builder", new CenterRadiusBuilder());


        /* Filters */
            applications.put("filter-text-wiki", new TextFilter());
            applications.put("filter-cat-wiki", new CategoryFilter());
            applications.put("filter-mixed-wiki", new MixedFilter());
            applications.put("filter-html-wiki", new HtmlFilter());
            applications.put("by-cat-mysql-filter", new ByCategoryMySqlFilter());
            applications.put("by-cat-java-filter", new ByCategoryJavaFilter());

        /* Statistics */
            applications.put("statistics", new Statistics());
            applications.put("deviation", new SemanticGroupDeviationBuilder());
            applications.put("mistake", new MistakeReport());
            applications.put("entropy", new EntropyCalculator());
            applications.put("sem-entropy", new SemanticSaver());
            applications.put("sem-sd-builder", new SymDiffSemanticBuilder());
            applications.put("overall", new OverallCalculator());

        /* Tests */
            //  application.put("test-db",            new TestDB());
            applications.put("test-graph", new TestGraph());
            applications.put("test-metrics", new TestMetrics());
            applications.put("test-colored-page", new TestColoredPage());
            applications.put("test-triplet", new TestRelations());
            applications.put("sandbox", new Sandbox());
            applications.put("sandbox-2", new Sandbox2());
            applications.put("painter", new Painter());
            /** program parameters for test-grammer "--service=test-grammar -javaagent:object_size_fetcher.jar" */
            applications.put("test-grammar", new TestGrammar());

            applications.put("ac-dict-builder,sd-dict-builder,cent-rad-builder", null);
            applications.put("test-saver", new TestSemanticGroup());
            applications.put("collocations-parser", new TestCollocationSpanTaggingParser());

            applications.put("dictionary-analyzer", new AnalyzeDictionary());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private static void printHelp() {
        info("Use: java " + Starter.class.getCanonicalName() + " --service=SERVICENAME [--force]");
        info("");
        info("Available services are:");
        Set<String> services = applications.keySet();
        for (String serviceName : services)
            info("  " + serviceName);
    }

    /**
     *  Парсинг командной строки
     */

    private static boolean parse(String[] args) {
        if (args.length < 1)
            return false;

        for (String kv : args) {
            if (!kv.contains("="))
                arguments.put(kv, null);
            else {
                String[] keyValue = kv.split("=");
                arguments.put(keyValue[0], keyValue[1]);
            }
        }

        return true;
    }

    private static void stop(String message) {
        if (message != null)
            error(message);

        printHelp();
        System.exit(0);
    }

    //Начало программы всей
    public static void main(String[] args) {
        if (!parse(args))
            stop("Not enough arguments");

        if (!arguments.containsKey("--service"))
            stop(" --service parameter missed");

        if (applications.containsKey(arguments.get("--service").toLowerCase())) {
            String[] appKeys = arguments.get("--service").toLowerCase().split(",");
            for(String appKey : appKeys){
                Application app = applications.get(appKey);
                app.run();
            }

        } else
            stop("Unknown service name: " + arguments.get("--service"));

        info("Application completed");
    }
}
