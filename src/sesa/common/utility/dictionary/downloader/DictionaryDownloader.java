package sesa.common.utility.dictionary.downloader;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sesa.common.model.dictionary.DictionaryTerms;
import sesa.common.model.dictionary.Term;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Ira on 13.05.2016.
 */
public class DictionaryDownloader {
    public final Logger log = LogManager.getLogger(DictionaryDownloader.class);
    //  private DictionaryTerms dictionary;

//    public DictionaryDownloader() {
//        dictionary = new DictionaryTerms();
//    }

    public DictionaryTerms readDictionaryFromFile(String fileName) {
        List<String> strings = new ArrayList<String>();
        JSONParser parser = new JSONParser();
        log.info("Download dictionary from file:" + fileName);
        DictionaryTerms dictionary = new DictionaryTerms();
        try {
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                String jsonStr = in.nextLine();

                try {
                    Object obj = parser.parse(jsonStr);
                    JSONObject jsonObject = (JSONObject) obj;
                    if (jsonObject.get("term_name") != null) {
                        String term_name = jsonObject.get("term_name").toString();
                        String def = jsonObject.get("term_def").toString();
                        List<String> terms_links = (List<String>) jsonObject.get("term_link");
                        dictionary.addTerm(new Term(term_name, def, terms_links));
                    }
                } catch (ParseException pe) {
                    log.error(pe.getMessage());
                    log.error(jsonStr);
                    pe.printStackTrace();
                }
            }
            log.info("-- end of downloaded --  ");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Dictionary size:" + dictionary.getSize());
        //System.out.println("All:" + allLines);
        //System.out.println("Strings:" + strings.size());
        return dictionary;
    }

//    public DictionaryTerms getDictionary() {
//        return dictionary;
//    }
}
