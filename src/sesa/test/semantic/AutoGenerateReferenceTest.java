package sesa.test.semantic;

import org.junit.Test;
import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.common.utility.FileReaderWriter;
import sesa.tools.semantic.AutoReference;
import sesa.tools.semantic.linkBuilder.AutoGenerateReference;
import sesa.common.utility.UtilityDBMethods;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Ira on 07.06.2016.
 */
public class AutoGenerateReferenceTest {

    @Test
    public void testFindReferenceForListTerms() throws IOException, SQLException, ClassNotFoundException {

        List<Integer> list = UtilityDBMethods.getSubsetTermsNotContainLink(FileReaderWriter.readNumber("data/idUniqueTerm.txt"));
        AutoGenerateReference autoGenerateReference = new AutoGenerateReference();
        autoGenerateReference.run(list);
    }

    @Test
    public void testFindReferenceForOneTerm() throws IOException, SQLException, ClassNotFoundException {

       // List<Integer> list = UtilityDBMethods.getSubsetTermsNotContainLink(FileReaderWriter.readNumber("data/idUniqueTerm.txt"));
        AutoGenerateReference autoGenerateReference = new AutoGenerateReference();
        AutoReference dto = autoGenerateReference.findLinksInDefinition("COGO");
        autoGenerateReference.saveToDataBase(dto);
    }
    @Test
    public void testCheckReference() throws SQLException, ClassNotFoundException {
        LinkStructureDao dao = new LinkStructureDao();
        System.out.println(dao.getListSemanticGroupDtoByTermID(11119, "auto_comp_reference").getTermLink().size());
    }

    @Test
    public void testGetIdWithNullReference() throws SQLException, ClassNotFoundException {
        List<Integer> list = UtilityDBMethods.getSubsetTermsNotContainLink(FileReaderWriter.readNumber("data/idUniqueTerm.txt"));
        FileReaderWriter.write("data/dictionary/auto_reference/idTermsNotContainAutoRef", UtilityDBMethods.getSubsetTermsNotContainsAutoReference(list));
    }

    @Test
    public void testAddOneRowToDBTableReference() throws SQLException, ClassNotFoundException {
       UtilityDBMethods.addLinkPairToDataBase("virtual connection","high resolution","auto_comp_reference");
    }
}
