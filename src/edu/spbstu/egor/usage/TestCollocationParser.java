package edu.spbstu.egor.usage;

import edu.spbstu.appmath.collocmatch.Collocation;
import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.appmath.collocmatch.Sentence;
import edu.spbstu.egor.*;
import edu.stanford.nlp.parser.lexparser.ExhaustivePCFGParser;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ira on 04.04.2016.
 */
public class TestCollocationParser {

    @Test
    public void testCollocation()throws IOException{
        Grammar grammar = new Grammar();

        // parser factory used for easier access to parsers
        ParserFactory parserFactory = new ParserFactory(grammar);

        // reading collocations dictionary
        final String[] collocationDictFiles = { "./egor_in/collocations.txt" };
        CollocationDictionary collocationDictionary = new CollocationDictionary(collocationDictFiles);

        // creating parser
        ExhaustivePCFGParser collocationExtendedParser =
                parserFactory.getCollocationSpanTaggingParser(collocationDictionary);

        // creating tree getter (actually is just a wrapper...)
        TreeGetter tg = new TreeGetter();
        tg.setPparser(collocationExtendedParser);

        Sentence sentence = new Sentence("Ada programming language .");
        Tree tree = tg.getBestParseTree(SentenceGetter.convert(sentence));

        // Tree Utils for tree printing
        TreeUtils treeUtils = new TreeUtils(grammar.op);

        // printing tree
        treeUtils.preprocessedPennPrint(tree);
    }

    @Test
    public void testCollocationParser() throws IOException{
        Grammar grammar = new Grammar();

        // parser factory used for easier access to parsers
        ParserFactory parserFactory = new ParserFactory(grammar);

        // reading collocations dictionary
        final String[] collocationDictFiles = { "./egor_in/collocations.txt" };
        CollocationDictionary collocationDictionary = new CollocationDictionary(collocationDictFiles);

        // creating parser
        ExhaustivePCFGParser collocationExtendedParser =
                parserFactory.getCollocationSpanTaggingParser(collocationDictionary);

        // creating tree getter (actually is just a wrapper...)
        TreeGetter tg = new TreeGetter();
        tg.setPparser(collocationExtendedParser);

        List<String> strings = readCollocation("./egor_in/collocations.txt");
        TreeUtils treeUtils = new TreeUtils(grammar.op);
        PrintWriter stdPW = new PrintWriter(new File("out_trees.txt"));
        for(String s: strings){
            Sentence sentence = new Sentence(s);
            Tree tree = tg.getBestParseTree(SentenceGetter.convert(sentence));

            treeUtils.preprocessedPennPrint2(tree, stdPW);
            System.out.println("----------");
        }
        stdPW.close();
//        Sentence sentence = new Sentence("Programming languages created in 1953 ");
//        Tree tree = tg.getBestParseTree(SentenceGetter.convert(sentence));
//
//        // Tree Utils for tree printing
//        TreeUtils treeUtils = new TreeUtils(grammar.op);
//
//        // printing tree
//        treeUtils.preprocessedPennPrint(tree);
    }


    public List<String> readCollocation(String filename) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
        List<String> inputStrings = new ArrayList<String>();

        while(true) {
            StringBuilder stringBuilder = new StringBuilder();

            String s = br.readLine();
            if(s == null) {
                break;
            }
            String inputStr = s.trim().toLowerCase() + " . ";
            inputStrings.add(inputStr);
            System.out.println(inputStr);


        }
        return inputStrings;
    }

    public List<String> readCollocation2(String filename) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
        List<String> inputStrings = new ArrayList<String>();

        while(true) {
            StringBuilder stringBuilder = new StringBuilder();

            String s = br.readLine();
            if(s == null) {
                break;
            }

            String inputStr = s.trim().toLowerCase() + " . ";
            inputStrings.add(inputStr);
            System.out.println(inputStr);


        }
        return inputStrings;
    }



}
