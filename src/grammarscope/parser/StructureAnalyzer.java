package grammarscope.parser;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.tregex.ParseException;

/**
 * Grammatical Structure Analyzer
 * 
 * @author Bernard Bou
 */
public class StructureAnalyzer
{
	/**
	 * Grammatical structure factory
	 */
	private final GrammaticalStructureFactory theFactory;

	/**
	 * Grammatical relations
	 */
	private final Collection<GrammaticalRelation> theGrammaticalRelations;

	// C O N S T R U C T

	/**
	 * Constructor
	 * 
	 * @param thisTlp
	 *            tree languagepack
	 * @param theseGrammaticalRelations
	 *            grammatical relations
	 * @throws UnsupportedOperationException
	 * @throws IllegalArgumentException
	 */
	public StructureAnalyzer(final TreebankLanguagePack thisTlp, final Collection<GrammaticalRelation> theseGrammaticalRelations) throws UnsupportedOperationException, IllegalArgumentException
	{
		if (thisTlp == null)
			throw new IllegalArgumentException("Null TreebankLanguagePack");
		this.theFactory = thisTlp.grammaticalStructureFactory();
		this.theGrammaticalRelations = theseGrammaticalRelations;
	}

	/**
	 * Constructor
	 */
	public StructureAnalyzer()
	{
		this(new PennTreebankLanguagePack(), EnglishGrammaticalRelations.values());
	}

	// A C C E S S

	public Collection<GrammaticalRelation> getGrammaticalRelations()
	{
		return this.theGrammaticalRelations;
	}

	// A N A L Y Z E G R A M M A T I C A L S T R U C T U R E

	/**
	 * Analyze document
	 * 
	 * @param thisUrl
	 *            document url
	 * @param thisParser
	 *            parser
	 * @return grammatical structures
	 * @throws IOException
	 */
	/*public List<GrammaticalStructure> analyzeDocument(final URL thisUrl, final Parser thisParser) throws IOException
	{
		final List<Sentence> theseSentences = Parser.getSentences(thisUrl);
		final List<Tree> theseTrees = thisParser.parse(theseSentences);
		return analyze(theseTrees);
	}*/

	/**
	 * Analyze sentence
	 * 
	 * @param thisSentence
	 *            sentence
	 * @param thisParser
	 *            parser
	 * @return grammatical structures
	 */
	public GrammaticalStructure analyze(final Sentence thisSentence, final Parser thisParser)
	{
		final Tree thisTree = thisParser.parse(thisSentence);
		return analyzeTree(thisTree);
	}

	/**
	 * Analyze trees
	 * 
	 * @param theseTrees
	 *            trees to analyze
	 * @return grammatical structures
	 */
	public List<GrammaticalStructure> analyze(final List<Tree> theseTrees)
	{
		final List<GrammaticalStructure> thisList = new ArrayList<GrammaticalStructure>();
		for (final Tree thisTree : theseTrees)
		{
			thisList.add(analyzeTree(thisTree));
		}
		return thisList;
	}

	/**
	 * Analyze tree
	 * 
	 * @param thisTree
	 *            tree to analyze
	 * @return grammatical structure
	 */
	public GrammaticalStructure analyzeTree(final Tree thisTree)
	{
		return this.theFactory.newGrammaticalStructure(thisTree);
	}

	// M A I N

	/**
	 * Analyze document
	 * 
	 * @param theseArgs
	 *            arg[1] document path
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	/*static public void main(final String[] theseArgs) throws ParseException, InterruptedException, IOException
	{
		final URL thisUrl = new File(theseArgs[0]).toURI().toURL();
		final ObjectInputStream thisInputStream = IOUtils.readStreamFromString(theseArgs[1]);
		final Parser thisParser = new Parser(thisInputStream, "-retainNPTmpSubcategories");
		final StructureAnalyzer thisAnalyzer = new StructureAnalyzer();
		final List<GrammaticalStructure> theseStructures = thisAnalyzer.analyzeDocument(thisUrl, thisParser);
		for (final GrammaticalStructure thisStructure : theseStructures)
		{
			System.out.println();
			System.out.println(thisStructure.typedDependencies());
		}
	} */
}
