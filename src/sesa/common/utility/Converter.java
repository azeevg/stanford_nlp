package sesa.common.utility;

import edu.stanford.nlp.ling.Word;
import grammarscope.parser.Sentence;

import java.util.ArrayList;

/**
 * Created by Ira on 06.09.2016.
 */
public class Converter {

    public static Sentence convertStringToSentence(String text) {
        ArrayList<Word> wordArrayList = new ArrayList<Word>();
        for (String token : text.split(" ")) {
            wordArrayList.add(new Word(token));
        }
        Sentence sentence = new Sentence(wordArrayList);
        return sentence;

    }
}
