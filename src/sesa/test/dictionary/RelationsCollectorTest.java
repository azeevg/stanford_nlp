package sesa.test.dictionary;

import org.junit.Test;
import sesa.common.utility.dictionary.RelationsCollector;
import sesa.common.utility.dictionary.TermRelationsGraph;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by gleb on 31.05.17.
 */

public class RelationsCollectorTest {

    public static void main(String[] s) throws SQLException, ClassNotFoundException {
        RelationsCollector collector = new RelationsCollector();
        Map<Integer, Map<String, Integer>> relations = collector.collectRelatedTerms();
        TermRelationsGraph graph = new TermRelationsGraph(collector.getTermsIds(), relations);

        int t1 = 1;
        int t2 = 1;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert ids:");

        while (scanner.hasNext()) {
            t1 = scanner.nextInt();
            t2 = scanner.nextInt();

            System.out.println(t1 + "->" + t2 + ": " + graph.getDistance(t1, t2));
        }

        scanner.close();
    }

    @Test
    public void littleDictionaryTest() throws Exception {
        Integer[] intIds = new Integer[]{1, 2, 4, 3, 5, 7, 8, 10, 6, 9};
        List<Integer> ids = new ArrayList<>(Arrays.stream(intIds).collect(Collectors.toList()));
        Map<Integer, Map<String, Integer>> relations = new HashMap<>();
        relations.put(1, new HashMap<>());
        relations.put(4, new HashMap<>());
        relations.put(5, new HashMap<>());
        relations.put(9, new HashMap<>());
        relations.put(10, new HashMap<>());

        Map<String, Integer> map = new HashMap<>();
        map.put("e", 5);
        map.put("i", 9);
        map.put("h", 8);
        relations.put(2, map);

        map = new HashMap<>();
        map.put("i", 9);
        map.put("g", 7);
        map.put("b", 2);
        relations.put(8, map);

        map = new HashMap<>();
        map.put("d", 4);
        map.put("b", 2);
        relations.put(7, map);

        map = new HashMap<>();
        map.put("d", 4);
        relations.put(3, map);

        map = new HashMap<>();
        map.put("j", 10);
        relations.put(6, map);


        TermRelationsGraph graph = new TermRelationsGraph(ids, relations);

        int[][] distances = graph.getAllDistances();
        ids = new ArrayList<>(Arrays.stream(intIds).collect(Collectors.toList()));
        ids.sort(Integer::compareTo);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (distances[i][j] == 1000000)
                    System.out.print("y\t");
                else
                    System.out.print(distances[i][j] + "\t");
            }
            System.out.println();
        }

        boolean[] isRowRelated = new boolean[10];
        boolean[] isColumnRelated = new boolean[10];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j != i && distances[i][j] != 1000000) {
                    isColumnRelated[j] = true;
                    isRowRelated[i] = true;
                }
            }
        }

        List<Integer> totallyLonelyNodes = new ArrayList<>();
        List<Integer> noInNodes = new ArrayList<>();
        List<Integer> noOutNodes = new ArrayList<>();

        for (int j = 0; j < isRowRelated.length; j++) {
            if (!isRowRelated[j])
                noOutNodes.add(ids.get(j));
            if (!isColumnRelated[j])
                noInNodes.add(ids.get(j));
            if (!isRowRelated[j] && !isColumnRelated[j])
                totallyLonelyNodes.add(ids.get(j));

        }

        totallyLonelyNodes.sort(Integer::compareTo);
        noInNodes.sort(Integer::compareTo);
        noOutNodes.sort(Integer::compareTo);

        FileWriter writer = new FileWriter("lonelyNodes_10.txt");

        writer.append("totallyLonelyNodes" + totallyLonelyNodes.size() + "\n");
        totallyLonelyNodes.forEach(n -> {
            try {
                writer.append(n + "").append(";");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.append("\nnoInNodes" + noInNodes.size() + "\n");

        noInNodes.forEach(n -> {
            try {
                writer.append(n + "").append(";");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.append("\nnoOutNodes" + noOutNodes.size() + "\n");


        noOutNodes.forEach(n -> {
            try {
                writer.append(n + "").append(";");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        writer.close();
    }

    // calculates distances between each pair of graph nodes
    // then writes it and nodes id (terms id) to csv-file
    @Test
    public void calculateDistances() throws Exception {
        long start = System.currentTimeMillis();

        new DistancesWriter().handle();

        long end = System.currentTimeMillis();
        long t = end - start;
        long mins = t / 1000 / 60;
        System.out.println("Elapsed time: " + (mins / 60) + ":" + (mins % 60) + ":" + t / 1000 % 60);


    }

    @Test
    public void name1() throws Exception {
        String s = "In Cryptographic Support, sixteen 8-byte keys created by the Data Encryption Algorithm from the supplied cryptographic key that are used to encrypt or decrypt the supplied data.";
//        System.out.println(Arrays.toString(s.split("((?<=[^\\w]+)|(?=[^\\w]+))")));
        RelationsCollector collector = new RelationsCollector();
        System.out.println(s);
        collector.findTerms(s);
    }

    /* prints or writes to file with format
        *  termId:definitionId
        *  relatedTermId_1=relatedTerm_1
        *  relatedTermId_2=relatedTerm_2
        *  ...
        *  0=undefinedTerm_1
        *  0=undefinedTerm_2
        *  ...
        * */
    @Test
    public void handleNDefs() throws Exception {
        long n = Long.MAX_VALUE;
        boolean writeToFile = true;

        FileWriter writer = new FileWriter("relationsBetweenTerms.txt");

        long start = System.currentTimeMillis();

        RelationsCollector collector = new RelationsCollector();

        collector.collectRelatedTerms(n).forEach((termId, defIdAndRelatedTerms) -> {
            StringJoiner joiner = new StringJoiner("\n");
            String title = "?" + termId;
            joiner.add(title);
            defIdAndRelatedTerms.forEach((term, id) -> joiner.add(id + "=" + term));
            joiner.add("\n");

            if (writeToFile)
                try {
                    writer.append(joiner.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            else
                System.out.println(joiner.toString());
        });

        writer.close();

        long end = System.currentTimeMillis();
        long t = end - start;
        long mins = t / 1000 / 60;
        System.out.println("Elapsed time: " + (mins / 60) + ":" + (mins % 60) + ":" + t / 1000 % 60);
    }

    @Test
    public void testAddAllTerms() throws SQLException, ClassNotFoundException {
        new RelationsCollector().findTerms("In a relational database, a database object that consists of a specific number of columns and is used to store an unordered set of rows. ")
                .forEach((id, term) -> System.out.println(id + "=" + term));
    }

    @Test
    public void ibmDefTest() throws Exception {
        String s = "A 4-byte, unsigned integer (uid) used to identify a user profile. PoP POP pop. Bid BID CASE or case casE";

        new RelationsCollector().findTerms(s).entrySet().stream().sorted(Comparator.comparing(p -> p.getKey().length()))
                .forEach(term -> System.out.println(term.getKey() + "=" + term.getValue()));

//        RelationsCollector collector = new RelationsCollector();
//        Map<Integer, Map<String, Integer>> relations = collector.collectRelatedTerms();
//        TermRelationsGraph graph = new TermRelationsGraph(collector.getTermsIds(), relations);
    }

    @Test
    public void termClassifierTest() throws Exception {
        String s1 = "ADSD";
        String s2 = "ADSD sdf";
        String s3 = "pop";
        String s4 = "POP";
        String s5 = "Pop";

        System.out.println(RelationsCollector.classifyTerm(s1));
        System.out.println(RelationsCollector.classifyTerm(s2));
        System.out.println(RelationsCollector.classifyTerm(s3));
        System.out.println(RelationsCollector.classifyTerm(s4));
        System.out.println(RelationsCollector.classifyTerm(s5));
    }

    class DistancesWriter {
        public void handle() throws SQLException, ClassNotFoundException, IOException {

            RelationsCollector collector = new RelationsCollector();
            Map<Integer, Map<String, Integer>> relations = collector.collectRelatedTerms();
            List<Integer> ids = collector.getTermsIds();
            ids.sort(Integer::compareTo);
            TermRelationsGraph graph = new TermRelationsGraph(ids, relations);

            int[][] distances = graph.getAllDistances();

            FileWriter idsWriter = new FileWriter("ids_" + ids.size() + ".csv");
            for (Integer id : ids) {
                idsWriter.append(id.toString()).append(";");
            }
            idsWriter.close();

            FileWriter writer = new FileWriter("relationsTable_" + collector.getTermsIds().size() + ".csv");

            for (int i = 0; i < distances.length; i++) {
                for (int j = 0; j < distances.length; j++) {
                    if (distances[i][j] == 1000000)
                        writer.append(String.valueOf(-1)).append(j == distances.length - 1 ? "" : ";");
                    else
                        writer.append(String.valueOf(distances[i][j])).append(j == distances.length - 1 ? "" : ";");
                }
                writer.append("\n");
            }

            writer.close();
        }
    }
}
