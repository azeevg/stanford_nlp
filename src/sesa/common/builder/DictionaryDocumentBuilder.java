package sesa.common.builder;

import org.jsoup.Jsoup;
import sesa.common.model.processing.document.CategorizedDocument;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.document.Document;
import sesa.common.model.processing.document.DocumentNode;

import java.util.*;


public class DictionaryDocumentBuilder extends BasicBuilder {

    public IDocument buildDocument(String id, String text, Set<String> categoryDictionary) throws BuilderException {

        org.jsoup.nodes.Document document = Jsoup.parse(text);
        DocumentNode root = buildDocumentTree(document);

        if (categoryDictionary == null)
            return new Document(id, title, root);
        else
            return new CategorizedDocument(id, title, root, categoryDictionary);
    }


}



