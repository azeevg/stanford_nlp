package grammarscope.parser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import edu.stanford.nlp.international.Languages;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.international.arabic.ArabicTreebankLanguagePack;
import edu.stanford.nlp.trees.international.french.FrenchTreebankLanguagePack;
import edu.stanford.nlp.trees.international.hebrew.HebrewTreebankLanguagePack;
import edu.stanford.nlp.trees.international.negra.NegraPennLanguagePack;
import edu.stanford.nlp.trees.international.pennchinese.ChineseGrammaticalRelations;
import edu.stanford.nlp.trees.international.pennchinese.ChineseTreebankLanguagePack;
import grammarscope.exporter.Setter;

public class Factories
{
	// L A N G U A G E

	static public GrammaticalRelation.Language map(final Languages.Language thisLanguage)
	{
		switch (thisLanguage)
		{
		case Chinese:
			return GrammaticalRelation.Language.Chinese;
		case English:
			return GrammaticalRelation.Language.English;
		case Arabic:
		case French:
		case German:
		case Hebrew:
		default:
			return GrammaticalRelation.Language.Any;
		}
	}

	// L A N G U A G E P A C K

	/**
	 * TreebankLanguagePack factory
	 * 
	 * @param thisTlpName
	 *            TreebankLanguagePack name
	 * @return TreebankLanguagePack
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	static public TreebankLanguagePack makeLanguagePack(final String thisTlpName) throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		final Class<?> thisClass = Class.forName(thisTlpName);
		return (TreebankLanguagePack) thisClass.newInstance();
	}

	/**
	 * TreebankLanguagePack factory
	 * 
	 * @param thisLanguage
	 *            language
	 * @return TreebankLanguagePack
	 */
	static public TreebankLanguagePack makeLanguagePack(final Languages.Language thisLanguage)
	{
		switch (thisLanguage)
		{
		case Chinese:
			return new ChineseTreebankLanguagePack();
		case English:
			return new PennTreebankLanguagePack();
		case Arabic:
			return new ArabicTreebankLanguagePack();
		case French:
			return new FrenchTreebankLanguagePack();
		case German:
			return new NegraPennLanguagePack();
		case Hebrew:
			return new HebrewTreebankLanguagePack();
		}
		throw new IllegalArgumentException(thisLanguage.toString());
	}

	/**
	 * TreebankLanguagePack factory
	 * 
	 * @param thisLanguage
	 *            language
	 * @return TreebankLanguagePack
	 */
	static public TreebankLanguagePack makeLanguagePack(final GrammaticalRelation.Language thisLanguage)
	{
		switch (thisLanguage)
		{
		case Any:
			return new TreebankLanguagePackProxy(new PennTreebankLanguagePack(), AnyGrammaticalRelations.values());
		case Chinese:
			return new ChineseTreebankLanguagePack();
		case English:
			return new PennTreebankLanguagePack();
		}
		throw new IllegalArgumentException(thisLanguage.toString());
	}

	// G R A M M A T I C A L S T R U C T U R E

	static public Class<? extends edu.stanford.nlp.trees.GrammaticalStructure> makeGrammaticalStructureClass(final GrammaticalRelation.Language thisLanguage)
	{
		switch (thisLanguage)
		{
		case Any:
			return GrammaticalStructure.Any.class;
		case English:
			return GrammaticalStructure.English.class;
		case Chinese:
			return GrammaticalStructure.Chinese.class;
		}
		throw new IllegalArgumentException(thisLanguage.toString());
	}

	// G R A M M A T I C A L R E L A T I O N S

	static public Class<?> makeGrammaticalRelationClass(final GrammaticalRelation.Language thisLanguage)
	{
		switch (thisLanguage)
		{
		case Any:
			return AnyGrammaticalRelations.class;
		case English:
			return EnglishGrammaticalRelations.class;
		case Chinese:
			return ChineseGrammaticalRelations.class;
		}
		throw new IllegalArgumentException(thisLanguage.toString());
	}

	// G R A M M A T I C A L R E L A T I O N S

	/**
	 * Make collection of grammatical relations from model
	 * 
	 * @param thisLanguage
	 *            language
	 * @param thisModel
	 *            to set relation class with
	 * @return Collection of grammatical relations
	 */
	static public List<GrammaticalRelation> makeGrammaticalRelationCollection(final GrammaticalRelation.Language thisLanguage, final RelationModel thisModel)
	{
		final Class<?> thisClass = Factories.makeGrammaticalRelationClass(thisLanguage);
		Setter.set(thisModel, thisClass);
		System.out.println("Updating " + thisClass + " to current model");
		try
		{
			return Factories.invokeValues(thisClass);
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Make collection of grammatical relations from default model
	 * 
	 * @param thisLanguage
	 *            language
	 * @return Collection of grammatical relations
	 */
	static public List<GrammaticalRelation> makeDefaultGrammaticalRelationCollection(final GrammaticalRelation.Language thisLanguage)
	{
		final RelationModel thisModel = DefaultMutableGrammaticalRelations.makeDefaultModel();
		return Factories.makeGrammaticalRelationCollection(thisLanguage, thisModel);
	}

	/**
	 * Make collection of grammatical relations from language
	 * 
	 * @param thisLanguage
	 *            language
	 * @return Collection of grammatical relations
	 */
	static public List<GrammaticalRelation> makeGrammaticalRelationCollection(final GrammaticalRelation.Language thisLanguage)
	{
		final Class<?> thisClass = Factories.makeGrammaticalRelationClass(thisLanguage);
		return Factories.makeGrammaticalRelationCollection(thisClass);
	}

	/**
	 * Make collection of grammatical relations from relations set class
	 * 
	 * @param thisClass
	 *            relation set class
	 * @return Collection of grammatical relations
	 */
	static public List<GrammaticalRelation> makeGrammaticalRelationCollection(final Class<?> thisClass)
	{
		try
		{
			return Factories.invokeValues(thisClass);
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * All grammatical relations classes have a values method but no common interface
	 * 
	 * @param thisClass
	 *            class
	 * @return result of calling values();
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@SuppressWarnings("unchecked")
	static public List<GrammaticalRelation> invokeValues(final Class<?> thisClass) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException
	{
		final Method thisMethod = thisClass.getMethod("values", (Class<?>[]) null);
		return (List<GrammaticalRelation>) thisMethod.invoke(null, (Object[]) null);
	}
}
