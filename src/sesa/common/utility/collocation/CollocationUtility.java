package sesa.common.utility.collocation;

import edu.spbstu.appmath.collocmatch.Collocation;
import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.stanford.nlp.trees.Tree;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;

import java.io.*;
import java.util.*;

/**
 * Created by Ira on 28.04.2016.
 */
public class CollocationUtility {
    public static final Logger log = LogManager.getLogger(CollocationUtility.class);
    private static String collocationsInTree= "./data/collocationsInTree.txt";
    private static String collocationsInTextFile = "./data/collocationsInText.txt";
    /**
     * Save list of collocation to file
     * @param collocation
     * @param collocationFileName
     */
    public static void addToCollocationFiles(List<String> collocation, String collocationFileName) {
        log.info("Adding collocations to file" + collocationFileName);
        for (String s : collocation) {
            FileReaderWriter.write(collocationFileName, s);
        }
        log.info(" -- added --");

    }

    public static CollocationDictionary initCollocationDictionary(){
        CollocationDictionary dictionary;
        FileManager fileManager = new FileManager("path");
        try {
            dictionary = new CollocationDictionary(new String[]{fileManager.getValue("path.computerCollocations")});
            return dictionary;
        } catch (IOException e) {

        }
        return null;
    }

    /**
     * Extract the list of collocations from all titles
     * After that we add this list to another collocations list
     *
     * @return list of collocations
     */
    public static List<String> getCollocations(List<String> titles) {
        List<String> collocations = new ArrayList<String>();
        for (String terms : titles) {
            terms = terms.toLowerCase().trim();
            if (terms.contains(" ")) {
                collocations.add(terms);
            }
        }
        return collocations;
    }

    public static List<String> convertSetToList(Set<String> terms) {
        List<String> titles_terms = new ArrayList<String>();
        for (String term : terms) {
            titles_terms.add(term);
        }
        return titles_terms;
    }

    public static HashMap<String,Integer> findSubSetCollocations(String fName1, String fName2, int scoreValue){
        HashMap<String,Integer> collocations = new HashMap<String, Integer>();

        try{
            Scanner in1 = new Scanner(new File(fName1));
            Scanner in2 = new Scanner(new File(fName2));
            while (in1.hasNextLine() && in2.hasNextLine()) {
                int score = Integer.valueOf(in2.nextLine());
                String key = in1.nextLine();
                if(score > scoreValue){
                    collocations.put(key,score);
                }
            }
        }catch(FileNotFoundException e){

        }
        return collocations;
    }


    public static int calcNumCollocationInSentence(CollocationDictionary collocations , String sentence) {
        int numCollocationsInSentence = 0;
        for (Collocation collocation : collocations) {
            numCollocationsInSentence += frequencyOfOccurrence(sentence,collocation.toString());
        }
        return numCollocationsInSentence;
    }

    public static List<String> getCollocationInSentence(CollocationDictionary collocations , String sentence){
        List<String> list = new ArrayList<String>();

        for (Collocation collocation : collocations) {
            list.addAll(getListTerm(sentence, collocation.toString()));
        }
        return list;
    }


    public static List<String> getCollocationInTree(Set<String> collocations , Tree tree){
        List<String> leaves = convertLeavesToString(tree);
        List<String> list = new ArrayList<String>();
        for(String leave: leaves){
            if(collocations.contains(leave.toLowerCase())){
                list.add(leave);
            }
        }

        return list;
    }



    public static List<String> convertLeavesToString(Tree tree){

        List<String> list = new ArrayList<String>();
        for(Tree t: tree.getLeaves()){
            list.add(t.toString());
        }
        System.out.println(list);
        return list;
    }

    public static int calcNumCollocationInTree(CollocationDictionary collocations , String sentence){
        int numCollocationsInSentence = 0;
        for (Collocation collocation : collocations) {
            StringBuilder sb = new StringBuilder();
            for (String w : collocation.words()) {
                sb.append(w);
            }
            numCollocationsInSentence += frequencyOfOccurrence(sentence,sb.toString());
        }
        return numCollocationsInSentence;
    }

    public static int calcNumCollocationInText(CollocationDictionary collocations ,List<String> text) {
        int res = 0;
        for (String sentence : text) {
            res += calcNumCollocationInSentence(collocations , sentence);
        }
        return res;
    }

    /**
     * ������� ������� ������� � ������
     * @param text
     * @param term
     * @return
     */
    public static int frequencyOfOccurrence(String text, String term){

        text = text.toLowerCase();
        term = term.toLowerCase();
        int count = 0;
        int termLength = term.length();
        if(text.contains(term)){
            count++;

            int index = text.indexOf(term);
            while(true){
                index = text.indexOf(term, index + termLength);
                if(index == -1){
                    return count;
                }

                count++;
            }
        }
        return count;
    }

    public static List<String> getListTerm(String text, String term){
        List<String> list = new ArrayList<String>();
        // System.out.println(text);
        text = text.toLowerCase();
        term = term.toLowerCase();
        int count = 0;
        int termLength = term.length();
        if(text.contains(term)){
            count++;
            list.add(term);
            int index = text.indexOf(term);
            while(true){
                index = text.indexOf(term, index + termLength);
                if(index == -1){
                    return list;
                    //return count;
                }
                list.add(term);
                count++;
            }
        }
        return list;
    }

    public static List<String> readCollocation(String filename) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
        List<String> inputStrings = new ArrayList<String>();

        while(true) {
            String s = br.readLine();
            if(s == null) {
                break;
            }
            String inputStr = s.trim().toLowerCase() + " . ";
            inputStrings.add(inputStr);
            System.out.println(inputStr);
        }
        return inputStrings;
    }
}
