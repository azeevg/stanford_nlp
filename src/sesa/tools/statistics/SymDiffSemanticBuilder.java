package sesa.tools.statistics;

import sesa.common.builder.BuilderException;
import sesa.common.configuration.Categories;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.CommonDao;
import sesa.common.database.dao.SemanticGroupDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.dictionary.DictionaryUtils;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.builder.DictionaryDocumentBuilder;
import sesa.common.utility.vector.DocumentUtils;

import java.sql.SQLException;
import java.util.*;

/**
 * Пополнение словарей семантическими группами, используя симментическую разницу
 */
public class SymDiffSemanticBuilder extends Application {

    private CommonDao commonDao = new CommonDao();
    private SemanticGroupDao semanticGroupDao = new SemanticGroupDao();

    public SymDiffSemanticBuilder() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() {
     //   dbController.useConnection(0);


   /*   Set<String> categorySet = new HashSet<String>();
      String collectionName = Categories.buildCategoryHtmlCollectionName(Categories.LITERATURE_CATEGORY);
      IDBIterator iterator = dbController.getDocumentsCollectionIterator(collectionName);
      //iterator = new DBReportingIterator(iterator, "Extracting tokens | category = " + categoryName);
      while (iterator.hasNext()) {

        IDBEntity doc = iterator.next().get();
        Map<String, Object> map = doc.toMap();
        String text = (String) map.get("w_filtered_text");
        String id = doc.getID();
        IDocument document = DictionaryDocumentBuilder.buildDocument(id, text, null);
      }

    */

  /*  String[] categories = new String[]{Categories.LITERATURE_CATEGORY, Categories.ALGEBRA_CATEGORY, Categories.COMPUTING_CATEGORY};

    for(String category : categories){

      Set<String> set = DictionaryUtils.loadCategoryDictionary(dbController, category);
      LoggingUtility.info("CATEGORY  = " + category);
      for(String token : set){
        LoggingUtility.info("T:" + token);

      }
    }



    if(true)
      return true;*/

        Collection<RawSemanticGroup> algebraGroups = loadCategoryGroups(Categories.ALGEBRA_CATEGORY);
        Collection<RawSemanticGroup> literatureGroups = loadCategoryGroups(Categories.LITERATURE_CATEGORY);
        Collection<RawSemanticGroup> computingGroups = loadCategoryGroups(Categories.COMPUTING_CATEGORY);

        LoggingUtility.info("CATEGORY = " + Categories.COMPUTING_CATEGORY + " COLLECTION SIZE = " + computingGroups.size());
        LoggingUtility.info("CATEGORY = " + Categories.ALGEBRA_CATEGORY + " COLLECTION SIZE = " + algebraGroups.size());
        LoggingUtility.info("CATEGORY = " + Categories.LITERATURE_CATEGORY + " COLLECTION SIZE = " + literatureGroups.size());

        Double total = (double) (computingGroups.size() + algebraGroups.size() + literatureGroups.size());

        List<SemanticGroupHashProvider> hashProviders = new LinkedList<SemanticGroupHashProvider>();
        //hashProviders.add(new BaseProvider());
        hashProviders.add(new TokenProvider());
        System.out.println("hashProviders.size(): " + hashProviders.size());
        //hashProviders.add(new RelationProvider());
        //hashProviders.add(new PartOfSpeechProvider());

        List<SemanticGroupFilterProvider> filterProviders = new LinkedList<SemanticGroupFilterProvider>();
        //filterProviders.add(new TrueProvider());


        filterProviders.add(new LengthProvider());


        for (SemanticGroupFilterProvider filterProvider : filterProviders) {
            for (SemanticGroupHashProvider hashProvider : hashProviders) {

                LoggingUtility.info("*** HASH PROVIDER = " + hashProvider.getClass().getSimpleName() + " FILTER PROVIDER = " + filterProvider.getClass().getSimpleName() + " ***");

                Map<String, RawGroup> algebraMap = createGroupMapping(algebraGroups, filterProvider, hashProvider);
                Map<String, RawGroup> literatureMap = createGroupMapping(literatureGroups, filterProvider, hashProvider);
                Map<String, RawGroup> computingMap = createGroupMapping(computingGroups, filterProvider, hashProvider);

                Map<String, Map<String, RawGroup>> maps = new HashMap<String, Map<String, RawGroup>>();
                maps.put(Categories.ALGEBRA_CATEGORY, algebraMap);
                maps.put(Categories.LITERATURE_CATEGORY, literatureMap);
                maps.put(Categories.COMPUTING_CATEGORY, computingMap);

                Map<String, RawCrossGroup> crossMap = new HashMap<String, RawCrossGroup>();
                Map<String, Map<String, RawGroup>> resultMaps = getSymmetricDiffMaps(maps, crossMap);

                int mapsSize = countMapsSize(maps);
                int resultMapsSize = countMapsSize(resultMaps);
                int crossMapSize = crossMap.size();
                int validCrossMapSize = validateCrossMap(crossMap, maps);

                LoggingUtility.info("DIST MAPS TOTAL = " + mapsSize);
                LoggingUtility.info("SYMM MAPS TOTAL = " + resultMapsSize);
                LoggingUtility.info("DISTINCT CROSS MAP SIZE = " + crossMapSize);
                LoggingUtility.info("VALID CROSS MAP SIZE = " + validCrossMapSize);
                LoggingUtility.info("VALIDATE CROSS MAP = " + (mapsSize - validCrossMapSize));
                printMaps(maps, resultMaps);

                //getPrettyGroups(maps);
                countCrossProbabilities(total, crossMap, maps);


                // getPrettyGroups(crossMap);
                Map<String, RawCrossGroup> bestGroups = getBestCrossProbabilities(crossMap);
                printCrossProbabilities(bestGroups);

       /* for(String thisKey : resultMaps.keySet()){
          for(String thatKey : resultMaps.keySet()){
            getMixedProbabilityMap(thisKey, thatKey, resultMaps);
          }
        }*/


            }

        }
        return true;
    }

    public void countKernel() {

    }

    public void validateMap(Map<String, Double> map) {
        double result = 0;
        for (String token : map.keySet()) {
            result += map.get(token);
        }

        LoggingUtility.info("VALIDATION = " + result);
        if (Math.abs(result - 1) > 1e-8) {

            LoggingUtility.info("VALIDATION FAILED!!!!");
        }
    }

    public void getMixedProbabilityMap(String key, String dictKey, Map<String, Map<String, RawGroup>> symmMaps) throws SQLException, ClassNotFoundException {

        Set<String> strictSet = DictionaryUtils.loadCategoryDictionary(dictKey);
        Map<String, Double> strictMap = calculateStrictMap(strictSet, dictKey);
        //LoggingUtility.info("STRICT SET SIZE = " + strictSet.size());
        //LoggingUtility.info("STRICT MAP SIZE = " + strictMap.keySet().size());

        Set<String> additionalSet = new HashSet<String>(strictSet);
        Map<String, RawGroup> groups = symmMaps.get(key);
        Map<String, Double> probabilityMap = new HashMap<String, Double>();


        for (RawGroup group : groups.values()) {
            for (RawSemanticRelation relation : group.group.getRelations()) {
                Relation rel = new Relation(relation.getTokenBaseRolePos());
                String token = rel.token.toLowerCase().trim();
                if (additionalSet.contains(token)) {
                    additionalSet.remove(token);
                    //LoggingUtility.info("REMOVED token = " + token);
                }
            }
        }


        Map<String, RawGroup> allGroups = new HashMap<String, RawGroup>(groups);
        RelationComparator comparator = new RelationComparator();
        for (String additional : additionalSet) {
            RawGroup group = new RawGroup();
            RawSemanticRelation relation = new RawSemanticRelation();
            relation.setTokenBaseRolePos(additional + "|*|*|*");
            RawSemanticGroup newGroup = new RawSemanticGroup(new RelationComparator());
            newGroup.addRelation(relation);
            group.group = newGroup;
            group.count = strictMap.get(additional);
            allGroups.put(additional, group);
        }

        LoggingUtility.info("SYMM GROUPS SIZE = " + groups.size());
        LoggingUtility.info("ADDITIONAL SET SIZE = " + additionalSet.size());
        LoggingUtility.info("ENLARGED GROUPS = " + groups.size());

        double total = 0;
        for (String groupKey : allGroups.keySet()) {
            RawGroup group = allGroups.get(groupKey);
            double dictCount = 0;
            double groupLength = 0;
            for (RawSemanticRelation relation : group.group.getRelations()) {
                Relation rel = new Relation(relation.getTokenBaseRolePos());
                String token = rel.token.toLowerCase().trim();
                if (strictMap.containsKey(token))
                    dictCount++;
                groupLength++;
            }

            double groupFreq = group.count;

            double groupResult = groupFreq * (1 + dictCount) / groupLength;
            total += groupResult;
            probabilityMap.put(groupKey, groupResult);
        }


        for (String probabilityKey : probabilityMap.keySet()) {
            probabilityMap.put(probabilityKey, probabilityMap.get(probabilityKey) / total);
        }

        validateMap(probabilityMap);

        double entropy = countEntropy(probabilityMap);
        LoggingUtility.info("CATEGORY = " + key + "DICT = " + dictKey + "MIXED ENTROPY  = " + entropy);
    }


    public Map<String, Double> calculateStrictMap(Set<String> strictSet, String key) throws SQLException, ClassNotFoundException {

        Map<String, Double> strictMap = new HashMap<String, Double>();

        String categoryCollection = Categories.buildCategoryHtmlCollectionName(key);
        IDBIterator iterator = commonDao.getDocumentsCollectionIterator(categoryCollection);//dbController.getDocumentsCollectionIterator(categoryCollection);

        while (iterator.hasNext()) {
            IDBEntity doc = iterator.next().get();
            Map<String, Object> map = doc.toMap();
            String id = doc.getID();


            String text = (String) map.get("w_filtered_text");
            IDocument document = null;
            DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
            try{
                document = documentBuilder.buildDocument(id, text, null);
            }catch (BuilderException e){
                e.printStackTrace();
                //TODO: add log
            }
           // IDocument document = DictionaryDocumentBuilder.buildDocument(id, text, null);
            List<SentenceTree> sentences = document.getSentences();
            List<String> tokens = DocumentUtils.getTokens(sentences);
            for (String token : tokens) {

                if (strictSet.contains(token)) {
                    if (!strictMap.containsKey(token)) {
                        strictMap.put(token, 1d);
                    } else {
                        double value = strictMap.get(token);
                        strictMap.put(token, value + 1);
                    }
                }
            }
            //tokens.put(id, documentTokens);
        }
        return strictMap;
    }

    public class RelationComparator implements Comparator<RawSemanticRelation> {
        public int compare(RawSemanticRelation o1, RawSemanticRelation o2) {
            return o1.getIndex() - o2.getIndex();
        }
    }

    public double countEntropy(Map<String, Double> probabilityMap) {
        double result = 0;
        for (String token : probabilityMap.keySet()) {
            double probability = probabilityMap.get(token);
            result += probability * log2(probability);
        }
        return -result;
    }

    private static double log2(double x) {
        // Math.log is base e, natural log, ln
        return Math.log(x) / Math.log(2);
    }


    private Map<String, RawCrossGroup> getBestCrossProbabilities(Map<String, RawCrossGroup> crossMap) {
        Map<String, RawCrossGroup> bestMap = new HashMap<String, RawCrossGroup>();
        for (String crossKey : crossMap.keySet()) {
            Map<String, Double> probabilityMap = crossMap.get(crossKey).probabilityMap;
            for (String thisKey : probabilityMap.keySet()) {
                if (containsZero(probabilityMap))
                    continue;
                for (String otherKey : probabilityMap.keySet()) {
                    Double thisValue = probabilityMap.get(thisKey);
                    Double otherValue = probabilityMap.get(otherKey);
                    if (otherValue != 0 && thisValue / otherValue > 10) {
                        if (!bestMap.containsKey(crossKey))
                            bestMap.put(crossKey, crossMap.get(crossKey));
                    }
                }
            }
        }
        return bestMap;
    }

    public boolean containsZero(Map<String, Double> probabilityMap) {
        for (String checkKey : probabilityMap.keySet()) {
            if (probabilityMap.get(checkKey) == 0)
                return true;
        }
        return false;
    }

//  private void getPrettyGroups(Map<String, Map<String, RawGroup>> maps) {
//
//    for (String mapKey : maps.keySet()) {
//      Map<String, List<RawGroup>> frequentGroups = new HashMap<String, List<RawGroup>>();
//
//      for (RawGroup group : maps.get(mapKey).values()) {
//
//
//        if (group.count > 2) {
//          String groupString = group.count.toString();
//          if (frequentGroups.get(groupString) == null)
//            frequentGroups.put(groupString, new LinkedList<RawGroup>());
//          frequentGroups.get(groupString).add(group);
//        }
//      }
//
//      for(String count : frequentGroups.keySet()){
//        LoggingUtility.info("IN CATEGORY = " + mapKey + "GROUPS WITH COUNT = " + count);
//        for(RawGroup group : frequentGroups.get(count))
//          LoggingUtility.info(group.toString());
//      }
//    }
//  }


    private void printCrossProbabilities(Map<String, RawCrossGroup> crossMap) {
        LoggingUtility.info("MAP SIZE = " + crossMap.size());
        for (RawCrossGroup crossGroup : crossMap.values()) {
            LoggingUtility.info(crossGroup.toString());
        }
    }


    private void countCrossProbabilities(Double total, Map<String, RawCrossGroup> crossMap, Map<String, Map<String, RawGroup>> maps) {
        for (String crossKey : crossMap.keySet()) {
            Map<String, Double> probabilityMap = new HashMap<String, Double>();
            for (String mapKey : maps.keySet()) {
                Map<String, RawGroup> map = maps.get(mapKey);
                if (map.containsKey(crossKey))
                    probabilityMap.put(mapKey, map.get(crossKey).count);
                    //probabilityMap.put(mapKey, map.get(crossKey).count / total);
                else
                    probabilityMap.put(mapKey, 0d);
            }
            crossMap.get(crossKey).probabilityMap = probabilityMap;
        }
    }


    private int validateCrossMap(Map<String, RawCrossGroup> crossMap, Map<String, Map<String, RawGroup>> maps) {
        int total = 0;
        for (String mapKey : maps.keySet()) {
            for (String crossKey : crossMap.keySet()) {
                if (maps.get(mapKey).containsKey(crossKey))
                    total++;
            }
        }
        return total;
    }

    private int countMapsSize(Map<String, Map<String, RawGroup>> maps) {
        int mapsSize = 0;
        for (String key : maps.keySet())
            mapsSize += maps.get(key).size();
        return mapsSize;
    }

    private void printMaps(Map<String, Map<String, RawGroup>> maps, Map<String, Map<String, RawGroup>> resultMaps) {
        for (String key : maps.keySet()) {
            LoggingUtility.info("CATEGORY = " + key + " DIST MAP SIZE = " + maps.get(key).size());
            LoggingUtility.info("CATEGORY = " + key + " SYMM MAP SIZE = " + resultMaps.get(key).size());


            for (RawGroup group : maps.get(key).values())
                LoggingUtility.info(group.toString());
        }
    }

    private Map<String, Map<String, RawGroup>> getSymmetricDiffMaps(Map<String, Map<String, RawGroup>> maps, Map<String, RawCrossGroup> crossMap) {
        Map<String, Map<String, RawGroup>> resultMaps = new HashMap<String, Map<String, RawGroup>>();
        Set<String> categoryKeys = maps.keySet();
        for (String thisKey : categoryKeys) {
            Map<String, RawGroup> thisMap = maps.get(thisKey);
            Map<String, RawGroup> thisResult = new HashMap<String, RawGroup>();
            for (String thisValueKey : thisMap.keySet()) {
                if (!containsInOtherMaps(maps, thisKey, thisValueKey))
                    thisResult.put(thisValueKey, thisMap.get(thisValueKey));
                else {
                    if (!crossMap.containsKey(thisValueKey)) {
                        RawCrossGroup crossGroup = new RawCrossGroup();
                        crossGroup.group = thisMap.get(thisValueKey).group;
                        crossMap.put(thisValueKey, crossGroup);
                    }
                }
            }
            resultMaps.put(thisKey, thisResult);
        }
        return resultMaps;
    }

    private boolean containsInOtherMaps(Map<String, Map<String, RawGroup>> maps, String thisKey, String thisValueKey) {
        Set<String> categoryKeys = maps.keySet();
        for (String otherKey : categoryKeys) {
            if (!thisKey.equals(otherKey)) {
                Map<String, RawGroup> otherMap = maps.get(otherKey);
                if (otherMap.containsKey(thisValueKey))
                    return true;
            }
        }
        return false;

    }

    /**
     * Create
     * @param groups
     * @param filterProvider
     * @param hashProvider
     * @return
     */
    private Map<String, RawGroup> createGroupMapping(Collection<RawSemanticGroup> groups,
                                                     SemanticGroupFilterProvider filterProvider, SemanticGroupHashProvider hashProvider) {

        Map<String, RawGroup> map = new HashMap<String, RawGroup>();
        for (RawSemanticGroup group : groups) {
            if (filterProvider.isValid(group)) {
                String hash = hashProvider.getHashString(group);
                if (!map.containsKey(hash)) {
                    RawGroup counter = new RawGroup();
                    counter.count = 1d;
                    counter.group = group;
                    map.put(hash, counter);
                } else
                    map.get(hash).count++;
            }
        }
        return map;
    }


    private Collection<RawSemanticGroup> loadCategoryGroups(String categoryName) {
        LoggingUtility.info("RETRIEVING GROUPS FOR CATEGORY = " + categoryName);
        String groupTable = "semantic_groups_" + categoryName;
        Collection<RawSemanticGroup> groups = semanticGroupDao.getSemanticGroups(groupTable);//dbController.getSemanticGroups(groupTable);
        return groups;
    }

    public interface SemanticGroupFilterProvider {
        public boolean isValid(RawSemanticGroup g);
    }

    public class TrueProvider implements SemanticGroupFilterProvider {

        public boolean isValid(RawSemanticGroup g) {
            return true;
        }
    }

    public class LengthProvider implements SemanticGroupFilterProvider {

        public boolean isValid(RawSemanticGroup g) {
            int size = g.getRelations().size();
            return size > 2 && size < 7;
        }
    }

    public interface SemanticGroupHashProvider {
        public String getHashString(RawSemanticGroup g);
    }

    public class TokenProvider implements SemanticGroupHashProvider {

        public String getHashString(RawSemanticGroup g) {
            StringBuilder stringForHash = new StringBuilder();
            for (RawSemanticRelation r : g.getRelations()) {
                Relation relation = new Relation(r.getTokenBaseRolePos());
                stringForHash.append(relation.token.toLowerCase().trim()).append("|");
            }
            return stringForHash.toString();
        }
    }

    public class BaseProvider implements SemanticGroupHashProvider {

        public String getHashString(RawSemanticGroup g) {
            StringBuilder stringForHash = new StringBuilder();
            for (RawSemanticRelation r : g.getRelations()) {
                Relation relation = new Relation(r.getTokenBaseRolePos());
                stringForHash.append(relation.base.toLowerCase().trim()).append("|");
            }
            return stringForHash.toString();
        }
    }

    public class RelationProvider implements SemanticGroupHashProvider {

        public String getHashString(RawSemanticGroup g) {
            StringBuilder stringForHash = new StringBuilder();
            for (RawSemanticRelation r : g.getRelations()) {
                Relation relation = new Relation(r.getTokenBaseRolePos());
                stringForHash.append(relation.token.toLowerCase().trim()).append("|").append(relation.role.toLowerCase().trim()).append("|");
            }
            return stringForHash.toString();
        }
    }

    public class PartOfSpeechProvider implements SemanticGroupHashProvider {

        public String getHashString(RawSemanticGroup g) {
            StringBuilder stringForHash = new StringBuilder();
            for (RawSemanticRelation r : g.getRelations()) {
                Relation relation = new Relation(r.getTokenBaseRolePos());
                stringForHash.append(relation.token.toLowerCase().trim()).append("|");
                stringForHash.append(relation.role.toLowerCase().trim()).append("|");
                stringForHash.append(relation.pos.toLowerCase().trim()).append("|");
            }
            return stringForHash.toString();
        }
    }


    private class Relation {
        public String token;
        public String base;
        public String role;
        public String pos;

        private Relation(String tokenBaseRolePos) {
            String[] split = tokenBaseRolePos.split("\\|");
            token = split[0];
            base = split[1];
            role = split[2];
            pos = split[3];
        }
    }

    private class RawGroup {
        public RawSemanticGroup group;
        public Double count;

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("G:");
            for (RawSemanticRelation r : group.getRelations()) {
                Relation rel = new Relation(r.getTokenBaseRolePos());
                builder.append(rel.token).append(" ");
            }
            return builder.toString();
        }
    }

    private class RawCrossGroup {
        public RawSemanticGroup group;
        public Map<String, Double> probabilityMap;


        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append(" P:");
            for (String mapKey : probabilityMap.keySet()) {
                builder.append(mapKey.substring(0, 1).toUpperCase()).append(" = " + format(probabilityMap.get(mapKey), 8) + "; ");
            }
            builder.append("G:");
            for (RawSemanticRelation r : group.getRelations()) {
                Relation rel = new Relation(r.getTokenBaseRolePos());
                builder.append(rel.token).append(" ");
            }
            return builder.toString();
        }
    }


    public static String format(int w, double d) {
        String r = String.format("%01." + w + "G", d);
        if (r.length() > w) {
            r = String.format("%01." + (w + w - r.length()) + "G", d);
        }

        return r;
    }

    private String format(double d, int length) {
        return format(length, d);
    }

}
