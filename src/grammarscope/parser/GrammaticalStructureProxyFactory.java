package grammarscope.parser;

import java.util.Collection;

import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.Filter;

public class GrammaticalStructureProxyFactory extends edu.stanford.nlp.trees.GrammaticalStructureFactory
{
	static final String theGSClass = GrammaticalStructureProxy.class.getName();

	/**
	 * Copy : superclass member is private
	 */
	protected Filter<String> puncFilter2;

	/**
	 * Copy : superclass member is private
	 */
	protected HeadFinder hf2;

	protected Collection<GrammaticalRelation> theRelations;

	public GrammaticalStructureProxyFactory(final Collection<GrammaticalRelation> theseRelations, final Filter<String> puncFilter, final HeadFinder hf)
	{
		super(GrammaticalStructureProxyFactory.theGSClass, puncFilter, hf);
		this.theRelations = theseRelations;
		this.puncFilter2 = puncFilter;
		this.hf2 = hf;
	}

	@Override
	public GrammaticalStructure newGrammaticalStructure(final Tree t)
	{
		if (this.puncFilter2 == null)
		{
			this.puncFilter2 = new PennTreebankLanguagePack().punctuationWordRejectFilter();
		}
		if (this.hf2 == null)
		{
			this.hf2 = new SemanticHeadFinder(true);
		}
		return new GrammaticalStructureProxy(t, this.theRelations, this.puncFilter2, this.hf2);
	}
}
