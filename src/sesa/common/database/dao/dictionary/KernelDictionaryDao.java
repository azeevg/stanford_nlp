package sesa.common.database.dao.dictionary;

import sesa.common.database.dao.CommonDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class for extract terms from db, that are kernel
 */
public class KernelDictionaryDao extends CommonDao {
    private String collectionName = "all_cats_idf";

    /**
     * In constructor executed connection to database
     */
    public KernelDictionaryDao() throws SQLException, ClassNotFoundException {
    }

    public String extractDictionary() {
        String result = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "`");
            ps.execute();
            resultSet = ps.getResultSet();
            while (resultSet.next()) {
                result = (String) resultSet.getObject("data");
            }
            //  resultList.add(new DBEntity((String) resultSet.getObject("id"), (String) resultSet.getObject("data")));
        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
            result = null;
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return result;
    }
}
