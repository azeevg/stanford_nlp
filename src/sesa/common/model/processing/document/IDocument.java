package sesa.common.model.processing.document;

import sesa.tools.math.Vector;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.dictionary.Dictionary;

import java.util.List;

public interface IDocument {
    Vector toVector(Dictionary dictionary);
    List<SentenceTree> getSentences();
    String getId();
    String getTitle();
}
