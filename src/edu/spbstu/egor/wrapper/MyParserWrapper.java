package edu.spbstu.egor.wrapper;

/**
 * Created by Egor Gorbunov on 18.04.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */


import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.search.AhoTrie;
import edu.stanford.nlp.io.EncodingPrintWriter;
import edu.stanford.nlp.ling.HasContext;
import edu.stanford.nlp.ling.HasTag;
import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.util.Index;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Class for retrieving parse trees only. Has few improvements of stanford code.
 */
public class MyParserWrapper extends ExhaustivePCFGParserNoOutsides {

    @Override
    protected void doInsideScores() {
        final List<ParserConstraint> constraints = getConstraints();
        for (int diff = 2; diff <= length; diff++) {
            // usually stop one short because boundary symbol only combines
            // with whole sentence span
            for (int start = 0; start < ((diff == length) ? 1: length - diff); start++) {
                if (spillGuts) {
                    tick("Binaries for span " + diff + " start " + start + " ...");
                }
                int end = start + diff;

                if (constraints != null) {
                    boolean skip = false;
                    for (ParserConstraint c : constraints) {
                        if ((start > c.start && start < c.end && end > c.end) || (end > c.start && end < c.end && start < c.start)) {
                            skip = true;
                            break;
                        }
                    }
                    if (skip) {
                        continue;
                    }
                }

                // 2011-11-26 jdk1.6: caching/hoisting a bunch of variables gives you about 15% speed up!
                // caching this saves a bit of time in the inner loop, maybe 1.8%
                int[] narrowRExtent_start = narrowRExtent[start];
                // caching this saved 2% in the inner loop
                int[] wideRExtent_start = wideRExtent[start];
                int[] narrowLExtent_end = narrowLExtent[end];
                int[] wideLExtent_end = wideLExtent[end];
                float[][] iScore_start = iScore[start];
                float[] iScore_start_end = iScore_start[end];

                for (int leftState = 0; leftState < numStates; leftState++) {
                    int narrowR = narrowRExtent_start[leftState];
                    boolean iPossibleL = (narrowR < end); // can this left constituent leave space for a right constituent?
                    if (!iPossibleL) {
                        continue;
                    }
                    BinaryRule[] leftRules = bg.splitRulesWithLC(leftState);
                    //      if (spillGuts) System.out.println("Found " + leftRules.length + " left rules for state " + stateIndex.get(leftState));
                    for (BinaryRule rule : leftRules) {
                        int rightChild = rule.rightChild;
                        int narrowL = narrowLExtent_end[rightChild];
                        boolean iPossibleR = (narrowL >= narrowR); // can this right constituent fit next to the left constituent?
                        if (!iPossibleR) {
                            continue;
                        }
                        int min2 = wideLExtent_end[rightChild];
                        int min = (narrowR > min2 ? narrowR : min2);
                        // Erik Frey 2009-12-17: This is unnecessary: narrowR is <= narrowL (established in previous check) and wideLExtent[e][r] is always <= narrowLExtent[e][r] by design, so the check will never evaluate true.
                        // if (min > narrowL) { // can this right constituent stretch far enough to reach the left constituent?
                        //   continue;
                        // }
                        int max1 = wideRExtent_start[leftState];
                        int max = (max1 < narrowL ? max1 : narrowL);
                        if (min > max) { // can this left constituent stretch far enough to reach the right constituent?
                            continue;
                        }
                        float pS = rule.score;
                        int parentState = rule.parent;
                        float oldIScore = iScore_start_end[parentState];
                        float bestIScore = oldIScore;
                        boolean foundBetter;  // always set below for this rule
                        //System.out.println("Min "+min+" max "+max+" start "+start+" end "+end);

                        if ( ! op.testOptions.lengthNormalization) {
                            // find the split that can use this rule to make the max score
                            for (int split = min; split <= max; split++) {

                                if (constraints != null) {
                                    boolean skip = false;
                                    for (ParserConstraint c : constraints) {
                                        if (((start < c.start && end >= c.end) || (start <= c.start && end > c.end)) && split > c.start && split < c.end) {
                                            skip = true;
                                            break;
                                        }
                                        if ((start == c.start && split == c.end)) {
                                            String tag = stateIndex.get(leftState);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                        if ((split == c.start && end == c.end)) {
                                            String tag = stateIndex.get(rightChild);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (skip) {
                                        continue;
                                    }
                                }

                                float lS = iScore_start[split][leftState];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightChild];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                if (spillGuts) { System.err.println("Rule " + rule + " over [" + start + "," + end + ") has log score " + tot + " from L[" + stateIndex.get(leftState) + "=" + leftState + "] = "+ lS  + " R[" + stateIndex.get(rightChild) + "=" + rightChild + "] =  " + rS); }
                                if (tot > bestIScore) {
                                    bestIScore = tot;
                                }
                            } // for split point
                            foundBetter = bestIScore > oldIScore;
                        } else {
                            // find split that uses this rule to make the max *length normalized* score
                            int bestWordsInSpan = wordsInSpan[start][end][parentState];
                            float oldNormIScore = oldIScore / bestWordsInSpan;
                            float bestNormIScore = oldNormIScore;

                            for (int split = min; split <= max; split++) {
                                float lS = iScore_start[split][leftState];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightChild];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                int newWordsInSpan = wordsInSpan[start][split][leftState] + wordsInSpan[split][end][rightChild];
                                float normTot = tot / newWordsInSpan;
                                if (normTot > bestNormIScore) {
                                    bestIScore = tot;
                                    bestNormIScore = normTot;
                                    bestWordsInSpan = newWordsInSpan;
                                }
                            } // for split point
                            foundBetter = bestNormIScore > oldNormIScore;
                            if (foundBetter) {
                                wordsInSpan[start][end][parentState] = bestWordsInSpan;
                            }
                        } // fi op.testOptions.lengthNormalization
                        if (foundBetter) { // this way of making "parentState" is better than previous
                            iScore_start_end[parentState] = bestIScore;

                            if (spillGuts) System.err.println("Could build " + stateIndex.get(parentState) + " from " + start + " to " + end + " score " + bestIScore);
                            if (oldIScore == Float.NEGATIVE_INFINITY) {
                                if (start > narrowLExtent_end[parentState]) {
                                    narrowLExtent_end[parentState] = start; // EGOR_INFO: why not split!? There is no any difference between wideLExtent and narrowLExtent .. ???
                                    wideLExtent_end[parentState] = start;
                                } else {
                                    if (start < wideLExtent_end[parentState]) {
                                        wideLExtent_end[parentState] = start;
                                    }
                                }
                                if (end < narrowRExtent_start[parentState]) {
                                    narrowRExtent_start[parentState] = end;
                                    wideRExtent_start[parentState] = end;
                                } else {
                                    if (end > wideRExtent_start[parentState]) {
                                        wideRExtent_start[parentState] = end;
                                    }
                                }
                            }
                        } // end if foundBetter
                    } // end for leftRules
                } // end for leftState
                // do right restricted rules
                for (int rightState = 0; rightState < numStates; rightState++) {
                    int narrowL = narrowLExtent_end[rightState];
                    boolean iPossibleR = (narrowL > start);
                    if (!iPossibleR) {
                        continue;
                    }
                    BinaryRule[] rightRules = bg.splitRulesWithRC(rightState);
                    //      if (spillGuts) System.out.println("Found " + rightRules.length + " right rules for state " + stateIndex.get(rightState));
                    for (BinaryRule rule : rightRules) {
                        //      if (spillGuts) System.out.println("Considering rule for " + start + " to " + end + ": " + rightRules[i]);

                        int leftChild = rule.leftChild;
                        int narrowR = narrowRExtent_start[leftChild];
                        boolean iPossibleL = (narrowR <= narrowL);
                        if (!iPossibleL) {
                            continue;
                        }
                        int min2 = wideLExtent_end[rightState];
                        int min = (narrowR > min2 ? narrowR : min2);
                        // Erik Frey 2009-12-17: This is unnecessary: narrowR is <= narrowL (established in previous check) and wideLExtent[e][r] is always <= narrowLExtent[e][r] by design, so the check will never evaluate true.
                        // if (min > narrowL) {
                        //   continue;
                        // }
                        int max1 = wideRExtent_start[leftChild];
                        int max = (max1 < narrowL ? max1 : narrowL);
                        if (min > max) {
                            continue;
                        }
                        float pS = rule.score;
                        int parentState = rule.parent;
                        float oldIScore = iScore_start_end[parentState];
                        float bestIScore = oldIScore;
                        boolean foundBetter; // always initialized below
                        //System.out.println("Start "+start+" end "+end+" min "+min+" max "+max);
                        if ( ! op.testOptions.lengthNormalization) {
                            // find the split that can use this rule to make the max score
                            for (int split = min; split <= max; split++) {

                                if (constraints != null) {
                                    boolean skip = false;
                                    for (ParserConstraint c : constraints) {
                                        if (((start < c.start && end >= c.end) || (start <= c.start && end > c.end)) && split > c.start && split < c.end) {
                                            skip = true;
                                            break;
                                        }
                                        if ((start == c.start && split == c.end)) {
                                            String tag = stateIndex.get(leftChild);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                //if (!tag.startsWith(c.state+"^")) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                        if ((split == c.start && end == c.end)) {
                                            String tag = stateIndex.get(rightState);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                //if (!tag.startsWith(c.state+"^")) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (skip) {
                                        continue;
                                    }
                                }

                                float lS = iScore_start[split][leftChild];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightState];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                if (tot > bestIScore) {
                                    bestIScore = tot;
                                }
                            } // end for split // EGOR_INFO: Why we do not store any information about split point??????????????
                            foundBetter = bestIScore > oldIScore;
                        } else {
                            // find split that uses this rule to make the max *length normalized* score
                            int bestWordsInSpan = wordsInSpan[start][end][parentState];
                            float oldNormIScore = oldIScore / bestWordsInSpan;
                            float bestNormIScore = oldNormIScore;
                            for (int split = min; split <= max; split++) {
                                float lS = iScore_start[split][leftChild];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightState];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                int newWordsInSpan = wordsInSpan[start][split][leftChild] + wordsInSpan[split][end][rightState];
                                float normTot = tot / newWordsInSpan;
                                if (normTot > bestNormIScore) {
                                    bestIScore = tot;
                                    bestNormIScore = normTot;
                                    bestWordsInSpan = newWordsInSpan;
                                }
                            } // end for split
                            foundBetter = bestNormIScore > oldNormIScore;
                            if (foundBetter) {
                                wordsInSpan[start][end][parentState] = bestWordsInSpan;
                            }
                        } // end if lengthNormalization
                        if (foundBetter) { // this way of making "parentState" is better than previous
                            iScore_start_end[parentState] = bestIScore;
                            if (spillGuts) System.err.println("Could build " + stateIndex.get(parentState) + " from " + start + " to " + end + " with score " + bestIScore);
                            if (oldIScore == Float.NEGATIVE_INFINITY) {
                                if (start > narrowLExtent_end[parentState]) {
                                    narrowLExtent_end[parentState] = start;
                                    wideLExtent_end[parentState] = start;
                                } else {
                                    if (start < wideLExtent_end[parentState]) {
                                        wideLExtent_end[parentState] = start;
                                    }
                                }
                                if (end < narrowRExtent_start[parentState]) {
                                    narrowRExtent_start[parentState] = end;
                                    wideRExtent_start[parentState] = end;
                                } else {
                                    if (end > wideRExtent_start[parentState]) {
                                        wideRExtent_start[parentState] = end;
                                    }
                                }
                            }
                        } // end if foundBetter
                    } // for rightRules
                } // for rightState
                if (spillGuts) {
                    tick("Unaries for span " + diff + "...");
                }
                // do unary rules -- one could promote this loop and put start inside
                for (int state = 0; state < numStates; state++) {
                    float iS = iScore_start_end[state];
                    if (iS == Float.NEGATIVE_INFINITY) {
                        continue;
                    }

                    UnaryRule[] unaries = ug.closedRulesByChild(state);
                    for (UnaryRule ur : unaries) {

                        if (constraints != null) {
                            boolean skip = false;
                            for (ParserConstraint c : constraints) {
                                if ((start == c.start && end == c.end)) {
                                    String tag = stateIndex.get(ur.parent);
                                    Matcher m = c.state.matcher(tag);
                                    if (!m.matches()) {
                                        //if (!tag.startsWith(c.state+"^")) {
                                        skip = true;
                                        break;
                                    }
                                }
                            }
                            if (skip) {
                                continue;
                            }
                        }

                        int parentState = ur.parent;
                        float pS = ur.score;
                        float tot = iS + pS;
                        float cur = iScore_start_end[parentState];
                        boolean foundBetter;  // always set below
                        if (op.testOptions.lengthNormalization) {
                            int totWordsInSpan = wordsInSpan[start][end][state];
                            float normTot = tot / totWordsInSpan;
                            int curWordsInSpan = wordsInSpan[start][end][parentState];
                            float normCur = cur / curWordsInSpan;
                            foundBetter = normTot > normCur;
                            if (foundBetter) {
                                wordsInSpan[start][end][parentState] = wordsInSpan[start][end][state];
                            }
                        } else {
                            foundBetter = (tot > cur);
                        }
                        if (foundBetter) {
                            if (spillGuts) System.err.println("Could build " + stateIndex.get(parentState) + " from " + start + " to " + end + " with score " + tot);
                            iScore_start_end[parentState] = tot;
                            if (cur == Float.NEGATIVE_INFINITY) {
                                if (start > narrowLExtent_end[parentState]) {
                                    narrowLExtent_end[parentState] = start;
                                    wideLExtent_end[parentState] = start;
                                } else {
                                    if (start < wideLExtent_end[parentState]) {
                                        wideLExtent_end[parentState] = start;
                                    }
                                }
                                if (end < narrowRExtent_start[parentState]) {
                                    narrowRExtent_start[parentState] = end;
                                    wideRExtent_start[parentState] = end;
                                } else {
                                    if (end > wideRExtent_start[parentState]) {
                                        wideRExtent_start[parentState] = end;
                                    }
                                }
                            }

                            if (parentState < state) {
                                state = parentState;
                            }
                        } // end if foundBetter
                    } // for UnaryRule r
                } // for unary rules
            } // for start
        } // for diff (i.e., span)
    } // end doInsideScores()

    @Override
    protected void initializeChart(List sentence) {
        int boundary = wordIndex.indexOf(Lexicon.BOUNDARY);

        for (int start = 0; start + 1 <= length; start++) {
            int word = words[start];
            int end = start + 1;
            Arrays.fill(tags[start], false);

            //Force tags
            String trueTagStr = null;
            if (sentence.get(start) instanceof HasTag) {
                trueTagStr = ((HasTag) sentence.get(start)).tag();
                if ("".equals(trueTagStr)) {
                    trueTagStr = null;
                }
            }

            //Word context (e.g., morphosyntactic info)
            String wordContextStr = null;
            if(sentence.get(start) instanceof HasContext) {
                wordContextStr = ((HasContext) sentence.get(start)).originalText();
                if("".equals(wordContextStr))
                    wordContextStr = null;
            }

            boolean assignedSomeTag = false;

            if ( ! floodTags || word == boundary) {
                // in this case we generate the taggings in the lexicon,
                // which may itself be tagging flexibly or using a strict lexicon.
                if (dumpTagging) {
                    EncodingPrintWriter.err.println("Normal tagging " + wordIndex.get(word) + " [" + word + "]", "UTF-8");
                }
                for (Iterator<IntTaggedWord> taggingI = lex.ruleIteratorByWord(word, start, wordContextStr); taggingI.hasNext(); ) {
                    IntTaggedWord tagging = taggingI.next();
                    int state = stateIndex.indexOf(tagIndex.get(tagging.tag));
                    if (trueTagStr != null) { // if word was supplied with a POS tag, skip all taggings not basicCategory() compatible with supplied tag.
                        if ((!op.testOptions.forceTagBeginnings && !tlp.basicCategory(tagging.tagString(tagIndex)).equals(trueTagStr)) ||
                                (op.testOptions.forceTagBeginnings &&  !tagging.tagString(tagIndex).startsWith(trueTagStr))) {
                            if (dumpTagging) {
                                EncodingPrintWriter.err.println("  Skipping " + tagging + " as it doesn't match trueTagStr: " + trueTagStr, "UTF-8");
                            }
                            continue;
                        }
                    }
                    float lexScore = lex.score(tagging, start, wordIndex.get(tagging.word)); // score the cell according to P(word|tag) in the lexicon
                    if (lexScore > Float.NEGATIVE_INFINITY) {
                        assignedSomeTag = true;
                        iScore[start][end][state] = lexScore;
                        narrowRExtent[start][state] = end; // EGOR_INFO: why not start but end??? Is it [..) or what? Yes...
                        narrowLExtent[end][state] = start;
                        wideRExtent[start][state] = end; // EGOR_INFO: why not start but end???
                        wideLExtent[end][state] = start;
                    }
                    int tag = tagging.tag;
                    tags[start][tag] = true;
                    if (dumpTagging) {
                        EncodingPrintWriter.err.println("Word pos " + start + " tagging " + tagging + " score " + iScore[start][end][state] + " [state " + stateIndex.get(state) + " = " + state + "]", "UTF-8");
                    }
                }
            } // end if ( ! floodTags || word == boundary)

            if ( ! assignedSomeTag) {
                if (dumpTagging) {
                    EncodingPrintWriter.err.println("Forced FlexiTagging " + wordIndex.get(word), "UTF-8");
                }
                for (int state = 0; state < numStates; state++) {
                    if (isTag[state] && iScore[start][end][state] == Float.NEGATIVE_INFINITY) {
                        if (trueTagStr != null) {
                            String tagString = stateIndex.get(state);
                            if ( ! tlp.basicCategory(tagString).equals(trueTagStr)) {
                                continue;
                            }
                        }

                        float lexScore = lex.score(new IntTaggedWord(word, tagIndex.indexOf(stateIndex.get(state))), start, wordIndex.get(word));
                        if (lexScore > Float.NEGATIVE_INFINITY) {
                            iScore[start][end][state] = lexScore;
                            narrowRExtent[start][state] = end;
                            narrowLExtent[end][state] = start;
                            wideRExtent[start][state] = end;
                            wideLExtent[end][state] = start;
                        }
                        if (dumpTagging) {
                            EncodingPrintWriter.err.println("Word pos " + start + " tagging " + (new IntTaggedWord(word, tagIndex.indexOf(stateIndex.get(state)))) + " score " + iScore[start][start + 1][state]  + " [state " + stateIndex.get(state) + " = " + state + "]", "UTF-8");
                        }
                    }
                }
            } // end if ! assignedSomeTag

            // tag multi-counting
            if (op.dcTags) {
                for (int state = 0; state < numStates; state++) {
                    if (isTag[state]) {
                        iScore[start][end][state] *= (1.0 + op.testOptions.depWeight);
                    }
                }
            }

            if (floodTags && (!op.testOptions.noRecoveryTagging) && ! (word == boundary)) {
                if (dumpTagging) {
                    EncodingPrintWriter.err.println("Flooding tags for " + wordIndex.get(word), "UTF-8");
                }
                for (int state = 0; state < numStates; state++) {
                    if (isTag[state] && iScore[start][end][state] == Float.NEGATIVE_INFINITY) {
                        iScore[start][end][state] = -1000.0f;
                        narrowRExtent[start][state] = end; // EGOR_INFO: why not start????
                        narrowLExtent[end][state] = start;
                        wideRExtent[start][state] = end;
                        wideLExtent[end][state] = start;
                    }
                }
            }

            // Apply unary rules in diagonal cells of chart
            if (spillGuts) {
                tick("Terminal Unary...");
            }
            // EGOR: I think there is something uncertain below...
            for (int state = 0; state < numStates; state++) {
                float iS = iScore[start][end][state]; // EGOR_INFO: here we also may get Parent nodes, not only tags...
                if (iS == Float.NEGATIVE_INFINITY) {
                    continue;
                }
                UnaryRule[] unaries = ug.closedRulesByChild(state);
                int backState = state;
                for (UnaryRule ur : unaries) {
                    int parentState = ur.parent;
                    float pS = ur.score;
                    float tot = iS + pS;
                    if (tot > iScore[start][end][parentState]) {
                        iScore[start][end][parentState] = tot;
                        narrowRExtent[start][parentState] = end;
                        narrowLExtent[end][parentState] = start;
                        wideRExtent[start][parentState] = end;
                        wideLExtent[end][parentState] = start;

                        if (parentState < backState) {
                            backState = parentState;
                        }
                    }
                }
                state = backState;
            }
            if (spillGuts) {
                tick("Next word...");
            }
        }
    }

    public float[][][] getScores() {
        return iScore;
    }

    public MyParserWrapper(BinaryGrammar bg, UnaryGrammar ug, Lexicon lex, Options op, Index<String> stateIndex, Index<String> wordIndex, Index<String> tagIndex) {
        super(bg, ug, lex, op, stateIndex, wordIndex, tagIndex);
    }
}