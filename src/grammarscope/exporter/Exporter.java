package grammarscope.exporter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalRelation.Language;
import grammarscope.parser.DefaultMutableGrammaticalRelations;
import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.RelationModel;
import grammarscope.parser.RelationProcessor;

/**
 * Export relation model to a set of grammatical relations
 * 
 * @author Bernard Bou <bbou@ac-toulouse.fr>
 */
public class Exporter
{
	/**
	 * Export source relation model by creating new Grammatical relations on the fly
	 * 
	 * @param thisModel
	 *            source relation model
	 * @param thisLanguage
	 *            target language
	 * @return collection of grammatical relations
	 */
	static public Collection<GrammaticalRelation> export(final RelationModel thisModel, final Language thisLanguage)
	{
		final Map<MutableGrammaticalRelation, GrammaticalRelation> thisMap = new HashMap<MutableGrammaticalRelation, GrammaticalRelation>();
		thisModel.theRoot.walkTree(new RelationProcessor()
		{
			// relies on top-down tree walk


			public void process(final MutableGrammaticalRelation thatRelation)
			{
				GrammaticalRelation thisRelation = null;
				if (thatRelation.getId().equals("gov"))
				{
					thisRelation = GrammaticalRelation.GOVERNOR;
				}
				else if (thatRelation.getId().equals("dep"))
				{
					thisRelation = GrammaticalRelation.DEPENDENT;
				}
				else if (thatRelation.getId().equals("KILL"))
				{
					thisRelation = GrammaticalRelation.KILL;
				}
				else
				{
					thisRelation = new GrammaticalRelation(thisLanguage, thatRelation.getShortName(), thatRelation.getLongName(), AnnotationFactory.getAnnotation(thatRelation, thisMap), thisMap.get(thatRelation.getParent()), thatRelation
							.getSourcePattern() == null ? null : thatRelation.getSourcePattern().toString(), thatRelation.getTargetPatternStrings(), thatRelation.getSpecific());
				}
				thisMap.put(thatRelation, thisRelation);
			}
		});
		// System.out.println("exported "+thisMap.values());
		// System.out.println("done: exporting");
		return thisMap.values();
	}

	/**
	 * Dump maps (requires tweaked version of class)
	 */
	static public void dump()
	{
		System.out.println("vvvvvvvvvvvvvvvvvvvvvv");
		System.out.println("a2r=" + GrammaticalRelation.getAnnotationsToRelations());
		System.out.println("r2a=" + GrammaticalRelation.getRelationsToAnnotations());
		System.out.println("l2[s2r]=" + GrammaticalRelation.getStringsToRelations());
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^");
	}

	static public void main(final String[] args)
	{
		final GrammaticalRelation c = EnglishGrammaticalRelations.PREDICATE;
		System.out.println("using " + c + " forces loading of EnglishGrammaticalRelations class");

		// reset
		Resetter.reset(Language.English);

		// export
		final RelationModel thisModel = DefaultMutableGrammaticalRelations.makeDefaultModel();
		Exporter.export(thisModel, Language.English);
		// AnnotationFactory.dump();

		for (final MutableGrammaticalRelation thatRelation : thisModel.theRelations)
		{
			final String name = thatRelation.getShortName();
			final boolean isAny = thatRelation.getId().equals("gov") || thatRelation.getId().equals("dep") || thatRelation.getId().equals("KILL");
			if (isAny)
			{
				continue;
			}

			System.out.println("Data for GrammaticalRelation loaded as valueOf(\"" + name + "\"):");
			final GrammaticalRelation reln = GrammaticalRelation.valueOf(Language.English, name);
			System.out.println("\tShort name:    " + reln.getShortName());
			System.out.println("\tLong name:     " + reln.getLongName());
			System.out.println("\tSpecific name: " + reln.getSpecific());
			System.out.println("\tParent:        " + reln.getParent().getShortName());
			System.out.println("\tAnnotation:    " + GrammaticalRelation.getAnnotationClass(reln));
		}
	}
}
