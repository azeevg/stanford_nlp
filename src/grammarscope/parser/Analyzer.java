package grammarscope.parser;

import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Analyzer
{
	/**
	 * Relation model
	 */
	private final RelationModel theRelationModel;

	// C O N S T R U C T

	public Analyzer(final RelationModel thisRelationModel)
	{
		this.theRelationModel = thisRelationModel;
	}

	// A N A L Y Z E R E L A T I O N S

	/**
	 * Analyze document sentence for grammatical relations
	 * 
	 * @param thisParsedDocument
	 *            the parsed document
	 * @param thisSentence
	 *            sentence
	 * @return Map<MutableGrammaticalRelation, List<ParsedRelation>>
	 */
	public Map<MutableGrammaticalRelation, List<ParsedRelation>> analyze(final ParsedDocument thisParsedDocument, final Sentence thisSentence)
	{
		//System.out.println("### " + thisSentence);
		//System.out.println(thisSentence.toString().hashCode());
		//System.out.println("###ParsedTrees: " + thisParsedDocument.theParsedTrees);

//		for(Sentence s: thisParsedDocument.theParsedTrees.keySet()){
//			System.out.println(s);
//			System.out.println(s.toString().hashCode());
//			System.out.println("### " + thisParsedDocument.theParsedTrees.get(s));
//		}

		Tree thisTree = thisParsedDocument.theParsedTrees.get(thisSentence);
		//System.out.println("###ParsedTrees: " + thisTree);
		if (thisTree != null)
		{
			final Map<MutableGrammaticalRelation, List<ParsedRelation>> thisGrammaticalRelation2ParsedRelationsMap = new HashMap<MutableGrammaticalRelation, List<ParsedRelation>>();

			for (final MutableGrammaticalRelation thisRelation : this.theRelationModel.theRelations)
			{
				final List<ParsedRelation> theseParsedRelations = getRelations(thisRelation, thisTree, thisTree, thisSentence);
				thisGrammaticalRelation2ParsedRelationsMap.put(thisRelation, theseParsedRelations);
			}
			return thisGrammaticalRelation2ParsedRelationsMap;
		}
		return null;
	}

	/**
	 * Analyze sentence for a given grammatical relation
	 * 
	 * @param thisRelation
	 *            the targeted grammatical relation
	 * @param thisTree
	 *            the parsed subtree to analyze
	 * @param thisRootTree
	 *            the root tree above the parsed subtree to analyze
	 * @param thisSentence
	 *            the sentence container
	 * @return List<ParsedRelation>
	 */
	private List<ParsedRelation> getRelations(final MutableGrammaticalRelation thisRelation, final Tree thisTree, final Tree thisRootTree, final Sentence thisSentence)
	{
		// System.out.println("GET RELATION INSTANCES:" + thisRelation.getShortName());
		final List<ParsedRelation> thisList = new ArrayList<ParsedRelation>();
		getRelations(thisRelation, thisTree, thisRootTree, thisSentence, thisList);
		// System.out.println("#" + thisList.size());
		return thisList;
	}

	/**
	 * Analyze sentence (recursive)
	 * 
	 * @param thisGrammaticalRelation
	 *            the targeted grammatical relation
	 * @param thisTree
	 *            the parsed subtree to analyze
	 * @param thisRootTree
	 *            the root tree above the parsed subtree to analyze
	 * @param thisSentence
	 *            the sentence container
	 * @param thisList
	 *            the list accumulation results
	 */
	private void getRelations(final MutableGrammaticalRelation thisGrammaticalRelation, final Tree thisTree,
							  final Tree thisRootTree, final Sentence thisSentence, final List<ParsedRelation> thisList)
	{
		// don't do leaves
		if (thisTree.numChildren() <= 0)
			return;

		// scan for predicate grammatical relation
		if (thisGrammaticalRelation.isApplicable(thisTree))
		{
			final SemanticHeadFinder theFinder = new SemanticHeadFinder();
			for (final Tree thisTargetTree : thisGrammaticalRelation.getRelatedNodes(thisTree, thisRootTree))
			{
				final ParsedRelation thisRelation = new ParsedRelation();

				// context
				thisRelation.theSentence = thisSentence;
				thisRelation.theRelation = thisGrammaticalRelation;

				// relation target
				thisRelation.theTree = thisTargetTree;
				thisRelation.theHead = thisTargetTree.headTerminal(theFinder);
				thisRelation.theHeadBase = Parser.morphology(thisRelation.theHead.value());

				// relation scope
				thisRelation.theScopeTree = thisTree;
				thisRelation.theScopeHead = thisTree.headTerminal(theFinder);
				thisRelation.theScopeHeadBase = Parser.morphology(thisRelation.theScopeHead.value());

				thisList.add(thisRelation);
				//System.out.println(thisRelation);
			}
		}

		// now recurse into children
		for (final Tree thisChildTree : thisTree.children())
		{
			getRelations(thisGrammaticalRelation, thisChildTree, thisRootTree, thisSentence, thisList);
		}
	}
}
