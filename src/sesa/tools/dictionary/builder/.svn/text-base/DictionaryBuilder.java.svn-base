package sesa.tools.dictionary;

import sesa.common.database.common.DBIterator;
import sesa.common.database.common.IDBEntity;
import sesa.common.features.Option;
import sesa.common.features.Pair;
import sesa.common.utility.common.Application;
import sesa.common.utility.dictionary.Dictionary;
import sesa.common.utility.dictionary.Token;
import sesa.common.utility.logging.ReportUtility;

import java.util.*;

public class DictionaryBuilder extends Application {
    private Dictionary dictionary = new Dictionary();

    private void processDocument(IDBEntity document) {
        Map<String, Object> documentMap = document.toMap();
        if (!documentMap.containsKey("keywords"))
            return;

        Set<String> keywords = new HashSet<String>();
        List<String> documentKeywords = (List<String>) documentMap.get("keywords");

        for (String keyword : documentKeywords) {
            String token = keyword.toLowerCase().trim();
            if (token.length() <= 2)
                continue;

            keywords.add(keyword);
        }

        dictionary.merge(keywords);
    }

    private void prepareDictionary() {
        DBIterator iterator = dbController.getIteratorDocumentPUBMEDIterator();

        ReportUtility reporter = new ReportUtility("Dictionary building [1/2]", iterator.count());
        while (iterator.hasNext()) {
            Option<IDBEntity> document = iterator.next();

            if (document.isDefined())
                processDocument(document.get());

            reporter.addProcessed(1);
            reporter.report();
        }
    }

    private void saveDictionary() {
        ReportUtility reporter = new ReportUtility("Saving dictionary [2/2]", dictionary.getSize());

        for (String keyword : dictionary) {
            Token token = new Token(keyword, dictionary.getWeight(keyword));
            dbController.addDictionaryToken(token);
            reporter.addProcessed(1);
            reporter.report();
        }
    }

    @Override
    protected boolean body() {
        prepareDictionary();
        saveDictionary();

        return true;
    }
}
