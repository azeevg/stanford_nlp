package edu.spbstu.egor.wrapper;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.util.Index;

import java.util.List;

/**
 * Created by Egor Gorbunov on 17.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class ParserTracer {

    private final List<HasWord> sentence;
    private final Index<String> stateIndex;
    private final Index<String> tagIndex;
    private final float[][][] iScore;
    private final int[][] narrowLExtent;
    private final int[][] narrowRExtent;
    private final int[][] wideLExtent;
    private final int[][] wideRExtent;
    private final int[][][] wordsInSpan;

    public ParserTracer(List<HasWord> sentence,
                        Index<String> stateIndex,
                        Index<String> tagIndex,
                        float[][][] iScore,
                        int[][] narrowLExtent,
                        int[][] narrowRExtent,
                        int[][] wideLExtent,
                        int[][] wideRExtent,
                        int[][][] wordsInSpan) {

        this.sentence = sentence;
        this.stateIndex = stateIndex;
        this.tagIndex = tagIndex;
        this.iScore = iScore;
        this.narrowLExtent = narrowLExtent;
        this.narrowRExtent = narrowRExtent;
        this.wideLExtent = wideLExtent;
        this.wideRExtent = wideRExtent;
        this.wordsInSpan = wordsInSpan;
    }

    public void trace() {

        int length = sentence.size();

        for (int start = 0; start < iScore.length; ++start) {
            for (int end = start + 1; end <= length; ++end) {
                for (int state = 0; state < iScore[start][end].length; state++) {
                    if (iScore[start][end][state] > Float.NEGATIVE_INFINITY) {

                        System.out.println("From ( " + start + ", " + sentence.get(0).word() + " )" +
                        " To ( " + (end - 1) + ", " + sentence.get(end - 1) + " )" +
                        " STATE = " + stateIndex.get(state));

                    }
                }
            }
        }



        for (int start = 0; start < length; ++start) {
            for (int state = 0; state < narrowRExtent[start].length; ++state) {
                if (narrowRExtent[start][state] != length + 1) {

                    System.out.println("NARROW_R_EXTENT of STATE = " + stateIndex.get(state) +
                            " STARTING AT = ( " + start + ", " + sentence.get(start).word() + " )" +
                            " IS = " + narrowRExtent[start][state]);

                }
            }

        }

        for (int start = 0; start < length; ++start) {
            for (int state = 0; state < wideRExtent[start].length; ++state) {
                if (wideRExtent[start][state] != -1) {

                    System.out.println("WIDE_R_EXTENT of STATE = " + stateIndex.get(state) +
                            " STARTING AT = ( " + start + ", " + sentence.get(start).word() + " )" +
                            " IS = " + wideRExtent[start][state]);

                }
            }
        }

        for (int end = 0; end < length; ++end) {
            for (int state = 0; state < narrowLExtent[end].length; ++state) {
                if (narrowLExtent[end][state] != -1) {

                    System.out.println("NARROW_L_EXTENT of STATE = " + stateIndex.get(state) +
                            " ENDING AT = ( " + end + ", " + sentence.get(end).word() + " )" +
                            " IS = " + narrowLExtent[end][state]);

                }
            }
        }

        for (int end = 0; end < length; ++end) {
            for (int state = 0; state < wideLExtent[end].length; ++state) {
                if (wideLExtent[end][state] != length + 1) {

                    System.out.println("WIDE_L_EXTENT of STATE = " + stateIndex.get(state) +
                            " ENDING AT = ( " + end + ", " + sentence.get(end).word() + " )" +
                            " IS = " + wideLExtent[end][state]);

                }
            }
        }
    }
}
