package sesa.common.database.entity.dictionary;

/**
 * Created by Ira on 13.05.2016.
 */
public class TermEntity {

    private int id;
    private String headWord;

    public TermEntity(int id, String headWord) {
        this.id = id;
        this.headWord = headWord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeadWord() {
        return headWord;
    }

    public void setHeadWord(String headWord) {
        this.headWord = headWord;
    }

    @Override
    public String toString() {
        return "TermEntity{" +
                "id=" + id +
                ", headWord='" + headWord + '\'' +
                '}';
    }
}
