package sesa.common.database.common;

import sesa.common.configuration.Configuration;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.features.Option;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;


public class DBIterator implements IDBIterator{

    private DocumentsDao documentsDao;
    private String collectionName;
    private long collectionPos;
    private Queue<IDBEntity> entities = new LinkedList<IDBEntity>();

    public DBIterator(String collectionName) throws SQLException, ClassNotFoundException {
        documentsDao = new DocumentsDao();
        this.collectionName = collectionName;
        this.collectionPos = 0;
        updateQueue();
    }
    private void updateQueue() {
        entities.addAll(documentsDao.getDocuments(collectionName, collectionPos, Configuration.DB_ITERATOR_BATCH_SIZE));
        collectionPos += Configuration.DB_ITERATOR_BATCH_SIZE;
    }

    public boolean hasNext() {
        return !entities.isEmpty();
    }

    public synchronized Option<IDBEntity> next() {
        IDBEntity entity = entities.poll();

        if (entities.isEmpty())
            updateQueue();

        return new Option<IDBEntity>(entity);
    }

    public long count() {
        return documentsDao.countTotalOfRow(collectionName);
    }
}
