package sesa.tools.filters.WIKI.mixed;

import sesa.common.configuration.Categories;
import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.tools.filters.WIKI.text.WIKIExtractImportException;
import sesa.tools.filters.WIKI.text.WIKIExtractProcessor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;


public class MixedFilter extends Application {
    private DocumentsDao documentsDao = new DocumentsDao();
    private final static String CAT_LIST = "cat_list";
    private int counter = 0;

    private String[] categoryNames = new String[]{Categories.ALGEBRA_CATEGORY, Categories.COMPUTING_CATEGORY,
            Categories.HISTORY_CATEGORY, Categories.LITERATURE_CATEGORY,
            Categories.CHEMISTRY_CATEGORY, Categories.PHYSICS_CATEGORY, Categories.BIOLOGY_CATEGORY};

    public MixedFilter() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {


        Map<String, Set<String>> tagMap = null;
        try {
            tagMap = createCategoryTagMap(categoryNames);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tagMap == null)
            return false;

        createCategoryTables(categoryNames);
        IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
        //dbController.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);
        iterator = new DBReportingIterator(iterator, "Mixed filter");

        while (iterator.hasNext()) {
            IDBEntity entity = iterator.next().get();
            Map<String, Object> map = entity.toMap();
            List<String> docTags = (List<String>) map.get("cat");
            filterByCategoriesArray(entity, docTags, tagMap);

            if (counter++ % 10000 == 0)
                System.gc();
        }

        return true;
    }

    private void filterByCategoriesArray(IDBEntity entity, List<String> docTags, Map<String, Set<String>> tagMap) {

        boolean docHasAlreadyProceed = false;
        docTags = clearDocCats(docTags);

        for (String docTag : docTags) {
            for (String category : categoryNames) {
                if (tagMap.get(category).contains(docTag)) {
                    if (!docHasAlreadyProceed) {
                        if (processDocumentWithTextFilter(entity))
                            docHasAlreadyProceed = true;
                        else
                            return;
                    }
                    String categoryTable = Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI + "_" + category;
                    documentsDao.addToDocumentsCollection(categoryTable, entity);
                   // dbController.insertToDocumentsCollection(categoryTable, entity);
                }
            }
        }
    }

    private List<String> clearDocCats(List<String> docTags) {
        List<String> cleared = new LinkedList<String>();
        for (String docTag : docTags)
            cleared.add(clearTag(docTag));
        return cleared;
    }

    private boolean processDocumentWithTextFilter(IDBEntity entity) {
        WIKIExtractProcessor processor = new WIKIExtractProcessor(entity);
        try {
            processor.processDocument();
        } catch (WIKIExtractImportException e) {
            LoggingUtility.error(e.getMessage());
            return false;
        }
        return true;
    }

    private void createCategoryTables(String[] categoryNames) {
        for (String categoryName : categoryNames) {
            String categoryTable = Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI + "_" + categoryName;
            documentsDao.truncateOrCreateDocumentCollection(categoryTable);
           // dbController.truncateOrCreate(categoryTable);
        }
    }

    private Map<String, Set<String>> createCategoryTagMap(String[] categoryNames) throws IOException {

        Map<String, Set<String>> tagMap = new HashMap<String, Set<String>>();

        for (String categoryName : categoryNames) {
            String categoryFile = configuration.getProperty(CAT_LIST) + "_" + categoryName + ".txt";
            Set<String> tags = new HashSet<String>();

            BufferedReader reader = new BufferedReader(new FileReader(categoryFile));
            String text = null;

            while ((text = reader.readLine()) != null) {
                if (text.length() > 0)
                    tags.add(text.trim());
            }
            reader.close();

            tagMap.put(categoryName, tags);
        }

        return tagMap;
    }

    public String clearTag(String category) {
        return category.replaceAll("\\|", "").trim();
    }
}
