package sesa.common.database.connection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;


/**
 * Created by Ira on 24.01.2016.
 */
public class ConnectionPool {
    private BlockingQueue<Connection> connectionQueue;
    private BlockingQueue<Connection> givenAwayConQueue; // TODO: change
    public final Logger log = LogManager.getLogger(ConnectionPool.class);
    private String driverName;
    private String url;
    private String user;
    private String password;
    private int poolSize;

    public ConnectionPool() {

        DBResourceManager dbResourseManager = DBResourceManager.getInstance();

        this.driverName = dbResourseManager.getValue(DBParameter.DB_DRIVER);
        this.url = dbResourseManager.getValue(DBParameter.DB_URL);
        this.user = dbResourseManager.getValue(DBParameter.DB_USER);
        this.password = dbResourseManager.getValue(DBParameter.DB_PASSWORD);

        try {
            this.poolSize = Integer.parseInt(dbResourseManager.getValue(DBParameter.DB_POLL_SIZE));
        } catch (NumberFormatException e) {
            poolSize = 5;
        }

    }

    public void initPoolData() throws SQLException, ClassNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        try {

            Class.forName(driverName);
            givenAwayConQueue = new ArrayBlockingQueue<Connection>(poolSize);
            connectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                PooledConnection pooledConnection = new PooledConnection(connection);
                connectionQueue.add(pooledConnection);
            }

        } catch (SQLException e) {
            log.error("Cannot connect to database. " + e.getMessage(), e);
            throw new SQLException("Cannot connect to database. " + e.getMessage(), e);
        } catch (ClassNotFoundException e) {
            log.error("Cannot find database driver: " + driverName, e);
            throw new ClassNotFoundException("Cannot find database driver: " + driverName, e);
        }
    }

    public void dispose() {
        clearConnectionQueue();
    }

    private void clearConnectionQueue() {
        try {

            closeConnectionsQueue(givenAwayConQueue);
            closeConnectionsQueue(connectionQueue);
        } catch (SQLException e) {
            log.error("Error closing the com.company.connection.", e);
        }

    }

    public Connection takeConnection() {
        Connection connection = null;
        try {
            connection = connectionQueue.take();
            givenAwayConQueue.add(connection);
            //checkConnection(connection);
        } catch (InterruptedException e) {
            log.error("Error connecting to the data source", e);
        }
        return connection;
    }

    /**
     * @param con
     * @return
     */
    private boolean checkConnection(Connection con) {
        Statement statement = null;
        ResultSet resultSet = null;
        boolean result = false;
        try {
            statement = con.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM 1");
            if (resultSet.next()) {
                result = true;
            }

        } catch (SQLException e) {
            log.error("Error connecting to the data source", e);
        } finally {
            closeStatementResultSet(statement, resultSet);
        }
        return result;
    }

    private void closeStatementResultSet(Statement st, ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            log.error("ResultSet isn't closed.");

        }

        try {
            st.close();
        } catch (SQLException e) {
            log.error("Statement isn't closed.");

        }
    }

    public void closeConnection(Connection con, Statement st, ResultSet rs) {

        try {
            con.close();
        } catch (SQLException e) {
            log.error("Connection isn't return to the pool.");

        }


        try {
            st.close();
        } catch (SQLException e) {
            log.error("Statement isn't closed.");

        }
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            log.error("ResultSet isn't closed.");

        }

    }

    public void closeConnection(Connection con, ResultSet rs) {

        try {
            con.close();
        } catch (SQLException e) {
            log.error("Connection isn't return to the pool.");
        }

        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            log.error("ResultSet isn't closed.");

        }


    }

    public void closeConnection(Connection con, Statement st) {
        try {
            if (con != null)
                con.close();
        } catch (SQLException e) {
            log.error("Connection isn't return to the pool.");
        }
        try {
            if (st != null)
                st.close();
        } catch (SQLException e) {
            log.error("Statement isn't closed.");
        }
    }

    public void closeConnection(Connection con, PreparedStatement st, ResultSet rs) {

        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            log.error("Connection isn't return to the pool.");

        }

        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            log.error("ResultSet isn't closed.");

        }

        try {
            st.close();
        } catch (SQLException e) {
            log.error("Statement isn't closed.");

        }

    }

    public void closeConnection(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            log.error("Connection isn't return to the pool.");

        }

    }


    private void closeConnectionsQueue(BlockingQueue<Connection> queue) throws SQLException {
        Connection connection;
        while ((connection = queue.poll()) != null) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            ((PooledConnection) connection).reallyClose();
        }

    }


    private class PooledConnection implements Connection {
        private Connection connection;

        public PooledConnection(Connection c) throws SQLException {
            this.connection = c;
            this.connection.setAutoCommit(true);
        }

        public void reallyClose() throws SQLException {
            connection.close();
        }


        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }


        public void close() throws SQLException {
            if (connection.isClosed()) {
                throw new SQLException("Attempting to close closed com.company.connection.");
            }

            if (connection.isReadOnly()) {
                connection.setReadOnly(false);
            }

            if (!givenAwayConQueue.remove(this)) {
                throw new SQLException("Error deleting com.company.connection from the given away connections pool.");
            }

            if (!connectionQueue.offer(this)) {
                throw new SQLException("Error allocating com.company.connection in the pool.");
            }
        }


        public void commit() throws SQLException {
            connection.commit();
        }


        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName, elements);

        }

        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }


        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }


        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }


        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }


        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }


        public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
        }


        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName, attributes);
        }


        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }


        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }


        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }


        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }


        public int getHoldability() throws SQLException {
            return connection.getHoldability();

        }


        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();

        }


        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();

        }


        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }


        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }


        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }


        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }


        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }


        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }


        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }


        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }


        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);

        }


        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);

        }


        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }


        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);

        }


        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }


        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
                throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
        }


        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability)
                throws SQLException {

            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);

        }


        public void rollback() throws SQLException {
            connection.rollback();

        }


        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);

        }


        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);

        }


        public void setClientInfo(String name, String value) throws SQLClientInfoException {
            connection.setClientInfo(name, value);
        }


        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);

        }


        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);

        }


        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();

        }


        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);

        }


        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);

        }


        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);

        }


        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);

        }


        public void abort(Executor arg0) throws SQLException {
            connection.abort(arg0);

        }


        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();

        }


        public String getSchema() throws SQLException {
            return connection.getSchema();

        }


        public void releaseSavepoint(Savepoint arg0) throws SQLException {
            connection.releaseSavepoint(arg0);

        }


        public void rollback(Savepoint arg0) throws SQLException {
            connection.rollback(arg0);

        }


        public void setClientInfo(Properties arg0) throws SQLClientInfoException {
            connection.setClientInfo(arg0);

        }


        public void setNetworkTimeout(Executor arg0, int arg1) throws SQLException {
            connection.setNetworkTimeout(arg0, arg1);
        }


        public void setSchema(String arg0) throws SQLException {
            connection.setSchema(arg0);

        }


        public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException {
            connection.setTypeMap(arg0);
        }
    }
}
