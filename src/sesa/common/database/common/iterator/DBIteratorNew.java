package sesa.common.database.common.iterator;

import sesa.common.configuration.Configuration;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.connections.DBConnection;
import sesa.common.features.Option;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Ira on 25.02.2016.
 */
@Deprecated
public class DBIteratorNew implements IDBIterator {

    private DBConnection connection;
    private String collectionName;
    private long collectionPos;
    private Queue<IDBEntity> entities = new LinkedList<IDBEntity>();

    public DBIteratorNew(String collectionName, DBConnection connection) {
        this.connection = connection;
        this.collectionName = collectionName;
        this.collectionPos = 0;

        updateQueue();
    }
    private void updateQueue() {
        //TODO: replace function ( use new function)
        entities.addAll(connection.select(collectionName, collectionPos, Configuration.DB_ITERATOR_BATCH_SIZE));

        collectionPos += Configuration.DB_ITERATOR_BATCH_SIZE;
    }



    public boolean hasNext() {
        return !entities.isEmpty();
    }

    public synchronized Option<IDBEntity> next() {
        IDBEntity entity = entities.poll();
        //System.out.println(entities.size());

        if (entities.isEmpty())
            updateQueue();

        return new Option<IDBEntity>(entity);
    }

    public long count() {
        return connection.count(collectionName);
    }
}
