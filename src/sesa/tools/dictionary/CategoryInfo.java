package sesa.tools.dictionary;

import sesa.tools.math.Vector;

public class CategoryInfo {
    private String categoryName;
    private Vector center;
    private double radius;

    public CategoryInfo(String categoryName, Vector center, double radius) {
      this.categoryName = categoryName;
      this.center = center;
      this.radius = radius;
    }

    @Override
    public String toString() {
      return "CATEGORY INFO:\n NAME = " + categoryName + "\n CENTER DIM = " + center.size() + "\n RADIUS = " + radius;
    }
  }
