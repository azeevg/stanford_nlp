package sesa.common.model.processing.dictionary;

import sesa.common.database.entity.IDBEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * This class represents a <br> Token </br>
 * Token could be a word or punctuation mark
 */
public class Token implements IDBEntity {
    private String keyword; // name of token
    private Double weight; // ������ ������???

    public Token(String keyword, Double weight) {
        this.keyword = keyword;
        this.weight = weight;
    }

    public String getID() {
        return keyword;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> token = new HashMap<String, Object>();
        token.put("token", keyword);
        token.put("weight", weight);

        return token;
    }
}
