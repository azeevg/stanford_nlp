package sesa.common.database.common;

import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.ParsedRelation;
import sesa.common.configuration.Configuration;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.connections.DBConnection;
import sesa.common.model.parsing.JSONDocument;
import sesa.common.model.parsing.XMLDocument;
import sesa.common.model.processing.dictionary.Token;
import sesa.tools.statistics.RawSemanticGroup;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ����� ������ � ����� ������
 * ��������� ����������:
 * Common class for working with DB and contains all methods
 */

@Deprecated
public class DBController {
    private DBConnection dbConnection;

    public DBController(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void useConnection(int nodeIndex) {
        this.dbConnection = dbConnection.useConnection(nodeIndex);
    }

    //----------------- insert
    // use the same method
    public boolean addDocumentPUBMED(JSONDocument document) {
        return dbConnection.insert(Configuration.DB_COLLECTION_PUBMED, document);
    }

    public boolean addDocumentWIKI(XMLDocument document) {
        return dbConnection.insert(Configuration.DB_COLLECTION_WIKI, document);
    }

    public boolean addDictionaryToken(Token token) {
        return dbConnection.insert(Configuration.DB_COLLECTION_DICTIONARY, token);
    }

    public void insertToDocumentsCollection(String collectionName, IDBEntity entity) {
        dbConnection.insert(collectionName, entity);
    }
    //----------------- end of insert

    //----------------- select

    public IDBEntity selectDocument(String collectionName, String id) {
        return dbConnection.selectById(collectionName, id);
    }

    //----------------- end of select

    //----------------- count
    public long getDocumentsPUBMEDCount() {
        return dbConnection.count(Configuration.DB_COLLECTION_PUBMED);
    }
    public long getDocumentsWIKICount() {
        return dbConnection.count(Configuration.DB_COLLECTION_WIKI);
    }
    public long getDocumentsWIKIFilteredCount() {return dbConnection.count(Configuration.DB_COLLECTION_FILTERED_WIKI);}
    public long countByCategoryDocuments() {return dbConnection.count(Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI);}
    public long getCollectionSize(String collectionName) {
        return dbConnection.count(collectionName);
    }
    //----------------- end of count

    //----------------- truncate
    public boolean removeCategorizedDocumentWIKI() {return dbConnection.truncate(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI);}
    public boolean removeByCategoryDocuments() {return dbConnection.truncate(Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI);}
    public void truncateDocumentsCollection(String collectionName) {dbConnection.truncate(collectionName);}

    public void truncateOrCreate(String collectionName) {
        if (dbConnection.tableExists(collectionName))
            dbConnection.truncate(collectionName);
        else
            dbConnection.create(collectionName);
    }
    //----------------- end of truncate

    //----------------- counter
    public int getCounter(String key) {
        return dbConnection.getCounter(key);
    }
    public void resetCounter(String key) {
        setCounter(key, 0);
    }
    public void setCounter(String key, int value) {
        dbConnection.setCounter(key, value);
    }
    public void increaseCounter(String key) {
        int current = getCounter(key);
        setCounter(key, current + 1);
    }
    //----------------- eng of counter

    //----------------- semantic group
    public Collection<RawSemanticGroup> getSemanticGroups(String collectionName) {
        return dbConnection.getSemanticGroups(collectionName);
    }
    public synchronized void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, String pos, int index, String groupId, int sentenceId, int sentenceService, int prevDelimiter, int sentenceSize) {
        dbConnection.saveSemanticRelation(collectionName, token, tokenBase, role, pos, index, groupId, sentenceId, sentenceService, prevDelimiter, sentenceSize);
    }
    public HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> getCachedRelations(String collectionName, String title) {
        return dbConnection.getCachedRelations(collectionName, title);
    }
    @Deprecated
    public void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, int position, int groupHash, int groupId) {
        dbConnection.saveSemanticRelation(collectionName, token, tokenBase, role, position, groupHash, groupId);
    }
    //----------------- end of semantic group

    //----------------- iterator
//    public DBIterator getIteratorDocumentPUBMEDIterator() {return dbConnection.getIterator(Configuration.DB_COLLECTION_PUBMED);}
   // public DBIterator getDocumentsCollectionIterator(String collectionName) {return dbConnection.getIterator(collectionName);}
    //----------------- end of iterator

    public boolean addByCategoryDocuments(String categories) {
        return dbConnection.copyByCategory(Configuration.DB_COLLECTION_CAT_FILTERED_WIKI, Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI, categories);
    }

    //------------------ tree

    @Deprecated
    public void insertObject(String collectionName, String title, String hash, byte[] data) {
        dbConnection.insertObject(collectionName, title, hash, data);
    }

    public void insertObject(String collectionName, int title, int hash, String data) {
        dbConnection.insertObject(collectionName, title, hash, data);
    }
    public synchronized HashMap<Integer, Tree> getCachedTrees(String collectionName, int titleHash) {
        return dbConnection.getCachedTrees(collectionName, titleHash);
    }
    @Deprecated
    public HashMap<Integer, Tree> getCachedTreesObjects(String collectionName, String title) {
        return dbConnection.getCachedTreesObjects(collectionName, title);
    }

    //------------------ end of tree



    // ??
    public InputStream readObject(String collectionName, String title, String id) {
        return dbConnection.readObject(collectionName, id);
    }




    //----------------- error doc
    public void writeErrorDocument(String title, String type, String message) {
        dbConnection.writeErrorDocument(title, type, message);
    }
    public List<String> readParsingErrors() {
        return dbConnection.readParsingErrors();
    }
    //----------------- end of error doc




}
