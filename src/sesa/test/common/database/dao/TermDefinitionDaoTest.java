package sesa.test.common.database.dao;

import org.junit.Test;
import sesa.common.database.dao.dictionary.SemanticStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Ira on 01.06.2016.
 */
public class TermDefinitionDaoTest {
    private FileManager file = new FileManager("path");

    public void init(){

    }
    @Test
    public void testExtractIdTermByIdDef() throws SQLException, ClassNotFoundException {
        List<String> idUniqueTerms = FileReaderWriter.read(file.getValue("path.uniqueTerm"));
        TermDefinitionDao dao = new TermDefinitionDao();
        int idTerm = dao.getTermIdByDefId(2069);
        String id = "" + idTerm;
        System.out.println("id:" + id);
        System.out.println(idUniqueTerms.contains(id.trim()));
    }

    @Test
    public void testGetSemanticStructureDtoByTermId() throws SQLException, ClassNotFoundException {
        SemanticStructureDao dao = new SemanticStructureDao();
        System.out.println(dao.getSemanticGroupDtoByTermID(2));
    }

    @Test
    public void testGetTermInfoByIdTerm() throws SQLException, ClassNotFoundException {
        List<Integer> idUniqueTerms = FileReaderWriter.readNumber("data/dictionary/auto_reference/idTermsNotContainAutoRef");
        TermDefinitionDao dao = new TermDefinitionDao();
        TermDao termDao = new TermDao();
        for(int id : idUniqueTerms ){
            FileReaderWriter.write("termWithoutAllReference.txt","#" + termDao.getTermById(id).getHeadWord() + "###" + dao.getTermDefinitionByTermID(id).get(0).getDefinition());
            //System.out.println(termDao.getTermById(id).getHeadWord());
            //System.out.println(dao.getTermDefinitionByTermID(id).get(0).getDefinition());
        }
    }
}
