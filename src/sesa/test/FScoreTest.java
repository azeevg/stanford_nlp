package sesa.test;

import edu.stanford.nlp.trees.Tree;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import sesa.common.utility.io.CommonUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ira on 18.05.2016.
 */
public class FScoreTest {
    public final Logger log = LogManager.getLogger(FScoreTest.class);
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
    private String languageModel = "./data/grammar/englishFactored.ser.gz";
    public Map<Integer,Integer> brackets = new HashMap<Integer, Integer>();

    @Test
    public void testF() throws IOException {
        String pennTree = "(S (NP (NN Information)) (VP (VBZ is) (NP (NP (DT the) (NN application)) (PP (IN of) (NP (NP (NNS computers)) (PP (TO to) (NP (NN store))))))) (. .))";
        Tree tree = CommonUtils.readPennTreeFormString(pennTree);

        System.out.println(tree);
        recursiveRead(tree);

//        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
//        Parser parser = new Parser(inputStream);
//        Sentence sentence = SentenceUtils.readSentenceFromFile("data/test.txt");
//        Tree tree2 = parser.parse(sentence,true);
//        System.out.println(tree2.getChildrenAsList());


    }

    @Test
    public void test(){
        int i = 12334456;
        String name = "" + i;
        String test = String.valueOf(i);
        System.out.println(test.equals(name.trim()));

    }

    public void recursiveRead(Tree tree) {
        if (tree.getChildrenAsList().size() > 1) {
            for (Tree t : tree.getChildrenAsList()) {
                recursiveRead(t);
            }
        }else{
            System.out.println("###");
            System.out.println(tree);
        }

    }


}
