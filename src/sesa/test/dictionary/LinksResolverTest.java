package sesa.test.dictionary;

import org.junit.Test;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.utility.dictionary.LinksResolver;
import sesa.common.utility.dictionary.remove.CrossReferenceFinder;

import java.util.List;

/**
 * Created by gleb on 23.05.17.
 */

public class LinksResolverTest {
    @Test
    public void resolveOneLink() throws Exception {
        int defId = 62760;
        if (new LinksResolver().resolveLink(defId)) {
            TermDefinitionDao dao = new TermDefinitionDao();

            System.out.println("resolved: DEF_ID=" + defId);
            System.out.println(dao.getDefinitionByDefinitionId(defId));
        }
    }

    @Test
    public void resolveAllLinks() throws Exception {
        TermDefinitionDao definitionDao = new TermDefinitionDao();
        List<Integer> links = definitionDao.getAllLinks();
        LinksResolver linksResolver = new LinksResolver();
        for (Integer link : links) {
            linksResolver.resolveLink(link);
        }
    }

    @Test
    public void name() throws Exception {
        String def = "BLACKBERRY -- See also BlackBerry App World.";
        int pos = def.indexOf("See also");
        String term = def.substring(pos + 9, def.indexOf(".", pos + 9));

        System.out.println(term);
        CrossReferenceFinder finder = new CrossReferenceFinder();
        System.out.println(finder.removeSentenceFromText(String.format("See also %s.", term), def));
    }
}
