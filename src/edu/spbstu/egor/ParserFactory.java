package edu.spbstu.egor;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.egor.wrapper.CollocationReplaceParser;
import edu.spbstu.egor.wrapper.CollocationSpanTaggingParser;
import edu.spbstu.egor.wrapper.ExhaustivePCFGParserNoOutsides;
import edu.spbstu.egor.wrapper.MyParserWrapper;
import edu.stanford.nlp.parser.lexparser.*;

/**
 * Created by Egor Gorbunov on 29.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class ParserFactory {

    private Grammar grammar;

    public ParserFactory(Grammar grammar) {
        if (grammar == null) {
            throw new NullPointerException();
        }
        this.grammar = grammar;
    }

    public ExhaustivePCFGParser getParserNoOutsides() {
        return new ExhaustivePCFGParserNoOutsides(grammar.bg, grammar.ug, grammar.lex, grammar.op,
                grammar.stateIndex, grammar.wordIndex, grammar.tagIndex);
    }

    public MyParserWrapper getMyParser() {
        return new MyParserWrapper(grammar.bg, grammar.ug, grammar.lex, grammar.op,
                grammar.stateIndex, grammar.wordIndex, grammar.tagIndex);
    }

    public ExhaustivePCFGParser getPCFGParser() {
        return new ExhaustivePCFGParser(grammar.bg, grammar.ug, grammar.lex, grammar.op,
                grammar.stateIndex, grammar.wordIndex, grammar.tagIndex);
    }

    public ExhaustiveDependencyParser getDependencyParser() {
        grammar.dg.setLexicon(grammar.lex);
        return new ExhaustiveDependencyParser(grammar.dg, grammar.lex, grammar.op, grammar.wordIndex, grammar.tagIndex);
    }

    public CollocationSpanTaggingParser getCollocationSpanTaggingParser(CollocationDictionary dictionary) {
        return new CollocationSpanTaggingParser(grammar, dictionary);
    }

    public CollocationReplaceParser getCollocationReplaceParser(CollocationDictionary dictionary) {
        return new CollocationReplaceParser(grammar, dictionary);
    }
}
