package sesa.common.utility.vector;


import sesa.common.model.processing.document.IDocument;
import sesa.tools.math.metrics.IMetrics;
import sesa.tools.math.Vector;
import sesa.tools.math.VectorSpace;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.utility.dictionary.DictionaryUtils;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class VectorUtils {

    public static List<Vector> createVectors(List<IDocument> documents) throws SQLException, ClassNotFoundException {
        return createVectors(documents, DictionaryUtils.loadDictionary());
    }

    public static List<Vector> createVectors(List<IDocument> documents, Dictionary dictionary) {
        List<Vector> vectors = new LinkedList<Vector>();
        for (IDocument document : documents) {
            vectors.add(document.toVector(dictionary));
        }
        return vectors;
    }

    public static Vector getCenter(List<Vector> vectors) {
        Vector center = new Vector();
        for (Vector vector : vectors)
            center.add(vector);
        center.multiply(1.0 / vectors.size());
        return center;
    }

    public static double getMaxRadius(List<Vector> vectors, Vector center, IMetrics metrics) {

        double radius = Double.MIN_VALUE;
        for (Vector vector : vectors)
            radius = Math.max(radius, metrics.getDistance(vector, center));
        return radius;
    }

    public static List<Vector> getVectorsSubSet(VectorSpace vectorSpace, int fromPercentage, int toPercentage) {
        if (Math.abs(fromPercentage - 50) > 50 || Math.abs(toPercentage - 50) > 50 || fromPercentage > toPercentage)
            return null;

        List<Vector> subSet = new LinkedList<Vector>();
        double fromRadius = vectorSpace.getMaxRadius() * fromPercentage / 100;
        double toRadius = vectorSpace.getMaxRadius() * toPercentage / 100;

        IMetrics metrics = vectorSpace.getMetrics();
        Vector center = vectorSpace.getCenter();
        for (Vector vector : vectorSpace.getVectors()) {
            double distance = metrics.getDistance(vector, center);
            if (distance >= fromRadius && distance <= toRadius)
                subSet.add(vector);
        }
        return subSet;
    }

    public static Vector getRandomVector(List<Vector> vectors) {
        Random randomGenerator = new Random();
        int randomPos = randomGenerator.nextInt(vectors.size());
        int counter = 0;
        for (Vector vector : vectors) {
            if (counter++ == randomPos)
                return vector;
        }
        return null;
    }
}
