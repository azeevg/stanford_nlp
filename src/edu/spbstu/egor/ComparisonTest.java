package edu.spbstu.egor;

import edu.spbstu.egor.wrapper.ExhaustivePCFGParserNoOutsides;
import edu.spbstu.egor.wrapper.MyParserWrapper;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;
import edu.stanford.nlp.trees.TreeTransformer;
import edu.stanford.nlp.util.ScoredObject;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Egor Gorbunov on 18.04.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class ComparisonTest {
    private static final String SERIALIZED_GRAMMAR_FILENAME = "./data/grammar/englishFactored.ser.gz";
    private static final String TEXT_FILENAME = "./egor_in/text.txt";

    private static final String STANFORD_PARSER_OUT = "./egor_out/stanford_trees.txt";
    private static final String MY_PARSER_OUT = "./egor_out/my_trees.txt";


    private static TreeTransformer debinarizer;
    private static TreePrint treePrint;
    private static TreeTransformer subcategoryStripper;



    public static void parse(ExhaustivePCFGParser parser, List<List<? extends HasWord> >  sentences, PrintWriter pw) {
        for (List<? extends HasWord>  sentence : sentences) {
            parser.parse(sentence);

            Tree binaryTree = parser.getBestParse();

            if (binaryTree == null) {
                return;
            }

            //treePrint.printTree(new BoundaryRemover().transformTree(parser.getBestParse()), pwOut);
            treePrint.printTree(debinarizer.transformTree(parser.getBestParse()), pw);
            //Tree t = subcategoryStripper.transformTree(new BoundaryRemover().transformTree(parser.getBestParse()));
            //treePrint.printTree(t, pwOut);
        }


    }


    public static void main(String[] args) throws IOException {
        LexicalizedParser lexicalizedParser = new LexicalizedParser(SERIALIZED_GRAMMAR_FILENAME);
        ParserData pd = lexicalizedParser.getPD();
        Options op = lexicalizedParser.getOp();

        treePrint = op.testOptions.treePrint(lexicalizedParser.getOp().tlpParams);
        subcategoryStripper = op.tlpParams.subcategoryStripper();
        debinarizer = new Debinarizer(op.forceCNF);

        ExhaustivePCFGParserNoOutsides stanfrodParser = new ExhaustivePCFGParserNoOutsides(pd.bg, pd.ug, pd.lex, op,
                pd.stateIndex, pd.wordIndex, pd.tagIndex);

        MyParserWrapper myParser = new MyParserWrapper(pd.bg, pd.ug, pd.lex, op,
                pd.stateIndex, pd.wordIndex, pd.tagIndex);

        /*
        PrintWriter stanfordPw = new PrintWriter(new FileWriter(STANFORD_PARSER_OUT));
        PrintWriter myPw = new PrintWriter(new FileWriter(MY_PARSER_OUT));
        */

        StringWriter stanfordSWriter = new StringWriter(1000);
        PrintWriter stanfordPw = new PrintWriter(stanfordSWriter);

        StringWriter mySWriter = new StringWriter(1000);
        PrintWriter myPw = new PrintWriter(mySWriter);

        Reader rd = new BufferedReader(new FileReader(new File(TEXT_FILENAME)));

        DocumentPreprocessor dp = new DocumentPreprocessor(rd);

        List<List<? extends HasWord> > sentences = new LinkedList<List<? extends HasWord>>();
        for (List<? extends HasWord> s : dp) {
            sentences.add(s);
        }

        // ============= SMART MUTATIONS FLAG TO TRUE =================
        // op.lexOptions.smartMutation = true;

        //System.out.println("======================== FIXED PARSER ============================");
        //parse(myParser, sentences, myPw);
        //System.out.println("======================== STANFORD PARSER ============================");
        //parse(stanfrodParser, sentences, stanfordPw);

        List<Tree> stanfordTrees = new ArrayList<Tree>(sentences.size());
        List<Tree> myTrees = new ArrayList<Tree>(sentences.size());


        int k = 30;

        System.out.println("========= STANFORD PARSER =========");

        for (List<? extends HasWord>  sentence : sentences) {
            stanfrodParser.parse(sentence);

            List<ScoredObject<Tree>> kBestParses = stanfrodParser.getKBestParses(k);
            for (ScoredObject<Tree> so : kBestParses) {
                stanfordTrees.add(so.object());
            }
        }

        System.out.println("========= FIXED PARSER =========");

        stanfrodParser.clearArrays();

        int i = 0;
        for (List<? extends HasWord> sentence : sentences) {
            myParser.parse(sentence);

            List<ScoredObject<Tree>> kBestParses = myParser.getKBestParses(k);
            for (ScoredObject<Tree> so : kBestParses) {
                myTrees.add(so.object());

                if (!myTrees.get(i).equals(stanfordTrees.get(i))) {
                    System.out.println("============= NOT EQUAL!!! SENTENCE = " + sentence.toString());
                }
                ++i;
            }
        }

        myParser.clearArrays();
    }
}
