package sesa.tools.filters.WIKI.html;

import info.bliki.htmlcleaner.ContentToken;
import info.bliki.htmlcleaner.Utils;
import info.bliki.wiki.model.IWikiModel;
import info.bliki.wiki.tags.HTMLTag;

import java.io.IOException;

public class FilteringHtmlConverter extends ExtendedHtmlConverter {

  public FilteringHtmlConverter() {
    super();
  }

  public FilteringHtmlConverter(boolean noLinks) {
    super(noLinks);
  }

  public FilteringHtmlConverter(boolean noLinks, boolean noImages) {
    super(noLinks, noImages);
  }

  protected void renderContentToken(Appendable resultBuffer,
      ContentToken contentToken, IWikiModel model) throws IOException {


    String content = contentToken.getContent();
    content = content.replaceAll("\\(,", "(").replaceAll("\\(\\)", "()");
    content = Utils.escapeXml(content, true, true, true);
    resultBuffer.append(content);
  }

  protected void renderHtmlTag(Appendable resultBuffer, HTMLTag htmlTag,
      IWikiModel model) throws IOException {
    String tagName = htmlTag.getName();
    if (!tagName.equals("ref") && !tagName.equals("math")) {
      super.renderHtmlTag(resultBuffer, htmlTag, model);
    }
  }
}
