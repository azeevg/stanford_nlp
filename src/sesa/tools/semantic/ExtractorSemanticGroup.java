package sesa.tools.semantic;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sesa.common.database.dao.SemanticGroupDao;
import sesa.common.database.dao.dictionary.SemanticStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.database.entity.SemanticGroupEntity;
import sesa.common.utility.bundle.FileManager;
import sesa.tools.statistics.RawSemanticGroup;
import sesa.tools.statistics.RawSemanticRelation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Ira on 01.06.2016.
 */
public class ExtractorSemanticGroup {
    public final Logger log = LogManager.getLogger(ExtractorSemanticGroup.class);
    //private List<SemanticGroupEntity> allSemanticGroup;
    private List<String> idUniqueTerms;
    private TermDao termDao;
    private FileManager file;


    public ExtractorSemanticGroup() throws SQLException, ClassNotFoundException {
        this.file = new FileManager("path");
        //  this.allSemanticGroup = new ArrayList<SemanticGroupEntity>();
        this.idUniqueTerms = FileReaderWriter.read(file.getValue("path.uniqueTerm"));
        this.termDao = new TermDao();
    }

    /**
     * Семантическая группа, размером size
     *
     * @param size
     * @return
     */
    public List<SemanticGroupEntity> getSemanticRelationFromDataBase(int size) throws SQLException, ClassNotFoundException {
        List<SemanticGroupEntity> semanticGroups = new ArrayList<SemanticGroupEntity>();
        SemanticGroupDao dao = new SemanticGroupDao();
        Collection<RawSemanticGroup> groups = dao.getSemanticGroupsWithCollocation("semantic_groups_computer_dict");
        for (RawSemanticGroup group : groups) {
            boolean added = true;
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (RawSemanticRelation relation : group.getRelations()) {
                i++;
                sb.append(relation.getToken());
                sb.append(" ");
                if (relation.getPos().contains("COLLOCATION")) {
                    added = false;
                }
            }
            if (added && (i == size)) {
                semanticGroups.add(new SemanticGroupEntity(group.getId(), sb.toString().trim(), group.getSentenceId()));
            }
        }
        log.info("Number of semantic groups, which consist with " + size + " words: " + semanticGroups.size());
        return semanticGroups;
    }

    public List<SemanticGroupEntity> getSemanticRelationFromDataBaseContainCollocation() throws SQLException, ClassNotFoundException {
        List<SemanticGroupEntity> semanticGroups = new ArrayList<SemanticGroupEntity>();
        SemanticGroupDao dao = new SemanticGroupDao();
        Collection<RawSemanticGroup> groups = dao.getSemanticGroupsWithCollocation("semantic_groups_computer_dict");
        for (RawSemanticGroup group : groups) {
            boolean added = false;
            StringBuilder sb = new StringBuilder();
            for (RawSemanticRelation relation : group.getRelations()) {
                sb.append(relation.getToken());
                sb.append(" ");
                if (relation.getPos().contains("COLLOCATION")) {
                    added = true;
                }
            }

            if (added) {
                semanticGroups.add(new SemanticGroupEntity(group.getId(), sb.toString().trim(), group.getSentenceId()));
            }
        }

        log.info("Number of semantic groups, which contain collocations:" + semanticGroups.size());
        return semanticGroups;
    }


    /**
     * Add to database (semantic_structure) relevant semantic groups
     * Relevant group it is group with frequency meeting on the search wiki page more 2
     * json: "group_name" and "term_link_name":
     * "group_name": text_group
     * "term_link_name": list of terms contains in group_name, terms in Dictionary
     *
     * @param jSONList
     * @return
     */
    public List<SemanticGroupEntity> analyzeResult(List<String> jSONList, List<SemanticGroupEntity> semanticGroups) throws SQLException, ClassNotFoundException {
        log.info("input groups size:" + jSONList.size());
        Map<String, Integer> allTerms = termDao.getAllTerms();
        List<SemanticGroupEntity> subset = new ArrayList<SemanticGroupEntity>();
        TermDefinitionDao dao = new TermDefinitionDao();
        JSONParser parser = new JSONParser();
        try {
            for (String jsonStr : jSONList) {
                Object obj = parser.parse(jsonStr);
                JSONObject jsonObject = (JSONObject) obj;

                if (jsonObject.get("group_name") != null) {
                    String group = jsonObject.get("group_name").toString().trim();
                    List<String> linkTerm = (List<String>) jsonObject.get("term_link_name");
                    SemanticGroupEntity dto = getGroupByText(group, semanticGroups);
                    int idTerm = dao.getTermIdByDefId(dto.getSentenceId());
                    if (isUniqueTermById(idTerm)) {
                        dto.setTermLink(getListIdTerms(linkTerm, allTerms));
                        dto.setIdTerm(idTerm);
                        subset.add(dto);
                    } else {
                        System.out.println(idTerm);
                    }
                } else {
                    log.info("ERROR: " + jsonStr);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        log.info("output groups size:" + subset.size());
        return subset;

    }

    private SemanticGroupEntity getGroupByText(String text, List<SemanticGroupEntity> semanticGroups) {
        for (SemanticGroupEntity sg : semanticGroups) {
            if (sg.getText_group().equals(text)) {
                return sg;
            }
        }
        return null;
    }

    private boolean isUniqueTermById(int idTerm) {
        return idUniqueTerms.contains(String.valueOf(idTerm));
    }

    private int getTermIdByName(String name) {
        return termDao.getTermByName(name).getId();
    }

    private List<Integer> getListIdTerms(List<String> linkTerms, Map<String, Integer> allTerms) {
        List<Integer> idLinkTerms = new ArrayList<Integer>();
        for (String linkTerm : linkTerms) {
            idLinkTerms.add(allTerms.get(linkTerm));
            //idLinkTerms.add(getTermIdByName(linkTerm));
        }
        return idLinkTerms;
    }

    public void saveListSemanticGroupDto(List<SemanticGroupEntity> list) throws SQLException, ClassNotFoundException {
        SemanticStructureDao dao = new SemanticStructureDao();
        for (SemanticGroupEntity dto : list) {
            dao.addSemanticGroupStructure(dto);
        }
    }


}
