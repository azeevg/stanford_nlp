package sesa.common.model.processing.sentence;

import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a lexeme of a sentence
 */
public class SentenceToken {

    private String value;
    private String baseValue;
    private String partOfSpeech; // Part of speech
    private int sentencePosition;

    private List<SemanticRelation> relations;
    private SentenceTree sentence;
    private SemanticGroup group;


    public SentenceToken(String value) {
        this.value = value;
        relations = new LinkedList<SemanticRelation>();
    }


    public SentenceToken(String value, SemanticRelation relation) {
        this.value = value;
        relations = new LinkedList<SemanticRelation>();
        relations.add(relation);
    }

    public String toString() {
        return "ST{" +
                "val='" + value + '\'' +
                ", pos='" + partOfSpeech + '\'' +
                '}';
    }

    public SentenceTree getSentence() {
        return sentence;
    }

    public void setSentence(SentenceTree sentence) {
        this.sentence = sentence;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public SemanticGroup getGroup() {
        return group;
    }

    public void setGroup(SemanticGroup group) {
        this.group = group;
    }

    public int getSentencePosition() {
        return sentencePosition;
    }

    public void setSentencePosition(int sentencePosition) {
        this.sentencePosition = sentencePosition;
    }

    public String getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(String baseValue) {
        this.baseValue = baseValue;
    }


    public boolean hasRelations() {
        return relations != null && relations.size() > 0;
    }

    public void removeRelation(SemanticRelation relation) {
        if (relations != null) {
            relations.remove(relation);
        }
    }

    public List<SemanticRelation> getRelations() {
        return relations;
    }

    public void addRelation(SemanticRelation relation) {
        relations.add(relation);
    }

    public void setRelations(List<SemanticRelation> relations) {
        this.relations = relations;
    }

    public String getValue() {
        return value;
    }

    /**
     * Converts characters to lowercase and removes spaces at the beginning
     *
     * @return
     */
    public String getCleared() {
        return value.trim().toLowerCase();
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * The method checks that the previous token in sentence is a words delimiter : and / or /,
     *
     * @return true or false
     */
    public boolean isPreviousTokenDelimiter() {
        List<SentenceToken> tokens = getGroup().getSentenceTree().getTokens();
        return sentencePosition > 0 && tokens.get(sentencePosition - 1).isDelimiter();
    }

    /**
     * The method checks that the token in sentence is a words delimiter : and / or /,
     *
     * @return
     */
    private boolean isDelimiter() {
        String cleared = getCleared();
        return cleared.equals("and") || cleared.equals("or") || cleared.equals(",");
    }


    public boolean isServiceToken() {
        String cleared = getCleared();
        return cleared.equals("goto") || cleared.equals("then");
    }


    public boolean matchValue(SentenceToken token) {
        return getValue().trim().equals(token.getValue().trim());

    }
}
