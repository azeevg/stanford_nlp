package grammarscope.parser;

import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.utility.semantic.SentenceUtils;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

/**
 * Parsed document
 *
 * @author Bernard Bou
 */
public class ParsedDocument extends ScannedDocument {
    /**
     * Parsed tree per sentence
     */
    public Map<Sentence, Tree> theParsedTrees;

    /**
     * leaf-to-word map
     */
    public Map<Tree, Word> theLeafToWordMap;


    //protected DBController controller;

    //private HashMap<Integer, Tree> cachedTrees;

    //protected String title;

    //private static String CACHE_TABLE = "sentence_cache";

    protected StanfordDataModule dataModule;

    public ParsedDocument() {
        super();
    }

    /**
     * Constructor
     */
    //public ParsedDocument(HashMap<Integer, Tree> cachedTrees, String title, DBController controller) {
    //super();
    //this.controller = controller;
    //this.cachedTrees = cachedTrees;
    //this.title = title;
    //}
    public ParsedDocument(StanfordDataModule dataModule) {
        super();
        this.dataModule = dataModule;
    }

    /**
     * Parse scanned document
     *
     * @param thisParser parser
     */
    public void parse(final Parser thisParser) throws Exception {
        if (thisParser == null)
            return;
        if (this.theSentences == null)
            return;

        this.theParsedTrees = new IdentityHashMap<Sentence, Tree>();
        this.theLeafToWordMap = new IdentityHashMap<Tree, Word>();
        for (Sentence thisSentence : this.theSentences) {
            //final Tree thisTree = thisParser.parse(thisSentence);
            //final Tree thisTree = getParsedTree(thisParser, thisSentence);
            final Tree thisTree = dataModule.getTree(thisSentence);
            if (thisTree != null) {
                Sentence sentence = SentenceUtils.convertSentence(thisSentence,thisParser.getCollocationsIndex());
                System.out.println(this.theParsedTrees);
                this.theParsedTrees.put(sentence, thisTree);
               // System.out.println("###ParsedTrees: " + this.theParsedTrees.get(sentence));
                //Change by Irina Daniel
               // ParsedDocument.leaves2Words(this.theLeafToWordMap, thisSentence, thisTree, thisParser.getCollocationsIndex());
                ParsedDocument.leaves2Words(this.theLeafToWordMap, sentence, thisTree);
            }
        }
    }

    /**
     * Irina Daniel
     * @param thisParser
     * @param sentences
     * @throws Exception
     */
    public void parse(final Parser thisParser, List<Sentence> sentences) throws Exception {
        if (thisParser == null)
            return;
        if (sentences == null)
            return;

        this.theParsedTrees = new IdentityHashMap<Sentence, Tree>();
        this.theLeafToWordMap = new IdentityHashMap<Tree, Word>();
        for (Sentence thisSentence : sentences) {
            final Tree thisTree = dataModule.getTree(thisSentence);
            if (thisTree != null) {
                this.theParsedTrees.put(thisSentence, thisTree);
                //Change by Irina Daniel
                ParsedDocument.leaves2Words(this.theLeafToWordMap, thisSentence, thisTree);
            }
        }
        this.theSentences = sentences;
    }

  /*public Tree getParsedTree(final Parser thisParser, Sentence sentence) throws Exception {
    Integer hash = sentence.toString().hashCode();
     ca
    if (cachedTrees.containsKey(hash)) {
      //LoggingUtility.info("CACHED SENTENCE = " + sentence.toString());
      return cachedTrees.get(hash);
    } else {
      //LoggingUtility.info("PARSED SENTENCE = " + sentence.toString());
      final Tree thisTree = thisParser.parse(sentence);
      controller.insertObject(CACHE_TABLE, title, hash + "", getBytes(thisTree));
      return thisTree;
    }
  }


  public static byte[] getBytes(Object obj) throws IOException {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(bos);
    oos.writeObject(obj);
    oos.flush();
    oos.close();
    bos.close();
    byte[] data = bos.toByteArray();
    return data;
  }       */

    /**
     * Parse scanned document
     *
     * @param thisParser   parser
     * @param thisSentence sentence
     * @return true if successful
     */
    public Tree parse(final Parser thisParser, final Sentence thisSentence) {
        if (thisParser == null)
            return null;
        if (this.theSentences == null)
            return null;

        final Tree thisTree = thisParser.parse(thisSentence);
        if (thisTree != null) {
            this.theParsedTrees.put(thisSentence, thisTree);
            ParsedDocument.leaves2Words(this.theLeafToWordMap, thisSentence, thisTree);
            return thisTree;
        }
        return null;
    }

    public void oneSentenceInit(Sentence sentence, Tree sentenceTree) {
        this.theLeafToWordMap = new IdentityHashMap<Tree, Word>();
        this.theParsedTrees = new IdentityHashMap<Sentence, Tree>();

        this.theParsedTrees.put(sentence, sentenceTree);
        ParsedDocument.leaves2Words(this.theLeafToWordMap, sentence, sentenceTree);
    }







    /**
     * Init parse
     *
     * @param thisParser parser
     * @return true if successful
     */
    public boolean parse_init(final Parser thisParser) {
        if (thisParser == null)
            return false;
        if (this.theSentences == null)
            return false;

        this.theParsedTrees = new IdentityHashMap<Sentence, Tree>();
        this.theLeafToWordMap = new IdentityHashMap<Tree, Word>();
        return true;
    }

    /**
     * Map leaves to words
     *
     * @param thisMap      map to receive results
     * @param thisSentence sentence
     * @param thisTree     sentence parsed tree
     */
    static void leaves2Words(final Map<Tree, Word> thisMap, final Sentence thisSentence, final Tree thisTree) {

        final List<Tree> theseLeaves = thisTree.getLeaves();

        for (int i = 0; i < theseLeaves.size(); i++) {
            final Tree thisLeaf = theseLeaves.get(i);
            final Word thisWord = thisSentence.get(i);
            final Label thisLeafLabel = thisLeaf.label();
            //System.out.println("thisLeafLabel.value().toString():" + thisLeafLabel.value().toString());
           // System.out.println("thisWord.toString():" + thisWord.toString());
            if (!thisLeafLabel.value().toString().equals(thisWord.toString()))
                throw new IllegalArgumentException("LEAF-WORD MISMATCH:" + thisLeafLabel + "<->" + thisWord);
            // System.out.println("LEAF-WORD MATCH:" + thisLeafLabel + "<->" + thisWord);
            thisMap.put(thisLeaf, thisWord);
        }
    }



    /**
     * Get leaf words
     *
     * @param thisTree parsed tree
     * @return leaf words
     */
    public List<Word> getWords(final Tree thisTree) {
        final List<Word> thisList = new ArrayList<Word>();
        for (final Tree thisLeaf : thisTree.getLeaves()) {
            thisList.add(this.theLeafToWordMap.get(thisLeaf));
        }
        return thisList;
    }

    /**
     * Locate
     *
     * @param thisSentence sentence
     * @param thisTree     parsed tree
     * @return location
     */
    public Location getLocation(final Sentence thisSentence, final Tree thisTree) {
        // System.out.print(">LOCATING: " + PrintHelpers.treeToString(thisTree));
        final List<Tree> theseLeaves = thisTree.getLeaves();
        if (theseLeaves.isEmpty())
            return null;

        final Word thisFromWord = this.theLeafToWordMap.get(theseLeaves.get(0));
        final Word thisToWord = this.theLeafToWordMap.get(theseLeaves.get(theseLeaves.size() - 1));
        final Location thisLocation = getLocation(thisSentence, thisFromWord, thisToWord);
        // System.out.println("<LOCATED: " + thisLocation.toString());
        return thisLocation;
    }
}
