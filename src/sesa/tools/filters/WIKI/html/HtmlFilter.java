package sesa.tools.filters.WIKI.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import sesa.common.configuration.Categories;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.Application;
import sesa.common.utility.strings.StringUtility;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;


public class HtmlFilter extends Application {

    private static String END_OF_CONTENT = "----------EndOfContent-------";
    private DocumentsDao documentsDao = new DocumentsDao();

    public HtmlFilter() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        String[] categoryNames = Categories.ALL_CATEGORIES;
        for (String categoryName : categoryNames) {

            String collectionName = Categories.buildCategoryCollectionName(categoryName);
            String htmlCollectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
            documentsDao.truncateOrCreateDocumentCollection(htmlCollectionName);
          //  dbController.truncateOrCreate(htmlCollectionName);
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(collectionName);
            //dbController.getDocumentsCollectionIterator(collectionName);
            iterator = new DBReportingIterator(iterator, "Converting to html for category " + categoryName);

            while (iterator.hasNext()) {
                String title = null;
                try {
                    IDBEntity entity = iterator.next().get();
                    Map<String, Object> map = entity.toMap();
                    title = (String) map.get("title");
                    String text = (String) map.get("text");
                    String wikiHtml = processWikiPage(text, title);
                    map.put("w_filtered_text", wikiHtml);
                    documentsDao.addToDocumentsCollection(htmlCollectionName, entity);
                    //dbController.insertToDocumentsCollection(htmlCollectionName, entity);
                } catch (Exception e) {
                    error("Error during parsing page with title = " + title);
                    error(e.getMessage());
                }
            }
        }
        return true;
    }

    private String processWikiPage(String wikiText, String title) {
        FilteringWikiModel wikiModel = new FilteringWikiModel("${image}", "${title}");
        String content = wikiText;
        String wikiHtml = wikiModel.render(new FilteringHtmlConverter(false, true), content);

        Document doc = Jsoup.parse(wikiHtml);
        removeTableOfContents(doc);
        removeTables(doc);
        removeLists(doc);
        removeNonTextHrefs(doc);
        removeDirtyBlocks(doc);
        boolean hasContentLine = addContentLine(doc);

        String wikiStructureHtml = doc.textWithHeaderStructure();
        wikiStructureHtml = wrapToHtml(extractContent(wikiStructureHtml, hasContentLine), title);
        return wikiStructureHtml;
    }


    private void removeTableOfContents(Document doc) {
        doc.select("table#toc").remove();
    }

    private void removeTables(Document doc) {
        doc.select("table").remove();
    }

    private void removeLists(Document doc) {
        doc.select("ul").remove();
        doc.select("ol").remove();
    }

    private void removeNonTextHrefs(Document doc) {
        doc.select("a:contains(:)").remove();
    }

    private void removeDirtyBlocks(Document doc) {
        doc.select("p:contains(|})").remove();
        doc.select("p:contains({|)").remove();
    }

    private boolean addContentLine(Document doc) {
        String[] stopSpans = new String[]{"See also", "References", "Further reading", "External links", "Notes"};
        String stopString = StringUtility.join(Arrays.asList(stopSpans), "|");
        Elements stopHeaders = doc.select("span:matchesOwn(" + stopString + ")");
        int size = stopHeaders.size();
        if (size > 0)
            stopHeaders.first().parent().before(END_OF_CONTENT);
        return size > 0;
    }

    private String extractContent(String text, boolean hasContentLine) {
        if (!hasContentLine)
            return text;
        return text.substring(0, text.indexOf(END_OF_CONTENT));
    }

    private String wrapToHtml(String text, String title) {
        return "<html><head><title>" + title + "</title></head><body>" + text + "</body></html>";
    }
}
