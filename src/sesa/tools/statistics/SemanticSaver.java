package sesa.tools.statistics;

import edu.stanford.nlp.io.IOUtils;

import sesa.common.dataModule.ReadTreeDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.configuration.Categories;
//import sesa.common.database.common.DBController;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.CommonDao;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.database.dao.SemanticGroupDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.builder.DocumentBuilder;
import sesa.common.utility.vector.DocumentUtils;
import sesa.common.builder.StanfordRelationsBuilder;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;

public class SemanticSaver extends Application {

    String categoryName = Categories.COMPUTING_CATEGORY;
    volatile double totalCount = 0;
    volatile double totalErrors = 0;
    private DocumentsDao documentsDao = new DocumentsDao();

    public SemanticSaver() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() {

        try {

            LoggingUtility.info("SAVING GROUPS FOR CATEGORY  = " + categoryName);
            //RunSequentialTest();
            RunParallelVersion();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void RunSequentialTest() throws Exception {
        //dbController.useConnection(0);
        String groupTable = "semantic_groups_" + categoryName;
        //dbController.truncateOrCreate(groupTable);
        IterateTask task = new IterateTask(0, 100, "Sequential", groupTable);
        task.call();
    }


    private void RunParallelVersion() throws IOException, InterruptedException, ExecutionException, SQLException, ClassNotFoundException {
        //dbController.useConnection(0);
        String groupTable = "semantic_groups_" + categoryName;
        //dbController.truncateOrCreate(groupTable);
        String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);

        long collectionSize = documentsDao.countTotalOfRow(collectionName);//dbController.getCollectionSize(collectionName);
        int nrOfProcessors = Runtime.getRuntime().availableProcessors() - 3; // number of process
        System.out.println("number of processors:" + nrOfProcessors);
        ExecutorService eService = Executors.newFixedThreadPool(nrOfProcessors);
        CompletionService<Object> cService = new ExecutorCompletionService<Object>(eService);

        //intentionally reduced collection size to avoid unprocessed tail of collection
        //collectionSize = 12000;
        //collectionSize  = collectionSize / 2;

        //nrOfProcessors = nrOfProcessors;
        //intentionally reduced to avoid 100% load

        int[][] split = new int[nrOfProcessors][2];
        long shift = collectionSize / nrOfProcessors;
        for (int i = 0; i < nrOfProcessors; i++) {
            split[i][0] = (int) shift * i;
            split[i][1] = (int) shift * (i + 1);
        }
        split[nrOfProcessors - 1][1] += collectionSize % nrOfProcessors;


        for (int i = 0; i < nrOfProcessors; i++) {
            cService.submit(new IterateTask(split[i][0], split[i][1], "Thread " + i, groupTable));
        }

        for (int i = 0; i < nrOfProcessors; i++) {
            cService.take().get();
            System.out.println("Iteration completed");
        }

        LoggingUtility.info("TOTAL COUNT = " + totalCount);
        LoggingUtility.info("TOTAL COL SIZE = " + collectionSize);
        LoggingUtility.info("TOTAL ERRORS = " + totalErrors);
        System.exit(0);
    }


    public class IterateTask implements Callable {

        private static final int SENTENCE_SIZE_LOWER_LIMIT = 0;
        private static final int SENTENCE_SIZE_UPPER_LIMIT = 50;
        private int lower;
        private int upper;
        private String label;
        private int errorBlocker = 0;
        private StanfordDataModule dataModule;
        private DocumentBuilder documentBuilder;
        // private DocumentsDao documentsDao = new DocumentsDao();
        private SemanticGroupDao semanticGroupDao = new SemanticGroupDao();
        private CommonDao commonDao = new CommonDao();
       // private DBController controller;
        private ObjectInputStream inputStream;
        private String groupTable;


        public IterateTask(int lower, int upper, String label, String groupTable) throws IOException, SQLException, ClassNotFoundException {
            this.lower = lower;
            this.upper = upper;
            this.label = label;

            //this.controller = controller;
            this.groupTable = groupTable;
            init();
        }

        // TODO: Change "./data/grammar/englishPCFG.ser.gz"
        private synchronized void init() throws IOException, SQLException, ClassNotFoundException {
            inputStream = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz");
            String cacheTable = "tree_cache_" + categoryName;
            dataModule = new ReadTreeDataModule(cacheTable);//CacheTreeDataModule(controller, cacheTable);
            documentBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
            inputStream = null;
            System.gc();
        }

        private synchronized void reset() {
            inputStream = null;
            dataModule = null;
            documentBuilder = null;
            System.gc();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            System.gc();
        }

        private void saveSemanticGroup(SemanticGroup group) {

            String groupId = UUID.randomUUID().toString();
            for (SentenceToken token : group.getTokens()) {
                for (SemanticRelation relation : token.getRelations()) {

                    SentenceTree sentence = group.getSentenceTree();
                    int serviceTokens = sentence.containsServiceTokens() ? 1 : 0;
                    int prevDelimiter = token.isPreviousTokenDelimiter() ? 1 : 0;
                    String value = token.getBaseValue();
                    String base = value != null ? value : "";
                    semanticGroupDao.saveSemanticRelation(groupTable, token.getValue(), base,
                            relation.getShortName(), token.getPartOfSpeech(), token.getSentencePosition(),
                            groupId, sentence.getSentence().hashCode(), serviceTokens, prevDelimiter, sentence.getSize());

                  /*  controller.saveSemanticRelation(groupTable, token.getValue(), base,
                            relation.getShortName(), token.getPartOfSpeech(), token.getSentencePosition(),
                            groupId, sentence.getSentence().hashCode(), serviceTokens, prevDelimiter, sentence.getSize());
                */
                }
            }
        }


        public Object call() throws Exception {
            try {

                String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
                long collectionSize = commonDao.countTotalOfRow(collectionName);//dbController.getCollectionSize(collectionName);
                System.out.println(collectionSize);
                System.out.println(lower + " " + upper);
                IDBIterator iterator = commonDao.getDocumentsCollectionIterator(collectionName);//controller.getDocumentsCollectionIterator(collectionName);

                int counter = 0;
                while (iterator.hasNext()) {
                    counter++;
                    IDBEntity entity = iterator.next().get();
                    if (counter > lower && counter < upper) {
                        try {
                            String title = (String) entity.toMap().get("title");
                            System.out.println(label + " = " + title);
                            IDocument document = DocumentUtils.createDocument(entity, documentBuilder);

                            List<SentenceTree> sentences = document.getSentences();

                            System.out.println(sentences.get(1).getSentence());

                            System.out.println(sentences.size() + "?" + SENTENCE_SIZE_LOWER_LIMIT );

                            //&& sentences.size() < SENTENCE_SIZE_UPPER_LIMIT
                            //(sentences.size() <= SENTENCE_SIZE_LOWER_LIMIT)
                            if (sentences.size() > SENTENCE_SIZE_LOWER_LIMIT && sentences.size() <= SENTENCE_SIZE_UPPER_LIMIT) {
                                for (SentenceTree sentence : sentences) {
                                    for (SemanticGroup groups : sentence.getSemanticGroups()) {
                                        saveSemanticGroup(groups);
                                    }
                                }
                            }
                            errorBlocker = 0;
                        } catch (Exception e) {
                            totalErrors++;
                            errorBlocker++;

                            if (errorBlocker > 10) {
                                LoggingUtility.info("Ahead of schedule completion");
                                break;
                            }
                            System.out.println("Error in" + label + " " + e.getLocalizedMessage());
                            reset();
                            init();
                        }
                        totalCount++;

                        if (totalCount % 100 == 0)
                            LoggingUtility.info("COMPLETED = " + totalCount / collectionSize * 100 + " %");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
    }
}
