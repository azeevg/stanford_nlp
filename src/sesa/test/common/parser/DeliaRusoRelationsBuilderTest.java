package sesa.test.common.parser;

import edu.stanford.nlp.io.IOUtils;

import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Parser;
import grammarscope.parser.Sentence;
import org.junit.Before;
import org.junit.Test;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.relationsBuilder.DeliaRusoRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.semantics.TripletFinder;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.semantic.SentenceUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Ira on 16.05.2016.
 */
public class DeliaRusoRelationsBuilderTest {
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";

    @Before
    public void init(){

    }
    @Test
    public void testFindTripletsInText() throws IOException{
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel); //parserModel


     //   String text = FileUtility.readAllText("./data/ada_cache/db_text_2.txt");
        String text = FileUtility.readAllText("./data/test.txt");
        StanfordDataModule dataModule = new BasicDataModule();

        try{
            DocumentBuilder builder = new DeliaRusoRelationsBuilder(inputStream);
            builder.parseText("title", text);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testFindTriplet() throws IOException{
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile("data/test.txt");
        Tree parse = parser.parse(sentence, true);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        try {
            parse.indentedXMLPrint(writer);
        } catch (NullPointerException e) {
           e.printStackTrace();
        }
        String xmlTree = stringWriter.toString();
        System.out.println("xmlTree:" + xmlTree);
        SentenceTriplet triplet = TripletFinder.findSentenceTriplet(xmlTree);
        triplet.setSentence(sentence.toString());
        printTriplet(triplet);
    }

    public void printTriplet(SentenceTriplet triplet) {
        System.out.println("Sentence: " + triplet.getSentence());
        System.out.println("Triplet subj - pred - obj : " + triplet.getSubject() + " - " + triplet.getPredicate() + " - " + triplet.getObject());

    }
}
