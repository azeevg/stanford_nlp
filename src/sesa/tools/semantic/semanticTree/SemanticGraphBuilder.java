package sesa.tools.semantic.semanticTree;

import sesa.common.database.dao.dictionary.SemanticStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.tools.semantic.VisualizationGraph;
import sesa.common.database.entity.SemanticGroupEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <b>Semantic graph based on found semantic groups</b>
 * Created by Ira on 01.06.2016.
 */
public class SemanticGraphBuilder {

    private SemanticStructureDao semanticStructureDao;
    private TermDao termDao;
    private VisualizationGraph vis;

    public SemanticGraphBuilder() throws SQLException, ClassNotFoundException {
        semanticStructureDao = new SemanticStructureDao();
        termDao = new TermDao();
        vis = new VisualizationGraph();
    }

    private SemanticNode buildSemanticGraph(int id, Set<Integer> vertex, List<SemanticEdge> edge) {
        SemanticNode node = new SemanticNode();
        node.setIdTerm(id);
        List<Integer> listIds = new ArrayList<Integer>();
        node = readSemanticStructure(node, vertex, edge, listIds);
        return node;
    }

    public void buildAndPrintSemanticGraphForTerm(String term, String outputFileEdge, String outputFileVertex) {
        int id = termDao.getTermByName(term).getId();
        buildAndPrintSemanticGraphForTerm(id, outputFileEdge, outputFileVertex);
    }

    public void buildAndPrintSemanticGraphForTerm(int id, String outputFileEdge, String outputFileVertex) {
        Set<Integer> vertex = new HashSet<Integer>();
        List<SemanticEdge> edge = new ArrayList<SemanticEdge>();
        SemanticNode node = buildSemanticGraph(id, vertex, edge);
        // plot semantic graph
        printSemanticGraph(node, 0);
        // print data to output file
        vis.printEdgesToFile(outputFileEdge, edge);
        vis.printVertexToFile(outputFileVertex, vertex);
    }

    private void printSemanticGraph(SemanticNode parentNode, int i) {
        int tmp = 3;
        if (parentNode.getChildList().size() > 0 && i == 0) {
            System.out.println(getTab(i * tmp) + "i:" + i + "  parent: " + termDao.getTermById(parentNode.getIdTerm()).getHeadWord());
        }

        for (SemanticNode child : parentNode.getChildList()) {
            System.out.println(getTab((i + 1) * tmp) + "i:" + (i + 1) + "  " + termDao.getTermById(child.getIdTerm()).getHeadWord());
            printSemanticGraph(child, i + 1);
        }
    }

    /**
     * Building a graph of depth of no more than MAX
     * Fill data list of SemanticEdge and vertex
     * @param parentNode
     * @param vertex
     * @param edge
     * @param listIds additional list for builder
     * @return
     */
    private SemanticNode readSemanticStructure(SemanticNode parentNode,
                                               Set<Integer> vertex,
                                               List<SemanticEdge> edge,
                                               List<Integer> listIds) {
        List<SemanticGroupEntity> list = semanticStructureDao.getListSemanticGroupDtoByTermID(parentNode.getIdTerm());
        vertex.add(parentNode.getIdTerm());
        if (listIds.contains(parentNode.getIdTerm())) {
            return parentNode;
        } else {
            listIds.add(parentNode.getIdTerm());
        }

        if (list.size() == 0) {
            return parentNode;
        }

        for (SemanticGroupEntity dto : list) {
            for (int linkId : dto.getTermLink()) {
                SemanticNode child = new SemanticNode(linkId);
                parentNode.addChild(child);
                vertex.add(linkId);
                edge.add(new SemanticEdge(parentNode.getIdTerm(), linkId));
                readSemanticStructure(child, vertex, edge, listIds);
            }
        }
        return parentNode;
    }

    private String getTab(int i) {
        String s = "";
        for (int j = 0; j < i; j++) {
            s += " ";
        }
        return s;
    }
}
