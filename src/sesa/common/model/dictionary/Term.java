package sesa.common.model.dictionary;

import java.util.List;

/**
 * Created by Ira on 13.05.2016.
 */
public class Term {

    private int id;
    private String name;
    private String def;
    public int linksSize;
    private List<String> links;

    public Term(String name, String def) {
        this.name = name;
        this.def = def;
    }

    public Term(String name, String def, List<String> links) {
        this.name = name;
        this.def = def;
        this.links = links;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDef() {
        return def;
    }

    public void setDef(String def) {
        this.def = def;
    }

    public int getLinksSize() {
        return linksSize;
    }

    public void setLinksSize(int linksSize) {
        this.linksSize = linksSize;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
