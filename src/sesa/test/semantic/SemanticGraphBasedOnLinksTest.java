package sesa.test.semantic;

import org.junit.Test;
import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.tools.semantic.VisualizationGraph;
import sesa.tools.semantic.semanticTree.SemanticGraphBasedOnLinks;

import java.sql.SQLException;

/**
 * Created by Ira on 05.06.2016.
 */
public class SemanticGraphBasedOnLinksTest {
    //"link_comp_structure"
    //"auto_comp_reference"

    @Test
    public void testBuild() throws SQLException, ClassNotFoundException {
        SemanticGraphBasedOnLinks builder = new SemanticGraphBasedOnLinks();
        builder.buildGraph("Win Folder organization");
    }

    @Test
    public void testBuildSemanticGraphBasedOnReference() throws SQLException, ClassNotFoundException {
        SemanticGraphBasedOnLinks builder = new SemanticGraphBasedOnLinks();
        builder.setCollectionName("auto_comp_reference");
        builder.buildGraph(22754);
    }

    @Test
    public void testCalcVertex() throws SQLException, ClassNotFoundException {
        LinkStructureDao dao = new LinkStructureDao();
        int n = dao.getNumberVertex("link_comp_structure");//"link_comp_structure" "auto_comp_reference"
        System.out.println(n);
    }

    @Test
    public void testPrintVertexToFile() throws SQLException, ClassNotFoundException {
        VisualizationGraph visualizationGraph = new VisualizationGraph();
        visualizationGraph.printVertexToFile("data/graph_vertex.txt");
        visualizationGraph.printEdgesToFile("data/graph_edges.txt");
    }
}
