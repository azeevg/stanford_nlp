package sesa.tools.filters.WIKI.category;

import sesa.common.configuration.Configuration;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.Application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ByCategoryMySqlFilter extends Application {

    private final static String CAT_LIST = "cat_list"; // what is it?
    private DocumentsDao documentsDao = new DocumentsDao();

    public ByCategoryMySqlFilter() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() {

        List<String> categories = new LinkedList<String>();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(configuration.getProperty(CAT_LIST)));
            String text = null;

            while ((text = reader.readLine()) != null) {
                if (text.length() > 0)
                    categories.add(mysqlEscaping(text).trim());
            }


            System.out.println(categories);
            documentsDao.truncateDocumentCollection(Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI);
         //  dbController.removeByCategoryDocuments(); // truncate(Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI)
            String regexp = ",[[:blank:]]" + join(categories, "|") + "[[.comma.][.right-square-bracket.]]{1}";
            System.out.println(regexp);
            documentsDao.addByCategoryDocuments(regexp);
            //dbController.addByCategoryDocuments(regexp);
          //  LoggingUtility.info(dbController.countByCategoryDocuments() + "");

            reader.close();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public String mysqlEscaping(String category) {
        category = category.replaceAll("\\s", "[[:blank:]]");
        category = category.replaceAll("\\-", "[[:hyphen-minus:]]");
        category = category.replaceAll("\\)", "[[:right-parenthesis:]]");
        category = category.replaceAll("\\(", "[[:left-parenthesis:]]");
        category = category.replaceAll("'", "[[:apostrophe:]]");
        return category;
    }

    private static String join(Iterable<?> pColl, String separator) {
        Iterator<?> oIterator;
        if (pColl == null || (!(oIterator = pColl.iterator()).hasNext()))
            return "";
        StringBuilder oBuilder = new StringBuilder(String.valueOf(oIterator.next()));
        while (oIterator.hasNext())
            oBuilder.append(separator).append(oIterator.next());
        return oBuilder.toString();
    }
}
