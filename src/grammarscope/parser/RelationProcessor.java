package grammarscope.parser;

/**
 * Relation processor interface
 * 
 * @author Bernard Bou
 */
public interface RelationProcessor
{
	/**
	 * Process relation
	 * 
	 * @param thisRelation
	 *            relation
	 */
	public void process(MutableGrammaticalRelation thisRelation);
}
