package sesa.common.database.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Semantic group contains dependency between term,
 * links( which are terms)
 * and sentence where we meet that links
 * Created by Ira on 01.06.2016.
 */
public class SemanticGroupEntity {
    private int idTerm;
    private String groupId;
    private String text_group;
    private int sentenceId;
    private List<Integer> termLink;

    public SemanticGroupEntity() {
    }

    public SemanticGroupEntity(String groupId, String text_group, int sentenceId) {
        this.groupId = groupId;
        this.text_group = text_group;
        this.sentenceId = sentenceId;
        this.termLink = new ArrayList<Integer>();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getText_group() {
        return text_group;
    }

    public void setText_group(String text_group) {
        this.text_group = text_group;
    }

    public int getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(int sentenceId) {
        this.sentenceId = sentenceId;
    }

    public int getIdTerm() {
        return idTerm;
    }

    public void setIdTerm(int idTerm) {
        this.idTerm = idTerm;
    }

    public List<Integer> getTermLink() {
        return termLink;
    }

    public void setTermLink(List<Integer> termLink) {
        this.termLink = termLink;
    }

    @Override
    public String toString() {
        return "SemanticGroupEntity{" +
                "termLink=" + termLink +
                ", idTerm=" + idTerm +
                ", text_group='" + text_group + '\'' +
                '}';
    }
}
