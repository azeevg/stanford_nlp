package sesa.application;

import sesa.common.configuration.Categories;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.utility.common.Application;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.utility.dictionary.DictionaryUtils;
import sesa.common.utility.html.HtmlUtils;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.vector.DocumentUtils;
import sesa.common.utility.vector.VectorUtils;
import sesa.tools.math.metrics.EuclideanMetrics;
import sesa.tools.math.metrics.IMetrics;
import sesa.tools.math.Vector;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestColoredPage extends Application {

    private static final String COLORED_DIR_PATH = "colored_pages";
    private DocumentsDao documentsDao = new DocumentsDao();

    public TestColoredPage() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        IMetrics metrics = new EuclideanMetrics();
        String categoryName = Categories.PHYSICS_CATEGORY;
        Dictionary dictionary = DictionaryUtils.loadDictionary();

        LoggingUtility.info("Loading dictionary...");
        Set<String> categoryDictionary = DictionaryUtils.loadCategoryDictionary(categoryName);

        LoggingUtility.info("Loading documents...");
        List<IDocument> documents = DocumentUtils.loadDocuments(categoryName);

        List<Vector> vectors = VectorUtils.createVectors(documents, dictionary);
        LoggingUtility.info("All vectors = " + vectors.size());


        List<Vector> zeroVectors = new LinkedList<Vector>();
        for (Vector vector : vectors) {
            if (vector.size() == 0) {
                zeroVectors.add(vector);
            }
        }

        LoggingUtility.info("Zero vectors = " + zeroVectors.size());

        /**************Choose zero vector here ***************/
        Vector vector = zeroVectors.get(1);
        /**************Choose zero vector here ***************/

        String id = vector.getId();
        Map<String, Object> map = documentsDao.getDocumentById(Categories.buildCategoryHtmlCollectionName(categoryName), id).toMap();

        String html = (String) map.get("w_filtered_text");
        String paintedHtml = HtmlUtils.paintWikiHtmlPage(html, categoryDictionary);

        String filePath = configuration.getProperty(COLORED_DIR_PATH) + "/" + id + ".html";
        String dictionaryPath = configuration.getProperty(COLORED_DIR_PATH) + "/" + "dict.txt";
        FileUtility.writeAllTextToFile(filePath, paintedHtml);
        LoggingUtility.info("Generated html colored file = " + filePath);
        FileUtility.writeAllLinesToFile(dictionaryPath, categoryDictionary);
        LoggingUtility.info("Generated dictionary file = " + dictionaryPath);
        return true;
    }

}
