package sesa.test.dictionary;

import org.junit.Test;
import sesa.common.configuration.Configuration;
import sesa.common.database.dao.CrossReferenceDao;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.dictionary.downloader.LinksDownloader;
import sesa.common.utility.io.DirectoryUtility;

import java.io.File;
import java.sql.SQLException;

/**
 * Created by Ira on 03.05.2016.
 */
public class LinksDownloaderTest {

    @Test
    public void testDownloader() throws SQLException, ClassNotFoundException {
        FileManager fileManager = new FileManager("path");
        LinksDownloader downloader = new LinksDownloader();
        File[] files = DirectoryUtility.getAllFilesDirectories(fileManager.getValue("path.dictionaryLinks"));
        CrossReferenceDao referenceDao = new CrossReferenceDao();

        for (File f : files) {
            referenceDao.addToRefCollections(downloader.downloaderLinksFromFile(f.toString()), Configuration.DB_COLLECTION_COMPUTER_DICTIONARY_CROSS_REF);
        }

    }

    @Test
    public void testExtractDownloader() {
        FileManager fileManager = new FileManager("path");
        LinksDownloader downloader = new LinksDownloader();
        File[] files = DirectoryUtility.getAllFilesDirectories(fileManager.getValue("path.dictionary"));
        for (File f : files) {
            System.out.println(f);
        }
    }


}
