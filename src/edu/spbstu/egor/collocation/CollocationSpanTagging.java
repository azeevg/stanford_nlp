/**
 * Created by Egor Gorbunov on 09.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */

package edu.spbstu.egor.collocation;

import edu.spbstu.appmath.collocmatch.*;
import edu.spbstu.egor.*;
import edu.spbstu.egor.wrapper.CollocationSpanTaggingParser;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.trees.Tree;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * <p>
 *     That class usage assumption, that if we parse collocation pairs, triples and etc. as one semantic unit,
 *     parse results (here parse results are the syntactic trees) would be better (more similar to expert parse)
 * </p>
 * <p>
 *     The way it works is pretty simple. Firstly we read usage sentences which contain collocations. After that we
 *     extract collocations from that sentences (actually that collocations must be read from some files or something,
 *     but for better performance we look at collocations which are presented in target sentence set). For every
 *     collocation {@code {w1, w2, ...}}  we add the word {@code w1w2...} (collocation words concatenation) to grammar
 *     like we saw that synthetic words during training phase with noun tag (NN). After that sentences are parsed with
 *     standard {@link edu.stanford.nlp.parser.lexparser.ExhaustivePCFGParser} and with (!) it's modified version:
 *     {@link CollocationSpanTaggingParser}, which uses such parameters of Stanford parser
 *     like {@code maxSpanForTags} -- max number of consequent words in the sentence, that can be considered as one
 *     unit and so can be tagged as one semantic unit (as said in some comments in Stanford Parser that option is only
 *     applicable to non-english languages, ex. Chinese, but we want to use that feature with English too).
 *     So accordingly to collocation dictionary we know maximum number of words, that may occur in collocation and so
 *     that value is considered as {@code maxSpanForTags}.
 * </p>
 */
public class CollocationSpanTagging {
    private static final Logger logger = Logger.getLogger(CollocationReplace.class.getSimpleName());

    private static Tree ERROR_TREE;

    private static final String MATCH_BANK_FILENAME = "./egor_in/colloc_sentences/all_matches.txt";

    // SETTINGS
    private static final boolean DO_TREE_DUMP = false;
    private static final boolean DO_FILTER = true;
    private static final boolean DO_READ_BEST_TREES_FROM_FILE = true;
    private static final boolean DO_READ_STANF_BEST_TREES_FROM_FILE = true;

    private static final boolean DO_PDF_COMPILE = true;
    private static final boolean DO_TREE_COMPARE_AND_PRINT = false;


    private static MatchBank matchBank;
    private static Grammar grammar;
    private static TreeGetter stanfordTreeGetter;
    private static TreeGetter collocationExtTreeGetter;
    private static TreeUtils treeUtils;
    private static ParserFactory parserFactory;

    private static void prepare() throws IOException {
        matchBank = new MatchBank();

        logger.info("Reading sentences with matched collocations.");
        matchBank.read(new BufferedReader(new FileReader(new File(MATCH_BANK_FILENAME))));

        logger.info("Initializing grammar.");
        grammar = new Grammar();
        grammar.op.testOptions.verbose = false;

        parserFactory = new ParserFactory(grammar);

        stanfordTreeGetter = new TreeGetter();
        stanfordTreeGetter.setPparser(parserFactory.getMyParser());

        treeUtils = new TreeUtils(grammar.op);

        ERROR_TREE = stanfordTreeGetter.getBestParseTree(SentenceGetter.convert(new Sentence("ERROR TREE")));
        assert ERROR_TREE != null;
    }

    public static void changeGrammar() throws IOException {
        CollocationDictionary collocationDictionary = matchBank.extractCollocationDictionary();

        CollocationSpanTaggingParser collocationExtendedParser =
                parserFactory.getCollocationSpanTaggingParser(collocationDictionary);

        logger.info("Refreshing state number and stuff in CollocationExtendedParser object.");
        collocationExtendedParser.refreshStateNumber();

        collocationExtTreeGetter = new TreeGetter();
        collocationExtTreeGetter.setPparser(collocationExtendedParser);
    }

    public static void main(String[] args) throws IOException {
        prepare();

        Match[] matches = matchBank.getAsArray(3);

        logger.info("Sorting by sentence length for better clearness");
        Arrays.sort(matches, Match.BY_SENTENCE_LENGTH);

        Tree[] oldTrees;
        Tree[] newTrees;

        LinkedList<Match> notParsedMatches = new LinkedList<Match>();


        if (!DO_READ_BEST_TREES_FROM_FILE) {

            if (!DO_READ_STANF_BEST_TREES_FROM_FILE) {
                oldTrees = new Tree[matches.length];
            } else {
                logger.info("Reading sanford best trees from file...");

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        new FileInputStream("./egor_out/tree_dump/tag_span/old_trees.txt"), "UTF-8"));
                oldTrees = TreeUtils.readAllTrees(reader);

                oldTrees = Arrays.copyOfRange(oldTrees, 0, matches.length);
            }

            newTrees = new Tree[matches.length];

            logger.info("Starting tree stanford parsing... Total sentence to be parsed: " + matches.length);
            if (!DO_READ_STANF_BEST_TREES_FROM_FILE) {
                for (int i = 0; i < oldTrees.length; ++i) {
                    logger.info("progress: " + (i) + " / " + matches.length);
                    logger.info("Parsing sentence: " + matches[i].getSentence().str());
                    List<? extends HasWord> s = SentenceGetter.convert(matches[i].getSentence());

                    oldTrees[i] = stanfordTreeGetter.getBestParseTree(s);
                }
            }
            logger.info("progress: " + matches.length + " / " + matches.length);

            changeGrammar();

            logger.info("Starting tree collocation parsing... Total sentence to be parsed: " + matches.length);
            for (int i = 0; i < newTrees.length; ++i) {
                logger.info("progress: " + (i) + " / " + matches.length);
                logger.info("Parsing sentence: " + matches[i].getSentence().str());
                List<? extends HasWord> s = SentenceGetter.convert(matches[i].getSentence());
                try {
                    newTrees[i] = collocationExtTreeGetter.getBestParseTree(s);
                    if (newTrees[i] == null) {
                        logger.severe("No parse tree can be retrieved!");
                        notParsedMatches.add(matches[i]);
                        newTrees[i] = ERROR_TREE;
                    }
                } catch (Exception e) {
                    logger.severe("Exception caught during parsing: " + e.getMessage() + " : ");
                    e.printStackTrace();
                    notParsedMatches.add(matches[i]);
                    newTrees[i] = ERROR_TREE;
                }
            }
            logger.info("progress: " + matches.length + " / " + matches.length);

            if (DO_TREE_DUMP) {
                logger.info("Dumping trees to files...");
                BufferedWriter treeWriter = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("./egor_out/tree_dump/tag_span/old_trees.txt"), "UTF-8"));
                TreeUtils.dumpAllTrees(oldTrees, treeWriter);
                treeWriter.flush();
                treeWriter.close();

                treeWriter = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("./egor_out/tree_dump/tag_span/new_trees.txt"), "UTF-8"));
                TreeUtils.dumpAllTrees(newTrees, treeWriter);
                treeWriter.flush();
                treeWriter.close();
            }
        } else {
            // EGOR_INFO: be ware reading dumped trees, because they must correspond to sentences from MatchBank.
            logger.info("Reading best trees from file...");

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream("./egor_out/tree_dump/tag_span/old_trees.txt"), "UTF-8"));
            oldTrees = TreeUtils.readAllTrees(reader);

            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream("./egor_out/tree_dump/tag_span/new_trees.txt"), "UTF-8"));
            newTrees = TreeUtils.readAllTrees(reader);
        }

        if (oldTrees.length == 0) {
            logger.info("ALL TREES ARE EQUAL =(");
            return;
        }

        if (DO_TREE_COMPARE_AND_PRINT) {

            logger.info("Transforming and comparing trees...");
            int changesDepths[] = new int[oldTrees.length];
            for (int i = 0; i < oldTrees.length; ++i) {
                oldTrees[i] = treeUtils.transformTreeToStandardShape(oldTrees[i]);
                newTrees[i] = treeUtils.transformTreeToStandardShape(newTrees[i]);

                changesDepths[i] = TreeUtils.compareTrees(oldTrees[i], newTrees[i]);
            }

            if (DO_FILTER) {
                logger.info("Filtering trees by max change depth...");
                List<Tree> finalOldTrees = new ArrayList<Tree>();
                List<Tree> finalNewTrees = new ArrayList<Tree>();

                for (int i = 0; i < changesDepths.length; ++i) {
                    if (changesDepths[i] > 2) {
                        finalOldTrees.add(oldTrees[i]);
                        finalNewTrees.add(newTrees[i]);
                    }
                }

                oldTrees = finalOldTrees.toArray(new Tree[finalOldTrees.size()]);
                newTrees = finalNewTrees.toArray(new Tree[finalNewTrees.size()]);
            }

            logger.info("Writing trees to .tex files.");
            final int MAX_TREES_IN_FILE = 30;
            final String oldTreesFilePrefix = "./egor_out/tex/collocations/tag_span/old_trees_tag_span_";
            final String newTreesFilePrefix = "./egor_out/tex/collocations/tag_span/new_trees_tag_span_";

            treeUtils.printTreesToTexFiles(newTrees, newTreesFilePrefix, MAX_TREES_IN_FILE,
                    "Parse trees. Technique: new COLLOCATION tag and unary rules $X \\rightarrow COLLOCATION$, where X has NP basic category.");
            treeUtils.printTreesToTexFiles(oldTrees, oldTreesFilePrefix, MAX_TREES_IN_FILE,
                    "Parse trees. Standard Stanford Parser Algorithm.");
        }

        if (DO_PDF_COMPILE) {
            logger.info("Starting pdf compilation");
            final String PDF_COMPILE_SCRIPT = "egor_out\\tex\\collocations\\tag_span\\pdfgen.bat";
            final String INPUT_TEX_FILES_FOLDER = "egor_out\\tex\\collocations\\tag_span";
            final String OUTPUT_PDF_FILES_FILDER = "egor_out\\tex\\collocations\\pdf_trees\\tag_span";
            Runtime.getRuntime().exec("cmd /c start " + PDF_COMPILE_SCRIPT + " " + INPUT_TEX_FILES_FOLDER + " " + OUTPUT_PDF_FILES_FILDER);
        }
    }
}