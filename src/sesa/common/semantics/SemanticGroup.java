package sesa.common.semantics;

import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.support.GroupMatchType;

import java.util.*;

/**
 * Semantic group  (��������� ������)
 */
public class SemanticGroup {

    private SortedSet<SentenceToken> tokens; // ����� ������ �����������
    private SentenceTree sentenceTree;      // ������ ������� �����������
    private int id;

    /**
     * Constructor
     * @param comparator
     */
    public SemanticGroup(Comparator<? super SentenceToken> comparator) {
        tokens = new TreeSet<SentenceToken>(comparator);
    }

    /**
     * Constructor
     * @param comparator
     * @param token
     */
    public SemanticGroup(Comparator<? super SentenceToken> comparator, SentenceToken token) {
        tokens = new TreeSet<SentenceToken>(comparator);
        tokens.add(token);
    }


    public SentenceTree getSentenceTree() {
        return sentenceTree;
    }

    public void setSentenceTree(SentenceTree sentenceTree) {
        this.sentenceTree = sentenceTree;
    }

  /*public void addRelation(SemanticRelation relation) {
    //relationLists.add(relation);
  }*/

  /*public void addRelation(SentenceToken token, SemanticRelation relation) {

  }*/


    public void addToken(SentenceToken token) {
        tokens.add(token);
    }

    public List<SentenceToken> getTokens() {
        return new LinkedList<SentenceToken>(tokens);
    }

  /*public void addRelation(SentenceToken token, SentenceRelation relation){

  } */

    /**
     * Check , Semantic group contains relation with shortName?
     * @param shortName
     * @return
     */

    //TODO: id semanticgroup is really big, maybe we check hashCode()
    public boolean containsRelation(String shortName) {
        for (SentenceToken token : tokens) {
            for (SemanticRelation relation : token.getRelations()) {
                if (relation.getShortName().equals(shortName))
                    return true;
                // if(relation.getShortName().hashCode() == shortName.hashCode()){
                //       if (relation.getShortName().equals(shortName))
                //          return true;
            }
        }
        return false;
    }

    public List<SemanticRelation> getRelations() {
        List<SemanticRelation> relations = new LinkedList<SemanticRelation>();
        for (SentenceToken token : tokens) {
            if (token.getRelations() != null) {
                relations.addAll(token.getRelations());
            }
        }
        return relations;
    }

    public void removeRelation(SemanticRelation relation) {
        for (SentenceToken token : tokens) {
            if (token.getRelations() != null) {
                if (token.getRelations().remove(relation)) {
                    return;
                }
            }
        }
    }

    public void removeAllRelations(List<SemanticRelation> relationsToDelete) {
        for (SemanticRelation relToDelete : relationsToDelete) {
            for (SentenceToken token : tokens) {
                if (token.getRelations() != null) {
                    if (token.getRelations().remove(relToDelete)) {
                        continue;
                    }
                }
            }
        }
    }

    public List<SemanticRelation> findMistakeRelationsByRole(String roleName) {
        List<SemanticRelation> relations = new LinkedList<SemanticRelation>();
        for (SentenceToken groupToken : getTokens()) {
            for (SemanticRelation relation : groupToken.getRelations()) {
                if (relation.getRole().equals(roleName) && relation.hasMistake())
                    relations.add(relation);
            }
        }
        return relations;
    }


  /*public SortedSet<List<SemanticRelation>> getRelations() {
   return relationLists;
 } */


  /*public boolean containsRelation(String shortName) {

    for (SemanticRelation relation : relationLists)
      if (relation.getShortName().equals(shortName))
        return true;
    return false;
  } */

    // @Override
  /*public String toString() {
    StringBuilder builder = new StringBuilder();
    for (SemanticRelation relation : relationLists)
      builder.append(relation.getToken()).append(" ");
    return builder.toString();
  } */

  /*public String longPrint() {
    StringBuilder builder = new StringBuilder();
    for (SemanticRelation relation : relationLists)
      builder.append(relation.getToken().getValue()).append("|").append(relation.getShortName()).append(" ");
    return builder.toString();
  }    */

    public String longPrint() {
        StringBuilder builder = new StringBuilder();

        for (SentenceToken token : tokens) {
            for (SemanticRelation relation : token.getRelations()) {
                builder.append(relation.getToken().getValue()).append("|").append(relation.getShortName()).append(" ");
            }
        }

        return builder.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    //TODO: == / equals?
    public GroupMatchType matchGroup(SemanticGroup group) {
        return getId() == group.getId() ? GroupMatchType.Matched : GroupMatchType.NotMatched;
    }

  /*@Override
  public int hashCode() {
    StringBuilder builder = new StringBuilder();
    int i = 1;
    for (SemanticRelation relation : relations) {
      String value = relation.getToken().getBaseValue();
      if (value == null)
        value = relation.getToken().getValue();
      builder.append(value.toLowerCase()).append("||");
      builder.append(relation.getShortName()).append("||");
      builder.append(i++);
    }
    return builder.toString().hashCode();
  } */
}
