package grammarscope.browser;

//import grammarscope.browser.
import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.RelationSets;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.Vector;

/**
 * Relation display filter
 * 
 * @author Bernard Bou
 */
public class RelationFilter
{
	/**
	 * Relation id to color map
	 */
	public Map<String, Color> theColorMap;

	/**
	 * Relation id to display flag map
	 */
	public Map<String, Boolean> theDisplayMap;

	/**
	 * Relation id to rank map
	 */
	public Map<String, Integer> theRankMap;

	/**
	 * Constructor
	 */
	public RelationFilter()
	{
		this.theColorMap = RelationFilter.makeColorMap();
		this.theDisplayMap = RelationFilter.makeDisplayMap();
		this.theRankMap = RelationFilter.makeRankMap();
	}

	/**
	 * Copy Constructor
	 */
	@SuppressWarnings("unchecked")
	public RelationFilter(final RelationFilter thatRelationFilter)
	{
		this.theColorMap = (Map<String, Color>) ((HashMap<String, Color>) thatRelationFilter.theColorMap).clone();
		this.theDisplayMap = (Map<String, Boolean>) ((HashMap<String, Boolean>) thatRelationFilter.theDisplayMap).clone();
		this.theRankMap = (Map<String, Integer>) ((HashMap<String, Integer>) thatRelationFilter.theRankMap).clone();
	}

	// D E F A U L T M A P F A C T O R I E S

	/**
	 * Make display map
	 * 
	 * @return default display map
	 */
	static private Map<String, Boolean> makeDisplayMap()
	{
		final Map<String, Boolean> thisDisplayMap = new HashMap<String, Boolean>();
		return thisDisplayMap;
	}

	/**
	 * Make color map
	 * 
	 * @return default color map
	 */
	static private Map<String, Color> makeColorMap()
	{
		final Map<String, Color> thisColorMap = new HashMap<String, Color>();

		thisColorMap.put("gov", new Color(204, 204, 204));
		thisColorMap.put("dep", new Color(204, 204, 204));
		thisColorMap.put("sdep", new Color(204, 204, 204));
		thisColorMap.put("comp", new Color(204, 204, 204));
		thisColorMap.put("mod", new Color(204, 204, 204));

		thisColorMap.put("pred", new Color(255, 255, 153));
		thisColorMap.put("attr", new Color(242, 223, 44));

		thisColorMap.put("cop", new Color(204, 153, 255));

		thisColorMap.put("arg", new Color(204, 204, 204));

		thisColorMap.put("subj", new Color(255, 153, 102));
		thisColorMap.put("nsubj", new Color(255, 102, 0));
		thisColorMap.put("csubj", new Color(255, 102, 0));
		thisColorMap.put("nsubjpass", new Color(255, 79, 46));
		thisColorMap.put("csubjpass", new Color(255, 102, 20));
		thisColorMap.put("xsubj", new Color(255, 153, 102));
		thisColorMap.put("agent", new Color(214, 161, 107));

		thisColorMap.put("obj", new Color(244, 250, 172));
		thisColorMap.put("dobj", new Color(255, 255, 102));
		thisColorMap.put("iobj", new Color(255, 204, 102));
		thisColorMap.put("pobj", new Color(255, 255, 153));

		thisColorMap.put("pcomp", new Color(255, 153, 102));
		thisColorMap.put("ccomp", new Color(255, 204, 255));
		thisColorMap.put("xcomp", new Color(255, 204, 204));
		thisColorMap.put("acomp", new Color(204, 255, 204));

		thisColorMap.put("rel", new Color(153, 255, 204));
		thisColorMap.put("ref", new Color(135, 229, 218));
		thisColorMap.put("expl", new Color(246, 120, 66));
		thisColorMap.put("complm", new Color(255, 153, 255));
		thisColorMap.put("mark", new Color(204, 153, 255));

		thisColorMap.put("aux", new Color(158, 148, 222));
		thisColorMap.put("auxpass", new Color(153, 102, 255));
		thisColorMap.put("neg", new Color(255, 102, 102));

		thisColorMap.put("amod", new Color(153, 255, 153));

		thisColorMap.put("advcl", new Color(255, 204, 204));
		thisColorMap.put("purpcl", new Color(255, 204, 204));

		thisColorMap.put("tmod", new Color(255, 204, 204));
		thisColorMap.put("infmod", new Color(255, 204, 204));
		thisColorMap.put("advmod", new Color(255, 204, 204));
		thisColorMap.put("rcmod", new Color(153, 255, 204));
		thisColorMap.put("partmod", new Color(255, 204, 204));

		thisColorMap.put("det", new Color(0, 153, 102));
		thisColorMap.put("predet", new Color(0, 102, 102));
		thisColorMap.put("number", new Color(153, 255, 204));
		thisColorMap.put("num", new Color(153, 255, 204));
		thisColorMap.put("measure", new Color(51, 255, 153));
		thisColorMap.put("quantmod", new Color(153, 255, 204));
		thisColorMap.put("poss", new Color(153, 255, 255));
		thisColorMap.put("possessive", new Color(156, 246, 184));
		thisColorMap.put("nn", new Color(153, 255, 204));
		thisColorMap.put("appos", new Color(153, 255, 204));
		thisColorMap.put("abbrev", new Color(136, 234, 223));

		thisColorMap.put("prep", new Color(160, 227, 167));

		thisColorMap.put("prt", new Color(255, 102, 102));

		thisColorMap.put("conj", new Color(204, 255, 255));
		thisColorMap.put("cc", new Color(153, 255, 255));
		thisColorMap.put("preconj", new Color(235, 173, 173));

		thisColorMap.put("punct", new Color(0, 51, 102));
		return thisColorMap;
	}

	/**
	 * Make rank map
	 * 
	 * @return default rank map
	 */
	@SuppressWarnings("boxing")
	static private Map<String, Integer> makeRankMap()
	{
		final Map<String, Integer> thisRankMap = new HashMap<String, Integer>();
		int i = 0;
		for (final String thisId : RelationSets.theOrderedGrammaticalRelations)
		{
			thisRankMap.put(thisId, i++);
		}
		return thisRankMap;
	}

	// P R O P A G A T I O N

	/**
	 * Propagate display flag to group of relations
	 * 
	 * @param thisFlag
	 *            display flag
	 * @param theseRelations
	 *            target relations
	 */
	@SuppressWarnings("boxing")
	public void propagateDisplay(final boolean thisFlag, final MutableGrammaticalRelation[] theseRelations)
	{
		for (final MutableGrammaticalRelation thisRelation : theseRelations)
		{
			this.theDisplayMap.put(thisRelation.getId(), thisFlag);
		}
	}

	/**
	 * Propagate display flag to children of relation
	 * 
	 * @param thisFlag
	 *            display flag
	 * @param thisRelation
	 *            target relations
	 */
	@SuppressWarnings("boxing")
	public void propagateDisplayToChildren(final boolean thisFlag, final MutableGrammaticalRelation thisRelation)
	{
		for (final MutableGrammaticalRelation thisChild : thisRelation.getChildrenIterable())
		{
			this.theDisplayMap.put(thisChild.getId(), thisFlag);
			propagateDisplayToChildren(thisFlag, thisChild);
		}
	}

	/**
	 * Propagate color to children
	 * 
	 * @param thisColor
	 *            color
	 * @param thisRelation
	 *            target relation
	 */
	public void propagateColorToChildren(final Color thisColor, final MutableGrammaticalRelation thisRelation)
	{
		for (final MutableGrammaticalRelation thisChild : thisRelation.getChildrenIterable())
		{
			this.theColorMap.put(thisChild.getId(), new RandomColor(thisColor));
			propagateColorToChildren(thisColor, thisChild);
		}
	}

	// R E L A T I O N C O M P A R I S O N

	/**
	 * Relation comparator
	 */
	private class RelationComparator implements Comparator<MutableGrammaticalRelation>
	{
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(final MutableGrammaticalRelation thisRelation1, final MutableGrammaticalRelation thisRelation2)
		{
			final Integer thisRank1 = RelationFilter.this.theRankMap.get(thisRelation1.getId());
			final Integer thisRank2 = RelationFilter.this.theRankMap.get(thisRelation2.getId());
			if (thisRank1 == null && thisRank2 == null)
				return thisRelation1.getId().compareTo(thisRelation2.getId());
			else if (thisRank1 == null)
				return -1;
			else if (thisRank2 == null)
				return 1;
			return thisRank1.compareTo(thisRank2);
		}
	}

	/**
	 * Relation id comparator
	 */
	private class IdComparator implements Comparator<String>
	{
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(final String thisId1, final String thisId2)
		{
			final Integer thisRank1 = RelationFilter.this.theRankMap.get(thisId1);
			final Integer thisRank2 = RelationFilter.this.theRankMap.get(thisId2);
			if (thisRank1 == null && thisRank2 == null)
				return thisId1.compareTo(thisId2);
			else if (thisRank1 == null)
				return -1;
			else if (thisRank2 == null)
				return 1;
			return thisRank1.compareTo(thisRank2);
		}
	}

	// filter

	/**
	 * Filter list of relation ids
	 * 
	 * @param thatList
	 *            source list of relation ids
	 * @return sorted list of displayable relation ids
	 */
	@SuppressWarnings({ "synthetic-access", "boxing" })
	public List<String> filterIds(final Collection<String> thatList)
	{
		final List<String> thisList = new ArrayList<String>();
		for (final String thisId : thatList)
		{
			final Boolean display = this.theDisplayMap.get(thisId);
			if (!(display == null ? false : display))
			{
				continue;
			}
			thisList.add(thisId);
		}
		final IdComparator thisComparator = new IdComparator();
		Collections.sort(thisList, thisComparator);
		return thisList;
	}

	/**
	 * Filter list of relations
	 * 
	 * @param thatList
	 *            source list of relations
	 * @return sorted list of displayable relations
	 */
	@SuppressWarnings({ "synthetic-access", "boxing" })
	public List<MutableGrammaticalRelation> filterRelations(final Collection<MutableGrammaticalRelation> thatList)
	{
		final List<MutableGrammaticalRelation> thisList = new ArrayList<MutableGrammaticalRelation>();
		for (final MutableGrammaticalRelation thisRelation : thatList)
		{
			final String thisId = thisRelation.getId();
			final Boolean display = this.theDisplayMap.get(thisId);
			if (!(display == null ? false : display))
			{
				continue;
			}
			thisList.add(thisRelation);
		}
		final RelationComparator thisComparator = new RelationComparator();
		Collections.sort(thisList, thisComparator);
		return thisList;
	}

	// S T R I N G I F Y

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		final StringBuffer thisBuffer = new StringBuffer();
		final TreeSet<String> theseSortedIds1 = new TreeSet<String>();
		theseSortedIds1.addAll(this.theColorMap.keySet());
		for (final String thisId : theseSortedIds1)
		{
			if (thisId == null)
			{
				continue;
			}
			final Color thisColor = this.theColorMap.get(thisId);
			if (thisColor == null)
			{
				continue;
			}
			final String thisKey = "color-" + thisId.toString();
			final String thisValue = thisColor.getRed() + "," + thisColor.getGreen() + "," + thisColor.getBlue();
			thisBuffer.append(thisKey);
			thisBuffer.append("=");
			thisBuffer.append(thisValue);
			thisBuffer.append("\n");
		}
		final TreeSet<String> theseSortedIds2 = new TreeSet<String>();
		theseSortedIds2.addAll(this.theDisplayMap.keySet());
		for (final String thisId : theseSortedIds2)
		{
			if (thisId == null)
			{
				continue;
			}
			final Boolean thisFlag = this.theDisplayMap.get(thisId);
			if (thisFlag == null)
			{
				continue;
			}
			final String thisKey = "display-" + thisId.toString();
			final String thisValue = thisFlag.toString();
			thisBuffer.append(thisKey);
			thisBuffer.append("=");
			thisBuffer.append(thisValue);
			thisBuffer.append("\n");
		}
		final TreeSet<String> theseSortedIds3 = new TreeSet<String>();
		theseSortedIds3.addAll(this.theRankMap.keySet());
		for (final String thisId : theseSortedIds3)
		{
			if (thisId == null)
			{
				continue;
			}
			final Integer thisRank = this.theRankMap.get(thisId);
			if (thisRank == null)
			{
				continue;
			}
			final String thisKey = "rank-" + thisId.toString();
			final String thisValue = thisRank.toString();
			thisBuffer.append(thisKey);
			thisBuffer.append("=");
			thisBuffer.append(thisValue);
			thisBuffer.append("\n");
		}
		return thisBuffer.toString();
	}

	// P E R S I S T

	/**
	 * Load properties into maps
	 * 
	 * @param theseProperties
	 *            properties read from persist file
	 */
	public void setProperties(final Properties theseProperties)
	{
		for (final String thisKey : theseProperties.stringPropertyNames())
		{
			final String[] theseSubKeys = thisKey.split("-");
			final String thisId = theseSubKeys[1];
			// System.out.println(theseSubKeys[0] + "," + theseSubKeys[1] + "=" + theseProperties.getProperty(thisKey));
			if (theseSubKeys[0].equals("color"))
			{
				final String[] theseValues = theseProperties.getProperty(thisKey).split(",");
				final int thisRValue = Integer.parseInt(theseValues[0]);
				final int thisGValue = Integer.parseInt(theseValues[1]);
				final int thisBValue = Integer.parseInt(theseValues[2]);
				final Color thisColor = new Color(thisRValue, thisGValue, thisBValue);
				this.theColorMap.put(thisId, thisColor);
			}
			else if (theseSubKeys[0].equals("display"))
			{
				final Boolean thisValue = Boolean.valueOf(theseProperties.getProperty(thisKey));
				this.theDisplayMap.put(thisId, thisValue);
			}
			else if (theseSubKeys[0].equals("rank"))
			{
				final Integer thisValue = Integer.valueOf(theseProperties.getProperty(thisKey));
				this.theRankMap.put(thisId, thisValue);
			}
		}
	}

	/**
	 * Save maps to properties
	 * 
	 * @return properties
	 */
	@SuppressWarnings("serial")
	public Properties getProperties()
	{
		final Properties theseProperties = new Properties()
		{
			@Override
			public synchronized Enumeration<Object> keys()
			{
				return Collections.enumeration(new TreeSet<Object>(keySet()));
			}
		};

		Vector<String> theseIds = new Vector<String>(this.theColorMap.keySet());
		Collections.sort(theseIds);
		for (final String thisId : theseIds)
		{
			if (thisId == null)
			{
				continue;
			}
			final Color thisColor = this.theColorMap.get(thisId);
			if (thisColor == null)
			{
				continue;
			}
			final String thisKey = "color-" + thisId.toString();
			final String thisValue = thisColor.getRed() + "," + thisColor.getGreen() + "," + thisColor.getBlue();
			theseProperties.setProperty(thisKey, thisValue);
		}
		theseIds = new Vector<String>(this.theDisplayMap.keySet());
		Collections.sort(theseIds);
		for (final String thisId : this.theDisplayMap.keySet())
		{
			if (thisId == null)
			{
				continue;
			}
			final Boolean thisFlag = this.theDisplayMap.get(thisId);
			if (thisFlag == null)
			{
				continue;
			}
			final String thisKey = "display-" + thisId.toString();
			final String thisValue = thisFlag.toString();
			theseProperties.setProperty(thisKey, thisValue);
		}
		theseIds = new Vector<String>(this.theRankMap.keySet());
		Collections.sort(theseIds);
		for (final String thisId : this.theRankMap.keySet())
		{
			if (thisId == null)
			{
				continue;
			}
			final Integer thisRank = this.theRankMap.get(thisId);
			if (thisRank == null)
			{
				continue;
			}
			final String thisKey = "rank-" + thisId.toString();
			final String thisValue = thisRank.toString();
			theseProperties.setProperty(thisKey, thisValue);
		}
		return theseProperties;
	}
}
