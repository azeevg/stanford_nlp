package sesa.tools.statistics;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class RawSemanticGroup {

    private Set<RawSemanticRelation> relations;
    private String id;
    private int sentenceId;
    private boolean sentenceHasServiceTokens;
    private int sentenceSize;

    public RawSemanticGroup(Comparator<? super RawSemanticRelation> comparator) {
        relations = new TreeSet<RawSemanticRelation>(comparator);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(int sentenceId) {
        this.sentenceId = sentenceId;
    }

    public boolean isSentenceHasServiceTokens() {
        return sentenceHasServiceTokens;
    }

    public void setSentenceHasServiceTokens(boolean sentenceHasServiceTokens) {
        this.sentenceHasServiceTokens = sentenceHasServiceTokens;
    }

    public int getSentenceSize() {
        return sentenceSize;
    }

    public void setSentenceSize(int sentenceSize) {
        this.sentenceSize = sentenceSize;
    }

    public Set<RawSemanticRelation> getRelations() {
        return relations;
    }

    public void addRelation(RawSemanticRelation relation) {
        this.relations.add(relation);
    }


}
