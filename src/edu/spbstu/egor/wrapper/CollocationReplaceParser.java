package edu.spbstu.egor.wrapper;

/**
 * Created by Egor Gorbunov on 03.04.2016.
 * email: egor-mailbox@ya.ru
 */

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchedCollocation;
import edu.spbstu.appmath.collocmatch.Sentence;
import edu.spbstu.egor.Grammar;
import edu.spbstu.egor.SentenceGetter;
import edu.spbstu.egor.collocation.CollocationReplace;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.trees.Tree;

import java.util.List;
import java.util.logging.Logger;

/**
 * Uses collocation replace
 */
public class CollocationReplaceParser extends ExhaustivePCFGParserNoOutsides {
    private static final Logger logger = Logger.getLogger(CollocationReplaceParser.class.getSimpleName());

    private static final String COLLOCATION_SPECIAL_WORD = "_COLLOCATION_";

    private final Grammar grammar;
    private final CollocationDictionary collocationDictionary;

    private Match curMatch = null;

    public CollocationReplaceParser(Grammar grammar, CollocationDictionary collocationDictionary) {
        super(grammar.bg, grammar.ug, grammar.lex, grammar.op, grammar.stateIndex, grammar.wordIndex, grammar.tagIndex);


        this.grammar = grammar;
        this.collocationDictionary = collocationDictionary;

        // preparing grammar
        logger.info("Adding new special word to grammar: " + COLLOCATION_SPECIAL_WORD);
        grammar.addWord(COLLOCATION_SPECIAL_WORD, Grammar.Const.NOUN_TAG, 200);
    }


    @Override
    public boolean parse(List<? extends HasWord> wordList) {
        String str = SentenceGetter.sentenceToString(wordList);

        logger.info("Replacing collocations.");

        curMatch = collocationDictionary.searchAllCollocations(str);
        Sentence sentence = curMatch.replaceCollocations(COLLOCATION_SPECIAL_WORD);

        logger.info("Sentence to be parsed: " + sentence.str());

        wordList = SentenceGetter.convert(sentence);

        return super.parse(wordList);
    }

    @Override
    public Tree getBestParse() {
        Tree bestParse = super.getBestParse();

        replaceCollocationTag(bestParse, curMatch);

        return bestParse;
    }

    private static void replaceCollocationTag(Tree newTree, Match match) {
        int i = 0;
        MatchedCollocation[] collocations = match.getSortedCollocations();
        for (Tree t : newTree.getLeaves()) {
            if (t.label().value().equals(COLLOCATION_SPECIAL_WORD)) {
                t.label().setValue(collocations[i].getCollocationStr());
                logger.info("Replaced _COLLOCATION_ tag at: " + i + " to value = " + t.label().value());
                ++i;
            }
        }
    }

}
