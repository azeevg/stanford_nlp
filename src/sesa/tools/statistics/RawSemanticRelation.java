package sesa.tools.statistics;


public class RawSemanticRelation {

    private String token;
    //private String tokenBase;
    //private String role;
    private String pos;
    private boolean prevDelimiter;

    private String tokenBaseRolePos;

    public String getTokenBaseRolePos() {
        return tokenBaseRolePos;
    }

    public void setTokenBaseRolePos(String tokenBaseRolePos) {
        this.tokenBaseRolePos = tokenBaseRolePos;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    private int index;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  /*public String getTokenBase() {
    return tokenBase;
  }

  public void setTokenBase(String tokenBase) {
    this.tokenBase = tokenBase;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getPos() {
    return pos;
  }

  public void setPos(String pos) {
    this.pos = pos;
  }  */

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public boolean isPrevDelimiter() {
        return prevDelimiter;
    }

    public void setPrevDelimiter(boolean prevDelimiter) {
        this.prevDelimiter = prevDelimiter;
    }
}
