package sesa.common.builder.relationsBuilder;

import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.Tree;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.builder.BasicBuilder;
import sesa.common.model.processing.sentence.SentenceForest;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.model.processing.support.PositionComparator;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.semantics.TripletFinder;

import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Ira on 26.02.2016.
 */

/**
 * The abstract class <br> CommonRelationsBuilder </br> is used
 * to build parse trees, find triplets to sentences
 * The one abstract method that must be implemented is: getParseTree(List sentence)
 */
public abstract class CommonRelationsBuilder extends BasicBuilder {

    public final Logger log = LogManager.getLogger(CommonRelationsBuilder.class);

    /**
     * This method does syntactic analysis and "dependency parsing", find triplets for sentence in one paragraph
     *
     * @param text    The input text
     * @param isTitle flag
     * @param level
     * @return
     */
    @Override
    protected List<SentenceForest> buildParagraphSubTree(String text, boolean isTitle, int level) {
        System.out.println("buildParagraphSubTree in CommonRelationsBuilder");
        List<SentenceForest> paragraph = new LinkedList<SentenceForest>();

        Reader rd = new StringReader(text);
        DocumentPreprocessor dp = new DocumentPreprocessor(rd);
        for (List sentence : dp) {

            SentenceForest forest = new SentenceForest();
            SentenceTree tree = new SentenceTree();


            Tree parse = getParseTree(sentence);
            if (parse == null) {
                continue;
            }

            log.info("Sentence" + Sentence.listToString(sentence).hashCode());
            log.info(parse.toString());

            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            try {
                parse.indentedXMLPrint(writer);
            } catch (NullPointerException e) {
                log.error(e.getMessage());

            }
            String xmlTree = stringWriter.toString();
            System.out.println("xmlTree:" + xmlTree);
            SentenceTriplet triplet = TripletFinder.findSentenceTriplet(xmlTree);
            triplet.setSentence(Sentence.listToString(sentence));
            printTriplet(triplet);
            triplet.setSentence(Sentence.listToString(sentence));


            List<SentenceToken> tokens = new LinkedList<SentenceToken>();
            SemanticGroup group = new SemanticGroup(new PositionComparator());
            int position = 1;

            Set<String> alreadyResolved = new HashSet<String>();
            for (Object w : sentence) {
                Word word = (Word) w;
                String value = word.value();
                SentenceToken token = new SentenceToken(value);
                token.setSentencePosition(position);
                SemanticRelation relation = null;
                if (value.equals(triplet.getSubject()) && !alreadyResolved.contains("subj")) {
                    relation = new SemanticRelation("subj");
                    alreadyResolved.add("subj");
                    relation.setShortName("subj");
                } else if (value.equals(triplet.getPredicate()) && !alreadyResolved.contains("pred")) {
                    relation = new SemanticRelation("pred");
                    relation.setShortName("pred");
                    alreadyResolved.add("pred");
                } else if (value.equals(triplet.getObject()) && !alreadyResolved.contains("obj")) {
                    relation = new SemanticRelation("obj");
                    relation.setShortName("obj");
                    alreadyResolved.add("obj");
                }
                if (relation != null) {
                    relation.setToken(token);
                    token.addRelation(relation);
                    group.addToken(token);
                    group.setId(0);
                    token.setGroup(group);
                }
                token.setPartOfSpeech("");
                tokens.add(token);
                position++;
            }

            tree.addSemanticGroup(group);
            tree.setSentence(Sentence.listToString(sentence));
            tree.setTokens(tokens);
            forest.addTree(tree);
            paragraph.add(forest);
        }

        return paragraph;
    }

    protected abstract Tree getParseTree(List sentence);

    public void printTriplet(SentenceTriplet triplet) {
        System.out.println("Sentence: " + triplet.getSentence());
        System.out.println("Triplet subj - pred - obj : " + triplet.getSubject() + " - " + triplet.getPredicate() + " - " + triplet.getObject());

    }


}
