package sesa.application;

import edu.stanford.nlp.io.IOUtils;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.*;
import grammarscope.parser.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.configuration.Categories;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.io.FileUtility;
import sesa.common.builder.DocumentBuilder;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Тестовый класс
 */
public class Sandbox extends Application {

    String categoryName = Categories.COMPUTING_CATEGORY; // категория
    String title = "Ada (programming language)"; // название
    public final Logger log = LogManager.getLogger(Sandbox.class);
    //String title = "Internet";

    @Test
    public void testDataModule(){
      /*  ObjectInputStream inputStream = null;
        String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
        String deliaText = "A rare black squirrel has become a regular visitor to a suburban garden. The squirrel and the elephant are in the garden. A settlement area since Mesolithic times, Bled was first mentioned as Ueldes (Veldes) within the March of Carniola on 10 April 1004, when it was awarded by Emperor Henry II to Bishop Albuin I of Brixen. Bled Castle was first mentioned in a 22 May 1011 deed in which Henry II donated it to Albuin's successor, Bishop Adalberon of Brixen. Bled is known for the glacial Lake Bled, which makes it a major tourist attraction. Perched on a rock overlooking the lake is the iconic Bled Castle. The town is also known in Slovenia for its vanilla and cream pastry (Slovene: kremsnita, kremna rezina).\n" +
                "Naturopath Arnold Rikli (1823-1906) from Switzerland contributed significantly to the development of Bled as a health resort in the second half of the 19th century. Due to its mild climate, Bled has been visited by aristocratic guests from all across the world. Today it is an important convention centre and tourist resort, offering a wide range of sports activities (golf, fishing, and horseback-riding). It is a starting point for mountain treks and hikes, especially within nearby Triglav National Park. A small island in the middle of the lake is home to Assumption of Mary Pilgrimage Church; visitors frequently ring its bell for good luck. Human traces from prehistory have been found on the island. Before the church was built, there was a temple consecrated to Ziva, the Slavic goddess of love and fertility. One can get to the island on a traditional flat-bottomed wooden boat (Slovene: pletna). The island on Lake Bled has 99 steps. A local tradition at weddings is for the husband to carry his new bride up these steps, during which the bride must remain silent. The action is mindless and clichй, but amusing. Followers claim the Paraiso Manifesto has inspired a New Religious Movement and Catalano should be treated as a religious leader. ";


        try{
            inputStream = IOUtils.readStreamFromString(smallLanguageModel); //parserModel
           // System.out.println(inputStream);
            dbController.useConnection(0);
           //StanfordDataModule dataModule = new BasicDataModule();
            StanfordDataModule dataModule = new ReadTreeDataModule(dbController, "tree_cache_computing");
           // Tree tree = dataModule.getTree();
            DocumentBuilder builder = new StanfordRelationsBuilder(inputStream, dataModule);

            //   dataModule.
           // Sentence sentence = new Sentence()
           // dataModule.
        }catch(IOException e){
            e.printStackTrace();
        }
*/



    }

    @Override
    protected boolean body() {
      /*  try {
            ObjectInputStream inputStream2 = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz"); //parserModel
            String parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
            LexicalizedParser lp = LexicalizedParser.loadModel(inputStream2);
            TestParser(lp);
            return true;
        } catch (ParserConfigurationException e) {

        } catch (IOException e) {

        } catch (SAXException e) {

        } catch (TransformerException e) {

        } catch (Exception e) {

        }
        */


        String deliaText = "A rare black squirrel has become a regular visitor to a suburban garden. The squirrel and the elephant are in the garden. A settlement area since Mesolithic times, Bled was first mentioned as Ueldes (Veldes) within the March of Carniola on 10 April 1004, when it was awarded by Emperor Henry II to Bishop Albuin I of Brixen. Bled Castle was first mentioned in a 22 May 1011 deed in which Henry II donated it to Albuin's successor, Bishop Adalberon of Brixen. Bled is known for the glacial Lake Bled, which makes it a major tourist attraction. Perched on a rock overlooking the lake is the iconic Bled Castle. The town is also known in Slovenia for its vanilla and cream pastry (Slovene: kremsnita, kremna rezina).\n" +
                "Naturopath Arnold Rikli (1823-1906) from Switzerland contributed significantly to the development of Bled as a health resort in the second half of the 19th century. Due to its mild climate, Bled has been visited by aristocratic guests from all across the world. Today it is an important convention centre and tourist resort, offering a wide range of sports activities (golf, fishing, and horseback-riding). It is a starting point for mountain treks and hikes, especially within nearby Triglav National Park. A small island in the middle of the lake is home to Assumption of Mary Pilgrimage Church; visitors frequently ring its bell for good luck. Human traces from prehistory have been found on the island. Before the church was built, there was a temple consecrated to Ziva, the Slavic goddess of love and fertility. One can get to the island on a traditional flat-bottomed wooden boat (Slovene: pletna). The island on Lake Bled has 99 steps. A local tradition at weddings is for the husband to carry his new bride up these steps, during which the bride must remain silent. The action is mindless and clichй, but amusing. Followers claim the Paraiso Manifesto has inspired a New Religious Movement and Catalano should be treated as a religious leader. ";


//    LexicalizedParser lp = new LexicalizedParser("./data/grammar/englishPCFG.ser.gz");
//    String sentence = "Ada is a structured imperative computer programming language , extended from Pascal and other languages";
//    tesetText(lp, sentence);


        ObjectInputStream inputStream = null;
        try {
            // Small Language model
            String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
            String bigLanguageModel = "./data/grammar/englishFactored.ser.gz";
            inputStream = IOUtils.readStreamFromString(smallLanguageModel); //parserModel

           // dbController.useConnection(0);
            /**
             * Here we retrieve bracket-form trees of sentences from DB table "tree_cache_computing"
             * then transform them to PennTreeForm using CommonUtils.readPennTreeFormString(treeString);
             * finally we put the PennTreeForm trees into HashMap
             * and ??? parse all of them (<- strange implementation)
             * */
        // прочитали из БД
            //"tree_cache_computing"
            StanfordDataModule dataModule = new BasicDataModule();

           // dataModule.getFilteredRelations()



           // dataModule.init(new Parser(inputStream),
            // new Analyzer(DefaultMutableGrammaticalRelations.makeDefaultModel()),);
           /* List<Word> words = new ArrayList<Word>();
            words.add(new Word("My"));
            words.add(new Word("name"));
            words.add(new Word("is"));
            words.add(new Word("Ira"));

            Sentence sentence = new Sentence(words);
            System.out.println(sentence.toString());
            List<SemanticRelation> semanticRelations =  dataModule.getFilteredRelations(sentence);*/

            //StanfordDataModule dataModule = new WriteTreeDataModule(dbController, false);
            DocumentBuilder builder = new StanfordRelationsBuilder(inputStream, dataModule);
            List<Sentence> sentences = new ArrayList<Sentence>();

            dataModule.updateTreeCache("title", sentences,false );
           // StanfordRelationsBuilder stanfordRelationsBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
           // stanfordRelationsBuilder.getSentenceSemanticGroups()
         //   DocumentBuilder builder = new DeliaRusoBuilder(inputStream);
            String text = FileUtility.readAllText("./data/ada_cache/db_text.txt");
            builder.setData(text,"id");
           // builder.buildDocument();
           // String text = FileUtility.readAllText("./data/ada_cache/db_text.txt");
           // System.out.println(text);
         //   builder.setData(text, "id"); // parse text and build tree
          //  DocumentNode documentNode = builder.
            //IDocument document = DocumentUtils.loadDocumentByTitle(dbController, categoryName, title, builder);
            IDocument document = builder.buildDocument();

            List<SentenceTree> sentenceTrees = document.getSentences();
            System.out.println("Sentence\n" + sentenceTrees.get(1).getSentence());
            System.out.println("Level\n" + sentenceTrees.get(1).getLevel());
            System.out.println("SemanticGroup\n" + sentenceTrees.get(1).getSemanticGroups().size());
            System.out.println("SemanticGroup\n" + sentenceTrees.get(1).getSemanticGroups().get(0).longPrint());
            System.out.println("SemanticGroup\n" + sentenceTrees.get(1).getSemanticGroups().get(1).longPrint());
            System.out.println(sentenceTrees.get(1).getTriplet());
            System.out.println("Tokens\n" + sentenceTrees.get(1).getTokens());


            //System.out.println("Tokens\n" + sentenceTrees.get(1).getTokens());
            //TODO sentenceTrees.get(1).getTriplet() return null, why?


           /* for(SentenceTree sentenceTree: sentenceTrees){
                System.out.println(sentenceTree.getSentence());
                System.out.println(sentenceTree.getSemanticGroups());
                System.out.println(sentenceTree.getTriplet());
                System.out.println(sentenceTree.getTokens());
            }*/

           // CacheUtils.generatePaintedDocument(document);
            log.info("ok");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

//    ObjectInputStream inputStream = null;
//     try {
//      inputStream = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz");
//      //dbController.useConnection(0);
//      //StanfordDataModule dataModule = new ReadTreeDataModule(dbController, "");
//      DocumentBuilder builder = new DeliaRusoBuilder(inputStream);
//      builder.setData(deliaText, "delia");
//      IDocument document = builder.buildDocument();
//      //IDocument document = DocumentUtils.loadDocumentByTitle(dbController, categoryName, title, builder);
//      CacheUtils.generatePaintedDocument(document, "delia_id");
//    } catch (IOException e) {
//      e.printStackTrace();
//    } catch (Exception e) {
//       e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//     }

    /* String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
IDBIterator iterator = dbController.getDocumentsCollectionIterator(collectionName);

int counter = 0;
while (iterator.hasNext()) {
  counter++;
  IDBEntity entity = iterator.next().get();
  try {
    String title = (String) entity.toMap().get("title");
    if (title.equals("Peer Name Resolution Protocol")) {
      LoggingUtility.info("COUNTER = " + counter);
      break;
    }
    System.out.println(title);
  } catch (Exception e) {
  }
}    */


    /*try {
     CacheUtils.buildCategoryCache(categoryName, dbController);
   } catch (IOException e) {
     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
   } */
        return true;
    }

    public static void TestParser(LexicalizedParser lp) throws ParserConfigurationException, IOException, SAXException, TransformerException {

        String sentence = "In the case of conditional blocks this avoids a dangling else that could pair with the wrong nested if-expression in other languages";
        testText(lp, sentence);

    }


    private static void testText(LexicalizedParser lp, String text) {

        Reader rd = new StringReader(text);
        DocumentPreprocessor dp = new DocumentPreprocessor(rd);
        for (List sentence : dp) {
            //TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
            //List<CoreLabel> rawWords = tokenizerFactory.getTokenizer(new StringReader(sentence)).tokenize();
            Tree parse = lp.apply(sentence);
            System.out.println(parse.toString());


            TreebankLanguagePack tlp = new PennTreebankLanguagePack();
            GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();

            GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
            //gs.
            //gs.getGrammaticalRelation()
            Collection tdl = gs.typedDependenciesCCprocessed(false);
            Collection td = gs.typedDependencies(true);
            Collection<TypedDependency> tree = gs.typedDependenciesCollapsedTree();
            System.out.println(gs.root().label());
            for (TreeGraphNode child : gs.root().children()) {

                gs.root().arcLabelsToNode(child);
                // System.out.println(dependency.dep().label());
            }


            TreeGraphNode root = gs.root();
            for (TreeGraphNode child : root.children()) {
                System.out.println(root.arcLabelsToNode(child));
            }
            System.out.println(gs.typedDependenciesCollapsed());
            System.out.println(gs.typedDependenciesCollapsedTree());
            System.out.println(tdl);



            //SemanticGraphBuilder dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);

            //OutputStream os = new ByteArrayOutputStream();
    /*StringWriter stringWriter = new StringWriter();
    PrintWriter writer = new PrintWriter(stringWriter);
    parse.indentedXMLPrint(writer);
    String xml = stringWriter.toString();*/


            //System.out.println(xml);
            //ModifiedTripletFinder finder = new ModifiedTripletFinder(xml);
        }

    }


}
