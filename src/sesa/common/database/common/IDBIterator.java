package sesa.common.database.common;

import sesa.common.database.entity.IDBEntity;
import sesa.common.features.Option;

public interface IDBIterator {

   Option<IDBEntity> next();
   long count();
   boolean hasNext();
}
