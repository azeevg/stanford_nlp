package sesa.common.utility.common;

import sesa.common.configuration.Configuration;
import sesa.common.utility.logging.LoggingUtility;

import java.sql.SQLException;
import java.util.Arrays;

public abstract class Application extends LoggingUtility {
    protected static ConfigUtility configuration = new ConfigUtility();
    //   protected DBController dbController = null;


    private LOGGING_LEVEL getLoggingLevel(String loggingLevel) {
        if (loggingLevel.equalsIgnoreCase(Configuration.LOG_ERROR))
            return LOGGING_LEVEL.LEVEL_ERROR;
        else if (loggingLevel.equalsIgnoreCase(Configuration.LOG_WARNING))
            return LOGGING_LEVEL.LEVEL_WARNING;
        else if (loggingLevel.equalsIgnoreCase(Configuration.LOG_INFO))
            return LOGGING_LEVEL.LEVEL_INFO;
        else if (loggingLevel.equalsIgnoreCase(Configuration.LOG_DEBUG))
            return LOGGING_LEVEL.LEVEL_DEBUG;

        return LOGGING_LEVEL.LEVEL_NONE;
    }

    /**
     * The Entry Point of our Application
     * Implements environment initialization:
     * 1. Creates connection to DB with credentials retrieved from config
     * 2. Launch application - see body()
     */
    public void run() {
        if (!configuration.isLoaded())
            die("Configuration wasn't loaded");

        /* Check necessary parameters */
        if (!configuration.contains(Arrays.asList(Configuration.DB_HOST, Configuration.DB_NAME, Configuration.DB_USER, Configuration.DB_PASSWORD)))
            return;

        /* Setup logging level */
        if (configuration.contains(Configuration.LOG_LEVEL))
            setLoggingLevel(getLoggingLevel(configuration.getProperty(Configuration.LOG_LEVEL)));

        /* Connection creation */
     /*   DBConnection connection = DBFactory.createConnection(configuration.getProperty(Configuration.DB_HOST), 3306, configuration.getProperty(Configuration.DB_NAME), configuration.getProperty(Configuration.DB_USER), configuration.getProperty(Configuration.DB_PASSWORD));
        if (!connection.isConnected()) {
            error("Can't establish connection to DB");
            //return;
        }

        /* DB Controller creation */
        // dbController = new DBController(connection);
        try {
            body(); //
        } catch (Exception e) {
            error(e.getMessage());
        } catch (Throwable e) {
            error(e.getMessage());
        }
    }

    protected abstract boolean body() throws SQLException, ClassNotFoundException;
}
