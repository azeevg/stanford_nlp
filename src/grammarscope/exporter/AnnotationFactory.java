package grammarscope.exporter;

import java.util.Map;

import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalRelation.GrammaticalRelationAnnotation;
import grammarscope.parser.MutableGrammaticalRelation;

/**
 * Conservative annotation factory. Preserves the annotation class of the standard grammatical relation. Matching is based of short name. A dynamic annotaion
 * factory is provided in the 'tweak' package but requires javassist to create annotation classes on the fly.
 * 
 * @author Bernard Bou <bbou@ac-toulouse.fr>
 */
public class AnnotationFactory
{
	/**
	 * Get annotation
	 * 
	 * @param thatRelation
	 *            mutable grammatical relation
	 * @param thisMap
	 *            mutable grammatical to grammatical relation map
	 * @return annotation class
	 */
	static public Class<? extends GrammaticalRelationAnnotation> getAnnotation(final MutableGrammaticalRelation thatRelation, final Map<MutableGrammaticalRelation, GrammaticalRelation> thisMap)
	{
		// return AnnotationFactory.makeAnnotation(thisRelation.getShortName());
		final GrammaticalRelation thisRelation = thisMap.get(thatRelation);
		return GrammaticalRelation.getAnnotationClass(thisRelation);
	}
}
