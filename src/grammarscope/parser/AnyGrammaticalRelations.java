package grammarscope.parser;

import java.util.Arrays;
import java.util.Collection;

import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalRelation.GrammaticalRelationAnnotation;
import edu.stanford.nlp.trees.GrammaticalRelation.Language;

public class AnyGrammaticalRelations
{
	/**
	 * The "dominates" grammatical relation". Example: "(x(y))" <code>dominates(x,y)</code>
	 */
	public static final GrammaticalRelation DOMINATES = new GrammaticalRelation(Language.Any, "dom", "Dominates", DominatesGRAnnotation.class, GrammaticalRelation.DEPENDENT, ".*", new String[] { "__=target << __" });

	public static class DominatesGRAnnotation extends GrammaticalRelationAnnotation
	{
		/* */
	}

	private static GrammaticalRelation[] theGrammaticalRelations = new GrammaticalRelation[] { GrammaticalRelation.GOVERNOR, GrammaticalRelation.DEPENDENT /*
																																							 * ,
																																							 * AnyGrammaticalRelations
																																							 * .
																																							 * DOMINATES
																																							 */};

	static public Collection<GrammaticalRelation> values()
	{
		return Arrays.asList(AnyGrammaticalRelations.theGrammaticalRelations);
	}

	static public GrammaticalRelation valueOf(final String name)
	{
		for (final GrammaticalRelation thisGrammaticalRelation : AnyGrammaticalRelations.theGrammaticalRelations)
			if (thisGrammaticalRelation.getShortName().equals(name))
				return thisGrammaticalRelation;
		return null;
	}

	static public void main(final String[] args)
	{
		for (final GrammaticalRelation reln : AnyGrammaticalRelations.values())
		{
			final GrammaticalRelation parent = reln.getParent();

			System.out.println(reln);
			System.out.println("\tShort name:    " + reln.getShortName());
			System.out.println("\tLong name:     " + reln.getLongName());
			System.out.println("\tSpecific name: " + reln.getSpecific());
			System.out.println("\tParent:        " + (parent == null ? "null" : parent.getShortName()));
			System.out.println("\tAnnotation:    " + GrammaticalRelation.getAnnotationClass(reln));
		}
	}
}
