package sesa.tools.dictionary.builder;

import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.features.Option;
import sesa.common.utility.common.Application;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.model.processing.dictionary.Token;
import sesa.common.utility.logging.ReportUtility;

import java.sql.SQLException;
import java.util.*;

/**
 *
 */
public class DictionaryBuilder extends Application {

    private DocumentsDao documentsDao = new DocumentsDao();
    private Dictionary dictionary = new Dictionary();

    public DictionaryBuilder() throws SQLException, ClassNotFoundException {
    }


    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {
        prepareDictionary();
        saveDictionary(Configuration.DB_COLLECTION_DICTIONARY);

        return true;
    }

    private void processDocument(IDBEntity document) {
        Map<String, Object> documentMap = document.toMap();
        if (!documentMap.containsKey("keywords")) // check elem with key
            return;

        Set<String> keywords = new HashSet<String>();
        List<String> documentKeywords = (List<String>) documentMap.get("keywords");

        for (String keyword : documentKeywords) {
            String token = keyword.toLowerCase().trim();
            if (token.length() <= 2)
                continue;
            System.out.println(token);
            keywords.add(keyword);
        }

        dictionary.merge(keywords);
    }

    private void prepareDictionary() throws SQLException, ClassNotFoundException {
        DBIterator iterator = documentsDao.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_PUBMED);
        //DBIterator iterator = dbController.getIteratorDocumentPUBMEDIterator();

        ReportUtility reporter = new ReportUtility("DictionaryTerms building [1/2]", iterator.count());
        while (iterator.hasNext()) {
            Option<IDBEntity> document = iterator.next();

            if (document.isDefined())
                processDocument(document.get());

            reporter.addProcessed(1);
            reporter.report();
        }
    }


    private void saveDictionary(String collectionName) {
        ReportUtility reporter = new ReportUtility("Saving dictionary [2/2]", dictionary.getSize());
        if(!documentsDao.isTableExists(collectionName)){
            documentsDao.createDocumentCollectionTable(collectionName);
        }
        for (String keyword : dictionary) {
            Token token = new Token(keyword, dictionary.getWeight(keyword));

            documentsDao.addToDocumentsCollection(collectionName,token);
            //dbController.addDictionaryToken(token); // insert(Configuration.DB_COLLECTION_DICTIONARY, token);
            reporter.addProcessed(1);
            reporter.report();
        }
    }


}
