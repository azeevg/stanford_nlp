package sesa.common.interfaces;


import java.sql.SQLException;

public interface IProcessor {
    void process(String item) throws SQLException, ClassNotFoundException;
}
