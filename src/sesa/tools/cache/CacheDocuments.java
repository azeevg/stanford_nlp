package sesa.tools.cache;


import sesa.common.configuration.Categories;
import sesa.common.utility.common.Application;

public class CacheDocuments extends Application {

  private static final String COLORED_DIR_PATH = "colored_pages";
  private static String CACHE_TABLE = "sentence_cache";
  private static String CACHE_REL_TABLE = "relation_cache";

  @Override
  protected boolean body() {
     try {
      CacheUtils.buildCategoryCache(Categories.LITERATURE_CATEGORY);
       //dbController.writeErrorDocument("123","321","222");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }
}


