package sesa.application;

import edu.stanford.nlp.io.IOUtils;
import sesa.common.dataModule.ReadTreeDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.model.processing.document.IDocument;
import sesa.common.utility.common.Application;
import sesa.common.utility.io.FileUtility;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.tools.cache.CacheUtils;

import java.io.ObjectInputStream;

/**
 * Created by Ira on 16.02.2016.
 */
public class Sandbox2 extends Application {

    @Override
    protected boolean body() {
        try {
            // Small Language model
            String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
            String bigLanguageModel = "./data/grammar/englishFactored.ser.gz";
            ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel); //parserModel


            String text = FileUtility.readAllText("./data/ada_cache/db_text.txt");
          //  StanfordDataModule dataModule = new BasicDataModule();
            StanfordDataModule dataModule = new ReadTreeDataModule("tree_cache_computing");
            DocumentBuilder builder = new StanfordRelationsBuilder(inputStream, dataModule);


            builder.setData(text, "id");
            IDocument document = builder.buildDocument();
            System.out.println(document.getSentences().size());
            CacheUtils.generatePaintedDocument(document);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }
}
