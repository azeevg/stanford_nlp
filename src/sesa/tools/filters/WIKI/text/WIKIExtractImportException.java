package sesa.tools.filters.WIKI.text;

public class WIKIExtractImportException extends Exception {
    public WIKIExtractImportException(String message, String title) {
        super("Parse Exception: title = " + title + " ,details = " + message);
    }
}
