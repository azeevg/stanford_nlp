package sesa.tools.dictionary;

import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.vector.DocumentUtils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class SemanticGroupDeviationBuilder extends Application {
    @Override
    protected boolean body() {

        try {
            final File manualFolder = new File("./data/c_colored_pages/manual_parse");
            List<String> manualFiles = listFilesForFolder(manualFolder);

            final File stanfordFolder = new File("./data/c_colored_pages/colored_pages");
            List<String> stanfordFiles = listFilesForFolder(stanfordFolder);

            for (String manualFilePath : manualFiles) {
                File manualFile = new File(manualFilePath);
                File stanfordFile = new File("./data/c_colored_pages/colored_pages/" + manualFile.getName());
                List<SentenceTree> st1 = null;
                st1 = DocumentUtils.loadSentencesFromXml(manualFilePath);
                List<SentenceTree> st2 = DocumentUtils.loadSentencesFromXml(stanfordFile.getAbsolutePath());
                //RoleDeviationStatistic statistic = SemanticUtils.matchSentencesSemantic(st1, st2);
                //System.out.println(statistic);
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return true;
    }

    public List<String> listFilesForFolder(final File folder) {
        List<String> files = new LinkedList<String>();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory())
                files.add(fileEntry.getAbsolutePath());
        }
        return files;
    }


}
