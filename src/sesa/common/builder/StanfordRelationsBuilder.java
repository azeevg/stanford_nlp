package sesa.common.builder;

import edu.spbstu.egor.Grammar;
import edu.spbstu.egor.SentenceGetter;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import grammarscope.browser.RelationFilter;
import grammarscope.common.Persist;
import grammarscope.parser.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.builder.semanticRule.RulesForSemanticGroup;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.model.processing.sentence.SentenceForest;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.model.processing.support.PositionComparator;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.semantics.TripletFinder;
import sesa.common.utility.semantic.SentenceUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.*;


/**
 * A set of methods for semantic and syntactic analysis of the text
 */
public class StanfordRelationsBuilder extends BasicBuilder {

    public final Logger log = LogManager.getLogger(StanfordRelationsBuilder.class);
    private Analyzer analyzer;
    private RelationFilter relationFilter;
    private Parser parser;
    private RelationModel relationModel;
    private StanfordDataModule dataModule;

    /**
     * Constructor
     * initialization
     * load model  Stanford Lexicalized Parser
     *
     * @param thisInputStream
     * @param dataModule
     */
    public StanfordRelationsBuilder(ObjectInputStream thisInputStream, StanfordDataModule dataModule) {
        init(dataModule);
        parser = new Parser(thisInputStream); // load model  Stanford Lexicalized Parser
        log.info("end of constructor StanfordRelationsBuilder");
    }

    public StanfordRelationsBuilder(String languageModel, StanfordDataModule dataModule) {
        init(dataModule);
        parser = new Parser(languageModel); // load model  Stanford Lexicalized Parser and initialization grammar
        log.info("end of constructor StanfordRelationsBuilder");
    }

    /**
     * Common init part for constructor
     *
     * @param dataModule
     */
    public void init(StanfordDataModule dataModule) {
        RelationModel.makeGovDepModel();
        relationModel = DefaultMutableGrammaticalRelations.makeDefaultModel();
        analyzer = new Analyzer(relationModel);
        Properties filterProperties = Persist.loadSettings("./data/filter/relations-filter.properties");
        relationFilter = new RelationFilter(); //Relation display filter
        relationFilter.setProperties(filterProperties); // Load properties into maps
        this.dataModule = dataModule;
    }


    /**
     * EGOR
     * Constructor
     *
     * @param grammar initialization grammar was before
     */
    public StanfordRelationsBuilder(Grammar grammar) {
        dataModule = new BasicDataModule();
        Properties filterProperties = Persist.loadSettings("./data/filter/relations-filter.properties");
        relationFilter = new RelationFilter();
        relationFilter.setProperties(filterProperties);
        relationModel = DefaultMutableGrammaticalRelations.makeDefaultModel();
        analyzer = new Analyzer(relationModel);
        parser = new Parser(grammar.lexicalizedParser);
    }

    /**
     * Set key value. The key is responsible for selecting the algorithm build parse tree
     *
     * @param isModCKY key
     *                 true - if you want to use modified CKY algorithm for parsing sentence
     *                 false - if you want to use stanford CKY algorithm for parsing sentence
     */
    @Override
    public void setIsModCKY(boolean isModCKY) {
        this.isModCKY = isModCKY;
    }


    public void postProcess() {
        dataModule.postProcess(title);
    }


    /**
     * The syntactic and semantic analysis for each sentences from the paragraph
     * Syntactic analysis: build parse trees for each sentence
     * Semantic analysis: find semantic groups in the sentences from text
     *
     * @param text    text to analysis
     * @param isTitle flag
     * @param level   number level in paragraph ( exm. <h1> </h1> => level = 1)
     * @return list of sentences forests (it's list of parse trees) for every sentence from paragraph
     * @throws BuilderException
     */
    @Override
    protected List<SentenceForest> buildParagraphSubTree(String text, boolean isTitle, int level) throws BuilderException {

        text = removeTextInBrackets(text);                                             // rule 6
        if (text.trim().length() == 1) {
            return null;
        }
        List<SentenceForest> paragraph = new LinkedList<SentenceForest>();

        try {
            Document document = new Document(dataModule);
            document.create(text);
            dataModule.init(parser, analyzer, document, relationFilter);
            //System.out.println(title.hashCode());
            dataModule.updateTreeCache(title, document.theSentences, isModCKY);
            // System.out.println("end of updateTreeCache");
            dataModule.updateRelationCache(title, document.theSentences);
            log.info(document.theSentences.size());
            for (Sentence sentence : document.theSentences) {
                // System.out.println(sentence);
                SentenceForest forest = new SentenceForest();
                SentenceTree tree = new SentenceTree();


                tree.setTriplet(getSentenceTriplet(sentence, document));
                HashMap<Object, SentenceToken> wordTokenMap = new HashMap<Object, SentenceToken>();
                for (Word word : sentence)
                    wordTokenMap.put(word, new SentenceToken(word.value()));

                // Only this method links relations and tokens
                List<SemanticGroup> semanticGroups = getSentenceSemanticGroups(sentence, document, wordTokenMap);
                tree.setSemanticGroups(semanticGroups);
                for (SemanticGroup group : semanticGroups) {
                    group.setSentenceTree(tree);
                }

                tree.setIsTitle(isTitle);
                tree.setTokens(getSentenceTokens(sentence, wordTokenMap));
                tree.setSentence(sentence.toString());
                tree.setLevel(level);
                forest.addTree(tree);
                paragraph.add(forest);
            }
        } catch (Exception e) {
            throw new BuilderException(e.getMessage() + " | " + e.getStackTrace().toString(), e);
        }
        log.info("end of buildParagraphSubTree in StanfordRelationsBuilder");
        log.info(paragraph.size());
        return paragraph;
    }

    private SentenceTriplet getSentenceTriplet(Sentence sentence, Document document) {
        Tree parse = document.theParsedTrees.get(sentence);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        try {
            parse.indentedXMLPrint(writer);
        } catch (NullPointerException e) {
            log.error(e.getMessage());

        }
        String xmlTree = stringWriter.toString();
        SentenceTriplet triplet = TripletFinder.findSentenceTriplet(xmlTree);
        return triplet;
    }

    private List<SentenceToken> getSentenceTokens(Sentence sentence, HashMap<Object, SentenceToken> wordTokenMap) {

        List<SentenceToken> tokens = new LinkedList<SentenceToken>();
        for (Word word : sentence)
            tokens.add(wordTokenMap.get(word));
        return tokens;
    }


    private String removeTextInBrackets(String text) {
        return text.replaceAll("\\([^\\)]*\\)", " ");
    }

    /**
     * Find semantic groups in sentence.
     * Apply the rules to the parse tree and dependency tree to find groups
     * 1) Grammar rule
     * 2) Structure Rules
     * The rules contain in <b> class RulesForSemanticGroup </b>
     * Used shortened set of grammatical relations to find semantic groups
     *
     * @param sentence     sentence: list of words
     * @param document
     * @param wordTokenMap
     * @return list of semantic group for one sentence
     * @throws IOException
     */
    public List<SemanticGroup> getSentenceSemanticGroups(Sentence sentence, Document document, HashMap<Object, SentenceToken> wordTokenMap) throws IOException {

        Map<Tree, SemanticGroup> subSentenceMap = new HashMap<Tree, SemanticGroup>();
        HashSet<Tree> skipNodes = new HashSet<Tree>();

        List<SemanticRelation> relations = dataModule.getFilteredRelations(sentence);

        if (relations == null) {
            log.error("FilteredRelations = null");
            return null;
        }

        int groupId = 0;
        //The semantic tree Relationship
        for (SemanticRelation relation : relations) {
            //System.out.println(relation);
            Tree root = document.theParsedTrees.get(sentence);
            // System.out.println("###:" + root);
            Tree relationNode = RulesForSemanticGroup.checkRelationNode(root, relation.getTreeHash());

            //setPPSkips(root, relationNode, skipNodes);
            Tree sentenceNode = RulesForSemanticGroup.findSemanticRoot(root, relationNode);
            if (sentenceNode == null)
                continue;

            Word word = document.theLeafToWordMap.get(relationNode);
            //relation.setSentencePosition(sentence.indexOf(word));
            SentenceToken token = wordTokenMap.get(word);
            token.setSentencePosition(sentence.indexOf(word));
            token.setBaseValue(relation.getTokenBaseValue());

            Tree parentNode = relationNode.parent(root);

            if (parentNode != null)
                token.setPartOfSpeech(parentNode.label().value());
            //token.setLowerValue(token.getValue().toLowerCase());
            List<SemanticRelation> semanticRelations = new LinkedList<SemanticRelation>();
            semanticRelations.add(relation);
            token.setRelations(semanticRelations);
            relation.setToken(token);

            if (!subSentenceMap.containsKey(sentenceNode)) {
                SemanticGroup group = new SemanticGroup(new PositionComparator());
                group.setId(groupId);
                groupId++;
                if (RulesForSemanticGroup.isRelationValid(group, relation)) {
                    //group.addRelation(relation);
                    group.addToken(token);
                    //relation.setGroup(group);
                    //relation.getToken().setGroup(group);
                    token.setGroup(group);
                    subSentenceMap.put(sentenceNode, group);
                }
            } else {
                SemanticGroup group = subSentenceMap.get(sentenceNode);
                token.setGroup(group);
                //relation.setGroup(group);
                //if (isRelationValid(group, relation))
                //group.addRelation(relation);
                group.addToken(token);
            }
        }

        LinkedList<SemanticGroup> groups = new LinkedList<SemanticGroup>(subSentenceMap.values());
        RulesForSemanticGroup.clearSemanticGroups(groups); // apply GR for all element in groups

        return groups;
    }

//-------------------------------------------------

    //--   EGOR methods

    /**
     * Get semantic groups for one sentence
     * Used all set of grammatical relations to find semantic groups
     *
     * @param listSent     sentence
     * @param sentenceTree parse tree sentence
     * @return list of semantic relation for one sentence
     */
    public ArrayList<SemanticRelation> getGrammaticalTextStructure(List<? extends HasWord> listSent, Tree sentenceTree) throws SQLException, ClassNotFoundException {
        ArrayList<SemanticRelation> allRelations = new ArrayList<SemanticRelation>();
        Document document = new Document(dataModule);
        Sentence oldSentence = SentenceUtils.convertWordsToSentence(listSent);
        oldSentence = SentenceUtils.convertSentence(oldSentence, parser.getCollocationsIndex());
        System.out.println(oldSentence);
        document.create(oldSentence.toString());
        dataModule.init(parser, analyzer, document, relationFilter);

        assert document.theSentences.size() == 1;

        Sentence sentence = document.theSentences.get(0);
        System.out.println("###Sentence:" + oldSentence);

        // EGOR_INFO: better to wrap it into Document class method, because view scope of leaves2Words method was changed
        document.oneSentenceInit(sentence, sentenceTree);

        try {
            SentenceTree tree = new SentenceTree();

            HashMap<Object, SentenceToken> wordTokenMap = new HashMap<Object, SentenceToken>();
            for (Word word : sentence)
                wordTokenMap.put(word, new SentenceToken(word.value()));

            // Also this method links relations and tokens
            List<SemanticGroup> semanticGroups = getSentenceSemanticGroups(sentence, sentenceTree, document, wordTokenMap);
            tree.setSemanticGroups(semanticGroups);
            for (SemanticGroup group : semanticGroups) {
                group.setSentenceTree(tree);
            }

            allRelations = tree.getAllRelations();

            tree.setTokens(getSentenceTokens(sentence, wordTokenMap));
            tree.setSentence(sentence.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return allRelations;
    }


    /**
     * Get semantic relations for all text
     *
     * @param sentenceGetter sentence
     * @return
     */
    public ArrayList<ArrayList<SemanticRelation>> getGrammaticalTextStructure(SentenceGetter sentenceGetter) throws SQLException, ClassNotFoundException {
        List<Sentence> sentenceList = new ArrayList<Sentence>();
        ArrayList<ArrayList<SemanticRelation>> allRelations = new ArrayList<ArrayList<SemanticRelation>>();
        Document document = new Document(dataModule);
        document.create(sentenceGetter.getText());
        dataModule.init(parser, analyzer, document, relationFilter);

        try {
            dataModule.updateTreeCache(title, document.theSentences, isModCKY); // EGOR_INFO: here sentences are parsed
            log.info("###sentenceList" + sentenceList);
            dataModule.updateRelationCache(title, document.theSentences);

            for (Sentence sentence : document.theSentences) {
                System.out.println("sentence:" + sentence);
                SentenceForest forest = new SentenceForest();
                SentenceTree tree = new SentenceTree();

                HashMap<Object, SentenceToken> wordTokenMap = new HashMap<Object, SentenceToken>();
                for (Word word : sentence)
                    wordTokenMap.put(word, new SentenceToken(word.value()));
                System.out.println("wordTokenMap:" + wordTokenMap.size() + ", value: " + wordTokenMap);
                // Also this method links relations and tokens
                List<SemanticGroup> semanticGroups = getSentenceSemanticGroups(sentence, document, wordTokenMap);

                tree.setSemanticGroups(semanticGroups);
                for (SemanticGroup group : semanticGroups) {
                    group.setSentenceTree(tree);
                }
                allRelations.add(tree.getAllRelations());

                tree.setTokens(getSentenceTokens(sentence, wordTokenMap));
                tree.setSentence(sentence.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return allRelations;
    }

    // That method do the same that method getSentenceSemanticGroups(Sentence sentence, Document document, HashMap<Object, SentenceToken> wordTokenMap)
    public List<SemanticGroup> getSentenceSemanticGroups(Sentence sentence, Tree sentenceTree, Document document, HashMap<Object, SentenceToken> wordTokenMap) throws IOException {

        Map<Tree, SemanticGroup> subSentenceMap = new HashMap<Tree, SemanticGroup>();
        HashSet<Tree> skipNodes = new HashSet<Tree>();

        List<SemanticRelation> relations = dataModule.getFilteredRelations(sentence);


        if (relations == null) {
            log.error("FilteredRelations = null");
            return null;
        }


        int groupId = 0;
        for (SemanticRelation relation : relations) {
            Tree root = sentenceTree;
            Tree relationNode = RulesForSemanticGroup.checkRelationNode(root, relation.getTreeHash());

            //setPPSkips(root, relationNode, skipNodes);
            Tree sentenceNode = RulesForSemanticGroup.findSemanticRoot(root, relationNode);
            if (sentenceNode == null)
                continue;

            Word word = document.theLeafToWordMap.get(relationNode);
            //relation.setSentencePosition(sentence.indexOf(word));
            SentenceToken token = wordTokenMap.get(word);
            token.setSentencePosition(sentence.indexOf(word));
            token.setBaseValue(relation.getTokenBaseValue());
            Tree parentNode = relationNode.parent(root);
            if (parentNode != null)
                token.setPartOfSpeech(parentNode.label().value());
            //token.setLowerValue(token.getValue().toLowerCase());
            List<SemanticRelation> semanticRelations = new LinkedList<SemanticRelation>();
            semanticRelations.add(relation);
            token.setRelations(semanticRelations);
            relation.setToken(token);

            if (!subSentenceMap.containsKey(sentenceNode)) {
                SemanticGroup group = new SemanticGroup(new PositionComparator());
                group.setId(groupId);
                groupId++;
                if (RulesForSemanticGroup.isRelationValid(group, relation)) {
                    //group.addRelation(relation);
                    group.addToken(token);
                    //relation.setGroup(group);
                    //relation.getToken().setGroup(group);
                    token.setGroup(group);
                    subSentenceMap.put(sentenceNode, group);
                }
            } else {
                SemanticGroup group = subSentenceMap.get(sentenceNode);
                token.setGroup(group);
                //relation.setGroup(group);
                //if (isRelationValid(group, relation))
                //group.addRelation(relation);
                group.addToken(token);
            }
        }

        LinkedList<SemanticGroup> groups = new LinkedList<SemanticGroup>(subSentenceMap.values());
        RulesForSemanticGroup.clearSemanticGroups(groups);
        System.out.println(groups.toString());
        return groups;
    }
}
