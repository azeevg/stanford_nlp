package grammarscope.parser;

import java.util.Collection;

import edu.stanford.nlp.international.morph.MorphoFeatureSpecification;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.objectbank.TokenizerFactory;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeReaderFactory;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.util.Filter;
import edu.stanford.nlp.util.Function;

public class TreebankLanguagePackProxy implements TreebankLanguagePack
{
	private static final long serialVersionUID = 1L;

	protected TreebankLanguagePack theLanguagePack;

	protected Collection<GrammaticalRelation> theRelations;

	// C O N S T R U C T O R

	public TreebankLanguagePackProxy(final TreebankLanguagePack delegate, final Collection<GrammaticalRelation> theseRelations)
	{
		this.theLanguagePack = delegate;
		this.theRelations = theseRelations;
	}

	// G R A M M A T I C A L S T R U C T U R E F A C T O R Y

	public GrammaticalStructureProxyFactory grammaticalStructureFactory(final Filter<String> thisPunctFilter)
	{
		final HeadFinder thisHeadFinder = new SemanticHeadFinder();
		return new GrammaticalStructureProxyFactory(this.theRelations, thisPunctFilter, thisHeadFinder);
	}

	public GrammaticalStructureProxyFactory grammaticalStructureFactory()
	{
		final Filter<String> thisPunctFilter = this.theLanguagePack.punctuationWordRejectFilter();
		// final HeadFinder thisHeadFinder = new SemanticHeadFinder();
		final HeadFinder thisHeadFinder = this.theLanguagePack.headFinder();
		return new GrammaticalStructureProxyFactory(this.theRelations, thisPunctFilter, thisHeadFinder);
	}

	public GrammaticalStructureFactory grammaticalStructureFactory(Filter<String> thisPunctFilter, HeadFinder thisHeadFinder)
	{
		return new GrammaticalStructureProxyFactory(this.theRelations, thisPunctFilter, thisHeadFinder);
	}

	// D E L E G A T E

	public boolean isPunctuationTag(final String str)
	{
		return this.theLanguagePack.isPunctuationTag(str);
	}

	public boolean isPunctuationWord(final String str)
	{
		return this.theLanguagePack.isPunctuationWord(str);
	}

	public boolean isSentenceFinalPunctuationTag(final String str)
	{
		return this.theLanguagePack.isSentenceFinalPunctuationTag(str);
	}

	public boolean isEvalBIgnoredPunctuationTag(final String str)
	{
		return this.theLanguagePack.isEvalBIgnoredPunctuationTag(str);
	}

	public Filter<String> punctuationTagAcceptFilter()
	{
		return this.theLanguagePack.punctuationTagAcceptFilter();
	}

	public Filter<String> punctuationTagRejectFilter()
	{
		return this.theLanguagePack.punctuationTagRejectFilter();
	}

	public Filter<String> punctuationWordAcceptFilter()
	{
		return this.theLanguagePack.punctuationWordAcceptFilter();
	}

	public Filter<String> punctuationWordRejectFilter()
	{
		return this.theLanguagePack.punctuationWordRejectFilter();
	}

	public Filter<String> sentenceFinalPunctuationTagAcceptFilter()
	{
		return this.theLanguagePack.sentenceFinalPunctuationTagAcceptFilter();
	}

	public Filter<String> evalBIgnoredPunctuationTagAcceptFilter()
	{
		return this.theLanguagePack.evalBIgnoredPunctuationTagAcceptFilter();
	}

	public Filter<String> evalBIgnoredPunctuationTagRejectFilter()
	{
		return this.theLanguagePack.evalBIgnoredPunctuationTagRejectFilter();
	}

	public String[] punctuationTags()
	{
		return this.theLanguagePack.punctuationTags();
	}

	public String[] punctuationWords()
	{
		return this.theLanguagePack.punctuationWords();
	}

	public String[] sentenceFinalPunctuationTags()
	{
		return this.theLanguagePack.sentenceFinalPunctuationTags();
	}

	public String[] sentenceFinalPunctuationWords()
	{
		return this.theLanguagePack.sentenceFinalPunctuationWords();
	}

	public String[] evalBIgnoredPunctuationTags()
	{
		return this.theLanguagePack.evalBIgnoredPunctuationTags();
	}

	public String getEncoding()
	{
		return this.theLanguagePack.getEncoding();
	}

	public TokenizerFactory<? extends HasWord> getTokenizerFactory()
	{
		return this.theLanguagePack.getTokenizerFactory();
	}

	public char[] labelAnnotationIntroducingCharacters()
	{
		return this.theLanguagePack.labelAnnotationIntroducingCharacters();
	}

	public boolean isLabelAnnotationIntroducingCharacter(final char ch)
	{
		return this.theLanguagePack.isLabelAnnotationIntroducingCharacter(ch);
	}

	public String basicCategory(final String category)
	{
		return this.theLanguagePack.basicCategory(category);
	}

	public String stripGF(final String category)
	{
		return this.theLanguagePack.stripGF(category);
	}

	public Function<String, String> getBasicCategoryFunction()
	{
		return this.theLanguagePack.getBasicCategoryFunction();
	}

	public String categoryAndFunction(final String category)
	{
		return this.theLanguagePack.categoryAndFunction(category);
	}

	public Function<String, String> getCategoryAndFunctionFunction()
	{
		return this.theLanguagePack.getCategoryAndFunctionFunction();
	}

	public boolean isStartSymbol(final String str)
	{
		return this.theLanguagePack.isStartSymbol(str);
	}

	
	public Filter<String> startSymbolAcceptFilter()
	{
		return this.theLanguagePack.startSymbolAcceptFilter();
	}

	public String[] startSymbols()
	{
		return this.theLanguagePack.startSymbols();
	}

	public String startSymbol()
	{
		return this.theLanguagePack.startSymbol();
	}

	public String treebankFileExtension()
	{
		return this.theLanguagePack.treebankFileExtension();
	}

	public void setGfCharacter(final char gfCharacter)
	{
		this.theLanguagePack.setGfCharacter(gfCharacter);
	}

	public TreeReaderFactory treeReaderFactory()
	{
		return this.theLanguagePack.treeReaderFactory();
	}

	public TokenizerFactory<Tree> treeTokenizerFactory()
	{
		return this.theLanguagePack.treeTokenizerFactory();
	}

	public HeadFinder headFinder()
	{
		return this.theLanguagePack.headFinder();
	}
  
	public HeadFinder typedDependencyHeadFinder()
	{
		return this.theLanguagePack.typedDependencyHeadFinder();
	}

	public MorphoFeatureSpecification morphFeatureSpec()
	{
		return this.theLanguagePack.morphFeatureSpec();
	}
}
