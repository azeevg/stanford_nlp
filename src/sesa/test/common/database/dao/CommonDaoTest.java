package sesa.test.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import sesa.common.database.dao.CommonDao;

import java.sql.SQLException;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


/**
 * Created by Ira on 05.05.2016.
 */
public class CommonDaoTest {
    public final Logger log = LogManager.getLogger(CommonDaoTest.class);

    @Test
    public void testDataBase() throws SQLException, ClassNotFoundException {
        CommonDao commonDao = new CommonDao();
        assertTrue(commonDao.isTableExists("cats_dicts"));
        assertFalse(commonDao.isTableExists("abs"));
        assertTrue(commonDao.isTableExists("cats_dicts"));
    }
}
