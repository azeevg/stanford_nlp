package sesa.tools.math;

import sesa.tools.math.metrics.IMetrics;

import java.util.List;

/**
 * ��������� ������������
 */
public class VectorSpace {

    private List<Vector> vectors; // ������ ��������
    private Vector center;        // ����������� ������
    private IMetrics metrics;     // �������
    private double maxRadius;     // ������

    public VectorSpace(List<Vector> vectors, Vector center, IMetrics metrics, double maxRadius) {
        this.vectors = vectors;
        this.center = center;
        this.metrics = metrics;
        this.maxRadius = maxRadius;
    }

    public Vector getCenter() {
        return center;
    }

    public List<Vector> getVectors() {
        return vectors;
    }

    public IMetrics getMetrics() {
        return metrics;
    }

    public double getMaxRadius() {
        return maxRadius;
    }

}
