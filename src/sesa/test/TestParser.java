package sesa.test;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.egor.*;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.parser.lexparser.ExhaustivePCFGParser;
import edu.stanford.nlp.trees.*;
import grammarscope.parser.Parser;
import org.junit.Test;
import sesa.common.utility.collocation.CollocationUtility;
import sesa.common.utility.semantic.SentenceUtils;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ira on 06.04.2016.
 */
public class TestParser {
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";


    @Test
    public void testStanfordParserGrammarScope() throws IOException {

        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        Tree tree = parser.parse(SentenceUtils.readSentenceFromFile("data/test.txt"), false);
        tree.pennPrint();

    }

    @Test
    public void testKBestParseStanfordParserGrammarScope() throws IOException {

        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        List<Tree> trees = parser.getKBestParse(SentenceUtils.readSentenceFromFile("data/Gleb_test.txt"), false, 5);
        System.out.println("------- k best parse  -------");
//        for( Tree tree: trees){
//            tree.pennPrint();
//        }
        // Tree tree = parser.parse(prepareSentence("data/test.txt"),false);
        //  tree.pennPrint();

    }


    @Test
    public void testCollocationParserGrammarScope() throws IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        Tree tree = parser.parse(SentenceUtils.readSentenceFromFile("data/test.txt"), true);
        System.out.println(parser.getCollocationsIndex());
        tree.pennPrint();
    }


    @Test
    public void testCollocationParserGrammarScope_3() throws IOException {

        //  ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(smallLanguageModel);
        Tree tree = parser.parse(SentenceUtils.readSentenceFromFile("data/test.txt"), true);
        tree.pennPrint();
    }

    @Test
    public void testKBestParseCollocationParserGrammarScope() throws IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        List<Tree> trees = parser.getKBestParse(SentenceUtils.readSentenceFromFile("data/test.txt"), true, 2);
        System.out.println("------- k best parse  -------");
//        for( Tree tree: trees){
//            tree.pennPrint();
//        }
    }

    @Test
    public void testCollocationParser() throws IOException {
        Grammar grammar = new Grammar();

        // parser factory used for easier access to parsers
        ParserFactory parserFactory = new ParserFactory(grammar);

        // reading collocations dictionary
        final String[] collocationDictFiles = {"./egor_in/collocations.txt"};
        CollocationDictionary collocationDictionary = new CollocationDictionary(collocationDictFiles);

        // creating parser
        ExhaustivePCFGParser collocationExtendedParser =
                parserFactory.getCollocationSpanTaggingParser(collocationDictionary);

        // creating tree getter (actually is just a wrapper...)
        TreeGetter tg = new TreeGetter();
        tg.setPparser(collocationExtendedParser);

        List<String> strings = CollocationUtility.readCollocation("./egor_in/collocations.txt");
        TreeUtils treeUtils = new TreeUtils(grammar.op);
        PrintWriter stdPW = new PrintWriter(new File("out_trees.txt"));
        for (String s : strings) {
            edu.spbstu.appmath.collocmatch.Sentence sentence = new edu.spbstu.appmath.collocmatch.Sentence(s);
            Tree tree = tg.getBestParseTree(SentenceGetter.convert(sentence));

            treeUtils.preprocessedPennPrint2(tree, stdPW);
            System.out.println("----------");
        }
        stdPW.close();
    }

    @Test
    public void testKBestParse() throws IOException {
        Grammar grammar = new Grammar();

        // parser factory used for easier access to parsers
        ParserFactory parserFactory = new ParserFactory(grammar);

        // reading collocations dictionary
        final String[] collocationDictFiles = {"./egor_in/collocations.txt"};
        CollocationDictionary collocationDictionary = new CollocationDictionary(collocationDictFiles);

        // creating parser
        ExhaustivePCFGParser collocationExtendedParser =
                parserFactory.getCollocationSpanTaggingParser(collocationDictionary);

        // creating tree getter (actually is just a wrapper...)
        TreeGetter tg = new TreeGetter();
        tg.setPparser(collocationExtendedParser);
        edu.spbstu.appmath.collocmatch.Sentence sentence = new edu.spbstu.appmath.collocmatch.Sentence("3D computer graphics , programming language , algebra stubs and computing .");

        // Tree Utils for tree printing
        TreeUtils treeUtils = new TreeUtils(grammar.op);

        ArrayList<Tree> trees = tg.getKBestParseTrees(SentenceGetter.convert(sentence), 1);
        treeUtils.preprocessedPennPrint(trees.get(0));
    }

    @Test
    public void GlebAzeevTest() throws Exception {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        Tree tree = parser.parse(SentenceUtils.readSentenceFromFile("data/Gleb_test.txt"), true);

        GrammaticalStructure stanfGs = new EnglishGrammaticalStructure(tree);

        List<TypedDependency> filteredDependencies = stanfGs.typedDependencies(false);

        EnglishGrammaticalStructure.printDependencies(stanfGs, filteredDependencies, tree, false, false);

    }
}
