package sesa.common.dataModule;

import edu.stanford.nlp.trees.Tree;
import grammarscope.browser.RelationFilter;
import grammarscope.parser.*;
import sesa.common.semantics.SemanticRelation;

import java.io.IOException;
import java.util.List;

/**
 * The interface provides the methods to work with data: the parse trees, grammatical relations
 */
public interface StanfordDataModule {

    /**
     * Get parse tree of sentence
     * @param sentence
     * @return parse tree
     */
    Tree getTree(Sentence sentence);

    /**
     * The method returns a shorter list of grammatical relations for sentence
     * Because our algorithms require a shortened set of grammatical relations
     * In that method we are not find again grammatical relations for sentence!
     * @param sentence input sentence
     * @return a list of grammatical relations
     * @throws IOException
     */
    List<SemanticRelation> getFilteredRelations(Sentence sentence) throws IOException;


    /**
     * Read parse trees and write parse tree to database
     * @param title
     * @param sentences
     * @param isModCKY
     * @throws Exception
     */
    void updateTreeCache(String title, List<Sentence> sentences, boolean isModCKY) throws Exception;

    /**
     * Read relation cache
     * @param title
     * @param sentences
     */
    void updateRelationCache(String title, List<Sentence> sentences); // TODO: Change name

    /**
     * initialize data module to build parse trees and find grammatical relation in sentence
     * @param parser
     * @param analyzer
     * @param document
     * @param relationFilter
     */
    void init(Parser parser, Analyzer analyzer, Document document, RelationFilter relationFilter);


    void postProcess(String title);

    List<Sentence> getSentenceList();

    int getCollocationNumberInSentence();
    int getCollocationNumberInParseTree();
}
