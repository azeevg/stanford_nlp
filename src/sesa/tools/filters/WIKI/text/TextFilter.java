package sesa.tools.filters.WIKI.text;

import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.configuration.Categories;

import java.sql.SQLException;

public class TextFilter extends Application {

    private int counter = 0;
    private final DocumentsDao documentsDao = new DocumentsDao();

    public TextFilter() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        String[] categoryNames = new String[]{Categories.CHEMISTRY_CATEGORY, Categories.PHYSICS_CATEGORY, Categories.BIOLOGY_CATEGORY};
        //dbController.useConnection(1);

        for (String categoryName : categoryNames) {
            String categoryCollectionName = Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI + "_" + categoryName;
            String textCollectionName = Configuration.DB_COLLECTION_TEXT_WIKI + "_" + categoryName;
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryCollectionName);
            //dbController.getDocumentsCollectionIterator(categoryCollectionName);
            iterator = new DBReportingIterator(iterator, "Text Filtering | category =  " + categoryName);
            documentsDao.truncateOrCreateDocumentCollection(textCollectionName);
           // dbController.truncateOrCreate(textCollectionName);
            while (iterator.hasNext()) {
                IDBEntity entity = iterator.next().get();
                WIKIExtractProcessor processor = new WIKIExtractProcessor(entity);
                try {
                    IDBEntity filteredDoc = processor.processDocument();
                    documentsDao.addToDocumentsCollection(textCollectionName, filteredDoc);
                   // dbController.insertToDocumentsCollection(textCollectionName, filteredDoc);
                } catch (WIKIExtractImportException e) {
                    LoggingUtility.error(e.getMessage());
                }

                if (counter++ % 1000 == 0)
                    System.gc();
            }
        }

        return false;
    }
}
