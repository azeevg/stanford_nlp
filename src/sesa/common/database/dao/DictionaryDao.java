package sesa.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.dictionary.DictionarySourceDao;
import sesa.common.database.entity.DBTermEntity;
import sesa.common.database.entity.IDBEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Ira on 18.04.2016.
 */

/**
 * Class for work with table: dictionary
 * table contains 4 columns: id, HEAD_WORDS, DEF, IMPORTED, SRC
 */
@Deprecated
public class DictionaryDao extends CommonDao {
    public final Logger log = LogManager.getLogger(DictionaryDao.class);
    private final int sourceId = new DictionarySourceDao().getSourceId("IBM");

    public DictionaryDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public boolean addToDictionaryCollections(Map<String, String> map, String collectionName) {
        PreparedStatement ps = null;
        Connection connection = null;

        boolean isInserted = false;
        try {
            connection = cp.takeConnection();
            for (String key : map.keySet()) {

                String def = map.get(key);
                // System.out.println("key:" + key);
                String query = "INSERT INTO `" + collectionName + "` (HEAD_WORDS ,DEF,IMPORTED,SRC) VALUES(?,?,?,?)";
                ps = connection.prepareStatement(query);

                ps.setString(1, key);
                ps.setString(2, def);
                ps.setTimestamp(3, getNow());
//                ps.setString(4, "http://computerlanguage.com/terms/");
                ps.setInt(4, sourceId);
                ps.execute();
            }

            isInserted = true;
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps);
        }

        return isInserted;

    }
    //SELECT LAST_INSERT_ID();

    public List<IDBEntity> getDictionary(String collectionName) {
        return getDictionary(collectionName, 0, countTotalOfRow(collectionName));
    }


    public List<IDBEntity> getDictionary(String collectionName, long from, long size) {
        List<IDBEntity> resultList = new LinkedList<IDBEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` LIMIT ?,?");

            ps.setLong(1, from);
            ps.setLong(2, size);
            ps.execute();
            resultSet = ps.getResultSet();
            while (resultSet.next())
                resultList.add(new DBTermEntity((String) resultSet.getObject("HEAD_WORDS"), (String) resultSet.getObject("DEF")));

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
            resultList = null;

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return resultList;


    }

    public IDBEntity getTermDefinitionByName(String collectionName, String termName) {
        IDBEntity result = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE HEAD_WORDS = ?");
            ps.setString(1, termName);
            ps.execute();
            resultSet = ps.getResultSet();

            if (resultSet.next()) {
                //  System.out.println((String) resultSet.getObject("DEF"));
                result = new DBTermEntity(termName, (String) resultSet.getObject("DEF"));
            }

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return result;

    }

    public List<IDBEntity> fullTextSearch(String collectionName, String text) {
        List<IDBEntity> resultList = new LinkedList<IDBEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE MATCH (HEAD_WORDS,DEF) AGAINST (?)");
            ps.setString(1, text);
            ps.execute();
            resultSet = ps.getResultSet();
            while (resultSet.next())
                resultList.add(new DBTermEntity((String) resultSet.getObject("HEAD_WORDS"), (String) resultSet.getObject("DEF")));

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
            resultList = null;

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return resultList;
    }


}
