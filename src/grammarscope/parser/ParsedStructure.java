package grammarscope.parser;

import edu.stanford.nlp.trees.Tree;

/**
 * Parsed subject-predicate-object structure which collects parsing results
 * 
 * @author Bernard Bou
 */
public class ParsedStructure
{
	/**
	 * Predicate tree
	 */
	public Tree thePredicateTree;

	/**
	 * Subject tree
	 */
	public Tree theSubjectTree;

	/**
	 * Object tree
	 */
	public Tree theObjectTree;

	/**
	 * Predicate string
	 */
	public String thePredicate;

	/**
	 * Subject string
	 */
	public String theSubject;

	/**
	 * Object string
	 */
	public String theObject;

	/**
	 * Constructor
	 * 
	 * @param thisPredicateTree
	 *            predicate tree
	 * @param thisSubjectTree
	 *            subject tree
	 * @param thisObjectTree
	 *            object tree
	 * @param thisPredicate
	 *            predicate
	 * @param thisSubject
	 *            subject
	 * @param thisObject
	 *            object
	 */
	public ParsedStructure(final Tree thisPredicateTree, final Tree thisSubjectTree, final Tree thisObjectTree, final String thisPredicate, final String thisSubject, final String thisObject)
	{
		this.thePredicateTree = thisPredicateTree;
		this.theSubjectTree = thisSubjectTree;
		this.theObjectTree = thisObjectTree;
		this.thePredicate = thisPredicate;
		this.theSubject = thisSubject;
		this.theObject = thisObject;
	}

	/**
	 * (Object-less) Constructor
	 * 
	 * @param thisPredicateTree
	 *            predicate tree
	 * @param thisSubjectTree
	 *            subject tree
	 * @param thisPredicate
	 *            predicate
	 * @param thisSubject
	 *            subject
	 */
	public ParsedStructure(final Tree thisPredicateTree, final Tree thisSubjectTree, final String thisPredicate, final String thisSubject)
	{
		this(thisPredicateTree, thisSubjectTree, null, thisPredicate, thisSubject, null);
	}
}
