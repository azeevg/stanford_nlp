package sesa.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.common.DBIterator;
import sesa.common.database.connection.ConnectionPool;
import sesa.common.database.connection.ConnectionPoolSingleton;


import java.sql.*;


/**
 * Common class for work with database.
 * <b>CommonDao</b> contains generic functions for working with database.
 * Classes which work with database are extending that class.
 */
public class CommonDao {
    protected ConnectionPool cp;
    public final Logger log = LogManager.getLogger(CommonDao.class);

    /**
     * In constructor executed connection to database
     */
    public CommonDao() throws SQLException, ClassNotFoundException {
        this.cp = ConnectionPoolSingleton.getInstance().getConnectionPool();
    }

    /**
     * Check for the existence of the table
     *
     * @param collectionName table name
     * @return result
     */
    public boolean isTableExists(String collectionName) {
        Connection connection = null;
        ResultSet resultSet = null;
        boolean isTableExist = false;
        try {
            connection = cp.takeConnection();
            DatabaseMetaData dbm = connection.getMetaData(); // ����� ��� ��������� �������������� � ��
            resultSet = dbm.getTables(null, null, collectionName, null);
            isTableExist = resultSet.next();
            // log.info("ok");
        } catch (SQLException e) {
            log.error(e);
        } finally {
            cp.closeConnection(connection, resultSet);
        }
        return isTableExist;
    }

    /**
     * Use this method to calculate the number of rows in table
     *
     * @param collectionName table name
     * @return number rows in table
     */
    public long countTotalOfRow(String collectionName) {
        Connection connection = null;
        PreparedStatement s = null;
        ResultSet resultSet = null;
        long collectionSize = 0;
        try {
            connection = cp.takeConnection();
            s = connection.prepareStatement("SELECT COUNT(*) as cnt FROM `" + collectionName + "`");
            s.execute();
            resultSet = s.getResultSet();

            if (resultSet.next())
                collectionSize = resultSet.getLong("cnt");

        } catch (SQLException e) {
            log.error("Can't obtain collection size: " + collectionName, e);

        } finally {
            cp.closeConnection(connection, s, resultSet);
        }

        return collectionSize;
    }

    /**
     * Delete all rows in table
     *
     * @param collectionName table name
     * @return
     */
    public boolean truncateDocumentCollection(String collectionName) {
        Connection connection = null;
        Statement statement = null;
        boolean isTruncate = false;
        try {
            connection = cp.takeConnection();
            statement = connection.createStatement();
            statement.executeQuery("TRUNCATE TABLE `" + collectionName + "`");
            isTruncate = true;
        } catch (SQLException e) {
            log.debug("Can't truncate table:" + collectionName);
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, statement);
        }

        return isTruncate;
    }

    /**
     * Present date and time
     *
     * @return
     */
    protected final Timestamp getNow() {
        java.util.Date now = new java.util.Date();
        return new java.sql.Timestamp(now.getTime());
    }

    /**
     * Remove a table definition and all data
     *
     * @param collectionName table name
     * @return remove result
     */
    public boolean dropTable(String collectionName) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        boolean isDrop = false;
        try {
            connection = cp.takeConnection();
            preparedStatement = connection.prepareStatement("DROP TABLE ?");
            preparedStatement.setString(1, collectionName);
            preparedStatement.execute();
            isDrop = true;


        } catch (SQLException e) {
            log.debug("Can't drop collection: " + collectionName);
            log.error("Exception: " + e.getMessage());
        } finally {
            cp.closeConnection(connection, preparedStatement);
        }

        return isDrop;
    }

    public DBIterator getDocumentsCollectionIterator(String collectionName) throws SQLException, ClassNotFoundException {
        return new DBIterator(collectionName);
    }
}


