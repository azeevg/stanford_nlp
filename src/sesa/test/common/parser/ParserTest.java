package sesa.test.common.parser;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import sesa.common.builder.BuilderException;
import sesa.common.builder.StanfordParseTreesBuilder;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.io.FileUtility;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Ira on 09.05.2016.
 * Test CKY parser with different input param
 * different text:
 */
public class ParserTest {

    public final Logger log = LogManager.getLogger(ParserTest.class);

    /**
     * Extract documents from db table: w_dbch_computing
     * Text contains in brackets: w_filtered_text
     *
     * @return list of texts
     */
    public List<String> extractDocuments(String tableName, int from, int to) throws SQLException, ClassNotFoundException {
        DocumentsDao documentsDao = new DocumentsDao();
        List<IDBEntity> entities = documentsDao.getDocuments(tableName, from, to);
        List<String> documents = new ArrayList<String>();
        for (IDBEntity entity : entities) {
            Map<String, Object> map = entity.toMap();
            documents.add((String) map.get("w_filtered_text"));
        }
        return documents;
    }

    /**
     * Extract document from db in html format
     * @param tableName table name in database (
     * @param id id row
     * @return
     */
    public String extractDocument(String tableName, String id) throws SQLException, ClassNotFoundException {
        DocumentsDao documentsDao = new DocumentsDao();
        IDBEntity entity = documentsDao.getDocumentById(tableName, id);
        Map<String, Object> map = entity.toMap();
        return (String) map.get("w_filtered_text");
    }

    /**
     * Test for parsing text in html format
     * @throws BuilderException
     */
    @Test
    public void testParseHtmlText() throws BuilderException, SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        String text = extractDocument("w_dbch_computing", "-1496110656");
        builder.parseHtmlDocument(text);
        // builder.buildParseTrees(text);
    }

    @Test
    public void testParseHTMLDocuments() throws BuilderException, SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        for (String text : extractDocuments("w_dbch_computing", 3, 5)) {
            builder.parseHtmlDocument(text);
        }
    }

    @Test
    public void readParseTree() {
        ArrayList<String> list = FileReaderWriter.read("./data/wikiParseTree.txt");
        System.out.println(list.get(0));
    }

    @Test
    public void testParseSimpleText() throws SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        String text = FileUtility.readAllText("./data/test.txt");
        builder.parseText(text);
    }

    /**
     * Each sentence text ( textwithcollocation.txt) contains collocations
     */
    @Test
    public void testParseText() throws SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        String text = FileUtility.readAllText("./data/collocations/textwithcollocation.txt");
        builder.parseText(text);
    }

    /**
     * Parse sentence contains only one collocation/
     */
    @Test
    public void testParseCollocationSentence() throws SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        // List<String> collocations = FileReaderWriter.read("./data/collocations.txt");
        List<String> collocations = FileReaderWriter.read("./data/subsetCollocations.txt");
        int numCollocationInTree = 0;
        for (String collocation : collocations) {
            String sentence = collocation + " .";
            int tmp = builder.buildParseSentence(sentence);
            if (tmp == 0) {
                FileReaderWriter.write("data/collocationNotFound.txt", collocation);
            }
            numCollocationInTree += tmp;
        }

        log.info("Number collocations in parse trees:" + numCollocationInTree);
        log.info("Number sentences:" + collocations.size());
    }


    @Test
    public void testParseDictionaryText() throws SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        String text = FileUtility.readAllText("./data/collocations/text/Z_text.txt");
        builder.parseText(text);
    }

    @Test
    public void testParseListString() throws SQLException, ClassNotFoundException {
        StanfordParseTreesBuilder builder = new StanfordParseTreesBuilder();
        List<String> list = FileReaderWriter.readJSONFromFile("./data/collocations/Misk_text.txt", "dictionary_text");
        builder.parseListString(list);
        // System.out.println(list.size());
        //builder.parseText(text);
    }
}
