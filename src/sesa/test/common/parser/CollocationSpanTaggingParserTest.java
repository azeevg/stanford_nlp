package sesa.test.common.parser;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.egor.SentenceGetter;
import edu.spbstu.egor.wrapper.CollocationSpanTaggingParser;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Parser;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import sesa.common.builder.BuilderException;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.model.processing.document.IDocument;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.semantic.SentenceUtils;
import sesa.tools.cache.CacheUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Ira on 05.05.2016.
 */
public class CollocationSpanTaggingParserTest {
    public final Logger log = LogManager.getLogger(CollocationSpanTaggingParserTest.class);
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
    private String languageModel = "./data/grammar/englishFactored.ser.gz";
    //"./data/grammar/englishFactored.ser.gz"

    @Test
    public void testInitializeParser() throws IOException {
        CollocationDictionary dictionary = null;
        FileManager path = new FileManager("path");
        Parser parser = new Parser(path.getValue("path.languageModel"));
        log.info("Download collocations from file");

        try {
            dictionary = new CollocationDictionary(new String[]{path.getValue("path.computerCollocations")});
        } catch (IOException e) {

        }
        System.out.println(dictionary.size());
        log.info("Initialize parser");
        CollocationSpanTaggingParser collocationParser = new CollocationSpanTaggingParser(parser.getGrammar(), dictionary);
    }

    @Test
    public void testGetGrammaticalRelations() throws IOException, BuilderException, SQLException, ClassNotFoundException {
        StanfordDataModule dataModule = new BasicDataModule();
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        StanfordRelationsBuilder builder = new StanfordRelationsBuilder(inputStream,dataModule);
        builder.setIsModCKY(true);
        SentenceGetter sentenceGetter = new SentenceGetter("data/test.txt");
        ArrayList<ArrayList<SemanticRelation>> sesaGs = builder.getGrammaticalTextStructure(sentenceGetter);
        for(ArrayList<SemanticRelation> semanticRelations: sesaGs){
            System.out.println("##-----");
            for(SemanticRelation sr: semanticRelations){
                System.out.println(sr);
            }
        }
        System.out.println();
        // builder.setIsModCKY(false);
    }

    @Test
    public void testParser() throws IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        Tree tree = parser.parse(SentenceUtils.readSentenceFromFile("data/test.txt"), true);
    }

    // Use CKY algorithm
    @Test
    public void parseWikiText() throws IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel); //parserModel

        String text = FileUtility.readAllText("./data/ada_cache/db_text_2.txt");
        StanfordDataModule dataModule = new BasicDataModule();
        DocumentBuilder builder = new StanfordRelationsBuilder(inputStream,dataModule);
        builder.setIsModCKY(true);

        try {
            builder.setData(text, "id");
            IDocument document = builder.buildDocument();
            CacheUtils.generatePaintedDocument(document, "ada_small");
        } catch (BuilderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
