package sesa.common.utility;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.dictionary.LinkStructureDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.dao.dictionary.TermLinksDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Ira on 07.06.2016.
 */
public class UtilityDBMethods {
    public static final Logger log = LogManager.getLogger(UtilityDBMethods.class);

    public static List<Integer> getSubsetTermsNotContainLink(List<Integer> uniqueTerms) throws SQLException, ClassNotFoundException {
        TermLinksDao linksDao = new TermLinksDao();
        Set<Integer> idTermsWithLinks = linksDao.getTermsContainLink();
        List<Integer> idTermsWithoutLinks = new ArrayList<Integer>();

        for (Integer idTerm : uniqueTerms) {
            if (idTermsWithLinks.contains(idTerm)) {
                continue;
            } else {
                idTermsWithoutLinks.add(idTerm);
            }
        }

        return idTermsWithoutLinks;
    }

    /**
     * Input list terms  - it's terms which do not contain link like: See term_
     *
     * @param termsWithoutLinks
     * @return
     */
    public static List<Integer> getSubsetTermsNotContainsAutoReference(List<Integer> termsWithoutLinks) throws SQLException, ClassNotFoundException {
        List<Integer> idTermsWithoutRef = new ArrayList<Integer>();
        LinkStructureDao dao = new LinkStructureDao();
        for (int id : termsWithoutLinks) {
            if (dao.getListSemanticGroupDtoByTermID(id, "auto_comp_reference").getTermLink().size() == 0) {
                idTermsWithoutRef.add(id);
            }
        }
        return idTermsWithoutRef;
    }


    public static void addLinkPairToDataBase(String termName, String linkName, String collectionName) throws SQLException, ClassNotFoundException {
        TermDao termDao = new TermDao();
        TermDefinitionDao termDefinitionDao = new TermDefinitionDao();
        LinkStructureDao dao = new LinkStructureDao();
        int idTerm = termDao.getTermByName(termName).getId();
        int idTermLink = termDao.getTermByName(linkName).getId();
        int idDef = termDefinitionDao.getTermDefinitionByTermID(idTerm).get(0).getIdDefinition();
        log.info("idTerm:idDef:idLink --" + idTerm + ":" + idDef + ":" + idTermLink);
        dao.addToLinkStructureOneRow(idTerm, idDef, idTermLink, collectionName);

    }


}
