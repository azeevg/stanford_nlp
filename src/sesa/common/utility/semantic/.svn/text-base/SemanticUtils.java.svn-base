package sesa.common.utility.vector;

import org.apache.commons.lang3.StringUtils;
import sesa.common.model.processing.SemanticGroup;
import sesa.common.model.processing.SemanticRelation;
import sesa.common.model.processing.SentenceToken;
import sesa.common.model.processing.SentenceTree;
import sesa.common.model.processing.support.GroupMatchType;
import sesa.common.model.processing.support.RelationMatchType;
import sesa.common.model.processing.support.RelationMistake;
import sesa.common.model.processing.support.SentenceTreeCollection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SemanticUtils {

  //standard - manual parse
  //document - stanford parse
  public static void findMistakes(List<SentenceTree> standard, List<SentenceTree> document) throws Exception {
    if (standard.size() != document.size()) {
      throw new Exception("Sentences size mismatch!");
    }

    SentenceTreeCollection standardCollection = new SentenceTreeCollection(standard);

    for (int i = 0; i < standard.size(); i++) {
      SentenceTree sSentence = standard.get(i);
      SentenceTree dSentence = document.get(i);

      List<SentenceToken> sTokens = sSentence.getTokens();
      List<SentenceToken> dTokens = dSentence.getTokens();

      if (sTokens.size() != dTokens.size()) {
        throw new Exception("Tokens size mismatch!");
      }

      for (int j = 0; j < sTokens.size(); j++) {
        SentenceToken sToken = sTokens.get(j);
        SentenceToken dToken = dTokens.get(j);

        if (!sToken.matchValue(dToken)) {
          throw new Exception("Tokens mismatch");
        }

        if (!dToken.hasRelations())
          continue;

        List<SemanticRelation> sRelations = sToken.getRelations();
        List<SemanticRelation> dRelations = dToken.getRelations();

        if (!dToken.hasRelations())
          continue;

        SemanticRelation dRelation = dRelations.get(0);
        SemanticGroup dGroup = dRelation.getToken().getGroup();
        RelationMatchType relationMatchType = matchRelations(sRelations, dRelations);
        GroupMatchType groupMatchType = sToken.getGroup().matchGroup(dToken.getGroup());


        if (relationMatchType != RelationMatchType.Matched || groupMatchType != GroupMatchType.Matched) {
          RelationMistake mistake = new RelationMistake();
          if (relationMatchType == RelationMatchType.NotMatched) {
            mistake.setType(RelationMistake.RelationMistakeType.NotMatched);
            if (groupMatchType == GroupMatchType.NotMatched)
              mistake.addSubType(RelationMistake.RelationMistakeSubType.GroupNotMatched);

          } else if (relationMatchType == RelationMatchType.PartiallyMatched) {
            if (groupMatchType == GroupMatchType.NotMatched) {
              mistake.setType(RelationMistake.RelationMistakeType.NotMatched);
              mistake.addSubType(RelationMistake.RelationMistakeSubType.GroupNotMatched);
            } else
              mistake.setType(RelationMistake.RelationMistakeType.PartiallyMatched);
          }

          if (dToken.isPreviousTokenDelimiter())
            mistake.addSubType(RelationMistake.RelationMistakeSubType.DelimiterNeighbor);
          if (dSentence.containsServiceTokens())
            mistake.addSubType(RelationMistake.RelationMistakeSubType.ServiceTokens);
          if (dSentence.getSize() > standardCollection.getAvgSentenceSize())
            mistake.addSubType(RelationMistake.RelationMistakeSubType.SentenceLength, dSentence.getSize() + " > " +  standardCollection.getAvgSentenceSize());

          List<SemanticRelation> subjMistakeRelations = dGroup.findMistakeRelationsByRole("subj");
          if (subjMistakeRelations.size() > 0)
            mistake.addSubType(RelationMistake.RelationMistakeSubType.SubjNeighborMistake, StringUtils.join(subjMistakeRelations, "</br>"));

          List<SemanticRelation> objMistakeRelations = dGroup.findMistakeRelationsByRole("obj");
          if (objMistakeRelations.size() > 0)
            mistake.addSubType(RelationMistake.RelationMistakeSubType.ObjNeighborMistake, StringUtils.join(objMistakeRelations, "</br>"));

          if (mistake.getSubTypes().size() == 0)
            mistake.addSubType(RelationMistake.RelationMistakeSubType.Undefined);

          dRelation.setMistake(mistake);
        }
      }
    }
  }

  public static Map<String, Map<String, List<SemanticRelation>>> createRelationGrouping(List<SentenceTree> document) {
    Map<String, Map<String, List<SemanticRelation>>> roleGrouping = new HashMap<String, Map<String, List<SemanticRelation>>>();
    for (SentenceTree sentence : document) {
      for (SemanticGroup group : sentence.getSemanticGroups()) {
        for (SentenceToken token : group.getTokens()) {
          for (SemanticRelation relation : token.getRelations()) {
            String roleName = relation.getRole();
            String relationName = relation.getShortName();
            if (!roleGrouping.containsKey(roleName)) {
              Map<String, List<SemanticRelation>> relationGrouping = new HashMap<String, List<SemanticRelation>>();
              List<SemanticRelation> relations = new LinkedList<SemanticRelation>();
              relations.add(relation);
              relationGrouping.put(relationName, relations);
              roleGrouping.put(roleName, relationGrouping);
            } else {
              Map<String, List<SemanticRelation>> relationGrouping = roleGrouping.get(roleName);
              if (!relationGrouping.containsKey(relationName)) {
                List<SemanticRelation> relations = new LinkedList<SemanticRelation>();
                relations.add(relation);
                relationGrouping.put(relationName, relations);
              }else{
                relationGrouping.get(relationName).add(relation);
              }
            }
          }
        }
      }
    }
    return roleGrouping;
  }

  public static RelationMatchType matchRelations(List<SemanticRelation> srs1, List<SemanticRelation> srs2) {
    int s1 = srs1.size();
    int s2 = srs2.size();
    if (s1 == 0)
      return RelationMatchType.NotMatched;
    if (s1 == 1 && s2 == 1 && matchRelations(srs1.get(0), srs2.get(0)))
      return RelationMatchType.Matched;
    SemanticRelation stanfordRelation = srs2.get(0);
    for (SemanticRelation relation : srs1) {
      if (matchRelations(relation, stanfordRelation))
        return RelationMatchType.PartiallyMatched;
    }
    return RelationMatchType.NotMatched;
  }

  public static boolean matchRelations(SemanticRelation r1, SemanticRelation r2) {
    return r1.getShortName().toLowerCase().trim().equals(r2.getShortName().toLowerCase().trim());
  }
}
