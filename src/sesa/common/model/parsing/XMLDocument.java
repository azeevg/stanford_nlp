package sesa.common.model.parsing;

import sesa.common.database.entity.IDBEntity;

import java.util.Map;

/**
 * Parsing document in XML format
 */
public class XMLDocument implements IDBEntity {

    private Map<String, Object> data; // массив пар ключ-значение

    public XMLDocument(Map<String, Object> data) {
        this.data = data;
    }

    /**
     * Get value by key: "title"
     * @return
     */
    public String getID() {
        try{
        return Integer.toString(data.get("title").hashCode());
        } catch (Exception e){
           return "no title";
        }
    }

    //TODO: change name getMap()
    public Map<String, Object> toMap() {
        return data;
    }


    public Map<String, Object> getData() {
        return data;
    }


    @Override
    protected void finalize() throws Throwable {
        if (data != null)
            data.clear();

        super.finalize();
    }


}
