package grammarscope.parser;

import edu.stanford.nlp.ling.Word;

import java.util.ArrayList;
import java.util.List;

public class Sentence extends ArrayList<Word> {
    private static final long serialVersionUID = 1L;

    public Sentence(final List<Word> thisProtoSentence) {
        super.addAll(thisProtoSentence);
    }

    @Override
    public String toString() {
        final StringBuffer thisBuffer = new StringBuffer();
        for (final Word w : this) {
            thisBuffer.append(w);
            thisBuffer.append(' ');
        }
        return thisBuffer.toString();
    }
}
