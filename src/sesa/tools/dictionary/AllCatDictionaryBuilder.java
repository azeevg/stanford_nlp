package sesa.tools.dictionary;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.builder.BuilderException;
import sesa.common.configuration.Categories;
import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.parsing.XMLDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.vector.DocumentUtils;
import sesa.common.builder.DictionaryDocumentBuilder;

import java.sql.SQLException;
import java.util.*;


/**
 * Build dictionary for all categories
 * �������� ��� ������� �� ���������� ����� ���������� �������
 *
 */
public class AllCatDictionaryBuilder extends Application {
    public final Logger log = LogManager.getLogger(AllCatDictionaryBuilder.class);
    private String[] categoryNames = Categories.ALL_CATEGORIES;
    private DocumentsDao documentsDao = new DocumentsDao();

    public AllCatDictionaryBuilder() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        Set<String> documentIdSet = new HashSet<String>();
        Map<String, Double> globalTokenMap = new HashMap<String, Double>();

        for (String categoryName : categoryNames) {
            String categoryCollection = Categories.buildCategoryHtmlCollectionName(categoryName);
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryCollection);
            //dbController.getDocumentsCollectionIterator(categoryCollection);
            iterator = new DBReportingIterator(iterator, categoryCollection + " idf obtaining");

            while (iterator.hasNext()) {
                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String id = doc.getID();

                if (documentIdSet.contains(id)) {
                    continue;
                } else
                    documentIdSet.add(id);

                String text = (String) map.get("w_filtered_text");
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();

                IDocument document = null;
                try {
                    document = documentBuilder.buildDocument(id, text, null);
                } catch (BuilderException e) {
                    log.error(e.getMessage());
                }

                List<SentenceTree> sentences = document.getSentences();
                Set<String> documentUniqueTokens = DocumentUtils.getUniqueTokens(sentences);

                for (String token : documentUniqueTokens) {
                    if (globalTokenMap.containsKey(token)) {
                        double val = globalTokenMap.get(token);
                        globalTokenMap.put(token, val + 1.0);
                    } else
                        globalTokenMap.put(token, 1.0);
                }
            }
        }

        Map<String, Object> entity = new HashMap<String, Object>();
        entity.put("dictionary", globalTokenMap);

        documentsDao.truncateOrCreateDocumentCollection(Configuration.DB_COLLECTION_CATS_DICTIONARY);
        // dbController.truncateOrCreate(Configuration.DB_COLLECTION_CATS_DICTIONARY);
        documentsDao.addToDocumentsCollection(Configuration.DB_COLLECTION_CATS_DICTIONARY, new XMLDocument(entity));
        //dbController.insertToDocumentsCollection(Configuration.DB_COLLECTION_CATS_DICTIONARY, new XMLDocument(entity));

        //saveDictionary(entity,Configuration.DB_COLLECTION_CATS_DICTIONARY);
        return true;
    }

    private Map<String, Double> prepareDictionary() throws SQLException, ClassNotFoundException {
        Set<String> documentIdSet = new HashSet<String>();
        Dictionary dictionary = new Dictionary();
        Map<String, Double> globalTokenMap = new HashMap<String, Double>();
        for (String categoryName : categoryNames) {
            String categoryCollection = Categories.buildCategoryHtmlCollectionName(categoryName);
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryCollection);
            iterator = new DBReportingIterator(iterator, categoryCollection + " idf obtaining");

            while (iterator.hasNext()) {
                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String id = doc.getID();
                if (documentIdSet.contains(id)) {
                    continue;
                } else {
                    documentIdSet.add(id);
                }
                String text = (String) map.get("w_filtered_text");
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
                // return DictionaryDocumentBuilder.buildDocument(id, text, categoryDictionary);
                IDocument document = null;
                try {
                    document = documentBuilder.buildDocument(id, text, null);
                } catch (BuilderException e) {
                    log.error(e.getMessage());
                }
                List<SentenceTree> sentences = document.getSentences();
                Set<String> documentUniqueTokens = DocumentUtils.getUniqueTokens(sentences);

                //From complete set of tokens choose unique and calculate frequency of occurrence for every one
              //  DictionaryTerms dictionary = new DictionaryTerms();
                dictionary.merge(documentUniqueTokens);
               /* for (String token : documentUniqueTokens) {
                    if (globalTokenMap.containsKey(token)) {
                        double val = globalTokenMap.get(token);
                        globalTokenMap.put(token, val + 1.0);
                    } else
                        globalTokenMap.put(token, 1.0);
                }*/


            }
        }

        return globalTokenMap;
    }

    private void saveDictionary(Map<String, Object> entity, String collectionName) {
        if (documentsDao.isTableExists(collectionName)) {
            documentsDao.truncateOrCreateDocumentCollection(collectionName);
        } else {
            documentsDao.createDocumentCollectionTable(collectionName);
        }

        documentsDao.addToDocumentsCollection(collectionName, new XMLDocument(entity));

    }
}
