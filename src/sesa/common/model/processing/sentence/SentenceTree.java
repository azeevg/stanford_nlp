package sesa.common.model.processing.sentence;

import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a information about sentence after parse and semantic analysis
 */
public class SentenceTree {

    private boolean isTitle;
    private int level;
    private String sentence;
    private SentenceTriplet triplet;
    private List<SemanticGroup> semanticGroups;
    private List<SentenceToken> tokens;

    public SentenceTree() {
        this.tokens = new LinkedList<SentenceToken>();
        this.semanticGroups = new LinkedList<SemanticGroup>();
    }


    public List<SemanticGroup> getSemanticGroups() {
        return semanticGroups;
    }

    public ArrayList<SemanticRelation> getAllRelations() {
        ArrayList<SemanticRelation> relations = new ArrayList<SemanticRelation>();
        for (SemanticGroup sg : semanticGroups) {
            for (SemanticRelation sr : sg.getRelations()) {
                relations.add(sr);
            }
        }

        return relations;
    }


    public void addSemanticGroup(SemanticGroup semanticGroup) {
        semanticGroups.add(semanticGroup);
    }

    public void addToken(SentenceToken token) {
        tokens.add(token);
    }

    public SentenceTriplet getTriplet() {
        return triplet;
    }

    public void setTriplet(SentenceTriplet triplet) {
        this.triplet = triplet;
    }

    public boolean isTitle() {
        return isTitle;
    }

    public void setIsTitle(boolean title) {
        isTitle = title;
    }

    public void printSemanticGroups() {
        for (SemanticGroup group : semanticGroups) {
            //LoggingUtility.info(group.longPrint());
        }
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    public boolean containsServiceTokens() {
        for (SentenceToken token : tokens) {
            if (token.isServiceToken())
                return true;
        }
        return false;
    }

    public int getSize() {
        return tokens.size();
    }

    public String first(int value) {
        if (value > sentence.length())
            return sentence;
        else
            return sentence.substring(0, value) + "...";
    }


    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<SentenceToken> getTokens() {
        return tokens;
    }

    public void setTokens(List<SentenceToken> tokens) {
        this.tokens = tokens;
    }

    public void setSemanticGroups(List<SemanticGroup> semanticGroups) {
        this.semanticGroups = semanticGroups;
    }

}
