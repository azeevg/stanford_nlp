package grammarscope.parser;

import java.util.Collection;
import java.util.List;

import edu.stanford.nlp.trees.CoordinationTransformer;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalRelation.Language;
import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.Filter;
import grammarscope.exporter.Exporter;

/**
 * Glue class between MutableGrammaticalRelations and GrammaticalStructure
 * 
 * @author Bernard Bou
 */
public class GrammaticalStructure extends edu.stanford.nlp.trees.GrammaticalStructure
{
	private static final long serialVersionUID = 1L;

	static public class English extends GrammaticalStructure
	{
		private static final long serialVersionUID = 1L;

		public English(final Tree t, final Filter<String> puncFilter, final HeadFinder hf)
		{
			super(t, puncFilter, hf, Language.English);
		}

		public English(final Tree t, final Filter<String> puncFilter)
		{
			super(t, puncFilter, Language.English);
		}

		public English(final Tree t, final HeadFinder hf)
		{
			super(t, hf, Language.English);
		}

		public English(final Tree t)
		{
			super(t, Language.English);
		}
	}

	static public class Chinese extends GrammaticalStructure
	{
		private static final long serialVersionUID = 1L;

		public Chinese(final Tree t, final Filter<String> puncFilter, final HeadFinder hf)
		{
			super(t, puncFilter, hf, Language.Chinese);
		}

		public Chinese(final Tree t, final Filter<String> puncFilter)
		{
			super(t, puncFilter, Language.Chinese);
		}

		public Chinese(final Tree t, final HeadFinder hf)
		{
			super(t, hf, Language.Chinese);
		}

		public Chinese(final Tree t)
		{
			super(t, Language.Chinese);
		}
	}

	static public class Any extends GrammaticalStructure
	{
		private static final long serialVersionUID = 1L;

		public Any(final Tree t, final Filter<String> puncFilter, final HeadFinder hf)
		{
			super(t, puncFilter, hf, Language.Any);
		}

		public Any(final Tree t, final Filter<String> puncFilter)
		{
			super(t, puncFilter, Language.Any);
		}

		public Any(final Tree t, final HeadFinder hf)
		{
			super(t, hf, Language.Any);
		}

		public Any(final Tree t)
		{
			super(t, Language.Any);
		}
	}

	/**
	 * Construct a new <code>GrammaticalStructure</code> from an existing parse tree. The new <code>GrammaticalStructure</code> has the same tree structure and
	 * label values as the given tree (but no shared storage). As part of construction, the parse tree is analyzed using definitions from to populate the new
	 * <code>GrammaticalStructure</code> with as many labeled grammatical relations as it can.
	 * 
	 * @param t
	 *            Tree to process
	 * @param thisLanguage
	 *            language
	 */
	protected GrammaticalStructure(final Tree t, final Language thisLanguage)
	{
		this(t, GrammaticalStructure.makePunctFilt(thisLanguage), thisLanguage);
	}

	protected GrammaticalStructure(final Tree t, final Filter<String> puncFilter, final Language thisLanguage)
	{
		this(t, puncFilter, new SemanticHeadFinder(true), thisLanguage);
	}

	protected GrammaticalStructure(final Tree t, final HeadFinder hf, final Language thisLanguage)
	{
		this(t, null, hf, thisLanguage);
	}

	protected GrammaticalStructure(final Tree t, final Filter<String> puncFilter, final HeadFinder hf, final Language thisLanguage)
	{
		super(new CoordinationTransformer().transformTree(t), Factories.makeDefaultGrammaticalRelationCollection(thisLanguage), hf, puncFilter);
		System.out.println("make grammatical relations from mutable model into " + thisLanguage);
	}

	@Override
	protected void collapseDependencies(final List<TypedDependency> list, final boolean CCprocess)
	{
		super.collapseDependencies(list, CCprocess);
		System.out.println("collapseDependencies");
	}

	@Override
	protected void collapseDependenciesTree(final List<TypedDependency> list)
	{
		super.collapseDependenciesTree(list);
		System.out.println("collapseDependenciesTree");
	}

	@Override
	protected void correctDependencies(final Collection<TypedDependency> list)
	{
		super.correctDependencies(list);
		System.out.println("correctDependencies");
	}

	@SuppressWarnings("unused")
	static private Collection<GrammaticalRelation> exportGrammaticalRelations(final Language thisLanguage)
	{
		final RelationModel thisModel = DefaultMutableGrammaticalRelations.makeDefaultModel();
		return Exporter.export(thisModel, thisLanguage);
	}

	static private Filter<String> makePunctFilt(final Language thisLanguage)
	{
		final TreebankLanguagePack thisLanguagePack = Factories.makeLanguagePack(thisLanguage);
		return thisLanguagePack.punctuationWordRejectFilter();
	}
}
