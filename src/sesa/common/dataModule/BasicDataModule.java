package sesa.common.dataModule;

import edu.spbstu.appmath.collocmatch.Collocation;
import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.stanford.nlp.trees.Tree;
import grammarscope.browser.RelationFilter;
import grammarscope.parser.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.collocation.CollocationUtility;
import sesa.common.utility.json.JSONUtility;
import sesa.common.utility.semantic.SentenceUtils;

import java.io.IOException;
import java.util.*;

/**
 * BasicDataModule() makes trees of parsed sentences on-the-fly.
 * It does not need any cached trees in DB tables.
 */
public class BasicDataModule implements StanfordDataModule {
    public final Logger log = LogManager.getLogger(BasicDataModule.class);
    protected Parser parser;
    protected Analyzer analyzer; //Class that bundles sentence analysis results
    protected Document document;
    protected RelationFilter relationFilter;
    protected Map<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> relationMap;
    protected Map<Integer, Tree> treeMap;
    protected List<Sentence> sentenceList;
    protected Set<String> collocationForParseTree;

    private int collocationNumberInTree = 0;
    private int collocationNumberInSentence = 0;

    protected FileManager file = new FileManager("path");


    public void init(Parser parser, Analyzer analyzer, Document document, RelationFilter relationFilter) {
        this.parser = parser;
        this.analyzer = analyzer;
        this.relationFilter = relationFilter;
        this.document = document;
        initCollocationForParseTree(parser.getDictionary());
    }

    public int getCollocationNumberInSentence() {
        return collocationNumberInSentence;
    }

    public int getCollocationNumberInParseTree() {
        return collocationNumberInTree;
    }

    private void initCollocationForParseTree(CollocationDictionary dictionary) {
        collocationForParseTree = new HashSet<String>();
        for (Collocation collocation : dictionary) {
            StringBuilder sb = new StringBuilder();
            for (String w : collocation.words()) {
                sb.append(w);
            }
            collocationForParseTree.add(sb.toString());
        }
    }

    public void postProcess(String title) {
        relationMap = null;
        treeMap = null;
    }

    /**
     * Get tree by sentence
     *
     * @param sentence
     * @return Tree by hashCode of sentence
     */
    public Tree getTree(Sentence sentence) {
        return treeMap.get(sentence.toString().hashCode());
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    /**
     * The method returns a shorter list of grammatical relations for sentence
     * Because our algorithms require a shortened set of grammatical relations
     * In that method we are not find again grammatical relations for sentence!
     *
     * @param sentence
     * @return a list of grammatical relations
     * @throws IOException
     */
    public List<SemanticRelation> getFilteredRelations(Sentence sentence) throws IOException {
        List<SemanticRelation> resultRelations = new LinkedList<SemanticRelation>();

        if (relationMap == null) {
            return null;
        }
        //   System.out.println("sentence.toString().hashCode(): " + sentence.toString().hashCode());
        Map<MutableGrammaticalRelation, List<ParsedRelation>> sentenceRelations = relationMap.get(sentence.toString().hashCode());

        if (sentenceRelations == null)
            return null;

        Collection<MutableGrammaticalRelation> allRelations = sentenceRelations.keySet();
        List<MutableGrammaticalRelation> filteredRelations = relationFilter.filterRelations(allRelations);
        for (final MutableGrammaticalRelation thisRelation : filteredRelations) {
            final List<ParsedRelation> parsedRelations = sentenceRelations.get(thisRelation);
            for (final ParsedRelation parsedRelation : parsedRelations) {
                SemanticRelation relation = new SemanticRelation(parsedRelation.theRelation.getLongName());
                relation.setTokenBaseValue(parsedRelation.theHeadBase);
                relation.setShortName(parsedRelation.theRelation.getShortName());
                relation.setTreeHash(parsedRelation.theHead.hashCode());
                resultRelations.add(relation);
            }
        }
        if (resultRelations == null) {
            log.error("Filtered relations = null");
        }
        return resultRelations;
    }


    /**
     * Read tree cache to treeMap and write to database
     * Realization of methods (readTreeCache and writeTreeCache) in different classes,
     * therefore, there not will be if  we load trees to the treeMap, and it is loaded back to the database
     *
     * @param title
     * @param sentences
     * @param isModCKY
     * @throws Exception
     */
    public void updateTreeCache(String title, List<Sentence> sentences, boolean isModCKY) throws Exception {
        readTreeCache(title, sentences, isModCKY);
        writeTreeCache(title, sentences);
    }

    //TODO: change name
    public void updateRelationCache(String title, List<Sentence> sentences) {
        readRelationCache(title, sentences);
        writeRelationCache(title, sentences);
    }


    protected void writeTreeCache(String title, List<Sentence> sentences) {

    }

    @Deprecated
    protected void writeRelationCache(String title, List<Sentence> sentences) {

    }

    /**
     * Parse each sentence separately and the better parse tree put to treeMap
     * The bestParse have max probability
     * TODO: rename this method (buildTreeCache)
     *
     * @param title
     * @param sentences list of sentences
     * @throws Exception
     */
    protected void readTreeCache(String title, List<Sentence> sentences, boolean isModCKY) throws Exception {
        buildTreeCache(title, sentences, isModCKY);
    }

    /**
     * Parse each sentence separately and the better parse tree put to treeMap
     * The bestParse have max probability
     *
     * @param title
     * @param sentences list of sentences
     * @param isModCKY  key
     *                  true - if you want to use modified CKY algorithm for parsing sentence
     *                  false - if you want to use stanford CKY algorithm for parsing sentence
     * @throws Exception
     */
    public void buildTreeCache(String title, List<Sentence> sentences, boolean isModCKY) throws Exception {
        treeMap = new HashMap<Integer, Tree>();
        sentenceList = new ArrayList<Sentence>();
        for (Sentence sentence : sentences) {

            Tree tree = parser.parse(sentence, isModCKY);   // tree is better parse tree
            if (isModCKY) {
                Sentence convertedSentence = SentenceUtils.convertSentence(sentence, parser.getCollocationsIndex());
                sentenceList.add(convertedSentence);
                treeMap.put(convertedSentence.toString().hashCode(), tree);
            } else {
                sentenceList.add(sentence);
                treeMap.put(sentence.toString().hashCode(), tree);
            }
            writeCollocationResult(tree, sentence);
        }

        if (isModCKY) {
            document.parse(parser, sentenceList);
        } else {
            document.parse(parser);
        }

    }

    /**
     * Method write result to file after build parse tree
     * @param tree
     * @param sentence
     */
    private void writeCollocationResult(Tree tree, Sentence sentence) {
        List<String> collocationInTree = CollocationUtility.getCollocationInTree(collocationForParseTree, tree);
        List<String> collocationInSentence = CollocationUtility.getCollocationInSentence(parser.getDictionary(), sentence.toString());
        collocationNumberInTree += collocationInTree.size();
        collocationNumberInSentence += collocationInSentence.size();
        FileReaderWriter.write(file.getValue("path.parseTree"), tree.toString());
        FileReaderWriter.write(file.getValue("path.collocationInParseTree"), JSONUtility.listToJsonString(collocationInTree, "collocationInTree"));
        FileReaderWriter.write(file.getValue("path.collocationInSentence"), JSONUtility.listToJsonString(collocationInSentence, "collocationInSentence"));
    }


    /**
     * Analyze document of sentence for getting grammatical relations
     * Use after build parse trees for sentences
     *
     * @param title
     * @param sentences the list of sentences
     */
    protected void readRelationCache(String title, List<Sentence> sentences) {
        relationMap = new HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>>();
        document.analyze_init(analyzer);
        for (Sentence sentence : sentences) {
            // log.info("###ParsedTrees: " + document.theParsedTrees);
            //System.out.println("hashCode sentence:" + sentence.toString().hashCode());
            //  sentence = SentenceUtils.convertSentence(sentence,parser.getCollocationsIndex());
            //System.out.println("hashCode convert sentence:" + sentence.toString().hashCode());

            Map<MutableGrammaticalRelation, List<ParsedRelation>> relationSentenceMap = analyzer.analyze(document, sentence);
            document.theSentenceToParsedRelationsMap.put(sentence, relationSentenceMap);
            relationMap.put(sentence.toString().hashCode(), relationSentenceMap);

        }
        if (relationMap.size() == 0) {
            log.error("Relation map = null");
        } else {
            log.info("relationMap.size:" + relationMap.size());
        }

    }
}
