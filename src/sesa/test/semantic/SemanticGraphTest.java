package sesa.test.semantic;

import org.junit.Test;
import sesa.common.database.dao.dictionary.SemanticStructureDao;
import sesa.tools.semantic.semanticTree.SemanticGraphBuilder;

import java.sql.SQLException;

/**
 * Created by Ira on 02.06.2016.
 */
public class SemanticGraphTest {

    /**
     * Need connect to db, because table contains information for graph
     */
    @Test
    public void testShowSemanticGraph() throws SQLException, ClassNotFoundException {
        int id = 22260;
        SemanticGraphBuilder semanticGraphBuilder = new SemanticGraphBuilder();

        semanticGraphBuilder.buildAndPrintSemanticGraphForTerm("function", "data/graph_edges.txt", "data/graph_vertex.txt");
    }

    @Test
    public void testCalcNodes() throws SQLException, ClassNotFoundException {
        SemanticStructureDao dao = new SemanticStructureDao();
        System.out.println(dao.getNumberNode());
    }


}
