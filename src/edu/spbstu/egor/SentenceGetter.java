package edu.spbstu.egor;

import edu.spbstu.appmath.collocmatch.Sentence;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.lexparser.Lexicon;
import edu.stanford.nlp.process.DocumentPreprocessor;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Egor Gorbunov on 29.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class SentenceGetter implements Iterable<List<? extends HasWord>> {

    public static List<? extends HasWord> convert(Sentence sentence) {
        List<HasWord> res = new ArrayList<HasWord>(sentence.words().length + 1);
        for (String w : sentence.words()) {
            res.add(new Word(w));
        }
        res.add(new Word(Lexicon.BOUNDARY));
        return res;
    }

    private final ArrayList<List<? extends HasWord>> sentences;

    public SentenceGetter(String filename) {
        Reader rd = null;
        try {
            rd = new BufferedReader(new FileReader(new File(filename)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        DocumentPreprocessor dp = new DocumentPreprocessor(rd);

        sentences = new ArrayList<List<? extends HasWord>>();
        for (List<HasWord> s : dp) {
            s.add(new Word(Lexicon.BOUNDARY));
            sentences.add(s);
        }
    }

    public List<List<? extends HasWord>> getSentences() {
        return sentences;
    }

    public List<? extends HasWord> getSentence(int i) {
        return sentences.get(i);
    }

    public Iterator<List<? extends HasWord>> iterator() {
        return sentences.iterator();
    }

    public static String sentenceToString(List<? extends HasWord> s) {
        String str = "";
        for (HasWord w : s) {
            if (!w.word().equals(Lexicon.BOUNDARY))
                str += w + " ";
            else
                break;
        }
        return str;
    }

    public String getText() {
        String text = "";
        for (List<? extends HasWord> s : sentences) {
            text += sentenceToString(s);
            text += '\n';
        }
        return text;
    }
}
