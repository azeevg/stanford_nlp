package sesa.common.configuration;

/**
 * List of categories
 */
public class Categories {

    public final static String ALGEBRA_CATEGORY = "algebra";
    public final static String COMPUTING_CATEGORY = "computing";
    public final static String HISTORY_CATEGORY = "history";
    public final static String LITERATURE_CATEGORY = "literature";
    public final static String BIOLOGY_CATEGORY = "biology";
    public final static String CHEMISTRY_CATEGORY = "chemistry";
    public final static String PHYSICS_CATEGORY = "physics";


    public final static String[] ALL_CATEGORIES = new String[]{ALGEBRA_CATEGORY, COMPUTING_CATEGORY, HISTORY_CATEGORY, LITERATURE_CATEGORY, BIOLOGY_CATEGORY, CHEMISTRY_CATEGORY, PHYSICS_CATEGORY};
    public final static String[] TEST_CATEGORIES = new String[]{ALGEBRA_CATEGORY, COMPUTING_CATEGORY};
    public static String buildCategoryCollectionName(String categoryName) {
        return Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI + "_" + categoryName;
    }

    public static String buildCategoryHtmlCollectionName(String categoryName) {
        return Configuration.DB_COLLECTION_BY_CAT_HTML_WIKI + "_" + categoryName;
    }
}
