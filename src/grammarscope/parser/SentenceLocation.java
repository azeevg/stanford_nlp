package grammarscope.parser;

import java.util.Map;

import edu.stanford.nlp.ling.Word;

/**
 * Sentence location
 * 
 * @author Bernard Bou
 */
public class SentenceLocation extends Location
{
	/**
	 * Word locations
	 */
	public Map<Word, Location> theWordMap;
}