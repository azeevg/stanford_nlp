package sesa.common.builder;

import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.document.Document;
import sesa.common.model.processing.sentence.SentenceForest;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.dictionary.DictionaryUtils;

import java.util.LinkedList;
import java.util.List;

public class BasicBuilder extends DocumentBuilder {

    public BasicBuilder(String text, String id) throws BuilderException {
        super(text, id);
    }

    public BasicBuilder() {
    }

    public IDocument buildDocument() {
        return new Document(id, title, model);
    }

    @Override
    protected List<SentenceForest> buildParagraphSubTree(String text, boolean isTitle, int level) throws BuilderException {
        List<SentenceForest> paragraph = new LinkedList<SentenceForest>();
        List<List<String>> validTokens = DictionaryUtils.getValidLowerTokens(text);

        for (List<String> sentenceTokens : validTokens) {
            SentenceForest forest = new SentenceForest();
            SentenceTree tree = new SentenceTree();
            for (String token : sentenceTokens) {
                tree.addToken(new SentenceToken(token, null));
            }
            forest.addTree(tree);
            paragraph.add(forest);
        }
        return paragraph;
    }




}
