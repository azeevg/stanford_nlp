package sesa.tools.math;

import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.strings.StringUtility;

import java.util.*;

/**
 * ����� ��� ������ � ��������
 * (������ �����)
 */
public class Vector {
    private Map<String, Double> components = new HashMap<String, Double>();
    private String id;

    public Vector() {
    }

    public Vector(String id) {
        this.id = id;
    }

    private Vector(Vector v) {
        this.components.putAll(v.components);
    }

    public String getId() {
        return id;
    }

    public int size() {
        return components.size();
    }

    /**
     * �������� ������� � components
     *
     * @param name  ����
     * @param value ��������
     */
    public void addComponent(String name, Double value) {
        if (name.isEmpty())
            return;

        String processedName = StringUtility.getHash(name);
        // ���� ������� � ����� ������ ��� ����������, �� ����������� �������� �� value
        if (components.containsKey(processedName))
            components.put(processedName, components.get(processedName) + value);
        else
            components.put(processedName, value);
    }

    /**
     * ��������� ������� � this.components V.components
     * ?? id �� �����??
     *
     * @param v
     */
    public void add(Vector v) {
        Set<String> tokens = v.components.keySet();
        for (String token : tokens)
            if (components.containsKey(token))
                components.put(token, components.get(token) + v.components.get(token));
            else
                components.put(token, v.components.get(token));
    }

    /**
     * ��������� ������� �� �����
     *
     * @param v
     */
    public void multiply(double v) {
        Set<String> tokens = components.keySet();
        for (String token : tokens)
            components.put(token, components.get(token) * v);
    }

    public double dot(Vector v) {
        double dotProduct = 0.0;

        Set<String> keys = components.keySet();
        for (String key : keys)
            if (v.components.containsKey(key))
                dotProduct += components.get(key) * v.components.get(key);

        return dotProduct;
    }

    /**
     * ���������� ������� ������� (�� ���������)
     *
     * @return
     */
    public double norm() {
        double sum = 0.0;

        Set<String> keys = components.keySet();
        for (String key : keys)
            sum += java.lang.Math.pow(components.get(key), 2.0);

        return java.lang.Math.pow(sum, 0.5);
    }

    /**
     * ������������ �������
     */
    public void normalize() {
        double norm = norm();
        if (norm > 0) {
            Set<String> keys = components.keySet();
            for (String key : keys)
                components.put(key, components.get(key) / norm);
        }
    }

    public static Vector average(Vector v1, int v1Weight, Vector v2, int v2Weight) {
        double v1Part = (double) v1Weight / (v1Weight + v2Weight);

        v1.multiply(v1Part);
        v2.multiply(1. - v1Part);

        Vector average = new Vector(v1);
        average.add(v2);
        return average;
    }

    /**
     * ���������� ���� tf-idf
     * �������������� ���� ��� ������ �������� ����� � ��������� ���������
     *
     * @param dictionary
     */
    public void toTF_IDF(Dictionary dictionary) {
        double totalWeight = 0.0;
        Collection<Double> weights = components.values();
        for (Double weight : weights)
            totalWeight += weight; // ���������� ����������, � ������� ����������� �����

        Set<Map.Entry<String, Double>> items = components.entrySet();
        for (Map.Entry<String, Double> item : items)
            components.put(item.getKey(), item.getValue() * dictionary.getWeight(item.getKey()) / totalWeight);
    }

    public void print() {
        LoggingUtility.info(components.toString());
    }

    public Map<String, Double> getComponents() {
        return components;
    }

    @Override
    public String toString() {
        return components.toString();
    }
}
