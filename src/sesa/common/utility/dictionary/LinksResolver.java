package sesa.common.utility.dictionary;

import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.dao.dictionary.TermLinksDao;
import sesa.common.database.entity.dictionary.TermEntity;
import sesa.common.utility.dictionary.remove.CrossReferenceFinder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LinksResolver {

    private final TermDao termDao;
    private final TermDefinitionDao termDefinitionDao;
    private final TermLinksDao termLinksDao;

    public LinksResolver() throws SQLException, ClassNotFoundException {
        termDefinitionDao = new TermDefinitionDao();
        termDao = new TermDao();
        termLinksDao = new TermLinksDao();
    }

    public boolean resolveLink(int defId) {
        String def = termDefinitionDao.getDefinitionByDefinitionId(defId);
        if (!def.contains("See also"))
            return false;
        int pos = def.indexOf("See also");
        String term = def.substring(pos + 9, def.indexOf(".", pos + 9));

        List<Integer> linkedTermsIds = findAllLinks(term);

        if (linkedTermsIds.size() > 0) {
            termLinksDao.saveToLinksCollections(defId, linkedTermsIds);
            CrossReferenceFinder finder = new CrossReferenceFinder();
            termDefinitionDao.updateDef(defId, finder.removeSentenceFromText(String.format("See also %s.", term), def));
        }
        return true;
    }

    private List<Integer> findAllLinks(String term) {
        int linkedTermId = 0;
        TermEntity termEntity = termDao.getTermByName(term);

        if (termEntity != null)
            linkedTermId = termEntity.getId();
        else
            linkedTermId = termDao.getTermByNameLike(term);

        List<Integer> linkedTermsIds = new ArrayList<>();

        if (linkedTermId != 0) {
            linkedTermsIds.add(linkedTermId);
            return linkedTermsIds;
        } else if (term.contains(",")) {
            String[] terms = term.split(",");
            for (String t : terms)
                linkedTermsIds.addAll(findAllLinks(t.trim()));
        }

        return linkedTermsIds;
    }
}
