package grammarscope.parser;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;

/**
 * Semantic pred-subject-object analyzer
 *
 * @author Bernard Bou
 */
public class SubjectPredicateAnalyzer {
    /**
     * Relation model
     */
    private final RelationModel theRelationModel;

    /**
     * Id to relation map
     */
    private final Map<String, MutableGrammaticalRelation> theIdMap;

    /**
     * Head finder
     */
    private final HeadFinder theHeadFinder;

    // C O N S T R U C T

    /**
     * Constructor
     *
     * @param thisRelationModel relation model
     */
    public SubjectPredicateAnalyzer(final RelationModel thisRelationModel) {
        System.out.println("in");
        this.theRelationModel = thisRelationModel;
        this.theIdMap = this.theRelationModel.makeIdToRelationMap();
        this.theHeadFinder = new SemanticHeadFinder();
    }

    // A N A L Y Z E P R E D I C A T E / S U B J E C T / O B J E C T

    /**
     * Analyze document
     *
     * @param thisDocumentPath document path
     * @param thisParser       parser
     * @return parsed structures
     */
    public List<List<ParsedStructure>> analyzeDocument(final String thisDocumentPath, final Parser thisParser) {
        final List<List<ParsedStructure>> thisList = new ArrayList<List<ParsedStructure>>();
        final List<Sentence> theseSentences = Parser.getSentences(thisDocumentPath);
        for (final Sentence thisSentence : theseSentences) {
            thisList.add(analyze(thisSentence, thisParser));
        }
        return thisList;
    }

    /**
     * Analyze sentence
     *
     * @param thisSentence sentence
     * @param thisParser   parser
     * @return parsed structures
     */
    public List<ParsedStructure> analyze(final Sentence thisSentence, final Parser thisParser) {
        final Tree thisTree = thisParser.parse(thisSentence);
        return analyze(thisTree);
    }

    /**
     * Analyze tree
     *
     * @param thisTree tree
     * @return parsed structures
     */
    public List<ParsedStructure> analyze(final Tree thisTree) {
        final List<ParsedStructure> thisList = new ArrayList<ParsedStructure>();
        analyze(thisTree, thisTree, thisList);
        return thisList;
    }

    /**
     * Analyze tree
     *
     * @param thisTree     target tree
     * @param thisRootTree root tree
     * @param thisList     list to collect results
     */
    private void analyze(final Tree thisTree, final Tree thisRootTree, final List<ParsedStructure> thisList) {
        //System.out.println("in");
        // don't do leaves
        if (thisTree.numChildren() <= 0)
            return;

        // System.out.println("SCOPE=" + thisTree);

        // scan for predicate grammatical relations
        Tree thisPredicateTree = null;
        String thisPredicateHead = null;
        for (final String thisId : RelationSets.thePredicateGrammaticalRelations) {
            final MutableGrammaticalRelation thisGrammaticalRelation = this.theIdMap.get(thisId);
            if (thisGrammaticalRelation.isApplicable(thisTree)) {
                for (final Tree thisTargetTree : thisGrammaticalRelation.getRelatedNodes(thisTree, thisRootTree)) {
                    thisPredicateTree = thisTargetTree;
                    final String thisTargetHead = thisTargetTree.headTerminal(this.theHeadFinder).value();
                    thisPredicateHead = Parser.morphology(thisTargetHead);
                    // System.out.println("predicate head=" + thisPredicateHead);
                    // String thisHead = thisTree.headTerminal(theHeadFinder).value();
                    // thisHead = Parser.morphology(thisHead);
                }
            }
        }
        Tree thisSubjectTree = null;
        String thisSubjectHead = null;
        for (final String thisId : RelationSets.theSubjectGrammaticalRelations) {
            final MutableGrammaticalRelation thisGrammaticalRelation = this.theIdMap.get(thisId);
            if (thisGrammaticalRelation.isApplicable(thisTree)) {
                for (final Tree thisTargetTree : thisGrammaticalRelation.getRelatedNodes(thisTree, thisRootTree)) {
                    thisSubjectTree = thisTargetTree;
                    final String thisTargetHead = thisTargetTree.headTerminal(this.theHeadFinder).value();
                    thisSubjectHead = Parser.morphology(thisTargetHead);
                    // System.out.println("subject head=" + thisSubjectHead);
                    // String thisHead = thisTree.headTerminal(theHeadFinder).value();
                    // thisHead = Parser.morphology(thisHead);
                }
            }
        }
        Tree thisObjectTree = null;
        String thisObjectHead = null;
        for (final String thisId : RelationSets.theObjectGrammaticalRelations) {
            final MutableGrammaticalRelation thisGrammaticalRelation = this.theIdMap.get(thisId);
            if (thisGrammaticalRelation.isApplicable(thisTree)) {
                for (final Tree thisTargetTree : thisGrammaticalRelation.getRelatedNodes(thisTree, thisRootTree)) {
                    thisObjectTree = thisTargetTree;
                    final String thisTargetHead = thisTargetTree.headTerminal(this.theHeadFinder).value();
                    thisObjectHead = Parser.morphology(thisTargetHead);
                    // System.out.println("object head=" + thisObjectHead);
                    // String thisHead = thisTree.headTerminal(theHeadFinder).value();
                    // thisHead = Parser.morphology(thisHead);
                }
            }
        }
        if (thisPredicateHead != null || thisSubjectHead != null || thisObjectHead != null) {
            thisList.add(new ParsedStructure(thisPredicateTree, thisSubjectTree, thisObjectTree, thisPredicateHead, thisSubjectHead, thisObjectHead));
        }

        // now recurse into children
        for (final Tree thisChildTree : thisTree.children()) {
            analyze(thisChildTree, thisRootTree, thisList);
        }
    }

    /**
     * Analyze document
     *
     * @param theseArgs arg[1] document path
     * @throws IOException
     */
    static public void main(final String[] theseArgs) throws IOException {
        final ObjectInputStream thisInputStream = IOUtils.readStreamFromString("grammar/englishPCFG.ser.gz");
        final Parser thisParser = new Parser(thisInputStream);
        final SubjectPredicateAnalyzer thisAnalyzer = new SubjectPredicateAnalyzer(DefaultMutableGrammaticalRelations.makeDefaultModel());
        final List<List<ParsedStructure>> thisParsedStructureList = thisAnalyzer.analyzeDocument(theseArgs[0], thisParser);
        for (final List<ParsedStructure> theseParsedStructures : thisParsedStructureList) {
            for (final ParsedStructure thisParsedStructure : theseParsedStructures) {
                if (thisParsedStructure.thePredicate != null) {
                    System.out.println("P=" + thisParsedStructure.thePredicate);
                }
                if (thisParsedStructure.theSubject != null) {
                    System.out.println("S=" + thisParsedStructure.theSubject);
                }
                if (thisParsedStructure.theObject != null) {
                    System.out.println("O=" + thisParsedStructure.theObject);
                }
            }
        }
        System.out.println(".");
    }
}
