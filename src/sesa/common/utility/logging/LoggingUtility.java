package sesa.common.utility.logging;

import sesa.common.configuration.Configuration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LoggingUtility {
    public static enum LOGGING_LEVEL {
        LEVEL_DEBUG, LEVEL_INFO, LEVEL_WARNING, LEVEL_ERROR, LEVEL_NONE
    }

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private BufferedWriter bufferedWriter = null;
    private LOGGING_LEVEL loggingLevel = LOGGING_LEVEL.LEVEL_NONE;

    private static LoggingUtility self = new LoggingUtility();

    protected LoggingUtility() {
        if (self != null)
            return;

        formatter.setTimeZone(Calendar.getInstance().getTimeZone());

        try {
            FileWriter fileWriter = new FileWriter(Configuration.LOG_FILE);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            System.err.println("Can't create an instance of logger class");
        }
    }

    private void close() {
        try {
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException ignored) {
        }
    }

    private void setLevel(LOGGING_LEVEL level) {
        loggingLevel = level;
    }

    private String getLevelName(LOGGING_LEVEL level) {
        switch (level) {
            case LEVEL_ERROR:
                return "  ERROR";
            case LEVEL_WARNING:
                return "WARNING";
            case LEVEL_INFO:
                return "   INFO";
            case LEVEL_DEBUG:
                return "  DEBUG";
        }

        return null;
    }

    private synchronized void print(LOGGING_LEVEL level, String message) {
        try {
            Calendar calendar = Calendar.getInstance(Calendar.getInstance().getTimeZone());
            //String output = "[" + formatter.format(calendar.getTime()) + "] " + getLevelName(level) + ": " + message;
            String output = message;
            bufferedWriter.write(output);
            bufferedWriter.newLine();
            bufferedWriter.flush();

            System.out.println(output);
        } catch (IOException e) {
            System.err.println("Can't add to log file: " + message);
        }
    }

    private void report(LOGGING_LEVEL level, String message) {
        switch (loggingLevel) {
            case LEVEL_ERROR:
                if (level == LOGGING_LEVEL.LEVEL_ERROR)
                    print(level, message);
                break;
            case LEVEL_WARNING:
                if ((level == LOGGING_LEVEL.LEVEL_ERROR) || (level == LOGGING_LEVEL.LEVEL_WARNING))
                    print(level, message);
                break;
            case LEVEL_INFO:
                if ((level == LOGGING_LEVEL.LEVEL_ERROR) || (level == LOGGING_LEVEL.LEVEL_WARNING) || (level == LOGGING_LEVEL.LEVEL_INFO))
                    print(level, message);
                break;
            case LEVEL_DEBUG:
                print(level, message);
                break;
        }
    }

    public static void debug(String message) {
        self.report(LOGGING_LEVEL.LEVEL_DEBUG, message);
    }

    public static void info(String message) {
        self.report(LOGGING_LEVEL.LEVEL_INFO, message);
    }

    public static void warning(String message) {
        self.report(LOGGING_LEVEL.LEVEL_WARNING, message);
    }

    public static void error(String message) {
        self.report(LOGGING_LEVEL.LEVEL_ERROR, message);
    }

    public static void setLoggingLevel(LOGGING_LEVEL level) {
        self.setLevel(level);
    }

    public static void die(String message) {
        error(message);
        self.close();
        System.exit(0);
    }
}
