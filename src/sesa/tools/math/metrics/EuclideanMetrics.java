package sesa.tools.math.metrics;

import sesa.tools.math.Vector;

import java.util.Map;

/**
 * Евклидова метрика
 */
public class EuclideanMetrics implements IMetrics {


  public double getDistance(Vector v1, Vector v2) {

    double result = 0;

    Map<String, Double> components1 =  v1.getComponents();
    Map<String, Double> components2 =  v2.getComponents();

    for (String component : components1.keySet())
       result += Math.pow(components1.get(component) - components2.get(component), 2);

    return Math.sqrt(result);
  }
}
