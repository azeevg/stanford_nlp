package sesa.common.features;

/**
 * ����������������� ����� Option
 *
 * @param <T>
 */
public class Option<T> {
    private T item = null;

    public Option(T item) {
        this.item = item;
    }

    public T get() {
        return item;
    }

    public T getOrElse(T item) {
        if (this.item != null)
            return this.item;
        return item;
    }

    /**
     * �������� �� ����
     * @return
     */
    public boolean isDefined() {

        return item != null;
    }
}
