package sesa.common.database.dao;

import edu.stanford.nlp.trees.Tree;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.utility.io.CommonUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


/**
 * Created by Ira on 19.02.2016.
 */
public class TreeCacheDao extends CommonDao {
    public final Logger log = LogManager.getLogger(TreeCacheDao.class);

    public TreeCacheDao() throws SQLException, ClassNotFoundException {
        super();
    }

    /**
     * Here we create instance of HashMap for trees, which we create on-fly using their serialized form (bracket-form)
     * Sentence in bracket-form are retrieved from corresponding  table of DB
     * If exception happens  don't return empty HashMap trees! return NULL!
     *
     * @param collectionName table name
     * @param titleHash      value in column titlehash
     * @return
     */
    public HashMap<Integer, Tree> getCachedTrees(String collectionName, int titleHash) {

        HashMap<Integer, Tree> trees = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE title = ?");
            ps.setInt(1, titleHash);
            ps.execute();
            resultSet = ps.getResultSet();
            trees = new HashMap<Integer, Tree>();
            while (resultSet.next()) {
                String treeString = resultSet.getString("tree");
                int hash = resultSet.getInt("hash");
                Tree tree = CommonUtils.readPennTreeFormString(treeString);
                trees.put(hash, tree);
            }

        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage());
            trees = null;
            //  debug("Can't select data from table:" + collectionName);
            //   debug("Exception: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return trees;
    }

    /**
     * Add a new record to the table "collectionName"
     *
     * @param collectionName table name
     * @param title
     * @param hash
     * @param tree
     * @return
     */
    // old name insertObject
    public boolean addCacheTree(String collectionName, int title, int hash, String tree) {

        PreparedStatement ps = null;
        Connection connection = null;

        boolean resultInsertion = false;
        try {
            connection = cp.takeConnection();
            String query = "INSERT INTO `" + collectionName + "` (title, hash, tree) VALUES(?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setInt(1, title);
            ps.setInt(2, hash);
            ps.setString(3, tree);
            ps.execute();
            resultInsertion = true;

        } catch (SQLException e) {
            log.error("Can't insert record: " + title, e);
            //  debug("Can't insert record: " + title);
            //  debug("Exception: " + e.getMessage());
            // LoggingUtility.info("SQL Exception: " + e.getMessage());
        } finally {
            cp.closeConnection(connection, ps);
        }


        return false;
    }

    //    public abstract HashMap<Integer, Tree> getCachedTreesObjects(String collectionName, String title);

}
