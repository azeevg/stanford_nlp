package sesa.tools.dictionary.builder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.builder.BuilderException;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.dao.SemanticGroupDao;
import sesa.common.database.dao.dictionary.SentenceTripletDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceForest;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.common.Application;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * In that class we call methods to parse computer dictionary definition
 * Call Stanford parser
 * Save semantic group to database
 */
public class AnalyzeDictionary extends Application {
    public final Logger log = LogManager.getLogger(AnalyzeDictionary.class);
    private String collectionName = "semantic_groups_computer_dict";
    private StanfordDataModule dataModule;
    private StanfordRelationsBuilder builder;
    private TermDao termDao;
    private TermDefinitionDao dao;
    private SemanticGroupDao semanticGroupDao;
    private SentenceTripletDao tripletDao;
    private FileManager file;

    public AnalyzeDictionary() throws SQLException, ClassNotFoundException {
        this.file = new FileManager("path");
        this.dataModule = new BasicDataModule();
        this.builder = new StanfordRelationsBuilder(file.getValue("path.smallLanguageModel"), dataModule);
        this.termDao = new TermDao();
        this.dao = new TermDefinitionDao();
        this.semanticGroupDao = new SemanticGroupDao();
        this.tripletDao = new SentenceTripletDao();
        builder.setIsModCKY(true);
    }

    public void printCollocationResult(){
        System.out.println("CollocationInSentence:" + dataModule.getCollocationNumberInSentence());
        System.out.println("CollocationInParseTree:" + dataModule.getCollocationNumberInParseTree());
    }

    /**
     * Input point to parse
     * @throws BuilderException
     */
    public void run() {
        Map<String, Integer> terms = termDao.getAllTerms();
        for (String term : terms.keySet()) {
            int idTerm = terms.get(term);
            log.info("Find semantic groups for definition term:" + term);
            try {
                saveSemanticGroupForOneTerm(idTerm, term);
            } catch (BuilderException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {
        return false;
    }

    public void saveSemanticGroupForOneTerm(int idTerm, String term) throws BuilderException{
        List<SentenceForest> list = builder.getSentenceForest(term + " .",true);
        List<TermDefinitionEntity> entities = dao.getNotNUllTermDefinitionByTermID(idTerm);

        for (TermDefinitionEntity entity : entities) {
//            FileReaderWriter.write(file.getValue("path.parseTree"),String.valueOf(entity.getIdDefinition()));
            String text = entity.getDefinition();
            try{
                builder.parseText(term,list,text); // builder.parseText(term, text);
                IDocument document = builder.buildDocument();
                saveSemanticGroupInDocument(document, entity.getIdDefinition());
            }catch (BuilderException e){
                e.printStackTrace();
            }
        }
    }

    public void saveSemanticGroupInDocument(IDocument document, int idDef) {
        for (SentenceTree sentenceTree : document.getSentences()) {
            saveTriplet(sentenceTree.getTriplet(),idDef);
            tripletDao.addToTripletCollections(sentenceTree.getTriplet(),idDef);
            for (SemanticGroup group : sentenceTree.getSemanticGroups()) {
                saveSemanticGroup(group, idDef);
            }
        }
    }

    private void saveTriplet(SentenceTriplet triplet,int idDef){
        if(triplet.getObject() != null && triplet.getSubject()!= null && triplet.getPredicate() != null){
            tripletDao.addToTripletCollections(triplet,idDef);
        }
    }

    private void saveSemanticGroup(SemanticGroup group, int idDef) {
        String groupId = UUID.randomUUID().toString();
        StringBuilder sb = new StringBuilder();
//        log.info("Save semantic relation with group id:" + groupId);
        for (SentenceToken token : group.getTokens()) {
            for (SemanticRelation relation : token.getRelations()) {
                SentenceTree sentence = group.getSentenceTree();
                int serviceTokens = sentence.containsServiceTokens() ? 1 : 0;
                int prevDelimiter = token.isPreviousTokenDelimiter() ? 1 : 0;
                String value = token.getBaseValue();
                String base = value != null ? value : "";
                semanticGroupDao.saveSemanticRelation(collectionName, token.getValue(), base,
                        relation.getShortName(), token.getPartOfSpeech(), token.getSentencePosition(),
                        groupId, idDef, serviceTokens, prevDelimiter, sentence.getSize());

                sb.append(token.getValue());
                sb.append(" -- ");
            }
        }
//        FileReaderWriter.write(file.getValue("path.semanticGroup"), "SemanticGroup: id = " + groupId + ", group value: " + sb.toString());
    }


}
