package sesa.common.builder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import sesa.common.model.processing.document.DocumentNode;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceForest;

import java.util.List;

/**
 * This abstract class represents a methods to building a model of the document
 * one abstract method IDocument buildDocument()
 */
public abstract class DocumentBuilder {
    public final Logger log = LogManager.getLogger(DocumentBuilder.class);
    protected String title;
    protected DocumentNode model;
    protected String id;
    protected boolean isModCKY = false; //Key responsible for the choice of CKY algorithm

    public abstract IDocument buildDocument();

    /**
     * Parse text to HTML document
     *
     * @param text html text in string
     * @return HTML Document
     */
    protected Document parseDocument(String text) {
        return Jsoup.parse(text);
    }

    public DocumentBuilder() {
    }

    public DocumentBuilder(String text, String id) throws BuilderException {
        setData(text, id);
    }

    public void setIsModCKY(boolean isModCKY) {
    }

    /**
     * Initialize data
     *
     * @param text HTML in a Java String
     * @param id   id text
     * @throws BuilderException
     */
    public void setData(String text, String id) throws BuilderException {
        org.jsoup.nodes.Document parsed = parseDocument(text);
        this.title = parsed.title(); // <title>
        this.model = buildDocumentTree(parsed); //Parse document from a String
        this.id = id;
    }

    /**
     * Set data
     *
     * @param text  input text
     * @param title title of text
     * @return
     * @throws BuilderException
     */
    public DocumentNode parseText(String title, String text) throws BuilderException {
        DocumentNode root = new DocumentNode();
        DocumentNode node = root;
        int currentLevel = 1;
        root.setTitle(buildParagraphSubTree(title, true, currentLevel));
        node.setParagraph(buildParagraphSubTree(text, false, currentLevel));
        this.title = title;
        this.model = root;
        this.id = title;
        return root;
    }

    /**
     * Text was in html format
     *
     * @param title
     * @param titleForest
     * @param text
     * @return
     * @throws BuilderException
     */
    public DocumentNode parseText(String title, List<SentenceForest> titleForest, String text) throws BuilderException {
        DocumentNode root = new DocumentNode();
        DocumentNode node = root;
        int currentLevel = 1;
        root.setTitle(titleForest);
        node.setParagraph(buildParagraphSubTree(text, false, currentLevel));
        this.title = title;
        this.model = root;
        this.id = title;
        return root;
    }

    public List<SentenceForest> getSentenceForest(String text, boolean isTile) throws BuilderException {
        int currentLevel = 1;
        return buildParagraphSubTree(text, isTile, currentLevel);
    }


    /**
     * Parse text in HTML format
     * For each sentence perform syntactic and semantic analysis
     * 1) Syntactic analysis - build parse trees
     * 2) Semantic analysis - find semantic groups and find triplet
     * Convert text to a tree structure
     *
     * @param document parsed HTML input string
     * @return
     * @throws BuilderException
     */
    protected DocumentNode buildDocumentTree(Document document) throws BuilderException {

        title = document.title(); // page header

        DocumentNode root = new DocumentNode();
        DocumentNode currentNode = root;
        DocumentNode parentNode = root;

        int currentLevel = 1;
        root.setLevel(currentLevel);
        root.setTitle(buildParagraphSubTree(title, true, currentLevel));
        for (Node child : document.body().childNodes()) {
            if (child instanceof TextNode) {
                String paragraph = ((TextNode) child).text();
                currentNode.setParagraph(buildParagraphSubTree(paragraph, false, currentLevel));

            } else if (child instanceof Element) {
                Element header = (Element) child;
                String tagName = header.tagName();
                if (isHeaderElement(tagName)) {
                    int headerLevel = getHeaderLevel(tagName);
                    DocumentNode node = new DocumentNode();
                    node.setLevel(headerLevel);
                    node.setTitle(buildParagraphSubTree(header.text(), true, currentLevel));

                    if (headerLevel == currentLevel)
                        parentNode.append(node);
                    else if (headerLevel > currentLevel) {
                        parentNode = currentNode;
                        parentNode.append(node);
                    } else {
                        parentNode = currentNode.getParent();
                        parentNode.append(node);
                    }
                    node.setParent(parentNode);
                    currentNode = node;
                    currentLevel = headerLevel;
                }
            }
        }
        postProcess();
        return root;
    }


    /**
     * Get header level
     *
     * @param headerTag header string
     * @return level
     */
    protected int getHeaderLevel(String headerTag) {
        return new Integer(headerTag.replaceAll("[^\\d]", ""));
    }

    /**
     * Is the title bar? * Does the header Tag string regular expression h [1-9] +
     *
     * @param headerTag
     * @return true if it's header element
     */
    protected boolean isHeaderElement(String headerTag) {
        return headerTag.matches("h[1-9]+"); // check regular
    }

    /**
     * Building of the parse trees, semantic groups for suggestions in one paragraph
     *
     * @param text    input text
     * @param isTitle flag
     * @param level   paragraph level
     * @return list of sentence forest
     * @throws BuilderException
     */
    protected abstract List<SentenceForest> buildParagraphSubTree(String text, boolean isTitle, int level) throws BuilderException;

    public void postProcess() {

    }
}
