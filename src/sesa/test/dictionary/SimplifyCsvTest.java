package sesa.test.dictionary;

import org.jsoup.select.Collector;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by gleb on 05.06.17.
 */
public class SimplifyCsvTest {
    int num = 31649;

    @Test
    public void findLonelyNodesAndRetrieveStatistics() throws IOException {
        List<Integer> totallyLonelyNodes = new ArrayList<>();
        List<Integer> noInNodes = new ArrayList<>();
        List<Integer> noOutNodes = new ArrayList<>();
        boolean[] isRowRelated = new boolean[num];
        boolean[] isColumnRelated = new boolean[num];
        int[] stat = new int[50];
        Scanner scanner = new Scanner(new File("relationsTable_" + num + ".csv"));

        int i = 0;
        while (scanner.hasNextLine()) {
            int[] row = Arrays.stream(scanner.nextLine().split(";")).mapToInt(Integer::parseInt).toArray();
            for (int j = 0; j < isColumnRelated.length; j++) {
                if (row[j] != -1)
                    stat[row[j]]++;
                if (j != i && row[j] != -1) {
                    isColumnRelated[j] = true;
                    isRowRelated[i] = true;
                }
            }

            i++;
        }
        scanner.close();

        scanner = new Scanner(new File("ids_" + num + ".csv"));
        List<Integer> ids = Arrays.stream(scanner.nextLine().split(";")).map(Integer::parseInt).collect(Collectors.toList());
        ids.sort(Integer::compareTo);
        scanner.close();


        for (int j = 0; j < isRowRelated.length; j++) {
            if (!isRowRelated[j])
                noOutNodes.add(ids.get(j));
            if (!isColumnRelated[j])
                noInNodes.add(ids.get(j));
            if (!isRowRelated[j] && !isColumnRelated[j])
                totallyLonelyNodes.add(ids.get(j));
        }

        totallyLonelyNodes.sort(Integer::compareTo);
        noInNodes.sort(Integer::compareTo);
        noOutNodes.sort(Integer::compareTo);

        FileWriter writer = new FileWriter("lonelyNodes_" + num + ".txt");

        writer.append("totallyLonelyNodes" + totallyLonelyNodes.size() + "\n");
        totallyLonelyNodes.forEach(n -> {
            try {
                writer.append(n + "").append(";");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.append("\nnoInNodes" + noInNodes.size() + "\n");

        noInNodes.forEach(n -> {
            try {
                writer.append(n + "").append(";");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.append("\nnoOutNodes" + noOutNodes.size() + "\n");

        noOutNodes.forEach(n -> {
            try {
                writer.append(n + "").append(";");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        writer.close();

        FileWriter writer1 = new FileWriter("stats_" + num + ".csv");
        for (int j = 0; j < stat.length; j++) {
            if (stat[j] != 0)
                writer1.append("" + j + ";").append("" + stat[j] + "\n");
        }
        writer1.close();
    }
}