package sesa.tools.dictionary.builder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.builder.BuilderException;
import sesa.common.builder.DictionaryDocumentBuilder;
import sesa.common.configuration.Categories;
import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.parsing.XMLDocument;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.vector.DocumentUtils;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by Ira on 11.03.2016.
 */

/**
 * Use symmetric difference for dictionary build
 */
public class SymDifferenceDictionaryBuilder extends Application {
    private String collectionName = Configuration.DB_COLLECTION_CATEGORIES_UNIQUE_DICTIONARIES;
    private DocumentsDao documentsDao = new DocumentsDao();
    private String[] categoryNames = Categories.ALL_CATEGORIES;

    public final Logger log = LogManager.getLogger(SymDifferenceDictionaryBuilder.class);

    public SymDifferenceDictionaryBuilder() throws SQLException, ClassNotFoundException {
    }

    protected boolean body() throws SQLException, ClassNotFoundException {
        Map<String, Object> entity = prepareDictionary();
        saveDictionary(collectionName, entity);
        return true;
    }

    private Map<String, Object> prepareDictionary() throws SQLException, ClassNotFoundException {

        Map<String, Set<String>> uniqueTokenMap = new HashMap<String, Set<String>>();
        Map<String, Object> entity = new HashMap<String, Object>();

        for (String categoryName : categoryNames) {
            Set<String> categorySet = new HashSet<String>();
            String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);

            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(collectionName);
            iterator = new DBReportingIterator(iterator, "Extracting tokens | category = " + categoryName);
            while (iterator.hasNext()) {

                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String text = (String) map.get("w_filtered_text");
                String id = doc.getID();

                IDocument document = documentBuilder(id, text);

               /*
                IDocument document = null
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
                try{
                    document = documentBuilder.buildDocument(id, text, null);
                }catch (BuilderException e){
                    log.error(e.getMessage());
                }*/

                List<SentenceTree> sentences = document.getSentences();
                // получаем уникальные токены в предложении
                Set<String> uniqueTokens = DocumentUtils.getUniqueTokens(sentences);
                for (String token : uniqueTokens) {
                    categorySet.add(token);
                }
                // все уникальные токены кладем в мапу по конкретному ключу:  categorySet
                uniqueTokenMap.put(categoryName, categorySet);
            }
        }

        Map<String, Set<String>> filteredMap = getFilteredMap(uniqueTokenMap);

        for (String key : filteredMap.keySet()) {
            Set<String> tokens = filteredMap.get(key);
            entity.put(key, tokens);

        }
        return entity;
    }

    private IDocument documentBuilder(String id, String text){

        IDocument document = null;

        DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
        try{
            document = documentBuilder.buildDocument(id, text, null);
        }catch (BuilderException e){
            log.error(e.getMessage());
        }

        return document;
    }

    // Оставляет неповторяющиеся значения по (Set<String>) в uniqueTokenMap
    // симментрическая разница, оставляет только те токены, которые уникальны в каждой категории
    private Map<String, Set<String>> getFilteredMap(Map<String, Set<String>> uniqueTokenMap) {
        Map<String, Set<String>> filteredTokenMap = new HashMap<String, Set<String>>();
        Map<String, Integer> globalTokenMap = new HashMap<String, Integer>();

        for (String key : uniqueTokenMap.keySet()) {
            for (String token : uniqueTokenMap.get(key)) {
                if (globalTokenMap.containsKey(token)) {
                    Integer value = globalTokenMap.get(token);
                    globalTokenMap.put(token, ++value);
                } else {
                    globalTokenMap.put(token, 0);
                }
            }
        }

        for (String key : uniqueTokenMap.keySet()) {
            Set<String> filteredList = new HashSet<String>();
            for (String token : uniqueTokenMap.get(key)) {
                if (globalTokenMap.get(token) == 0) {
                    filteredList.add(token);
                }
            }
            filteredTokenMap.put(key, filteredList);
        }

        return filteredTokenMap;
    }
    private void saveDictionary(String collectionName, Map<String, Object> entity) {

        documentsDao.truncateOrCreateDocumentCollection(collectionName);
        documentsDao.addToDocumentsCollection(collectionName, new XMLDocument(entity));

    }
}
