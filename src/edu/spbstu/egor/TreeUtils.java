package edu.spbstu.egor;

import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchedCollocation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.trees.LabeledScoredTreeNode;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;
import edu.stanford.nlp.trees.TreeTransformer;

import java.io.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by Egor Gorbunov on 30.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class TreeUtils {
    private final TreeTransformer subcatStripper;
    private final TreeTransformer debinarizer;
    private final BoundaryRemover boundaryRemover;
    TreePrint pennTreePrint;
    TreePrint tikzTreePrint;
    PrintWriter stdPrintWriter;

    public TreeUtils(Options options) {
        pennTreePrint = new TreePrint("penn");
        tikzTreePrint = new TreePrint("tikz");
        stdPrintWriter = new PrintWriter(System.out, true);


        subcatStripper = options.tlpParams.subcategoryStripper();
        debinarizer = new Debinarizer(options.forceCNF);
        boundaryRemover = new BoundaryRemover();
    }

    public Tree transformTreeToStandardShape(Tree t) {
        return boundaryRemover.transformTree(
                subcatStripper.transformTree(
                        debinarizer.transformTree(t)));
    }

    public void purePennPrint(Tree t, PrintWriter pw) {
        pennTreePrint.printTree(t, pw);
    }

    public void pureTikzPrint(Tree t, PrintWriter pw) {
        tikzTreePrint.printTree(t, pw);
    }

    public void preprocessedPennPrint(Tree t, PrintWriter pw) {
        pennTreePrint.printTree(transformTreeToStandardShape(t), pw);
    }


    public void preprocessedTikzPrint(Tree t, PrintWriter pw) {
        tikzTreePrint.printTree(transformTreeToStandardShape(t), pw);
    }

    // std out
    public void purePennPrint(Tree t) {
        purePennPrint(t, stdPrintWriter);
    }

    public void pureTikzPrint(Tree t) {
        pureTikzPrint(t, stdPrintWriter);
    }

    public void preprocessedPennPrint(Tree t) {
        preprocessedPennPrint(t, stdPrintWriter);
    }
    public void preprocessedPennPrint2(Tree t,PrintWriter pw ) {
        preprocessedPennPrint(t, pw);
    }

    public void preprocessedTikzPrint(Tree t) {
        preprocessedTikzPrint(t, stdPrintWriter);
    }

    public void printTexFile(Tree[] trees, Writer writer, int from, int to, String title) {
        if (from < 0 || from > to || to > trees.length) {
            throw new IllegalArgumentException("Bad sub array boundaries: [" + from + ", " + to + ")");
        }

        final int BIG_TREE_SIZE = 25;

        PrintWriter pw = new PrintWriter(writer);

        pw.write("\\documentclass[12pt, a4paper]{article}\n");
        pw.write("\\usepackage[utf8]{inputenc}\n");
        pw.write("\\usepackage[russian]{babel}\n");
        pw.write("\\usepackage[margin=0.5in]{geometry}\n");
        pw.write("\\usepackage{pscyr}\n");
        pw.write("\\usepackage[usenames,dvipsnames]{xcolor}\n");
        pw.write("\\usepackage{tikz}\n");
        pw.write("\\usepackage{tikz-qtree}\n\n");
        pw.write("\\newcommand{\\red}[1]{\\textcolor{red}{#1}}\n");
        pw.write("\\newcommand{\\green}[1]{\\textcolor{OliveGreen}{#1}}\n\n");

        if (title != null) {
            pw.write("\\title{" + title + "}\n");
            pw.write("\\date{}\n");
            pw.write("\\author{}\n");
        }

        pw.write("\\begin{document}\n");

        if (title != null) {
            pw.write("\\maketitle\n");
        }

        for (int i = from; i < to; ++i) {
            if (trees[i].size() >= BIG_TREE_SIZE) {
                pw.write("\\resizebox{\\linewidth}{!}{");
            }

            preprocessedTikzPrint(trees[i], pw);

            if (trees[i].size() >= BIG_TREE_SIZE) {
                pw.write("}");
            }
            pw.write("\\\\\n");
        }

        pw.write("\\end{document}");

        pw.flush();
        pw.close();
    }

    public void printTexFile(Tree[] trees, Writer writer, String title) throws IOException {
        printTexFile(trees, writer, 0, trees.length, title);
    }

    private static void addColor(Tree t, String color) {
        t.setValue("\\" + color + "{" + t.value() + "}");
    }

    public static void colorSubtree(Tree t, String color) {
        addColor(t, color);
        for (Tree child : t.getChildrenAsList()) {
            colorSubtree(child, color);
        }
    }

    private static final String RED = "red";
    private static final String GREEN = "green";

    /**
     * <p>
     * That method colours given trees labels. If {@code oldT} label not equal to {@code newT} label
     * first one will be coloured {@code red} with all it's subtree and second one will be coloured {@code green} with
     * all it's subtree. If labels are equal procedure applied to children of given nodes, that children
     * are compared in strict (from left to right) order. If given {@code oldT} has more children than {@code newT}
     * all extra children trees will be coloured {@code red}, else if {@code newT} has more children, it's extra
     * children will be coloured {@code green}.
     * </p>
     * <p>
     * To colour tree label means to wrap it into LaTeX construction like: {@code \red{label}}.
     * Use LaTeX commands for correct compilation by some LaTeX compiling tool:
     * {@code \newcommand{\red}[1]{\textcolor{red}{#1}}}
     * </p>
     * <p>
     * Green colour can be considered as a colour, that means, that something new appeared in {@code newT} in comparison
     * to {@code oldT} (that colour only used in {@code newT} labels. In contrast red colour, which used only on
     * {@code oldT} labels may be treated as colour of nodes, which were deleted from the {@code oldT} tree during
     * process of converting it to {@code newT}
     * </p>
     *
     * @param oldT - first tree to compare (treat it like an old tree - tree, which were converted to {@code newT}
     * @param newT - second tree to compare
     *
     * @return returns depth of the maximum change in the tree
     */
    public static int compareTrees(Tree oldT, Tree newT) {
        if (oldT == null && newT == null)
            return 0;

        int oldTDepth = (oldT == null) ? 0 :oldT.depth();
        int newTDepth = (newT == null) ? 0 :newT.depth();

        if (newT == null) {
            colorSubtree(oldT, RED);
            return oldTDepth;
        } else if (oldT == null) {
            colorSubtree(newT, GREEN);
            return newTDepth;
        } else if (!oldT.value().equals(newT.value())) {
            colorSubtree(oldT, RED);
            colorSubtree(newT, GREEN);
            return Math.max(oldTDepth, newTDepth);
        } else {
            int oldChildNum = oldT.getChildrenAsList().size();
            int newChildNum = newT.getChildrenAsList().size();

            int maxChange = 0;
            for (int i = 0; i < Math.max(oldChildNum, newChildNum); ++i) {
                Tree oldChild = (i < oldChildNum) ? oldT.getChild(i) : null;
                Tree newChild = (i < newChildNum) ? newT.getChild(i) : null;
                int changeDep = compareTrees(oldChild, newChild);
                maxChange = Math.max(changeDep, maxChange);
            }
            return maxChange;
        }
    }

    /**
     * Dumps tree into given reader in recursive manner:
     * Given tree:
     *              ROOT
     *             / | \
     *             A B C
     *            /|   |\
     *           D E   F G
     * Will be dumped to writer next way:
     *          ROOT
     *          3
     *          A
     *          2
     *          D
     *          0
     *          E
     *          0
     *          B
     *          0
     *          C
     *          2
     *          F
     *          0
     *          G
     *          0
     * @param t -- tree to dump
     * @param writer -- where to dump
     */
    public static void dumpTree(Tree t, BufferedWriter writer) throws IOException {
        writer.write(t.label().value());
        writer.write('\n');
        writer.write(Integer.toString(t.children().length));
        writer.write('\n');
        for (Tree child : t.children()) {
            dumpTree(child, writer);
        }
    }

    /**
     * Recursively reads tree from reader, where dump of the tree is written (see {@code dumpTree()} method).
     * @param parent -- tree which label will be immediately read
     * @param reader -- from where to read
     */
    private static void recursiveTreeRead(Tree parent, BufferedReader reader) throws IOException {
        String labelStr = reader.readLine();
        CoreLabel l = new CoreLabel();
        l.setValue(labelStr);
        parent.setLabel(l);

        int childNum = Integer.valueOf(reader.readLine());

        for (int i = 0; i < childNum; ++i) {
            Tree child = new LabeledScoredTreeNode();
            parent.addChild(child);
            recursiveTreeRead(child, reader);
        }
    }

    /**
     * Returns tree read from given reader. See {@code recursiveTreeRead()} method for details
     * of how read process is constructed
     */
    public static Tree readDumpedTree(BufferedReader reader) throws IOException {
        Tree resTree = new LabeledScoredTreeNode();
        recursiveTreeRead(resTree, reader);
        return resTree;
    }

    /**
     * Reads trees from given reader. Thre trees must be stored in reader as like
     * they dumped into with {@code TreeUtils.dumpAllTrees(...)} method.
     */
    public static Tree[] readAllTrees(BufferedReader reader) throws IOException {
        int numTrees = Integer.valueOf(reader.readLine());

        Tree[] resultTrees = new Tree[numTrees];

        for (int i = 0; i < resultTrees.length; ++i) {
            resultTrees[i] = readDumpedTree(reader);
        }

        return resultTrees;
    }

    /**
     * Writes array of trees to given writer. The first line of output will be number of trees and after that line
     * all the trees are written with {@code TreeUtils.dumpTree(...)} method.
     */
    public static void dumpAllTrees(Tree[] trees, BufferedWriter writer) throws IOException {
        writer.write(Integer.toString(trees.length));
        writer.write('\n');

        for (Tree tree : trees) {
            dumpTree(tree, writer);
        }
    }

    /**
     * That method takes parse tree and Match as an argument and that parse tree must correspond
     * to given Match (i.e. sentence stored in Match must be the sentence, for which parse tree was constructed).
     * @param parse -- tree to check
     * @param match -- match, where all collocation info for sentence stored
     * @return {@code true} if for every collocation C all words w_i \in C has same closest non-terminal and not
     * pre-terminal node
     */
    public static boolean isGoodCollocationParse(Tree parse, Match match) {
        List<Tree> leaves = parse.getLeaves();
        for (MatchedCollocation mc : match.getMatchedCollocations()) {
            Tree reference = leaves.get(mc.wEnd).parent(parse).parent(parse); // first parent is pre-terminal (pos tag), but we need group label.
            for (int i = mc.wStart; i < mc.wEnd; ++i) {
                if (leaves.get(i).parent(parse).parent(parse) != reference) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Prints all trees into multiple tex files.
     */
    public void printTreesToTexFiles(Tree[] trees, String pathPrefix, int maxTreesInFileNum, String titlePrefix) throws IOException {
        int fileIndex = 1;
        for (int i = 0; i < trees.length; i += maxTreesInFileNum) {

            String fileIndexStr = Integer.toString(fileIndex);
            fileIndexStr = ("00" + fileIndexStr).substring(fileIndexStr.length());

            this.printTexFile(trees, new BufferedWriter(new FileWriter(new File(pathPrefix + fileIndexStr + ".tex"))),
                    i, Math.min(i + maxTreesInFileNum, trees.length), titlePrefix + "\\\\Part: " + fileIndexStr);

            fileIndex += 1;
        }
    }

    public void printTreesToTexFiles(Collection<Tree> trees, String pathPrefix, int maxTreesInFileNum, String titlePrefix) throws IOException {
        Tree[] arr = trees.toArray(new Tree[trees.size()]);
        printTreesToTexFiles(arr, pathPrefix, maxTreesInFileNum, titlePrefix);
    }

    public static void colorCollocations(Match match, Tree tree) {
        List<Tree> leaves = tree.getLeaves();
        for (MatchedCollocation mc : match.getMatchedCollocations()) {
            for (int i = mc.wStart; i <= mc.wEnd; ++i) {
                addColor(leaves.get(i), "red");
            }
        }
    }

}
