package edu.spbstu.egor.wrapper;

import edu.spbstu.appmath.collocmatch.Collocation;
import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.egor.Grammar;
import edu.stanford.nlp.io.EncodingPrintWriter;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.util.Timing;
import sesa.common.utility.FileReaderWriter;

import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;

/**
 * Created by Egor Gorbunov on 09.06.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class CollocationSpanTaggingParser extends ExhaustivePCFGParserNoOutsides {
    private static final Logger logger = Logger.getLogger(CollocationSpanTaggingParser.class.getSimpleName());

    ParserTracer parserTracer;

    private final CollocationDictionary collocationDictionary;
    private int maxSpanForTags = 1;
    private boolean lengthNormalization = true;

    private BaseLexicon baseLexicon;
    private Grammar grammar;


   // public HashMap<Integer, Integer> start_end_collocation_index = new HashMap<Integer, Integer>();

    public CollocationSpanTaggingParser(Grammar grammar, CollocationDictionary collocationDictionary) {
        super(grammar.bg, grammar.ug, grammar.lex, grammar.op, grammar.stateIndex, grammar.wordIndex, grammar.tagIndex);

        this.grammar = grammar;
       // FileReaderWriter.write("data/indexOldWord3.txt",grammar.wordIndex.toString());
        this.collocationDictionary = collocationDictionary;

        extendGrammar();

        for (Collocation c : collocationDictionary) {
            if (maxSpanForTags < c.words().length) {
                maxSpanForTags = c.words().length;
            }
        }

        baseLexicon = (BaseLexicon) lex;
    }

    //isExtended: using a flag we will not extend grammar again
    // change flag in constructor LexicalizedParserQuery(LexicalizedParser parser, Grammar grammar)
    public CollocationSpanTaggingParser(Grammar grammar, CollocationDictionary collocationDictionary, boolean isExtended) {
        super(grammar.bg, grammar.ug, grammar.lex, grammar.op, grammar.stateIndex, grammar.wordIndex, grammar.tagIndex);

        this.grammar = grammar;
        this.collocationDictionary = collocationDictionary;

        if(!isExtended){
            extendGrammar();
        }

        for (Collocation c : collocationDictionary) {
            if (maxSpanForTags < c.words().length) {
                maxSpanForTags = c.words().length;
            }
        }

        baseLexicon = (BaseLexicon) lex;
    }


    private void extendGrammar() {
        // Preparing grammar for collocation span parser
        logger.info("Adding new tag to grammar: " + Grammar.Const.COLLOCATION_TAG);
        grammar.addNewTag(Grammar.Const.COLLOCATION_TAG);

        float score = -1f;
        TreebankLanguagePack tlp = grammar.op.langpack();
        for (int state = 0; state < grammar.stateIndex.size(); ++state) {
            if (tlp.basicCategory(grammar.stateIndex.get(state)).equals(Grammar.Const.NUOUN_PHRASE_TAG)) {

                logger.info("Adding new unary rule to grammar: (" + grammar.stateIndex.get(state) +
                        " --> " + Grammar.Const.COLLOCATION_TAG + ", score = " + score + ")");
                grammar.addNewUnaryRule(grammar.stateIndex.get(state), Grammar.Const.COLLOCATION_TAG, score);
            }
        }

        logger.info("Adding collocation words to grammar.");
        int oldSize = grammar.wordIndex.size();
        int collocationSize = 0;
        int i = 0;


        for (Collocation c : collocationDictionary) {
            StringBuilder sb = new StringBuilder();
            for (String w : c.words()) {
                sb.append(w);
            }
            collocationSize++;
            // Added by Ira Daniel
            try {

                grammar.addWordNoRefresh(sb.toString(), Grammar.Const.COLLOCATION_TAG, 5000000);
            } catch (IllegalArgumentException e) {
                System.out.println(sb.toString());
                i++;
              //  FileReaderWriter.write("data/problemWords3.txt", sb.toString());
                logger.warning("wordIndex contains word:" + sb.toString());
            }

        }

        System.out.println("collocationSize:" + collocationSize);


        logger.info("Refreshing grammar...");
        grammar.refresh();
        refreshStateNumber();
     //   FileReaderWriter.write("data/indexFinalWord.txt",grammar.wordIndex.toString());
        System.out.println("Size of problem words:" + i);
        System.out.println("Size old wordIndex:" + oldSize);
        System.out.println("Size new wordIndex:" + grammar.wordIndex.size());
        // FileReaderWriter.write("data/indexNewWord2.txt",grammar.wordIndex.toString());
    }




    @Override
    protected void initializeChart(List sentence) {
        int boundary = wordIndex.indexOf(Lexicon.BOUNDARY);

        for (int start = 0; start + 1 <= length; start++) {
            // parserTracer.trace();

            String curWord = "";
            if (sentence.get(start) instanceof HasWord) {
                curWord = ((HasWord) sentence.get(start)).word();
            }

            if (maxSpanForTags > 1) { // only relevant for parsing single words as multiple input tokens.
                final float VERY_LOW_LOG_PROB = -10000.0f;
                // EGOR_INFO: Code has some differences from Stanford code, which are made because of
                // EGOR_INFO: our knowledge about collocations, so if we met a word, which can be considered
                // EGOR_INFO: as first word in collocations we trying to capture that collocation in code below.
                // EGOR_INFO: We append sentence words only in case if all of them could be found in collocations
                // EGOR_INFO: words dictionary...
                // note we don't look for "words" including the end symbol!
                for (int end = start + 1; (end < length - 1 && end - start <= maxSpanForTags) || (start + 1 == end); end++) {
                    StringBuilder collocationStr = new StringBuilder();
                    //wsg: Feb 2010 - Appears to support character-level parsing
                    for (int i = start; i < end; i++) {

                        if (i - start > 0) {
                            collocationStr.append(" ");
                        }

                        if (sentence.get(i) instanceof HasWord) {
                            HasWord cl = (HasWord) sentence.get(i);
                            collocationStr.append(cl.word());
                        } else {
                            collocationStr.append(sentence.get(i).toString());
                        }
                    }


                    Collocation collocation = new Collocation(collocationStr.toString().toLowerCase());
                    //System.out.println("collocations:" + collocationStr.toString().toLowerCase());
                    for (int state = 0; state < numStates; state++) {
                        float iS = iScore[start][end][state];
                        if (iS == Float.NEGATIVE_INFINITY && isTag[state]) {

                            collocation.setPOS(stateIndex.get(state));

                            // scoring
                            iScore[start][end][state] = baseLexicon.score(collocation, start);

                            if (iScore[start][end][state] > Float.NEGATIVE_INFINITY) {
                                narrowRExtent[start][state] = start + 1;
                                narrowLExtent[end][state] = end - 1;
                                wideRExtent[start][state] = start + 1;
                                wideLExtent[end][state] = end - 1;
                            }
                        }
                    }

                    // EGOR_INFO: I've added unaries rules adding, because there was no way to
                    // EGOR_INFO: to construct, for example, NP over NN (i.e. apply rule NP --> NN)
                    for (int state = 0; state < numStates; state++) {
                        float iS = iScore[start][end][state]; // EGOR: here we also may get Parent nodes, not only tags...
                        if (iS == Float.NEGATIVE_INFINITY) {
                            continue;
                        }
                        UnaryRule[] unaries = ug.closedRulesByChild(state);
                        for (UnaryRule ur : unaries) {
                            int parentState = ur.parent;
                            float pS = ur.score;
                            float tot = iS + pS;
                            if (tot > iScore[start][end][parentState]) {
                                iScore[start][end][parentState] = tot;
                                narrowRExtent[start][parentState] = end;
                                narrowLExtent[end][parentState] = start;
                                wideRExtent[start][parentState] = end;
                                wideLExtent[end][parentState] = start;
                            }
                        }
                    }
                }

            } else { // "normal" chart initialization of the [start,start+1] cell
                normalTaggingInit(sentence, boundary, start);
            }
        }
    }

    private void normalTaggingInit(List sentence, int boundary, int start) {
        int word = words[start];
        int end = start + 1;
        Arrays.fill(tags[start], false);

        //Force tags
        String trueTagStr = null;
        if (sentence.get(start) instanceof HasTag) {
            trueTagStr = ((HasTag) sentence.get(start)).tag();
            if ("".equals(trueTagStr)) {
                trueTagStr = null;
            }
        }

        //Word context (e.g., morphosyntactic info)
        String wordContextStr = null;
        if (sentence.get(start) instanceof HasContext) {
            wordContextStr = ((HasContext) sentence.get(start)).originalText();
            if ("".equals(wordContextStr))
                wordContextStr = null;
        }

        boolean assignedSomeTag = false;

        if (!floodTags || word == boundary) {
            // in this case we generate the taggings in the lexicon,
            // which may itself be tagging flexibly or using a strict lexicon.
            if (dumpTagging) {
                EncodingPrintWriter.err.println("Normal tagging " + wordIndex.get(word) + " [" + word + "]", "UTF-8");
            }
            for (Iterator<IntTaggedWord> taggingI = baseLexicon.ruleIteratorByWord(word, start, wordContextStr); taggingI.hasNext(); ) {
                IntTaggedWord tagging = taggingI.next();
                int state = stateIndex.indexOf(tagIndex.get(tagging.tag));
                if (trueTagStr != null) { // if word was supplied with a POS tag, skip all taggings not basicCategory() compatible with supplied tag.
                    if ((!op.testOptions.forceTagBeginnings && !tlp.basicCategory(tagging.tagString(tagIndex)).equals(trueTagStr)) ||
                            (op.testOptions.forceTagBeginnings && !tagging.tagString(tagIndex).startsWith(trueTagStr))) {
                        if (dumpTagging) {
                            EncodingPrintWriter.err.println("  Skipping " + tagging + " as it doesn't match trueTagStr: " + trueTagStr, "UTF-8");
                        }
                        continue;
                    }
                }
                // try {
                float lexScore = baseLexicon.score(tagging, start, wordIndex.get(tagging.word)); // score the cell according to P(word|tag) in the lexicon
                if (lexScore > Float.NEGATIVE_INFINITY) {
                    assignedSomeTag = true;
                    iScore[start][end][state] = lexScore;
                    narrowRExtent[start][state] = end; // EGOR: why not start but end??? Is it [..) or what?
                    narrowLExtent[end][state] = start;
                    wideRExtent[start][state] = end; // EGOR: why not start but end???
                    wideLExtent[end][state] = start;
                }
                // } catch (Exception e) {
                // e.printStackTrace();
                // System.out.println("State: " + state + " tags " + Numberer.getGlobalNumberer("tags").object(tagging.tag));
                // }
                int tag = tagging.tag;
                tags[start][tag] = true;
                if (dumpTagging) {
                    EncodingPrintWriter.err.println("Word pos " + start + " tagging " + tagging + " score " + iScore[start][end][state] + " [state " + stateIndex.get(state) + " = " + state + "]", "UTF-8");
                }
                //if (start == length-2 && tagging.parent == puncTag)
                //  lastIsPunc = true;
            }
        } // end if ( ! floodTags || word == boundary)

        if (!assignedSomeTag) {
            // If you got here, either you were using forceTags (gold tags)
            // and the gold tag was not seen with that word in the training data
            // or we are in floodTags=true (recovery parse) mode
            // Here, we give words all tags for
            // which the lexicon score is not -Inf, not just seen or
            // specified taggings
            if (dumpTagging) {
                EncodingPrintWriter.err.println("Forced FlexiTagging " + wordIndex.get(word), "UTF-8");
            }
            for (int state = 0; state < numStates; state++) {
                if (isTag[state] && iScore[start][end][state] == Float.NEGATIVE_INFINITY) {
                    if (trueTagStr != null) {
                        String tagString = stateIndex.get(state);
                        if (!tlp.basicCategory(tagString).equals(trueTagStr)) {
                            continue;
                        }
                    }

                    float lexScore = baseLexicon.score(new IntTaggedWord(word, tagIndex.indexOf(stateIndex.get(state))), start, wordIndex.get(word));
                    if (lexScore > Float.NEGATIVE_INFINITY) {
                        iScore[start][end][state] = lexScore;
                        narrowRExtent[start][state] = end;
                        narrowLExtent[end][state] = start;
                        wideRExtent[start][state] = end;
                        wideLExtent[end][state] = start;
                    }
                    if (dumpTagging) {
                        EncodingPrintWriter.err.println("Word pos " + start + " tagging " + (new IntTaggedWord(word, tagIndex.indexOf(stateIndex.get(state)))) + " score " + iScore[start][start + 1][state] + " [state " + stateIndex.get(state) + " = " + state + "]", "UTF-8");
                    }
                }
            }
        } // end if ! assignedSomeTag

        // tag multi-counting
        if (op.dcTags) {
            for (int state = 0; state < numStates; state++) {
                if (isTag[state]) {
                    iScore[start][end][state] *= (1.0 + op.testOptions.depWeight);
                }
            }
        }

        if (floodTags && (!op.testOptions.noRecoveryTagging) && !(word == boundary)) {
            // if parse failed because of tag coverage, we put in all tags with
            // a score of -1000, by fiat.  You get here from the invocation of
            // parse(ls) inside parse(ls) *after* floodTags has been turned on.
            // Search above for "floodTags = true".
            if (dumpTagging) {
                EncodingPrintWriter.err.println("Flooding tags for " + wordIndex.get(word), "UTF-8");
            }
            for (int state = 0; state < numStates; state++) {
                if (isTag[state] && iScore[start][end][state] == Float.NEGATIVE_INFINITY) {
                    iScore[start][end][state] = -1000.0f;
                    narrowRExtent[start][state] = end; // EGOR: why not start????
                    narrowLExtent[end][state] = start;
                    wideRExtent[start][state] = end;
                    wideLExtent[end][state] = start;
                }
            }
        }

        // Apply unary rules in diagonal cells of chart
        if (spillGuts) {
            tick("Terminal Unary...");
        }
        // EGOR: I think there is something uncertain below...
        for (int state = 0; state < numStates; state++) {
            float iS = iScore[start][end][state]; // EGOR: here we also may get Parent nodes, not only tags...
            if (iS == Float.NEGATIVE_INFINITY) {
                continue;
            }
            UnaryRule[] unaries = ug.closedRulesByChild(state);
            for (UnaryRule ur : unaries) {
                int parentState = ur.parent;
                float pS = ur.score;
                float tot = iS + pS;
                if (tot > iScore[start][end][parentState]) {
                    iScore[start][end][parentState] = tot;
                    narrowRExtent[start][parentState] = end;
                    narrowLExtent[end][parentState] = start;
                    wideRExtent[start][parentState] = end;
                    wideLExtent[end][parentState] = start;
                }
            }
        }
        if (spillGuts) {
            tick("Next word...");
        }
    }

    @Override
    protected void createArrays(int length) {
        // zero out some stuff first in case we recently ran out of memory and are reallocating
        clearArrays();

        int numTags = tagIndex.size();
        // allocate just the parts of iScore and oScore used (end > start, etc.)
        //    System.out.println("initializing iScore arrays with length " + length + " and numStates " + numStates);
        iScore = new float[length + 1][length + 1][];
        for (int start = 0; start <= length; start++) {
            for (int end = start + 1; end <= length; end++) {
                iScore[start][end] = new float[numStates];
            }
        }
        //    System.out.println("finished initializing iScore arrays");
        if (op.doDep && !op.testOptions.useFastFactored) {
            //      System.out.println("initializing oScore arrays with length " + length + " and numStates " + numStates);
            oScore = new float[length + 1][length + 1][];
            for (int start = 0; start <= length; start++) {
                for (int end = start + 1; end <= length; end++) {
                    oScore[start][end] = new float[numStates];
                }
            }
            //      System.out.println("finished initializing oScore arrays");
        }
        iPossibleByL = new boolean[length + 1][numStates];
        iPossibleByR = new boolean[length + 1][numStates];
        narrowRExtent = new int[length + 1][numStates];
        wideRExtent = new int[length + 1][numStates];
        narrowLExtent = new int[length + 1][numStates];
        wideLExtent = new int[length + 1][numStates];
        if (op.doDep && !op.testOptions.useFastFactored) {
            oPossibleByL = new boolean[length + 1][numStates];
            oPossibleByR = new boolean[length + 1][numStates];

            oFilteredStart = new boolean[length + 1][numStates];
            oFilteredEnd = new boolean[length + 1][numStates];
        }
        tags = new boolean[length + 1][numTags];

        if (lengthNormalization) {
            wordsInSpan = new int[length + 1][length + 1][];
            for (int start = 0; start <= length; start++) {
                for (int end = start + 1; end <= length; end++) {
                    wordsInSpan[start][end] = new int[numStates];
                }
            }
        }
        //    System.out.println("ExhaustivePCFGParser constructor finished.");
    }

    @Override
    public boolean parse(List<? extends HasWord> sentence) {
        lr = null; // better nullPointer exception than silent error
        //System.out.println("is it a taggedword?" + (sentence.get(0) instanceof TaggedWord)); //debugging
        if (sentence != this.sentence) {
            this.sentence = sentence;
            floodTags = false;
        }
        if (op.testOptions.verbose) {
            Timing.tick("Starting pcfg parse.");
        }
        if (spillGuts) {
            tick("Starting PCFG parse...");
        }
        length = sentence.size();
        if (length > arraySize) {
            considerCreatingArrays(length);
        }
        int goal = stateIndex.indexOf(goalStr);
        if (op.testOptions.verbose) {
            // System.out.println(numStates + " states, " + goal + " is the goal state.");
            // System.err.println(new ArrayList(ug.coreRules.keySet()));
            System.err.print("Initializing PCFG...");
        }
        // map input words to words array (wordIndex ints)
        words = new int[length];
        beginOffsets = new int[length];
        endOffsets = new int[length];
        originalCoreLabels = new CoreLabel[length];
        int unk = 0;
        StringBuilder unkWords = new StringBuilder("[");
        // int unkIndex = wordIndex.size();
        for (int i = 0; i < length; i++) {
            String s = sentence.get(i).word();

            if (sentence.get(i) instanceof HasOffset) {
                HasOffset word = (HasOffset) sentence.get(i);
                beginOffsets[i] = word.beginPosition();
                endOffsets[i] = word.endPosition();
            } else {
                //Storing the positions of the word interstices
                //Account for single space between words
                beginOffsets[i] = ((i == 0) ? 0 : endOffsets[i - 1] + 1);
                endOffsets[i] = beginOffsets[i] + s.length();
            }

            if (sentence.get(i) instanceof CoreLabel) {
                originalCoreLabels[i] = (CoreLabel) sentence.get(i);
            }

            if (op.testOptions.verbose && (!wordIndex.contains(s) || !baseLexicon.isKnown(wordIndex.indexOf(s)))) {
                unk++;
                unkWords.append(' ');
                unkWords.append(s);
                unkWords.append(" { ");
                for (int jj = 0; jj < s.length(); jj++) {
                    char ch = s.charAt(jj);
                    unkWords.append(Character.getType(ch)).append(" ");
                }
                unkWords.append("}");
            }
            // TODO: really, add a new word?
            //words[i] = wordIndex.indexOf(s, unkIndex);
            //if (words[i] == unkIndex) {
            //  ++unkIndex;
            //}
            words[i] = wordIndex.indexOf(s, true);
           // System.out.println("add new word:" + words[i] + " - " + wordIndex.get(words[i]));

            //if (wordIndex.contains(s)) {
            //  words[i] = wordIndex.indexOf(s);
            //} else {
            //  words[i] = wordIndex.indexOf(Lexicon.UNKNOWN_WORD);
            //}

        }

        // initialize inside and outside score arrays
        if (spillGuts) {
            tick("Wiping arrays...");
        }
        for (int start = 0; start < length; start++) {
            for (int end = start + 1; end <= length; end++) {
                Arrays.fill(iScore[start][end], Float.NEGATIVE_INFINITY);
                if (op.doDep && !op.testOptions.useFastFactored) {
                    Arrays.fill(oScore[start][end], Float.NEGATIVE_INFINITY);
                }
                if (lengthNormalization) {
                    Arrays.fill(wordsInSpan[start][end], 1);
                }
            }
        }
        for (int loc = 0; loc <= length; loc++) {
            Arrays.fill(narrowLExtent[loc], -1); // the rightmost left with state s ending at i that we can get is the beginning
            Arrays.fill(wideLExtent[loc], length + 1); // the leftmost left with state s ending at i that we can get is the end
            Arrays.fill(narrowRExtent[loc], length + 1); // the leftmost right with state s starting at i that we can get is the end
            Arrays.fill(wideRExtent[loc], -1); // the rightmost right with state s starting at i that we can get is the beginning
        }
        // int puncTag = stateIndex.indexOf(".");
        // boolean lastIsPunc = false;
        if (op.testOptions.verbose) {
            Timing.tick("done.");
            unkWords.append(" ]");
            op.tlpParams.pw(System.err).println("Unknown words: " + unk + " " + unkWords);
            System.err.print("Starting filters...");
        }

        parserTracer = new ParserTracer((List<HasWord>) sentence,
                stateIndex,
                tagIndex,
                iScore,
                narrowLExtent,
                narrowRExtent,
                wideLExtent,
                wideRExtent,
                wordsInSpan);

        // do tags
        if (spillGuts) {
            tick("Tagging...");
        }
        initializeChart(sentence);
        //if (op.testOptions.outsideFilter)
        // buildOFilter();
        if (op.testOptions.verbose) {
            Timing.tick("done.");
            System.err.print("Starting insides...");
        }
        // do the inside probabilities
        doInsideScores();
        if (op.testOptions.verbose) {
            // insideTime += Timing.tick("done.");
            Timing.tick("done.");
            System.out.println("PCFG parsing " + length + " words (incl. stop): insideScore = " + iScore[0][length][goal]);
        }
        bestScore = iScore[0][length][goal];
        boolean succeeded = hasParse();
        if (op.testOptions.doRecovery && !succeeded && !floodTags) {
            floodTags = true; // sentence will try to reparse
            // ms: disabled message. this is annoying and it doesn't really provide much information
            // System.err.println("Trying recovery parse...");
            return parse(sentence);
        }
        if (!op.doDep || op.testOptions.useFastFactored) {
            return succeeded;
        }

        return succeeded;
    }

    @Override
    protected void doInsideScores() {
        final List<ParserConstraint> constraints = getConstraints();
        for (int diff = 2; diff <= length; diff++) {
            // usually stop one short because boundary symbol only combines
            // with whole sentence span
            for (int start = 0; start < ((diff == length) ? 1 : length - diff); start++) {
                if (spillGuts) {
                    tick("Binaries for span " + diff + " start " + start + " ...");
                }
                int end = start + diff;

                if (constraints != null) {
                    boolean skip = false;
                    for (ParserConstraint c : constraints) {
                        if ((start > c.start && start < c.end && end > c.end) || (end > c.start && end < c.end && start < c.start)) {
                            skip = true;
                            break;
                        }
                    }
                    if (skip) {
                        continue;
                    }
                }

                // 2011-11-26 jdk1.6: caching/hoisting a bunch of variables gives you about 15% speed up!
                // caching this saves a bit of time in the inner loop, maybe 1.8%
                int[] narrowRExtent_start = narrowRExtent[start];
                // caching this saved 2% in the inner loop
                int[] wideRExtent_start = wideRExtent[start];
                int[] narrowLExtent_end = narrowLExtent[end];
                int[] wideLExtent_end = wideLExtent[end];
                float[][] iScore_start = iScore[start];
                float[] iScore_start_end = iScore_start[end];

                for (int leftState = 0; leftState < numStates; leftState++) {
                    int narrowR = narrowRExtent_start[leftState];
                    boolean iPossibleL = (narrowR < end); // can this left constituent leave space for a right constituent?
                    if (!iPossibleL) {
                        continue;
                    }
                    BinaryRule[] leftRules = bg.splitRulesWithLC(leftState);
                    //      if (spillGuts) System.out.println("Found " + leftRules.length + " left rules for state " + stateIndex.get(leftState));
                    for (BinaryRule rule : leftRules) {
                        int rightChild = rule.rightChild;
                        int narrowL = narrowLExtent_end[rightChild];
                        boolean iPossibleR = (narrowL >= narrowR); // can this right constituent fit next to the left constituent?
                        if (!iPossibleR) {
                            continue;
                        }
                        int min2 = wideLExtent_end[rightChild];
                        int min = (narrowR > min2 ? narrowR : min2);
                        // Erik Frey 2009-12-17: This is unnecessary: narrowR is <= narrowL (established in previous check) and wideLExtent[e][r] is always <= narrowLExtent[e][r] by design, so the check will never evaluate true.
                        // if (min > narrowL) { // can this right constituent stretch far enough to reach the left constituent?
                        //   continue;
                        // }
                        int max1 = wideRExtent_start[leftState];
                        int max = (max1 < narrowL ? max1 : narrowL);
                        if (min > max) { // can this left constituent stretch far enough to reach the right constituent?
                            continue;
                        }
                        float pS = rule.score;
                        int parentState = rule.parent;
                        float oldIScore = iScore_start_end[parentState];
                        float bestIScore = oldIScore;
                        boolean foundBetter;  // always set below for this rule
                        //System.out.println("Min "+min+" max "+max+" start "+start+" end "+end);

                        if (!lengthNormalization) {
                            // find the split that can use this rule to make the max score
                            for (int split = min; split <= max; split++) {

                                if (constraints != null) {
                                    boolean skip = false;
                                    for (ParserConstraint c : constraints) {
                                        if (((start < c.start && end >= c.end) || (start <= c.start && end > c.end)) && split > c.start && split < c.end) {
                                            skip = true;
                                            break;
                                        }
                                        if ((start == c.start && split == c.end)) {
                                            String tag = stateIndex.get(leftState);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                        if ((split == c.start && end == c.end)) {
                                            String tag = stateIndex.get(rightChild);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (skip) {
                                        continue;
                                    }
                                }

                                float lS = iScore_start[split][leftState];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightChild];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                if (spillGuts) {
                                    System.err.println("Rule " + rule + " over [" + start + "," + end + ") has log score " + tot + " from L[" + stateIndex.get(leftState) + "=" + leftState + "] = " + lS + " R[" + stateIndex.get(rightChild) + "=" + rightChild + "] =  " + rS);
                                }
                                if (tot > bestIScore) {
                                    bestIScore = tot;
                                }
                            } // for split point
                            foundBetter = bestIScore > oldIScore;
                        } else {
                            // find split that uses this rule to make the max *length normalized* score
                            int bestWordsInSpan = wordsInSpan[start][end][parentState];
                            float oldNormIScore = oldIScore / bestWordsInSpan;
                            float bestNormIScore = oldNormIScore;

                            for (int split = min; split <= max; split++) {
                                float lS = iScore_start[split][leftState];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightChild];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                int newWordsInSpan = wordsInSpan[start][split][leftState] + wordsInSpan[split][end][rightChild];
                                float normTot = tot / newWordsInSpan;
                                if (normTot > bestNormIScore) {
                                    bestIScore = tot;
                                    bestNormIScore = normTot;
                                    bestWordsInSpan = newWordsInSpan;
                                }
                            } // for split point
                            foundBetter = bestNormIScore > oldNormIScore;
                            if (foundBetter) {
                                wordsInSpan[start][end][parentState] = bestWordsInSpan;
                            }
                        } // fi lengthNormalization
                        if (foundBetter) { // this way of making "parentState" is better than previous
                            iScore_start_end[parentState] = bestIScore;

                            if (spillGuts)
                                System.err.println("Could build " + stateIndex.get(parentState) + " from " + start + " to " + end + " score " + bestIScore);
                            if (oldIScore == Float.NEGATIVE_INFINITY) {
                                if (start > narrowLExtent_end[parentState]) {
                                    narrowLExtent_end[parentState] = start; // EGOR: why not split!? There is no any difference between wideLExtent and narrowLExtent .. ???
                                    wideLExtent_end[parentState] = start;
                                } else {
                                    if (start < wideLExtent_end[parentState]) {
                                        wideLExtent_end[parentState] = start;
                                    }
                                }
                                if (end < narrowRExtent_start[parentState]) {
                                    narrowRExtent_start[parentState] = end;
                                    wideRExtent_start[parentState] = end;
                                } else {
                                    if (end > wideRExtent_start[parentState]) {
                                        wideRExtent_start[parentState] = end;
                                    }
                                }
                            }
                        } // end if foundBetter
                    } // end for leftRules
                } // end for leftState
                // do right restricted rules
                for (int rightState = 0; rightState < numStates; rightState++) {
                    int narrowL = narrowLExtent_end[rightState];
                    boolean iPossibleR = (narrowL > start);
                    if (!iPossibleR) {
                        continue;
                    }
                    BinaryRule[] rightRules = bg.splitRulesWithRC(rightState);
                    //      if (spillGuts) System.out.println("Found " + rightRules.length + " right rules for state " + stateIndex.get(rightState));
                    for (BinaryRule rule : rightRules) {
                        //      if (spillGuts) System.out.println("Considering rule for " + start + " to " + end + ": " + rightRules[i]);

                        int leftChild = rule.leftChild;
                        int narrowR = narrowRExtent_start[leftChild];
                        boolean iPossibleL = (narrowR <= narrowL);
                        if (!iPossibleL) {
                            continue;
                        }
                        int min2 = wideLExtent_end[rightState];
                        int min = (narrowR > min2 ? narrowR : min2);
                        // Erik Frey 2009-12-17: This is unnecessary: narrowR is <= narrowL (established in previous check) and wideLExtent[e][r] is always <= narrowLExtent[e][r] by design, so the check will never evaluate true.
                        // if (min > narrowL) {
                        //   continue;
                        // }
                        int max1 = wideRExtent_start[leftChild];
                        int max = (max1 < narrowL ? max1 : narrowL);
                        if (min > max) {
                            continue;
                        }
                        float pS = rule.score;
                        int parentState = rule.parent;
                        float oldIScore = iScore_start_end[parentState];
                        float bestIScore = oldIScore;
                        boolean foundBetter; // always initialized below
                        //System.out.println("Start "+start+" end "+end+" min "+min+" max "+max);
                        if (!lengthNormalization) {
                            // find the split that can use this rule to make the max score
                            for (int split = min; split <= max; split++) {

                                if (constraints != null) {
                                    boolean skip = false;
                                    for (ParserConstraint c : constraints) {
                                        if (((start < c.start && end >= c.end) || (start <= c.start && end > c.end)) && split > c.start && split < c.end) {
                                            skip = true;
                                            break;
                                        }
                                        if ((start == c.start && split == c.end)) {
                                            String tag = stateIndex.get(leftChild);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                //if (!tag.startsWith(c.state+"^")) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                        if ((split == c.start && end == c.end)) {
                                            String tag = stateIndex.get(rightState);
                                            Matcher m = c.state.matcher(tag);
                                            if (!m.matches()) {
                                                //if (!tag.startsWith(c.state+"^")) {
                                                skip = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (skip) {
                                        continue;
                                    }
                                }

                                float lS = iScore_start[split][leftChild];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightState];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                if (tot > bestIScore) {
                                    bestIScore = tot;
                                }
                            } // end for split // EGOR: Why we do not store any information about split point??????????????
                            foundBetter = bestIScore > oldIScore;
                        } else {
                            // find split that uses this rule to make the max *length normalized* score
                            int bestWordsInSpan = wordsInSpan[start][end][parentState];
                            float oldNormIScore = oldIScore / bestWordsInSpan;
                            float bestNormIScore = oldNormIScore;
                            for (int split = min; split <= max; split++) {
                                float lS = iScore_start[split][leftChild];
                                if (lS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float rS = iScore[split][end][rightState];
                                if (rS == Float.NEGATIVE_INFINITY) {
                                    continue;
                                }
                                float tot = pS + lS + rS;
                                int newWordsInSpan = wordsInSpan[start][split][leftChild] + wordsInSpan[split][end][rightState];
                                float normTot = tot / newWordsInSpan;
                                if (normTot > bestNormIScore) {
                                    bestIScore = tot;
                                    bestNormIScore = normTot;
                                    bestWordsInSpan = newWordsInSpan;
                                }
                            } // end for split
                            foundBetter = bestNormIScore > oldNormIScore;
                            if (foundBetter) {
                                wordsInSpan[start][end][parentState] = bestWordsInSpan;
                            }
                        } // end if lengthNormalization
                        if (foundBetter) { // this way of making "parentState" is better than previous
                            iScore_start_end[parentState] = bestIScore;
                            if (spillGuts)
                                System.err.println("Could build " + stateIndex.get(parentState) + " from " + start + " to " + end + " with score " + bestIScore);
                            if (oldIScore == Float.NEGATIVE_INFINITY) {
                                if (start > narrowLExtent_end[parentState]) {
                                    narrowLExtent_end[parentState] = start;
                                    wideLExtent_end[parentState] = start;
                                } else {
                                    if (start < wideLExtent_end[parentState]) {
                                        wideLExtent_end[parentState] = start;
                                    }
                                }
                                if (end < narrowRExtent_start[parentState]) {
                                    narrowRExtent_start[parentState] = end;
                                    wideRExtent_start[parentState] = end;
                                } else {
                                    if (end > wideRExtent_start[parentState]) {
                                        wideRExtent_start[parentState] = end;
                                    }
                                }
                            }
                        } // end if foundBetter
                    } // for rightRules
                } // for rightState
                if (spillGuts) {
                    tick("Unaries for span " + diff + "...");
                }
                // do unary rules -- one could promote this loop and put start inside
                for (int state = 0; state < numStates; state++) {
                    float iS = iScore_start_end[state];
                    if (iS == Float.NEGATIVE_INFINITY) {
                        continue;
                    }

                    UnaryRule[] unaries = ug.closedRulesByChild(state);
                    for (UnaryRule ur : unaries) {

                        if (constraints != null) {
                            boolean skip = false;
                            for (ParserConstraint c : constraints) {
                                if ((start == c.start && end == c.end)) {
                                    String tag = stateIndex.get(ur.parent);
                                    Matcher m = c.state.matcher(tag);
                                    if (!m.matches()) {
                                        //if (!tag.startsWith(c.state+"^")) {
                                        skip = true;
                                        break;
                                    }
                                }
                            }
                            if (skip) {
                                continue;
                            }
                        }

                        int parentState = ur.parent;
                        float pS = ur.score;
                        float tot = iS + pS;
                        float cur = iScore_start_end[parentState];
                        boolean foundBetter;  // always set below
                        if (lengthNormalization) {
                            int totWordsInSpan = wordsInSpan[start][end][state];
                            float normTot = tot / totWordsInSpan;
                            int curWordsInSpan = wordsInSpan[start][end][parentState];
                            float normCur = cur / curWordsInSpan;
                            foundBetter = normTot > normCur;
                            if (foundBetter) {
                                wordsInSpan[start][end][parentState] = wordsInSpan[start][end][state];
                            }
                        } else {
                            foundBetter = (tot > cur);
                        }
                        if (foundBetter) {
                            if (spillGuts)
                                System.err.println("Could build " + stateIndex.get(parentState) + " from " + start + " to " + end + " with score " + tot);
                            iScore_start_end[parentState] = tot;
                            if (cur == Float.NEGATIVE_INFINITY) {
                                if (start > narrowLExtent_end[parentState]) {
                                    narrowLExtent_end[parentState] = start;
                                    wideLExtent_end[parentState] = start;
                                } else {
                                    if (start < wideLExtent_end[parentState]) {
                                        wideLExtent_end[parentState] = start;
                                    }
                                }
                                if (end < narrowRExtent_start[parentState]) {
                                    narrowRExtent_start[parentState] = end;
                                    wideRExtent_start[parentState] = end;
                                } else {
                                    if (end > wideRExtent_start[parentState]) {
                                        wideRExtent_start[parentState] = end;
                                    }
                                }
                            }
                        } // end if foundBetter
                    } // for UnaryRule r
                } // for unary rules
            } // for start
        } // for diff (i.e., span)
    } // end doInsideScores()

    @Override
    protected Tree extractBestParse(int goal, int start, int end) {
        // System.out.println("extractBestParse_2" + end);
        // find source of inside score
        // no backtraces so we can speed up the parsing for its primary use
        double bestScore = iScore[start][end][goal];
        double normBestScore = lengthNormalization ? (bestScore / wordsInSpan[start][end][goal]) : bestScore;
        String goalStr = stateIndex.get(goal);
        // System.err.println("Searching for "+goalStr+" from "+start+" to "+end+" scored "+bestScore +
        //                " tagNumberer.hasSeen: " + tagNumberer.hasSeen(goalStr));
        // check tags
        if (end - start <= maxSpanForTags && tagIndex.contains(goalStr)) {
            if (maxSpanForTags > 1) {

                Tree wordNode = null;
                if (sentence != null) {
                    StringBuilder word = new StringBuilder();
                    // System.out.println("start - end:" + start + "-" + end);
                   // System.out.println(sentence.toString());
                    for (int i = start; i < end; i++) {
                        if (sentence.get(i) instanceof HasWord) {
                            HasWord cl = (HasWord) sentence.get(i);

                            word.append(cl.word());
                        } else {
                            word.append(sentence.get(i).toString());
                        }
                        //System.out.println("word:" + word);
                    }
                    //start_collocation.add(start);
                    // end_collocation.add(end);
                    start_end_collocation_index.put(start, end);

                  //  System.out.println("stringBuilder:" + word.toString());
                  //  System.out.println("start_end_collocation_index:" + start_end_collocation_index);
                    wordNode = tf.newLeaf(word.toString());

                } else if (lr != null) {
                    List<LatticeEdge> latticeEdges = lr.getEdgesOverSpan(start, end);
                    for (LatticeEdge edge : latticeEdges) {
                        IntTaggedWord itw = new IntTaggedWord(edge.word, stateIndex.get(goal), wordIndex, tagIndex);

                        float tagScore = (floodTags) ? -1000.0f : baseLexicon.score(itw, start, edge.word);
                        if (matches(bestScore, tagScore + (float) edge.weight)) {
                            wordNode = tf.newLeaf(edge.word);
                            if (wordNode.label() instanceof CoreLabel) {
                                CoreLabel cl = (CoreLabel) wordNode.label();
                                cl.setBeginPosition(start);
                                cl.setEndPosition(end);
                            }
                            break;
                        }
                    }
                    if (wordNode == null) {
                        throw new RuntimeException("could not find matching word from lattice in parse reconstruction");
                    }

                } else {
                    throw new RuntimeException("attempt to get word when sentence and lattice are null!");
                }
                Tree tagNode = tf.newTreeNode(goalStr, Collections.singletonList(wordNode));
                tagNode.setScore(bestScore);
                return tagNode;
            } else {  // normal lexicon is single words case
                IntTaggedWord tagging = new IntTaggedWord(words[start], tagIndex.indexOf(goalStr));
                float tagScore = baseLexicon.score(tagging, start, wordIndex.get(words[start]));
                if (tagScore > Float.NEGATIVE_INFINITY || floodTags) {
                    // return a pre-terminal tree
                    //    System.out.println("start_getCorelabel - " + start + ", word - " +  words[start] + " - " + wordIndex.get(words[start]));
                    CoreLabel terminalLabel = getCoreLabel(start);

                    Tree wordNode = tf.newLeaf(terminalLabel);
                    Tree tagNode = tf.newTreeNode(goalStr, Collections.singletonList(wordNode));
                    tagNode.setScore(bestScore);
                    // todo cdm Sept 2011: The next line fails. Should it be testing and filling in tagNode.label()?
                    if (tagNode instanceof HasTag)
                        ((HasTag) tagNode.label()).setTag(goalStr);

                    return tagNode;
                }
            }
        }
        // check binaries first
        for (int split = start + 1; split < end; split++) {
            for (Iterator<BinaryRule> binaryI = bg.ruleIteratorByParent(goal); binaryI.hasNext(); ) {
                BinaryRule br = binaryI.next();
                double score = br.score + iScore[start][split][br.leftChild] + iScore[split][end][br.rightChild];
                boolean matches;
                if (lengthNormalization) {
                    double normScore = score / (wordsInSpan[start][split][br.leftChild] + wordsInSpan[split][end][br.rightChild]);
                    matches = matches(normScore, normBestScore);
                } else {
                    matches = matches(score, bestScore);
                }
                if (matches) {
                    // build binary split
                    Tree leftChildTree = extractBestParse(br.leftChild, start, split);
                    Tree rightChildTree = extractBestParse(br.rightChild, split, end);
                    List<Tree> children = new ArrayList<Tree>();
                    children.add(leftChildTree);
                    children.add(rightChildTree);
                    Tree result = tf.newTreeNode(goalStr, children);
                    result.setScore(score);
                    // System.err.println("    Found Binary node: "+result);
                    return result;
                }
            }
        }
        // check unaries
        // note that even though we parse with the unary-closed grammar, we can
        // extract the best parse with the non-unary-closed grammar, since all
        // the intermediate states in the chain must have been built, and hence
        // we can exploit the sparser space and reconstruct the full tree as we go.
        // for (Iterator<UnaryRule> unaryI = ug.closedRuleIteratorByParent(goal); unaryI.hasNext(); ) {
        for (Iterator<UnaryRule> unaryI = ug.ruleIteratorByParent(goal); unaryI.hasNext(); ) {
            UnaryRule ur = unaryI.next();
            // System.err.println("  Trying " + ur + " dtr score: " + iScore[start][end][ur.child]);
            double score = ur.score + iScore[start][end][ur.child];
            boolean matches;
            if (lengthNormalization) {
                double normScore = score / wordsInSpan[start][end][ur.child];
                matches = matches(normScore, normBestScore);
            } else {
                matches = matches(score, bestScore);
            }
            if (ur.child != ur.parent && matches) {
                // build unary
                Tree childTree = extractBestParse(ur.child, start, end);
                Tree result = tf.newTreeNode(goalStr, Collections.singletonList(childTree));
                // System.err.println("    Matched!  Unary node: "+result);
                result.setScore(score);
                return result;
            }
        }
        System.err.println("Warning: no parse found in ExhaustivePCFGParser.extractBestParse: failing on: [" + start + ", " + end + "] looking for " + goalStr);
        return null;
    }


   // Irina Daniel
   // method use when find kBestParse
   // add information about collocation
    @Override
    protected Tree getTree(Vertex v, int k, int kPrime) {
        lazyKthBest(v, k, kPrime);
        String goalStr = stateIndex.get(v.goal);
        int start = v.start;

        Tree wordNode = null;
        //System.out.println("getTree_start:" + start);


        List<Derivation> dHatV = dHat.get(v);
        // System.out.println("isTag[v.goal]:" + isTag[v.goal]);
        if (isTag[v.goal]) {
            if (start_end_collocation_index.containsKey(start)) {
                if (sentence != null) {
                    StringBuilder word = new StringBuilder();
                    //combine words of collocation
                    for (int i = start; i < start_end_collocation_index.get(start); i++) {
                        if (sentence.get(i) instanceof HasWord) {
                            HasWord cl = (HasWord) sentence.get(i);
                            word.append(cl.word());
                        } else {
                            word.append(sentence.get(i).toString());
                        }
                    }
                    //System.out.println("stringBuilder:" + word.toString());
                    wordNode = tf.newLeaf(word.toString());
                    Tree tagNode = tf.newTreeNode(goalStr, Collections.singletonList(wordNode));
                    if (tagNode instanceof HasTag)
                        ((HasTag) tagNode.label()).setTag(goalStr);

                    return tagNode;
                }
            } else {
                IntTaggedWord tagging = new IntTaggedWord(words[start], tagIndex.indexOf(goalStr));
                float tagScore = lex.score(tagging, start, wordIndex.get(words[start]));
                if (tagScore > Float.NEGATIVE_INFINITY || floodTags) {
                    // return a pre-terminal tree

                    CoreLabel terminalLabel = getCoreLabel(start);
                    //  wordNode = tf.newLeaf(word.toString());
                    wordNode = tf.newLeaf(terminalLabel);

                    Tree tagNode = tf.newTreeNode(goalStr, Collections.singletonList(wordNode));
                    if (tagNode instanceof HasTag)
                        ((HasTag) tagNode.label()).setTag(goalStr);

                    return tagNode;
                } else {
                    assert false;
                }
            }

        }

        if (k - 1 >= dHatV.size()) {
            return null;
        }
        Derivation d = dHatV.get(k - 1);
        List<Tree> children = new ArrayList<Tree>();
        //  System.out.println(d.arc.size());
        for (int i = 0; i < d.arc.size(); i++) {

            Vertex child = d.arc.tails.get(i);

            Tree t = getTree(child, d.j.get(i), kPrime);
            assert (t != null);
            children.add(t);
        }

        return tf.newTreeNode(goalStr, children);
    }
}
