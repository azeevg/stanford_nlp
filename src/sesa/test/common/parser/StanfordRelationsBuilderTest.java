package sesa.test.common.parser;

import edu.spbstu.egor.SentenceGetter;
import edu.stanford.nlp.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import sesa.common.builder.BuilderException;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.semantics.SemanticRelation;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Ira on 18.05.2016.
 */
public class StanfordRelationsBuilderTest {

    public final Logger log = LogManager.getLogger(StanfordRelationsBuilderTest.class);
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
    private String languageModel = "./data/grammar/englishFactored.ser.gz";
    ObjectInputStream inputStream;
    StanfordDataModule dataModule;
    StanfordRelationsBuilder builder;

    @Before
    public void init()throws IOException{
        inputStream = IOUtils.readStreamFromString(languageModel);
        dataModule = new BasicDataModule();
        builder = new StanfordRelationsBuilder(languageModel,dataModule);
    }

    @Test
    public void testGetGrammaticalRelations() throws IOException, BuilderException, SQLException, ClassNotFoundException {
        builder.setIsModCKY(false);
        SentenceGetter sentenceGetter = new SentenceGetter("data/test.txt");
        ArrayList<ArrayList<SemanticRelation>> sesaGs = builder.getGrammaticalTextStructure(sentenceGetter);
        for(ArrayList<SemanticRelation> semanticRelations: sesaGs){
            System.out.println("##-----");
            for(SemanticRelation sr: semanticRelations){
                System.out.println(sr);
            }
        }
    }

    @Test
    public void testGetGrammaticalRelationsUseModCKY() throws IOException, BuilderException, SQLException, ClassNotFoundException {
        builder.setIsModCKY(true);
        SentenceGetter sentenceGetter = new SentenceGetter("data/test.txt");
        ArrayList<ArrayList<SemanticRelation>> sesaGs = builder.getGrammaticalTextStructure(sentenceGetter);
        for(ArrayList<SemanticRelation> semanticRelations: sesaGs){
            System.out.println("##-----");
            for(SemanticRelation sr: semanticRelations){
                System.out.println(sr);
            }
        }
    }
}
