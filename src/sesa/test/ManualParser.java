package sesa.test;

import sesa.common.semantics.SemanticRelation;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.io.FileUtility;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Deprecated
public class ManualParser {



    public List<SentenceTree> getSentences(String fileName) {

        List<SentenceTree> trees = new LinkedList<SentenceTree>();
        String text = FileUtility.readAllText(fileName);
        String[] sentences = text.split("\\.!!");

        List<String> lines = new LinkedList<String>();
        for (String sentence : sentences) {
            Pattern syntaxPattern = Pattern.compile("\\{([^\\}]+)\\}\\{([^\\}]+)\\}");
            Matcher m = syntaxPattern.matcher(sentence);
            SentenceTree tree = new SentenceTree();
            List<SemanticRelation> relations = new LinkedList<SemanticRelation>();

            List<String> src = new LinkedList<String>();
            List<String> repl = new LinkedList<String>();
            String filteredSentence = new String(sentence);
            while (m.find()) {
                String g = m.group();
                String g1 = m.group(1);
                String g2 = m.group(2);
                //relations.add(new SemanticRelation(g1, g2));

                src.add(g);
                repl.add(g2);
            }

            for (int i = 0; i < src.size(); i++) {
                filteredSentence = filteredSentence.replace(src.get(i), repl.get(i));
            }

            filteredSentence = filteredSentence.replaceAll("!!", "");
            System.out.println(filteredSentence);
            tree.setSemanticGroups(null);
            tree.setSentence(filteredSentence);
            trees.add(tree);
        }
        return trees;
    }
}
