package sesa.common.database.dao.dictionary;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.CommonDao;
import sesa.common.model.processing.sentence.SentenceTriplet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Ira on 21.05.2016.
 */
public class SentenceTripletDao extends CommonDao {
    public final Logger log = LogManager.getLogger(SentenceTripletDao.class);
    private String collectionName = "triplet_computer_dict";


    public SentenceTripletDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public void addToTripletCollections(SentenceTriplet triplet, int idDef) {

        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();


            String query = "INSERT INTO `" + collectionName + "` (subject, predicat, object, DEF_ID) VALUES(?,?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, triplet.getSubject());
            ps.setString(2, triplet.getPredicate());
            ps.setString(3, triplet.getObject());
            ps.setInt(4, idDef);
            ps.execute();

            log.info("Added new triplet: " + triplet.toString() + " ;to table:" + collectionName);


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }


    }
}
