package sesa.tools.semantic;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ira on 07.06.2016.
 */

// dto change to entity

/**
 * Utility class for AutoGenerateReference
 */
public class AutoReference {

    private int idTerm;
    private String termDefinition;
    private int idDefinition;
    private Set<String> termLink;

    public AutoReference() {
        termLink = new HashSet<String>();
    }

    public int getIdTerm() {
        return idTerm;
    }

    public void setIdTerm(int idTerm) {
        this.idTerm = idTerm;
    }

    public String getTermDefinition() {
        return termDefinition;
    }

    public void setTermDefinition(String termDefinition) {
        this.termDefinition = termDefinition;
    }

    public Set<String> getTermLink() {
        return termLink;
    }

    public void setTermLink(List<String> termLink) {
        this.termLink.addAll(termLink);

    }

    public void setIdDefinition(int idDefinition) {
        this.idDefinition = idDefinition;
    }

    public int getIdDefinition() {
        return idDefinition;
    }

    @Override
    public String toString() {
        return "AutoReference{" +
                "idTerm=" + idTerm +
                ", termDefinition='" + termDefinition + '\'' +
                ", termLink=" + termLink +
                '}';
    }


}
