package sesa.common.database.dao.dictionary;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.CommonDao;
import sesa.common.database.entity.SemanticGroupEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class contains method for work with table from db, which contains semantic group
 * db.table = "semantic_structure"
 * Created by Ira on 01.06.2016.
 */
public class SemanticStructureDao extends CommonDao {

    public final Logger log = LogManager.getLogger(SemanticStructureDao.class);

    private String collectionName = "semantic_structure";

    public SemanticStructureDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public void addSemanticGroupStructure(SemanticGroupEntity dto) {

        PreparedStatement ps = null;
        Connection connection = null;
        log.info("Add terms to " + collectionName);
        int i = 0;
        boolean added = true;
        try {
            connection = cp.takeConnection();

            for (Integer idLink : dto.getTermLink()) {
                log.debug("Added new row: " + dto.toString() + "to table:" + collectionName);
                if (idLink == null) {
                    added = false;
                    continue;
                }
                // id sentence / id definition
                String query = "INSERT INTO `" + collectionName + "` (`ID_TERM`, `ID_GROUP`, `ID_SENTENCE`, `ID_LINK_TERM`, `TEXT`) VALUES(?,?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setInt(1, dto.getIdTerm());
                ps.setString(2, dto.getGroupId());
                ps.setInt(3, dto.getSentenceId());

                ps.setInt(4, idLink);
                ps.setString(5, dto.getText_group());
                ps.execute();
                log.debug("Added completed");
                i++;
                added = true;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            if (added) {
                cp.closeConnection(connection, ps);
            } else {
                cp.closeConnection(connection);
            }
        }

        log.info("Added __" + i + "__ rows ");
    }

    public SemanticGroupEntity getSemanticGroupDtoByTermID(int idTerm) {
        SemanticGroupEntity dto = new SemanticGroupEntity();
        List<Integer> listLinks = new ArrayList<Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `ID_TERM` = " + idTerm;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();
            String idGroup = null;
            int idSentence = 0;
            String text = null;
            int i = 0;
            boolean isResult = false;
            while (resultSet.next()) {
                if (i == 0) {
                    idGroup = resultSet.getString("ID_GROUP");
                    idSentence = resultSet.getInt("ID_SENTENCE");
                    text = resultSet.getString("TEXT");
                }
                int idLinkTerm = resultSet.getInt("ID_LINK_TERM");
                listLinks.add(idLinkTerm);
                i++;
                isResult = true;
            }
            if (isResult) {
                dto.setIdTerm(idTerm);
                dto.setGroupId(idGroup);
                dto.setSentenceId(idSentence);
                dto.setText_group(text);
                dto.setTermLink(listLinks);
            } else {
                dto = null;
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return dto;
    }

    //TODO change method ( not optimal)
    public List<SemanticGroupEntity> getListSemanticGroupDtoByTermID(int idTerm) {
        List<SemanticGroupEntity> list = new ArrayList<SemanticGroupEntity>();

        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `ID_TERM` = " + idTerm;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            List<Integer> listLinks = new ArrayList<Integer>();

            String idGroup_prev = null;
            int idSentence = 0;
            String text = null;

            boolean firstRow = true;
            while (resultSet.next()) {
                String idGroup = resultSet.getString("ID_GROUP");
                if (idGroup != null) {
                    if (firstRow) {
                        idGroup_prev = idGroup;
                        firstRow = false;
                    }
                    if (idGroup.equals(idGroup_prev)) {
                        idSentence = resultSet.getInt("ID_SENTENCE");
                        text = resultSet.getString("TEXT");
                        int idLinkTerm = resultSet.getInt("ID_LINK_TERM");
                        listLinks.add(idLinkTerm);
                    } else {
                        SemanticGroupEntity dto = new SemanticGroupEntity();
                        dto.setIdTerm(idTerm);
                        dto.setGroupId(idGroup_prev);
                        dto.setSentenceId(idSentence);
                        dto.setText_group(text);
                        dto.setTermLink(listLinks);

                        list.add(dto);
                        listLinks = new ArrayList<Integer>();

                        idSentence = resultSet.getInt("ID_SENTENCE");
                        text = resultSet.getString("TEXT");
                        int idLinkTerm = resultSet.getInt("ID_LINK_TERM");
                        listLinks.add(idLinkTerm);
                        idGroup_prev = idGroup;
                    }
                }
            }
            SemanticGroupEntity dto = new SemanticGroupEntity();
            dto.setIdTerm(idTerm);
            dto.setGroupId(idGroup_prev);
            dto.setSentenceId(idSentence);
            dto.setText_group(text);
            dto.setTermLink(listLinks);

            list.add(dto);


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return list;
    }

    /**
     * Method found are not repeated ids in table and calculates their number
     * @return number of node in semantic graph
     */
    public int getNumberNode() {
        Set<Integer> set = new HashSet<Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "`";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();
            while (resultSet.next()) {
                set.add(resultSet.getInt("ID_TERM"));
                set.add(resultSet.getInt("ID_LINK_TERM"));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return set.size();
    }
}
