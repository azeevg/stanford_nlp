package sesa.common.utility.logging;

import java.text.DecimalFormat;

public class ReportUtility extends LoggingUtility {
    private DecimalFormat df = new DecimalFormat("00.00");
    private String title = "";
    private long totalBytes;
    private long timeStarted = System.currentTimeMillis();
    private double lastReported;
    private long processedBytes;

    public ReportUtility(String title, long totalBytes) {
        this.title = title;
        this.totalBytes = totalBytes;
        info("Starting <" + title + "> process...");
    }

    public void addProcessed(long bytes) {
        processedBytes += bytes;
    }

    private long addUtility() {
        return ((long) ((0.97 * ((double) processedBytes)) / ((double) (System.currentTimeMillis() - timeStarted))));
    }

    private long bytes2Megabytes(long bytes) {
        return bytes / 1048576;
    }

    /**
     * �������������� ����������� � ������
     * @param milliseconds
     * @return
     */
    private String milliseconds2String(long milliseconds) {
        long seconds = (milliseconds / 1000) % 60;
        long minutes = (milliseconds / 60000) % 60;
        long hours = (milliseconds / 3600000) % 24;
        long days = (milliseconds / 86400000);

        String result = "";

        if (days > 0)
            result += days + "d ";

        if (hours > 0)
            result += hours + "h ";

        if (minutes > 0)
            result += minutes + "m ";

        if (seconds > 0)
            result += seconds + "s";

        return result;
    }

    /**
     * ������ ������� ���������� ��������
     * @param percent
     * @return
     */
    private String getETA(double percent) {
        if (percent <= 0.0)
            return "";

        double timePerPercent = ((double) (System.currentTimeMillis() - timeStarted)) / percent;
        long millisecondsETA = (long) ((100.0 - percent) * timePerPercent);
        return milliseconds2String(millisecondsETA);
    }

    public void report() {
        double percent = 100.0 * ((double) processedBytes) / ((double) totalBytes);

        double step = 0.1;
        if ((percent - lastReported) > step) {
            lastReported = percent;

            info(title + ": " + df.format(percent) + "%; " + addUtility() + "KB/s; [" + bytes2Megabytes(processedBytes) + "m of " + bytes2Megabytes(totalBytes) + "m] ETA: " + getETA(percent));
        }
    }
}
