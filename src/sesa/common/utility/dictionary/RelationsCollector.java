package sesa.common.utility.dictionary;

import edu.spbstu.appmath.collocmatch.stem.StemUtils;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.dao.dictionary.TermLinksDao;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.min;

/**
 * Created by gleb on 02.06.17.
 */
public class RelationsCollector {
    private final TermDao termDao;
    private final Map<String, Integer> mixedTerms;
    private final Map<String, Integer> upperTerms;
    private final Map<String, Integer> lowerTerms;
    private final Map<String, Integer> allTerms;
    private final Map<String, Integer> originalTerms;
    private final List<Integer> termsIds;

    public RelationsCollector() throws SQLException, ClassNotFoundException {
        this.termDao = new TermDao();
        originalTerms = termDao.getAllTerms();
        upperTerms = new HashMap<>();
        lowerTerms = new HashMap<>();
        mixedTerms = new HashMap<>();
        allTerms = new HashMap<>();
        termsIds = new ArrayList<>();

        originalTerms.entrySet().forEach(entry -> {
            int bracket = entry.getKey().indexOf("(");
            termsIds.add(entry.getValue());

            String term;

            if (bracket == -1) {
                term = entry.getKey().trim();
            } else {
                term = entry.getKey().substring(0, bracket).trim();
            }

            switch (classifyTerm(term)) {
                case LOWER:
                    lowerTerms.put(term, entry.getValue());
                    break;
                case UPPER:
                    upperTerms.put(term, entry.getValue());
                    break;
                case MIXED:
                case NO_LETTERS:
                    mixedTerms.put(term, entry.getValue());
            }

            String lowerCaseTerm = term.toLowerCase();

            if (!allTerms.containsKey(lowerCaseTerm))
                allTerms.put(lowerCaseTerm, entry.getValue());
        });

    }

    public static TermType classifyTerm(String term) {
        String filtered = term.replaceAll("[\\W0-9]+", "");

        if (filtered.length() == 0)
            return TermType.NO_LETTERS;
        if (filtered.matches("[a-z]+"))
            return TermType.LOWER;
        if (filtered.matches("[A-Z]+"))
            return TermType.UPPER;

        return TermType.MIXED;
    }

    public List<Integer> getTermsIds() {
        return termsIds;
    }

    public Map<Integer, Map<String, Integer>> collectRelatedTerms() throws SQLException, ClassNotFoundException {
        return collectRelatedTerms(Long.MAX_VALUE);
    }

    public Map<Integer, Map<String, Integer>> collectRelatedTerms(long limit) throws SQLException, ClassNotFoundException {
        final Map<Integer, Map<String, Integer>> relatedTerms = collectReferencedTerms();

        List<TermDefinitionEntity> definitions = new TermDefinitionDao().getAllTermDefinition();

        definitions.stream()
                .limit(limit)
                .forEach(termDefinitionEntity -> {
                    if (termDefinitionEntity.getDefinition().equals("???") || termDefinitionEntity.getDefinition().length() == 0) {
                        if (!relatedTerms.containsKey(termDefinitionEntity.getIdTerm()))
                            relatedTerms.put(termDefinitionEntity.getIdTerm(), new HashMap<>());
                    }

                    try {
                        if (relatedTerms.containsKey(termDefinitionEntity.getIdTerm()))
                            relatedTerms.get(termDefinitionEntity.getIdTerm()).putAll(findTerms(termDefinitionEntity.getDefinition()));
                        else
                            relatedTerms.put(termDefinitionEntity.getIdTerm(), findTerms(termDefinitionEntity.getDefinition()));
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                });

        return relatedTerms;
    }

    private Map<Integer, Map<String, Integer>> collectReferencedTerms() throws SQLException, ClassNotFoundException {
        Map<Integer, Map<String, Integer>> referencedTerms = new HashMap<>();
        new TermLinksDao().getAllReferences().forEach(ref -> {
            String term = originalTerms.entrySet()
                    .stream()
                    .filter(t -> t.getValue().equals(ref.second()))
                    .findFirst().get().getKey();

            if (referencedTerms.containsKey(ref.first())) {
                referencedTerms.get(ref.first()).put(term, ref.second());
            } else {
                Map<String, Integer> map = new HashMap<>();
                map.put(term, ref.second());
                referencedTerms.put(ref.first(), map);
            }
        });

        return referencedTerms;
    }

    private Integer findTerm(String term) {
        Integer id = upperTerms.get(term);

        if (id == null)
            id = mixedTerms.get(term);
        if (id == null)
            id = lowerTerms.get(term);
        if (id == null)
            id = allTerms.get(term);

        if (id == null) {
            String lowerCaseTerm = term.toLowerCase();
            id = lowerTerms.get(lowerCaseTerm);

            if (id == null)
                id = allTerms.get(lowerCaseTerm);
        }

        return id;
    }

    // works better because of using RAM instead of DB connections
    public Map<String, Integer> findTerms(final String definition) throws SQLException, ClassNotFoundException {
        String regex = "((?<=[^\\w]+)|(?=[^\\w]+))";

        String[] original = {definition.replace("(", "").replace(")", "")};

        String[] originalWords = original[0].split(regex);

        Map<String, Integer> terms = new HashMap<>();

        for (int i = 0; i < originalWords.length; i++) {
            if (", ;:\\\\/".contains(originalWords[i]))
                continue;
            int j;

            for (j = min(originalWords.length, i + 10); j > i; j--) {
                if (originalWords[j - 1].equals(" "))
                    continue;
                StringBuilder orig = new StringBuilder();
                for (int k = i; k < j; k++) {
                    if (originalWords[k].length() > 0)
                        orig.append(originalWords[k]);
                }

                String s = orig.toString().trim();

                Integer id = findTerm(s);

                if (id == null) {
                    String stemmed = StemUtils.stemAllWords(s);
                    id = findTerm(stemmed);
                    if (id != null && s.endsWith("s")) {
                        stemmed = s.substring(0, s.length() - 1);
                        id = findTerm(stemmed);
                    }
                }

                if (id != null && !terms.values().contains(id)) {
                    terms.put(s, id);
                }
            }
        }

        List<Map.Entry<String, Integer>> sortedTerms = new ArrayList<>(terms.entrySet());
        sortedTerms.sort((o1, o2) -> Integer.compare(o2.getKey().length(), o1.getKey().length()));

        sortedTerms.forEach(t -> {
            if (!original[0].contains(t.getKey()))
                terms.remove(t.getKey());

            original[0] = original[0].replace(t.getKey(), "");
        });

        return terms;
    }


    enum TermType {
        UPPER, LOWER, MIXED, NO_LETTERS
    }
}
