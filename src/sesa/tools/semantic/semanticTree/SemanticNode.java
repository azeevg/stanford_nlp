package sesa.tools.semantic.semanticTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ira on 01.06.2016.
 */
public class SemanticNode {

    private int idTerm;
    private String description;
    private List<SemanticNode> childList;

    public SemanticNode() {
        childList = new ArrayList<SemanticNode>();
    }

    public SemanticNode(int idTerm) {
        this.idTerm = idTerm;
        childList = new ArrayList<SemanticNode>();
    }

    public int getIdTerm() {
        return idTerm;
    }

    public void setIdTerm(int idTerm) {
        this.idTerm = idTerm;
    }

    public void addChild(SemanticNode node){
        childList.add(node);
    }


    public List<SemanticNode> getChildList() {
        return childList;
    }

    public void setChildList(List<SemanticNode> childList) {
        this.childList = childList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
