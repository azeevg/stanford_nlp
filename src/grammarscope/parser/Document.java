package grammarscope.parser;

import sesa.common.dataModule.StanfordDataModule;

import java.io.IOException;
import java.io.Serializable;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;


public class Document extends ParsedDocument implements Serializable
{

  //private static String CACHE_REL_TABLE = "relation_cache";

  private static final long serialVersionUID = 942470326434513964L;


	/**
	 * Sentence to list of per-relation parsed relations map
	 * 
	 * @see ParsedRelation
	 */
	public Map<Sentence, Map<MutableGrammaticalRelation, List<ParsedRelation>>> theSentenceToParsedRelationsMap;

  public Document(){
    super();
  }

  ///private HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> cachedRelations;


	/**
	 * Constructor from parsed document
	 */
	public Document(StanfordDataModule cacheModule)
	{
    super(cacheModule);
	}

  /**
	 * Analyze
	 * 
	 * @param thisAnalyzer
	 *            analyzer
	 * @param thisSentence
	 *            sentence
	 */
	public void analyze(final Analyzer thisAnalyzer, final Sentence thisSentence) throws IOException {
		if (thisAnalyzer == null)
			return;
		if (this.theParsedTrees == null)
			return;


		final Map<MutableGrammaticalRelation, List<ParsedRelation>> thisGrammaticalRelation2ParsedRelationsMap = thisAnalyzer.analyze(this, thisSentence);
    //final Map<MutableGrammaticalRelation, List<ParsedRelation>> thisGrammaticalRelation2ParsedRelationsMap = cacheModule.getFilteredRelations(thisSentence);
    //final Map<MutableGrammaticalRelation, List<ParsedRelation>> thisGrammaticalRelation2ParsedRelationsMap = getRelationMap(thisSentence, thisAnalyzer);
		this.theSentenceToParsedRelationsMap.put(thisSentence, thisGrammaticalRelation2ParsedRelationsMap);
	}

  /*public Map<MutableGrammaticalRelation, List<ParsedRelation>> getRelationMap(Sentence sentence, final Analyzer thisAnalyzer) throws IOException {
    Integer hash = sentence.toString().hashCode();
    if (cachedRelations.containsKey(hash)) {
      //LoggingUtility.info("CACHED RELATION = " + sentence.toString());
      return cachedRelations.get(hash);
    } else {
      //LoggingUtility.info("PARSED RELATION = " + sentence.toString());
      //final Tree thisTree = thisParser.parse(sentence);
      Map<MutableGrammaticalRelation, List<ParsedRelation>> thisGrammaticalRelation2ParsedRelationsMap = thisAnalyzer.analyze(this, sentence);
      controller.insertObject(CACHE_REL_TABLE, title, hash + "", getBytes(thisGrammaticalRelation2ParsedRelationsMap));
      return thisGrammaticalRelation2ParsedRelationsMap;
    }
  }*/

	/**
	 * Analyze
	 * 
	 * @param thisAnalyzer
	 *            analyzer
	 */
	public boolean analyze_init(final Analyzer thisAnalyzer)
	{
		if (thisAnalyzer == null)
			return false;
		if (this.theParsedTrees == null)
			return false;
		this.theSentenceToParsedRelationsMap = new IdentityHashMap<Sentence, Map<MutableGrammaticalRelation, List<ParsedRelation>>>();
		return true;
	}
}
