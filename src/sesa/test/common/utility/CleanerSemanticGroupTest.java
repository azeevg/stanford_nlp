package sesa.test.common.utility;

import org.junit.Test;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.strings.StringUtility;
import sesa.tools.cleaner.CleanerSemanticGroup;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Created by Ira on 24.05.2016.
 */
public class CleanerSemanticGroupTest {

    @Test
    public void testFullText() throws SQLException, ClassNotFoundException {
        DocumentsDao dao = new DocumentsDao();
        // System.out.println(dao.fullTextSearch("base font", "w_dbch_computing"));
        CleanerSemanticGroup cleaner = new CleanerSemanticGroup();
        List<String> list = cleaner.readSemanticGroupFromFile();
        cleaner.findCountRelevantGroup(list);
        //  cleaner.findCountRelevantGroup2(list,69454,86323);

    }


    @Test
    public void testClean() throws SQLException, ClassNotFoundException {
        CleanerSemanticGroup.getSemanticRelationCollocationFromDataBase();
        CleanerSemanticGroup.clean("data/dictionary/parse_result/step/semanticGroups_double.txt",
                "semanticGroups_double_2.txt", 2);
    }

    @Test
    public void testClean2() throws SQLException, ClassNotFoundException {
        List<String> list = CleanerSemanticGroup.getSemanticRelationFromDataBase(3);
        FileReaderWriter.write("data/semanticGroups_3.txt", list);
//        CleanerSemanticGroup.clean("data/dictionary/parse_result/step/semanticGroups_double.txt",
//                "semanticGroups_double_3.txt", 3);
    }

    @Test
    public void saveAllTermToFile() throws SQLException, ClassNotFoundException {
        TermDao termDao = new TermDao();
        Set<String> terms = termDao.getAllTerms().keySet();
        FileReaderWriter.write("data/allTerms.txt", StringUtility.convertSetToList(terms));
    }
}
