/**
 * Created by Egor Gorbunov on 18.04.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */

package edu.spbstu.egor.wrapper;


import edu.stanford.nlp.parser.lexparser.BaseLexicon;
import edu.stanford.nlp.parser.lexparser.IntTaggedWord;
import edu.stanford.nlp.parser.lexparser.Options;
import edu.stanford.nlp.util.Index;

public class BaseLexiconWrapper extends BaseLexicon {

    public BaseLexiconWrapper(Index<String> wordIndex, Index<String> tagIndex) {
        super(wordIndex, tagIndex);
    }

    public BaseLexiconWrapper(Options op, Index<String> wordIndex, Index<String> tagIndex) {
        super(op, wordIndex, tagIndex);
    }

    protected void buildPT_T() {
        int numTags = tagIndex.size();
        m_TT = new double[numTags][numTags];
        m_T = new double[numTags];
        double[] tmp = new double[numTags];
        for (IntTaggedWord word : words) {
            double tot = 0.0;
            for (int t = 0; t < numTags; t++) {
                IntTaggedWord iTW = new IntTaggedWord(word.word, t);
                tmp[t] = seenCounter.getCount(iTW);
                tot += tmp[t];
            }
            if (tot < 10) {
                continue;
            }
            for (int t = 0; t < numTags; t++) {
                for (int t2 = 0; t2 < numTags; t2++) {
                    if (tmp[t2] > 0.0) {
                        double c = tmp[t2] / tot;
                        m_T[t] += c;
                        m_TT[t2][t] += c;
                    }
                }
            }
        }
    }
}
