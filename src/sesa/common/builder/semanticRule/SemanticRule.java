package sesa.common.builder.semanticRule;


import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;

/**
 * Grammar rule for check: can we add new grammatical relation
 */
public class SemanticRule {

    private String relationShortName;
    private String[] with;
    private String[] without;

    public SemanticRule(String relation, String[] with, String[] without) {
        this.relationShortName = relation;
        this.with = with;
        this.without = without;
    }


    public boolean isRelationValidForGroup(SemanticGroup group, SemanticRelation rel) {

        String shortName = rel.getShortName();
        if (!shortName.equals(relationShortName))
            return true;

        for (String w : with) {
            if (group.containsRelation(w))
                return true;
        }

        for (String wo : without) {
            if (group.containsRelation(wo))
                return false;
        }


        return true;
    }
}
