package sesa.test.dictionary;

import org.junit.Test;
import sesa.common.utility.dictionary.FreeFormat2JsonConverter;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by gleb on 15.04.17.
 */

public class FreeFormat2JsonConverterTest {

    static final String originalDictionary = "data/dictionary/loaded_dictionary/GlebAzeev_original_data";
    static final String preparedDictionary = "data/dictionary/loaded_dictionary/GlebAzeev_prepared_data";

    private static final String HEAD_WORD_REGEXP = "<HEAD_WORD>(.+)</HEAD_WORD>";

    @Test
    public void convertAllSourceFiles() throws IOException {
        FreeFormat2JsonConverter converter = new FreeFormat2JsonConverter();
        converter.convertAll(new File(originalDictionary), new File(preparedDictionary));
    }

    @Test
    public void oneDefinitionTest() throws Exception {
        Pattern headWordPattern = Pattern.compile(HEAD_WORD_REGEXP);

        String line = "<HEAD_WORD>A3-size paper</HEAD_WORD>";
        Matcher matcher = headWordPattern.matcher(line);
        if (matcher.find())
            System.out.println("TERM: " + matcher.group(1));
    }
}
