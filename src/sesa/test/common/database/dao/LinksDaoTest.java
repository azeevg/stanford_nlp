package sesa.test.common.database.dao;

import org.junit.Test;
import sesa.common.database.dao.dictionary.TermLinksDao;
import sesa.common.utility.FileReaderWriter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ira on 04.06.2016.
 */
public class LinksDaoTest {

    @Test
    public void testExtractTermWithoutLinks() throws SQLException, ClassNotFoundException {
        TermLinksDao dao = new TermLinksDao();
        List<Integer> termsNotContainLinks = dao.getTermsNotContainLink();
      //  FileReaderWriter.write("data/idTermsNotContainLink3.txt", termsNotContainLinks);
    }

    @Test
    public void testExtractUniqueTermWithoutLinks() throws SQLException, ClassNotFoundException {
        TermLinksDao dao = new TermLinksDao();
        List<Integer> idTermsNotContainLink = dao.getTermsNotContainLink();
        List<String> idUniqueTerms = FileReaderWriter.read("data/dictionary/utility/idUniqueTerm.txt");
        List<Integer> resultIdTerms = new ArrayList<Integer>();
        for (Integer id : idTermsNotContainLink) {
            if (idUniqueTerms.contains(id.toString())) {
                resultIdTerms.add(id);
            }
        }

        System.out.println("result count: " + resultIdTerms.size());

        //  TermLinksDao dao = new TermLinksDao();
        //FileReaderWriter.write("data/idUniqueTermsNotContainLink2.txt", resultIdTerms);

    }

}
