package sesa.common.builder.relationsBuilder;

import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.Tree;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.semantics.SemanticGroup;
import sesa.common.utility.io.CommonUtils;
import sesa.common.utility.io.FileUtility;

import java.util.*;

/**
 * This class represents a realisation the method getParseTree() of an abstract class CommonRelationsBuilder
 */
public class AdaRelationsBuilder extends CommonRelationsBuilder {

    private HashMap<String, String> treeMap = new HashMap<String, String>();

    public AdaRelationsBuilder() throws Exception {
        List<String> lines = FileUtility.readAllLines("./data/ada_cache/tree_cache.txt");
        for (int i = 0; i < lines.size(); i = i + 2) {
            treeMap.put(lines.get(i), lines.get(i + 1));
        }
    }

    /**
     * Get parse tree of sentence
     * Tree is loaded from a file
     * Use hash code of sentence to choose the parse tree of this sentence
     * @param sentence
     * @return
     */
    @Override
    protected Tree getParseTree(List sentence){
        String hash = String.valueOf(Sentence.listToString(sentence).hashCode());
        Tree parse = null;
        if (!treeMap.containsKey(hash)){
            return null;
        }

        try {
            parse = CommonUtils.readPennTreeFormString(treeMap.get(hash)); // подгружаем дерево
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parse;
    }


    public String printGroup(SemanticGroup group) {
        StringBuilder builder = new StringBuilder();
        for (SentenceToken token : group.getTokens()) {
            builder.append(token.getValue()).append(";");
        }
        return builder.toString();
    }
}
