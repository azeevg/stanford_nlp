package sesa.tools.cleaner;

import org.json.JSONException;
import sesa.common.database.dao.dictionary.KernelDictionaryDao;
import sesa.common.utility.FileReaderWriter;

import java.sql.SQLException;

/**
 * Created by Ira on 27.05.2016.
 */
public class ParseKernelDictionary {

    public static void parse() throws JSONException, SQLException, ClassNotFoundException {
        KernelDictionaryDao dao = new KernelDictionaryDao();

        String jsonStr = dao.extractDictionary();
        FileReaderWriter.write("kernel_dictionary.txt", jsonStr);
//        String name = "computing";
//        List<String> list = new ArrayList<String>();
//        JSONArray jsonarray = new JSONArray(jsonStr);
//        for (int i = 0; i < jsonarray.length(); i++) {
//            JSONObject jsonobject = jsonarray.getJSONObject(i);
//            if (jsonobject.getJSONObject(name) != null) {
//                list = (List<String>) jsonobject.getJSONObject(name);
//                break;
//            }
//        }


    }
}
