package sesa.common.database.entity;

import java.util.Map;

public interface IDBEntity {

    String getID();
    Map<String, Object> toMap();
}
