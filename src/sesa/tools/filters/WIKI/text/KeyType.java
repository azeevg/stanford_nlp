package sesa.tools.filters.WIKI.text;

public enum KeyType {
    START_TYPE,
    END_TYPE,
    BOTH_TYPE,
    NONE_TYPE
}
