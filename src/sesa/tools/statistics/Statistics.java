package sesa.tools.statistics;

import sesa.common.configuration.Configuration;
import sesa.common.database.dao.CommonDao;
import sesa.common.utility.common.Application;

import java.sql.SQLException;

public class Statistics extends Application {
    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {
        CommonDao commonDao = new CommonDao();

        info("         WIKI documents: " + commonDao.countTotalOfRow(Configuration.DB_COLLECTION_WIKI));
        info("Filtered WIKI documents: " + commonDao.countTotalOfRow(Configuration.DB_COLLECTION_FILTERED_WIKI));
        info("       PUBMED documents: " + commonDao.countTotalOfRow(Configuration.DB_COLLECTION_PUBMED) );

        return true;
    }
}
