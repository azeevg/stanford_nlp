package sesa.common.semantics;

import sesa.common.builder.semanticRule.AliasManager;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.support.RelationMistake;

/**
 * This class represents a semantic relation
 */
public class SemanticRelation {

    private String relationName; // name of relation: subj, obj, pred
    private String shortName;
    private SentenceToken token;
    private int treeHash;
    private RelationMistake mistake;
    private String tokenBaseValue;


    public SemanticRelation(String relationName) {
        this.relationName = relationName;
    }

    public SemanticRelation() {
    }

    public RelationMistake getMistake() {
        return mistake;
    }

    public void setMistake(RelationMistake mistake) {
        this.mistake = mistake;
    }

    public SentenceToken getToken() {
        return token;
    }

    public void setToken(SentenceToken token) {
        this.token = token;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    @Override
    public int hashCode() {
        StringBuilder builder = new StringBuilder();
        String value = getToken().getBaseValue();
        if (value == null)
            value = getToken().getValue();
        builder.append(value.toLowerCase()).append("||");
        builder.append(getShortName()).append("||");
        //builder.append(getSentencePosition());
        return builder.toString().hashCode();

    }

    @Override
    public String toString() {
        return "SR{" +
                "name='" + shortName + '\'' +
                ", token='" + token + '\'' +
                //  ", SentencePosition=" + sentencePosition +
                '}';
    }



    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public int getTreeHash() {
        return treeHash;
    }

    public void setTreeHash(int treeHash) {
        this.treeHash = treeHash;
    }

    public String getTokenBaseValue() {
        return tokenBaseValue;
    }

    public void setTokenBaseValue(String tokenBaseValue) {
        this.tokenBaseValue = tokenBaseValue;
    }

    public String getRole() {
        return AliasManager.relationToRole.get(shortName);
    }

    public boolean hasMistake() {
        return mistake != null;
    }
    //service member


  /* public int getSentencePosition() {
    return sentencePosition;
  }

  public void setSentencePosition(int sentencePosition) {
    this.sentencePosition = sentencePosition;
  }*/
    /*public SemanticGroup getGroup() {
    return group;
  } */

  /*public void setGroup(SemanticGroup group) {
    this.group = group;
  } */

     /*public int compareTo(SemanticRelation o) {
    return getSentencePosition() - o.getSentencePosition();
  } */

}
