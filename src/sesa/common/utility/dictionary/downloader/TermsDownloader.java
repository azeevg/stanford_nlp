package sesa.common.utility.dictionary.downloader;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sesa.common.model.dictionary.DictionaryTerms;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Ira on 13.05.2016.
 */
public class TermsDownloader {

    public final Logger log = LogManager.getLogger(TermsDownloader.class);

    public Set<String> readTermsFromFile(String fileName) {
        Set<String> terms = new HashSet<String>();

        JSONParser parser = new JSONParser();
        log.info("Download terms from file:" + fileName);
        DictionaryTerms dictionary = new DictionaryTerms();
        int i = 0;
        try {
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                String jsonStr = in.nextLine();

                try {
                    Object obj = parser.parse(jsonStr);
                    JSONObject jsonObject = (JSONObject) obj;
                    if (jsonObject.get("term_name") != null) {
                        terms.add(jsonObject.get("term_name").toString());
                        i++;
                    }
                } catch (ParseException pe) {
                    log.error(pe.getMessage());
                    log.error(jsonStr);
                    pe.printStackTrace();
                }
            }
            log.info("-- end of downloaded --  ");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("size:" + i);
        System.out.println("Terms size:" + dictionary.getSize());

        return terms;
    }
}
