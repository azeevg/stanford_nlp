package sesa.application;

import sesa.tools.semanticWeb.Edge;
import sesa.tools.semanticWeb.Graph;
import sesa.tools.semanticWeb.Vertex;
import sesa.common.utility.common.Application;

import java.util.List;

public class TestGraph extends Application {
    private Graph createGraph() {
        Graph g = new Graph(true);

        g.addVertex(new Vertex("Linux"));
        g.addVertex(new Vertex("Operation system"));
        g.addVertex(new Vertex("Computer science"));
        g.addVertex(new Vertex("Programming"));
        g.addVertex(new Vertex("C"));
        g.addVertex(new Vertex("Language"));
        g.addVertex(new Vertex("GCC"));
        g.addVertex(new Vertex("Compiler"));
        g.addVertex(new Vertex("Tool"));

        g.addEdge(new Edge<String>("Linux", "Operation system", "is"));
        g.addEdge(new Edge<String>("Programming", "Computer science", "is part of"));
        g.addEdge(new Edge<String>("Language", "Programming", "is instrument of"));
        g.addEdge(new Edge<String>("C", "Language", "is"));
        g.addEdge(new Edge<String>("GCC", "Compiler", "is"));
        g.addEdge(new Edge<String>("Compiler", "Tool", "is"));
        g.addEdge(new Edge<String>("GCC", "Linux", "is instrument of"));
        g.addEdge(new Edge<String>("GCC", "C", "is compiler of"));

        return g;
    }

    @Override
    protected boolean body() {
        Graph g = createGraph();
        g.print();

        info("");
        info("Path:");
        List<String> pathList = g.getPath("GCC", "Computer science");
        String[] path = pathList.toArray(new String[pathList.size()]);
        for (int vertexID = 0; vertexID < path.length - 1; vertexID++) {
            Edge<String> edge = g.getEdge(path[vertexID], path[vertexID + 1]);
            info(path[vertexID] + " " + edge.getData() + " " + path[vertexID + 1]);
        }

        return true;
    }
}
