package sesa.common.database.dao.dictionary;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.dao.CommonDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ira on 13.05.2016.
 */
public class TermDefinitionDao extends CommonDao {
    public final Logger log = LogManager.getLogger(TermDefinitionDao.class);
    private String collectionName = "terms_comp_definition";
    private final int sourceId = new DictionarySourceDao().getSourceId("IBM");

    public TermDefinitionDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public List<TermDefinitionEntity> addToTermDefinitionCollections(List<TermDefinitionEntity> entities) {
        log.info("Add terms to " + collectionName);
        for (TermDefinitionEntity entity : entities) {
            addToTermDefinitionCollections(entity);
        }
        log.info("Added rows to \"terms_comp_definition\":" + entities.size());
        return entities;
    }

    public TermDefinitionEntity addToTermDefinitionCollections(TermDefinitionEntity entity) {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "INSERT INTO `" + collectionName + "` (TERM_ID, DEF ,SRC_ID) VALUES(?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setInt(1, entity.getIdTerm());
            ps.setString(2, entity.getDefinition());
//            ps.setString(3, "http://computerlanguage.com/terms/");
            ps.setInt(3, sourceId);
            ps.execute();
            resultSet = ps.executeQuery("select last_insert_id() as last_id from " + collectionName);
            resultSet = ps.getResultSet();
            String id = null;
            if (resultSet.next()) {
                id = resultSet.getString("last_id");
            }
            entity.setIdDefinition(Integer.valueOf(id));
            //log.info("Added new row (" + Integer.valueOf(id) + ":" + entity.getIdTerm() + ") to table:" + collectionName);


        } catch (SQLException e) {
            log.error(e.getMessage());
            System.out.println("\n\n\nTermDefinitionDao\n\n\n");

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return entity;
    }


    public List<TermDefinitionEntity> getTermDefinitionByTermID(int idTerm) {
        List<TermDefinitionEntity> entities = new ArrayList<TermDefinitionEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE TERM_ID = " + idTerm;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int idDef = resultSet.getInt("DEF_ID");
                String def = resultSet.getString("DEF");
                // if(def != null) def = StringUtility.updateStringInBracket(def);
                log.info("Get row (" + idDef + ":" + def + ")");
                entities.add(new TermDefinitionEntity(idDef, idTerm, def));


            }


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return entities;
    }

    public List<TermDefinitionEntity> getNotNUllTermDefinitionByTermID(int idTerm) {
        List<TermDefinitionEntity> entities = new ArrayList<TermDefinitionEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `DEF` != \"\" AND TERM_ID = " + idTerm;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int idDef = resultSet.getInt("DEF_ID");
                String def = resultSet.getString("DEF");
                entities.add(new TermDefinitionEntity(idDef, idTerm, def));
//                if(def.length() > 1){
//                    entities.add(new TermDefinitionEntity(idDef,idTerm,def));
//                    log.debug("Get row with id(" + idDef + ")");
//                }
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return entities;
    }

    public List<TermDefinitionEntity> getAllNotNUllTermDefinition() {
        List<TermDefinitionEntity> entities = new ArrayList<TermDefinitionEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `DEF` != \"\"";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int idDef = resultSet.getInt("DEF_ID");
                int idTerm = resultSet.getInt("TERM_ID");
                String def = resultSet.getString("DEF");
                entities.add(new TermDefinitionEntity(idDef, idTerm, def));

            }


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return entities;
    }

    public List<TermDefinitionEntity> getAllTermDefinition() {
        List<TermDefinitionEntity> entities = new ArrayList<TermDefinitionEntity>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "`";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int idDef = resultSet.getInt("DEF_ID");
                int idTerm = resultSet.getInt("TERM_ID");
                String def = resultSet.getString("DEF");
                entities.add(new TermDefinitionEntity(idDef, idTerm, def));

            }


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return entities;
    }

    public int getTermIdByDefId(int defId) {
        int termId = 0;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `DEF_ID` = " + defId;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();
            if (resultSet.next()) {
                termId = resultSet.getInt("TERM_ID");
            }


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return termId;
    }


    public List<Integer> getAllReferences() {
        List<Integer> referencingDefsId = new ArrayList<>();

        try (Connection connection = cp.takeConnection()) {
            String query = "SELECT * FROM " + collectionName + " WHERE DEF LIKE 'See %.'";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            while (resultSet.next()) {
//                System.out.println(resultSet.getInt("DEF_ID") + "\t" + resultSet.getString("DEF"));
                String definition = resultSet.getString("DEF");

//                if (!definition.contains("See also"))
                    referencingDefsId.add(resultSet.getInt("DEF_ID"));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return referencingDefsId;
    }

    public String getDefinitionByDefinitionId(int defId) {
        try (Connection connection = cp.takeConnection()) {
            String query = "SELECT * FROM " + collectionName + " WHERE DEF_ID=" + defId;
            PreparedStatement ps = connection.prepareStatement(query);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            if (resultSet.next()) {
                return resultSet.getString("DEF");
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return null;
    }

    public boolean replaceReferences(int destId, int sourceId) {
        try (Connection connection = cp.takeConnection()) {
            String querySource = "SELECT * FROM " + collectionName + " WHERE DEF_ID=" + sourceId;
            PreparedStatement ps = connection.prepareStatement(querySource);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            if (resultSet.next()) {
                String def = resultSet.getString("DEF");
                String updateQuery = "UPDATE " + collectionName + " SET DEF=? WHERE DEF_ID=?";
                ps = connection.prepareStatement(updateQuery);
                ps.setString(1, def);
                ps.setInt(2, destId);
                ps.executeUpdate();

                return true;
            }


        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return false;
    }

    public int getDefIdByTermId(int termId) {
        int defId = 0;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `TERM_ID` = " + termId;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();
            if (resultSet.next()) {
                defId = resultSet.getInt("DEF_ID");
            }


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return defId;
    }

    public void deleteDef(int defId) {
        PreparedStatement ps = null;
        Connection connection = null;

        try {
            connection = cp.takeConnection();

            String query = "DELETE FROM `" + collectionName + "` WHERE `DEF_ID` = " + defId;
            ps = connection.prepareStatement(query);
            ps.execute();
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps);
        }
    }

    public void updateDef(int defId, String newDef) {
        PreparedStatement ps = null;
        Connection connection = null;

        try {
            connection = cp.takeConnection();

            String query = "UPDATE `" + collectionName + "` SET DEF=? WHERE `DEF_ID` = " + defId;
            ps = connection.prepareStatement(query);
            ps.setString(1, newDef);
            ps.execute();
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps);
        }
    }

    public List<Integer> getAllLinks() {
        List<Integer> referencingDefsId = new ArrayList<>();

        try (Connection connection = cp.takeConnection()) {
            String query = "SELECT * FROM " + collectionName + " WHERE DEF LIKE '%See also %.'";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            while (resultSet.next()) {
                String definition = resultSet.getString("DEF");
                referencingDefsId.add(resultSet.getInt("DEF_ID"));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return referencingDefsId;
    }

    public List<String> getDefsByTermId(int termId) {
        List<String> defs = new ArrayList<>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE TERM_ID = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, termId);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int idDef = resultSet.getInt("DEF_ID");
                String def = resultSet.getString("DEF");
                log.info("Get row (" + idDef + ":" + def + ")");
                defs.add(def);
            }


        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return defs;
    }
}
