package sesa.test.junit;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.egor.Grammar;
import edu.spbstu.egor.wrapper.CollocationSpanTaggingParser;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.lexparser.ExhaustivePCFGParser;
import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Parser;
import grammarscope.parser.Sentence;
import org.junit.Test;
import sesa.common.utility.bundle.FileManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Ira on 25.04.2016.
 */
public class CollocationTest {
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";



    @Test
    public void testGrammar(){
        FileManager fileManager = new FileManager("path");
       // fileManager.init("path");
        CollocationDictionary dictionary = null;
        try {
            dictionary = new CollocationDictionary(new String[]{fileManager.getValue("path.computerCollocations")});
        } catch (IOException e) {

        }

        Grammar grammar = new Grammar();

        ExhaustivePCFGParser pparser = new CollocationSpanTaggingParser(grammar, dictionary);
    }

    @Test
    public void testCollocationParserGrammarScope() throws IOException {
        FileManager fileManager = new FileManager("path");

        ObjectInputStream inputStream = IOUtils.readStreamFromString(fileManager.getValue("path.smallLanguageModel"));
        Parser parser = new Parser(inputStream);
        Tree tree = parser.parse(prepareSentence("data/test.txt"),true);
        tree.pennPrint();
    }

    public Sentence prepareSentence(String fileName) throws FileNotFoundException {
        ArrayList<Word> wordArrayList = new ArrayList<Word>();
        Scanner in = new Scanner(new File(fileName));
        while (in.hasNext()) {
            wordArrayList.add(new Word(in.next()));
        }
        in.close();

        Sentence sentence = new Sentence(wordArrayList);
        return sentence;

    }


}
