package sesa.common.builder.relationsBuilder;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.semantics.SemanticGroup;

import java.io.*;
import java.util.*;

/**
 * This class represents a realisation the method getParseTree() of an abstract class CommonRelationsBuilder
 */
public class DeliaRusoRelationsBuilder extends CommonRelationsBuilder {

    private LexicalizedParser lexicalizedParser;

    public DeliaRusoRelationsBuilder(ObjectInputStream thisInputStream) throws Exception {
        this.lexicalizedParser = new LexicalizedParser(thisInputStream);
    }

    /**
     * Build tree on the fly
     * @param sentence The input Sentence
     * @return A Tree that is the parse tree for the sentence.
     */
    @Override
    protected Tree getParseTree(List sentence){
        return lexicalizedParser.apply(sentence); // �� ���� ������ ������
    }


    public String printGroup(SemanticGroup group) {
        StringBuilder builder = new StringBuilder();
        for (SentenceToken token : group.getTokens()) {
            builder.append(token.getValue()).append(";");
        }
        return builder.toString();
    }


//  private List<SentenceToken> getSentenceTokens(Sentence sentence, HashMap<Object, SentenceToken> wordTokenMap) {
//
//    List<SentenceToken> tokens = new LinkedList<SentenceToken>();
//    for (Object word : sentence)
//      tokens.add(new SentenceToken((Word) word).value());
//    return tokens;
//  }
//
//  public List<SemanticRelation> tripletToSyntaxRelations(SentenceTriplet triplet) {
//    List<SemanticRelation> relations = new LinkedList<SemanticRelation>();
//
//    relations.add(new SemanticRelation("subj", triplet.getSubject()));
//    relations.add(new SemanticRelation("predicate", triplet.getPredicate()));
//    relations.add(new SemanticRelation("object", triplet.getObject()));
//    return relations;
//  }
//
//  public List<SentenceTriplet> getSentenceTriplets(LexicalizedParser lp, String text) {
//
//    List<SentenceTriplet> triplets = new LinkedList<SentenceTriplet>();
//    Reader rd = new StringReader(text);
//    DocumentPreprocessor dp = new DocumentPreprocessor(rd);
//    for (List sentence : dp) {
//      Tree parse = lp.apply(sentence);
//      StringWriter stringWriter = new StringWriter();
//      PrintWriter writer = new PrintWriter(stringWriter);
//      parse.indentedXMLPrint(writer);
//      String xmlTree = stringWriter.toString();
//      SentenceTriplet triplet = TripletFinder.findSentenceTriplet(xmlTree);
//      for (Object w : sentence) {
//
//        Word word = (Word) w;
//        if (word == t)
//
//
//      }
//
//      triplet.setSentence(Sentence.listToString(sentence));
//      triplets.add(triplet);
//      //List<SemanticRelation> relations = tripletToSyntaxRelations(triplet);
//      //LoggingUtility.info(Sentence.listToString(sentence));
//      //LoggingUtility.info(relations.toString());
//    }
//    return triplets;
//  }
}
