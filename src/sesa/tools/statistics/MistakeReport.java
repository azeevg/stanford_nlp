package sesa.tools.statistics;

import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.vector.DocumentUtils;
import sesa.common.utility.semantic.SemanticUtils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ashishagin
 * Date: 3/18/13
 * Time: 6:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class MistakeReport extends Application {
    @Override
    protected boolean body() {
        try {
            final File manualFolder = new File("./data/c_colored_pages/manual_parse");
            List<String> manualFiles = listFilesForFolder(manualFolder);

            final File stanfordFolder = new File("./data/c_colored_pages/colored_pages");
            List<String> stanfordFiles = listFilesForFolder(stanfordFolder);

            List<SentenceTree> allst1 = new LinkedList<SentenceTree>();
            List<SentenceTree> allst2 = new LinkedList<SentenceTree>();

            for (String manualFilePath : manualFiles) {
                //if(manualFilePath.contains("Ada"))
                //  continue;
                //LoggingUtility.info("BUILDING REPORT FOR = " + manualFilePath);
                File manualFile = new File(manualFilePath);
                File stanfordFile = new File("./data/c_colored_pages/colored_pages/" + manualFile.getName());
                List<SentenceTree> st1 = DocumentUtils.loadSentencesFromXml(manualFilePath);
                //LoggingUtility.info("MANUAL OK!");
                List<SentenceTree> st2 = DocumentUtils.loadSentencesFromXml(stanfordFile.getAbsolutePath());

                allst1.addAll(st1);
                allst2.addAll(st2);

                //LoggingUtility.info("STANFORD OK!");


//        SemanticUtils.findMistakes(st1, st2);
//        String report = DocumentUtils.buildRoleMistakeReport(SemanticUtils.createRelationGrouping(st2));
//        String filePath = "./data/reports/" + manualFile.getName();
//        FileUtility.writeAllTextToFile(filePath, report);
                //LoggingUtility.info("Generated html colored file = " + filePath);
                //Map<String, Map<String, List<SemanticR>>>
                //RoleDeviationStatistic statistic = SemanticUtils.matchSentencesSemantic(st1, st2);
                //System.out.println(statistic);
                //  break;
            }

            SemanticUtils.findMistakes(allst1, allst2);
            String report = DocumentUtils.buildRoleMistakeReport(SemanticUtils.createRelationGrouping(allst2));
            String filePath = "./data/reports/" + "overall.html";
            FileUtility.writeAllTextToFile(filePath, report);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return true;
    }

    public List<String> listFilesForFolder(final File folder) {
        List<String> files = new LinkedList<String>();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory())
                files.add(fileEntry.getAbsolutePath());
        }
        return files;
    }
}
