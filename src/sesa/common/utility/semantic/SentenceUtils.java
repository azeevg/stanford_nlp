package sesa.common.utility.semantic;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Word;
import grammarscope.parser.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Ira on 15.05.2016.
 */
public class SentenceUtils {

    public static final Logger log = LogManager.getLogger(SentenceUtils.class);

    /**
     * Convert original sentence to sentence where words which form collocation united to one.
     * Collocations found through input array. Array contains list of indexes begin and end collocation.
     * We found them when built parse tree
     * Example:
     * [Java is a general-purpose computer programming language] ->
     * [Java is a general-purpose computer programminglanguage]
     *
     * @param thisSentence          original sentence
     * @param arrayIndexCollocation
     * @return
     */
    public static Sentence convertSentence(Sentence thisSentence, HashMap<Integer, Integer> arrayIndexCollocation) {
        if (arrayIndexCollocation.size() == 0) {
            return thisSentence;
        }
        log.debug("Sentence to convert:" + thisSentence + ", hashCode():" + thisSentence.toString().hashCode());
        ArrayList<Word> wordArrayList = new ArrayList<Word>();
        for (Integer start : arrayIndexCollocation.keySet()) {
            int end = arrayIndexCollocation.get(start);
            //System.out.println(start + "=" + end);
            StringBuilder sb = new StringBuilder();
            for (int i = start; i < end; i++) {
                if (i < thisSentence.size()) {
                    sb.append(thisSentence.get(i));
                }
            }
            wordArrayList.add(new Word(sb.toString()));
        }
        Sentence sentence = new Sentence(wordArrayList);
        log.debug("Convert sentence:" + sentence + ", hashCode():" + sentence.toString().hashCode());
        return sentence;
    }

    public static List<HasWord> convertSentence(List<HasWord> thisSentence, HashMap<Integer, Integer> arrayIndexCollocation) {
        log.debug("Sentence to convert:" + thisSentence.toString());
        ArrayList<Word> wordArrayList = new ArrayList<Word>();

        for (Integer start : arrayIndexCollocation.keySet()) {
            int end = arrayIndexCollocation.get(start);
            StringBuilder sb = new StringBuilder();
            for (int i = start; i < end; i++) {
                if (i < thisSentence.size()) {
                    sb.append(thisSentence.get(i));
                }
            }
            wordArrayList.add(new Word(sb.toString()));

        }
        Sentence sentence = new Sentence(wordArrayList);
        List<HasWord> sentenceB = new ArrayList<HasWord>(sentence);

        log.debug("Convert sentence:" + sentenceB);

        return sentenceB;
    }

    /**
     * Irina Daniel
     * Read string from file and convert string to form grammarscope.parser.Sentence
     *
     * @param fileName
     * @return
     * @throws
     */
    public static Sentence readSentenceFromFile(String fileName) throws FileNotFoundException {
        ArrayList<Word> wordArrayList = new ArrayList<Word>();
        Scanner in = new Scanner(new File(fileName));
        while (in.hasNext()) {
            wordArrayList.add(new Word(in.next()));
        }
        in.close();

        Sentence sentence = new Sentence(wordArrayList);
        return sentence;

    }

    public static Sentence convertStringToSentence(String text) {
        ArrayList<Word> wordArrayList = new ArrayList<Word>();
        for (String token : text.split(" ")) {
            wordArrayList.add(new Word(token));
        }
        Sentence sentence = new Sentence(wordArrayList);
        return sentence;

    }

    public static Sentence convertWordsToSentence(List<? extends HasWord> listSent) {
        ArrayList<Word> wordArrayList = new ArrayList<Word>();
        for (HasWord word : listSent) {
            wordArrayList.add(new Word(word.word()));
        }
        Sentence sentence = new Sentence(wordArrayList);
        return sentence;
    }
}
