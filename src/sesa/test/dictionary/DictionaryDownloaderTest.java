package sesa.test.dictionary;

import org.junit.Test;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.dictionary.DefinitionReferencesResolver;
import sesa.common.utility.dictionary.LinksResolver;
import sesa.common.utility.dictionary.downloader.AppDictionarySaver;
import sesa.common.utility.strings.StringUtility;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Ira on 20.04.2016.
 */
public class DictionaryDownloaderTest {


    @Test
    public void testAddAllTerms() throws SQLException, ClassNotFoundException {
        AppDictionarySaver saver = new AppDictionarySaver();
        saver.setDir("data/dictionary/update");

        saver.runSaveTermsToDataBase("data/dictionary/update");
    }

    @Test
    public void glebAddAllTerms() throws Exception, ClassNotFoundException {
        long start = System.currentTimeMillis();
        String path = "data/dictionary/loaded_dictionary/GlebAzeev_prepared_data";
//        path = "data/dictionary/loaded_dictionary/folder_for_tests";
        AppDictionarySaver saver = new AppDictionarySaver();
//        saver.setDir(path);
//        saver.runSaveTermsToDataBase(path);

        Arrays.stream(new File(path).listFiles()).forEach(f -> {
            try {
                saver.runSaveDictionaryFromFile1(f.getAbsolutePath());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });


        new DefinitionReferencesResolver().replaceAllReferences();

        TermDefinitionDao definitionDao = new TermDefinitionDao();
        List<Integer> links = definitionDao.getAllLinks();
        LinksResolver linksResolver = new LinksResolver();
        for (Integer link : links) {
            linksResolver.resolveLink(link);
        }

//        saver = new AppDictionarySaver();
//        saver.setDir(path);
//        saver.runSaveDictionaryToDataBase();

        long end = System.currentTimeMillis();
        long t = end - start;
        long mins = t / 1000 / 60;

        System.out.println("Elapsed time: " + (mins / 60) + ":" + (mins % 60));
    }

    @Test
    public void testGetAllTerms() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        System.out.println(dao.getAllTerms().size());
        for (String s : StringUtility.getListStringStartFromPoint(dao.getAllTerms().keySet())) {
            FileReaderWriter.write("startPointTerm.txt", s);
        }
    }

    @Test
    public void testSaveDictionary() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        AppDictionarySaver saver = new AppDictionarySaver();
        //    Map<String, Integer> allTerms = dao.getAllTerms();
        saver.setDir("data/dictionary/loaded_dictionary/processed_data/update/");
        saver.runSaveDictionaryToDataBase();
        //    saver.runSaveDictionaryFromFile("data/dictionary/loaded_dictionary/processed_data/update/U_update.txt");
        // saver.runSaveDictionaryFromFile("data/dictionary/update/09_update.txt",allTerms);
    }

    @Test
    public void testDeletePoint() throws SQLException, ClassNotFoundException {
        String text = ".NET Enterprise Servers are extensions to Microsoft's BackOffice suite with enhancements that support the .NET Framework.";


        TermDao dao = new TermDao();
        Map<String, Integer> map = dao.getAllTerms();
        List<String> terms = StringUtility.getListStringStartFromPoint(map.keySet());
        List<String> res = new ArrayList<String>();

        for (String term : terms) {
            if (text.contains(term)) {
                StringBuilder sb = new StringBuilder();
                sb.append("#");
                sb.append(term, 1, term.length());
                res.add(sb.toString());
                text = text.replace(term, sb.toString());

            }
        }
        System.out.println(text);


    }

    @Test
    public void deletePointInBracket() {

        String text = "  ( .COOP.erative) A top-level Internet domain used by members of cooperative societies and organizations. ";
        text = text.trim();
        StringBuilder sb = new StringBuilder();
        if (text.indexOf("(") == 0) {
            int end = text.indexOf(")");
            for (int i = 0; i < end + 1; i++) {
                sb.append(text.charAt(i));
            }
            if (sb.toString().contains(".")) {
                String s = sb.toString().replaceAll("\\.", "");
                //  System.out.println(s);
                //  System.out.println(text.replace(sb.toString(), ""));
                String result = s + "." + text.replace(sb.toString(), "");
                System.out.println(result);
            }


        }
    }

    @Test
    public void extractTermDefinition() throws SQLException, ClassNotFoundException {
        TermDefinitionDao dao = new TermDefinitionDao();
        List<TermDefinitionEntity> entities = dao.getNotNUllTermDefinitionByTermID(917);
        for (TermDefinitionEntity entity : entities) {
            String text = entity.getDefinition();
        }

    }


}
