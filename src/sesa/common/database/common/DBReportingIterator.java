package sesa.common.database.common;

import sesa.common.database.entity.IDBEntity;
import sesa.common.features.Option;
import sesa.common.utility.logging.ReportUtility;

public class DBReportingIterator implements IDBIterator {

    private IDBIterator iterator;
    private ReportUtility reporter;

    public DBReportingIterator(IDBIterator iterator, String reportMessage) {
        this.iterator = iterator;
        reporter = new ReportUtility(reportMessage, iterator.count());
    }

    public synchronized Option<IDBEntity> next() {
        reporter.addProcessed(1);
        reporter.report();
        return iterator.next();
    }

    public long count() {
        return iterator.count();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }
}
