package sesa.common.database.dao.dictionary;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.CommonDao;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.entity.dictionary.TermEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * This class represents methods for save and adding collection to table "terms_comp" in database
 * "terms_comp" contains all name of terms from dictionary
 * Created by Ira on 13.05.2016.
 */
public class TermDao extends CommonDao {
    public final Logger log = LogManager.getLogger(CommonDao.class);
    private final int sourceId = new DictionarySourceDao().getSourceId("IBM");
    private String collectionName = "terms_comp";

    public TermDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public Map<String, Integer> addToTermsCollectionsAndReturnAdded(Set<String> terms) {
        Map<String, Integer> termMap = new HashMap<String, Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        log.info("Add terms to " + collectionName);
        try {
            connection = cp.takeConnection();
            for (String name : terms) {
                String query = "INSERT INTO `" + collectionName + "` (HEAD_WORDS ,SRC_ID) VALUES(?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, name);
//                ps.setString(2, "http://computerlanguage.com/terms/");
                ps.setInt(2, sourceId);
                ps.execute();
                resultSet = ps.executeQuery("select last_insert_id() as last_id from " + collectionName);
                resultSet = ps.getResultSet();
                if (resultSet.next()) {
                    String id = resultSet.getString("last_id");
                    termMap.put(name, Integer.valueOf(id));
                    log.debug("Added new row (" + Integer.valueOf(id) + ":" + name + ") to table:" + collectionName);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        log.info("Terms:" + termMap.size());
        if (terms.size() != termMap.size()) {
            log.info("Not all terms added to DB");
        }

        return termMap;
    }

    public void addToTermsCollections(Set<String> terms) {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        log.info("Add terms to " + collectionName);
        int numberOfAdded = 0;
        try {
            connection = cp.takeConnection();
            for (String name : terms) {
                String query = "INSERT INTO `" + collectionName + "` (HEAD_WORDS ,SRC_ID) VALUES(?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, name);
//                ps.setString(2, "http://computerlanguage.com/terms/");
                ps.setInt(2, sourceId);
                ps.execute();
                log.debug("Added new row (" + numberOfAdded + ":" + name + ") to table:" + collectionName);
                numberOfAdded++;
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        if (terms.size() != numberOfAdded) {
            log.info("Not all terms added to DB");
        }
    }

    public Map<String, Integer> getAllTerms() {
        Map<String, Integer> termMap = new HashMap<String, Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            String query = "SELECT * FROM `" + collectionName + "`";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String name = resultSet.getString("HEAD_WORDS");
                // TODO : sometimes case of name is important!
                // it was resolved in RelationsCollector BUT some problems may occur in other classes
                termMap.put(name, id);
                log.debug("Get row (" + Integer.valueOf(id) + ":" + name + ")");
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        log.info("Terms:" + termMap.size());
        return termMap;
    }


    public TermEntity getTermById(int id) {
        TermEntity entity = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            String query = "SELECT * FROM `" + collectionName + "` WHERE ID = " + id;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                String name = resultSet.getString("HEAD_WORDS");
                entity = new TermEntity(id, name);
                log.debug("Get row (" + id + ":" + name + ")");
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return entity;
    }


    public TermEntity getTermByName(String name) {
        TermEntity entity = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE `HEAD_WORDS` = ?");
            ps.setString(1, name);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
//                entity = new TermEntity(id, name);
                entity = new TermEntity(id, resultSet.getString("HEAD_WORDS"));
                log.debug("Get row (" + id + ":" + name + ")");
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return entity;
    }

    /**
     * Extract term with the same definition
     *
     * @return map contains pair synonyms
     */
    public Map<String, String> extractSynonyms() {
        Map<String, String> termMap = new HashMap<String, String>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "select terms_comp_definition.TERM_ID , terms_comp_link.TERM_LINK_ID " +
                    "from terms_comp_definition , terms_comp_link " +
                    "where terms_comp_definition.DEF_ID=terms_comp_link.DEF_ID and terms_comp_definition.DEF=''";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int id = resultSet.getInt("terms_comp_definition.TERM_ID");
                int idLink = resultSet.getInt("terms_comp_link.TERM_LINK_ID");
                termMap.put(getTermById(id).getHeadWord(), getTermById(idLink).getHeadWord());
                log.debug("Get row (" + id + ":" + idLink + ")");
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        log.info("Synonyms terms:" + termMap.size());
        return termMap;
    }

    /**
     * Extract terms, which have more than one definition
     *
     * @return list of id polysemous terms
     */
    public List<Integer> getAllPolysemousTerm() {
        List<Integer> listResult = new ArrayList<Integer>();
        Map<String, Integer> allTerms = getAllTerms();
        int i = 0;
        for (String term : allTerms.keySet()) {
            System.out.println(i + " in " + allTerms.size());
            int id = allTerms.get(term);
            if (isPolysemousTerm(id)) {
                listResult.add(id);
            }
            i++;
        }
        return listResult;
    }

    public List<Integer> getAllUnambiguousTerms() {
        List<Integer> listResult = new ArrayList<Integer>();
        Map<String, Integer> allTerms = getAllTerms();
        int i = 0;
        for (String term : allTerms.keySet()) {
            int id = allTerms.get(term);
            if (isUnambiguousTermWithDef(id)) {
                listResult.add(id);
            }
            i++;
        }
        return listResult;
    }

    /**
     * Check that the term has only one not null definition
     *
     * @param idTerm id term
     * @return check result
     */
    public boolean isUnambiguousTermWithDef(int idTerm) {
        boolean res = false;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        int i = 0;
        try {
            connection = cp.takeConnection();
            String query = "SELECT * FROM `terms_comp_definition` WHERE `TERM_ID`= " + idTerm;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();
            String def = null;
            while (resultSet.next()) {
                def = resultSet.getString("DEF");
                i++;
            }
            if (i == 1 && !def.equals(" ")) {
                res = true;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return res;
    }

    /**
     * Check that the term has more than one definition
     *
     * @param idTerm id term
     * @return check result
     */
    public boolean isPolysemousTerm(int idTerm) {
        boolean res = true;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        int i = 0;
        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `terms_comp_definition` WHERE `TERM_ID`= ?");
            ps.setInt(1, idTerm);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                i++;
            }
            if (i == 1) {
                res = false;
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return res;

    }

    public List<String> getUniqueTermsWithNullDef() throws SQLException, ClassNotFoundException {
        List<String> listUniqueTerms = new ArrayList<String>();
        Map<String, Integer> allTerms = getAllTerms();
        int size = allTerms.size();
        int i = 0;
        for (String name : allTerms.keySet()) {
            log.debug(i + " in " + size);
            Integer id = allTerms.get(name);
            if (isTermDefinitionNull(id)) {
                listUniqueTerms.add(id.toString());
            }
            i++;
        }
        return listUniqueTerms;
    }

    /**
     * Returns true if, and only if, term definition is empty
     *
     * @param id
     * @return
     */
    public boolean isTermDefinitionNull(int id) throws SQLException, ClassNotFoundException {
        TermDefinitionDao dao = new TermDefinitionDao();
        List<TermDefinitionEntity> termDefinitionEntities = dao.getTermDefinitionByTermID(id);
        if (termDefinitionEntities.size() == 1) {
            if (termDefinitionEntities.get(0).getDefinition().equals(" ")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method return list of id terms which don't have definition
     *
     * @param idTermsWithDef list of id terms wich have definition
     * @return
     */
    public List<String> getTermWithoutDef(List<Integer> idTermsWithDef) {
        List<String> listTermWithoutDef = new ArrayList<String>();
        Map<String, Integer> allTerms = getAllTerms();
        for (Map.Entry<String, Integer> entry : allTerms.entrySet()) {
            if (!idTermsWithDef.contains(entry.getValue())) {
                listTermWithoutDef.add(entry.getKey());
            }
        }
        return listTermWithoutDef;
    }

    public Map<Integer, String> constructMapTermByIds(Set<Integer> idsTerms) {
        Map<Integer, String> termMap = new HashMap<Integer, String>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            for (Integer idTerm : idsTerms) {
                ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "`  WHERE ID = ?");
                ps.setInt(1, idTerm);
                ps.execute();
                resultSet = ps.getResultSet();

                while (resultSet.next()) {
                    String name = resultSet.getString("HEAD_WORDS");
                    termMap.put(idTerm, name);
                }
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        log.info("Terms:" + termMap.size());
        return termMap;
    }

    public int getTermByNameLike(String referencingDef) {
        try (Connection connection = cp.takeConnection()) {

            String query = "SELECT * FROM " + collectionName + " WHERE HEAD_WORDS LIKE ? ORDER BY CHAR_LENGTH(HEAD_WORDS)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, referencingDef + " %");
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            while (resultSet.next()) {
                if (resultSet.getString("HEAD_WORDS").contains(referencingDef))
                    return resultSet.getInt("ID");
            }

            resultSet.first();

            if (resultSet.next())
                return resultSet.getInt("ID");
            else
                return 0;

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return 0;
    }
}

