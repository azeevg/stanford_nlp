package sesa.tools.filters.WIKI.text;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.SortedSet;
import java.util.TreeSet;

public class WIKIExtractor {
    private static final int EXTRACTION_VALUE_LIMIT = 1000;

    private String input;
    private String title;
    private int extractMistakes = 0;


    public WIKIExtractor(String input, String title) {
        this.input = input;
        this.title = title;
    }

    private boolean isKey(String[] keys, String k) {
        for (String key : keys)
            if (key.equals(k))
                return true;

        return false;
    }

    private int startsWith(String[] keys, String k) {
        for (String key : keys)
            if (k.toLowerCase().startsWith(key.toLowerCase()))
                return key.length();

        return -1;
    }

    private void addTwoSkips(SortedSet<KeyWordPos> skipWordSet, KeyWordPos keyWord) {
        skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos()));
        skipWordSet.add(new KeyWordPos(keyWord.getEndPos(), keyWord.getEndPos() + keyWord.getEndSyms().length));
    }

    private void addOneSkip(SortedSet<KeyWordPos> skipWordSet, KeyWordPos keyWord) {
        skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getEndPos() + keyWord.getEndSyms().length));
    }

    public HashMap<String, Object> extract(SortedSet<KeyWordPos> keyWordSet) throws WIKIExtractImportException {

        HashMap<String, Object> extracts = new HashMap<String, Object>();
        try {
            SortedSet<KeyWordPos> skipWordSet = new TreeSet<KeyWordPos>();

            int skipUntilPos = 0;
            for (KeyWordPos keyWord : keyWordSet) {

                if (keyWord.getStartPos() < skipUntilPos)
                    continue;

                String key = keyWord.getSymbols().getKeyWord();
                if (isKey(RuleInitializer.REMOVE_TAG_KEYS, key)) {
                    addTwoSkips(skipWordSet, keyWord);
                    if (!key.equals(RuleInitializer.WRAP_KEY))
                        addExtract(extracts, key, input.substring(keyWord.getStartPos(), keyWord.getEndPos()), keyWord.isNotWellFormed());

                } else if (isKey(RuleInitializer.HTML_TAGS, key)) {
                    String tagValue = input.substring(keyWord.getStartPos(), keyWord.getEndPos());
                    int index = tagValue.indexOf(">");
                    skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos() + index + 1));
                    skipWordSet.add(new KeyWordPos(keyWord.getEndPos(), keyWord.getEndPos() + keyWord.getEndSyms().length));
                } else if (isKey(RuleInitializer.SINGLE_HTML_TAGS, key)) {
                    addOneSkip(skipWordSet, keyWord);
                } else if (isKey(RuleInitializer.REMOVE_KEYS, key)) {
                    addOneSkip(skipWordSet, keyWord);
                } else if (isKey(RuleInitializer.SKIP_INSIDE_KEYS, key)) {

                    addOneSkip(skipWordSet, keyWord);
                    skipUntilPos = keyWord.getEndPos();
                } else if (key.equals("w_link")) {

                    String linkValue = input.substring(keyWord.getStartPos(), keyWord.getEndPos());
                    int keyLen;

                    if (!linkValue.contains(":") && !linkValue.contains("|")) {
                        addTwoSkips(skipWordSet, keyWord);
                        addExtract(extracts, RuleInitializer.LINK_KEY, input.substring(keyWord.getStartPos(), keyWord.getEndPos()), keyWord.isNotWellFormed());
                    } else if (startsWith(RuleInitializer.IMAGE_LINKS, linkValue) != -1) {
                        addOneSkip(skipWordSet, keyWord);
                        skipUntilPos = keyWord.getEndPos();
                    } else if ((keyLen = startsWith(RuleInitializer.MEDIA_AND_HELP_LINKS, linkValue)) != -1) {
                        int index = linkValue.indexOf("|");
                        if (index != -1) {
                            skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos() + index + 1));
                            skipWordSet.add(new KeyWordPos(keyWord.getEndPos(), keyWord.getEndPos() + keyWord.getEndSyms().length));
                        } else {
                            addOneSkip(skipWordSet, keyWord);
                            skipUntilPos = keyWord.getEndPos();
                        }
                    } else if ((keyLen = startsWith(RuleInitializer.DICT_LINKS, linkValue)) != -1) {
                        int index = linkValue.indexOf("|");
                        if (index != -1) {
                            skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos() + index + 1));
                            skipWordSet.add(new KeyWordPos(keyWord.getEndPos(), keyWord.getEndPos() + keyWord.getEndSyms().length));
                        } else {
                            skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos() + keyLen));
                            skipWordSet.add(new KeyWordPos(keyWord.getEndPos(), keyWord.getEndPos() + keyWord.getEndSyms().length));
                        }
                    } else if ((keyLen = startsWith(RuleInitializer.CATEGORY_LINKS, linkValue)) != -1) {
                        addOneSkip(skipWordSet, keyWord);
                        addExtract(extracts, RuleInitializer.CATEGORIES_KEY, linkValue.substring(keyLen), keyWord.isNotWellFormed());
                    } else if (linkValue.contains(":")) {

                        /* Countries or undefined */
                        addOneSkip(skipWordSet, keyWord);
                        skipUntilPos = keyWord.getEndPos();
                    } else {
                        int index = linkValue.indexOf("|");
                        if (index != -1) {
                            int bracketIndex = linkValue.substring(index).indexOf("(");
                            int closeBracketIndex = linkValue.substring(index).indexOf(")");

                            if (bracketIndex != -1 && closeBracketIndex != -1) {
                                skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos() + index + 1));
                                skipWordSet.add(new KeyWordPos(keyWord.getStartPos() + index + 1 + bracketIndex + 1, keyWord.getEndPos() + keyWord.getEndSyms().length));
                                addExtract(extracts, RuleInitializer.LINK_KEY, input.substring(keyWord.getStartPos() + index + 1, keyWord.getStartPos() + index + 1 + bracketIndex + 1), keyWord.isNotWellFormed());
                            } else {
                                skipWordSet.add(new KeyWordPos(keyWord.getStartPos() - keyWord.getStartSyms().length, keyWord.getStartPos() + index + 1));
                                skipWordSet.add(new KeyWordPos(keyWord.getEndPos(), keyWord.getEndPos() + keyWord.getEndSyms().length));
                                addExtract(extracts, RuleInitializer.LINK_KEY, input.substring(keyWord.getStartPos() + index + 1, keyWord.getEndPos()), keyWord.isNotWellFormed());
                            }
                        } else {
                            addOneSkip(skipWordSet, keyWord);
                            skipUntilPos = keyWord.getEndPos();
                        }
                    }
                }
            }

            StringBuilder builder = new StringBuilder();
            char[] chs = input.toCharArray();
            int prevPos = 0, nextPos;

            for (KeyWordPos skipPos : skipWordSet) {
                nextPos = skipPos.getStartPos();
                for (int i = prevPos; i < nextPos; i++)
                    builder.append(chs[i]);

                prevPos = skipPos.getEndPos();
            }
            for (int i = prevPos; i < chs.length; i++)
                builder.append(chs[i]);


            String filteredText = builder.toString();
            filteredText = cleanUpNonBreaking(filteredText);
            filteredText = trimLineBreaks(filteredText);

            addExtract(extracts, RuleInitializer.FILTERED_TEXT_KEY, filteredText, false);
        } catch (Exception e) {
            throw new WIKIExtractImportException(e.getMessage(), title);
        }
        if (extractMistakes > 0) {
            addExtract(extracts, RuleInitializer.ERROR_KEY, "" + extractMistakes, false);
            //LoggingUtility.debug("Extraction value limit exceed " + extractMistakes + " times" + " | title = " + title);
        }

        return extracts;
    }

    private void addExtract(HashMap<String, Object> extracts, String key, String value, boolean isNotWellFormed) throws WIKIExtractImportException {
        if (isNotWellFormed)
            return;

        if (!extracts.containsKey(key))
            extracts.put(key, new LinkedList<String>());

        if (!key.equals(RuleInitializer.FILTERED_TEXT_KEY) && value.length() > EXTRACTION_VALUE_LIMIT) {
            extractMistakes++;
            return;
        }

        LinkedList<String> v = (LinkedList<String>) extracts.get(key);
        v.add(value.replaceAll("[\\[\\]]", "").replaceAll("\\|", " ").trim());
    }

    private String cleanUpNonBreaking(String text) {
        return text.replace("&nbsp;", "");
    }

    public String trimLineBreaks(String cleanedText) {
        return cleanedText.replaceAll("\\n+", "\n");
    }
}
