package sesa.common.utility.io;

import edu.stanford.nlp.ling.StringLabelFactory;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.LabeledScoredTreeFactory;
import edu.stanford.nlp.trees.PennTreeReader;
import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Sentence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Scanner;

public class CommonUtils {

    /**
     * Read parse tree from string
     *
     * @param pennString    string contains parse tree
     * @return              parse tree in normal format
     * @throws IOException
     */
    public static Tree readPennTreeFormString(String pennString) throws IOException {
        return (new PennTreeReader(new StringReader(pennString), new LabeledScoredTreeFactory(new StringLabelFactory()))).readTree();
    }



}
