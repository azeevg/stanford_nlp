package sesa.tools.cache;

import edu.stanford.nlp.io.IOUtils;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BulkWriteTreeDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.database.dao.CounterDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.vector.DocumentUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.List;

public class CacheUtils {

    private static final String COUNTER_KEY = "tree_cache";


    @Deprecated
    public static void rebuildCategoryCache(String categoryName/*, DBController dbController*/) throws IOException, SQLException, ClassNotFoundException {
        CounterDao counterDao = new CounterDao();
        ObjectInputStream inputStream = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz");
        counterDao.resetCounter(COUNTER_KEY);
        //  dbController.resetCounter(COUNTER_KEY);
        String cacheTable = "tree_cache_" + categoryName;
        StanfordDataModule dataModule = new BulkWriteTreeDataModule(false, cacheTable);
        //new WriteTreeDataModule(true, cacheTable);

        DocumentBuilder stanfordBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
        DocumentUtils.iterateDocumentsWithRecovery(categoryName, stanfordBuilder, cacheTable);
    }

    public static void buildCategoryCache(String categoryName) throws IOException, SQLException, ClassNotFoundException {
        boolean recoverRequired = true;
        ObjectInputStream inputStream = null;
        StanfordDataModule dataModule = null;
        DocumentBuilder stanfordBuilder = null;
        while (recoverRequired) {
            FileManager fileManager = new FileManager("path");
            inputStream = IOUtils.readStreamFromString(fileManager.getValue("path.smallLanguageModel"));

            String cacheTable = "tree_cache_" + categoryName;

            dataModule = new BulkWriteTreeDataModule(false, cacheTable);
            //new WriteTreeDataModule(false, cacheTable);

            //
            stanfordBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
            recoverRequired = DocumentUtils.iterateDocumentsWithRecovery(categoryName, stanfordBuilder, cacheTable);
            if (recoverRequired) {
                LoggingUtility.error("RECOVERY STARTED");
              /*  inputStream = null;
                dataModule = null;
                stanfordBuilder = null;*/
                System.gc();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.gc(); // garbage collector
                LoggingUtility.error("RECOVERY COMPLETED");
            }
        }
    }

    public static void generatePaintedDocument(IDocument document) {
        List<SentenceTree> trees = document.getSentences();
        String paintedHtml = DocumentUtils.buildPaintedHtml(trees);
        String filePath = "./data/c_colored_pages/colored_pages/" + document.getTitle() + ".html";
        FileUtility.writeAllTextToFile(filePath, paintedHtml);
        LoggingUtility.info("Generated html colored file = " + filePath);
    }

    public static void generatePaintedDocument(IDocument document, String title) {
        List<SentenceTree> trees = document.getSentences();
        String paintedHtml = DocumentUtils.buildPaintedHtml(trees);
        String filePath = "./data/c_colored_pages/colored_pages/" + title + ".html";
        FileUtility.writeAllTextToFile(filePath, paintedHtml);
        LoggingUtility.info("Generated html colored file = " + filePath);
    }
}
