package sesa.common.utility.io;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.interfaces.IProcessor;
import sesa.common.utility.logging.LoggingUtility;

import java.io.*;
import java.util.*;

/**
 * Class for working with file: read/write text
 */
public class FileUtility {

    public static final Logger log = LogManager.getLogger(FileUtility.class);

    public static long getFileSize(String filename) {
        File f = new File(filename);
        return f.length();
    }

    /**
     * Write text to the file
     * @param filePath path to the file
     * @param text     line for writing to the file
     */
    public static void writeAllTextToFile(String filePath, String text) {
        FileWriter outFile = null;
        try {
            outFile = new FileWriter(filePath);
            PrintWriter out = new PrintWriter(outFile);
            out.print(text);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param filePath path to the file
     * @param lines collection of strings for writing to the file
     */
    public static void writeAllLinesToFile(String filePath, Collection<String> lines) {
        FileWriter outFile = null;
        try {
            outFile = new FileWriter(filePath);
            PrintWriter out = new PrintWriter(outFile);
            for (String line : lines)
                out.println(line.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean createFile(String name) {
        try {
            FileWriter fileWriter = new FileWriter(name);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.flush();
            bufferedWriter.close();
            fileWriter.close();
        } catch (Exception ignored) {
            return false;
        }

        return true;
    }

    /**
     * Read all lines from file to list of lines
     * @param fileName
     * @return List of lines from file
     * @throws IOException
     */
    public static List<String> readAllLines(String fileName) throws IOException {

        List<String> lines = new LinkedList<String>();
        BufferedReader reader = null;
        reader = new BufferedReader(new FileReader(fileName));
        String text = null;

        while ((text = reader.readLine()) != null) {
            if (text.length() > 0)
                lines.add(text.trim()); // Remove the extra spaces in the string
        }
        reader.close();
        return lines;
    }

    /**
     * Translation of characters in the file strings to lowercase
     * @param filePath ���� �� �����
     * @return ��������� �����
     */
    public static Set<String> loadLinesLowerToSet(String filePath) {
        try {
            Set<String> set = new HashSet<String>();

            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String text = null;

            while ((text = reader.readLine()) != null) {
                if (text.length() > 0)
                    set.add(text.toLowerCase().trim());
            }
            reader.close();
            return set;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Read all text from file to String
     * @param filePath
     * @return the line of text from a file with name: filePath
     */
    public static String readAllText(String filePath) {
        File file = new File(filePath);
        StringBuffer contents = new StringBuffer();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            // repeat until all lines is read
            while ((text = reader.readLine()) != null) {
                contents.append(text)
                        .append(System.getProperty(
                                "line.separator"));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return contents.toString();
    }

    public static void write(String fileName, Collection<String> list) {
        try {
            FileWriter fileWriter = new FileWriter(fileName,true);
            BufferedWriter BWf = new BufferedWriter(fileWriter);
            for(String s:list){
                BWf.write(s); // дозаписываем строку в конец файла
                BWf.newLine();     // переходим на следующую строку
            }

            BWf.close();
            fileWriter.close();

        } catch (IOException e) {
            log.error(e.getMessage());
        }


    }

    /**
     * Append to file text
     * @param fileName
     * @param text
     */
    public static void write(String fileName, String text) {
        try {
            FileWriter fileWriter = new FileWriter(fileName,true);
            BufferedWriter BWf = new BufferedWriter(fileWriter);
            BWf.write(text); // дозаписываем строку в конец файла
            BWf.newLine();     // переходим на следующую строку
            BWf.close();
            fileWriter.close();

        } catch (IOException e) {
            log.error(e.getMessage());
        }


    }

    /**
     * Read list of stings from file to array
     * @param fileName
     * @return
     */
    public static ArrayList<String> read(String fileName){

        ArrayList<String> strings = new ArrayList<String>();
        try{
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                strings.add(in.nextLine());
            }
        }catch(FileNotFoundException e){
            log.error("File " + fileName + " not found.\n" + e.getMessage());
        }

        return strings;
    }



}
