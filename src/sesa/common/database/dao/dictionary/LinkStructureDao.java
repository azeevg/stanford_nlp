package sesa.common.database.dao.dictionary;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.CommonDao;
import sesa.tools.semantic.semanticTree.SemanticEdge;
import sesa.common.database.entity.SemanticGroupEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class work with table which contains the next columns ID_TERM`, `ID_SENTENCE`, `ID_LINK_TERM`
 * For example:"link_comp_structure" , "auto_comp_reference"
 * Created by Ira on 05.06.2016.
 */
public class LinkStructureDao extends CommonDao {
    public final Logger log = LogManager.getLogger(SemanticStructureDao.class);

    public LinkStructureDao() throws SQLException, ClassNotFoundException {
        super();
    }

    //TODO: write code for create table
    public void createTable(String collectionName) {

    }

    public void addToLinkStructure(SemanticGroupEntity dto, String collectionName) {
        PreparedStatement ps = null;
        Connection connection = null;
        log.info("Add terms to " + collectionName);
        int i = 0;
        boolean added = true;
        try {
            connection = cp.takeConnection();

            for (Integer idLink : dto.getTermLink()) {
                log.debug("Added new row: " + dto.toString() + "to table:" + collectionName);
                if (idLink == null) {
                    added = false;
                    continue;
                }
                String query = "INSERT INTO `" + collectionName + "` (`ID_TERM`, `ID_SENTENCE`, `ID_LINK_TERM`) VALUES(?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setInt(1, dto.getIdTerm());
                ps.setInt(2, dto.getSentenceId());
                ps.setInt(3, idLink);

                ps.execute();
                log.debug("Added completed");
                i++;
                added = true;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            if (added) {
                cp.closeConnection(connection, ps);
            } else {
                cp.closeConnection(connection);
            }
        }

        log.info("Added __" + i + "__ rows ");
    }

    public SemanticGroupEntity getListSemanticGroupDtoByTermID(int idTerm, String collectionName) {
        SemanticGroupEntity dto = new SemanticGroupEntity();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "` WHERE `ID_TERM` = " + idTerm;
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            List<Integer> listLinks = new ArrayList<Integer>();


            while (resultSet.next()) {
                int idSentence = resultSet.getInt("ID_SENTENCE");
                int idLinkTerm = resultSet.getInt("ID_LINK_TERM");
                listLinks.add(idLinkTerm);
                dto.setIdTerm(idTerm);
                dto.setSentenceId(idSentence);

            }
            dto.setTermLink(listLinks);
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return dto;
    }


    public void addToLinkStructureOneRow(int idTerm, int idSentence, int idLink, String collectionName) {
        PreparedStatement ps = null;
        Connection connection = null;
        log.info("Add terms to " + collectionName);
        int i = 0;
        boolean added = false;
        try {
            connection = cp.takeConnection();
            log.debug("Added new row: " + idTerm + " , link: " + idLink + " to table:" + collectionName);
            String query = "INSERT INTO `" + collectionName + "` (`ID_TERM`, `ID_SENTENCE`, `ID_LINK_TERM`) VALUES(?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setInt(1, idTerm);
            ps.setInt(2, idSentence);
            ps.setInt(3, idLink);
            ps.execute();
            log.debug("Added completed");
            i++;
            added = true;

        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            if (added) {
                cp.closeConnection(connection, ps);
            } else {
                cp.closeConnection(connection);
            }
        }

        log.info("Added __" + i + "__ rows ");
    }

    /**
     * Returned number of nodes in collectionName
     * It's not a COUNT(*), because we need only unique values in ID_TERM and ID_LINK_TERM
     */
    public int getNumberVertex(String collectionName) {
        return getAllVertex(collectionName).size();
    }

    /**
     * Returned collection vertex in graph
     *
     * @param collectionName table name
     */
    public Set<Integer> getAllVertex(String collectionName) {
        Set<Integer> set = new HashSet<Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            String query = "SELECT * FROM `" + collectionName + "`";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();
            while (resultSet.next()) {
                set.add(resultSet.getInt("ID_TERM"));
                set.add(resultSet.getInt("ID_LINK_TERM"));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return set;
    }

    public List<SemanticEdge> getAllEdges(String collectionName) {
        List<SemanticEdge> semanticEdges = new ArrayList<SemanticEdge>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();

            String query = "SELECT * FROM `" + collectionName + "`";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int source = resultSet.getInt("ID_TERM");
                int target = resultSet.getInt("ID_LINK_TERM");
                semanticEdges.add(new SemanticEdge(source, target));
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return semanticEdges;
    }
}
