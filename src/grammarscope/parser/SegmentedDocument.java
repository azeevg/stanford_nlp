package grammarscope.parser;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import edu.stanford.nlp.ling.Word;
import sesa.common.utility.strings.StringUtility;

public class SegmentedDocument
{
	/**
	 * List of words
	 */
	public List<Word> theWords;

	/**
	 * List of sentences
	 */
	public List<Sentence> theSentences;

	/**
	 * Word-to-sentence map
	 */
	public Map<Word, Sentence> theWord2SentenceMap;

	// I T E R A T I O N

	/**
	 * Word iterator
	 */
	private ListIterator<Word> theWordIterator;

	/**
	 * Sentence iterator
	 */
	private ListIterator<Sentence> theSentenceIterator;

	/**
	 * Current word
	 */
	private Word theCurrentWord;

	/**
	 * Current sentence
	 */
	private Sentence theCurrentSentence;

	/**
	 * Document path
	 */
	private String thePath;

	/**
	 * Constructor
	 */
	public SegmentedDocument()
	{
		// do nothing
	}

	/**
	 * Initialize iterator/maps
	 */
	protected void initialize()
	{
		this.theSentences = Parser.getSentences(this.theWords);
		this.theWord2SentenceMap = new IdentityHashMap<Word, Sentence>();
		for (final Sentence thisSentence : this.theSentences)
		{
			for (final Word thisWord : thisSentence)
			{
				this.theWord2SentenceMap.put(thisWord, thisSentence);
			}
		}
		this.theWordIterator = this.theWords.listIterator();
		this.theSentenceIterator = this.theSentences.listIterator();
	}

	/**
	 * Create document from URL
	 * 
	 * @param thisUrl
	 *            source URL
	 * @throws IOException
	 */
	/*public void create(final URL thisUrl) throws IOException
	{
		this.thePath = thisUrl.toString();
		this.theWords = Parser.getDocument(thisUrl);
		initialize();
	} */

	/**
	 * Create document from text
	 * 
	 * @param thisText
	 *            text
	 */
//	public void create(final String thisText)
//	{
//		this.thePath = "<pasted>";
//		this.theWords = Parser.getDocument(thisText);
//		System.out.println(this.theWords);
//		initialize();
//	}

	public void create(final String thisText) throws SQLException, ClassNotFoundException {
		this.thePath = "<pasted>";
		String text = StringUtility.replacePoint(thisText); // replace . to #
		this.theWords = Parser.getDocument(text);
		this.theWords = StringUtility.updateWordList(this.theWords);
		initialize();


	}

	// I T E R A T I O N

	/**
	 * Next word
	 * 
	 * @return next word
	 */
	public Word nextWord()
	{
		if (this.theWordIterator.hasNext())
		{
			this.theCurrentWord = this.theWordIterator.next();
		}
		return this.theCurrentWord;
	}

	/**
	 * Previous word
	 * 
	 * @return previous word
	 */
	public Word previousWord()
	{
		if (this.theWordIterator.hasPrevious())
		{
			this.theCurrentWord = this.theWordIterator.previous();
		}
		return this.theCurrentWord;
	}

	/**
	 * Current word
	 * 
	 * @return current word
	 */
	public Word currentWord()
	{
		return this.theCurrentWord;
	}

	/**
	 * Next sentence
	 * 
	 * @return next sentence
	 */
	public Sentence nextSentence()
	{
		if (this.theSentenceIterator.hasNext())
		{
			this.theCurrentSentence = this.theSentenceIterator.next();
		}
		return this.theCurrentSentence;
	}

	/**
	 * Previous sentence
	 * 
	 * @return previous sentence
	 */
	public Sentence previousSentence()
	{
		if (this.theSentenceIterator.hasPrevious())
		{
			this.theCurrentSentence = this.theSentenceIterator.previous();
		}
		return this.theCurrentSentence;
	}

	/**
	 * Set sentence
	 * 
	 * @return selected sentence
	 */
	public Sentence setSentence(final Sentence thisSentence)
	{
		this.theSentenceIterator = this.theSentences.listIterator(this.theSentences.indexOf(thisSentence));
		this.theCurrentSentence = thisSentence;
		return this.theCurrentSentence;
	}

	/**
	 * Current sentence
	 * 
	 * @return current sentence
	 */
	public Sentence currentSentence()
	{
		return this.theCurrentSentence;
	}

	/**
	 * Get document path
	 * 
	 * @return document path
	 */
	public String getPath()
	{
		return this.thePath;
	}
}
