package sesa.application;

import edu.stanford.nlp.io.IOUtils;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.ReadTreeDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.configuration.Categories;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.CommonDao;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.database.dao.SemanticGroupDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.vector.DocumentUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Ira on 16.03.2016.
 */

/**
 * Show semantic group
 * use COMPUTING_CATEGORY
 */
public class TestSemanticGroup extends Application {
    String categoryName = Categories.COMPUTING_CATEGORY;
    private DocumentsDao documentsDao = new DocumentsDao();
    private StanfordDataModule dataModule;
    private DocumentBuilder documentBuilder;
    private SemanticGroupDao semanticGroupDao = new SemanticGroupDao();
    private CommonDao commonDao = new CommonDao();
    private ObjectInputStream inputStream;
    private String groupTable;
    private static final int SENTENCE_SIZE_LOWER_LIMIT = 0;
    private static final int SENTENCE_SIZE_UPPER_LIMIT = 50;

    public TestSemanticGroup() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() {

        try {

            LoggingUtility.info("SAVING GROUPS FOR CATEGORY  = " + categoryName);
            printSemanticGroups();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void init() throws IOException, SQLException, ClassNotFoundException {
        inputStream = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz");
        String cacheTable = "tree_cache_" + categoryName;
        dataModule = new ReadTreeDataModule(cacheTable);
        documentBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
        inputStream = null;

    }

    private void printSemanticGroups() throws IOException, SQLException, ClassNotFoundException {
        String groupTable = "semantic_groups_" + categoryName;
        init();

        try {

            String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
            long collectionSize = commonDao.countTotalOfRow(collectionName);
            IDBIterator iterator = commonDao.getDocumentsCollectionIterator(collectionName);

            int counter = 0;
            while (iterator.hasNext()) {
                counter++;
                IDBEntity entity = iterator.next().get();
                try {
                    String title = (String) entity.toMap().get("title");
                    IDocument document = DocumentUtils.createDocument(entity, documentBuilder);
                    List<SentenceTree> sentences = document.getSentences();
                    System.out.println("Title:" + title + ":" + "sentenceSize" + sentences.size());


                    if (sentences.size() > SENTENCE_SIZE_LOWER_LIMIT && sentences.size() <= SENTENCE_SIZE_UPPER_LIMIT) {
                        for (SentenceTree sentence : sentences) {
                            for (SemanticGroup groups : sentence.getSemanticGroups()) {
                                printSemanticGroup(groups);
                                System.out.println();
                            }
                        }
                    }

                } catch (Exception e) {

                }

            }


        } catch (Exception e) {

        }
    }

    private void printSemanticGroup(SemanticGroup group) {

        String groupId = UUID.randomUUID().toString();
        System.out.println(group.getSentenceTree().getSentence().hashCode());
        System.out.println(group.getSentenceTree().getSentence());
        for (SentenceToken token : group.getTokens()) {
            for (SemanticRelation relation : token.getRelations()) {
                SentenceTree sentence = group.getSentenceTree();
                String value = token.getBaseValue();
                System.out.print(token.getBaseValue() + ",");
            }
        }
    }
}





