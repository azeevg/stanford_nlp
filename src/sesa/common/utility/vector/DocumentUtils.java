package sesa.common.utility.vector;


import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;
import sesa.common.configuration.Categories;
//import sesa.common.database.common.DBController;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.dao.*;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.model.processing.document.IDocument;
//import sesa.common.model.json.JSemanticGroup;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.model.processing.support.PositionComparator;
import sesa.common.model.processing.support.RelationMistake;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.dictionary.DictionaryUtils;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.builder.BuilderException;
import sesa.common.builder.DictionaryDocumentBuilder;
import sesa.common.builder.DocumentBuilder;
import sesa.common.semantics.RelationInfo;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Класс для построения html страницы
 */
public class DocumentUtils {

    public static List<IDocument> loadDocuments(/*DBController controller, */String categoryName) throws SQLException, ClassNotFoundException {
        CommonDao commonDao = new CommonDao();
        List<IDocument> documents = new LinkedList<IDocument>();
        String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
        Set<String> categoryDictionary = DictionaryUtils.loadCategoryDictionary(categoryName);
        IDBIterator iterator = commonDao.getDocumentsCollectionIterator(collectionName);//controller.getDocumentsCollectionIterator(collectionName);
        //iterator = new DBReportingIterator(iterator, "Loading vectors for category " + collectionName);
        while (iterator.hasNext()) {
            IDBEntity entity = iterator.next().get();
            try {
                documents.add(createDocument(entity, categoryDictionary));
            } catch (Exception e) {
                String title = (String) entity.toMap().get("title");
                String id = entity.getID();
                LoggingUtility.error("error in title = " + title + " id = " + id);
            }
        }
        return documents;
    }

    public static IDocument createDocument(IDBEntity entity, Set<String> categoryDictionary) throws BuilderException{
        Map<String, Object> map = entity.toMap();
        String text = (String) map.get("w_filtered_text");
        String id = entity.getID();
        // DocumentBuilder builder = new DictionaryDocumentBuilder();
        DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
       // return DictionaryDocumentBuilder.buildDocument(id, text, categoryDictionary);
        return documentBuilder.buildDocument(id, text, categoryDictionary);
    }

    public static boolean iterateDocumentsWithRecovery(String categoryName, DocumentBuilder builder, String cacheTable) throws SQLException, ClassNotFoundException {
        CommonDao commonDao = new CommonDao();
        ErrorDocumentDao errorDocumentDao = new ErrorDocumentDao();
        CounterDao counterDao = new CounterDao();
        long start, end;
        float time;
        int count = 0;
        String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
        IDBIterator iterator = commonDao.getDocumentsCollectionIterator(collectionName);//controller.getDocumentsCollectionIterator(collectionName);
        iterator = new DBReportingIterator(iterator, "Loading vectors for category " + collectionName);

        String counterName = cacheTable;
        int startPos = counterDao.getCounter(counterName);
        LoggingUtility.info("Starting from " + startPos);
        while (iterator.hasNext()) {

            IDBEntity entity = iterator.next().get();
            String title = (String) entity.toMap().get("title");
            if (count < startPos) {
                count++;
                LoggingUtility.info("Skipped " + title);
                continue;
            }


            start = System.currentTimeMillis();
            //IDBEntity entity = iterator.next().get();
            //String title = (String) entity.toMap().get("title");
            try {

                LoggingUtility.info("Building document for '" + title);
                IDocument document = createDocument(entity, builder);
                end = System.currentTimeMillis() - start;
                time = end / 1000F;
                LoggingUtility.info("Elapsed time for '" + title + "' = " + time + " seconds");

            } catch (BuilderException be) {

                System.out.println("catched simple error");
                System.out.println("title" + title);
                errorDocumentDao.writeErrorDocument(title, "builder", counterName);
                System.out.println("error logged");
            } catch (OutOfMemoryError e) {
                String id = entity.getID();
                LoggingUtility.error("Error = " + title + " id = " + id);
                counterDao.increaseCounter(counterName);
                errorDocumentDao.writeErrorDocument(title, "critical", counterName);
                LoggingUtility.error("OUT OF MEMORY: TRYING TO RECOVER");
                return true;
            } catch (Exception e) {
                String id = entity.getID();
                LoggingUtility.error("Error = " + title + " id = " + id);
                counterDao.increaseCounter(counterName);
                errorDocumentDao.writeErrorDocument(title, "critical", counterName);
                LoggingUtility.error("CRITICAL");
                e.printStackTrace();
                return true;
            }
            counterDao.increaseCounter(counterName);
            LoggingUtility.info("Counter = " + counterDao.getCounter(counterName));
        }
        return false;
    }

    public static List<IDocument> loadFirstDocuments(/*DBController controller,*/ String categoryName, int limit, DocumentBuilder builder) throws SQLException, ClassNotFoundException {
        DocumentsDao documentsDao = new DocumentsDao();
        long start, end;
        float time;
        List<IDocument> documents = new LinkedList<IDocument>();
        String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
        IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(collectionName);
        iterator = new DBReportingIterator(iterator, "Loading vectors for category " + collectionName);
        int count = 0;
        while (iterator.hasNext()) {


            if (count++ == limit)
                return documents;

            start = System.currentTimeMillis();
            IDBEntity entity = iterator.next().get();
            String title = (String) entity.toMap().get("title");
            try {

                IDocument document = createDocument(entity, builder);
                end = System.currentTimeMillis() - start;
                time = end / 1000F;
                LoggingUtility.info("Elapsed time for '" + title + "' = " + time + " seconds");
                documents.add(document);
            } catch (Exception e) {

                String id = entity.getID();
                LoggingUtility.error("error in title = " + title + " id = " + id);
            }
        }
        return documents;
    }


    public static List<IDocument> loadDocuments(/*DBController controller,*/ String categoryName, int from, int limit, DocumentBuilder builder) throws SQLException, ClassNotFoundException {
        DocumentsDao documentsDao = new DocumentsDao();
        long start, end;
        float time;
        List<IDocument> documents = new LinkedList<IDocument>();
        String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
        IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(collectionName);
        iterator = new DBReportingIterator(iterator, "Loading vectors for category " + collectionName);
        int count = 0;
        int startPos = from;
        while (iterator.hasNext()) {

            if (count++ < startPos)
                continue;

            if (count++ == limit + startPos)
                return documents;

            start = System.currentTimeMillis();
            IDBEntity entity = iterator.next().get();
            String title = (String) entity.toMap().get("title");
            try {

                IDocument document = createDocument(entity, builder);
                end = System.currentTimeMillis() - start;
                time = end / 1000F;
                LoggingUtility.info("Elapsed time for '" + title + "' = " + time + " seconds");
                documents.add(document);
            } catch (Exception e) {

                String id = entity.getID();
                LoggingUtility.error("error in title = " + title + " id = " + id);
            }
        }
        return documents;
    }

    public static IDocument loadDocumentByTitle(/*DBController controller,*/ String categoryName, String title, DocumentBuilder builder) throws SQLException, ClassNotFoundException {
        DocumentsDao documentsDao = new DocumentsDao();
        String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);
        IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(collectionName);
        iterator = new DBReportingIterator(iterator, "Loading vectors for category " + collectionName);
        while (iterator.hasNext()) {
            IDBEntity entity = iterator.next().get();
            String currentTitle = (String) entity.toMap().get("title");
            System.out.println(currentTitle);
            if (!currentTitle.equals(title))
                continue;
            try {
                return createDocument(entity, builder);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                System.out.println(e.getMessage());
                String id = entity.getID();
                LoggingUtility.error("error in title = " + title + " id = " + id);
                return null;
            }
        }
        return null;
    }

    public static IDocument createDocument(IDBEntity entity, DocumentBuilder builder) throws BuilderException {
        Map<String, Object> map = entity.toMap();
        String text = (String) map.get("w_filtered_text");//
        String id = entity.getID();
        builder.setData(text, id);
        return builder.buildDocument();
    }

    public static List<String> getTokens(List<SentenceTree> trees) {
        List<String> tokens = new LinkedList<String>();
        for (SentenceTree tree : trees) {
            for (SentenceToken token : tree.getTokens()) {
                tokens.add(token.getValue());
            }
        }
        return tokens;
    }


    public static Set<String> getUniqueTokens(List<SentenceTree> trees) {
        Set<String> tokens = new HashSet<String>();
        for (SentenceTree tree : trees) {
            for (SentenceToken token : tree.getTokens()) {
                tokens.add(token.getValue());
            }
        }
        return tokens;
    }

    public static List<List<SemanticGroup>> getSemanticGroups(List<SentenceTree> trees) {
        List<List<SemanticGroup>> relations = new LinkedList<List<SemanticGroup>>();
        for (SentenceTree tree : trees) {
            relations.add(tree.getSemanticGroups());
        }
        return relations;
    }

    public static Map<String, Object> getJSemanticGroups(List<SentenceTree> trees) {
        Map<String, Object> groups = new TreeMap<String, Object>();
        int counter = 0;
        for (SentenceTree tree : trees) {
            for (SemanticGroup group : tree.getSemanticGroups()) {
                //JSemanticGroup jGroup = new JSemanticGroup(group);
               // groups.put("group" + counter, jGroup.getRelations());
                counter++;
            }
        }
        return groups;
    }

    public static List<SentenceTriplet> getTriplets(List<SentenceTree> trees) {
        List<SentenceTriplet> triplet = new LinkedList<SentenceTriplet>();
        for (SentenceTree tree : trees) {
            triplet.add(tree.getTriplet());
        }
        return triplet;
    }


    public static String buildSemanticGroupReport(List<List<SentenceTree>> allSentences) {
        StringBuilder paintedHtmlBuilder = new StringBuilder();
        String styleDefinition = "<style>body{font-family: Courier New; font-size:11px} td{border:1px solid black;} table{font-family: Courier New; font-size:11px !important;} v {color:red;} r {color: green; vertical-align: super} o {color: indigo; vertical-align: sub}</style>";
        paintedHtmlBuilder.append("<html><head><title>");
        paintedHtmlBuilder.append("Semantic Report");
        paintedHtmlBuilder.append("</title>" + styleDefinition + "</head><body>");
        paintedHtmlBuilder.append("<table cellspacing='0' >");

        for (List<SentenceTree> sentences : allSentences) {
            for (SentenceTree sentence : sentences) {
                boolean first = true;
                for (SemanticGroup group : sentence.getSemanticGroups()) {

                    int groupSize = group.getTokens().size();
                    if (groupSize < 3 || groupSize > 7)
                        continue;

                    paintedHtmlBuilder.append("<tr><td>");
                    appendColoredSemanticGroup(group, paintedHtmlBuilder);
                    paintedHtmlBuilder.append("</td>");
                    paintedHtmlBuilder.append("<td style='width:100px'>");
                    appendPlainSemanticGroup(group, paintedHtmlBuilder);
                    paintedHtmlBuilder.append("</td>");
                    paintedHtmlBuilder.append("<td>");
                    if (first) {
                        appendMinSentence(group.getSentenceTree(), paintedHtmlBuilder);
                        first = false;
                    }
                    paintedHtmlBuilder.append("</td></tr>");
                }
            }
        }


        paintedHtmlBuilder.append("</table>");
        paintedHtmlBuilder.append("</body></html>");
        return paintedHtmlBuilder.toString();
    }

    private static void appendColoredSemanticGroup(SemanticGroup group, StringBuilder paintedHtmlBuilder) {
        for (SentenceToken token : group.getTokens()) {
            paintedHtmlBuilder.append(" " + escape(token.getValue()));
      /*for (SemanticRelation rel : token.getRelations())
    paintedHtmlBuilder.append("<r>" + escape(rel.getShortName()) + "</r>");
  paintedHtmlBuilder.append("<o>" + token.getPartOfSpeech() + "</o>");*/
        }
    }

    private static void appendPlainSemanticGroup(SemanticGroup group, StringBuilder paintedHtmlBuilder) {

        for (SentenceToken token : group.getTokens()) {
            for (SemanticRelation rel : token.getRelations())
                paintedHtmlBuilder.append(rel.getToken().getValue()).append("| ").append(rel.getShortName());
        }

    }

    private static void appendMinSentence(SentenceTree tree, StringBuilder paintedHtmlBuilder) {

        for (SentenceToken token : tree.getTokens()) {
            if (token.getRelations() != null && token.getRelations().size() > 0) {
                paintedHtmlBuilder.append("<v>" + " " + escape(token.getValue()) + "</v>");
                for (SemanticRelation rel : token.getRelations())
                    paintedHtmlBuilder.append("<r>" + escape(rel.getShortName()) + "</r>");
                paintedHtmlBuilder.append("<o>" + token.getPartOfSpeech() + "</o>");
            } else
                paintedHtmlBuilder.append(" " + escape(token.getValue()));
            //paintedHtmlBuilder.append("<v class=\" \">" + " " + escape(token.getValue()) + "</v>");
        }
    }

    /**
     * Построение html - странички
     * @param trees
     * @return
     */
    public static String buildPaintedHtml(List<SentenceTree> trees) {
        StringBuilder paintedHtmlBuilder = new StringBuilder();
        String styleDefinition = "<style>body{font-family: Courier New; font-size:11px} .s {color:red;} relation {color: green; vertical-align: super} text,hash,group,pos {display : none} break {height:10px}</style>";
        paintedHtmlBuilder.append("<html><head><title>");
        appendTitle(trees.get(0), paintedHtmlBuilder);
        paintedHtmlBuilder.append("</title>" + styleDefinition + "</head><body>");

        for (SentenceTree tree : trees) {

            if (tree.isTitle()) {
                String hTag = "h" + tree.getLevel();
                String openingTag = "<" + hTag + ">";
                String closingTag = "</" + hTag + ">";
                paintedHtmlBuilder.append(openingTag);
                appendSentence(tree, paintedHtmlBuilder);
                paintedHtmlBuilder.append(closingTag);
            } else
                appendSentence(tree, paintedHtmlBuilder);
        }
        paintedHtmlBuilder.append("</body></html>");
        return paintedHtmlBuilder.toString();
    }

    /**
     * Добавить предложение в строку
     * @param tree
     * @param paintedHtmlBuilder
     */
    private static void appendSentence(SentenceTree tree, StringBuilder paintedHtmlBuilder) {

        paintedHtmlBuilder.append("<sentence>");
        paintedHtmlBuilder.append("<hash>" + escape(tree.getSentence().hashCode() + "") + "</hash>");
        paintedHtmlBuilder.append("<text>" + escape(tree.getSentence()) + "</text>");
        paintedHtmlBuilder.append("<tokens>");
        for (SentenceToken token : tree.getTokens()) {
            paintedHtmlBuilder.append("<token>");
            if (token.getRelations() != null && token.getRelations().size() > 0) {
                paintedHtmlBuilder.append("<value class=\"s\" >" + " " + escape(token.getValue()) + "</value><relations>");
                for (SemanticRelation rel : token.getRelations())
                    paintedHtmlBuilder.append("<relation>" + escape(rel.getShortName()) + "</relation>");
                paintedHtmlBuilder.append("</relations><group>" + escape(token.getGroup().getId() + "") + "</group><pos>" + token.getPartOfSpeech() + "</pos>");

                // paintedHtmlBuilder.append("<value class=\"s\" >" + " " + escape(token.getValue()) + "</value><relation>" + escape(relation.getShortName()) + "</relation><group>" + escape(token.getGroup().getId() + "") + "</group><pos>" + token.getPartOfSpeech() + "</pos>");
            } else
                paintedHtmlBuilder.append("<value class=\" \">" + " " + escape(token.getValue()) + "</value><relations></relations><group></group><pos></pos>");
            paintedHtmlBuilder.append("</token>");
        }
        paintedHtmlBuilder.append("</tokens>");
        paintedHtmlBuilder.append("</sentence>");
        paintedHtmlBuilder.append("<br></br>");
    }

    private static String escape(String text) {
        return StringEscapeUtils.escapeXml(text);
    }


    private static void appendTitle(SentenceTree tree, StringBuilder paintedHtmlBuilder) {
        for (SentenceToken token : tree.getTokens())
            paintedHtmlBuilder.append(" " + token.getValue());
    }

/*    public static JSemanticGroup convertToJObject(SemanticGroup semanticGroup) {
        return new JSemanticGroup(semanticGroup);
    }*/


    @Deprecated
    public static int saveSemanticGroups(/*DBController controller, */String collectionName, List<List<SemanticGroup>> groups, int groupLastId) throws SQLException, ClassNotFoundException {
        int id = groupLastId + 1;
        SemanticGroupDao semanticGroupDao = new SemanticGroupDao();
        for (List<SemanticGroup> sentenceGroups : groups) {
            for (SemanticGroup group : sentenceGroups) {
                for (SemanticRelation relation : group.getRelations()) {
                    SentenceToken token = relation.getToken();
                   //TODO: add semanticGroupDao.saveSemanticRelation
                   // controller.saveSemanticRelation
                     //       (collectionName, token.getValue(), token.getBaseValue(), relation.getShortName(), token.getSentencePosition(), group.hashCode(), id);
                }
                id++;
            }
        }
        return id;
    }


    @Deprecated
    public static int saveSemanticGroups(/*DBController controller,*/ HashMap<Integer, HashMap<Integer, List<RelationInfo>>> countMap, String collectionName, List<List<SemanticGroup>> groups, int groupLastId) throws SQLException, ClassNotFoundException {
        int id = groupLastId + 1;
        SemanticGroupDao semanticGroupDao = new SemanticGroupDao();
        for (List<SemanticGroup> sentenceGroups : groups) {
            for (SemanticGroup group : sentenceGroups) {
                for (SemanticRelation relation : group.getRelations()) {
                    int hash = group.hashCode();
                    SentenceToken token = relation.getToken();
                    //TODO: add semanticGroupDao.saveSemanticRelation
                   // semanticGroupDao.saveSemanticRelation(...);
                   /* controller.saveSemanticRelation
                            (collectionName, token.getValue(), token.getBaseValue(),
                                    relation.getShortName(), token.getSentencePosition(), hash, id);

                    */
                    RelationInfo info = new RelationInfo();
                    info.setToken(relation.getToken().getValue());
                    info.setTokenBase(relation.getToken().getBaseValue());
                    info.setRelation(relation.getShortName());
                    info.setHash(hash);
                    info.setId(id);
                    info.setSentence(group.getSentenceTree().getSentence());
                    //info.setTitle(group.getSentenceTree().get);


                    if (countMap.containsKey(hash)) {
                        if (countMap.get(hash).containsKey(id)) {
                            countMap.get(hash).get(id).add(info);
                        } else {
                            List<RelationInfo> list = new LinkedList<RelationInfo>();
                            list.add(info);
                            countMap.get(hash).put(id, list);
                        }

                        //System.out.println("Added id = " + id);
                    } else {
                        //System.out.println("Created set for hash = " + hash + " and added id = " + id);
                        HashMap<Integer, List<RelationInfo>> map = new HashMap<Integer, List<RelationInfo>>();
                        List<RelationInfo> list = new LinkedList<RelationInfo>();
                        list.add(info);
                        map.put(id, list);
                        countMap.put(hash, map);
                    }
                }
                id++;
            }
        }
        return id;
    }


    public static List<SentenceTree> loadSentencesFromXml(String fileName) throws IOException {
        List<SentenceTree> sentenceTrees = new LinkedList<SentenceTree>();

        File file = new File(fileName);

        org.jsoup.nodes.Document doc = Jsoup.parse(file, "UTF-8");
        Elements sentences = doc.select("sentence");
        for (Element sentence : sentences) {
            try {

                //if(sentence.select("hash").text().equals("1189975503"))
                //  LoggingUtility.info("ERROR DETECTED");

                SentenceTree tree = new SentenceTree();
                List<SemanticGroup> semanticGroups = new LinkedList<SemanticGroup>();
                //SentenceTreeInfo info = new SentenceTreeInfo();
                Elements tokens = sentence.select("token");
                //info.setSentenceSize(tokens.size());
                //info.setContainsServiceTokens(containsServiceTokens(tokens));
                //info.setContainsDelimiters(containsDelimiters(tokens));
                int groupId = Integer.MIN_VALUE;
                SemanticGroup semanticGroup = null;
                String sentenceText = sentence.select("text").text();
                //info.setSentenceHash(new Integer(sentence.select("hash").text()));
                //String hash = sentence.select("hash").text();
                for (int i = 0; i < tokens.size(); i++) {
                    Element token = tokens.get(i);
                    //String relationString = token.select("relation").text();
                    String relationString = getRelations(token);

                    String value = token.select("value").text();
                    SentenceToken sentenceToken = new SentenceToken(value.replaceAll("!", ""));
                    sentenceToken.setSentencePosition(i);
                    sentenceToken.setSentence(tree);
                    if (!relationString.trim().equals("")) {
                        int group;
                        String groupText = token.select("group").text().replaceAll("!", "");
                        if (groupId != Integer.MIN_VALUE && groupText.equals(""))
                            group = groupId;
                        else {
                            if (groupText.equals(""))
                                group = 0;
                            else
                                group = new Integer(token.select("group").text().replaceAll("!", ""));
                        }


                        String pos = token.select("pos").text();
                        sentenceToken.setPartOfSpeech(pos);
                        if (groupId != group) {
                            //if (semanticGroup != null)
                            semanticGroup = new SemanticGroup(new PositionComparator());
                            semanticGroups.add(semanticGroup);
                            groupId = group;
                        }

                        List<String> relations = getSemanticRelations(relationString);

                        for (String relationName : relations) {

                            SemanticRelation relation = new SemanticRelation();
                            relation.setShortName(relationName);
                            relation.setToken(sentenceToken);
                            //relation.setSentencePosition(i);
                            //relation.setGroup(semanticGroup);
                            //List<SemanticRelation> relations = new LinkedList<SemanticRelation>();
                            //relations.add(relation);
                            //sentenceToken.setRelations(relations);
                            sentenceToken.addRelation(relation);
                        }
                        sentenceToken.setGroup(semanticGroup);
                        semanticGroup.addToken(sentenceToken);
                        semanticGroup.setSentenceTree(tree);
                    }


                    tree.addToken(sentenceToken);
                }
                //if(semanticGroup != null){
                tree.setSemanticGroups(semanticGroups);
                //tree.setInfo(info);
                tree.setSentence(sentenceText);
                sentenceTrees.add(tree);
                //}
            } catch (Exception ex) {


                ex.printStackTrace();
                LoggingUtility.info("Exception loading xml document");
                LoggingUtility.info(sentence.select("hash").text());
                LoggingUtility.info(ex.getMessage());
            }

        }

        return sentenceTrees;
    }


    private static String tryGetValue(Element token, String key) {
        String result = "";
        try {
            result = token.select(key).text();
        } catch (Exception e) {
            result = "";
        }
        return result;
    }

    private static String getRelations(Element token) {
        String relationString = "";
        relationString = tryGetValue(token, "relation");
        if (relationString.equals(""))
            relationString = tryGetValue(token, "role");
        return relationString;
    }

    public static boolean containsServiceTokens(Elements tokens) {
        for (int i = 0; i < tokens.size(); i++) {
            Element token = tokens.get(i);
            String value = getTokenValue(token);
            if (value.equals("goto") || value.equals("then"))
                return true;
        }
        return false;
    }

    public static boolean containsDelimiters(Elements tokens) {
        for (int i = 1; i < tokens.size() - 1; i++) {
            String tokenValue = getTokenValue(tokens.get(i));
            boolean hasPrevRelation = !tokens.get(i - 1).select("relation").text().trim().equals("");
            boolean hasNextRelation = !tokens.get(i + 1).select("relation").text().trim().equals("");
            boolean isTokenDelimiter = tokenValue.equals("and") || tokenValue.equals("or") || tokenValue.equals(",");
            if (hasNextRelation && hasPrevRelation && isTokenDelimiter)
                return true;
        }
        return false;
    }

    public static String getTokenValue(Element token) {
        return token.select("value").text().replaceAll("!", "").trim().toLowerCase();
    }

    public static List<String> getSemanticRelations(String relations) {
        List<String> rels = new LinkedList<String>();
        String[] split = relations.split("\\s+vs\\s+");
        for (String rel : split)
            rels.add(rel.replaceAll("!", "").trim());
        return rels;
    }

    public static int getSentencesAvgSize(List<SentenceTree> sentences) {
        int avg = 0;
        for (SentenceTree sentence : sentences)
            avg += sentence.getTokens().size();
        return avg / sentences.size();
    }


    public static String buildRoleMistakeReport(Map<String, Map<String, List<SemanticRelation>>> roleGrouping) {
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html><head><title>Mistake report</title><style>table{ border-collapse:collapse;}table,th, td{border: 1px solid black;}td,th{text-align:center;font-size:12px;font-family:Courier New} .role{background-color: SteelBlue} .relation {background-color: SkyBlue}</style></head>");
        htmlBuilder.append("<body><table><tr><th width=\"60px\">Role</th><th width=\"100px\">Relation</th><th width=\"120px\">Token</th><th width=\"120px\">Mistake Type</th><th width=\"300px\">Mistake Subtype \\ Mistake info</th><th width=\"400px\">Sentence info</th></tr>");
        int roleCounter = 0;
        int relationCounter = 0;
        int mistakeRoleCount = 0;
        int overAllRoleCount = 0;

        int relationMistakeCounter = 0;
        int relationOverallCounter = 0;
        for (String roleKey : roleGrouping.keySet()) {
            mistakeRoleCount = 0;
            overAllRoleCount = 0;
            Map<String, List<SemanticRelation>> relationGrouping = roleGrouping.get(roleKey);
            for (String relationKey : relationGrouping.keySet()) {
                relationMistakeCounter = 0;
                relationOverallCounter = 0;

                Map<RelationMistake.RelationMistakeSubType, Double> detailReport = new HashMap<RelationMistake.RelationMistakeSubType, Double>();
                for (SemanticRelation relation : relationGrouping.get(relationKey)) {
                    if (relation.hasMistake()) {

                        relationMistakeCounter++;

                        RelationMistake mistake = relation.getMistake();
                        if (roleCounter == 0 && relationCounter == 0)
                            htmlBuilder.append("<tr class='role'>");
                        else if (relationCounter == 0)
                            htmlBuilder.append("<tr class='relation'>");
                        else
                            htmlBuilder.append("<tr>");
                        htmlBuilder.append("<td>" + (roleCounter == 0 ? relation.getRole() : "") + "</td>");
                        htmlBuilder.append("<td>" + (relationCounter == 0 ? relation.getShortName() : "") + "</td>");
                        htmlBuilder.append("<td>" + relation.getToken().getValue() + "</td>");
                        htmlBuilder.append("<td>" + mistake.getType() + "</td>");


                        htmlBuilder.append("<td><table border='none' width='100%' height='100%'>");
                        for (RelationMistake.RelationMistakeSubType key : mistake.getSubTypes().keySet()) {
                            if (!detailReport.containsKey(key))
                                detailReport.put(key, 1d);
                            else {
                                detailReport.put(key, detailReport.get(key) + 1);
                            }

                            htmlBuilder.append("<tr><td width='150px'>" + key + "</td><td width='300px'>" + mistake.getSubTypes().get(key) + "</td></tr>");
                        }
                        htmlBuilder.append("</table></td>");
                        htmlBuilder.append("<td>" + relation.getToken().getSentence().first(50) + "</td>");
                        //+ StringUtils.join(mistake.getSubTypes().keySet(), "<br/>") + "</td>");
                        //htmlBuilder.append("<td>" + mistake.getInfoString() + "</td>");


                        roleCounter++;
                        relationCounter++;

                        mistakeRoleCount++;
                    }
                    overAllRoleCount++;
                    relationOverallCounter++;
                }

                LoggingUtility.info(relationKey + "," + relationOverallCounter + "," + relationMistakeCounter);
                for (RelationMistake.RelationMistakeSubType key : detailReport.keySet()) {
                    Double percent = detailReport.get(key) / relationMistakeCounter;
                    LoggingUtility.info(key + "," + detailReport.get(key) + ',' + percent);

                }

                relationCounter = 0;

            }

            LoggingUtility.info(roleKey + "," + overAllRoleCount + "," + mistakeRoleCount);


            roleCounter = 0;
        }
        return htmlBuilder.toString();
    }

}


