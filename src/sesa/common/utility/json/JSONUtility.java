package sesa.common.utility.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sesa.common.utility.logging.LoggingUtility;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class JSONUtility extends LoggingUtility {

    private static ArrayList<Object> JSONArray2Collection(JSONArray array) throws JSONException {
        ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object object = array.get(i);

            if (object instanceof JSONArray)
                list.add(JSONArray2Collection((JSONArray) object));
            else if (object instanceof JSONObject)
                list.add(JSONObject2Map((JSONObject) object));
            else
                list.add(object);
        }

        return list;
    }

    private static Map<String, Object> JSONObject2Map(JSONObject jsonObject) throws JSONException {
        HashMap<String, Object> object = new HashMap<String, Object>();

        Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            String key = keys.next().toString();
            Object value = jsonObject.get(key);

            if (value instanceof JSONArray)
                object.put(key, JSONArray2Collection((JSONArray) value));
            else if (value instanceof JSONObject)
                object.put(key, JSONObject2Map((JSONObject) value));
            else
                object.put(key, value);
        }

        return object;
    }

    @SuppressWarnings("unchecked")
    private static JSONArray collection2JSONArray(Collection<Object> collection) throws JSONException {
        JSONArray array = new JSONArray();
        for (Object obj : collection) {
            if (obj instanceof Collection)
                array.put(collection2JSONArray((Collection<Object>) obj));
            if (obj instanceof Map)
                array.put(map2JSONObject((Map<String, Object>) obj));
            else
                array.put(obj);
        }

        return array;
    }

    @SuppressWarnings("unchecked")
    private static JSONObject map2JSONObject(Map<String, Object> map) throws JSONException {
        JSONObject object = new JSONObject();
        Set<String> keys = map.keySet();

        for (String key : keys) {
            Object value = map.get(key);

            if (value instanceof Collection)
                object.put(key, collection2JSONArray((Collection<Object>) value));
            else if (value instanceof Map)
                object.put(key, map2JSONObject((Map<String, Object>) value));
            else
                object.put(key, value);
        }

        return object;
    }

    public static Map<String, Object> fromString(String s) {
        try {
            JSONObject obj = new JSONObject(s);
            return JSONObject2Map(obj);
        } catch (JSONException ignored) {
            error(ignored.getMessage());
        }

        return null;
    }

    public static String toString(Map<String, Object> map) {
        JSONObject object = new JSONObject(map);
        return object.toString();
    }

    /**
     * Convert list to JSON string
     * @param links list of string
     * @param key jsonKey
     * @return string in json format
     */
    public static String listToJsonString(List<String> links, String key){
        org.json.JSONObject jsonObject = new org.json.JSONObject();
        try{
            jsonObject.put(key, links);
        }catch(JSONException e){

        }
        return jsonObject.toString();
    }




}
