package sesa.tools.math.metrics;

import sesa.tools.math.Vector;

/**
 *
 */
public class CosineMetrics implements IMetrics {

    public double getDistance(Vector v1, Vector v2) {
        Double dotProduct = v1.dot(v2);
        if (java.lang.Math.abs(dotProduct) <= IMetrics.DOUBLE_ZERO)
            return Double.MAX_VALUE;

        return (v1.norm() * v2.norm()) / dotProduct;
    }
}
