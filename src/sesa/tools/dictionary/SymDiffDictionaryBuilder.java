package sesa.tools.dictionary;

import java.sql.SQLException;
import java.util.*;

import sesa.common.builder.BuilderException;
import sesa.common.configuration.Categories;
import sesa.common.configuration.Configuration;
import sesa.common.database.common.DBReportingIterator;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.parsing.XMLDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.vector.DocumentUtils;
import sesa.common.builder.DictionaryDocumentBuilder;

public class SymDiffDictionaryBuilder extends Application {
    DocumentsDao documentsDao = new DocumentsDao();

    public SymDiffDictionaryBuilder() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {


        String[] categoryNames = Categories.ALL_CATEGORIES;
      //  String categoryName = Categories.PHYSICS_CATEGORY;
        Map<String, Set<String>> uniqueTokenMap = new HashMap<String, Set<String>>();

        //--------------------------------
        for (String categoryName : categoryNames) {
            Set<String> categorySet = new HashSet<String>();
            String collectionName = Categories.buildCategoryHtmlCollectionName(categoryName);

            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(collectionName);
                    //dbController.getDocumentsCollectionIterator(collectionName);
            iterator = new DBReportingIterator(iterator, "Extracting tokens | category = " + categoryName);
            while (iterator.hasNext()) {

                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String text = (String) map.get("w_filtered_text");
                String id = doc.getID();
                IDocument document = null;
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
                try{
                    document = documentBuilder.buildDocument(id, text, null);
                }catch (BuilderException e){
                    e.printStackTrace();
                    //TODO: add log
                }
               // System.out.println(document.getTitle());
                List<SentenceTree> sentences = document.getSentences();
                // TODO: I can't get sentence: sentences.get(1).getSentence()
               // System.out.println(sentences.get(1).getSentence());
                Set<String> uniqueTokens = DocumentUtils.getUniqueTokens(sentences);
                for (String token : uniqueTokens) {
                    categorySet.add(token);
                  //  System.out.println(token);

                }
                uniqueTokenMap.put(categoryName, categorySet);
            }
        }
        // end of for
        //------------------------------------------------------

      /*  for (String key : uniqueTokenMap.keySet()) {
            System.out.println("In category " + key);
            System.out.println(uniqueTokenMap.get(key).size());
        }*/


        Map<String, Set<String>> filteredMap = getFilteredMap(uniqueTokenMap);

        Map<String, Object> entity = new HashMap<String, Object>();
        for (String key : filteredMap.keySet()) {
            Set<String> tokens = filteredMap.get(key);
            entity.put(key, tokens);
            // TODO: Whey we use this function 2 times filteredMap.get(key).size()
           // System.out.println(filteredMap.get(key).size());
            System.out.println(tokens.size());
        }

        // может быть не создана?
        documentsDao.truncateOrCreateDocumentCollection(Configuration.DB_COLLECTION_CATS_UNIQUE_DICTIONARIES);
       // dbController.truncateOrCreate(Configuration.DB_COLLECTION_CATS_UNIQUE_DICTIONARIES);
        documentsDao.addToDocumentsCollection(Configuration.DB_COLLECTION_CATS_UNIQUE_DICTIONARIES, new XMLDocument(entity));
        //dbController.insertToDocumentsCollection(Configuration.DB_COLLECTION_CATS_UNIQUE_DICTIONARIES, new XMLDocument(entity));

        return true;
    }

    /**
     * ��������� ��������������� �������� �� (Set<String>) � uniqueTokenMap
     * @param uniqueTokenMap
     * @return
     */
    private Map<String, Set<String>> getFilteredMap(Map<String, Set<String>> uniqueTokenMap) {
        Map<String, Set<String>> filteredTokenMap = new HashMap<String, Set<String>>();
        Map<String, Integer> globalTokenMap = new HashMap<String, Integer>();

        for (String key : uniqueTokenMap.keySet()) {
            for (String token : uniqueTokenMap.get(key)) {
                if (globalTokenMap.containsKey(token)) {
                    Integer value = globalTokenMap.get(token);
                    globalTokenMap.put(token, ++value);
                } else {
                    globalTokenMap.put(token, 0);
                }
            }
        }

        for (String key : uniqueTokenMap.keySet()) {
            Set<String> filteredList = new HashSet<String>();
            for (String token : uniqueTokenMap.get(key)) {
                if (globalTokenMap.get(token) == 0) {
                    filteredList.add(token);
                }
            }
            filteredTokenMap.put(key, filteredList);
        }

        return filteredTokenMap;
    }
}