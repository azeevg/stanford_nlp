package grammarscope.exporter;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalRelation.GrammaticalRelationAnnotation;
import edu.stanford.nlp.trees.GrammaticalRelation.Language;

/**
 * Reset GrammaticalRelation mapping data
 * 
 * @author Bernard Bou <bbou@ac-toulouse.fr>
 */
public class Resetter
{
	/**
	 * Reset GrammaticalRelation class data so that creation/import of a set of Grammatical relations becomes possible
	 * 
	 * @param l
	 *            target language to clear
	 */
	@SuppressWarnings("unchecked")
	static synchronized void reset(final Language l)
	{
		final Map<Language, Map<String, GrammaticalRelation>> s2r = GrammaticalRelation.getStringsToRelations();
		final Collection<GrammaticalRelation> set = s2r.get(l).values();

		final Map<Class<? extends GrammaticalRelationAnnotation>, GrammaticalRelation> a2r = GrammaticalRelation.getAnnotationsToRelations();
		final Map<GrammaticalRelation, Class<? extends GrammaticalRelationAnnotation>> r2a = GrammaticalRelation.getRelationsToAnnotations();

		for (final Iterator<Map.Entry<Class<? extends GrammaticalRelationAnnotation>, GrammaticalRelation>> it = a2r.entrySet().iterator(); it.hasNext();)
		{
			final Map.Entry<Class<? extends GrammaticalRelationAnnotation>, GrammaticalRelation> e = it.next();
			if (set.contains(e.getValue()))
			{
				// System.out.println("a2r removing key " + e.getKey());
				it.remove();
			}
		}

		for (final Iterator<Map.Entry<GrammaticalRelation, Class<? extends GrammaticalRelationAnnotation>>> it = r2a.entrySet().iterator(); it.hasNext();)
		{
			final Map.Entry<GrammaticalRelation, Class<? extends GrammaticalRelationAnnotation>> e = it.next();
			if (set.contains(e.getKey()))
			{
				// System.out.println("r2a removing key " + e.getKey());
				it.remove();
			}
		}
		s2r.remove(l);
		System.out.println("done: resetting " + l);
	}

}
