package sesa.application;

import edu.stanford.nlp.io.IOUtils;
import sesa.common.dataModule.*;
import sesa.common.configuration.Categories;
import sesa.common.database.dao.CommonDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.semantics.RelationInfo;
import sesa.common.semantics.SemanticGroup;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.vector.*;
import sesa.common.builder.BuilderException;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;


import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.*;

public class TestRelations extends Application {

    private static final String GRAMMAR_PATH = "grammar";
    private static final String COLORED_DIR_PATH = "colored_pages";
    private static final String SEMANTIC_DICT = "semantic_dict";

    private static final String COUNTER_KEY = "tree_cache";


    public void runTests(DocumentBuilder stanfordBuilder) throws BuilderException {

        System.out.println("TESTS:");

        String ts1 = "Ada is a structured , statically typed , imperative , wide-spectrum , and object-oriented high-level computer programming language , extended from Pascal and other languages .";
        String[] er1 = new String[]{"Ada is language", "extended from Pascal languages"};
        System.out.println(matchResults(ts1, er1, stanfordBuilder));

        String ts2 = "It has strong built-in language support for explicit concurrency , offering tasks , synchronous message passing ( via guarded task entries ) , protected objects ( a monitor-like construct with additional guards as in conditional critical regions ) and nondeterminism ( via select statements ) .";
        String[] er2 = new String[]{"It has support"};
        System.out.println(matchResults(ts2, er2, stanfordBuilder));

        String ts3 = "Ada was originally designed by a team led by Jean Ichbiah of CII Honeywell Bull under contract to the United States Department of Defense from 1977 to 1983 to supersede the hundreds of programming languages then used by the DoD .";
        String[] er3 = new String[]{"Ada designed by a team", "supersede languages"};
        System.out.println(matchResults(ts3, er3, stanfordBuilder));
    }

    @Override
    protected boolean body() {

        try {
            String categoryName = Categories.COMPUTING_CATEGORY;
            String title = "Ada (programming language)";
            ObjectInputStream inputStream = IOUtils.readStreamFromString("./data/grammar/englishPCFG.ser.gz");
            //dbController.useConnection(0);
            //dbController.resetCounter(COUNTER_KEY);
            //StanfordDataModule dataModule = new BulkWriteTreeDataModule(dbController, false, "tree_cache_computing");


            //   dbController.resetCounter(COUNTER_KEY);
            //   StanfordDataModule dataModule = new BulkWriteTreeDataModule(dbController, true, "tree_cache_computing");
            //   DocumentBuilder stanfordBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
            //  IDocument stanfordDocument = DocumentUtils.loadDocumentByTitle(dbController, categoryName, title, stanfordBuilder);
            //   DocumentUtils.iterateDocumentsWithRecovery(dbController, categoryName, stanfordBuilder);
            //dbController.increaseCounter(COUNTER_KEY);
            //dbController.increaseCounter(COUNTER_KEY);
///TODO!!! second parameter "tree_cache_computing" must be generated according to current value of categoryName!
            StanfordDataModule dataModule = new ReadTreeDataModule("tree_cache_computing");
            //generatePaintedDocument(categoryName, inputStream, title, dataModule);
            DocumentBuilder stanfordBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
            IDocument stanfordDocument = DocumentUtils.loadDocumentByTitle(categoryName, title, stanfordBuilder);
            List<SentenceTree> sentences = stanfordDocument.getSentences();
          //  System.out.println("sentetce:   " + sentences.get(1).getTriplet());


            String paintedHtml = DocumentUtils.buildPaintedHtml(sentences);

            String id = stanfordDocument.getId();
            String filePath = configuration.getProperty(COLORED_DIR_PATH) + "/" + id + dataModule.getClass().getSimpleName() + ".html";
            FileUtility.writeAllTextToFile(filePath, paintedHtml);

            LoggingUtility.info("Generated html colored file = " + filePath);
            LoggingUtility.info("Done...");
            //generatePaintedDocument(categoryName, inputStream, title, dataModule);


            //dataModule = new ReadTreeDataModule(dbController);
            //generatePaintedDocument(categoryName, inputStream, title, dataModule);

            //List<List<SemanticGroup>> semanticGroups = DocumentUtils.getSemanticGroups(sentences);
            //Map<String, Object> jSemanticGroups = DocumentUtils.getJSemanticGroups(sentences);
            //JSONObject object = new JSONObject(jSemanticGroups);


            //Map<String, Object> map = dbController.selectDocument(Categories.buildCategoryHtmlCollectionName(categoryName), id).toMap();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void generatePaintedDocument(String categoryName, ObjectInputStream inputStream, String title, StanfordDataModule dataModule) throws SQLException, ClassNotFoundException {
        CommonDao commonDao  = new CommonDao();
        //StanfordDataModule dataModule = new WriteTreeDataModule(dbController);
        DocumentBuilder stanfordBuilder = new StanfordRelationsBuilder(inputStream, dataModule);
        //IDocument stanfordDocument = DocumentUtils.loadDocumentByTitle(dbController, categoryName, title, stanfordBuilder);
        List<IDocument> stanfordDocument = DocumentUtils.loadFirstDocuments(categoryName, 20, stanfordBuilder);
        int id = 0;
        commonDao.truncateDocumentCollection(SEMANTIC_DICT);

        HashMap<Integer, HashMap<Integer, List<RelationInfo>>> countMap = new HashMap<Integer, HashMap<Integer, List<RelationInfo>>>();
        for (IDocument document : stanfordDocument) {
            List<SentenceTree> sentences = document.getSentences();
            List<List<SemanticGroup>> semanticGroups = DocumentUtils.getSemanticGroups(sentences);
          //  id = DocumentUtils.saveSemanticGroups(countMap, SEMANTIC_DICT, semanticGroups, id);
        }

        int count = 0;
        for (Integer key : countMap.keySet()) {
            if (countMap.get(key).size() > 1) {
                for (Integer idKey : countMap.get(key).keySet()) {
                    for (RelationInfo info : countMap.get(key).get(idKey)) {
                        System.out.println("\n" + info);
                    }

                }
            }
        }
        System.out.println("Total: " + count);


    /*String paintedHtml = DocumentUtils.buildPaintedHtml(sentences);

String id = stanfordDocument.getId();
String filePath = configuration.getProperty(COLORED_DIR_PATH) + "/" + id + dataModule.getClass().getSimpleName() + ".html";
FileUtility.writeAllTextToFile(filePath, paintedHtml);

LoggingUtility.info("Generated html colored file = " + filePath);
LoggingUtility.info("Done...");    */
    }

    public class DocumentSemanticGroups {

        private List<List<SemanticGroup>> groups;

        public DocumentSemanticGroups(List<List<SemanticGroup>> groups) {
            this.groups = groups;
        }
    }

    /**
     * совпадающие результаты
     * @param ts
     * @param er
     * @param b
     * @return
     * @throws BuilderException
     */
    public boolean matchResults(String ts, String[] er, DocumentBuilder b) throws BuilderException {
        b.setData(ts, "");
        IDocument d1 = b.buildDocument();
        List<List<SemanticGroup>> groups = DocumentUtils.getSemanticGroups(d1.getSentences());
        for (int i = 0; i < er.length; i++) {
          //  System.out.println(er[i]);
            if (!groups.get(0).get(i).toString().trim().equals(er[i].trim()))
                return false;
        }
        return true;
    }

}


