package sesa.test.junit;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import sesa.common.utility.bundle.FileManager;

/**
 * Created by Ira on 24.02.2016.
 */
public class FileManagerTest {

    @Test
    public void pathTest(){
        FileManager fileManager = new FileManager("path");
        assertEquals(fileManager.getValue("path.stopList"), "./data/stoplist/stoplist.txt");
    }
}
