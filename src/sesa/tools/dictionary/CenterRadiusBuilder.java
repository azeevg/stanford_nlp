package sesa.tools.dictionary;

import sesa.common.configuration.Categories;
import sesa.common.model.processing.document.IDocument;
import sesa.tools.math.metrics.EuclideanMetrics;
import sesa.tools.math.metrics.IMetrics;
import sesa.tools.math.Vector;
import sesa.common.utility.common.Application;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.utility.dictionary.DictionaryUtils;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.vector.DocumentUtils;
import sesa.common.utility.vector.VectorUtils;


import java.sql.SQLException;
import java.util.*;

public class CenterRadiusBuilder extends Application {

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        Map<String, CategoryInfo> categoryInfoMap = new HashMap<String, CategoryInfo>();
        IMetrics metrics = new EuclideanMetrics();
        String[] categoryNames = Categories.ALL_CATEGORIES;
        Dictionary dictionary = DictionaryUtils.loadDictionary();

        for (String categoryName : categoryNames) {

            List<IDocument> documents = DocumentUtils.loadDocuments(categoryName);
            List<Vector> vectors = VectorUtils.createVectors(documents, dictionary);
            List<Vector> zeroVectors = new LinkedList<Vector>();
            for (Vector vector : vectors) {
                if (vector.size() == 0) {
                    zeroVectors.add(vector);
                }
            }

            Vector center = VectorUtils.getCenter(vectors);
            double radius = VectorUtils.getMaxRadius(vectors, center, metrics);
            categoryInfoMap.put(categoryName, new CategoryInfo(categoryName, center, radius));
            CategoryInfo info = categoryInfoMap.get(categoryName);
            info(info.toString());

            LoggingUtility.info("All vectors = " + vectors.size());
            LoggingUtility.info("Zero vectors = " + zeroVectors.size());
        }

        return true;
    }
}
