package sesa.tools.importers;

import sesa.common.configuration.Configuration;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.interfaces.IProcessor;
import sesa.common.model.parsing.JSONDocument;
import sesa.common.utility.common.Application;
import sesa.common.utility.io.DirectoryUtility;

import java.sql.SQLException;
import java.util.Arrays;

/**
 * Add a json document to database
 * link to json document: data/json/publications.json ( = importer_pubmed.input)
 */
public class ImporterPUBMED extends Application implements IProcessor {
    private final static String PARAM_INPUT = "importer_pubmed.input";

    public void process(String item) throws SQLException, ClassNotFoundException {
        DocumentsDao documentsDao = new DocumentsDao();
        if (!documentsDao.isTableExists(Configuration.DB_COLLECTION_PUBMED_2)) {
            documentsDao.createDocumentCollectionTable(Configuration.DB_COLLECTION_PUBMED_2);
        }
        documentsDao.addToDocumentsCollection(Configuration.DB_COLLECTION_PUBMED_2, new JSONDocument(item));

    }

    @Override
    protected boolean body() {
        if (!configuration.contains(Arrays.asList(PARAM_INPUT)))
            return false;

        DirectoryUtility.processDirectory(configuration.getProperty(PARAM_INPUT), this);
        return true;
    }
}
