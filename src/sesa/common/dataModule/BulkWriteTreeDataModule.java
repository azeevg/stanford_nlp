package sesa.common.dataModule;


import grammarscope.parser.Sentence;

import java.sql.SQLException;
import java.util.List;



public class BulkWriteTreeDataModule extends WriteTreeDataModule {

   // private TreeCacheDao treeCacheDao = new TreeCacheDao();
    public BulkWriteTreeDataModule(boolean truncate) throws SQLException, ClassNotFoundException {
        super(truncate);
    }

    public BulkWriteTreeDataModule(boolean truncate, String collection) throws SQLException, ClassNotFoundException {
        super(truncate, collection);
    }

    @Override
    public void postProcess(String title) {

        for (Integer key : treeMap.keySet()) {
            String penn = treeMap.get(key).toString();
            String zippedPenn = penn.replaceAll(" \\(", "(").replaceAll("\\) ", ")");
            treeCacheDao.addCacheTree(collection, title.hashCode(), key, zippedPenn);
           // controller.insertObject(collection, title.hashCode(), key, zippedPenn);
        }
        super.postProcess(title);
    }

   @Override
    protected void writeTreeCache(String title, List<Sentence> sentences) {
    }
}
