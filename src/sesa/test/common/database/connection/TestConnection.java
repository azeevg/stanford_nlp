package sesa.test.common.database.connection;


import org.junit.Test;
import sesa.common.database.connection.ConnectionPool;
import sesa.common.database.connection.ConnectionPoolSingleton;

import java.sql.Connection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Created by Ira on 30.10.2016.
 */
public class TestConnection {

    @Test
    public void testConnectionToDB(){
        try{
            ConnectionPool cp = ConnectionPoolSingleton.getInstance().getConnectionPool();
            Connection connection = cp.takeConnection();
            assertTrue(!connection.isClosed());
        }catch (Exception e){
            assertEquals(e.getMessage(), "Problem with connected to database.");
        }
    }
}
