package sesa.common.model.processing.document;

import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.model.processing.sentence.SentenceForest;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.utility.logging.LoggingUtility;
import sesa.tools.math.Vector;

import java.util.*;

/**
 * This class represents a full information about document
 */
public class Document implements IDocument {
    protected String id;        // document id
    private String title;       // document title
    private DocumentNode tree;

    public String getTitle() {
        return title;
    }

    public Document(String id, String title, DocumentNode tree) {
        this.id = id;
        this.tree = tree;
        this.title = title;
    }

    public void print() {
        LoggingUtility.info("Document with id = " + id + ", title = " + title);
        printNode(tree);
        printNodes(tree.getChildren());
    }

    public void printNodes(List<DocumentNode> nodes) {
        for (DocumentNode node : nodes) {
            printNode(node);
            printNodes(node.getChildren());
        }
    }

    public List<SentenceTree> getSentences() {
        List<SentenceTree> trees = new LinkedList<SentenceTree>();
        getTrees(tree, trees);
        return trees;
    }


    private void getTrees(DocumentNode node, List<SentenceTree> trees) {
        if (node.getTitle() != null) {
            for (SentenceForest sentenceForest : node.getTitle()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    trees.add(sentenceTree);
                    //triplets.add(sentenceTree.getTriplet());
                }
            }
        }

        /* Process paragraph */
        if (node.getParagraph() != null) {
            for (SentenceForest sentenceForest : node.getParagraph()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    trees.add(sentenceTree);
                }
            }
        }

        /* Process children */
        if (node.getChildren() != null) {
            for (DocumentNode child : node.getChildren()) {
                getTrees(child, trees);
            }
        }
    }

    /**
     * Get list of tree triplets
     *
     * @return
     */
    public List<SentenceTriplet> getTriplets() {
        List<SentenceTriplet> triplets = new LinkedList<SentenceTriplet>();
        getNodeTriplets(tree, triplets);
        return triplets;
    }


    public void getNodeTriplets(DocumentNode node, List<SentenceTriplet> triplets) {
        /* Process title */
        if (node.getTitle() != null) {
            for (SentenceForest sentenceForest : node.getTitle()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    triplets.add(sentenceTree.getTriplet());
                }
            }
        }

        /* Process paragraph */
        if (node.getParagraph() != null) {
            for (SentenceForest sentenceForest : node.getParagraph()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    triplets.add(sentenceTree.getTriplet());
                }
            }
        }

        /* Process children */
        if (node.getChildren() != null) {
            for (DocumentNode child : node.getChildren()) {
                getNodeTriplets(child, triplets);
            }
        }

    }

    public void printNode(DocumentNode node) {
        System.out.println(node.getTitle() + "| level = " + node.getLevel());
        for (SentenceForest forest : node.getParagraph()) {
            for (SentenceTree tree : forest.getTrees()) {
                for (SentenceToken token : tree.getTokens()) {
                    System.out.print(token + " ");
                }
            }
        }
        System.out.println();
    }

    protected void updateVector(Vector v, SentenceTree tree, Double level) {

        if (tree == null)
            return;

        double ultraWeight = 1.0 / Math.pow(2, level);
        for (SentenceToken token : tree.getTokens())
            v.addComponent(token.toString(), ultraWeight);
    }


    private synchronized void buildVector(Vector v, DocumentNode node, Double level) {

        /* Process title */
        if (node.getTitle() != null) {
            for (SentenceForest sentenceForest : node.getTitle()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    updateVector(v, sentenceTree, level);
                }
            }
        }

        /* Process paragraph */
        if (node.getParagraph() != null) {
            for (SentenceForest sentenceForest : node.getParagraph()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    updateVector(v, sentenceTree, level);
                }
            }
        }

        /* Process children */
        if (node.getChildren() != null) {
            for (DocumentNode child : node.getChildren()) {
                buildVector(v, child, level + 1.0);
            }
        }
    }

    public Vector toVector(Dictionary dictionary) {
        Vector vector = new Vector(id);

        buildVector(vector, tree, 0.0);
        if (dictionary != null)
            vector.toTF_IDF(dictionary);
        else
            vector.normalize();

        return vector;
    }

    public String getId() {
        return id;
    }


    public List<String> getAllTokens() {
        List<String> tokens = new LinkedList<String>();
        buildTokensCollection(tree, tokens);
        return tokens;
    }

    public Set<String> getUniqueTokens() {
        Set<String> tokens = new HashSet<String>();
        buildTokensCollection(tree, tokens);
        return tokens;
    }

    private void buildTokensCollection(DocumentNode node, Collection<String> result) {
        if (node.getTitle() != null) {
            for (SentenceForest sentenceForest : node.getTitle())
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    for (SentenceToken token : sentenceTree.getTokens()) {
                        result.add(token.getValue());
                    }
                }
        }

        /* Process paragraph */
        if (node.getParagraph() != null) {
            for (SentenceForest sentenceForest : node.getParagraph()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    for (SentenceToken token : sentenceTree.getTokens()) {
                        result.add(token.getValue());
                    }
                }
            }
        }

        /* Process children */
        if (node.getChildren() != null) {
            for (DocumentNode child : node.getChildren()) {
                buildTokensCollection(child, result);
            }
        }
    }

    public String getPaintedHtml() {
        StringBuilder paintedHtmlBuilder = new StringBuilder();
        String styleDefinition = "<style> span {color:red; font: bold Arial;}</style>";
        paintedHtmlBuilder.append("<html><head><title>" + tree.getTitle() + "</title>" + styleDefinition + "</head>");
        getPaintedHtml(tree, paintedHtmlBuilder);
        paintedHtmlBuilder.append("</body></html>");
        return paintedHtmlBuilder.toString();
    }

    /**
     * recursive
     *
     * @param node
     * @param paintedHtmlBuilder
     */
    private void getPaintedHtml(DocumentNode node, StringBuilder paintedHtmlBuilder) {
        if (node.getTitle() != null) {
            for (SentenceForest sentenceForest : node.getTitle()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    String hTag = "h" + node.getLevel();
                    String openinigTag = "<" + hTag + ">";
                    String closingTag = "</" + hTag + ">";
                    paintedHtmlBuilder.append(openinigTag + sentenceTree.getSentence() + closingTag);
                    //triplets.add(sentenceTree.getTriplet());
                }
            }
        }

        /* Process paragraph */
        if (node.getParagraph() != null) {
            for (SentenceForest sentenceForest : node.getParagraph()) {
                for (SentenceTree sentenceTree : sentenceForest.getTrees()) {
                    paintedHtmlBuilder.append(sentenceTree.getSentence());
                    //triplets.add(sentenceTree.getTriplet());
                }
            }
        }

        /* Process children */
        if (node.getChildren() != null) {
            for (DocumentNode child : node.getChildren()) {
                getPaintedHtml(child, paintedHtmlBuilder);
            }
        }
    }
}
