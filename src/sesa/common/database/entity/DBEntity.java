package sesa.common.database.entity;

import sesa.common.utility.json.JSONUtility;

import java.util.Map;

public class DBEntity implements IDBEntity {
    private String id;
    private Map<String, Object> map;

    public DBEntity(String id, String data) {
        this.id = id;
        map = JSONUtility.fromString(data);
    }

    public String getID() {
        return id;
    }

    public Map<String, Object> toMap() {
        return map;
    }
}
