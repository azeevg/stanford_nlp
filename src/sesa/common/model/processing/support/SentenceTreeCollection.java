package sesa.common.model.processing.support;

import sesa.common.model.processing.sentence.SentenceTree;

import java.util.List;

public class SentenceTreeCollection {

    private List<SentenceTree> sentences;

    public List<SentenceTree> getSentences() {
        return sentences;
    }

    public void setSentences(List<SentenceTree> sentences) {
        this.sentences = sentences;
    }

    public SentenceTreeCollection(List<SentenceTree> sentences) {
        this.sentences = sentences;
    }


    public int getAvgSentenceSize() {
        int avg = 0;
        for (SentenceTree sentence : sentences)
            avg += sentence.getTokens().size();
        return avg / sentences.size();
    }
}
