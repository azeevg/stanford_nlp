package sesa.common.database.dao.dictionary;

import edu.stanford.nlp.util.Pair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.database.dao.CommonDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Ira on 14.05.2016.
 */
public class TermLinksDao extends CommonDao {
    public final Logger log = LogManager.getLogger(CommonDao.class);
    private int okLinks = 0;
    private String collectionName = "terms_comp_link";

    public TermLinksDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public void saveToLinksCollections(int idDef, List<Integer> idLinks) {
        PreparedStatement ps = null;
        Connection connection = null;
        log.info("Add terms to " + collectionName);
        int i = 0;
        try {
            connection = cp.takeConnection();
            for (Integer idLink : idLinks) {
                String query = "INSERT INTO `" + collectionName + "` (DEF_ID ,TERM_LINK_ID) VALUES(?,?)";
                ps = connection.prepareStatement(query);
                ps.setInt(1, idDef);
                ps.setInt(2, idLink);
                ps.execute();
                log.info("Added new row (" + idDef + ":" + idLink + ") to table:" + collectionName);
                i++;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps);
        }
        log.info("Added __" + i + "__ rows ");
    }

    public void saveToLinksCollections(List<TermDefinitionEntity> entities) {
        int warnLinks = 0;
        okLinks = 0;
        log.info("Add terms to " + collectionName);
        for (TermDefinitionEntity entity : entities) {
            if (entity.getLinks() == null) {
                continue;
            }
            warnLinks += saveToLinksCollections(entity);

        }
        log.info("Added __" + okLinks + "__ rows ");
        log.info("Links have not been added:" + warnLinks);
    }

    private int saveToLinksCollections(TermDefinitionEntity entity) {
        PreparedStatement ps = null;
        Connection connection = null;
        int warnLinks = 0;
        try {
            connection = cp.takeConnection();
            for (Integer idLink : entity.getLinks()) {
                if (idLink == null) {
                    log.warn("link = null, idTerm:" + entity.getIdTerm() + ", idDef:" + entity.getIdDefinition());
                    warnLinks++;
                    continue;
                }
                String query = "INSERT INTO `" + collectionName + "` (DEF_ID ,TERM_LINK_ID) VALUES(?,?)";
                ps = connection.prepareStatement(query);
                ps.setInt(1, entity.getIdDefinition());
                ps.setInt(2, idLink);
                ps.execute();
                // log.info("Added new row (" + entity.getIdTerm() + ":" + entity.getIdDefinition() + ":" + idLink + ")
                // to table:" + collectionName);
                okLinks++;
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            if (ps == null) {
                cp.closeConnection(connection);
            } else {
                cp.closeConnection(connection, ps);
            }

        }
        return warnLinks;
    }

    public Set<Integer> getTermsContainLink() {
        Set<Integer> terms = new HashSet<Integer>();
        List<Integer> id_terms = new ArrayList<Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            String query = "SELECT td.TERM_ID FROM terms_comp_definition as td , terms_comp_link as tl where td.DEF_ID=tl.DEF_ID";
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int id = resultSet.getInt("TERM_ID");
                terms.add(id);
                //    id_terms.add(id);
//                if(id_terms.contains(id)){
//                    continue;
//                }else{
//                    id_terms.add(id);
//                }

            }

        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        System.out.println("Set size:" + terms.size());
        System.out.println("List size:" + id_terms.size());

        return terms;
    }

    public List<Integer> getTermsNotContainLink() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        int countOfTermsWithoutLinks = 0;
        Set<Integer> idTermsWithLinks = getTermsContainLink();
        Map<String,Integer> allTerms = dao.getAllTerms();
        List<Integer> idTermsWithoutLinks = new ArrayList<Integer>();

        log.info("Count terms with links:" + idTermsWithLinks.size());

        for(String term : dao.getAllTerms().keySet()){
            int idTerm = allTerms.get(term);
            if(idTermsWithLinks.contains(idTerm)){
                continue;
            }else{
                countOfTermsWithoutLinks++;
                idTermsWithoutLinks.add(idTerm);
            }
        }
        log.info("Count terms without links:" + countOfTermsWithoutLinks);
        return idTermsWithoutLinks;
    }

    // (id1, id2) ->
    // id1 - term that have reference to term with id2 in its definition
    public List<Pair<Integer, Integer>> getAllReferences() throws SQLException, ClassNotFoundException {
        List<Pair<Integer, Integer>> references = new ArrayList<>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            String query = "SELECT terms_comp_definition.TERM_ID as REFERENCING, terms_comp_link.TERM_LINK_ID as REFERENCED_TO\n" +
                    "FROM terms_comp_link\n" +
                    "JOIN terms_comp_definition ON terms_comp_definition.DEF_ID = terms_comp_link.DEF_ID";

            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int referencing = resultSet.getInt("REFERENCING");
                int referencedTo = resultSet.getInt("REFERENCED_TO");

                references.add(new Pair<>(referencing, referencedTo));
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return references;
    }

    public List<Integer> getLinksByIdDef(int defId) {
        List<Integer> list = new ArrayList<Integer>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            String query = "SELECT * FROM `terms_comp_link` WHERE `DEF_ID`=" + defId;
            System.out.println(query);
            ps = connection.prepareStatement(query);
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                int id = resultSet.getInt("TERM_LINK_ID");
                list.add(id);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }

        return list;
    }
}
