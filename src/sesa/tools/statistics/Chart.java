package sesa.tools.statistics;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import sesa.common.features.Pair;

import java.awt.*;
import java.util.LinkedList;
import java.util.UUID;

/**
 * Класс для графического отображения диаграмм
 * free library
 */
public class Chart extends ApplicationFrame {


    public Chart(String title, LinkedList<LinkedList<Pair<Double, Double>>> seriesDataList, String t, String xLabel, String yLabel) {
        super(title);


        final XYDataset dataSet = createDataSet(seriesDataList);
        final JFreeChart chart = createChart(dataSet, t, xLabel, yLabel);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1000, 800));
        setContentPane(chartPanel);
    }

    private JFreeChart createChart(XYDataset dataset, String title, String xLabel, String yLabel) {
        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
                title,      // chart title
                xLabel,                      // x axis label
                yLabel,                      // y axis label
                dataset,                  // data
                PlotOrientation.VERTICAL,
                false,                     // include legend
                true,                     // tooltips
                false                     // urls
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

//        final StandardLegend legend = (StandardLegend) chart.getLegend();
        //      legend.setDisplaySeriesShapes(true);

        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesShapesVisible(0, false);
        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.

        return chart;

    }

    private XYDataset createDataSet(LinkedList<LinkedList<Pair<Double, Double>>> seriesDataList) {

        final XYSeriesCollection dataset = new XYSeriesCollection();
        boolean a = true;
        for (LinkedList<Pair<Double, Double>> seriesData : seriesDataList) {
            if (a) {
                XYSeries series = new XYSeries("Оператор TF");
                for (Pair<Double, Double> pair : seriesData)
                    series.add(pair.getLeft(), pair.getRight());
                dataset.addSeries(series);
                a = false;
            } else {
                XYSeries series = new XYSeries("Оператор Q SYMM");
                for (Pair<Double, Double> pair : seriesData)
                    series.add(pair.getLeft(), pair.getRight());
                dataset.addSeries(series);
                a = false;
            }

        }
        return dataset;
    }
}
