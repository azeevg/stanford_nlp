package sesa.test;

import edu.stanford.nlp.io.IOUtils;

import org.junit.Test;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.model.processing.document.IDocument;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.io.FileUtility;
import sesa.tools.cache.CacheUtils;

import java.io.ObjectInputStream;

/**
 * Created by Ira on 21.04.2016.
 */
public class ParseDocumentTest {

    @Test
    public void parseHTMLText(){
        try {
            // Small Language model
            FileManager fileManager = new FileManager("path");
         //   fileManager.init("path");
            ObjectInputStream inputStream = IOUtils.readStreamFromString(fileManager.getValue("path.smallLanguageModel"));

            String text = FileUtility.readAllText("./data/ada_cache/db_text.txt");
            StanfordDataModule dataModule = new BasicDataModule();
            // StanfordDataModule dataModule = new ReadTreeDataModule("tree_cache_computing");
            DocumentBuilder builder = new StanfordRelationsBuilder(inputStream, dataModule);
         //   builder.parseText(text,"Ada (programming language)");



            builder.setData(text, "id");
            IDocument document = builder.buildDocument();

            System.out.println(document.getSentences().size());

            CacheUtils.generatePaintedDocument(document);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }





}
