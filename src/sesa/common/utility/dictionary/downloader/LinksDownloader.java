package sesa.common.utility.dictionary.downloader;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sesa.common.database.dao.CrossReferenceDao;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by Ira on 03.05.2016.
 */
public class LinksDownloader {
    public final Logger log = LogManager.getLogger(LinksDownloader.class);

    /**
     * Download links from file for one letter
     * where every string in json format: {"term_name":"title", "links_term": "link1,link2,..."}
     *
     * @param fileName
     * @return map where:
     * key - is name of term
     * value - list of links for term
     */
    public Map<String, List<String>> downloaderLinksFromFile(String fileName) {
        log.info("Start to download links from file:" + fileName);
        Map<String, List<String>> map = new TreeMap<String, List<String>>();
        JSONParser parser = new JSONParser();

        try {
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                String jsonStr = in.nextLine();
                try {
                    Object obj = parser.parse(jsonStr);
                    JSONObject jsonObject = (JSONObject) obj;
                    if (jsonObject.get("term_name") != null) {
                        map.put(jsonObject.get("term_name").toString(), (List<String>) jsonObject.get("links_term"));
                    }

                } catch (ParseException pe) {
                    pe.printStackTrace();
                }

            }
            log.info("Number of terms:" + map.keySet().size());
            log.info("-- end of downloaded --  ");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return map;
    }

    /**
     * Save collection reference to data base
     * example: Configuration.DB_COLLECTION_COMPUTER_DICTIONARY_CROSS_REF
     *
     * @param map
     */
    public boolean saveReferences(Map<String, List<String>> map, String collectionName) throws SQLException, ClassNotFoundException {

        CrossReferenceDao referenceDao = new CrossReferenceDao();
        return referenceDao.addToRefCollections(map, collectionName);
    }


}
