package sesa.tools.importers.WIKI;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import sesa.common.configuration.Configuration;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.parsing.XMLDocument;
import sesa.common.utility.logging.LoggingUtility;
import sesa.tools.filters.WIKI.initial.InitialWIKIFilter;

import java.sql.SQLException;
import java.util.HashMap;

public class ImportHandlerWIKI extends DefaultHandler {

    private static final String TITLE_KEY = "title";
    private static final String TEXT_KEY = "text";

  //  private DBController controller;
    private DocumentsDao documentsDao;
    private StringBuilder titleBuilder = null;
    private StringBuilder textBuilder = null;
    private HashMap<String, Object> document;

    private boolean isText = false;
    private boolean isTitle = false;

    private static long documentsAdded = 0;
    private static long documentsSkipped = 0;
    private static long documentsTotal = 0;

    public ImportHandlerWIKI(/*DBController controller*/) throws SQLException, ClassNotFoundException {
        documentsDao = new DocumentsDao();
       // this.controller = controller;
    }

    public void startElement(String namespaceURI, String localName, String qualifiedName, Attributes atts) throws SAXException {
        isText = false;
        isTitle = false;

        String PAGE_KEY = "page";
        if (qualifiedName.equalsIgnoreCase(PAGE_KEY)) {
        } else if (qualifiedName.equalsIgnoreCase(TITLE_KEY)) {
            isTitle = true;
            titleBuilder = new StringBuilder();
        } else if (qualifiedName.equalsIgnoreCase(TEXT_KEY)) {
            isText = true;
            textBuilder = new StringBuilder();
        }
    }

    public void endElement(String namespaceURI, String localName, String qualifiedName) throws SAXException {
        if (qualifiedName.equalsIgnoreCase(TITLE_KEY)) {
            document = new HashMap<String, Object>();
            document.put(TITLE_KEY, titleBuilder.toString());
        } else if (qualifiedName.equalsIgnoreCase(TEXT_KEY)) {
            document.put(TEXT_KEY, textBuilder.toString());
            XMLDocument doc = new XMLDocument(document);
            InitialWIKIFilter filter = new InitialWIKIFilter(doc);

            if (filter.isInformativeDocument()) {
                documentsDao.addToDocumentsCollection(Configuration.DB_COLLECTION_WIKI,doc);
                //controller.addDocumentWIKI(doc); //insert(Configuration.DB_COLLECTION_WIKI, document);
                documentsAdded++;
            } else
                documentsSkipped++;

            documentsTotal++;
            if (documentsTotal % 1000 == 0)
                LoggingUtility.info("Total: " + documentsTotal + "; Added: " + documentsAdded + "; Skipped: " + documentsSkipped);
        }
    }

    public void characters(char[] text, int start, int length) throws SAXException {
        if (isText)
            textBuilder.append(text, start, length);
        else if (isTitle)
            titleBuilder.append(text, start, length);
    }
}
