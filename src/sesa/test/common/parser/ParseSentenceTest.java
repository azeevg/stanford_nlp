package sesa.test.common.parser;

import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Parser;
import grammarscope.parser.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.semantic.SentenceUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * Created by Ira on 18.05.2016.
 */
public class ParseSentenceTest {
    public final Logger log = LogManager.getLogger(ParseSentenceTest.class);
    private FileManager fileManager = new FileManager("path");
    private String smallLanguageModel;
    private String languageModel = "./data/grammar/englishFactored.ser.gz";
    private String testFile = "data/test.txt";

    @Before
    public void init(){
        smallLanguageModel = fileManager.getValue("path.smallLanguageModel");

    }
    @Test
    public void getKBestParseTrees() throws IOException{
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile(testFile);
        log.info("### CollocationSpanTagging parser: ");
        List<Tree> treeList2 = parser.getKBestParse(sentence, false, 3);
    }

    @Test
    public void testStanfordCKY() throws IOException {
        log.info("### Stanford parser: ");
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile(testFile);
        Tree tree = parser.parse(sentence,false);
    }

    @Test
    public void testCollocationSpanTaggingCKY() throws IOException {
        log.info("### CollocationSpanTagging parser: ");
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile(testFile);
        Tree tree = parser.parse(sentence,true);
    }

    @Test
    public void testCompareGetBestParse() throws IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile("data/test.txt");
        log.info("### Stanford parser: ");
        Tree tree1 = parser.parse(sentence,false);
        log.info("### CollocationSpanTagging parser: ");
        Tree tree2 = parser.parse(sentence,true);
    }

    @Test
    public void testCompareKGetBestParse() throws IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile(testFile);
        log.info("### Stanford parser: ");
        List<Tree> treeList = parser.getKBestParse(sentence, false, 2);
        log.info("### CollocationSpanTagging parser: ");
        List<Tree> treeList2 = parser.getKBestParse(sentence, true, 2);
    }
}
