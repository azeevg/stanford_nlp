package sesa.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Ira on 19.02.2016.
 */
public class ErrorDocumentDao extends CommonDao {

    public final Logger log = LogManager.getLogger(ErrorDocumentDao.class);
    public ErrorDocumentDao() throws SQLException, ClassNotFoundException {
        super();
    }

    public List<String> readParsingErrors() {
        List<String> resultList = new LinkedList<String>();
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM errors");
            preparedStatement.execute();

            resultSet = preparedStatement.getResultSet();
            while (resultSet.next())
                resultList.add(resultSet.getString("error"));

        } catch (SQLException e) {
            log.error(e.getMessage());
            // debug("Exception: " + e.getMessage());
        }finally {
            cp.closeConnection(connection,preparedStatement,resultSet);
        }

        return resultList;
    }

    public void writeErrorDocument(String key, String type, String message) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = cp.takeConnection();
            //String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO errors (error, errorType, message) VALUES(?,?,?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, key);
            preparedStatement.setString(2, type);
            preparedStatement.setString(3, message);

            preparedStatement.execute();
            preparedStatement.close();

            //  return true;
        } catch (SQLException e) {
            log.error(e.getMessage());
            log.debug("Can't insert record: " + key);
            //debug("Can't insert record: " + key);
            //debug("Exception: " + e.getMessage());
            //LoggingUtility.info("SQL Exception: " + e.getMessage());
        }finally {
            cp.closeConnection(connection,preparedStatement);
        }


    }
}
