package sesa.common.utility.common;

import sesa.common.configuration.Configuration;
import sesa.common.utility.logging.LoggingUtility;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

public class ConfigUtility extends LoggingUtility {
    private Properties properties = new Properties();
    private boolean isConfigurationLoaded = false;

    public ConfigUtility() {
        try {
            properties.load(new FileInputStream(Configuration.CFG_FILE)); //"config/configuration.cfg"
        } catch (Exception e) {
            die("Error during loading configuration file: " + Configuration.CFG_FILE);
        }
        isConfigurationLoaded = true;
    }

    public boolean isLoaded() {
        return isConfigurationLoaded;
    }

    public boolean contains(String key) {
        return properties.containsKey(key);
    }

    public boolean contains(List<String> keys) {
        for (String key : keys)
            if (!contains(key)) {
                error("Necessary parameter missed: " + key);
                return false;
            }

        return true;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
