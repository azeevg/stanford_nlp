package sesa.common.utility.html;

import sesa.common.utility.logging.LoggingUtility;

import java.util.Set;

public class HtmlUtils {

    //inefficient method, might be refactored
    public static String paintWikiHtmlPage(String wikiHtmlText, Set<String> categoryDictionary) {

        if (categoryDictionary == null)
            return wikiHtmlText;

        String styleDefenition = "<style> span {color:red; font: bold Arial;}</style>";

        for (String key : categoryDictionary) {
            if (wikiHtmlText.contains(key)) {
                wikiHtmlText = wikiHtmlText.replaceAll("(?i)\\b" + key + "\\b", "<span class=\"colored\">" + key + "</span>");
            }
        }

        if (!wikiHtmlText.contains("</span>"))
            LoggingUtility.info("No specific tokens detected");

        int bodyStart = wikiHtmlText.indexOf("<body>");
        return wikiHtmlText.substring(0, bodyStart) + styleDefenition + wikiHtmlText.substring(bodyStart, wikiHtmlText.length());
    }


}
