package sesa.tools.semantic.semanticTree;

/**
 * Created by Ira on 11.06.2016.
 */

/**
 * Edge in link graph
 */
public class SemanticEdge {

    private int source;
    private int target;

    public SemanticEdge(int source, int target) {
        this.source = source;
        this.target = target;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }
}
