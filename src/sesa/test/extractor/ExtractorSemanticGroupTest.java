package sesa.test.extractor;

import org.junit.Test;
import sesa.common.utility.FileReaderWriter;
import sesa.tools.semantic.ExtractorSemanticGroup;
import sesa.common.database.entity.SemanticGroupEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Ira on 01.06.2016.
 */
public class ExtractorSemanticGroupTest {

    @Test
    public void testExtractedSG() throws SQLException, ClassNotFoundException {
        ExtractorSemanticGroup extractor = new ExtractorSemanticGroup();
        List<String> list = FileReaderWriter.read("./data/dictionary/analyze_input/jsonResultGroup_five_input.txt");
        List<SemanticGroupEntity> semanticGroups = extractor.getSemanticRelationFromDataBase(5);
        List<SemanticGroupEntity> listDto = extractor.analyzeResult(list, semanticGroups);
        // extractor.saveListSemanticGroupDto(listDto);
    }

    @Test
    public void testExtractedSGCollocation() throws SQLException, ClassNotFoundException {
        ExtractorSemanticGroup extractor = new ExtractorSemanticGroup();
        List<String> list = FileReaderWriter.read("./data/dictionary/analyze_input/jsonResultCollocationGroups_input.txt");
        List<SemanticGroupEntity> semanticGroups = extractor.getSemanticRelationFromDataBaseContainCollocation();
        List<SemanticGroupEntity> listDto = extractor.analyzeResult(list, semanticGroups);
        // extractor.saveListSemanticGroupDto(listDto);
    }
}
