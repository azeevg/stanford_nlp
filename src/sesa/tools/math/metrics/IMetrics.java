package sesa.tools.math.metrics;

import sesa.tools.math.Vector;

public interface IMetrics {
    public static final double DOUBLE_ZERO = 10e-7;

    double getDistance(Vector v1, Vector v2);
}
