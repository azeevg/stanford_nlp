package sesa.test.dictionary;

import org.junit.Test;
import sesa.common.builder.BuilderException;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.utility.FileReaderWriter;
import sesa.tools.dictionary.builder.AnalyzeDictionary;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ira on 21.05.2016.
 */
public class AnalyzeDictionaryTest {


    @Test
    public void testSaveSemanticGroup() throws BuilderException, SQLException, ClassNotFoundException {
        AnalyzeDictionary analyzeDictionary = new AnalyzeDictionary();
        analyzeDictionary.run();
        // analyzeDictionary.saveSemanticGroupForOneTerm(919,"Android desktop");
        analyzeDictionary.printCollocationResult();
    }


    @Test
    public void testGetUnique() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        TermDefinitionDao definitionDao = new TermDefinitionDao();
        List<String> terms = FileReaderWriter.read("unique1.txt");
        List<String> out = new ArrayList<String>();
        for (String term : terms) {
            if (definitionDao.getTermDefinitionByTermID(Integer.valueOf(term)).size() > 1) {
                System.out.println("> 1:" + term);
            }
//            System.out.println(dao.getTermByID(Integer.valueOf(term)).getHeadWord());
//            System.out.println(definitionDao.extractTermDefinitionByTermID(Integer.valueOf(term)).get(0).getDefinition());
//            System.out.println("-----------------");
            // out.add(dao.getTermByID(Integer.valueOf(term)).getHeadWord());
        }
        // FileReaderWriter.write("data/termsUnique.txt",out);
    }
}
