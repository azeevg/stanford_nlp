package sesa.common.utility.dictionary;

import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.DocumentPreprocessor;
import sesa.common.configuration.Configuration;
//import sesa.common.database.common.DBController;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.CommonDao;
import sesa.common.model.processing.dictionary.Dictionary;
import sesa.common.utility.io.FileUtility;

import java.io.Reader;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DictionaryUtils {

    private static Set<String> stopList = loadStopList();


    private static boolean isTokenInStopList(String token) {
        return stopList.contains(token);
    }

    private static Set<String> loadStopList() {
        return FileUtility.loadLinesLowerToSet("./data/stoplist/stoplist.txt");
    }

    public static Set<String> loadCategoryDictionary(/*DBController controller, */String categoryName) throws SQLException, ClassNotFoundException {
        CommonDao commonDao = new CommonDao();
        IDBIterator iterator = commonDao.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CATS_UNIQUE_DICTIONARIES);
        //IDBIterator iterator = controller.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CATS_UNIQUE_DICTIONARIES);
        IDBEntity entity = iterator.next().get();
        return new HashSet<String>((ArrayList) entity.toMap().get(categoryName));
    }


    public static Dictionary loadDictionary() throws SQLException, ClassNotFoundException {
        CommonDao commonDao = new CommonDao();
        IDBIterator iterator = commonDao.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CATS_DICTIONARY);
       // IDBIterator iterator = controller.getDocumentsCollectionIterator(Configuration.DB_COLLECTION_CATS_DICTIONARY);
        IDBEntity entity = iterator.next().get();
        Dictionary dictionary = new Dictionary();
        Map<String, Integer> intMap = (HashMap<String, Integer>) entity.toMap().get("dictionary");
        HashMap<String, Double> doubleMap = new HashMap<String, Double>();
        for (String key : intMap.keySet())
            doubleMap.put(key, intMap.get(key).doubleValue());

        dictionary.setTokens(doubleMap);
        return dictionary;
    }

    public static List<List<String>> getValidLowerTokens(String text) {
        List<List<String>> sentences = new LinkedList<List<String>>();
        Reader rd = new StringReader(text);
        DocumentPreprocessor dp = new DocumentPreprocessor(rd);
        for (List sentence : dp) {
            List<String> lowerTokens = new LinkedList<String>();
            for (Object word : sentence) {
                String tokenValue = ((Word) word).value().trim();
                String tokenLowerCase = tokenValue.toLowerCase();
                if (isTokenValueValid(tokenValue, tokenLowerCase)) {
                    lowerTokens.add(tokenLowerCase);
                }
            }
            sentences.add(lowerTokens);
        }
        return sentences;
    }

    public static List<List<String>> getSentences(String text) {
        List<List<String>> sentences = new LinkedList<List<String>>();
        Reader rd = new StringReader(text);
        DocumentPreprocessor dp = new DocumentPreprocessor(rd);
        for (List sentence : dp) {
            System.out.println("SENTENCE = " + sentence.toString().hashCode());
        }
        return sentences;
    }


    private static boolean isTokenValueValid(String tokenValue, String tokenLowerValue) {
        return isTokenValid(tokenValue) && !isTokenInStopList(tokenLowerValue);
    }

    //too strong
    //private final static Pattern wordCharPattern = Pattern.compile("^[a-z][a-zA-Z]*$");

    private final static Pattern wordCharPattern = Pattern.compile("^[a-zA-Z]*$");
    private final static Pattern duplicatePattern = Pattern.compile(".*([a-zA-Z])\\1{2}.*");

    private final static int MINIMUM_TOKEN_LENGTH = 3;
    private final static int MAXIMIM_TOKEN_LENGTH = 20;

    private static boolean isTokenValid(String token) {
        int len = token.length();
        Matcher wordMatcher = wordCharPattern.matcher(token);
        Matcher vowelsMatcher = duplicatePattern.matcher(token);
        boolean lenCondition = len < MINIMUM_TOKEN_LENGTH || len > MAXIMIM_TOKEN_LENGTH;
        boolean symCondition = !wordMatcher.matches() || vowelsMatcher.matches();
        return !(lenCondition || symCondition);
    }

    public static String buildCategoryCollectionName(String categoryName) {
        return Configuration.DB_COLLECTION_BY_CAT_FILTERED_WIKI + "_" + categoryName;
    }

    public static String buildCategoryHtmlCollectionName(String categoryName) {
        return Configuration.DB_COLLECTION_BY_CAT_HTML_WIKI + "_" + categoryName;
    }
}
