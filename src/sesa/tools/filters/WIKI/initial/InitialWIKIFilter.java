package sesa.tools.filters.WIKI.initial;

import sesa.common.model.parsing.XMLDocument;

public class InitialWIKIFilter {

    private XMLDocument data;

    private static final int MINIMAL_LENGTH = 500;
    private static final String[] MAY_BE_WORDS = {"may refer", "may mean", "may stand for", "can refer", "refers to", "could refer", "may also refer"};
    private static final String[] REFERENCE_SYMBOLS = {"{{selfref", "{{softredirect", "{{unreferenced"};

    public InitialWIKIFilter(XMLDocument document) {
        this.data = document;
    }

    private boolean isDisambiguated(String text) {
        return text.contains("{{disambig}}");
    }

    /**
     * �������� �� ��, ��� �������� �� ��������� ����������� �����
     * @param text ����� ��������
     * @return ��/���
     */
    private boolean isShortPage(String text) {
        return text.length() < MINIMAL_LENGTH;
    }

    /**
     * ������������� �������� ��� ���
     * @return
     */
    public boolean isInformativeDocument() {
        String text = data.toMap().get("text").toString(); // �������� �������� �� ����� "text"
        String firstSymbols = text.substring(0, Math.min(text.length(), MINIMAL_LENGTH)).toLowerCase().trim();
        boolean notInformativePage = isRedirectPage(firstSymbols) || isMovedToPage(firstSymbols) || isMayBePage(firstSymbols)
                || isReferencePage(firstSymbols) || isDictionaryPage(firstSymbols) || isDisambiguated(text);
        return !isShortPage(text) && !notInformativePage;
    }

    /**
     *
     * @param firstTextBlock
     * @return
     */
    private boolean isRedirectPage(String firstTextBlock) {
        return firstTextBlock.contains("#redirect"); // ���������� ��� ��� #redirect
    }

    /**
     *
     * @param firstTextBlock
     * @return
     */
    private boolean isMovedToPage(String firstTextBlock) {
        return firstTextBlock.startsWith("moved to"); // ���������� ��� ��� � moved to
    }

    /**
     * �������� �� ��, ��� � ������ ���������� ����� �� ������� MAY_BE_WORDS
     * @param firstTextBlock ����� ��������
     * @return ��������� ��������: true/false
     */
    private boolean isMayBePage(String firstTextBlock) {
        for (String mayBeWord : MAY_BE_WORDS) {
            if (firstTextBlock.contains(mayBeWord))
                return true;
        }
        return false;
    }

    /**
     * �������� �� ��, ��� �� �������� ���������� ������, � ������
     * ����� �� ������� REFERENCE_SYMBOLS
     * @param firstTextBlock ����� ��������
     * @return ��������� ��������: true/false
     */
    private boolean isReferencePage(String firstTextBlock) {
        for (String referenceSymbol : REFERENCE_SYMBOLS) {
            if (firstTextBlock.startsWith(referenceSymbol))
                return true;
        }
        return false;
    }

    private boolean isDictionaryPage(String firstTextBlock) {
        return firstTextBlock.contains("{{wikt");
    }
}

