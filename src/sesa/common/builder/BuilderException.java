package sesa.common.builder;

/**
 * Created by IntelliJ IDEA.
 * User: ashishagin
 * Date: 4/28/13
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class BuilderException extends Exception{

  public BuilderException(String message) {
    super(message);
  }

  public BuilderException(String message, Throwable cause) {
    super(message, cause);
  }
}
