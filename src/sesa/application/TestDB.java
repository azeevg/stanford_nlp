package sesa.application;

import org.testng.annotations.Test;
//import sesa.common.database.common.DBFactory;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.dao.CommonDao;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.utility.common.Application;
import sesa.common.utility.hash.HashUtility;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDB extends Application {
    private final static String TEST_COLLECTION = "test";
    private CommonDao commonDao = new CommonDao();
    private DocumentsDao documentsDao = new DocumentsDao();

    public TestDB() throws SQLException, ClassNotFoundException {
    }

    private class TestHuman implements IDBEntity {
        private String surname;
        private String name;

        public TestHuman(String surname, String name) {
            this.surname = surname;
            this.name = name;
        }

        public String getID() {
            return HashUtility.generateHash(surname + "_" + name);
        }

        public Map<String, Object> toMap() {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("surname", surname);
            map.put("name", name);

            return map;
        }
    }
@Test
    private void testInsertion() {
        TestHuman human1 = new TestHuman("Eliseev", "Dmitry");
        TestHuman human2 = new TestHuman("Ivankov", "Alexey");
        TestHuman human3 = new TestHuman("Shishagin", "Alexey");

        documentsDao.addToDocumentsCollection(TEST_COLLECTION, human1);
        documentsDao.addToDocumentsCollection(TEST_COLLECTION, human2);
        documentsDao.addToDocumentsCollection(TEST_COLLECTION, human3);
    }

    private void printEntity(IDBEntity entity) {
        Map<String, Object> author = entity.toMap();
        info(author.get("surname") + " " + author.get("name"));
    }

    private void selectionTest() {
        List<IDBEntity> list = documentsDao.getDocuments(TEST_COLLECTION);//singleConnection.select(TEST_COLLECTION);

        info("");
        info("Authors:");
        for (IDBEntity entity : list)
            printEntity(entity);
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        CommonDao commonDao = new CommonDao();
        DocumentsDao documentsDao = new DocumentsDao();

        /* Create new collection */
        documentsDao.createDocumentCollectionTable(TEST_COLLECTION);

        /* Truncate test */
        documentsDao.truncateDocumentCollection(TEST_COLLECTION);
       // dbConnection.truncate(TEST_COLLECTION);
        info("Number of items at test collection: " + commonDao.countTotalOfRow(TEST_COLLECTION));

        /* Insertion test */
        testInsertion();
        info("Number of items at test collection: " + commonDao.countTotalOfRow(TEST_COLLECTION));

        /* Selection test */
        selectionTest();

        return true;
    }
}
