package sesa.common.builder.relationsBuilder;

import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.Tree;
import sesa.common.builder.BasicBuilder;
import sesa.common.model.processing.sentence.SentenceForest;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.model.processing.support.PositionComparator;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.semantics.TripletFinder;

import java.io.*;
import java.util.*;
/**
 * Created by Ira on 28.02.2016.
 */
//TODO: delete
    @Deprecated
public class DeliaRusoBuilder extends BasicBuilder {

    private LexicalizedParser lexicalizedParser;

    public DeliaRusoBuilder(ObjectInputStream thisInputStream) throws Exception {
        this.lexicalizedParser = new LexicalizedParser(thisInputStream);
    }

    @Override
    protected List<SentenceForest> buildParagraphSubTree(String text, boolean isTitle, int level) {
        List<SentenceForest> paragraph = new LinkedList<SentenceForest>();

        Reader rd = new StringReader(text);
        DocumentPreprocessor dp = new DocumentPreprocessor(rd);
        for (List sentence : dp) {

            SentenceForest forest = new SentenceForest();
            SentenceTree tree = new SentenceTree();


            Tree parse = lexicalizedParser.apply(sentence);

            LoggingUtility.info("" + Sentence.listToString(sentence).hashCode());
            LoggingUtility.info(parse.toString());

            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            parse.indentedXMLPrint(writer);
            String xmlTree = stringWriter.toString();
            SentenceTriplet triplet = TripletFinder.findSentenceTriplet(xmlTree);
            triplet.setSentence(Sentence.listToString(sentence));


            List<SentenceToken> tokens = new LinkedList<SentenceToken>();
            SemanticGroup group = new SemanticGroup(new PositionComparator());
            int position = 1;

            Set<String> alreadyResolved = new HashSet<String>();
            for (Object w : sentence) {
                Word word = (Word) w;
                String value = word.value();
                SentenceToken token = new SentenceToken(value);
                token.setSentencePosition(position);
                SemanticRelation relation = null;
                if (value.equals(triplet.getSubject()) && !alreadyResolved.contains("subj")) {
                    relation = new SemanticRelation("subj");
                    alreadyResolved.add("subj");
                    relation.setShortName("subj");
                } else if (value.equals(triplet.getPredicate()) && !alreadyResolved.contains("pred")) {
                    relation = new SemanticRelation("pred");
                    relation.setShortName("pred");
                    alreadyResolved.add("pred");
                } else if (value.equals(triplet.getObject()) && !alreadyResolved.contains("obj")) {
                    relation = new SemanticRelation("obj");
                    relation.setShortName("obj");
                    alreadyResolved.add("obj");
                }
                if (relation != null) {
                    relation.setToken(token);
                    token.addRelation(relation);
                    group.addToken(token);
                    group.setId(0);
                    token.setGroup(group);
                }
                token.setPartOfSpeech("");
                tokens.add(token);
                position++;
            }

            tree.addSemanticGroup(group);
            tree.setSentence(Sentence.listToString(sentence));
            tree.setTokens(tokens);
            forest.addTree(tree);
            paragraph.add(forest);
        }

        return paragraph;
    }


    public String printGroup(SemanticGroup group) {
        StringBuilder builder = new StringBuilder();
        for (SentenceToken token : group.getTokens()) {
            builder.append(token.getValue()).append(";");
        }
        return builder.toString();
    }
}
