package sesa.common.database.connections;

import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.ParsedRelation;
import sesa.common.database.entity.IDBEntity;
import sesa.common.features.Option;
import sesa.tools.statistics.RawSemanticGroup;

import java.io.InputStream;
import java.util.*;

/**
 * Класс многопоточного подключения к БД
 */

@Deprecated
public class DBShardedConnection extends DBConnection {
    private DBConnection singleConnections[];

    public DBShardedConnection(String dbHosts, int dbPort, String dbName, String dbUser, String dbPass) {
        super(dbHosts, dbPort, dbName, dbUser, dbPass);
        String hosts[] = dbHosts.split(";");

        int connectionID = 0;
        singleConnections = new DBConnection[hosts.length];
        for (String host : hosts)
            singleConnections[connectionID++] = new DBSingleConnection(host, dbPort, dbName, dbUser, dbPass);

        for (DBConnection singleConnection : singleConnections)
            isConnected &= singleConnection.isConnected();
    }

    public DBSingleConnection useConnection(int index) {
        return (DBSingleConnection) singleConnections[index];
    }

    @Override
    public boolean tableExists(String collectionName) {
        boolean tableExists = false;
        for (DBConnection singleConnection : singleConnections)
            tableExists &= singleConnection.tableExists(collectionName);
        return tableExists;
    }

    private int getShardID(String id) {
        return (id.hashCode() & 0x7FFFFFFF) % singleConnections.length;
    }

    @Override
    public boolean insert(String collectionName, IDBEntity document) {
        int shardID = getShardID(document.getID());
        return singleConnections[shardID].insert(collectionName, document);
    }

    @Override
    public boolean copyByCategory(String collectionNameSrc, String collectionNameDest, String categories) {
        for (DBConnection singleConnection : singleConnections)
            singleConnection.copyByCategory(collectionNameSrc, collectionNameDest, categories);
        return true;
    }

    @Override
    public boolean insert(String collectionName, IDBEntity document, Map<String, String> additional) {
        return true;
       // int shardID = getShardID(document.getID());
       // return singleConnections[shardID].insert(collectionName, document, additional);
    }

    @Override
    public boolean update(String collectionName, IDBEntity document) {
        int shardID = getShardID(document.getID());
        return singleConnections[shardID].update(collectionName, document);
    }

    @Override
    public long count(String collectionName, Map<String, Object> condition) {
        long totalCount = 0;

        for (DBConnection singleConnection : singleConnections)
            totalCount += singleConnection.count(collectionName, condition);

        return totalCount;
    }

    @Override
    public long count(String collectionName) {
        long totalCount = 0;

        for (DBConnection singleConnection : singleConnections)
            totalCount += singleConnection.count(collectionName);

        return totalCount;
    }

    @Override
    public boolean contains(String collectionName, String id) {
        //int shardID = getShardID(id);
       /* for (DBConnection singleConnection : singleConnections) {
            if (singleConnection.contains(collectionName, id))
                return true;
        }*/
        return false;
        //return singleConnections[shardID].contains(collectionName, id);
    }

    @Override
    public Option<IDBEntity> get(String collectionName, String id) {
        int shardID = getShardID(id);
        return singleConnections[shardID].get(collectionName, id);
    }

    @Override
    public List<IDBEntity> select(String collectionName, long from, long size) {
        List<IDBEntity> items = new LinkedList<IDBEntity>();

        for (DBConnection singleConnection : singleConnections)
            items.addAll(singleConnection.select(collectionName, from, size));

        return items;
    }

    @Override
    public IDBEntity selectById(String collectionName, String id) {
        return null;
    }

    @Override
    public List<IDBEntity> select(String collectionName) {
        List<IDBEntity> items = new LinkedList<IDBEntity>();

        for (DBConnection singleConnection : singleConnections)
            items.addAll(singleConnection.select(collectionName));

        return items;
    }

    @Override
    public boolean create(String collectionName) {
        for (DBConnection singleConnection : singleConnections)
            singleConnection.create(collectionName);

        return true;
    }

    @Override
    public boolean remove(String collectionName, String id) {
        int shardID = getShardID(id);
        return singleConnections[shardID].remove(collectionName, id);
    }

    @Override
    public boolean truncate(String collectionName) {
        for (DBConnection singleConnection : singleConnections)
            singleConnection.truncate(collectionName);

        return true;
    }

    @Override
    public boolean drop(String collectionName) {
        for (DBConnection singleConnection : singleConnections)
            singleConnection.drop(collectionName);

        return true;
    }

    public boolean insertObject(String collectionName, String title, String hash, byte[] data) {
        return false;
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean insertObject(String collectionName, int title, int hash, String data) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public InputStream readObject(String collectionName, String id) {
        return null;
    }

    @Override
    public HashMap<Integer, Tree> getCachedTrees(String collectionName, int titleCache) {
        return null;
    }

    @Override
    public HashMap<Integer, Tree> getCachedTreesObjects(String collectionName, String title) {
        return null;
    }

    @Override
    public HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> getCachedRelations(String collectionName, String title) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, int position, int group, int id) {

    }

    @Override
    public void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, String pos, int index, String groupId, int sentenceId, int sentenceService, int prevDelimiter, int sentenceSize) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getCounter(String key) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setCounter(String key, int value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeErrorDocument(String key, String type, String message) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<String> readParsingErrors() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Collection<RawSemanticGroup> getSemanticGroups(String collectionName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
