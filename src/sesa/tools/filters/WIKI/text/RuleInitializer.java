package sesa.tools.filters.WIKI.text;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class RuleInitializer {
    /* Wrap key */
    public static String WRAP_KEY = "w_wrap";

    /* HashMap key constants */
    public static String FILTERED_TEXT_KEY = "w_filtered_text";
    public static String CATEGORIES_KEY = "w_category";
    public static String LINK_KEY = "w_link";
    public static String ERROR_KEY = "w_error";

    /* WikiExtractor constants */
    public static String[] REMOVE_TAG_KEYS = new String[]{"w_italic", "w_bold", "w_super_bold", "w_wrap"};
    public static String[] REMOVE_KEYS = new String[]{};
    public static String[] SKIP_INSIDE_KEYS = new String[]{"w_ref", "w_cite", "w_ext_link", "w_comment", "w_math", "w_pre", "w_nowiki", "w_table", "w_syntax", "w_gallery", "w_timeline"};

    public static String[] IMAGE_LINKS = new String[]{"Image:", ":Image:", "File:", ":File:"};
    public static String[] MEDIA_AND_HELP_LINKS = new String[]{"Media:", ":Media:", "Help:", ":Help:"};
    public static String[] DICT_LINKS = new String[]{"wikt:", ":wikt:"};
    public static String[] CATEGORY_LINKS = new String[]{"Category:", ":Category:"};

    /* HTML tags constants */
    public static String[] HTML_TAGS = new String[]{"del", "i", "ins", "u", "font", "big", "small", "sub", "sup", "h1",
            "h2", "h3", "h4", "h5", "h6", "cite", "code", "em",
            "strike", "strong", "tt", "var", "div", "center",
            "blockquote", "abbr", "span", "p"};

    public static String[] SINGLE_HTML_TAGS = new String[]{"hr", "br"};

    public static List<KeySymbols> initializeKeySymbols() {

        LinkedList<KeySymbols> keys = new LinkedList<KeySymbols>();

        /* Wiki specials */
        keys.add(new KeySymbols(WRAP_KEY, "<#wrap>", "</#wrap>", 3));
        keys.add(new KeySymbols("w_ss_section", "====", "====", 1));
        keys.add(new KeySymbols("w_s_section", "===", "===", 1));
        keys.add(new KeySymbols("w_section", "==", "==", 1));

        KeySymbols wItalics = new KeySymbols("w_italic", new String[]{"''"}, new String[]{"''"}, new String[]{"s "}, 0, false);
        KeySymbols wBolds = new KeySymbols("w_bold", new String[]{"'''"}, new String[]{"'''"}, new String[]{"s "}, 0, false);
        wItalics.setAmbiguityWithSymbols(new KeySymbols[]{wBolds});
        wBolds.setAmbiguityWithSymbols(new KeySymbols[]{wItalics});
        keys.add(wBolds);
        keys.add(wItalics);

        keys.add(new KeySymbols("w_link", "[[", "]]", 1));
        keys.add(new KeySymbols("w_ext_link", "[http", "]", 1));
        keys.add(new KeySymbols("w_ref", new String[]{"<ref"}, new String[]{"</ref>", "/>"}, 2));

        keys.add(new KeySymbols("w_table", "{|", "|}", 1, false));
        keys.add(new KeySymbols("w_cite", "{{", "}}", 1, false));
        keys.add(new KeySymbols("w_syntax", "<syntaxhighlight", "</syntaxhighlight>", 2, false));
        keys.add(new KeySymbols("w_math", "<math", "</math>", 2, false));
        keys.add(new KeySymbols("w_comment", "<!--", "-->", 0, false));
        keys.add(new KeySymbols("w_pre", "<pre", "</pre>", 2, false));
        keys.add(new KeySymbols("w_nowiki", "<nowiki", "</nowiki>", 2, false));
        keys.add(new KeySymbols("w_timeline", "<timeline", "</timeline>", 2, false));
        keys.add(new KeySymbols("w_gallery", "<gallery", "</gallery>", 2, false));


        for (String textStyleTag : HTML_TAGS) {
            String[] endArray = new String[]{"</" + textStyleTag + ">", "/>"};
            keys.add(new KeySymbols(textStyleTag, "<" + textStyleTag, endArray, 1, false));
        }

        for (String textStyleTag : SINGLE_HTML_TAGS) {
            String[] endArray = new String[]{"/>"};
            keys.add(new KeySymbols(textStyleTag, "<" + textStyleTag, endArray, 1, false));
        }

        return keys;
    }

    public static HashMap<String, LinkedList<Integer>> hashCache;

    private static boolean matchSequenceArray(char[] symbols, char[][] matchChsArray) {
        for (char[] aMatchChsArray : matchChsArray)
            if (matchSymbols(aMatchChsArray, symbols))
                return true;

        return false;
    }

    public static LinkedList<Integer> getPossibleHashes(List<KeySymbols> allKeysSymbols, char[] symbols) {
        String key = new String(symbols);
        if (!hashCache.containsKey(key)) {
            LinkedList<Integer> hashes = new LinkedList<Integer>();
            for (KeySymbols currentSymbols : allKeysSymbols)
                if (matchSequenceArray(symbols, currentSymbols.getEndSymbolsArray()))
                    hashes.add(currentSymbols.getTagHash());

            hashCache.put(key, hashes);
        }

        return hashCache.get(key);
    }

    private static boolean matchSymbols(char[] chs1, char[] chs2) {
        if (chs1.length != chs2.length)
            return false;

        for (int i = 0; i < chs1.length; i++)
            if (chs1[i] != chs2[i])
                return false;

        return true;
    }
}
