package sesa.test.common.database.dao;

import org.junit.Test;
import sesa.common.database.entity.dictionary.TermEntity;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.utility.FileReaderWriter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Ira on 13.05.2016.
 */
public class TermsDaoTest {

    @Test
    public void testAddTerms() throws SQLException, ClassNotFoundException {

        TermDao dao = new TermDao();
        Set<String> terms = new HashSet<String>();
        terms.add("ZIP");
        terms.add("Programming language");
        terms.add("Computing");
        assertEquals(dao.addToTermsCollectionsAndReturnAdded(terms).size(),3);
    }

    @Test
    public void testGetSynonyms() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        FileReaderWriter.write("data/Synonyms.txt",dao.extractSynonyms().toString());
    }

    @Test
    public void testGetTermByID() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        TermEntity entity = dao.getTermById(23960);
        System.out.println(entity.toString());
    }
    @Test
    public void testGetTermDefinitionByTermID() throws SQLException, ClassNotFoundException {
        TermDefinitionDao dao = new TermDefinitionDao();
        System.out.println(dao.getTermDefinitionByTermID(886));
    }

    @Test
    public void testExtractUniqueTerms() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        FileReaderWriter.write("data/idUniqueTerm.txt",dao.getAllUnambiguousTerms());
    }
    @Test
    public void testIsUnambiguousTerm() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        System.out.println(dao.isUnambiguousTermWithDef(914));
    }

    @Test
    public void testExtractUniqueTermsWithNullDefinition() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        FileReaderWriter.write("data/idNUllUniqueTerm.txt",dao.getUniqueTermsWithNullDef());
    }

    @Test
    public void testExtractNotNUllTermDef() throws SQLException, ClassNotFoundException {
        TermDefinitionDao dao = new TermDefinitionDao();
        System.out.println(dao.getNotNUllTermDefinitionByTermID(914));
    }

    @Test
    public void testExtractNotNUllAllTermDef() throws SQLException, ClassNotFoundException {
        TermDefinitionDao dao = new TermDefinitionDao();
        System.out.println(dao.getAllNotNUllTermDefinition());
    }

    @Test
    public void testGetIdTermByName() throws SQLException, ClassNotFoundException {
      TermDao dao = new TermDao();
        System.out.println(dao.getTermByName("system"));
    }

    @Test
    public void testTermById() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        List<String> list = FileReaderWriter.read("unique.txt");
        for(String id : list){
            System.out.println(dao.getTermById(Integer.valueOf(id)).getHeadWord());
        }
    }


    @Test
    public void testIsTermDefinitionNull() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        assertTrue(dao.isTermDefinitionNull(299));
    }
    @Test
    public void testIsPolysemousTerm() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        assertTrue(dao.isPolysemousTerm(26306));
    }

    @Test
    public void testGetAllPolysemousTerm() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        FileReaderWriter.write("data/dictionary/term/AllPolysemousTerm.txt", dao.getAllPolysemousTerm());
    }

    @Test
    public void testGetTermsWithoutDef() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        List<Integer> list1 = FileReaderWriter.readNumber("AllPolysemousTerm.txt");
        System.out.println("list1:" + list1.size());
        List<Integer> list2 = FileReaderWriter.readNumber("data/idUniqueTerm.txt");
        System.out.println("list2:" + list2.size());
        list2.addAll(list1);
        System.out.println("list2:" + list2.size());
        FileReaderWriter.write("TermsWithoutDef.txt", dao.getTermWithoutDef(list2));
    }

    @Test
    public void testWriteToFileTerms() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        List<Integer> list2 = FileReaderWriter.readNumber("AllPolysemousTerm.txt");
        System.out.println("list2:" + list2.size());
        List<String> list = new ArrayList<String>();
        for(int id: list2){
            String termName = dao.getTermById(id).getHeadWord();
            if(termName.split(" ").length > 1){

            }else{
                list.add(termName);
            }
        }
        FileReaderWriter.write("data/AllPolysemousTerm_out.txt", list);
    }

    @Test
    public void testGetNotNUllTermDef() throws SQLException, ClassNotFoundException {
        TermDao termDao = new TermDao();
        int id = termDao.getTermByName("bit").getId();
        TermDefinitionDao dao = new TermDefinitionDao();
        System.out.println(dao.getNotNUllTermDefinitionByTermID(id).toString());
    }
}
