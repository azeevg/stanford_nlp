package sesa.test.common.parser;

import edu.spbstu.egor.SentenceGetter;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Parser;
import grammarscope.parser.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import sesa.common.builder.BuilderException;
import sesa.common.builder.DocumentBuilder;
import sesa.common.builder.StanfordRelationsBuilder;
import sesa.common.dataModule.BasicDataModule;
import sesa.common.dataModule.StanfordDataModule;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.semantics.SemanticRelation;
import sesa.common.utility.io.FileUtility;
import sesa.common.utility.semantic.SentenceUtils;
import sesa.tools.cache.CacheUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Ira on 15.05.2016.
 */
public class StanfordParserTest {
    public final Logger log = LogManager.getLogger(StanfordParserTest.class);
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
    private String languageModel = "./data/grammar/englishFactored.ser.gz";


    @Test
    public void testGetGrammaticalRelations() throws IOException, BuilderException, SQLException, ClassNotFoundException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        StanfordDataModule dataModule = new BasicDataModule();
        StanfordRelationsBuilder builder = new StanfordRelationsBuilder(inputStream,dataModule);
        builder.setIsModCKY(true);
        SentenceGetter sentenceGetter = new SentenceGetter("data/test.txt");
        ArrayList<ArrayList<SemanticRelation>> sesaGs = builder.getGrammaticalTextStructure(sentenceGetter);
        for(ArrayList<SemanticRelation> semanticRelations: sesaGs){
            System.out.println("##-----");
            for(SemanticRelation sr: semanticRelations){
                System.out.println(sr);
            }
        }
        System.out.println();
       // builder.setIsModCKY(false);
    }


    @Test
    public void testParseText() throws BuilderException, IOException, SQLException, ClassNotFoundException {
        TermDefinitionDao dao = new TermDefinitionDao();
        String text = dao.getTermDefinitionByTermID(797).get(0).getDefinition();
        System.out.println(text);
        StanfordDataModule dataModule = new BasicDataModule();
//        Document document = new Document(dataModule);
//        document.create(text);
        ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
        StanfordRelationsBuilder builder = new StanfordRelationsBuilder(inputStream,dataModule);
        builder.setIsModCKY(true);
        builder.parseText("text" ,text);
    }

    // test Stanford CKY parser
    @Test
    public void testStanfordCKYParser() throws IOException{

        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel);
        Parser parser = new Parser(inputStream);
        Sentence sentence = SentenceUtils.readSentenceFromFile("data/test.txt");
        Tree tree = parser.parse(sentence,true);
        System.out.println(tree.pennString());

    }


    @Test
    public void parseWikiText() throws FileNotFoundException, IOException {
        ObjectInputStream inputStream = IOUtils.readStreamFromString(languageModel); //parserModel

        String text = FileUtility.readAllText("./data/ada_cache/db_text_2.txt");
        StanfordDataModule dataModule = new BasicDataModule();

        DocumentBuilder builder = new StanfordRelationsBuilder(inputStream,dataModule);
         builder.setIsModCKY(true);


        try {
            builder.setData(text, "id");
            IDocument document = builder.buildDocument();
            // System.out.println(document.getSentences().size());
            CacheUtils.generatePaintedDocument(document);

        } catch (BuilderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
