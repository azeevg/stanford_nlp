package sesa.common.database.connections;

import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.ParsedRelation;
import sesa.common.database.entity.DBEntity;
import sesa.common.database.entity.IDBEntity;
import sesa.common.features.Option;
import sesa.common.utility.io.CommonUtils;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.json.JSONUtility;
import sesa.tools.statistics.RawSemanticGroup;
import sesa.tools.statistics.RawSemanticRelation;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.*;
import java.util.*;

/**
 * Класс однопоточного подключения к БД
 */

@Deprecated
public class DBSingleConnection extends DBConnection {
    private Connection connection = null;

    /**
     * Подключение к БД с помощью JDBC
     *
     * @param dbHost хост
     * @param dbPort порт
     * @param dbName имя БД
     * @param dbUser логин
     * @param dbPass пароль
     */
    public DBSingleConnection(String dbHost, int dbPort, String dbName, String dbUser, String dbPass) {
        super(dbHost, dbPort, dbName, dbUser, dbPass);

        try {
            String url = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;
            Class.forName("com.mysql.jdbc.Driver").newInstance(); // регистрируем драйвер
            connection = DriverManager.getConnection(url, dbUser, dbPass); // установка соединения
        } catch (Exception e) {
            debug("Can't establish connection to: " + dbUser + "@" + dbHost + ":" + dbPort + "/" + dbName);
            debug("Exception: " + e.getMessage());
            isConnected = false;
        }
    }

    @Override
    public DBConnection useConnection(int nodeIndex) {
        return this;
    }

    /**
     * Проверка на существование таблицы с именем collectionName
     *
     * @param collectionName имя таблицы
     * @return true/false  - найдена/ нет таблица с таким именем
     */
    public boolean tableExists(String collectionName) {
        try {
            DatabaseMetaData dbm = connection.getMetaData(); // класс для получения матаинформации о БД
            ResultSet tables = dbm.getTables(null, null, collectionName, null);
            return tables.next();
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * Insert to table like wiki
     * @param collectionName
     * @param document
     * @return
     */
    @Override
    public synchronized boolean insert(String collectionName, IDBEntity document) {
        PreparedStatement s = null;

        try {
            String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO `" + collectionName + "` (id, data, imported) VALUES(?,?,?)";
            s = connection.prepareStatement(query);
            s.setString(1, document.getID());
            s.setString(2, data);
            s.setTimestamp(3, getNow());
            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + document.getID());
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        return false;
    }

    @Override
    public boolean copyByCategory(String srcCollectionName, String destCollectionName, String regexpExpression) {

        PreparedStatement s = null;
        try {
            String query = "INSERT INTO " + destCollectionName + " SELECT * FROM " + srcCollectionName + " WHERE cat REGEXP ?";
            s = connection.prepareStatement(query);
            s.setString(1, regexpExpression);

            s.execute();
            s.close();

        } catch (SQLException e) {
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }
        return false;
    }

    /**
     * Где применяется?
     * @param collectionName
     * @param document
     * @param additional
     * @return
     */
    public synchronized boolean insert(String collectionName, IDBEntity document, Map<String, String> additional) {
        PreparedStatement s = null;

        try {
            String data = JSONUtility.toString(document.toMap());
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("INSERT INTO ").append(collectionName).append(" (id, data, imported");
            Set<String> keys = additional.keySet();
            for (String key : keys)
                queryBuilder.append(", ").append(key);
            queryBuilder.append(") ");
            queryBuilder.append("VALUES(?,?,?");
            for (int i = 0; i < keys.size(); i++)
                queryBuilder.append(",?");
            queryBuilder.append(")");

            s = connection.prepareStatement(queryBuilder.toString());
            s.setString(1, document.getID());
            s.setString(2, data);
            s.setTimestamp(3, getNow());

            int i = 1;
            for (String key : keys)
                s.setString(3 + i++, additional.get(key));

            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + document.getID());
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        return false;
    }

    @Override
    public synchronized final boolean update(String collectionName, IDBEntity document) {
        remove(collectionName, document.getID());
        return insert(collectionName, document);
    }

    /**
     * ???
     * @param collectionName
     * @param condition
     * @return
     */
    @Override
    public synchronized long count(String collectionName, Map<String, Object> condition) {
        return count(collectionName);
    }

    /**
     * TODO: Exception
     * @param collectionName
     * @return
     */
    @Override
    public synchronized long count(String collectionName) {
        PreparedStatement s = null;

        try {
            long collectionSize = 0;
            // количество строк
            s = connection.prepareStatement("SELECT COUNT(*) as cnt FROM `" + collectionName + "`");
            s.execute();

            ResultSet resultSet = s.getResultSet();
            if (resultSet.next())
                collectionSize = resultSet.getLong("cnt");
            resultSet.close();
            s.close();

            return collectionSize;
        } catch (SQLException e) {
            debug("Can't obtain collection size: " + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        return 0;
    }

    /**
     * Нигде не используется
     * @param collectionName
     * @param id
     * @return
     */

    @Override
    public synchronized boolean contains(String collectionName, String id) {
        PreparedStatement s = null;

        try {
            //long collectionSize = 0;
            s = connection.prepareStatement("SELECT COUNT(id) FROM `" + collectionName + "` WHERE id='" + id + "'");
            s.execute();

            ResultSet resultSet = s.getResultSet();
            boolean hasElements = resultSet.next();
            if (resultSet.next()) {
                String collectionSize = resultSet.getString("id");
            }
            resultSet.close();
            s.close();

            return hasElements;
        } catch (SQLException e) {
            debug("Can't obtain collection size: " + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        return false;
    }

    /**
     * Не используется
     * @param collectionName
     * @param id
     * @return
     */
    @Override
    public synchronized Option<IDBEntity> get(String collectionName, String id) {
        PreparedStatement s = null;
        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE _id = '" + id + "'");
            s.execute();

            ResultSet resultSet = s.getResultSet();
            if (resultSet.next()) {
                IDBEntity entity = new DBEntity((String) resultSet.getObject("id"), (String) resultSet.getObject("data"));
                resultSet.close();
                s.close();

                return new Option<IDBEntity>(entity);
            }
        } catch (SQLException e) {
            debug("Can't get record: " + id);
            debug("Exception: " + e.getMessage());
        }


        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return new Option<IDBEntity>(null);
    }

    public IDBEntity selectById(String collectionName, String id) {
        IDBEntity result = null;
        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE id = ?");
            s.setString(1, id);
            s.execute();

            ResultSet resultSet = s.getResultSet();

            if (resultSet.next())
                result = new DBEntity((String) resultSet.getObject("id"), (String) resultSet.getObject("data"));

            resultSet.close();
        } catch (SQLException e) {
            debug("Can't select data from table:" + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return result;
    }

    @Override
    public synchronized List<String> readParsingErrors() {
        List<String> resultList = new LinkedList<String>();
        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM errors");
            s.execute();

            ResultSet resultSet = s.getResultSet();
            while (resultSet.next())
                resultList.add(resultSet.getString("error"));

            resultSet.close();
        } catch (SQLException e) {
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return resultList;
    }

//TODO: select *  - нехорошо вытаскивать так
    @Override
    public synchronized List<IDBEntity> select(String collectionName, long from, long size) {
        List<IDBEntity> resultList = new LinkedList<IDBEntity>();
        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` LIMIT ?,?");
            s.setLong(1, from);
            s.setLong(2, size);
            s.execute();

            ResultSet resultSet = s.getResultSet();
            while (resultSet.next())
                resultList.add(new DBEntity((String) resultSet.getObject("id"), (String) resultSet.getObject("data")));

            resultSet.close();
        } catch (SQLException e) {
            debug("Can't select data from table:" + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return resultList;
    }

    /**
     * Какой смысл?
     * @param collectionName
     * @return
     */
    @Override
    public synchronized final List<IDBEntity> select(String collectionName) {
        return select(collectionName, 0, count(collectionName));
    }

    @Override
    public synchronized boolean create(String collectionName) {
        PreparedStatement s = null;

        try {
            String query = "CREATE TABLE `" + collectionName + "` (" +
                    "`id` VARCHAR(40)  NOT NULL, " +
                    "`imported` TIMESTAMP  NOT NULL DEFAULT NOW(), " +
                    "`data` LONGTEXT  NOT NULL, " +
                    "" +
                    "PRIMARY KEY (`id`)) " +
                    "ENGINE = MyISAM " +
                    "CHARACTER SET utf8 COLLATE utf8_general_ci";

            s = connection.prepareStatement(query);
            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't create collection: " + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return false;
    }

    @Override
    public synchronized boolean remove(String collectionName, String id) {
        PreparedStatement s = null;

        try {
            String query = "INSERT INTO `" + collectionName + "` (id, data, imported) VALUES(?,?,?)";
            s = connection.prepareStatement(query);
            s.setString(1, id);
            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't remove record with id: " + id);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return false;
    }

    @Override
    public synchronized boolean truncate(String collectionName) {
        Statement s = null;
        try {
            s = connection.createStatement();
            s.executeQuery("TRUNCATE TABLE `" + collectionName + "`");
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't truncate table: " + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return false;
    }

    @Override
    public synchronized boolean drop(String collectionName) {
        PreparedStatement s = null;

        try {
            String query = "DROP TABLE ?";
            s = connection.prepareStatement(query);
            s.setString(1, collectionName);
            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't drop collection: " + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return false;
    }


    // TODO delete

    @Override
    public boolean insertObject(String collectionName, String title, String hash, byte[] data) {
        PreparedStatement s = null;

        try {
            //String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO `" + collectionName + "` (title, hash, data) VALUES(?,?,?)";
            s = connection.prepareStatement(query);
            s.setString(1, title);
            s.setString(2, hash);
            s.setBytes(3, data);
            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + title);
            debug("Exception: " + e.getMessage());
            LoggingUtility.info("SQL Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        return false;
    }

    @Override
    public boolean insertObject(String collectionName, int title, int hash, String tree) {
        PreparedStatement s = null;

        try {
            //String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO `" + collectionName + "` (title, hash, tree) VALUES(?,?,?)";
            s = connection.prepareStatement(query);
            s.setInt(1, title);
            s.setInt(2, hash);
            s.setString(3, tree);
            s.execute();
            s.close();

            return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + title);
            debug("Exception: " + e.getMessage());
            LoggingUtility.info("SQL Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        return false;
    }

    @Override
    public InputStream readObject(String collectionName, String id) {
        InputStream result = null;
        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE id = ?");
            s.setString(1, id);
            s.execute();

            ResultSet resultSet = s.getResultSet();
            if (resultSet.next()) {
                result = resultSet.getBinaryStream("data");
            }

            resultSet.close();
        } catch (SQLException e) {
            debug("Can't select data from table:" + collectionName);
            debug("Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return result;
    }

    /**
     * Вытаскиваем по title
     * TODO delete
     * @param collectionName
     * @param title
     * @return
     */
    @Override
    public HashMap<Integer, Tree> getCachedTreesObjects(String collectionName, String title) {


        HashMap<Integer, Tree> trees = new HashMap<Integer, Tree>();
        InputStream result = null;

        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE title = ?");
            s.setString(1, title);
            s.execute();

            ResultSet resultSet = s.getResultSet();
            while (resultSet.next()) {
                //String treeString = resultSet.getString("tree");
                result = resultSet.getBinaryStream("data");
                //result.reset();
                ObjectInputStream ois = new ObjectInputStream(result);
                //ois.reset();

                Tree tree = (Tree) ois.readObject();

                int hash = resultSet.getInt("hash");
                //Tree tree = CommonUtils.readPennTreeFormString(treeString);
                //System.out.println("FUCK YEAHH");
                trees.put(hash, tree);
            }
        } catch (SQLException e) {
            debug("Can't select data from table:" + collectionName);
            debug("Exception: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            //System.out.println("FUCK");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return trees;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Вытаскиваем по titleHash
     * @param collectionName
     * @param titleHash
     * @return
     */
    @Override
    ///TODO!!! If exception happens  don't return empty HashMap trees! return NULL!
    public HashMap<Integer, Tree> getCachedTrees(String collectionName, int titleHash) {

        /**
         * Here we crate instance of HashMap for trees, which we create on-fly using their serialized form (bracket-form)
         * Sentence in bracket-form are retrieved from corresponding  table of DB
         *  */
        HashMap<Integer, Tree> trees = new HashMap<Integer, Tree>();

        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE title = ?");
            s.setInt(1, titleHash);
            s.execute();

            ResultSet resultSet = s.getResultSet();
            while (resultSet.next()) {
                String treeString = resultSet.getString("tree");
                int hash = resultSet.getInt("hash");
                Tree tree = CommonUtils.readPennTreeFormString(treeString);
                trees.put(hash, tree);
            }
        } catch (SQLException e) {
            debug("Can't select data from table:" + collectionName);
            debug("Exception: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
            ///TODO!!! If exception happens  don't return empty HashMap trees! return null!
            return null;
        }

        return trees;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> getCachedRelations(String collectionName, String title) {
        HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> relations = new HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>>();

        InputStream result = null;
        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE title = ?");
            s.setString(1, title);
            s.execute();

            ResultSet resultSet = s.getResultSet();
            while (resultSet.next()) {
                result = resultSet.getBinaryStream("data");
                String hash = resultSet.getString("hash");

                ObjectInputStream ois = new ObjectInputStream(result);
                Map<MutableGrammaticalRelation, List<ParsedRelation>> tree = (Map<MutableGrammaticalRelation, List<ParsedRelation>>) ois.readObject();
                result.close();
                ois.close();

                relations.put(Integer.parseInt(hash), tree);
            }

            resultSet.close();


        } catch (SQLException e) {
            debug("Can't select data from table:" + collectionName);
            debug("Exception: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }

        return relations;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Неполное сохранение какое-то, не по всем столбцам
     * Incomplete preservation
     * @param collectionName
     * @param token
     * @param tokenBase
     * @param role
     * @param position
     * @param groupHash
     * @param groupId
     */

    @Override
    public void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, int position, int groupHash, int groupId) {
        PreparedStatement s = null;

        try {
            //String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO `" + collectionName + "` (token, base, role, pos, group_hash, group_id) VALUES(?,?,?,?,?,?)";
            s = connection.prepareStatement(query);
            s.setString(1, token);
            s.setString(2, tokenBase);
            s.setString(3, role);

            s.setInt(4, position);
            s.setInt(5, groupHash);
            s.setInt(6, groupId);
            s.execute();
            s.close();

            //  return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + token);
            debug("Exception: " + e.getMessage());
            LoggingUtility.info("SQL Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }

        //return false;
    }

    @Override
    public void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, String pos, int index, String groupId, int sentenceId, int sentenceService, int prevDelimiter, int sentenceSize) {
        PreparedStatement s = null;

        try {
            String query = "INSERT INTO `" + collectionName + "` (token, base, role, pos, sentence_index, group_id, sentence_id, sentence_service, prev_delimiter, sentence_size) VALUES(?,?,?,?,?,?,?,?,?,?)";
            s = connection.prepareStatement(query);

            s.setString(1, token);
            s.setString(2, tokenBase);
            s.setString(3, role);
            s.setString(4, pos);
            s.setInt(5, index);
            s.setString(6, groupId);
            s.setInt(7, sentenceId);
            s.setInt(8, sentenceService);
            s.setInt(9, prevDelimiter);
            s.setInt(10, sentenceSize);

            s.execute();
            s.close();

            //  return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + token);
            debug("Exception: " + e.getMessage());
            LoggingUtility.info("SQL Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }
    }


    public class RelationComparator implements Comparator<RawSemanticRelation> {
        public int compare(RawSemanticRelation o1, RawSemanticRelation o2) {
            return o1.getIndex() - o2.getIndex();
        }
    }

    /**
     * При вытаскивании информации в RawSemanticGroup записывается не полностью
     * @param collectionName
     * @return
     */
    public Collection<RawSemanticGroup> getSemanticGroups(String collectionName) {

        //List<RawSemanticGroup> groups = new LinkedList<RawSemanticGroup>();
        Map<String, RawSemanticGroup> groups = new HashMap<String, RawSemanticGroup>();

        PreparedStatement s = null;

        RelationComparator relationComparator = new RelationComparator();
        try {
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "`");
            s.execute();

            ResultSet resultSet = s.getResultSet();


            while (resultSet.next()) {
                String token = resultSet.getString("token");
                String base = resultSet.getString("base");
                String role = resultSet.getString("role");
                String pos = resultSet.getString("pos");
                int index = resultSet.getInt("sentence_index");
                String groupId = resultSet.getString("group_id");
                //int sentenceId = resultSet.getInt("sentence_id");
                boolean hasService = resultSet.getInt("sentence_service") == 1;
                boolean prevDelimiter = resultSet.getInt("prev_delimiter") == 1;
                int sentenceSize = resultSet.getInt("sentence_size");


                if (!groups.containsKey(groupId)) {
                    RawSemanticGroup rsg = new RawSemanticGroup(relationComparator);
                    //rsg.setId(groupId);
                    //rsg.setSentenceId(sentenceId);
                    rsg.setSentenceHasServiceTokens(hasService);
                    rsg.setSentenceSize(sentenceSize);
                    groups.put(groupId, rsg);
                }

                RawSemanticGroup rsg = groups.get(groupId);
                RawSemanticRelation rsr = new RawSemanticRelation();
                rsr.setTokenBaseRolePos(token + "|" + base + "|" + role + "|" + pos);
        /*rsr.setToken(token);
        rsr.setTokenBase(base);
        rsr.setRole(role);
        rsr.setPos(pos);*/
                rsr.setIndex(index);
                rsr.setPrevDelimiter(prevDelimiter);
                rsg.addRelation(rsr);
            }

            resultSet.close();
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }


        return groups.values();
    }

    /**
     * Нет таблицы counters
     * @param key
     * @return
     */
   // TODO:  Delete. The database contains no table named "counters".
    @Override
    public int getCounter(String key) {
        PreparedStatement s = null;

        try {
            s = connection.prepareStatement("SELECT * FROM `counters` WHERE name = ?");
            s.setString(1, key);
            s.execute();

            ResultSet resultSet = s.getResultSet();

            if (resultSet.next()) {
                int count = resultSet.getInt("count");
                return count;
            }
        } catch (SQLException e) {
            debug("Can't select counter");
            debug("Exception: " + e.getMessage());
        }
        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }
        return -1;
    }

    /**
     *
     * @param key
     * @param value
     */
   // TODO:  Delete. The database contains no table named "counters".
    @Override
    public void setCounter(String key, int value) {

        PreparedStatement s = null;

        try {
            String query = "UPDATE `counters` SET `count`=" + value + " WHERE `name`='" + key + "'";
            s = connection.prepareStatement(query);
            s.execute();
            s.close();
        } catch (SQLException e) {
            debug("Can't update counter");
            debug("Exception: " + e.getMessage());
            LoggingUtility.info("SQL Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {
        }
    }

    /**
     * ок
     * @param key
     * @param type
     * @param message
     */
    @Override
    public void writeErrorDocument(String key, String type, String message) {
        PreparedStatement s = null;

        try {
            //String data = JSONUtility.toString(document.toMap());
            String query = "INSERT INTO errors (error, errorType, message) VALUES(?,?,?)";
            s = connection.prepareStatement(query);
            s.setString(1, key);
            s.setString(2, type);
            s.setString(3, message);

            s.execute();
            s.close();

            //  return true;
        } catch (SQLException e) {
            debug("Can't insert record: " + key);
            debug("Exception: " + e.getMessage());
            LoggingUtility.info("SQL Exception: " + e.getMessage());
        }

        try {
            if (s != null)
                s.close();
        } catch (SQLException ignored) {

        }
    }
}