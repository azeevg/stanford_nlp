package sesa.tools.statistics;

import sesa.common.builder.BuilderException;
import sesa.common.configuration.Categories;
import sesa.common.database.entity.IDBEntity;
import sesa.common.database.common.IDBIterator;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.model.processing.document.IDocument;
import sesa.common.model.processing.sentence.SentenceTree;
import sesa.common.utility.common.Application;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.builder.DictionaryDocumentBuilder;
import sesa.common.utility.vector.DocumentUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class OverallCalculator extends Application {

    private String[] categoryNames = Categories.ALL_CATEGORIES;
    private DocumentsDao documentsDao = new DocumentsDao();

    public OverallCalculator() throws SQLException, ClassNotFoundException {
    }

    @Override
    protected boolean body() throws SQLException, ClassNotFoundException {

        for (String categoryName : categoryNames) {

            String categoryCollection = Categories.buildCategoryHtmlCollectionName(categoryName);
            IDBIterator iterator = documentsDao.getDocumentsCollectionIterator(categoryName);//dbController.getDocumentsCollectionIterator(categoryCollection);
            //iterator = new DBReportingIterator(iterator, categoryCollection + " overall statistic");

            long sentenceCount = 0;
            long tokenCount = 0;

            while (iterator.hasNext()) {
                IDBEntity doc = iterator.next().get();
                Map<String, Object> map = doc.toMap();
                String id = doc.getID();


                String text = (String) map.get("w_filtered_text");
                IDocument document = null;
                DictionaryDocumentBuilder documentBuilder = new DictionaryDocumentBuilder();
                try{
                    document = documentBuilder.buildDocument(id, text, null);
                }catch (BuilderException e){
                    e.printStackTrace();
                    //TODO: add log
                }

             //   IDocument document = DictionaryDocumentBuilder.buildDocument(id, text, null);
                List<SentenceTree> sentences = document.getSentences();
                List<String> tokens = DocumentUtils.getTokens(sentences);
                sentenceCount += sentences.size();
                tokenCount += tokens.size();
            }

            LoggingUtility.info("SENT IN CAT " + categoryName + " = " + sentenceCount);
            LoggingUtility.info("TOKEN IN CAT " + categoryName + " = " + tokenCount);
        }

        return false;
    }
}
