package sesa.common.utility.dictionary.downloader;

/**
 * Created by Ira on 27.04.2016.
 */

import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.database.dao.dictionary.TermDefinitionDao;
import sesa.common.database.dao.dictionary.TermLinksDao;
import sesa.common.database.entity.dictionary.TermDefinitionEntity;
import sesa.common.model.dictionary.DictionaryTerms;
import sesa.common.model.dictionary.Term;
import sesa.common.utility.io.DirectoryUtility;

import java.sql.SQLException;
import java.util.*;

/**
 * Use this class to download all dictionary files and save to database
 */
public class AppDictionarySaver {

    Map<String, Integer> allTerms;
    String dir = "";

    public AppDictionarySaver() throws SQLException, ClassNotFoundException {
        TermDao dao = new TermDao();
        this.allTerms = dao.getAllTerms();
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    /**
     * Use this method to download dictionary from file and add to data base
     */
    public void runSaveDictionaryToDataBase() throws SQLException, ClassNotFoundException {

        DictionaryDownloader downloader = new DictionaryDownloader();
        List<String> files = DirectoryUtility.getAllFilesInDirectories(dir);
        for (String file : files) {
            System.out.println(file);
            DictionaryTerms dictionary = downloader.readDictionaryFromFile(file);
            saveTermsDefinitionToDataBase(dictionary);
        }

    }

    public void runSaveDictionaryFromFile(String fileName) throws SQLException, ClassNotFoundException {
        DictionaryDownloader downloader = new DictionaryDownloader();
        DictionaryTerms dictionary = downloader.readDictionaryFromFile(fileName);
        saveTermsDefinitionToDataBase(dictionary);
    }

    public void runSaveDictionaryFromFile1(String fileName) throws SQLException, ClassNotFoundException {
        DictionaryDownloader downloader = new DictionaryDownloader();
        DictionaryTerms dictionary = downloader.readDictionaryFromFile(fileName);

        List<Term> terms = dictionary.getTerms();
        terms.sort(Comparator.comparing(Term::getName));
        TermDao termDao = new TermDao();
        TermDefinitionDao termDefinitionDao = new TermDefinitionDao();

        String prevTerm = "";
        int prevId = 0;

        for (int i = 0; i < terms.size(); i++) {
            Term term = terms.get(i);
            int id;
            if (term.getName().equals(prevTerm)) {
                id = prevId;
            } else {
                Set<String> set = new HashSet<>();
                set.add(term.getName());
                id = termDao.addToTermsCollectionsAndReturnAdded(set).values().iterator().next();
            }

            if (id == 0) {
                System.err.println("ERROR: id == 0");
                return;
            }
            TermDefinitionEntity entity = new TermDefinitionEntity(0, id, term.getDef());
            termDefinitionDao.addToTermDefinitionCollections(entity);

            prevId = id;
            prevTerm = term.getName();

        }
    }

    public void saveTermsDefinitionToDataBase(DictionaryTerms dictionary) throws SQLException, ClassNotFoundException {
        List<Term> terms = dictionary.getTerms();
        List<TermDefinitionEntity> entityList = new ArrayList<TermDefinitionEntity>();
        for (Term term : terms) {
            //System.out.println(term.getName());
            TermDefinitionEntity entity = new TermDefinitionEntity();
            entity.setDefinition(updateDef(term.getDef()));
            entity.setLinks(getIdLinks(term.getLinks()));
            entity.setIdTerm(allTerms.get(term.getName().toLowerCase()));
            entityList.add(entity);
        }

        TermDefinitionDao definitionDao = new TermDefinitionDao();
        definitionDao.addToTermDefinitionCollections(entityList);
        TermLinksDao linksDao = new TermLinksDao();
        linksDao.saveToLinksCollections(entityList);
        //linksSaveToDB(entityList);
    }

    private void linksSaveToDB(List<TermDefinitionEntity> entities) throws SQLException, ClassNotFoundException {
        TermLinksDao linksDao = new TermLinksDao();
        for (TermDefinitionEntity entity : entities) {
            linksDao.saveToLinksCollections(entity.getIdDefinition(), entity.getLinks());
        }

    }

    public List<Integer> getIdLinks(List<String> links) {
        List<Integer> idLinks = new ArrayList<Integer>();
        //allTerms.
        if (links == null) {
            System.err.println("links = null");
            return null;
        }

        for (String link : links) {
            link = link.toLowerCase();
            idLinks.add(allTerms.get(link));
        }
        return idLinks;
    }

    /**
     * @param dir name of directories where save all dictionary
     */
    public void runSaveTermsToDataBase(String dir) throws SQLException, ClassNotFoundException {
        TermsDownloader downloader = new TermsDownloader();
        List<String> files = DirectoryUtility.getAllFilesInDirectories(dir);
        TermDao dao = new TermDao();
        // Map<String , Integer> termsMap = new HashMap<String , Integer>();
        for (String file : files) {
            Set<String> terms = downloader.readTermsFromFile(file);
            dao.addToTermsCollections(terms);
            // termsMap.putAll(dao.addToTermsCollectionsAndReturnAdded(terms));
        }
    }

    private String updateDef(String def) {
        if (def.length() <= 2) {
            return " ";
        }
        return def;
    }


}
