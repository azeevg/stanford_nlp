package sesa.tools.semanticWeb;


public class Vertex implements Comparable<Vertex> {
    private String name;

    public Vertex(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * ��������� �� �����
     * @param vertex
     * @return
     */
    public int compareTo(Vertex vertex) {
        return name.compareTo(vertex.getName());
    }
}
