package sesa.common.utility.dictionary.remove;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import sesa.common.database.dao.DictionaryDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.collocation.CollocationUtility;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ira on 01.05.2016.
 */

/**
 * Use for glossary
 */
public class CrossReferenceFinder {
    public final Logger log = LogManager.getLogger(CrossReferenceFinder.class);


    @Test
    public void start() throws SQLException, ClassNotFoundException {
        String[] fileName = {"./data/dictionary/glossary/GIS Glossary a_g.json", "./data/dictionary/glossary/GIS Glossary h_t.json","./data/dictionary/glossary/fiber optics glossary.json"};
        String[] glossaryName = {"GIS glossary: a-g", "GIS glossary: h-t","fiber optics glossary"};
        DictionaryDao dictionaryDao = new DictionaryDao();
        FileManager manager = new FileManager("path");
        log.info("Save collocation to file:" + manager.getValue("path.computerCollocations"));

        int i = 0;
        for (String file : fileName) {
            Map<String, String> map = downloaderDictionaryFromFile(file);

            CollocationUtility.addToCollocationFiles( CollocationUtility.getCollocations(CollocationUtility.convertSetToList(map.keySet())),
                                                        manager.getValue("path.computerCollocations"));
           // Map<String, String> res = findReferencesAndSave(map, glossaryName[i]);
          //  i++;
          //  System.out.println(res);
            //dictionaryDao.addToDictionaryCollections(res, "comp_glossary");
        }
        log.info("-- saved --");


    }

    public Map<String, String> findReferencesAndSave(Map<String, String> map, String glossary) {
        Map<String, String> newMap = new HashMap<String, String>();
        HashMap<String, List<String>> linksMap = new HashMap<String, List<String>>();

        for (String key : map.keySet()) {

            JSONObject object = new JSONObject();
            //{"term_name":"z-axis","links_term":["x-y matrix"]}
            log.info("Start find links to word:" + key);
            StringBuilder builder = new StringBuilder(map.get(key));
            List<String> list = findLinksInText(builder);
            if (!key.equals(glossary)) {
                list.add(glossary);
            }
            object.put("term_name", key);
            object.put("links_term", list);
            //linksMap.put(key, findLinksInText(builder));
            newMap.put(key, builder.toString());
            FileReaderWriter.write("Glossary_link.txt", object.toString());
        }
        // System.out.println("linksMap:" + linksMap);

        return newMap;

    }

    /**
     * 1. Find all references in definition ( find ref in the sentence which look like "See smart card")
     * 2. Update definition ( - remove references)
     * 3. Put to refMap new element: term and list refs
     * After that method def will be without all links
     *
     * @param def definition
     * @return list of references
     */
    public List<String> findLinksInText(StringBuilder def) {

        List<String> links = new ArrayList<String>();

        String pattern = "see\\s+[\\w & , ( ) / � \\- \" \\\\]+|See\\s+[\\w & , ( ) / � \\- \" \\\\]+";
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(def.toString());

        while (matcher.find()) {
            String tmp = matcher.group();
            System.out.println(tmp);
            removeSentenceFromText(tmp, def);
            StringBuilder builder = new StringBuilder();
            builder.append(tmp, 4, tmp.length());
            String builderString = builder.toString();

            for (String retval : builderString.split("\\sand\\s|,")) {
                links.add(retval.trim());

            }
        }

        return links;
    }


    public List<String> getMixedLinks(String def) {
        List<String> mixedLinks = new ArrayList<String>();
        // String pattern = "see\\s+[\\w & , ( ) / � -]+|See\\s+[\\w & , ( ) / � -]+";
        String pattern = "see\\s+[\\w & , ( ) / � \\- \" \\\\]+|See\\s+[\\w & , ( ) / � \\- \" \\\\]+";
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(def);
        while (matcher.find()) {
            String tmp = matcher.group();
            mixedLinks.add(tmp);
        }

        return mixedLinks;
    }

    public List<String> getReferences(List<String> mixedLinks) {
        List<String> links = new ArrayList<String>();
        for (String link : mixedLinks) {
            StringBuilder builder = new StringBuilder();
            builder.append(link, 4, link.length());
            String builderString = builder.toString();

            for (String retval : builderString.split("\\sand\\s|,")) {
                links.add(retval.trim());
            }
        }
        return links;
    }

    public Map<String, String> downloaderDictionaryFromFile(String fileName) {
        log.info("Start to download dictionary from file:" + fileName);
        Map<String, String> map = new TreeMap<String, String>();
        JSONParser parser = new JSONParser();

        try {
            Scanner in = new Scanner(new File(fileName));
            while (in.hasNextLine()) {
                String jsonStr = in.nextLine();

                try {
                    Object obj = parser.parse(jsonStr);
                    JSONObject jsonObject = (JSONObject) obj;
                    if (jsonObject.get("term_name") != null) {
                        map.put(jsonObject.get("term_name").toString(), jsonObject.get("term_def").toString());
                    } else if (jsonObject.get("titles_terms") != null) {


                    }

                } catch (ParseException pe) {
                    log.error(pe.getMessage());
                    log.error(jsonStr);
                    pe.printStackTrace();
                }

            }

            log.info("Number of terms:" + map.keySet().size());
            log.info("-- end of downloaded --  ");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return map;
    }

    public String removeSentenceFromText(String removeSentence, String text) {
        return text.replace(removeSentence, "");
    }

    public void removeSentenceFromText(String removeSentence, StringBuilder text) {
        int start = text.indexOf(removeSentence);
        int end = start + removeSentence.length();
        text.replace(start, end, "");
    }


    public String removeSentencesFromText(List<String> removeSentences, String text) {
        for (String str : removeSentences) {
            text = removeSentenceFromText(str, text);
        }
        return text;
    }

    /**
     * 1. Find all references in definition ( find ref in the sentence which look like "See smart card")
     * 2. Update definition ( - remove references)
     * 3. Put to refMap new element: term and list refs
     * @param text definition
     * @return list of references
     */
//    public List<String> findLinksInText(StringBuilder text) {
//        String str = text.toString();
//        List<String> links = new ArrayList<String>();
//        List<String> removeSentences = new ArrayList<String>();
//        String pattern = "see\\s+[\\w ,]+|See\\s+[\\w ,]+";
//        Pattern p = Pattern.compile(pattern);
//        Matcher matcher = p.matcher(str);
//
//        while (matcher.find()) {
//            String tmp = matcher.group();
//
//            str = removeSentenceFromText(tmp,str);
//            removeSentences.add(tmp);
//
//            StringBuilder builder = new StringBuilder();
//            builder.append(tmp, 4, tmp.length());
//            String builderString = builder.toString();
//
//            for (String retval: builderString.split("\\sand\\s|,")){
//                links.add(retval.trim());
//
//            }
//        }
//        text.delete(0, text.length());
//        text.append(str);
//
//        return links;
//    }


}
