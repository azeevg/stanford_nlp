package edu.spbstu.egor;

import edu.spbstu.appmath.collocmatch.Sentence;
import edu.stanford.nlp.trees.Tree;

/**
 * Created by Egor_Gorbunov on 6/18/2015.
 * Emain: egor_gorbunov@epam.com
 */
public class ClassForTest {

    private static Grammar grammar;
    private static TreeGetter treeGetter;
    private static TreeUtils treeUtils;

    static void prepare() {
        grammar = new Grammar();
        grammar.op.testOptions.verbose = false;
        grammar.op.testOptions.maxSpanForTags = 4;
        grammar.op.testOptions.lengthNormalization = true;
        ParserFactory parserFactory = new ParserFactory(grammar);
        treeGetter = new TreeGetter();
        treeGetter.setPparser(parserFactory.getPCFGParser());
        treeUtils = new TreeUtils(grammar.op);
    }

    public static void main(String[] args) {
        prepare();

        Sentence sentence = new Sentence("Student wrote a very good thesis");

        Tree bestParseTree = treeGetter.getBestParseTree(SentenceGetter.convert(sentence));
        treeUtils.preprocessedTikzPrint(bestParseTree);
    }
}
