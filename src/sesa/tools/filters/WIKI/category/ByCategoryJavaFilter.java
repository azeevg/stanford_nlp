package sesa.tools.filters.WIKI.category;
import sesa.common.configuration.Categories;
import sesa.common.utility.common.Application;

import java.sql.SQLException;

public class ByCategoryJavaFilter extends Application {

  @Override
  protected boolean body() throws SQLException, ClassNotFoundException {
   // dbController.useConnection(1);
    filterByCategories(new String[]{Categories.CHEMISTRY_CATEGORY, Categories.PHYSICS_CATEGORY, Categories.BIOLOGY_CATEGORY});
    return true;
}

  private void filterByCategory(String categoryName) throws SQLException, ClassNotFoundException {
    ByCategoryFilter computerFilter = new ByCategoryFilter(configuration, categoryName);
    computerFilter.filter();
  }

  private void filterByCategories(String[] categoryNames) throws SQLException, ClassNotFoundException {
    for(String categoryName : categoryNames)
      filterByCategory(categoryName);
  }
}
