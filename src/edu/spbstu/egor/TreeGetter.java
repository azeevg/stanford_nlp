package edu.spbstu.egor;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.ScoredObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Egor Gorbunov on 20.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class TreeGetter {
    private ExhaustivePCFGParser pparser;
    private ExhaustiveDependencyParser dparser;

    public ArrayList<Tree> getKBestParseTrees(List<? extends HasWord> sentence, int k) {
        ArrayList<Tree> curKBestTrees = new ArrayList<Tree>(k);

        pparser.parse(sentence);

        List<ScoredObject<Tree>> kBestParses = pparser.getKBestParses(k);
        for (ScoredObject<Tree> scoredObject : kBestParses) {
            curKBestTrees.add(scoredObject.object());
        }

        pparser.clearArrays();

        return curKBestTrees;
    }

    public Tree getBestParseTree(List<? extends HasWord> sentence) {
        if (pparser.parse(sentence)) {
            return pparser.getBestParse();
        }
        return null;
    }

    public Tree getDependencyTree(List<? extends HasWord> sentence) {
        if (dparser.parse(sentence)) {
            return dparser.getBestParse();
        }
        return null;
    }

    public void setPparser(ExhaustivePCFGParser pparser) {
        this.pparser = pparser;
    }

    public void setDparser(ExhaustiveDependencyParser dparser) {
        this.dparser = dparser;
    }
}
