package grammarscope.parser;

/**
 * Relation groupings
 * 
 * @author Bernard Bou
 */
public class RelationSets extends DefaultMutableGrammaticalRelations
{
	/**
	 * Overlay default order (first=bottom)
	 */
	static public String[] theOrderedGrammaticalRelations = { "pred", // Reagan DIED
			"attr", // he seemed COMPETENT

			"cop", // Bill IS big

			"nsubj", // CLINTON defeated Dole
			"csubj", // WHAT SHE SAID makes sense
			"nsubjpass", // DOLE was defeated by Clinton
			"csubjpass", // THAT SHE LIED was suspected by everyone
			"xsubj", // TOM likes to eat fish
			"expl", // THERE is a statue in the corner
			"agent", // the man has been killed BY THE POLICE

			"dobj", // she gave me A RAISE
			"iobj", // she gave ME a raise
			"pobj",

			"pcomp", // they heard about YOU MISSING CLASSES / we have no useful information on WHETHER USERS ARE AT RISK
			"ccomp", // he says THAT YOU LIKE TO SWIM / she is certain THAT HE DID IT
			"xcomp", // I like TO SWIM / I am ready TO LEAVE
			"acomp", // she looks VERY BEAUTIFUL

			"conj", // Bill is BIG and HONEST

			"complm", // he said THAT you like to swim
			"mark", // US forces have been engaged in intense fighting AFTER insurgents launched simultaneous attacks

			"tmod", // LAST NIGHT, I swam in the pool
			"purpcl", // he talked to the president IN ORDER TO SECURE THE ACCOUNT
			"advcl", // IF YOU KNOW WHO DID IT, you should tell the teacher / the accident happened AS THE NIGHT WAS FALLING

			"rel", // I saw the man WHOSE WIFE you love
			"ref", // I saw THE BOOK which you saw
			"rcmod", // I saw the man YOU LOVE / I saw the book WHICH YOU BOUGHT
			"partmod", // truffles PICKED DURING SPRING are tasty
			"infmod", // points TO ESTABLISH are ...
			"amod", // Sam eats RED meat

			"appos", // Sam, MY BROTHER, eats red meat
			"abbrev", // the British Broadcasting Corporation (BBC)

			"num", // Sam eats 2 sheep
			"number", // I lost twelve MILLION dollars
			"quantmod", // ABOUT 200 people came
			"advmod", // GENETICALLY modified food

			"poss", // THEIR offices / BILL's clothes
			"possessive", // John'S books
			"nn", // OIL PRICE futures
			"prep", // I saw the cat IN the hat / I saw the cat WITH a telescope
			"prt", // they shut DOWN the station

			"det", // THE man is here
			"predet", // ALL the boys are here

			"measure", // the director is 60-YEAR old

			"neg", // he is NOT a scientist / he doesN'T drive
			"aux", // Reagan has died
			"auxpass", // Kennedy has BEEN killed

			"cc", // Bill is big AND honest
			"preconj", // BOTH the boy an the girl are here

			"punct",

			// super empty relations
			"arg", "subj", "compl", "obj", "mod", "sdep",

			"gov", "dep" };

	/**
	 * Semantic predicate
	 */
	//static public String[] thePredicateGrammaticalRelations = { "pred", "attr" };

  //backup settings
  /*static public String[] thePredicateGrammaticalRelations = { "pred" ,"attr"};
  static public String[] theSubjectGrammaticalRelations = { "nsubj", "xsubj", "csubj" };
  static public String[] theObjectGrammaticalRelations = { "dobj", "iobj", "pobj"};*/

  static public String[] thePredicateGrammaticalRelations = { "pred" ,"attr", "aux", "auxpass", "cop"};
  static public String[] theSubjectGrammaticalRelations = { "subj", "nsubj", "nsubjpass", "csubj", "csubjpass", "xsubj", "agent" };
  static public String[] theObjectGrammaticalRelations = { "obj", "dobj", "iobj", "pobj"};


	/**
	 * Semantic subject
	 */


	/**
	 * Semantic object
	 */

}
