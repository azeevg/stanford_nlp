package sesa.common.utility.dictionary;

/**
 * Created by gleb on 15.04.17.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Covert the following format:
 * <p>
 * <HEAD_WORD>term_1</HEAD_WORD>
 * {definition_1_1}
 * {definition_1_2}
 * ...
 * <HEAD_WORD>term_2</HEAD_WORD>
 * {definition_2_1}
 * {definition_2_2}
 * ...
 * <p>
 * into appropriate JSON:
 * <p>
 * {"term_def":"definition","term_link":["related_term_1", "related_term_2", ...],"term_name":"term"}
 * ...
 * <p>
 * The key goal is to reuse AppDictionarySaver class. See {@link sesa.test.dictionary.DictionaryDownloaderTest}.
 */

public class FreeFormat2JsonConverter {
    private static final String HEAD_WORD_REGEXP = "<HEAD_WORD>(.+)</HEAD_WORD>";


    public List<List<String>> parseDirectory(File dir) throws FileNotFoundException {
        List<List<String>> terms = new ArrayList<>();

        if (!dir.isDirectory())
            throw new IllegalArgumentException();
        for (File filepath : dir.listFiles()) {
            terms.addAll(parseFile(filepath));
        }

        return terms;
    }

    public List<List<String>> parseFile(File filepath) throws FileNotFoundException {
        Scanner scanner = new Scanner(filepath);
        List<List<String>> terms = new ArrayList<>();
        List<String> currentTerm = null;
        Pattern headWordPattern = Pattern.compile(HEAD_WORD_REGEXP);
        Pattern definitionPattern = Pattern.compile("\\{(.*)\\}");

        System.out.println(filepath.getName());

        int headWords = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine().trim();

            if (line.length() == 0) {
                continue;
            }
            if (line.length() <= 3 && line.length() > 0) {
                System.out.println("-->" + line);
            }
            if (line.matches(HEAD_WORD_REGEXP)) {
                headWords++;
                if (currentTerm != null)
                    terms.add(currentTerm);

                currentTerm = new ArrayList<>();
                Matcher matcher = headWordPattern.matcher(line);
                if (matcher.find() && line.length() != 0) {
                    currentTerm.add(matcher.group(1));
                }
            } else if (line.matches("\\{(.*)\\}")) {
                Matcher matcher = definitionPattern.matcher(line);
                if (matcher.find() && line.length() != 0 && currentTerm != null) {
                    currentTerm.add(matcher.group(1));
                }
            } else {
                System.out.println(line);
            }

        }
        if (currentTerm != null)
            terms.add(currentTerm);

        System.out.println(headWords + " head words parsed.");

        return terms;
    }

    public void writeToFile(File path, List<List<String>> data) {

        try (FileWriter writer = new FileWriter(path)) {


            for (int i = 0; i < data.size(); i++) {
                for (int j = 1; j < data.get(i).size(); j++) {
                    StringJoiner joiner = new StringJoiner(",");
                    joiner.add("\"term_def\":\"" + data.get(i).get(j).trim().replaceAll("\"", "\\\\\"") + "\"")
                            .add("\"term_link\":[]")
                            .add("\"term_name\":\"" + data.get(i).get(0).trim().replaceAll("\"", "\\\\\"") + "\"");

                    String s = "{" + joiner.toString() + "}\n";
//                    System.out.println(s);
                    writer.append(s);
                }
            }
        } catch (Exception e) {

        }

    }

    public void convertAll(File source, File destination) throws IOException {
        List<List<String>> terms = new ArrayList<>();

        if (source == null || !source.isDirectory())
            throw new IllegalArgumentException();

        for (File filepath : source.listFiles()) {
            File newFile = new File(destination.getAbsolutePath() + "/" + filepath.getName());
            writeToFile(newFile, parseFile(filepath));
        }

    }
}
