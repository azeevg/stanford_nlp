package sesa.common.model.processing.support;


public enum GroupMatchType {
  NotMatched,
  Matched,
}
