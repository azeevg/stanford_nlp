package sesa.tools.filters.WIKI.html;

import info.bliki.wiki.model.WikiModel;

import java.util.Map;

public class FilteringWikiModel extends WikiModel {

  public FilteringWikiModel(String imageBaseURL, String linkBaseURL) {
    super(imageBaseURL, linkBaseURL);
  }

  @Override
  public String getRawWikiContent(String namespace, String articleName,
    Map<String, String> templateParameters) {
      String rawContent = super.getRawWikiContent(namespace, articleName, templateParameters);
      return rawContent == null ? "" : rawContent;
    }
}
