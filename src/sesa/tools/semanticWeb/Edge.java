package sesa.tools.semanticWeb;


public class Edge<T> implements Comparable<Edge> {
    private String sourceVertexID;
    private String destinationVertexID;
    private T data;
    private double weight;

    public Edge(String sourceVertexID, String destinationVertexID) {
        this.sourceVertexID = sourceVertexID;
        this.destinationVertexID = destinationVertexID;
        this.data = null;
        this.weight = 0.0;
    }

    public Edge(String sourceVertexID, String destinationVertexID, T data) {
        this.sourceVertexID = sourceVertexID;
        this.destinationVertexID = destinationVertexID;
        this.data = data;
        this.weight = 0.0;
    }

    public Edge(String sourceVertexID, String destinationVertexID, T data, double weight) {
        this.sourceVertexID = sourceVertexID;
        this.destinationVertexID = destinationVertexID;
        this.data = data;
        this.weight = weight;
    }

    public String getSourceVertexID() {
        return sourceVertexID;
    }

    public String getDestinationVertexID() {
        return destinationVertexID;
    }

    public T getData() {
        return data;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return sourceVertexID + "->" + destinationVertexID;
    }

    public int compareTo(Edge anotherEdge) {
        return destinationVertexID.compareTo(anotherEdge.getDestinationVertexID());
    }
}
