package sesa.common.model.processing.document;

import sesa.tools.math.Vector;
import sesa.common.model.processing.sentence.SentenceToken;
import sesa.common.model.processing.sentence.SentenceTree;

import java.util.Set;

public class CategorizedDocument extends Document {

    private Set<String> categoryDictionary;

    public CategorizedDocument(String id, String title, DocumentNode tree, Set<String> categoryDictionary) {
        super(id, title, tree);
        this.categoryDictionary = categoryDictionary;
    }

    @Override
    protected void updateVector(Vector v, SentenceTree tree, Double level) {

        if (tree == null)
            return;

        double ultraWeight = 1.0 / Math.pow(2, level);
        for (SentenceToken token : tree.getTokens()) {
            String tokenValue = token.getValue();
            if (categoryDictionary.contains(tokenValue)) {
                v.addComponent(tokenValue, ultraWeight);
            }
        }
    }

    public String getId() {
        return id;
    }
}
