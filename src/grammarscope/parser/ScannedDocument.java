package grammarscope.parser;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.ling.Word;

public class ScannedDocument extends SegmentedDocument
{
	/**
	 * Text
	 */
	public String theText;

	/**
	 * Locator
	 */
	public Map<Sentence, SentenceLocation> theLocator;

	/**
	 * Constructor
	 */
	public ScannedDocument()
	{
		super();
	}

	/**
	 * Scan
	 */
	public void scan()
	{
		this.theLocator = new IdentityHashMap<Sentence, SentenceLocation>();
		int thisPointer = 0;
		final StringBuilder thisBuilder = new StringBuilder();
		for (final Sentence thisSentence : this.theSentences)
		{
			final SentenceLocation thisSentenceLocation = new SentenceLocation();
			this.theLocator.put(thisSentence, thisSentenceLocation);
			thisSentenceLocation.from = thisPointer + (thisPointer > 0 ? 1 : 0);
			thisSentenceLocation.theWordMap = new IdentityHashMap<Word, Location>();
			for (final Word thisWord : thisSentence)
			{
				if (thisPointer > 0)
				{
					thisBuilder.append(' ');
					thisPointer++;
				}
				final Location thisWordLocation = new Location();
				thisSentenceLocation.theWordMap.put(thisWord, thisWordLocation);
				thisWordLocation.from = thisPointer;

				final String thisString = thisWord.toString();
				thisBuilder.append(thisString);
				thisPointer += thisString.length();

				thisWordLocation.to = thisPointer;
			}
			thisSentenceLocation.to = thisPointer;
		}
		this.theText = thisBuilder.toString();
	}

	// L O C A T E

	/**
	 * Locate list of words from sentence
	 * 
	 * @param thisSentence
	 *            the sentence containing the words
	 * @param theseWords
	 *            list of words to locate
	 * @return Location
	 */
	public Location getLocation(final Sentence thisSentence, final List<Word> theseWords)
	{
		if (theseWords.isEmpty())
			return null;

		final Word thisFromWord = theseWords.get(0);
		final Word thisToWord = theseWords.get(theseWords.size() - 1);
		return getLocation(thisSentence, thisFromWord, thisToWord);
	}

	/**
	 * @param thisSentence
	 *            the sentence containing the words
	 * @param thisFromWord
	 *            start word to locate
	 * @param thisToWord
	 *            end word to locate
	 * @return Location
	 */
	public Location getLocation(final Sentence thisSentence, final Word thisFromWord, final Word thisToWord)
	{
		final Location thisFromLocation = this.theLocator.get(thisSentence).theWordMap.get(thisFromWord);
		final Location thisToLocation = this.theLocator.get(thisSentence).theWordMap.get(thisToWord);
		if (thisFromLocation == null || thisToLocation == null)
			return new Location(0, 0);
		return new Location(thisFromLocation.from, thisToLocation.to);
	}
}
