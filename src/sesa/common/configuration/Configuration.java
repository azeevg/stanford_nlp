package sesa.common.configuration;

public class Configuration {
    /**
     * Logging settings **
     */
    public static final String LOG_FILE = "application.log";
    public static final String LOG_LEVEL = "logger.level";

    /**
     * Logging level **
     */
    public static final String LOG_ERROR = "error";
    public static final String LOG_WARNING = "warning";
    public static final String LOG_INFO = "info";
    public static final String LOG_DEBUG = "debug";

    /**
     * Configuration file **
     */
    public static final String CFG_FILE = "config/configuration.cfg";

    /**
     * Database parameters **
     */
    public static final String DB_HOST = "database.host";
    public static final String DB_NAME = "database.name";
    public static final String DB_USER = "database.user";
    public static final String DB_PASSWORD = "database.password";

    /**
     * Database collection **
     */
    public static final String DB_COLLECTION_PUBMED = "documents_pubmed";
    public static final String DB_COLLECTION_PUBMED_2 = "documents_pubmed_2";
    public static final String DB_COLLECTION_WIKI = "documents_wiki";
    public static final String DB_COLLECTION_FILTERED_WIKI = "documents_filtered_wiki";
    public static final String DB_COLLECTION_CAT_FILTERED_WIKI = "documents_cat_wiki";
    public static final String DB_COLLECTION_BY_CAT_FILTERED_WIKI = "documents_by_cat_wiki";
    public static final String DB_COLLECTION_BY_CAT_HTML_WIKI = "w_dbch";
    public static final String DB_COLLECTION_TEXT_WIKI = "documents_text_wiki";

    public static final String DB_COLLECTION_DICTIONARY = "dictionary";

    public static final String DB_COLLECTION_CATS_DICTIONARY = "all_cats_idf";
    public static final String DB_COLLECTION_TEST_DICTIONARY = "all_test_idf";
    public static final String DB_COLLECTION_CATS_UNIQUE_DICTIONARIES = "cats_dicts";
    public static final String DB_COLLECTION_CATEGORIES_UNIQUE_DICTIONARIES = "categories_dicts";
    public static final String DB_COLLECTION_CATS_INFO = "cats_info";

    public static final String DB_COLLECTION_COMPUTER_DICTIONARY = "comp_dictionary";
    public static final String DB_COLLECTION_COMPUTER_DICTIONARY_CROSS_REF = "comp_dictionary_cross_ref";

    /**
     * Database iterator batch size **
     */
    public static final long DB_ITERATOR_BATCH_SIZE = 10000;
}
