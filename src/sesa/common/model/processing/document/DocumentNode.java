package sesa.common.model.processing.document;

import sesa.common.model.processing.sentence.SentenceForest;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a data to store the document in tree structure
 * <p/>
 * Text consists of paragraphs, to save the hierarchy of sections used tree
 */
public class DocumentNode {
    private List<SentenceForest> title;
    private List<DocumentNode> children;    /* Child nodes */
    private DocumentNode parent;
    private int level;
    private List<SentenceForest> paragraph; /* Paragraph sentences */

    public DocumentNode(List<SentenceForest> paragraph, List<SentenceForest> title) {
        this.children = new LinkedList<DocumentNode>();
        this.paragraph = paragraph;
        this.title = title;
    }

    public DocumentNode() {
        this.children = new LinkedList<DocumentNode>();
    }

    public void setTitle(List<SentenceForest> title) {
        this.title = title;
    }

    public void setParent(DocumentNode parent) {
        this.parent = parent;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<DocumentNode> getChildren() {
        return children;
    }

    public void setParagraph(List<SentenceForest> paragraph) {
        this.paragraph = paragraph;
    }

    public DocumentNode getParent() {
        return parent;
    }

    public void append(DocumentNode node) {
        children.add(node);
    }

    public List<SentenceForest> getParagraph() {
        return paragraph;
    }

    public List<SentenceForest> getTitle() {
        return title;
    }
}
