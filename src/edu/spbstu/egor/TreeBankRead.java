package edu.spbstu.egor;

import edu.stanford.nlp.trees.*;

import java.io.*;
import java.util.List;

/**
 * Created by Egor Gorbunov on 20.05.2015.
 * Email: egor-mailbox@ya.ru
 * Github username: egorbunov
 */
public class TreeBankRead {
    private static final String TREE_PATH = "./egor_in/trees/";
    private static final String[] TREE_FILENAMES = new String[]{"11597317.tree"};

    public static void printTreeBank(BufferedWriter writer, Treebank treebank) throws IOException {
        for (Tree t : treebank) {
            List<Tree> leaves = t.getLeaves();
            for (Tree leaf : leaves) {
                writer.write(leaf.value() + " ");
            }
            writer.write('\n');
        }
        writer.flush();
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        PennTreeReaderFactory readerFactory = new PennTreeReaderFactory();

        Treebank treebank = new MemoryTreebank(readerFactory);
        treebank.loadPath(TREE_PATH + TREE_FILENAMES[0], ".tree", false);
        printTreeBank(new BufferedWriter(new OutputStreamWriter(System.out)), treebank);
    }
}
