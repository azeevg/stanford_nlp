package sesa.test.common.utility;

import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.spbstu.appmath.collocmatch.Match;
import edu.spbstu.appmath.collocmatch.MatchedCollocation;
import edu.spbstu.appmath.collocmatch.Sentence;
import org.junit.Test;
import sesa.common.utility.collocation.CollocationUtility;
import sesa.common.utility.io.FileUtility;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Ira on 14.05.2016.
 */
public class CollocationUtilityTest {

    @Test
    public void testGetCollocationInSentence(){
         try {
             CollocationDictionary dictionary = new CollocationDictionary(new String[]{"./data/subsetCollocations.txt"});
             String sentence = "Surface modeling is a more complex method for representing objects than wireframe modeling, but not as sophisticated as solid modeling ";
             String text = FileUtility.readAllText("./data/collocations/textwithcollocation.txt");
             System.out.println(CollocationUtility.getCollocationInSentence(dictionary,sentence));
             System.out.println(CollocationUtility.calcNumCollocationInSentence(dictionary,sentence));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testGetCollocationsScore() {
        HashMap<String, Integer> hashMap = CollocationUtility.findSubSetCollocations("data/scoreCollocationKey.txt", "data/scoreCollocationScore.txt", 5);
        //FileUtility.writeAllLinesToFile("subsetCollocations_4.txt", hashMap.keySet());
        System.out.println(hashMap.size());
    }

    @Test
    public void testReplaceCollocation(){
        try {
            CollocationDictionary collocationDictionary = new CollocationDictionary(new String[]{"./data/collocations/allCollocations.txt"});
            Match curMatch = null;
            String COLLOCATION_SPECIAL_WORD = "_COLLOCATION_";
            String str = " An Internet domain name made up of standard ASCII characters. The Internet was created using ASCII-based domain names.";

            curMatch = collocationDictionary.searchAllCollocations(str);
            for(MatchedCollocation m: curMatch.getMatchedCollocations()){
                System.out.println(m.getCollocationStr());
            }
            Sentence sentence = curMatch.replaceCollocations(COLLOCATION_SPECIAL_WORD);
            System.out.println(sentence.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
