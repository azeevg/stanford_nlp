package grammarscope.parser;

import java.io.Serializable;
import java.util.List;

import edu.stanford.nlp.trees.Tree;

/**
 * Parsed relation (clusters parsed data).
 * Example pred(clause,vp)->clause=scope,relation=vp (a subtree of the scope that bears the given relation to it).
 *
 * @author Bernard Bou
 */
public class ParsedRelation implements Serializable {
    /**
     * Relation tree
     */
    public Tree theTree;

    /**
     * Relation head
     */
    public Tree theHead;

    /**
     * Relation head morphological base
     */
    public String theHeadBase;

    /**
     * Search scope
     */
    public Tree theScopeTree;

    /**
     * Search scope head
     */
    public Tree theScopeHead;

    /**
     * Search scope head morphological base
     */
    public String theScopeHeadBase;

    /**
     * Reference sentence
     */
    public Sentence theSentence;

    /**
     * Reference grammatical relation
     */
    public MutableGrammaticalRelation theRelation;

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RELATION=" + this.theRelation.getLongName() + " TREE=" + this.theTree.toString() + " HEAD=" + this.theHead.toString() + " BASE=" + this.theHeadBase;
    }

    public String toLongString() {
        final StringBuffer thisBuffer = new StringBuffer();
        thisBuffer.append(String.format("%-6s ( ", this.theRelation.getShortName()));
        thisBuffer.append(String.format("%-16s", this.theHead));
        thisBuffer.append("[");
        thisBuffer.append(this.theTree.label());
        thisBuffer.append("] ");
        thisBuffer.append(ParsedRelation.join(this.theTree.getLeaves()));

        thisBuffer.append(" , ");

        thisBuffer.append(this.theScopeHead);
        thisBuffer.append("[");
        thisBuffer.append(this.theScopeTree.label());
        thisBuffer.append("] ");
        thisBuffer.append(ParsedRelation.join(this.theScopeTree.getLeaves()));
        return thisBuffer.toString();
    }

    static private String join(final List<Tree> theseTrees) {
        final String thisToken = " ";
        final StringBuffer thisBuffer = new StringBuffer();
        thisBuffer.append('{');
        for (final Tree thisTree : theseTrees) {
            thisBuffer.append(thisTree.value());
            thisBuffer.append(thisToken);
        }
        thisBuffer.deleteCharAt(thisBuffer.length() - 1);
        thisBuffer.append('}');
        return thisBuffer.toString();
    }

    static public String treeTopToString(final Tree thisTree) {
        return thisTree.localTree().toString();
    }

    static public String treeLeavesToString(final Tree thisTree) {
        final StringBuffer thisBuffer = new StringBuffer();
        final List<Tree> theseLeaves = thisTree.getLeaves();
        thisBuffer.append("{");
        for (final Tree thisLeaf : theseLeaves) {
            thisBuffer.append("[");
            thisBuffer.append(thisLeaf);
            thisBuffer.append("]");
        }
        thisBuffer.append("}");
        return thisBuffer.toString();
    }
}
