package sesa.tools.filters.WIKI.text;

import java.util.Arrays;

public class KeySymbols {
    private KeySymbols[] ambiguityWithSymbols;
    private static final int SKIP_LIMIT = 30000;

    private String keyWord;
    private char[][] sSymbolsArray;
    private char[][] eSymbolsArray;
    private char[][] suffixSkipSymbols;

    private char[] sFirstSymbols;
    private char[] eFirstSymbols;

    private int priority;

    private boolean seSymbols = false;
    private boolean skipParseInside = false;

    private int tagHash;

    public KeySymbols(String keyWord, String[] sStringSymbols, String[] eStringSymbols, int priority) {
        this.sSymbolsArray = convertToCharArrays(sStringSymbols);
        this.eSymbolsArray = convertToCharArrays(eStringSymbols);
        this.seSymbols = sStringSymbols.length == 1 && eStringSymbols.length == 1 && sStringSymbols[0].equals(eStringSymbols[0]);
        this.sFirstSymbols = getFirstSymbols(sSymbolsArray);
        this.eFirstSymbols = getFirstSymbols(eSymbolsArray);
        this.tagHash = (Arrays.deepToString(sSymbolsArray) + Arrays.deepToString(eSymbolsArray)).hashCode();
        this.keyWord = keyWord;
        this.priority = priority;
    }

    public KeySymbols(String keyWord, String sStringSymbols, String eStringSymbols, int priority) {
        this(keyWord, new String[]{sStringSymbols}, new String[]{eStringSymbols}, priority);
    }


    public KeySymbols(String keyWord, String sStringSymbols, String[] eStringSymbols, int priority, boolean skipParseInside) {
        this(keyWord, new String[]{sStringSymbols}, eStringSymbols, priority);
        this.skipParseInside = skipParseInside;
    }

    public KeySymbols(String keyWord, String sStringSymbols, String eStringSymbols, int priority, boolean skipParseInside) {
        this(keyWord, new String[]{sStringSymbols}, new String[]{eStringSymbols}, priority);
        this.skipParseInside = skipParseInside;
    }

    public KeySymbols(String keyWord, String[] sStringSymbols, String[] eStringSymbols, String[] suffixSkipSymbols, int priority, boolean skipParseInside) {
        this(keyWord, sStringSymbols, eStringSymbols, priority);
        this.skipParseInside = skipParseInside;
        this.suffixSkipSymbols = convertToCharArrays(suffixSkipSymbols);
    }

    public void setAmbiguityWithSymbols(KeySymbols[] ambiguityWithSymbols) {
        this.ambiguityWithSymbols = ambiguityWithSymbols;
    }

    public char[][] getEndSymbolsArray() {
        return eSymbolsArray;
    }

    public boolean isParseInsideSkipped() {
        return skipParseInside;
    }

    private char[] getFirstSymbols(char[][] sSymbolsArray) {
        char[] firstSymbols = new char[sSymbolsArray.length];
        for (int i = 0; i < sSymbolsArray.length; i++)
            firstSymbols[i] = sSymbolsArray[i][0];

        return firstSymbols;
    }

    public KeyEntry findSkipUntilPosition(char[] chs, int pos) throws Exception {
        KeyEntry endEntry = new KeyEntry();
        int posCount = 0, nestCounter = 0;
        boolean found = false;
        char[] symbols;

        while (!found) {
            if (posCount > SKIP_LIMIT)
                throw new Exception("Skip limit exceed");

            if (!isKeySymbol(chs[pos + posCount])) {
                posCount++;
                continue;
            }

            if ((symbols = matchSequenceArray(chs, eSymbolsArray, pos + posCount)) != null) {
                if (nestCounter > 0)
                    nestCounter--;
                else {
                    found = true;
                    endEntry.setSymbols(symbols);
                }
            } else if ((symbols = matchSequenceArray(chs, sSymbolsArray, pos + posCount)) != null)
                nestCounter++;

            posCount++;
        }

        endEntry.setPosition(pos + posCount);
        return endEntry;
    }


    private char[][] convertToCharArrays(String[] symbols) {
        char[][] arrays = new char[symbols.length][];
        for (int i = 0; i < symbols.length; i++)
            arrays[i] = symbols[i].toCharArray();

        return arrays;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public boolean isKeySymbol(char ch) {
        for (char sFirstSym : sFirstSymbols)
            if (sFirstSym == ch)
                return true;

        for (char eFirstSym : eFirstSymbols)
            if (eFirstSym == ch)
                return true;

        return false;
    }

    public KeyEntry getKeyEntry(char[] characters, int pos) {
        KeyEntry entry = new KeyEntry();
        entry.setType(KeyType.NONE_TYPE);
        char[] symbols;

        if ((symbols = matchSequenceArray(characters, sSymbolsArray, pos)) != null) {
            KeyEntry ambiguity;
            if ((ambiguity = getAmbiguity(characters, sSymbolsArray, pos)) != null)
                entry.setAmbiguityEntry(ambiguity);

            entry.setType(seSymbols ? KeyType.BOTH_TYPE : KeyType.START_TYPE);
            entry.setSymbols(symbols);
            entry.setKeySymbols(this);
        } else if ((symbols = matchSequenceArray(characters, eSymbolsArray, pos)) != null) {
            entry.setType(seSymbols ? KeyType.BOTH_TYPE : KeyType.END_TYPE);
            entry.setSymbols(symbols);
            entry.setKeySymbols(this);
        }

        return entry;
    }

    public int getTagHash() {
        return tagHash;
    }

    public int getPriority() {
        return priority;
    }

    private KeyEntry getAmbiguity(char[] chs, char[][] matchChsArray, int pos) {
        if (ambiguityWithSymbols == null)
            return null;

        for (KeySymbols keySymbols : ambiguityWithSymbols) {
            char[][] ssa = keySymbols.sSymbolsArray;
            for (char[] aSsa : ssa)
                for (char[] aMatchChsArray : matchChsArray)
                    if (matchSequence(chs, concatenateArrays(aMatchChsArray, aSsa), pos)) {
                        KeyEntry ambiguity = new KeyEntry();
                        ambiguity.setKeySymbols(keySymbols);
                        ambiguity.setSymbols(aSsa);
                        return ambiguity;
                    }
        }

        return null;
    }

    private char[] matchSequenceArray(char[] chs, char[][] matchChsArray, int pos) {
        for (char[] aMatchChsArray : matchChsArray)
            if (matchSequence(chs, aMatchChsArray, pos) && !hasSuffix(chs, aMatchChsArray, pos))
                return aMatchChsArray;

        return null;
    }

    private boolean hasSuffix(char[] chs, char[] matchChs, int pos) {
        if (suffixSkipSymbols == null)
            return false;

        for (char[] suffixSkipSym : suffixSkipSymbols)
            if (matchSequence(chs, concatenateArrays(matchChs, suffixSkipSym), pos))
                return true;

        return false;
    }

    private char[] concatenateArrays(char[] a, char[] b) {
        char[] res = new char[a.length + b.length];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        return res;
    }

    private boolean matchSequence(char[] chs, char[] matchChs, int pos) {

        if (pos + matchChs.length > chs.length)
            return false;

        for (int i = 0; i < matchChs.length; i++, pos++) {
            if (matchChs[i] != chs[pos])
                return false;
        }
        return true;
    }
}
