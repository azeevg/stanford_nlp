package sesa.common.utility.io;

import sesa.common.interfaces.IProcessor;
import sesa.common.utility.logging.LoggingUtility;
import sesa.common.utility.logging.ReportUtility;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Class for working with directory
 */
public class DirectoryUtility extends LoggingUtility {


    private static List<File> allFilesRecursive(List<File> files, File dir) {
        if (files == null)
            files = new LinkedList<File>();

        if (!dir.isDirectory()) {
            files.add(dir);
            return files;
        }

        for (File file : dir.listFiles())
            allFilesRecursive(files, file);
        return files;
    }

    /**
     * Get all files in directories
     * @return
     */
    public static File[] getAllFilesDirectories(String dirPath){
        File f = new File(dirPath);
        File[] files = f.listFiles(); // SecurityException

        return files;
    }
    /**
     * Get all files in directories
     * @return
     */
    public static List<String> getAllFilesInDirectories(String dirPath){
        File f = new File(dirPath);
        File[] files = f.listFiles(); // SecurityException
        List<String> filesList = new ArrayList<String>();

        for(int i = 0; i < files.length; i++){
            if(files[i].isFile()){
                filesList.add(files[i].toString());
                //System.out.println(files[i]);
            }

        }

        return filesList;
    }

    /**
     * Добавить к имеющемуся списку файлов файл из директории dir
     * @param files
     * @param dir
     * @return
     */
    private static List<File> allFiles(List<File> files, File dir) {
        if (files == null)
            files = new LinkedList<File>();

      //  dir.isFile() - тоже самое
        if (!dir.isDirectory()) {
            files.add(dir);
            return files;
        }

        Collections.addAll(files, dir.listFiles());
        return files;
    }

    /**
     * Получить размер директории
     * @param path
     * @return
     */
    public static long getDirectorySize(String path) {
        List<File> allFiles = allFilesRecursive(new LinkedList<File>(), new File(path));

        long size = 0;
        for (File file : allFiles)
            size += file.length();
        return size;
    }

    /**
     * List of directories
     * @param path
     * @return
     */
    public static List<String> listDirectory(String path) {
        List<File> allFiles = allFilesRecursive(new LinkedList<File>(), new File(path));

        List<String> files = new LinkedList<String>();
        for (File file : allFiles)
            files.add(file.getAbsolutePath());

        return files;
    }

    public static List<String> listDirectory(String path, boolean recursive) {
        if (recursive)
            return listDirectory(path);

        List<File> allFiles = allFiles(new LinkedList<File>(), new File(path));
        List<String> files = new LinkedList<String>();
        for (File file : allFiles)
            files.add(file.getAbsolutePath());

        return files;
    }

    /**
     * Some process with all lines in all directories in path
     * @param path
     * @param processor
     */
    public static void processDirectory(String path, IProcessor processor) {
        List<String> files = listDirectory(path);

        ReportUtility reporter = new ReportUtility("Directory processing", getDirectorySize(path));
        for (String file : files) {
            try {
                FileReader fReader = new FileReader(file);
                BufferedReader bReader = new BufferedReader(fReader);

                String line = bReader.readLine();
                while (line != null) {
                    processor.process(line); //
                    reporter.addProcessed(line.length());
                    reporter.report();

                    line = bReader.readLine();
                }

                bReader.close();
                fReader.close();
            } catch (FileNotFoundException ignored) {

            } catch (IOException | SQLException | ClassNotFoundException ignored) {
                error(ignored.getMessage());
            }
        }
    }
}
