package sesa.common.database.dao;

import grammarscope.parser.MutableGrammaticalRelation;
import grammarscope.parser.ParsedRelation;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.tools.statistics.RawSemanticGroup;
import sesa.tools.statistics.RawSemanticRelation;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Ira on 19.02.2016.
 */

/**
 * Class for work with table which contains semantic groups
 */
public class SemanticGroupDao extends CommonDao {

    public final Logger log = LogManager.getLogger(SemanticGroupDao.class);

    public SemanticGroupDao() throws SQLException, ClassNotFoundException {
        super();
    }

    private class RelationComparator implements Comparator<RawSemanticRelation> {
        public int compare(RawSemanticRelation o1, RawSemanticRelation o2) {
            return o1.getIndex() - o2.getIndex();
        }
    }

    /**
     * Extract semantic groups from table
     *
     * @param collectionName
     * @return
     */
    public Collection<RawSemanticGroup> getSemanticGroups(String collectionName) {

        Map<String, RawSemanticGroup> groups = new HashMap<String, RawSemanticGroup>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        RelationComparator relationComparator = new RelationComparator();
        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "`");
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                String token = resultSet.getString("token");
                String base = resultSet.getString("base");
                String role = resultSet.getString("role");
                String pos = resultSet.getString("pos");
                int index = resultSet.getInt("sentence_index");
                String groupId = resultSet.getString("group_id");
                int sentenceId = resultSet.getInt("sentence_id");
                boolean hasService = resultSet.getInt("sentence_service") == 1;
                boolean prevDelimiter = resultSet.getInt("prev_delimiter") == 1;
                int sentenceSize = resultSet.getInt("sentence_size");


                if (!groups.containsKey(groupId)) {
                    RawSemanticGroup rsg = new RawSemanticGroup(relationComparator);
                    //rsg.setId(groupId);
                    //rsg.setSentenceId(sentenceId);
                    rsg.setSentenceHasServiceTokens(hasService);
                    rsg.setSentenceSize(sentenceSize);
                    groups.put(groupId, rsg);
                }

                RawSemanticGroup rsg = groups.get(groupId);
                RawSemanticRelation rsr = new RawSemanticRelation();
                rsr.setTokenBaseRolePos(token + "|" + base + "|" + role + "|" + pos);
        rsr.setToken(token);
        /*rsr.setTokenBase(base);
        rsr.setRole(role);
        rsr.setPos(pos);*/
                rsr.setIndex(index);
                rsr.setPrevDelimiter(prevDelimiter);
                rsg.addRelation(rsr);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }


        return groups.values();
    }

    /**
     * Adding the semantic group to table
     *
     * @param collectionName  table name
     * @param token
     * @param tokenBase       - base form ( infinitive)
     * @param role            -
     * @param pos             - position in the sentence
     * @param index
     * @param groupId
     * @param sentenceId      id sentence (identifier)
     * @param sentenceService
     * @param prevDelimiter
     * @param sentenceSize
     */
    public void saveSemanticRelation(String collectionName, String token, String tokenBase, String role, String pos, int index, String groupId, int sentenceId, int sentenceService, int prevDelimiter, int sentenceSize) {

        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            String query = "INSERT INTO `" + collectionName + "` (token, base, role, pos, sentence_index, group_id, sentence_id, sentence_service, prev_delimiter, sentence_size) VALUES(?,?,?,?,?,?,?,?,?,?)";
            //  System.out.println(query);
            ps = connection.prepareStatement(query);

            ps.setString(1, token);
            ps.setString(2, tokenBase);
            ps.setString(3, role);
            ps.setString(4, pos);
            ps.setInt(5, index);
            ps.setString(6, groupId);
            ps.setInt(7, sentenceId);
            ps.setInt(8, sentenceService);
            ps.setInt(9, prevDelimiter);
            ps.setInt(10, sentenceSize);

            ps.execute();

            //  return true;
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            //  debug("Can't insert record: " + token);
            //   debug("Exception: " + e.getMessage());
            //  LoggingUtility.info("SQL Exception: " + e.getMessage());
        } finally {
            cp.closeConnection(connection, ps);
        }


    }

    /**
     * public abstract void saveSemanticRelation
     * (String collectionName, String token, String tokenBase, String role, int position, int groupHash, int groupId);
     */

    public HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> getCachedRelations(String collectionName, String title) {
        HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>> relations = new HashMap<Integer, Map<MutableGrammaticalRelation, List<ParsedRelation>>>();

        InputStream result = null;
        PreparedStatement s = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = cp.takeConnection();
            s = connection.prepareStatement("SELECT * FROM `" + collectionName + "` WHERE title = ?");
            s.setString(1, title);
            s.execute();

            resultSet = s.getResultSet();
            while (resultSet.next()) {
                result = resultSet.getBinaryStream("data");
                String hash = resultSet.getString("hash");
                ObjectInputStream ois = new ObjectInputStream(result);
                Map<MutableGrammaticalRelation, List<ParsedRelation>> tree = (Map<MutableGrammaticalRelation, List<ParsedRelation>>) ois.readObject();
                result.close();
                ois.close();
                relations.put(Integer.parseInt(hash), tree);
            }

        } catch (SQLException e) {
            log.error("Exception: " + e.getMessage());
            log.debug("Can't select data from table:" + collectionName);
            //   debug("Can't select data from table:" + collectionName);
            //   debug("Exception: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
            //e.printStackTrace();
        } catch (IOException e) {
            log.error(e.getMessage());
            // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            cp.closeConnection(connection, s, resultSet);
        }


        return relations;  //To change body of implemented methods use File | Settings | File Templates.
    }


    //Ira Daniel
    // delete, generate that method only for test
    public Collection<RawSemanticGroup> getSemanticGroupsWithCollocation(String collectionName) {

        Map<String, RawSemanticGroup> groups = new HashMap<String, RawSemanticGroup>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;
        RelationComparator relationComparator = new RelationComparator();
        try {
            connection = cp.takeConnection();
            //SELECT COUNT(*) FROM `semantic_groups_computer_dict` WHERE `pos`='COLLOCATION'
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "`");
            ps.execute();
            resultSet = ps.getResultSet();

            while (resultSet.next()) {
                String token = resultSet.getString("token");
                String base = resultSet.getString("base");
                String role = resultSet.getString("role");
                String pos = resultSet.getString("pos");
                int index = resultSet.getInt("sentence_index");
                String groupId = resultSet.getString("group_id");
                int sentenceId = resultSet.getInt("sentence_id");
                boolean hasService = resultSet.getInt("sentence_service") == 1;
                boolean prevDelimiter = resultSet.getInt("prev_delimiter") == 1;
                int sentenceSize = resultSet.getInt("sentence_size");


                if (!groups.containsKey(groupId)) {
                    RawSemanticGroup rsg = new RawSemanticGroup(relationComparator);
                    rsg.setId(groupId);
                    rsg.setSentenceId(sentenceId);
                    rsg.setSentenceHasServiceTokens(hasService);
                    rsg.setSentenceSize(sentenceSize);
                    groups.put(groupId, rsg);
                }

                RawSemanticGroup rsg = groups.get(groupId);
                RawSemanticRelation rsr = new RawSemanticRelation();
                // rsg.setId(groupId);
                rsr.setTokenBaseRolePos(token + "|" + base + "|" + role + "|" + pos);
                rsr.setToken(token);
                rsr.setPos(pos);
        /*rsr.setToken(token);
        rsr.setTokenBase(base);
        rsr.setRole(role);
        rsr.setPos(pos);*/
                rsr.setIndex(index);
                rsr.setPrevDelimiter(prevDelimiter);
                rsg.addRelation(rsr);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());

        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }


        return groups.values();
    }
}
