package sesa.common.model.parsing;

import sesa.common.database.entity.IDBEntity;
import sesa.common.utility.hash.HashUtility;
import sesa.common.utility.json.JSONUtility;

import java.util.Map;

/**
 * Parsing document in jSon format
 */
public class JSONDocument implements IDBEntity {
    private Map<String, Object> data;

    public JSONDocument(String line) {
        data = JSONUtility.fromString(line);
    }

    public String getID() {
        return HashUtility.generateHash((String) data.get("title"));
    }

    public Map<String, Object> toMap() {
        return data;
    }

    @Override
    protected void finalize() throws Throwable {
        if (data != null)
            data.clear();

        super.finalize();
    }
}
