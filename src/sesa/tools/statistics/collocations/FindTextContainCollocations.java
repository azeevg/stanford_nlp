package sesa.tools.statistics.collocations;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.utility.FileReaderWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ira on 06.05.2016.
 */
public class FindTextContainCollocations {
    public static final Logger log = LogManager.getLogger(FindTextContainCollocations.class);
    private List<String> collocations;
    private Map<String, String> mapText;

    public FindTextContainCollocations() {
        collocations = FileReaderWriter.read("./data/scoreCollocationKey.txt");
    }

    public void initMap(Map<String, String> mapText){
        this.mapText = mapText;
    }

    public HashMap<String,List<String>> findTextName(){
        HashMap<String,List<String>> textMap = new HashMap<String,List<String>>();
        for(String collocation:collocations){
            log.info("Find score for collocation:" + collocation);
            textMap.put(collocation,findTextWhenContainsTerm(collocation) );
        }
        return textMap;
    }

    public List<String> findTextWhenContainsTerm(String term){
        List<String> list = new ArrayList<String>();
        for(String key: mapText.keySet()){
            if(isContains(term,mapText.get(key))){
                list.add(key);
            }
        }
        return list;
    }

    public boolean isContains(String term, String text){
        if(text.contains(term)){
            return true;
        }
        return false;
    }
}
