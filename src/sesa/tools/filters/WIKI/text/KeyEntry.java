package sesa.tools.filters.WIKI.text;

public class KeyEntry {
    private char[] symbols;
    private KeyType type;
    private KeySymbols keySymbols;
    private KeyEntry ambiguityEntry;

    public char[] getSymbols() {
        return symbols;
    }

    public void setSymbols(char[] symbols) {
        this.symbols = symbols;
    }

    public KeyType getType() {
        return type;
    }

    public void setType(KeyType type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private int position;

    public KeySymbols getKeySymbols() {
        return keySymbols;
    }

    public void setKeySymbols(KeySymbols keySymbols) {
        this.keySymbols = keySymbols;
    }

    public KeyEntry getAmbiguityEntry() {
        return ambiguityEntry;
    }

    public void setAmbiguityEntry(KeyEntry ambiguityEntry) {
        this.ambiguityEntry = ambiguityEntry;
    }

}
