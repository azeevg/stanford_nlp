package sesa.common.builder.semanticRule;

import java.util.HashMap;

/**
 * Created by Ira on 24.02.2016.
 */


/**
 * Class contains a subset of the grammatical relations of the original sets of Stanford
 */

public class AliasManager {


    public static HashMap<String, String> relationToRole = new HashMap<String, String>();

    public static HashMap<String, String[]> roleToRelation = new HashMap<String, String[]>();

    static {
        relationToRole.put("pred", "pred");
        relationToRole.put("aux", "pred");
        relationToRole.put("auxpass", "pred");
        relationToRole.put("cop", "pred");

        relationToRole.put("subj", "subj");
        relationToRole.put("nsubj", "subj");
        relationToRole.put("nsubjpass", "subj");
        relationToRole.put("csubj", "subj");
        relationToRole.put("csubjpass", "subj");
        relationToRole.put("xsubj", "subj");


        relationToRole.put("conj", "obj");
        relationToRole.put("prep", "obj");
        relationToRole.put("partmod", "obj");
        relationToRole.put("obj", "obj");
        relationToRole.put("iobj", "obj");
        relationToRole.put("dobj", "obj");
        relationToRole.put("pobj", "obj");

        roleToRelation.put("pred", new String[]{"pred", "aux", "auxpass", "cop"});
        roleToRelation.put("subj", new String[]{"subj", "nsubj", "nsubjpass", "csubj", "csubjpass", "xsubj"});
        roleToRelation.put("obj", new String[]{"conj", "prep", "partmod", "obj", "iobj", "dobj", "pobj"});
    }


      /*#grammarscope

      # object relations

    #removed with 6th rule
    #brought back with 7 rule
    display-conj=true


    display-prep=true
    display-partmod=true

    display-obj=true
    display-iobj=true
    display-dobj=true
    display-pobj=true

    #Sun Jul 13 10:31:09 CEST 2008



    # predicate relations
    display-pred=true
    display-aux=true
    display-auxpass=true
    display-cop=true


    # subject relations
    display-subj=true
    display-nsubj=true
    display-nsubjpass=true
    display-csubj=true
    display-csubjpass=true
    display-xsubj=true





    */
}