package sesa.tools.semanticWeb;

import sesa.common.utility.logging.LoggingUtility;

import java.util.*;

/**
 * TODO: Where we use this class ?
 */
public class Graph extends LoggingUtility {
    private Map<String, Vertex> vertices = new HashMap<String, Vertex>();
    private Map<String, List<Edge>> edges = new HashMap<String, List<Edge>>();
    private Set<Edge> edgesSet = new HashSet<Edge>();

    private boolean oriented;

    public Graph(boolean oriented) {
        this.oriented = oriented;
    }

    private synchronized boolean hasVertex(String vertexID) {
        return vertices.containsKey(vertexID);
    }

    public synchronized void addVertex(Vertex vertex) {
        if (!hasVertex(vertex.getName())) {
            edges.put(vertex.getName(), new ArrayList<Edge>());
            vertices.put(vertex.getName(), vertex);
        }
    }

    private synchronized boolean hasEdge(Edge e) {
        return edgesSet.contains(e);
    }

    private synchronized Edge inverseEdge(Edge e) {
        return new Edge(e.getDestinationVertexID(), e.getSourceVertexID(), e.getData(), e.getWeight());
    }

    private synchronized boolean addOrientedEdge(Edge e) {
        if (!hasVertex(e.getSourceVertexID()))
            return false;

        List<Edge> currentSiblings = edges.get(e.getSourceVertexID());
        currentSiblings.add(e);
        Collections.sort(currentSiblings);
        edgesSet.add(e);

        return true;
    }

    public synchronized boolean addEdge(Edge e) {
        return hasEdge(e) || addOrientedEdge(e) && (oriented || addOrientedEdge(inverseEdge(e)));
    }

    /**
     * ����� � ������ ���� �����
     * @param vertexID
     * @return
     */
    private synchronized String printEdges(String vertexID) {
        StringBuilder sb = new StringBuilder();

        List<Edge> currentSiblings = edges.get(vertexID);
        for (Edge edge : currentSiblings)
            sb.append(edge.getDestinationVertexID()).append("; ");

        return sb.toString();
    }

    public synchronized void print() {
        Set<String> vertexIDs = edges.keySet();
        for (String vertexID : vertexIDs)
            info(vertexID + " -> " + printEdges(vertexID));
    }

    private synchronized String minVertex(Map<String, Double> distances, Map<String, Boolean> visited) {
        String minVertex = null;
        double distance = Double.MAX_VALUE;

        Set<String> vertexIDs = vertices.keySet();
        for (String vertexID : vertexIDs)
            if (!visited.get(vertexID) && distances.get(vertexID) < distance) {
                minVertex = vertexID;
                distance = distances.get(vertexID);
            }

        return minVertex;
    }

    /**
     * �������� ���� ����������������
     * @param sourceVertexID
     * @return
     */
    private synchronized Map<String, String> getPredecessors(String sourceVertexID) {
        Map<String, Double> distances = new HashMap<String, Double>();
        Map<String, String> predecessors = new HashMap<String, String>();
        Map<String, Boolean> visited = new HashMap<String, Boolean>();

        /* Initialize structures */
        Set<String> vertexIDs = vertices.keySet();
        for (String vertexID : vertexIDs) {
            distances.put(vertexID, Double.MAX_VALUE);
            predecessors.put(vertexID, null);
            visited.put(vertexID, false);
        }

        /* Distance to source vertex is known */
        distances.put(sourceVertexID, 0.0);

        /* For each vertex */
        for (int i = 0; i < vertices.size(); i++) {
            final String next = minVertex(distances, visited);

            /* We can't select any vertex */
            if (next == null)
                break;

            /* Mark selected vertex as visited */
            visited.put(next, true);

            List<Edge> neighbors = edges.get(next);
            for (Edge edge : neighbors) {
                final double distance = distances.get(next) + edge.getWeight();
                if (distances.get(edge.getDestinationVertexID()) > distance) {
                    distances.put(edge.getDestinationVertexID(), distance);
                    predecessors.put(edge.getDestinationVertexID(), next);
                }
            }
        }

        return predecessors;
    }

    public synchronized Edge getEdge(String sourceVertexID, String destinationVertexID) {
        List<Edge> currentSiblings = edges.get(sourceVertexID);

        Edge selectedEdge = null;
        double distance = Double.MAX_VALUE;
        for (Edge edge : currentSiblings)
            if ((edge.getWeight() < distance) && (edge.getDestinationVertexID().equals(destinationVertexID))) {
                distance = edge.getWeight();
                selectedEdge = edge;
            }

        return selectedEdge;
    }

    public List<String> getPath(String sourceVertexID, String destinationVertexID) {
        List<String> path = new LinkedList<String>();
        Map<String, String> predecessors = getPredecessors(sourceVertexID);

        String vertex = destinationVertexID;
        while (!vertex.equals(sourceVertexID)) {
            path.add(vertex);
            vertex = predecessors.get(vertex);
        }

        path.add(sourceVertexID);
        Collections.reverse(path);
        return path;
    }
}
