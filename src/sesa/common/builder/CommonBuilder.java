package sesa.common.builder;

/**
 * Created by Ira on 20.02.2016.
 */

import org.jsoup.Jsoup;
import sesa.common.model.processing.document.CategorizedDocument;
import sesa.common.model.processing.document.Document;
import sesa.common.model.processing.document.DocumentNode;
import sesa.common.model.processing.document.IDocument;

import java.util.Set;

/**
 * �����, � ������� ���������� DictionaryDocumentBuilder.buildDocument
 * DocumentBuilder.buildDocumentTree
 * TODO: Change class DictionaryDocumentBuilder
 */
@Deprecated
public class CommonBuilder extends BasicBuilder {

    public IDocument buildDocument(String id, String text, Set<String> categoryDictionary) throws BuilderException {

        org.jsoup.nodes.Document document = Jsoup.parse(text);
        DocumentNode root = buildDocumentTree(document);

        if (categoryDictionary == null)
            return new Document(id, title, root);
        else
            return new CategorizedDocument(id, title, root, categoryDictionary);
    }

    /**
     *    public static IDocument buildDocument(String id, String text, Set<String> categoryDictionary) {

     org.jsoup.nodes.Document document = Jsoup.parse(text);
     String title = document.title();

     DocumentNode root = new DocumentNode();
     DocumentNode currentNode = root;
     DocumentNode parentNode = root;

     int currentLevel = 1;
     root.setLevel(currentLevel);
     root.setTitle(buildParagraph(title));

     for (Node child : document.body().childNodes()) {

     if (child instanceof TextNode) {
     String paragraph = ((TextNode) child).text();
     currentNode.setParagraph(buildParagraph(paragraph));

     } else if (child instanceof Element) {
     Element header = (Element) child;
     String tagName = header.tagName();
     if (isHeaderElement(tagName)) {
     int headerLevel = getHeaderLevel(tagName);
     DocumentNode node = new DocumentNode();
     node.setLevel(headerLevel);
     node.setTitle(buildParagraph(header.text()));


     if (headerLevel == currentLevel)
     parentNode.append(node);
     else if (headerLevel > currentLevel) {
     parentNode = currentNode;
     parentNode.append(node);
     } else {
     parentNode = currentNode.getParent();
     parentNode.append(node);
     }

     node.setParent(parentNode);
     currentNode = node;
     currentLevel = headerLevel;
     }
     }
     }

     if (categoryDictionary == null)
     return new Document(id, title, root);
     else
     return new CategorizedDocument(id, title, root, categoryDictionary);
     }


    private static int getHeaderLevel(String headerTag) {
        return new Integer(headerTag.replaceAll("[^\\d]", ""));
    }

    // Should be removed on previous filtering steps
    private static boolean isHeaderElement(String headerTag) {
        return headerTag.matches("h[1-9]+");
    }

    //
    private static List<SentenceForest> buildParagraph(String text) {
        List<SentenceForest> paragraph = new LinkedList<SentenceForest>();
        List<List<String>> validTokens = DictionaryUtils.getValidLowerTokens(text);
        //List<List<String>> sent = DictionaryUtils.getSentences(text);
        //List<SentenceTriplet> triplets = DocumentUtils.getSentenceTriplets();

        for (List<String> sentenceTokens : validTokens) {
            SentenceForest forest = new SentenceForest();
            SentenceTree tree = new SentenceTree();

            for (String token : sentenceTokens) {
                tree.addToken(new SentenceToken(token, null));
            }
            forest.addTree(tree);
            paragraph.add(forest);
        }
        return paragraph;
    }
     */


}
