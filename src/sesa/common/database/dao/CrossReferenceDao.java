package sesa.common.database.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Ira on 03.05.2016.
 */

/**
 * Use this class to add and extract REF from
 */
public class CrossReferenceDao extends CommonDao {
    public final Logger log = LogManager.getLogger(CrossReferenceDao.class);
   // private String tableName = Configuration.DB_COLLECTION_COMPUTER_DICTIONARY_CROSS_REF;
    public CrossReferenceDao() throws SQLException, ClassNotFoundException {
        super();
    }

    /**
     * Add to reference collection table
     * @param map
     * @return
     */
    public boolean addToRefCollections(Map<String, List<String>> map,String collectionName) {
        PreparedStatement ps = null;
        Connection connection = null;

        boolean isInserted = false;
        try {
            connection = cp.takeConnection();
            for (String key : map.keySet()) {

                List<String> links = map.get(key);
                for (String link : links) {
                    System.out.println("HEAD_WORDS ,REF  - "+ key + "___" + link);
                    String query = "INSERT INTO `" + collectionName + "` (HEAD_WORDS ,REF) VALUES(?,?)";
                    ps = connection.prepareStatement(query);

                    ps.setString(1, key);
                    ps.setString(2, link);
                    ps.execute();
                }
            }
            isInserted = true;
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            cp.closeConnection(connection, ps);
        }
        return isInserted;
    }

    public List<String> extractReferencesByTerm(String term,String collectionName) {
        List<String> resultList = new ArrayList<String>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = cp.takeConnection();
            ps = connection.prepareStatement("SELECT * FROM `" + collectionName + "`WHERE `HEAD_WORDS` = ?");

            ps.setString(1, term);
            ps.execute();
            resultSet = ps.getResultSet();
            System.out.println("Extract result");
            while (resultSet.next()){
                resultList.add(resultSet.getString("REF"));
            }
        } catch (SQLException e) {
            log.debug("Can't select data from table:" + collectionName);
            log.error(e.getMessage(), e);
            resultList = null;
        } finally {
            cp.closeConnection(connection, ps, resultSet);
        }
        return resultList;
    }
}
