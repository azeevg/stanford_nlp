// DO NOT MODIFY
// DOCLET GENERATED FROM STANFORD PARSER
// EnglishGrammaticalRelations
// Thu Mar 29 07:40:47 CEST 2012

package grammarscope.parser;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class DefaultMutableGrammaticalRelations {

    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface GrammaticalRelationAnnotation {
        String name();

        String description() default "N/A";

        String[] codedexamples() default "N/A";
    }

    public static RelationModel makeDefaultModel() {
        @GrammaticalRelationAnnotation(
                name = "GOVERNOR",
                description = "The \"governor\" grammatical relation, which is the inverse of \"dependent\"",
                codedexamples = {"the red car", "gov(red, car)"})
        MutableGrammaticalRelation GOVERNOR = new MutableGrammaticalRelation("GOVERNOR", "gov", "governor",
                null,
                null,
                new String[]{},
                "The \"governor\" grammatical relation, which is the inverse of \"dependent\"",
                new String[][]{{"the red car", "gov(red, car)"}});


        @GrammaticalRelationAnnotation(
                name = "DEPENDENT",
                description = "The \"dependent\" grammatical relation, which is the inverse of \"governor\"",
                codedexamples = {"the red car", "dep(car, red)"})
        MutableGrammaticalRelation DEPENDENT = new MutableGrammaticalRelation("DEPENDENT", "dep", "dependent",
                null,
                null,
                new String[]{},
                "The \"dependent\" grammatical relation, which is the inverse of \"governor\"",
                new String[][]{{"the red car", "dep(car, red)"}});


        @GrammaticalRelationAnnotation(
                name = "ROOT",
                description = "The \"root\" grammatical relation between a faked \"ROOT\" node, and the root of the sentence.",
                codedexamples = {"", ""})
        MutableGrammaticalRelation ROOT = new MutableGrammaticalRelation("ROOT", "root", "root",
                null,
                null,
                new String[]{},
                "The \"root\" grammatical relation between a faked \"ROOT\" node, and the root of the sentence.",
                new String[][]{{"", ""}});


        @GrammaticalRelationAnnotation(
                name = "KILL",
                description = "Dummy relation, used while collapsing relations, in English &amp; Chinese GrammaticalStructure",
                codedexamples = {"", ""})
        MutableGrammaticalRelation KILL = new MutableGrammaticalRelation("KILL", "KILL", "dummy relation kill",
                null,
                null,
                new String[]{},
                "Dummy relation, used while collapsing relations, in English &amp; Chinese GrammaticalStructure",
                new String[][]{{"", ""}});


        @GrammaticalRelationAnnotation(
                name = "PREDICATE",
                description = "The \"predicate\" grammatical relation. The predicate of a clause is the main VP of that clause; the predicate of a subject is the predicate of the clause to which the subject belongs.",
                codedexamples = {"Reagan died", "pred(Reagan, died)"})
        MutableGrammaticalRelation PREDICATE = new MutableGrammaticalRelation("PREDICATE", "pred", "predicate",
                DEPENDENT,
                "S|SINV",
                new String[]{"S|SINV <# VP=target"},
                "The \"predicate\" grammatical relation. The predicate of a clause is the main VP of that clause; the predicate of a subject is the predicate of the clause to which the subject belongs.",
                new String[][]{{"Reagan died", "pred(Reagan, died)"}});


        @GrammaticalRelationAnnotation(
                name = "AUX_MODIFIER",
                description = "The \"auxiliary\" grammatical relation. An auxiliary of a clause is a non-main verb of the clause.",
                codedexamples = {"Reagan has died", "aux(died, has)"})
        MutableGrammaticalRelation AUX_MODIFIER = new MutableGrammaticalRelation("AUX_MODIFIER", "aux", "auxiliary",
                DEPENDENT,
                "VP|SQ|SINV|CONJP",
                new String[]{"VP < VP < /^(?:TO|MD|VB.*|AUXG?|POS)$/=target", "SQ|SINV < (/^(?:VB|MD|AUX)/=target $++ /^(?:VP|ADJP)/)", "CONJP < TO=target < VB", "SINV < (VP=target < (/^(?:VB|AUX|POS)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai)$/) $-- (VP < VBG))"},
                "The \"auxiliary\" grammatical relation. An auxiliary of a clause is a non-main verb of the clause.",
                new String[][]{{"Reagan has died", "aux(died, has)"}});


        @GrammaticalRelationAnnotation(
                name = "AUX_PASSIVE_MODIFIER",
                description = "The \"passive auxiliary\" grammatical relation. A passive auxiliary of a clause is a non-main verb of the clause which contains the passive information.",
                codedexamples = {"Kennedy has been killed", "auxpass(killed, been)"})
        MutableGrammaticalRelation AUX_PASSIVE_MODIFIER = new MutableGrammaticalRelation("AUX_PASSIVE_MODIFIER", "auxpass", "passive auxiliary",
                AUX_MODIFIER,
                "VP|SQ|SINV",
                new String[]{"VP < (/^(?:VB|AUX|POS)/=target < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|become|becomes|became|becoming|get|got|getting|gets|gotten|remains|remained|remain)$/ ) < (VP|ADJP [ < VBN|VBD | < (VP|ADJP < VBN|VBD) < CC ] )", "SQ|SINV < (/^(?:VB|AUX|POS)/=target < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai)$/ $++ (VP < /^VB[DN]$/))", "SINV < (VP=target < (/^(?:VB|AUX|POS)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai)$/) $-- (VP < /^VB[DN]$/))", "SINV < (VP=target < (VP < (/^(?:VB|AUX|POS)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai)$/)) $-- (VP < /^VB[DN]$/))"},
                "The \"passive auxiliary\" grammatical relation. A passive auxiliary of a clause is a non-main verb of the clause which contains the passive information.",
                new String[][]{{"Kennedy has been killed", "auxpass(killed, been)"}});


        @GrammaticalRelationAnnotation(
                name = "COPULA",
                description = "The \"copula\" grammatical relation. A copula is the relation between the complement of a copular verb and the copular verb.",
                codedexamples = {"Bill is big", "cop(big, is)", "Bill is an honest man", "cop(man, is)"})
        MutableGrammaticalRelation COPULA = new MutableGrammaticalRelation("COPULA", "cop", "copula",
                AUX_MODIFIER,
                "VP|SQ|SINV",
                new String[]{"VP < (/^(?:VB|AUX)/=target < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ [ $++ (/^(?:ADJP|NP$|WHNP$)/ !< VBN|VBD) | $++ (S <: (ADJP < JJ)) ] )", "SQ|SINV < (/^(?:VB|AUX)/=target < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ [ $++ (ADJP !< VBN|VBD) | $++ (NP $++ NP) | $++ (S <: (ADJP < JJ)) ] )"},
                "The \"copula\" grammatical relation. A copula is the relation between the complement of a copular verb and the copular verb.",
                new String[][]{{"Bill is big", "cop(big, is)"}, {"Bill is an honest man", "cop(man, is)"}});


        @GrammaticalRelationAnnotation(
                name = "CONJUNCT",
                description = "The \"conjunct\" grammatical relation. A conjunct is the relation between two elements connected by a conjunction word. We treat conjunctions asymmetrically: The head of the relation is the first conjunct and other conjunctions depend on it via the <i>conj</i> relation. <i>Note:</i>Modified in 2010 to exclude the case of a CC/CONJP first in its phrase: it has to conjoin things.",
                codedexamples = {"Bill is big and honest", "conj(big, honest)"})
        MutableGrammaticalRelation CONJUNCT = new MutableGrammaticalRelation("CONJUNCT", "conj", "conjunct",
                DEPENDENT,
                "VP|(?:WH)?NP(?:-TMP|-ADV)?|ADJP|PP|QP|ADVP|UCP|S|NX|SBAR|SBARQ|SINV|SQ|JJP|NML",
                new String[]{"VP|S|SBAR|SBARQ|SINV|SQ < (CC|CONJP $-- !/^(?:``|-LRB-|PRN|PP|ADVP|RB)/ $+ !/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/=target)", "VP|S|SBAR|SBARQ|SINV|SQ < (CC|CONJP $-- !/^(?:``|-LRB-|PRN|PP|ADVP|RB)/ $+ (ADVP $+ !/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/=target))", "VP|S|SBAR|SBARQ|SINV|SQ < (CC|CONJP $-- !/^(?:``|-LRB-|PRN|PP|ADVP|RB)/) < (/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/ $+ /^S$|^(?:A|N|V|PP|PRP|J|W|R)/=target)", "/^(?:ADJP|JJP|PP|QP|(?:WH)?NP(?:-TMP|-ADV)?|ADVP|UCP|NX|NML)$/ < (CC|CONJP $-- !/^(?:``|-LRB-|PRN)$/ $+ !/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/=target)", "/^(?:ADJP|PP|(?:WH)?NP(?:-TMP|-ADV)?|ADVP|UCP|NX|NML)$/ < (CC|CONJP $-- !/^(?:``|-LRB-|PRN)$/ $+ (ADVP $+ !/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/=target))", "/^(?:ADJP|PP|(?:WH)?NP(?:-TMP|-ADV)?|ADVP|UCP|NX|NML)$/ < (CC|CONJP $-- !/^(?:``|-LRB-|PRN)$/) < (/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/ $+ /^S$|^(?:A|N|V|PP|PRP|J|W|R)/=target)", "NX|NML < (CC|CONJP $- __) < (/^,$/ $- /^(?:A|N|V|PP|PRP|J|W|R|S)/=target)", "/^(?:VP|S|SBAR|SBARQ|ADJP|PP|QP|(?:WH)?NP(?:-TMP|-ADV)?|ADVP|UCP|NX|NML)$/ < (CC $++ (CC|CONJP $+ !/^(?:PRN|``|''|-[LR]RB-|,|:|\\.)$/=target))"},
                "The \"conjunct\" grammatical relation. A conjunct is the relation between two elements connected by a conjunction word. We treat conjunctions asymmetrically: The head of the relation is the first conjunct and other conjunctions depend on it via the <i>conj</i> relation. <i>Note:</i>Modified in 2010 to exclude the case of a CC/CONJP first in its phrase: it has to conjoin things.",
                new String[][]{{"Bill is big and honest", "conj(big, honest)"}});


        @GrammaticalRelationAnnotation(
                name = "COORDINATION",
                description = "The \"coordination\" grammatical relation. A coordination is the relation between an element and a conjunction.",
                codedexamples = {"Bill is big and honest.", "cc(big, and)"})
        MutableGrammaticalRelation COORDINATION = new MutableGrammaticalRelation("COORDINATION", "cc", "coordination",
                DEPENDENT,
                "S|VP|(?:WH)?NP(?:-TMP|-ADV)?|QP|ADJP|PP|ADVP|UCP|NX|SBAR|SBARQ|SINV|SQ|JJP|NML|CONJP",
                new String[]{"/^(?:S|VP|(?:WH)?NP(?:-TMP|-ADV)?|QP|ADJP|PP|ADVP|UCP|NX|SBAR|SBARQ|SINV|SQ|JJP|NML|CONJP)/ [ < (CC=target !< /^(?i:either|neither|both)$/) | < (CONJP=target !< (RB < /^(?i:not)$/ $+ (RB|JJ < /^(?i:only|just|merely)$/))) ]"},
                "The \"coordination\" grammatical relation. A coordination is the relation between an element and a conjunction.",
                new String[][]{{"Bill is big and honest.", "cc(big, and)"}});


        @GrammaticalRelationAnnotation(
                name = "PUNCTUATION",
                description = "The \"punctuation\" grammatical relation. This is used for any piece of punctuation in a clause, if punctuation is being retained in the typed dependencies.",
                codedexamples = {"Go home!", "punct(Go, !)"})
        MutableGrammaticalRelation PUNCTUATION = new MutableGrammaticalRelation("PUNCTUATION", "punct", "punctuation",
                DEPENDENT,
                ".*",
                new String[]{"__ < /^(?:\\.|:|,|''|``|-LRB-|-RRB-)$/=target"},
                "The \"punctuation\" grammatical relation. This is used for any piece of punctuation in a clause, if punctuation is being retained in the typed dependencies.",
                new String[][]{{"Go home!", "punct(Go, !)"}});


        @GrammaticalRelationAnnotation(
                name = "ARGUMENT",
                description = "The \"argument\" grammatical relation. An argument of a VP is a subject or complement of that VP; an argument of a clause is an argument of the VP which is the predicate of that clause.",
                codedexamples = {"Clinton defeated Dole", "arg(defeated, Clinton), arg(defeated, Dole)"})
        MutableGrammaticalRelation ARGUMENT = new MutableGrammaticalRelation("ARGUMENT", "arg", "argument",
                DEPENDENT,
                null,
                new String[]{},
                "The \"argument\" grammatical relation. An argument of a VP is a subject or complement of that VP; an argument of a clause is an argument of the VP which is the predicate of that clause.",
                new String[][]{{"Clinton defeated Dole", "arg(defeated, Clinton), arg(defeated, Dole)"}});


        @GrammaticalRelationAnnotation(
                name = "SUBJECT",
                description = "The \"subject\" grammatical relation. The subject of a VP is the noun or clause that performs or experiences the VP; the subject of a clause is the subject of the VP which is the predicate of that clause.",
                codedexamples = {"Clinton defeated Dole", "subj(defeated, Clinton)", "What she said is untrue", "subj(is, What she said)"})
        MutableGrammaticalRelation SUBJECT = new MutableGrammaticalRelation("SUBJECT", "subj", "subject",
                ARGUMENT,
                null,
                new String[]{},
                "The \"subject\" grammatical relation. The subject of a VP is the noun or clause that performs or experiences the VP; the subject of a clause is the subject of the VP which is the predicate of that clause.",
                new String[][]{{"Clinton defeated Dole", "subj(defeated, Clinton)"}, {"What she said is untrue", "subj(is, What she said)"}});


        @GrammaticalRelationAnnotation(
                name = "NOMINAL_SUBJECT",
                description = "The \"nominal subject\" grammatical relation. A nominal subject is a subject which is an noun phrase.",
                codedexamples = {"Clinton defeated Dole", "nsubj(defeated, Clinton)"})
        MutableGrammaticalRelation NOMINAL_SUBJECT = new MutableGrammaticalRelation("NOMINAL_SUBJECT", "nsubj", "nominal subject",
                SUBJECT,
                "S|SQ|SBARQ|SINV|SBAR|PRN",
                new String[]{"S < ((NP|WHNP=target !< EX !< (/^NN/ < (/^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/))) $++ VP)", "S < ( NP=target < (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/) !$++ NP $++VP)", "SQ|PRN < (NP=target !< EX $++ VP)", "SQ < ((NP=target !< EX) $- /^(?:VB|AUX)/ !$++ VP)", "SQ < ((NP=target !< EX) $- (RB $- /^(?:VB|AUX)/) ![$++ VP])", "SBARQ < WHNP=target < (SQ < (VP ![$-- NP]))", "SBARQ < (SQ=target < /^(?:VB|AUX)/ !< VP)", "SINV < (NP|WHNP=target [ $- VP|VBZ|VBD|VBP|VB|MD|AUX | $- (@RB|ADVP $- VP|VBZ|VBD|VBP|VB|MD|AUX) | !$- __ !$ @NP] )", "S < (NP=target $+ NP|ADJP) > VP", "SBAR < WHNP=target [ < (S < (VP !$-- NP) !< SBAR) | < (VP !$-- NP) !< S ]", "SBAR !< WHNP < (S !< (NP $++ VP)) > (VP > (S $- WHNP=target))", "SQ < ((NP < EX) $++ NP=target)", "S < (NP < EX) <+(VP) (VP < NP=target)"},
                "The \"nominal subject\" grammatical relation. A nominal subject is a subject which is an noun phrase.",
                new String[][]{{"Clinton defeated Dole", "nsubj(defeated, Clinton)"}});


        @GrammaticalRelationAnnotation(
                name = "NOMINAL_PASSIVE_SUBJECT",
                description = "The \"nominal passive subject\" grammatical relation. A nominal passive subject is a subject of a passive which is an noun phrase. This pattern recognizes basic (non-coordinated) examples. The coordinated examples are currently handled by correctDependencies() in EnglishGrammaticalStructure. This seemed more accurate than any tregex expression we could come up with.",
                codedexamples = {"Dole was defeated by Clinton", "nsubjpass(defeated, Dole)"})
        MutableGrammaticalRelation NOMINAL_PASSIVE_SUBJECT = new MutableGrammaticalRelation("NOMINAL_PASSIVE_SUBJECT", "nsubjpass", "nominal passive subject",
                NOMINAL_SUBJECT,
                "S|SQ",
                new String[]{"S|SQ < (/^(?:WH)?NP$/=target !< EX) < (VP < (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|become|becomes|became|becoming|get|got|getting|gets|gotten|remains|remained|remain)$/)  < (VP < VBN|VBD))"},
                "The \"nominal passive subject\" grammatical relation. A nominal passive subject is a subject of a passive which is an noun phrase. This pattern recognizes basic (non-coordinated) examples. The coordinated examples are currently handled by correctDependencies() in EnglishGrammaticalStructure. This seemed more accurate than any tregex expression we could come up with.",
                new String[][]{{"Dole was defeated by Clinton", "nsubjpass(defeated, Dole)"}});


        @GrammaticalRelationAnnotation(
                name = "CLAUSAL_SUBJECT",
                description = "The \"clausal subject\" grammatical relation. A clausal subject is a subject which is a clause. (subject is \"what she said\" in both examples)",
                codedexamples = {"What she said makes sense", "csubj(makes, said)", "What she said is untrue", "csubj(untrue, said)"})
        MutableGrammaticalRelation CLAUSAL_SUBJECT = new MutableGrammaticalRelation("CLAUSAL_SUBJECT", "csubj", "clausal subject",
                SUBJECT,
                "S",
                new String[]{"S < (SBAR|S=target !$+ /^,$/ $++ (VP !$-- NP))"},
                "The \"clausal subject\" grammatical relation. A clausal subject is a subject which is a clause. (subject is \"what she said\" in both examples)",
                new String[][]{{"What she said makes sense", "csubj(makes, said)"}, {"What she said is untrue", "csubj(untrue, said)"}});


        @GrammaticalRelationAnnotation(
                name = "CLAUSAL_PASSIVE_SUBJECT",
                description = "The \"clausal passive subject\" grammatical relation. A clausal passive subject is a subject of a passive verb which is a clause. (subject is \"that she lied\")",
                codedexamples = {"That she lied was suspected by everyone", "csubjpass(suspected, lied)"})
        MutableGrammaticalRelation CLAUSAL_PASSIVE_SUBJECT = new MutableGrammaticalRelation("CLAUSAL_PASSIVE_SUBJECT", "csubjpass", "clausal subject",
                CLAUSAL_SUBJECT,
                "S",
                new String[]{"S < (SBAR|S=target !$+ /^,$/ $++ (VP < (VP < VBN|VBD) < (/^(?:VB|AUXG?)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|become|becomes|became|becoming|get|got|getting|gets|gotten|remains|remained|remain)$/) !$-- NP))", "S < (SBAR|S=target !$+ /^,$/ $++ (VP <+(VP) (VP < VBN|VBD > (VP < (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|become|becomes|became|becoming|get|got|getting|gets|gotten|remains|remained|remain)$/))) !$-- NP))"},
                "The \"clausal passive subject\" grammatical relation. A clausal passive subject is a subject of a passive verb which is a clause. (subject is \"that she lied\")",
                new String[][]{{"That she lied was suspected by everyone", "csubjpass(suspected, lied)"}});


        @GrammaticalRelationAnnotation(
                name = "COMPLEMENT",
                description = "The \"complement\" grammatical relation. A complement of a VP is any object (direct or indirect) of that VP, or a clause or adjectival phrase which functions like an object; a complement of a clause is an complement of the VP which is the predicate of that clause.",
                codedexamples = {"She gave me a raise", "comp(gave, me), comp(gave, a raise)", "I like to swim", "comp(like, to swim)"})
        MutableGrammaticalRelation COMPLEMENT = new MutableGrammaticalRelation("COMPLEMENT", "comp", "complement",
                ARGUMENT,
                null,
                new String[]{},
                "The \"complement\" grammatical relation. A complement of a VP is any object (direct or indirect) of that VP, or a clause or adjectival phrase which functions like an object; a complement of a clause is an complement of the VP which is the predicate of that clause.",
                new String[][]{{"She gave me a raise", "comp(gave, me), comp(gave, a raise)"}, {"I like to swim", "comp(like, to swim)"}});


        @GrammaticalRelationAnnotation(
                name = "OBJECT",
                description = "The \"object\" grammatical relation. An object of a VP is any direct object or indirect object of that VP; an object of a clause is an object of the VP which is the predicate of that clause.",
                codedexamples = {"She gave me a raise", "obj(gave, me), obj(gave, raise)"})
        MutableGrammaticalRelation OBJECT = new MutableGrammaticalRelation("OBJECT", "obj", "object",
                COMPLEMENT,
                null,
                new String[]{},
                "The \"object\" grammatical relation. An object of a VP is any direct object or indirect object of that VP; an object of a clause is an object of the VP which is the predicate of that clause.",
                new String[][]{{"She gave me a raise", "obj(gave, me), obj(gave, raise)"}});


        @GrammaticalRelationAnnotation(
                name = "DIRECT_OBJECT",
                description = "The \"direct object\" grammatical relation. The direct object of a VP is the noun phrase which is the (accusative) object of the verb; the direct object of a clause is the direct object of the VP which is the predicate of that clause.",
                codedexamples = {"She gave me a raise", "dobj(gave, raise)"})
        MutableGrammaticalRelation DIRECT_OBJECT = new MutableGrammaticalRelation("DIRECT_OBJECT", "dobj", "direct object",
                OBJECT,
                "SBARQ|VP|SBAR",
                new String[]{"VP < (NP $+ (/^(?:NP|WHNP)$/=target !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter|lot)$/))) !<(/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/)", "VP < (NP < (NP $+ (/^(NP|WHNP)$/=target !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter|lot)$/))))!< (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/)", "VP !<(/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/) < (/^(?:NP|WHNP)$/=target [ [ !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/) !$+ NP ] | $+ NP-TMP | $+ (NP < (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/)) ] )", "SBARQ < (WHNP=target !< WRB !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/)) <+(SQ|SINV|S|VP) (VP !< NP|TO !< (S < (VP < TO)) !< (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ $++ (VP < VBN|VBD)) !<- PRT !<- (PP <: IN) $-- (NP !< /^-NONE-$/))", "SBAR < (WHNP=target !< WRB) < (S < NP < (VP !< SBAR !<+(VP) (PP <- IN) !< (S < (VP < TO))))", "SBAR !< WHNP|WHADVP < (S < (@NP $++ (VP !$++ NP))) > (VP > (S < NP $- WHNP=target))", "SBAR !< (WHPP|WHNP|WHADVP) < (S < (@NP $+ (VP !< (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ !$+ VP)  !<+(VP) (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ $+ (VP < VBN|VBD)) !<+(VP) NP !< SBAR !<+(VP) (PP <- IN)))) !$-- CC $-- NP > NP=target", "SBAR !< (WHPP|WHNP|WHADVP) < (S < (@NP $+ (ADVP $+ (VP !< (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ !$+ VP) !<+(VP) (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ $+ (VP < VBN|VBD)) !<+(VP) NP !< SBAR !<+(VP) (PP <- IN))))) !$-- CC $-- NP > NP=target"},
                "The \"direct object\" grammatical relation. The direct object of a VP is the noun phrase which is the (accusative) object of the verb; the direct object of a clause is the direct object of the VP which is the predicate of that clause.",
                new String[][]{{"She gave me a raise", "dobj(gave, raise)"}});


        @GrammaticalRelationAnnotation(
                name = "INDIRECT_OBJECT",
                description = "The \"indirect object\" grammatical relation. The indirect object of a VP is the noun phrase which is the (dative) object of the verb; the indirect object of a clause is the indirect object of the VP which is the predicate of that clause.",
                codedexamples = {"She gave me a raise", "iobj(gave, me)"})
        MutableGrammaticalRelation INDIRECT_OBJECT = new MutableGrammaticalRelation("INDIRECT_OBJECT", "iobj", "indirect object",
                OBJECT,
                "VP",
                new String[]{"VP < (NP=target !< /\\$/ !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/) $+ (NP !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/)))", "VP < (NP=target < (NP !< /\\$/ $++ (NP !< (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter|lot)$/)) !$ CC|CONJP !$ /^,$/ !$++ /^:$/))"},
                "The \"indirect object\" grammatical relation. The indirect object of a VP is the noun phrase which is the (dative) object of the verb; the indirect object of a clause is the indirect object of the VP which is the predicate of that clause.",
                new String[][]{{"She gave me a raise", "iobj(gave, me)"}});


        @GrammaticalRelationAnnotation(
                name = "PREPOSITIONAL_OBJECT",
                description = "The \"prepositional object\" grammatical relation. The object of a preposition is the head of a noun phrase following the preposition, or the adverbs \"here\" and \"there\". (The preposition in turn may be modifying a noun, verb, etc.) We here define cases of VBG quasi-prepositions like \"including\", \"concerning\", etc. as instances of pobj (unlike the Penn Treebank). (The preposition can be called a FW for pace, versus, etc. It can also be called a CC - but we don't currently handle that and would need to distinguish from conjoined PPs. Jan 2010 update: We now insist that the NP must follow the preposition. This prevents a preceding NP measure phrase being matched as a pobj. We do allow a preposition tagged RB followed by an NP pobj, as happens in the Penn Treebank for adverbial uses of PP like \"up 19%\")",
                codedexamples = {"I sat on the chair", "pobj(on, chair)"})
        MutableGrammaticalRelation PREPOSITIONAL_OBJECT = new MutableGrammaticalRelation("PREPOSITIONAL_OBJECT", "pobj", "prepositional object",
                OBJECT,
                "PP(?:-TMP)?|WHPP|PRT|ADVP|WHADVP|XS",
                new String[]{"/^(?:PP(?:-TMP)?|(?:WH)?(?:PP|ADVP))$/ < (IN|VBG|TO|FW|RB|RBR $++ (/^(?:WH)?(?:NP|ADJP)(?:-TMP|-ADV)?$/=target !$- @NP))", "/^PP(?:-TMP)?$/ < (/^(?:IN|VBG|TO)$/ $+ (ADVP=target [ < (RB < /^(?i:here|there)$/) | < (ADVP < /^NP(?:-TMP)?$/) ] ))", "PRT >- (VP !< (S < (VP < TO)) >+(SQ|SINV|S|VP) (SBARQ <, (WHNP=target !< WRB)) $-- (NP !< /^-NONE-$/))", "(PP <: IN|TO) >- (VP !< (S < (VP < TO)) >+(SQ|SINV|S|VP) (SBARQ <, (WHNP=target !< WRB)) $-- (NP !< /^-NONE-$/))", "(PP <: IN|TO) $- (NP $-- (VBZ|VBD) !$++ VP) >+(SQ) (SBARQ <, (WHNP=target !< WRB)) $-- (NP !< /^-NONE-$/)", "(PP <- IN|TO) >+(@VP|S|SINV|SBAR) (SBAR !< (WHPP|WHNP) < (S < (NP $+ (VP !<(/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ !$+ VP) !<+(VP) NP !< SBAR ))) $-- NP > NP=target)", "XS|ADVP < (IN < /^(?i:at)$/) < JJS|DT=target", "PP < (CC < less) < NP"},
                "The \"prepositional object\" grammatical relation. The object of a preposition is the head of a noun phrase following the preposition, or the adverbs \"here\" and \"there\". (The preposition in turn may be modifying a noun, verb, etc.) We here define cases of VBG quasi-prepositions like \"including\", \"concerning\", etc. as instances of pobj (unlike the Penn Treebank). (The preposition can be called a FW for pace, versus, etc. It can also be called a CC - but we don't currently handle that and would need to distinguish from conjoined PPs. Jan 2010 update: We now insist that the NP must follow the preposition. This prevents a preceding NP measure phrase being matched as a pobj. We do allow a preposition tagged RB followed by an NP pobj, as happens in the Penn Treebank for adverbial uses of PP like \"up 19%\")",
                new String[][]{{"I sat on the chair", "pobj(on, chair)"}});


        @GrammaticalRelationAnnotation(
                name = "PREPOSITIONAL_COMPLEMENT",
                description = "The \"prepositional complement\" grammatical relation. This is used when the complement of a preposition is a clause or an adverbial or prepositional phrase. The prepositional complement of a preposition is the head of the sentence following the preposition, or the preposition head of the PP. It is warmer in Greece than in Italy &arr; <code>pcomp</code>(than, in)",
                codedexamples = {"We have no useful information on whether users are at risk", "pcomp(on, are)", "They heard about you missing classes.", "pcomp(about, missing)"})
        MutableGrammaticalRelation PREPOSITIONAL_COMPLEMENT = new MutableGrammaticalRelation("PREPOSITIONAL_COMPLEMENT", "pcomp", "prepositional complement",
                COMPLEMENT,
                "PP(?:-TMP)?",
                new String[]{"@PP < (IN|VBG|VBN|TO $+ @SBAR|S|PP|ADVP=target)", "@PP !< IN|TO < (SBAR=target <, (IN $+ S))"},
                "The \"prepositional complement\" grammatical relation. This is used when the complement of a preposition is a clause or an adverbial or prepositional phrase. The prepositional complement of a preposition is the head of the sentence following the preposition, or the preposition head of the PP. It is warmer in Greece than in Italy &arr; <code>pcomp</code>(than, in)",
                new String[][]{{"We have no useful information on whether users are at risk", "pcomp(on, are)"}, {"They heard about you missing classes.", "pcomp(about, missing)"}});


        @GrammaticalRelationAnnotation(
                name = "ATTRIBUTIVE",
                description = "The \"attributive\" grammatical relation. The attributive is the complement of a verb such as \"to be, to seem, to appear\". These mainly occur in questions. Arguably they shouldn't and we should treat the question WHNP and WHADJP as predicates (as we do for ADJP and NP complements (NP-PRD and ADJP-PRD), but we at present don't produce this.",
                codedexamples = {})
        MutableGrammaticalRelation ATTRIBUTIVE = new MutableGrammaticalRelation("ATTRIBUTIVE", "attr", "attributive",
                COMPLEMENT,
                "VP|SBARQ|SQ",
                new String[]{"VP !$ (NP < EX) < NP=target <(/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/)", "SBARQ < (WHNP|WHADJP=target $+ (SQ < (/^VB|AUX?/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ !$++ VP) !< (VP <- (PP <:IN)) !<- (PP <: IN)))", "SBARQ <, (WHNP|WHADJP=target !< WRB) <+(SQ|SINV|S|VP) (VP !< (S < (VP < TO)) < (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ $++ (VP < VBN|VBD)) !<- PRT !<- (PP <: IN) $-- (NP !< /^-NONE-$/))", "SQ <, (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/) < (NP=target $-- (NP !< EX))"},
                "The \"attributive\" grammatical relation. The attributive is the complement of a verb such as \"to be, to seem, to appear\". These mainly occur in questions. Arguably they shouldn't and we should treat the question WHNP and WHADJP as predicates (as we do for ADJP and NP complements (NP-PRD and ADJP-PRD), but we at present don't produce this.",
                new String[][]{});


        @GrammaticalRelationAnnotation(
                name = "CLAUSAL_COMPLEMENT",
                description = "The \"clausal complement\" grammatical relation. A clausal complement of a verb or adjective is a dependent clause with an internal subject which functions like an object of the verb, or adjective. Clausal complements for nouns are limited to complement clauses with a subset of nouns like \"fact\" or \"report\". We analyze them the same (parallel to the analysis of this class as \"content clauses\" in Huddleston and Pullum 2002). Clausal complements are usually finite (though there are occasional exceptions including remnant English subjunctives, and we also classify the complement of causative \"have\" (She had him arrested) in this category.",
                codedexamples = {"He says that you like to swim", "ccomp(says, like)", "I am certain that he did it", "ccomp(certain, did)", "I admire the fact that you are honest", "ccomp(fact, honest)"})
        MutableGrammaticalRelation CLAUSAL_COMPLEMENT = new MutableGrammaticalRelation("CLAUSAL_COMPLEMENT", "ccomp", "clausal complement",
                COMPLEMENT,
                "VP|SINV|S|ADJP|ADVP|NP",
                new String[]{"VP < (S=target < (VP !<, TO|VBG|VBN) !$-- NP)", "VP < (SBAR=target < (S <+(S) VP) <, (IN|DT < /^(?i:that|whether)$/))", "VP < (SBAR=target < (SBAR < (S <+(S) VP) <, (IN|DT < /^(?i:that|whether)$/)) < CC|CONJP)", "VP < (SBAR=target < (S < VP) !$-- NP !<, (IN|WHADVP))", "VP < (SBAR=target < (S < VP) !$-- NP <, (WHADVP < (WRB < /^(?i:how)$/)))", "VP < (/^VB/ < /^(?i:have|had|has|having)$/) < (S=target < @NP < VP)", "S|SINV < (S|SBARQ=target $+ /^(,|\\.|'')$/ !$- /^(?:CC|:)$/ !< (VP < TO|VBG|VBN))", "ADVP < (SBAR=target < (IN < /^(?i:as|that)/) < (S < (VP !< TO)))", "ADJP < (SBAR=target < (IN !< than) < (S < (VP !< TO)))", "S <, (SBAR=target <, (IN < /^(?i:that|whether)$/) !$+ VP)", "@NP < JJ|NN|NNS < SBAR=target"},
                "The \"clausal complement\" grammatical relation. A clausal complement of a verb or adjective is a dependent clause with an internal subject which functions like an object of the verb, or adjective. Clausal complements for nouns are limited to complement clauses with a subset of nouns like \"fact\" or \"report\". We analyze them the same (parallel to the analysis of this class as \"content clauses\" in Huddleston and Pullum 2002). Clausal complements are usually finite (though there are occasional exceptions including remnant English subjunctives, and we also classify the complement of causative \"have\" (She had him arrested) in this category.",
                new String[][]{{"He says that you like to swim", "ccomp(says, like)"}, {"I am certain that he did it", "ccomp(certain, did)"}, {"I admire the fact that you are honest", "ccomp(fact, honest)"}});


        @GrammaticalRelationAnnotation(
                name = "XCLAUSAL_COMPLEMENT",
                description = "An open clausal complement (<i>xcomp</i>) of a VP or an ADJP is a clausal complement without its own subject, whose reference is determined by an external subject. These complements are always non-finite. The name <i>xcomp</i> is borrowed from Lexical-Functional Grammar. (Mainly \"TO-clause\" are recognized, but also some VBG like \"stop eating\")",
                codedexamples = {"I like to swim", "xcomp(like, swim)", "I am ready to leave", "xcomp(ready, leave)"})
        MutableGrammaticalRelation XCLAUSAL_COMPLEMENT = new MutableGrammaticalRelation("XCLAUSAL_COMPLEMENT", "xcomp", "xclausal complement",
                COMPLEMENT,
                "VP|ADJP",
                new String[]{"VP < (S=target !$- (NN < /^order$/) < (VP < TO))", "ADJP < (S=target <, (VP <, TO))", "VP < (S=target !$- (NN < /^order$/) < (NP $+ NP|ADJP))", "VP < (/^(?:VB|AUX)/ $+ (VP=target < VB < NP))", "VP !> (VP < (VB|AUX < be)) < (SBAR=target < (S !$- (NN < /^order$/) < (VP < TO)))", "VP > VP < (S=target !$- (NN < /^order$/) <: NP)", "VP < (S=target !< NP < (VP < VBG))"},
                "An open clausal complement (<i>xcomp</i>) of a VP or an ADJP is a clausal complement without its own subject, whose reference is determined by an external subject. These complements are always non-finite. The name <i>xcomp</i> is borrowed from Lexical-Functional Grammar. (Mainly \"TO-clause\" are recognized, but also some VBG like \"stop eating\")",
                new String[][]{{"I like to swim", "xcomp(like, swim)"}, {"I am ready to leave", "xcomp(ready, leave)"}});


        @GrammaticalRelationAnnotation(
                name = "COMPLEMENTIZER",
                description = "The \"complementizer\" grammatical relation. A complementizer of a clausal complement is the word introducing it.",
                codedexamples = {"He says that you like to swim", "complm(like, that)"})
        MutableGrammaticalRelation COMPLEMENTIZER = new MutableGrammaticalRelation("COMPLEMENTIZER", "complm", "complementizer",
                COMPLEMENT,
                "SBAR",
                new String[]{"SBAR <, (IN|DT=target < /^(?:that|whether)$/) [ $-- /^(?:VB|AUX)/ | $- NP|NN|NNS | > ADJP|PP | > (@NP|UCP|SBAR < CC|CONJP $-- /^(?:VB|AUX)/) ]", "SBAR <, (IN|DT=target < /^(?:That|Whether)$/)"},
                "The \"complementizer\" grammatical relation. A complementizer of a clausal complement is the word introducing it.",
                new String[][]{{"He says that you like to swim", "complm(like, that)"}});


        @GrammaticalRelationAnnotation(
                name = "MARKER",
                description = "The \"marker\" grammatical relation. A marker of an adverbial clausal complement is the word introducing it.",
                codedexamples = {"U.S. forces have been engaged in intense fighting after insurgents launched simultaneous attacks", "mark(launched, after)"})
        MutableGrammaticalRelation MARKER = new MutableGrammaticalRelation("MARKER", "mark", "marker",
                COMPLEMENT,
                "SBAR(?:-TMP)?",
                new String[]{"/^SBAR(?:-TMP)?$/ <, (IN=target !< /^(?i:that|whether)$/) < S|FRAG"},
                "The \"marker\" grammatical relation. A marker of an adverbial clausal complement is the word introducing it.",
                new String[][]{{"U.S. forces have been engaged in intense fighting after insurgents launched simultaneous attacks", "mark(launched, after)"}});


        @GrammaticalRelationAnnotation(
                name = "RELATIVE",
                description = "The \"relative\" grammatical relation. A relative of a relative clause is the head word of the WH-phrase introducing it. Note that this is designed to *not* match cases when there is no overt subject NP. They are instead matched by the nsubj rule. Effectively this gives us an HPSG-like relative clause analysis, where subject relatives are analyzed as regular subject structures. And \"why\" (WHADVP) cases are treated as advmod not as rel.",
                codedexamples = {"I saw the man that you love", "rel(love, that)", "I saw the man whose wife you love", "rel(love, wife)"})
        MutableGrammaticalRelation RELATIVE = new MutableGrammaticalRelation("RELATIVE", "rel", "relative",
                COMPLEMENT,
                "SBAR",
                new String[]{"SBAR <, WHNP|WHPP|WHADJP=target > /^NP/ [ !<, /^WHNP/ | < (S < (VP $-- (/^NP/ !< /^-NONE-$/)))]"},
                "The \"relative\" grammatical relation. A relative of a relative clause is the head word of the WH-phrase introducing it. Note that this is designed to *not* match cases when there is no overt subject NP. They are instead matched by the nsubj rule. Effectively this gives us an HPSG-like relative clause analysis, where subject relatives are analyzed as regular subject structures. And \"why\" (WHADVP) cases are treated as advmod not as rel.",
                new String[][]{{"I saw the man that you love", "rel(love, that)"}, {"I saw the man whose wife you love", "rel(love, wife)"}});


        @GrammaticalRelationAnnotation(
                name = "REFERENT",
                description = "The \"referent\" grammatical relation. A referent of the Wh-word of a NP is the relative word introducing the relative clause modifying the NP.",
                codedexamples = {"I saw the book which you bought", "ref(book, which)", "I saw the book the cover of which you designed", "ref(book, which)"})
        MutableGrammaticalRelation REFERENT = new MutableGrammaticalRelation("REFERENT", "ref", "referent",
                DEPENDENT,
                "NP",
                new String[]{"NP $+ (SBAR <+(SBAR) (WHNP|WHPP <+(@WHNP|WHPP|NP|PP) /^(?:WDT|WP|WP\\$)$/=target)) > NP", "NP $+ (/^(?:,|PP|PRN)$/ $+ (SBAR < (WHNP|WHPP <+(@WHNP|WHPP|NP|PP) /^(?:WDT|WP|WP\\$)$/=target))) > NP", "NP $+ (/^(?:,|PP|PRN)$/ $+ (/^(?:\"|''|,)$/ $+ (SBAR < (WHNP|WHPP <+(@WHNP|WHPP|NP|PP) /^(?:WDT|WP|WP\\$)$/=target)))) > NP"},
                "The \"referent\" grammatical relation. A referent of the Wh-word of a NP is the relative word introducing the relative clause modifying the NP.",
                new String[][]{{"I saw the book which you bought", "ref(book, which)"}, {"I saw the book the cover of which you designed", "ref(book, which)"}});


        @GrammaticalRelationAnnotation(
                name = "EXPLETIVE",
                description = "The \"expletive\" grammatical relation. This relation captures an existential there.",
                codedexamples = {"There is a statue in the corner", "expl(is, there)"})
        MutableGrammaticalRelation EXPLETIVE = new MutableGrammaticalRelation("EXPLETIVE", "expl", "expletive",
                DEPENDENT,
                "S|SQ",
                new String[]{"S|SQ < (NP=target < EX)"},
                "The \"expletive\" grammatical relation. This relation captures an existential there.",
                new String[][]{{"There is a statue in the corner", "expl(is, there)"}});


        @GrammaticalRelationAnnotation(
                name = "ADJECTIVAL_COMPLEMENT",
                description = "The \"adjectival complement\" grammatical relation. An adjectival complement of a VP is an adjectival phrase which functions as the complement (like an object of the verb); an adjectival complement of a clause is the adjectival complement of the VP which is the predicate of that clause.",
                codedexamples = {"She looks very beautiful", "acomp(looks, beautiful)"})
        MutableGrammaticalRelation ADJECTIVAL_COMPLEMENT = new MutableGrammaticalRelation("ADJECTIVAL_COMPLEMENT", "acomp", "adjectival complement",
                COMPLEMENT,
                "VP",
                new String[]{"VP [ < (ADJP=target !$-- NP) |  < (/^VB/ $+ (@S=target < (@ADJP < /^JJ/ ! $-- @NP|S))) ]"},
                "The \"adjectival complement\" grammatical relation. An adjectival complement of a VP is an adjectival phrase which functions as the complement (like an object of the verb); an adjectival complement of a clause is the adjectival complement of the VP which is the predicate of that clause.",
                new String[][]{{"She looks very beautiful", "acomp(looks, beautiful)"}});


        @GrammaticalRelationAnnotation(
                name = "MODIFIER",
                description = "The \"modifier\" grammatical relation. A modifier of a VP is any constituent that serves to modify the meaning of the VP (but is not an <code>ARGUMENT</code> of that VP); a modifier of a clause is an modifier of the VP which is the predicate of that clause.",
                codedexamples = {"Last night, I swam in the pool", "mod(swam, in the pool), mod(swam, last night)"})
        MutableGrammaticalRelation MODIFIER = new MutableGrammaticalRelation("MODIFIER", "mod", "modifier",
                DEPENDENT,
                null,
                new String[]{},
                "The \"modifier\" grammatical relation. A modifier of a VP is any constituent that serves to modify the meaning of the VP (but is not an <code>ARGUMENT</code> of that VP); a modifier of a clause is an modifier of the VP which is the predicate of that clause.",
                new String[][]{{"Last night, I swam in the pool", "mod(swam, in the pool), mod(swam, last night)"}});


        @GrammaticalRelationAnnotation(
                name = "ADV_CLAUSE_MODIFIER",
                description = "The \"adverbial clause modifier\" grammatical relation. An adverbial clause modifier of a VP or (inverted) sentence is a clause modifying the verb (temporal clauses, consequences, conditional clauses, etc.).",
                codedexamples = {"The accident happened as the night was falling", "advcl(happened, falling)", "If you know who did it, you should tell the teacher", "advcl(tell, know)"})
        MutableGrammaticalRelation ADV_CLAUSE_MODIFIER = new MutableGrammaticalRelation("ADV_CLAUSE_MODIFIER", "advcl", "adverbial clause modifier",
                MODIFIER,
                "VP|S|SQ|SINV|SBARQ",
                new String[]{"VP < (@SBAR=target [ <, (IN !< /^(?i:that|whether)$/ !$+ (NN < /^order$/)) | <: (SINV <1 /^(?:VB|MD|AUX)/) | < (IN < that) < (RB|IN < so) ] )", "S|SQ|SINV <, (/^SBAR(?:-TMP)?$/=target <, (IN !< /^(?i:that|whether)$/ !$+ (NN < /^order$/)) !$+ VP)", "S|SQ|SINV <, (/^SBAR(?:-TMP)?$/=target <2 (IN !< /^(?i:that|whether)$/ !$+ (NN < /^order$/)))", "S|SQ|SINV < (/^SBAR(?:-TMP)?$/=target <, (IN !< /^(?i:that|whether)$/ !$+ (NN < /^order$/)) !$+ @VP $+ /^,$/ $++ @NP)", "SBARQ < (/^SBAR(?:-TMP|ADV)?$/=target <, (IN !< /^(?i:that|whether)$/ !$+ (NN < /^order$/)) $+ /^,$/ $++ @SQ|S|SBARQ)", "VP < (/^SBAR(?:-TMP)?$/=target <, (WHADVP|WHNP < (WRB !< /^(?i:how)$/)) !< (S < (VP < TO)))", "S|SQ < (/^SBAR(?:-TMP)?$/=target <, (WHADVP|WHNP < (WRB !< /^(?i:how)$/)) !< (S < (VP < TO)))", "S|SQ <, (PP=target <, RB)"},
                "The \"adverbial clause modifier\" grammatical relation. An adverbial clause modifier of a VP or (inverted) sentence is a clause modifying the verb (temporal clauses, consequences, conditional clauses, etc.).",
                new String[][]{{"The accident happened as the night was falling", "advcl(happened, falling)"}, {"If you know who did it, you should tell the teacher", "advcl(tell, know)"}});


        @GrammaticalRelationAnnotation(
                name = "PURPOSE_CLAUSE_MODIFIER",
                description = "The \"purpose clause modifier\" grammatical relation. A purpose clause modifier of a VP is a clause headed by \"(in order) to\" specifying a purpose. Note: at present we only recognize ones that have \"in order to\" or are fronted. Otherwise we can't use our surface representations distinguish these from xcomp's. We can also recognize \"to\" clauses introduced by \"be VBN\".",
                codedexamples = {"He talked to the president in order to secure the account", "purpcl(talked, secure)"})
        MutableGrammaticalRelation PURPOSE_CLAUSE_MODIFIER = new MutableGrammaticalRelation("PURPOSE_CLAUSE_MODIFIER", "purpcl", "purpose clause modifier",
                MODIFIER,
                "VP|S",
                new String[]{"VP < (/^SBAR/=target < (IN < in) < (NN < order) < (S < (VP < TO)))", "@S < (@S=target < (VP < TO) $+ (/^,$/ $+ @NP))"},
                "The \"purpose clause modifier\" grammatical relation. A purpose clause modifier of a VP is a clause headed by \"(in order) to\" specifying a purpose. Note: at present we only recognize ones that have \"in order to\" or are fronted. Otherwise we can't use our surface representations distinguish these from xcomp's. We can also recognize \"to\" clauses introduced by \"be VBN\".",
                new String[][]{{"He talked to the president in order to secure the account", "purpcl(talked, secure)"}});


        @GrammaticalRelationAnnotation(
                name = "RELATIVE_CLAUSE_MODIFIER",
                description = "The \"relative clause modifier\" grammatical relation. A relative clause modifier of an NP is a relative clause modifying the NP. The link points from the head noun of the NP to the head of the relative clause, normally a verb.",
                codedexamples = {"I saw the man you love", "rcmod(man, love)", "I saw the book which you bought", "rcmod(book, bought)"})
        MutableGrammaticalRelation RELATIVE_CLAUSE_MODIFIER = new MutableGrammaticalRelation("RELATIVE_CLAUSE_MODIFIER", "rcmod", "relative clause modifier",
                MODIFIER,
                "(?:WH)?NP|NML",
                new String[]{"NP|WHNP|NML $++ (SBAR=target <+(SBAR) WHPP|WHNP) > @NP|WHNP", "NP|WHNP|NML $++ (SBAR=target <: (S !<, (VP <, TO))) > @NP|WHNP", "NP|NML $++ (SBAR=target < (WHADVP < (WRB </^(?i:where|why)/))) > @NP", "NP|WHNP|NML $++ RRC=target"},
                "The \"relative clause modifier\" grammatical relation. A relative clause modifier of an NP is a relative clause modifying the NP. The link points from the head noun of the NP to the head of the relative clause, normally a verb.",
                new String[][]{{"I saw the man you love", "rcmod(man, love)"}, {"I saw the book which you bought", "rcmod(book, bought)"}});


        @GrammaticalRelationAnnotation(
                name = "ADJECTIVAL_MODIFIER",
                description = "The \"adjectival modifier\" grammatical relation. An adjectival modifier of an NP is any adjectival phrase that serves to modify the meaning of the NP. The relation amod is also used for multiword country adjectives, despite their questionable treebank representation.",
                codedexamples = {"Sam eats red meat", "amod(meat, red)", "the West German economy", "amod(German, West), amod(economy, German)"})
        MutableGrammaticalRelation ADJECTIVAL_MODIFIER = new MutableGrammaticalRelation("ADJECTIVAL_MODIFIER", "amod", "adjectival modifier",
                MODIFIER,
                "NP(?:-TMP|-ADV)?|NX|NML|NAC|WHNP|ADJP",
                new String[]{"/^(?:NP(?:-TMP|-ADV)?|NX|NML|NAC|WHNP)$/ < (ADJP|WHADJP|JJ|JJR|JJS|JJP|VBN|VBG|VBD|IN=target !< QP !$- CC)", "ADJP !< CC|CONJP < (JJ|NNP $ JJ|NNP=target)"},
                "The \"adjectival modifier\" grammatical relation. An adjectival modifier of an NP is any adjectival phrase that serves to modify the meaning of the NP. The relation amod is also used for multiword country adjectives, despite their questionable treebank representation.",
                new String[][]{{"Sam eats red meat", "amod(meat, red)"}, {"the West German economy", "amod(German, West), amod(economy, German)"}});


        @GrammaticalRelationAnnotation(
                name = "NUMERIC_MODIFIER",
                description = "The \"numeric modifier\" grammatical relation. A numeric modifier of an NP is any number phrase that serves to modify the meaning of the NP.",
                codedexamples = {"Sam eats 3 sheep", "num(sheep, 3)"})
        MutableGrammaticalRelation NUMERIC_MODIFIER = new MutableGrammaticalRelation("NUMERIC_MODIFIER", "num", "numeric modifier",
                MODIFIER,
                "(?:WH)?NP(?:-TMP|-ADV)?|NML|NX",
                new String[]{"/^(?:WH)?(?:NP|NX|NML)(?:-TMP|-ADV)?$/ < (CD|QP=target !$- CC)", "/^(?:WH)?(?:NP|NX|NML)(?:-TMP|-ADV)?$/ < (ADJP=target <: QP)"},
                "The \"numeric modifier\" grammatical relation. A numeric modifier of an NP is any number phrase that serves to modify the meaning of the NP.",
                new String[][]{{"Sam eats 3 sheep", "num(sheep, 3)"}});


        @GrammaticalRelationAnnotation(
                name = "NUMBER_MODIFIER",
                description = "The \"compound number modifier\" grammatical relation. A compound number modifier is a part of a number phrase or currency amount.",
                codedexamples = {"I lost $ 3.2 billion", "number($, billion)"})
        MutableGrammaticalRelation NUMBER_MODIFIER = new MutableGrammaticalRelation("NUMBER_MODIFIER", "number", "compound number modifier",
                MODIFIER,
                "QP|ADJP",
                new String[]{"QP|ADJP < (/^(?:CD|$|#)$/=target !$- CC)"},
                "The \"compound number modifier\" grammatical relation. A compound number modifier is a part of a number phrase or currency amount.",
                new String[][]{{"I lost $ 3.2 billion", "number($, billion)"}});


        @GrammaticalRelationAnnotation(
                name = "QUANTIFIER_MODIFIER",
                description = "The \"quantifier phrase modifier\" grammatical relation. A quantifier modifier is an element modifying the head of a QP constituent.",
                codedexamples = {"About 200 people came to the party", "quantmod(200, About)"})
        MutableGrammaticalRelation QUANTIFIER_MODIFIER = new MutableGrammaticalRelation("QUANTIFIER_MODIFIER", "quantmod", "quantifier modifier",
                MODIFIER,
                "QP",
                new String[]{"QP < /^(?:IN|RB|RBR|RBS|PDT|DT|JJ|JJR|JJS|XS)$/=target"},
                "The \"quantifier phrase modifier\" grammatical relation. A quantifier modifier is an element modifying the head of a QP constituent.",
                new String[][]{{"About 200 people came to the party", "quantmod(200, About)"}});


        @GrammaticalRelationAnnotation(
                name = "NOUN_COMPOUND_MODIFIER",
                description = "The \"noun compound modifier\" grammatical relation. A noun compound modifier of an NP is any noun that serves to modify the head noun. Note that this has all nouns modify the rightmost a la Penn headship rules. There is no intelligent noun compound analysis.",
                codedexamples = {"Oil price futures", "nn(futures, oil), nn(futures, price)"})
        MutableGrammaticalRelation NOUN_COMPOUND_MODIFIER = new MutableGrammaticalRelation("NOUN_COMPOUND_MODIFIER", "nn", "nn modifier",
                MODIFIER,
                "(?:WH)?(?:NP|NX|NAC|NML|ADVP|ADJP)(?:-TMP|-ADV)?",
                new String[]{"/^(?:WH)?(?:NP|NX|NAC|NML)(?:-TMP|-ADV)?$/ < (NP|NML|NN|NNS|NNP|NNPS|FW=target $++ NN|NNS|NNP|NNPS|FW|CD !<<- POS !$- /^,$/ )", "/^(?:WH)?(?:NP|NX|NAC|NML)(?:-TMP|-ADV)?$/ < (NP|NML|NN|NNS|NNP|NNPS|FW=target !<<- POS $+ JJ|JJR|JJS) <# NN|NNS|NNP|NNPS !<<- POS", "ADJP|ADVP < (FW [ $- FW=target | $- (IN=target < in|In) ] )"},
                "The \"noun compound modifier\" grammatical relation. A noun compound modifier of an NP is any noun that serves to modify the head noun. Note that this has all nouns modify the rightmost a la Penn headship rules. There is no intelligent noun compound analysis.",
                new String[][]{{"Oil price futures", "nn(futures, oil), nn(futures, price)"}});


        @GrammaticalRelationAnnotation(
                name = "APPOSITIONAL_MODIFIER",
                description = "The \"appositional modifier\" grammatical relation. An appositional modifier of an NP is an NP that serves to modify the meaning of the NP. It includes parenthesized examples",
                codedexamples = {"Sam, my brother, eats red meat", "appos(Sam, brother)", "Bill (John's cousin)", "appos(Bill, cousin)."})
        MutableGrammaticalRelation APPOSITIONAL_MODIFIER = new MutableGrammaticalRelation("APPOSITIONAL_MODIFIER", "appos", "appositional modifier",
                MODIFIER,
                "(?:WH)?NP(?:-TMP|-ADV)?",
                new String[]{"/^(?:WH)?NP(?:-TMP|-ADV)?$/ < (NP=target $- /^,$/ $-- /^(?:WH)?NP/ !$ CC|CONJP)", "/^(?:WH)?NP(?:-TMP|-ADV)?$/ < (PRN=target < (NP < /^NNS?|CD$/ $-- /^-LRB-$/ $+ /^-RRB-$/))", "/^NP(?:-TMP|-ADV)?$/ < (NNP $+ (/^,$/ $+ NNP=target)) !< CC|CONJP"},
                "The \"appositional modifier\" grammatical relation. An appositional modifier of an NP is an NP that serves to modify the meaning of the NP. It includes parenthesized examples",
                new String[][]{{"Sam, my brother, eats red meat", "appos(Sam, brother)"}, {"Bill (John's cousin)", "appos(Bill, cousin)."}});


        @GrammaticalRelationAnnotation(
                name = "ABBREVIATION_MODIFIER",
                description = "The \"abbreviation modifier\" grammatical relation. An abbreviation modifier of an NP is a parenthesized NP that serves to abbreviate the NP (or to define an abbreviation). Abbreviations are recognized either by being deemed proper nouns or by matching a regex pattern.",
                codedexamples = {"The Australian Broadcasting Corporation (ABC)", "abbrev(Corporation, ABC)"})
        MutableGrammaticalRelation ABBREVIATION_MODIFIER = new MutableGrammaticalRelation("ABBREVIATION_MODIFIER", "abbrev", "abbreviation modifier",
                APPOSITIONAL_MODIFIER,
                "(?:WH)?NP(?:-TMP|-ADV)?",
                new String[]{"/^(?:WH)?NP(?:-TMP|-ADV)?$/ < (PRN=target <, /^-LRB-$/ <- /^-RRB-$/ !<< /^(?:POS|(?:WP|PRP)\\$|[,$#]|CC|RB|CD)$/ <+(NP) (NNP|NN < /^(?:[A-Z]\\.?){2,}/) )"},
                "The \"abbreviation modifier\" grammatical relation. An abbreviation modifier of an NP is a parenthesized NP that serves to abbreviate the NP (or to define an abbreviation). Abbreviations are recognized either by being deemed proper nouns or by matching a regex pattern.",
                new String[][]{{"The Australian Broadcasting Corporation (ABC)", "abbrev(Corporation, ABC)"}});


        @GrammaticalRelationAnnotation(
                name = "PARTICIPIAL_MODIFIER",
                description = "The \"participial modifier\" grammatical relation. A participial modifier of an NP or VP (or S) is a VP[part] that serves to modify the meaning of the NP or VP.",
                codedexamples = {"truffles picked during the spring are tasty", "partmod(truffles, picked)", "Bill picked Fred for the team demonstrating his incompetence", "partmod(picked, demonstrating)"})
        MutableGrammaticalRelation PARTICIPIAL_MODIFIER = new MutableGrammaticalRelation("PARTICIPIAL_MODIFIER", "partmod", "participial modifier",
                MODIFIER,
                "(?:WH)?NP(?:-TMP|-ADV)?|VP|S|SINV",
                new String[]{"/^(?:WH)?NP(?:-TMP|-ADV)?$/ < (VP=target < VBG|VBN|VBD $-- NP)", "/^(?:WH)?NP(?:-TMP|-ADV)?$/ < (/^,$/ $+ (VP=target <, VBG|VBN))", "S|SINV < (S=target < (VP <, VBG|VBN) [ $- (/^,$/ [ $- @NP | $- (@PP $ @NP) ] ) | $+ (/^,$/ $+ @NP) ] )", "VP < (S=target < (VP <, VBG|VBN) $- (/^,$/ [ $- @NP | $- (@PP $ @NP) ] ) )"},
                "The \"participial modifier\" grammatical relation. A participial modifier of an NP or VP (or S) is a VP[part] that serves to modify the meaning of the NP or VP.",
                new String[][]{{"truffles picked during the spring are tasty", "partmod(truffles, picked)"}, {"Bill picked Fred for the team demonstrating his incompetence", "partmod(picked, demonstrating)"}});


        @GrammaticalRelationAnnotation(
                name = "INFINITIVAL_MODIFIER",
                description = "The \"infinitival modifier\" grammatical relation. An infinitival modifier of an NP is an S/VP that serves to modify the meaning of the NP.",
                codedexamples = {"points to establish are ...", "infmod(points, establish)"})
        MutableGrammaticalRelation INFINITIVAL_MODIFIER = new MutableGrammaticalRelation("INFINITIVAL_MODIFIER", "infmod", "infinitival modifier",
                MODIFIER,
                "NP(?:-TMP|-ADV)?",
                new String[]{"/^NP(?:-[A-Z]+)?$/ < (S=target < (VP < TO) $-- /^NP|NNP?S?$/)", "/^NP(?:-[A-Z]+)?$/ < (SBAR=target < (S < (VP < TO)) $-- /^NP|NNP?S?$/)"},
                "The \"infinitival modifier\" grammatical relation. An infinitival modifier of an NP is an S/VP that serves to modify the meaning of the NP.",
                new String[][]{{"points to establish are ...", "infmod(points, establish)"}});


        @GrammaticalRelationAnnotation(
                name = "ADVERBIAL_MODIFIER",
                description = "The \"adverbial modifier\" grammatical relation. An adverbial modifier of a word is a (non-clausal) RB or ADVP that serves to modify the meaning of the word.",
                codedexamples = {"genetically modified food", "advmod(modified, genetically)", "less often", "advmod(often, less)"})
        MutableGrammaticalRelation ADVERBIAL_MODIFIER = new MutableGrammaticalRelation("ADVERBIAL_MODIFIER", "advmod", "adverbial modifier",
                MODIFIER,
                "VP|ADJP|WHADJP|ADVP|WHADVP|S|SBAR|SINV|SQ|SBARQ|XS|(?:WH)?(?:PP|NP)(?:-TMP|-ADV)?|RRC|CONJP|JJP",
                new String[]{"/^(?:VP|ADJP|JJP|WHADJP|SQ?|SBARQ?|SINV|XS|RRC|(?:WH)?NP(?:-TMP|-ADV)?)$/ < (RB|RBR|RBS|WRB|ADVP|WHADVP=target !< /^(?i:n[o']?t|never)$/)", "ADVP|WHADVP < (RB|RBR|RBS|WRB|ADVP|WHADVP|JJ=target !< /^(?i:n[o']?t|never)$/) !< CC !< CONJP", "SBAR < (WHNP=target < WRB)", "SBARQ <, WHADVP=target", "XS < /^JJ$/=target", "/(?:WH)?PP(?:-TMP|-ADV)?$/ <# (__ $-- (RB|RBR|RBS|WRB|ADVP|WHADVP=target !< /^(?i:n[o']?t|never)$/))", "/(?:WH)?PP(?:-TMP|-ADV)?$/ < @NP|WHNP < (RB|RBR|RBS|WRB|ADVP|WHADVP=target !< /^(?i:n[o']?t|never)$/)", "CONJP < (RB=target !< /^(?i:n[o']?t|never)$/)"},
                "The \"adverbial modifier\" grammatical relation. An adverbial modifier of a word is a (non-clausal) RB or ADVP that serves to modify the meaning of the word.",
                new String[][]{{"genetically modified food", "advmod(modified, genetically)"}, {"less often", "advmod(often, less)"}});


        @GrammaticalRelationAnnotation(
                name = "NEGATION_MODIFIER",
                description = "The \"negation modifier\" grammatical relation. The negation modifier is the relation between a negation word and the word it modifies.",
                codedexamples = {"Bill is not a scientist", "neg(scientist, not)", "Bill doesn't drive", "neg(drive, n't)"})
        MutableGrammaticalRelation NEGATION_MODIFIER = new MutableGrammaticalRelation("NEGATION_MODIFIER", "neg", "negation modifier",
                ADVERBIAL_MODIFIER,
                "VP|ADJP|S|SBAR|SINV|SQ|NP(?:-TMP|-ADV)?|FRAG|CONJP|PP",
                new String[]{"/^(?:VP|NP(?:-TMP|-ADV)?|ADJP|SQ|S|FRAG|CONJP|PP)$/< (RB=target < /^(?i:n[o']?t|never)$/)", "VP|ADJP|S|SBAR|SINV|FRAG < (ADVP=target <# (RB < /^(?i:n[o']?t|never)$/))", "VP > SQ $-- (RB=target < /^(?i:n[o']?t|never)$/)"},
                "The \"negation modifier\" grammatical relation. The negation modifier is the relation between a negation word and the word it modifies.",
                new String[][]{{"Bill is not a scientist", "neg(scientist, not)"}, {"Bill doesn't drive", "neg(drive, n't)"}});


        @GrammaticalRelationAnnotation(
                name = "NP_ADVERBIAL_MODIFIER",
                description = "The \"noun phrase as adverbial modifier\" grammatical relation. This relation captures various places where something syntactically a noun phrase is used as an adverbial modifier in a sentence. These usages include: A measure phrase, which is the relation between the head of an ADJP/ADVP and the head of a measure-phrase modifying the ADJP/ADVP. Noun phrases giving extent inside a VP which are not objects Financial constructions involving an adverbial or PP-like NP, notably the following construction where the NP means \"per share\" Reflexives Certain other absolutive NP constructions. A temporal modifier (tmod) is a subclass of npadvmod which is distinguished as a separate relation.",
                codedexamples = {"The director is 65 years old", "npadvmod(old, years)", "Shares eased a fraction", "npadvmod(eased, fraction)", "IBM earned $ 5 a share", "npadvmod($, share)", "The silence is itself significant", "npadvmod(significant, itself)", "90% of Australians like him, the most of any country", "npadvmod(like, most)"})
        MutableGrammaticalRelation NP_ADVERBIAL_MODIFIER = new MutableGrammaticalRelation("NP_ADVERBIAL_MODIFIER", "npadvmod", "noun phrase adverbial modifier",
                MODIFIER,
                "VP|(?:WH)?(?:NP|ADJP|ADVP|PP)(?:-TMP|-ADV)?",
                new String[]{"@ADVP|ADJP|WHADJP|WHADVP|PP|WHPP <# (JJ|JJR|IN|RB|RBR !< notwithstanding $- (@NP=target !< NNP|NNPS))", "@NP|WHNP < /^NP-ADV/=target", "@NP <1 (@NP <<# /^%$/) <2 (@NP=target <<# days|month|months) !<3 __", "@VP < /^NP-ADV/=target"},
                "The \"noun phrase as adverbial modifier\" grammatical relation. This relation captures various places where something syntactically a noun phrase is used as an adverbial modifier in a sentence. These usages include: A measure phrase, which is the relation between the head of an ADJP/ADVP and the head of a measure-phrase modifying the ADJP/ADVP. Noun phrases giving extent inside a VP which are not objects Financial constructions involving an adverbial or PP-like NP, notably the following construction where the NP means \"per share\" Reflexives Certain other absolutive NP constructions. A temporal modifier (tmod) is a subclass of npadvmod which is distinguished as a separate relation.",
                new String[][]{{"The director is 65 years old", "npadvmod(old, years)"}, {"Shares eased a fraction", "npadvmod(eased, fraction)"}, {"IBM earned $ 5 a share", "npadvmod($, share)"}, {"The silence is itself significant", "npadvmod(significant, itself)"}, {"90% of Australians like him, the most of any country", "npadvmod(like, most)"}});


        @GrammaticalRelationAnnotation(
                name = "TEMPORAL_MODIFIER",
                description = "The \"temporal modifier\" grammatical relation. A temporal modifier of a VP or an ADJP is any constituent that serves to modify the meaning of the VP or the ADJP by specifying a time; a temporal modifier of a clause is an temporal modifier of the VP which is the predicate of that clause.",
                codedexamples = {"Last night, I swam in the pool", "tmod(swam, night)"})
        MutableGrammaticalRelation TEMPORAL_MODIFIER = new MutableGrammaticalRelation("TEMPORAL_MODIFIER", "tmod", "temporal modifier",
                NP_ADVERBIAL_MODIFIER,
                "VP|S|ADJP|PP|SBAR|SBARQ|NP",
                new String[]{"VP|ADJP < /^NP-TMP$/=target", "VP|ADJP < (NP=target < (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/) !$+ (/^JJ/ < old))", "@PP < (IN|TO|VBG|FW $++ (@NP $+ /^NP-TMP$/=target))", "@PP < (IN|TO|VBG|FW $++ (@NP $+ (NP=target < (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/))))", "S < (/^NP-TMP$/=target $++ VP [ $++ NP | $-- NP] )", "S < (NP=target < (/^NN/ < /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/) $++ (NP $++ VP))", "SBAR < (@WHADVP < (WRB < when)) < (S < (NP $+ (VP !< (/^(?:VB|AUX)/ < /^(?i:am|is|are|be|being|'s|'re|'m|was|were|been|s|ai|seem|seems|seemed|seeming|appear|appears|appeared|stay|stays|stayed|remain|remains|remained|resemble|resembles|resembled|resembling|become|becomes|became|becoming)$/ !$+ VP) ))) !$-- CC $-- NP > NP=target", "SBARQ < (@WHNP=target < (/^NN/ <# /^(?i:Mondays?|Tuesdays?|Wednesdays?|Thursdays?|Fridays?|Saturdays?|Sundays?|years?|months?|weeks?|days?|mornings?|evenings?|nights?|January|Jan\\.|February|Feb\\.|March|Mar\\.|April|Apr\\.|May|June|July|August|Aug\\.|September|Sept\\.|October|Oct\\.|November|Nov\\.|December|Dec\\.|today|yesterday|tomorrow|spring|summer|fall|autumn|winter)$/)) < (SQ < @NP)", "NP < NP-TMP=target"},
                "The \"temporal modifier\" grammatical relation. A temporal modifier of a VP or an ADJP is any constituent that serves to modify the meaning of the VP or the ADJP by specifying a time; a temporal modifier of a clause is an temporal modifier of the VP which is the predicate of that clause.",
                new String[][]{{"Last night, I swam in the pool", "tmod(swam, night)"}});


        @GrammaticalRelationAnnotation(
                name = "MULTI_WORD_EXPRESSION",
                description = "The \"multi-word expression\" grammatical relation. This covers various multi-word constructions for which it would seem pointless or arbitrary to claim grammatical relations between words: as well as, rather than, instead of, but also; such as, because of, all but, in addition to .... <code>mwe</code>(well, as) \"fewer than 700 bottles\" &rarr; <code>mwe</code>(than, fewer)",
                codedexamples = {"dogs as well as cats", "mwe(well, as)"})
        MutableGrammaticalRelation MULTI_WORD_EXPRESSION = new MutableGrammaticalRelation("MULTI_WORD_EXPRESSION", "mwe", "multi-word expression",
                MODIFIER,
                "PP|XS|ADVP|CONJP",
                new String[]{"PP|XS < (IN|TO < as|of|at|to|in) < (JJ|IN|JJR|JJS|NN=target < such|because|Because|least|instead|due|Due|addition|to)", "ADVP < (RB|IN < well) < (IN|RB|JJS=target < as)", "ADVP < (DT < all) < (CC < but)", "CONJP < (RB < rather|well|instead) < (RB|IN=target < as|than|of)", "CONJP < (IN < in) < (NN|TO=target < addition|to)", "XS < JJR|JJS=target"},
                "The \"multi-word expression\" grammatical relation. This covers various multi-word constructions for which it would seem pointless or arbitrary to claim grammatical relations between words: as well as, rather than, instead of, but also; such as, because of, all but, in addition to .... <code>mwe</code>(well, as) \"fewer than 700 bottles\" &rarr; <code>mwe</code>(than, fewer)",
                new String[][]{{"dogs as well as cats", "mwe(well, as)"}});


        @GrammaticalRelationAnnotation(
                name = "DETERMINER",
                description = "The \"determiner\" grammatical relation.",
                codedexamples = {"The man is here", "det(man,the)", "Which man do you prefer?", "det(man,which)"})
        MutableGrammaticalRelation DETERMINER = new MutableGrammaticalRelation("DETERMINER", "det", "determiner",
                MODIFIER,
                "(?:WH)?NP(?:-TMP|-ADV)?|NAC|NML|NX|X",
                new String[]{"/^(?:NP(?:-TMP|-ADV)?|NAC|NML|NX|X)$/ < (DT=target !< /both|either|neither/ !$+ DT !$++ CC $++ /^(?:N[MNXP]|CD|JJ|FW|ADJP|QP|RB|PRP|PRN)/)", "/^NP(?:-TMP|-ADV)?$/ < (DT=target < /both|either|neither/ !$+ DT !$++ CC $++ /^(?:NN|NX|NML)/ !$++ (NP < CC))", "/^NP(?:-TMP|-ADV)?$/ < (DT=target !< /both|neither|either/ $++ CC $++ /^(?:NN|NX|NML)/)", "/^NP(?:-TMP|-ADV)?$/ < (DT=target $++ (/^JJ/ !$+ /^NN/) !$++CC !$+ DT)", "/^NP(?:-TMP|-ADV)?$/ < (RB=target $++ (/PDT/ $+ /^NN/))", "/^NP(?:-TMP|-ADV)?$/ <<, PRP <- (NP|DT|RB=target <<- all|both|each)", "WHNP < (NP $-- (WHNP=target < WDT))", "WHNP < (/^(?:NP|NN|CD)/ $-- (DT|WDT|WP=target))"},
                "The \"determiner\" grammatical relation.",
                new String[][]{{"The man is here", "det(man,the)"}, {"Which man do you prefer?", "det(man,which)"}});


        @GrammaticalRelationAnnotation(
                name = "PREDETERMINER",
                description = "The \"predeterminer\" grammatical relation.",
                codedexamples = {"All the boys are here", "predet(boys,all)"})
        MutableGrammaticalRelation PREDETERMINER = new MutableGrammaticalRelation("PREDETERMINER", "predet", "predeterminer",
                MODIFIER,
                "(?:WH)?(?:NP|NX|NAC|NML)(?:-TMP|-ADV)?",
                new String[]{"/^(?:(?:WH)?NP(?:-TMP|-ADV)?|NX|NAC|NML)$/ < (PDT|DT=target $+ /^(?:DT|WP\\$|PRP\\$)$/ $++ /^N[NXM]/ !$++ CC)", "/^(?:WH)?NP(?:-TMP|-ADV)?$/ < (PDT|DT=target $+ DT $++ (/^JJ/ !$+ /^NN/)) !$++ CC", "/^(?:WH)?NP(?:-TMP|-ADV)?$/ < PDT=target <- DT"},
                "The \"predeterminer\" grammatical relation.",
                new String[][]{{"All the boys are here", "predet(boys,all)"}});


        @GrammaticalRelationAnnotation(
                name = "PRECONJUNCT",
                description = "The \"preconjunct\" grammatical relation.",
                codedexamples = {"Both the boys and the girls are here", "preconj(boys,both)"})
        MutableGrammaticalRelation PRECONJUNCT = new MutableGrammaticalRelation("PRECONJUNCT", "preconj", "preconjunct",
                MODIFIER,
                "S|VP|ADJP|PP|ADVP|UCP|NX|NML|SBAR|NP(?:-TMP|-ADV)?",
                new String[]{"/^(?:NP(?:-TMP|-ADV)?|NX|NML)$/ < (PDT|CC|DT=target < /^(?i:both|neither|either)$/ $++ CC)", "/^(?:NP(?:-TMP|-ADV)?|NX|NML)$/ < (CONJP=target < (RB < /^(?i:not)$/) < (RB|JJ < /^(?i:only|merely|just)$/) $++ CC|CONJP)", "/^(?:NP(?:-TMP|-ADV)?|NX|NML)$/ < (PDT|CC|DT=target < /^(?i:both|neither|either)$/) < (NP < CC)", "S|VP|ADJP|PP|ADVP|UCP|NX|NML|SBAR < (PDT|DT|CC=target < /^(?i:both|neither|either)$/ $++ CC)", "S|VP|ADJP|PP|ADVP|UCP|NX|NML|SBAR < (CONJP=target < (RB < /^(?i:not)$/) < (RB|JJ < /^(?i:only|merely|just)$/) $++ CC|CONJP)"},
                "The \"preconjunct\" grammatical relation.",
                new String[][]{{"Both the boys and the girls are here", "preconj(boys,both)"}});


        @GrammaticalRelationAnnotation(
                name = "POSSESSION_MODIFIER",
                description = "The \"possession\" grammatical relation between the possessum and the possessor. Examples: <br/> \"their offices\" &rarr; <code>poss</code>(offices, their)<br/> \"Bill 's clothes\" &rarr; <code>poss</code>(clothes, Bill)",
                codedexamples = {})
        MutableGrammaticalRelation POSSESSION_MODIFIER = new MutableGrammaticalRelation("POSSESSION_MODIFIER", "poss", "possession modifier",
                MODIFIER,
                "(?:WH)?(NP|ADJP|INTJ|PRN|NAC|NX|NML)(?:-TMP|-ADV)?",
                new String[]{"/^(?:WH)?(?:ADJP|NP|INTJ|PRN|NAC|NX|NML)(?:-TMP|-ADV)?$/ < /^(?:W|PR)P\\$$/=target", "/^(?:WH)?(?:NP|NML)(?:-TMP|-ADV)?$/ [ < (/^(?:WH)?(?:NP|NML)$/=target [ < POS | < (VBZ < /^'s$/) ] ) !< (CC|CONJP $++ /^(?:WH)?(?:NP|NML)$/) |  < (/^(?:WH)?(?:NP|NML)$/=target < (CC|CONJP $++ /^(?:WH)?(?:NP|NML)$/) < (/^(?:WH)?(?:NP|NML)$/ [ < POS | < (VBZ < /^'s$/) ] )) ]", "/^(?:WH)?(?:NP|NML)(?:-TMP|-ADV)?$/ < (/^NN/=target $+ (POS < /'/ $++ /^NN/))"},
                "The \"possession\" grammatical relation between the possessum and the possessor. Examples: <br/> \"their offices\" &rarr; <code>poss</code>(offices, their)<br/> \"Bill 's clothes\" &rarr; <code>poss</code>(clothes, Bill)",
                new String[][]{});


        @GrammaticalRelationAnnotation(
                name = "POSSESSIVE_MODIFIER",
                description = "The \"possessive\" grammatical relation. This is the relation given to 's (or ' with plurals). Example: <br/> \"John's book\" &rarr; <code>possessive</code>(John, 's)",
                codedexamples = {})
        MutableGrammaticalRelation POSSESSIVE_MODIFIER = new MutableGrammaticalRelation("POSSESSIVE_MODIFIER", "possessive", "possessive modifier",
                MODIFIER,
                "(?:WH)?(?:NP|NML)(?:-TMP|-ADV)?",
                new String[]{"/^(?:WH)?(?:NP|NML)(?:-TMP|-ADV)?$/ < POS=target", "/^(?:WH)?(?:NP|NML)(?:-TMP|-ADV)?$/ < (VBZ=target < /^'s$/)"},
                "The \"possessive\" grammatical relation. This is the relation given to 's (or ' with plurals). Example: <br/> \"John's book\" &rarr; <code>possessive</code>(John, 's)",
                new String[][]{});


        @GrammaticalRelationAnnotation(
                name = "PREPOSITIONAL_MODIFIER",
                description = "The \"prepositional modifier\" grammatical relation. A prepositional modifier of a verb, adjective, or noun is any prepositional phrase that serves to modify the meaning of the verb, adjective, or noun. We also generate prep modifiers of PPs to account for treebank (PP PP PP) constructions (from 1984 through 2002).",
                codedexamples = {"I saw a cat in a hat", "prep(cat, in)", "I saw a cat with a telescope", "prep(saw, with)", "He is responsible for meals", "prep(responsible, for)"})
        MutableGrammaticalRelation PREPOSITIONAL_MODIFIER = new MutableGrammaticalRelation("PREPOSITIONAL_MODIFIER", "prep", "prepositional modifier",
                MODIFIER,
                "(?:WH)?(?:NP|PP|ADJP|ADVP|NX|NML)(?:-TMP|-ADV)?|VP|S|SINV|SQ|SBARQ|NAC",
                new String[]{"/^(?:(?:WH)?(?:NP|ADJP|ADVP|NX|NML)(?:-TMP|-ADV)?|VP|NAC|SQ)$/ < /^(?:WH)?PP(?:-TMP)?$/=target", "/^(?:WH)?PP(?:-TMP|-ADV)?$/ < /^(?:WH)?PP(?:-TMP|-ADV)?$/=target < @NP|PP !< @CC|CONJP", "S|SINV < (/^PP(?:-TMP)?$/=target !< SBAR) < VP|S", "SBARQ < /^(?:WH)?PP/=target < SQ"},
                "The \"prepositional modifier\" grammatical relation. A prepositional modifier of a verb, adjective, or noun is any prepositional phrase that serves to modify the meaning of the verb, adjective, or noun. We also generate prep modifiers of PPs to account for treebank (PP PP PP) constructions (from 1984 through 2002).",
                new String[][]{{"I saw a cat in a hat", "prep(cat, in)"}, {"I saw a cat with a telescope", "prep(saw, with)"}, {"He is responsible for meals", "prep(responsible, for)"}});


        @GrammaticalRelationAnnotation(
                name = "PHRASAL_VERB_PARTICLE",
                description = "The \"phrasal verb particle\" grammatical relation. The \"phrasal verb particle\" relation identifies phrasal verb.",
                codedexamples = {"They shut down the station.", "prt(shut, down)"})
        MutableGrammaticalRelation PHRASAL_VERB_PARTICLE = new MutableGrammaticalRelation("PHRASAL_VERB_PARTICLE", "prt", "phrasal verb particle",
                MODIFIER,
                "VP",
                new String[]{"VP < PRT=target"},
                "The \"phrasal verb particle\" grammatical relation. The \"phrasal verb particle\" relation identifies phrasal verb.",
                new String[][]{{"They shut down the station.", "prt(shut, down)"}});


        @GrammaticalRelationAnnotation(
                name = "PARATAXIS",
                description = "The \"parataxis\" grammatical relation. Relation between the main verb of a sentence and other sentential elements, such as a sentential parenthetical, a sentence after a \":\" or a \";\" etc.",
                codedexamples = {"The guy, John said, left early in the morning.", "parataxis(left,said)"})
        MutableGrammaticalRelation PARATAXIS = new MutableGrammaticalRelation("PARATAXIS", "parataxis", "parataxis",
                DEPENDENT,
                "S|VP",
                new String[]{"VP < (PRN=target < /^(?:S|SINV|SBAR)$/)", "VP $ (PRN=target [ < S|SINV|SBAR | < VP < @NP ] )", "S|VP < (/^:$/ $+ /^S/=target)"},
                "The \"parataxis\" grammatical relation. Relation between the main verb of a sentence and other sentential elements, such as a sentential parenthetical, a sentence after a \":\" or a \";\" etc.",
                new String[][]{{"The guy, John said, left early in the morning.", "parataxis(left,said)"}});


        @GrammaticalRelationAnnotation(
                name = "SEMANTIC_DEPENDENT",
                description = "The \"semantic dependent\" grammatical relation has been introduced as a supertype for the controlling subject relation.",
                codedexamples = {})
        MutableGrammaticalRelation SEMANTIC_DEPENDENT = new MutableGrammaticalRelation("SEMANTIC_DEPENDENT", "sdep", "semantic dependent",
                DEPENDENT,
                null,
                new String[]{},
                "The \"semantic dependent\" grammatical relation has been introduced as a supertype for the controlling subject relation.",
                new String[][]{});


        @GrammaticalRelationAnnotation(
                name = "CONTROLLING_SUBJECT",
                description = "The \"controlling subject\" grammatical relation. A controlling subject is the relation between the head of an xcomp and the external subject of that clause.",
                codedexamples = {"Tom likes to eat fish", "xsubj(eat, Tom)"})
        MutableGrammaticalRelation CONTROLLING_SUBJECT = new MutableGrammaticalRelation("CONTROLLING_SUBJECT", "xsubj", "controlling subject",
                SEMANTIC_DEPENDENT,
                "VP",
                new String[]{"VP < TO > (S !$- NP !< NP !>> (VP < (VB|AUX < be)) >+(VP) (VP $-- NP=target))", "VP < (/^(?i:stop|stops|stopped|stopping|keep|keeps|kept|keeping)$/ $+ (VP=target < VBG))"},
                "The \"controlling subject\" grammatical relation. A controlling subject is the relation between the head of an xcomp and the external subject of that clause.",
                new String[][]{{"Tom likes to eat fish", "xsubj(eat, Tom)"}});


        @GrammaticalRelationAnnotation(
                name = "AGENT",
                description = "The \"agent\" grammatical relation. The agent of a passive VP is the complement introduced by \"by\" and doing the action.",
                codedexamples = {"The man has been killed by the police", "agent(killed, police)"})
        MutableGrammaticalRelation AGENT = new MutableGrammaticalRelation("AGENT", "agent", "agent",
                DEPENDENT,
                null,
                new String[]{},
                "The \"agent\" grammatical relation. The agent of a passive VP is the complement introduced by \"by\" and doing the action.",
                new String[][]{{"The man has been killed by the police", "agent(killed, police)"}});


        MutableGrammaticalRelation[] theseRelations = {
                GOVERNOR,
                DEPENDENT,
                ROOT,
                KILL,
                PREDICATE,
                AUX_MODIFIER,
                AUX_PASSIVE_MODIFIER,
                COPULA,
                CONJUNCT,
                COORDINATION,
                PUNCTUATION,
                ARGUMENT,
                SUBJECT,
                NOMINAL_SUBJECT,
                NOMINAL_PASSIVE_SUBJECT,
                CLAUSAL_SUBJECT,
                CLAUSAL_PASSIVE_SUBJECT,
                COMPLEMENT,
                OBJECT,
                DIRECT_OBJECT,
                INDIRECT_OBJECT,
                PREPOSITIONAL_OBJECT,
                PREPOSITIONAL_COMPLEMENT,
                ATTRIBUTIVE,
                CLAUSAL_COMPLEMENT,
                XCLAUSAL_COMPLEMENT,
                COMPLEMENTIZER,
                MARKER,
                RELATIVE,
                REFERENT,
                EXPLETIVE,
                ADJECTIVAL_COMPLEMENT,
                MODIFIER,
                ADV_CLAUSE_MODIFIER,
                PURPOSE_CLAUSE_MODIFIER,
                RELATIVE_CLAUSE_MODIFIER,
                ADJECTIVAL_MODIFIER,
                NUMERIC_MODIFIER,
                NUMBER_MODIFIER,
                QUANTIFIER_MODIFIER,
                NOUN_COMPOUND_MODIFIER,
                APPOSITIONAL_MODIFIER,
                ABBREVIATION_MODIFIER,
                PARTICIPIAL_MODIFIER,
                INFINITIVAL_MODIFIER,
                ADVERBIAL_MODIFIER,
                NEGATION_MODIFIER,
                NP_ADVERBIAL_MODIFIER,
                TEMPORAL_MODIFIER,
                MULTI_WORD_EXPRESSION,
                DETERMINER,
                PREDETERMINER,
                PRECONJUNCT,
                POSSESSION_MODIFIER,
                POSSESSIVE_MODIFIER,
                PREPOSITIONAL_MODIFIER,
                PHRASAL_VERB_PARTICLE,
                PARATAXIS,
                SEMANTIC_DEPENDENT,
                CONTROLLING_SUBJECT,
                AGENT,
        };
        return new RelationModel(DEPENDENT, theseRelations);
    }
}
