package sesa.common.builder.semanticRule;

import edu.stanford.nlp.trees.Tree;
import sesa.common.semantics.SemanticGroup;
import sesa.common.semantics.SemanticRelation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * This class represents Grammar Rules and Structure Rules for Semantic group
 */

public class RulesForSemanticGroup {

    /**
     * Clear semantic groups
     * Apply grammar rules (GR)
     * 1. You can not add to the grammatical relations group "pobj", if it already is present "dobj".
     * 2. You can not add a group of grammatical relation "prep" or "conj", if it already is present nsubj.
     * @param groups -
     */
    public static void clearSemanticGroups(LinkedList<SemanticGroup> groups) {
        for (SemanticGroup group : groups)
            clearSemanticGroup(group);
    }

    /**
     * Clear semantic group
     *
     * @param group
     */
    private static void clearSemanticGroup(SemanticGroup group) {

        List<SemanticRelation> relationsToDelete = new LinkedList<SemanticRelation>();

        for (SemanticRelation relation : group.getRelations()) {
            if (!isRelationValid(group, relation)) {
                relationsToDelete.add(relation);
            }
        }
        group.removeAllRelations(relationsToDelete);
    }

    /**
     * Use Grammar rules for check: can we add to group grammatical relation
     * @param group
     * @param relation
     * @return
     */
    public static boolean isRelationValid(SemanticGroup group, SemanticRelation relation) {

        // String shotName = relation.getShortName();

        List<SemanticRule> rules = new LinkedList<SemanticRule>();

        SemanticRule rule5 = new SemanticRule("pobj", new String[]{}, new String[]{"dobj"});
        rules.add(rule5);

        SemanticRule rule6 = new SemanticRule("prep", new String[]{}, new String[]{"nsubj"});
        rules.add(rule6);

        SemanticRule rule7 = new SemanticRule("conj", new String[]{}, new String[]{"nsubj"});
        rules.add(rule7);

        return validateRules(rules, group, relation);
    }

    private static boolean validateRules(List<SemanticRule> rules, SemanticGroup group, SemanticRelation relation) {
        for (SemanticRule rule : rules) {
            if (!rule.isRelationValidForGroup(group, relation))
                return false;
        }
        return true;
    }

    public static Tree checkRelationNode(Tree root, int relationNodeHash) {

        Tree result = null;
        //if (relationNode.indexOf(root) == -1) {
        for (Tree tree : root) {
            if (relationNodeHash == tree.hashCode()) {
                result = tree;
                break;
            }
        }
        //}
        return result;
    }

    public static Tree findSemanticRoot(Tree root, Tree node) {
        while ((node = node.parent(root)) != null) {
            String value = node.label().value();
            if (value.equals("S") || value.equals("ROOT")) //  1-2 rule
                return node;
            else if (value.equals("VP") && checkVerbPhraseRoot(root, node))
                return node;
        }
        return null;
    }

    private static boolean checkVerbPhraseRoot(Tree root, Tree node) {           // 3 rule

        Tree parent = node.parent(root);
        String parentValue = parent.label().value();

        if (parent == null)
            return false;

        Tree firstNPVPNode = null;
        for (Tree child : parent.children()) {
            String value = child.label().value();
            boolean npvp = value.equals("VP") || value.equals("NP");    // 4 rule
            if (npvp && !parentValue.equals("S")) {      // 5 rule
                firstNPVPNode = child;
                break;
            }
        }

        return firstNPVPNode != null && firstNPVPNode != node;
    }

    private void setPPSkips(Tree root, Tree node, HashSet<Tree> skipNodes) {         // rule  8

        Tree parent = node.parent();
        if (parent == null)
            return;

        if (parent.label().value().equals("PP")) {

            Tree second = parent.getChild(1);
            if (second == null)
                return;

            Tree subSecond = second.getChild(1);

            for (int i = 0; i < subSecond.children().length; i++) {
                if (subSecond.getChild(i).label().value() != "S") {
                    skipNodes.add(subSecond.getChild(i));
                }
            }
        }
    }
}
