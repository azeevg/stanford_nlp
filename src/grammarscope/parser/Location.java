package grammarscope.parser;

/**
 * Location (pair of from- and to-indices). Indices refer to nth character in text.
 * 
 * @author Bernard Bou
 */
public class Location
{
	/**
	 * From index
	 */
	public int from;

	/**
	 * To index
	 */
	public int to;

	/**
	 * Default constructor
	 */
	public Location()
	{
		this(0, 0);
	}

	/**
	 * Constructor
	 * 
	 * @param thisFrom
	 *            from index
	 * @param thisTo
	 *            to index
	 */
	public Location(final int thisFrom, final int thisTo)
	{
		this.from = thisFrom;
		this.to = thisTo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[" + this.from + "-" + this.to + "]";
	}
}