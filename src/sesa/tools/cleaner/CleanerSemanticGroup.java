package sesa.tools.cleaner;

/**
 * Created by Ira on 24.05.2016.
 */

import edu.spbstu.appmath.collocmatch.Collocation;
import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import sesa.common.database.dao.DocumentsDao;
import sesa.common.database.dao.SemanticGroupDao;
import sesa.common.database.dao.dictionary.TermDao;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.collocation.CollocationUtility;
import sesa.tools.statistics.RawSemanticGroup;
import sesa.tools.statistics.RawSemanticRelation;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.*;

/**
 * In that class we clean semantic groups
 * delete not relevant
 */
public class CleanerSemanticGroup {
    public final Logger log = LogManager.getLogger(CleanerSemanticGroup.class);
    private CollocationDictionary dictionary = CollocationUtility.initCollocationDictionary();

    public List<String> readSemanticGroupFromFile() {
        FileManager fileManager = new FileManager("path");
        return FileReaderWriter.read(fileManager.getValue("path.semanticGroup"));
    }

    private Set<String> getCollocationForParseTree() {

        Set<String> collocationForParseTree = new HashSet<String>();
        for (Collocation collocation : dictionary) {
            StringBuilder sb = new StringBuilder();
            for (String w : collocation.words()) {
                sb.append(w);
            }
            collocationForParseTree.add(sb.toString());
        }

        return collocationForParseTree;
    }

    public int findCountRelevantGroup(List<String> groups) throws SQLException, ClassNotFoundException {
        List<String> relevantGroup = new ArrayList<String>();
        int relevantGroups = 0;
        DocumentsDao dao = new DocumentsDao();
        for (String group : groups) {
            if (group.contains(" ")) {
                int count = dao.fullTextSearch(group, "w_dbch_computing");
                if (count > 0) {
                    log.info("group - count: " + group + " - " + count);
                    relevantGroup.add(group);
                }
            }

        }
       // FileReaderWriter.write("cleanSemanticGroup.txt", relevantGroup);
        return relevantGroups;
    }

    public static void getSemanticRelationCollocationFromDataBase() throws SQLException, ClassNotFoundException {
        SemanticGroupDao dao = new SemanticGroupDao();
        List<String> list = new ArrayList<String>();
        Collection<RawSemanticGroup> groups = dao.getSemanticGroupsWithCollocation("semantic_groups_computer_dict");

        for (RawSemanticGroup group : groups) {
            boolean added = false;
            StringBuilder sb = new StringBuilder();
            for (RawSemanticRelation relation : group.getRelations()) {
                sb.append(relation.getToken() + " ");
                if (relation.getPos().contains("COLLOCATION")) {
                    added = true;
                }
            }
            if (added) {
                list.add(sb.toString());
            }
        }
        FileReaderWriter.write("data/semanticGroupCollocation.txt", list);
    }

    public static List<String> getSemanticRelationFromDataBase(int size) throws SQLException, ClassNotFoundException {
        SemanticGroupDao dao = new SemanticGroupDao();
        List<String> list = new ArrayList<String>();
        Collection<RawSemanticGroup> groups = dao.getSemanticGroupsWithCollocation("semantic_groups_computer_dict");

        for (RawSemanticGroup group : groups) {
            boolean added = true;
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (RawSemanticRelation relation : group.getRelations()) {
                i++;
                sb.append(relation.getToken() + " ");
                if (relation.getPos().contains("COLLOCATION")) {
                    added = false;
                }
            }

            if (added && (i == size)) {
                list.add(sb.toString());
            }
        }
        System.out.println(list.size());
        return list;
    }

    public int findCountRelevantGroup2(List<String> groups, int start, int end) throws SQLException, ClassNotFoundException {
        int relevantGroups = 0;
        DocumentsDao dao = new DocumentsDao();
        Set<String> relevantGroup = dao.fullTextSearch2(groups, "w_dbch_computing", start, end);
        FileReaderWriter.write("cleanSemanticGroup.txt", relevantGroup);
        return relevantGroups;
    }


    public static void clean(String inputFile, String outputFile, int numWords) throws SQLException, ClassNotFoundException {
        int i = 0;
        TermDao dao = new TermDao();
        Set<String> terms = dao.getAllTerms().keySet();
        Set<String> groups = new HashSet<String>();
        List<String> stopWords = new ArrayList<String>();
        for (String term : terms) {
            if (term.split(" ").length == numWords) {
                stopWords.add(term);
            }
        }

        try {
            Scanner in = new Scanner(new File(inputFile));
            while (in.hasNextLine()) {
                String s = in.nextLine();
                if (!stopWords.contains(s.toLowerCase())) {
                    groups.add(s);
                } else {
                    System.out.println(s);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(groups.size());
        FileReaderWriter.write(outputFile, groups);
    }


}
