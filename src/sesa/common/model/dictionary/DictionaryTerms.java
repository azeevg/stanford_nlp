package sesa.common.model.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ira on 13.05.2016.
 */
public class DictionaryTerms {

    private List<Term> terms = new ArrayList<Term>();
    private HashMap<String, Integer> terms_values = new HashMap<String, Integer>(); // ��� ������� � ���������� ������ � ����

    public DictionaryTerms() {

    }

    public int getSize(){
        return terms.size();
    }

    public void addTerm(Term word) {
        int value = 1;
        if (terms_values.containsKey(word.getName())) {
            value = terms_values.get(word.getName());
            value++;
        }
        terms_values.put(word.getName(), value);
        terms.add(word);


    }

    public List<String> getAllTermDef(String term_name){
        List<String> def = new ArrayList<String>();
        if(terms_values.containsKey(term_name)){
            for(Term term: terms){
                if(term.getName().equals(term_name)){
                    def.add(term.getDef());
                }
            }
        }else{
            return null;
        }
        return def;
    }

    public List<Term> getTerms() {
        return terms;
    }
}
