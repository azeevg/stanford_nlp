package sesa.common.semantics;

/**
 * Created by IntelliJ IDEA.
 * User: ashishagin
 * Date: 1/22/13
 * Time: 6:28 PM
 * To change this template use File | Settings | File Templates.
 */

public class RelationInfo {

    private String token;
    private String tokenBase;
    private String relation;
    private int hash;
    private int id;
    private String sentence;
    private String title;

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "RelationInfo{" +
                "t='" + token + '\'' +
                ", tb='" + tokenBase + '\'' +
                ", r='" + relation + '\'' +
                ", h=" + hash +
                ", id=" + id +
                ", s='" + sentence + '\'' +
                '}';
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenBase() {
        return tokenBase;
    }

    public void setTokenBase(String tokenBase) {
        this.tokenBase = tokenBase;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
