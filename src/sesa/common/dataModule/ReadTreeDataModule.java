package sesa.common.dataModule;


import grammarscope.parser.Sentence;
import sesa.common.builder.BuilderException;
import sesa.common.database.dao.TreeCacheDao;

import java.sql.SQLException;
import java.util.List;

/**
 * Extract parse trees from database
 */
public class ReadTreeDataModule extends BasicDataModule {

    protected String cacheTable;
    protected TreeCacheDao cacheDao;
   // protected DBController controller;

    /**
     * @param cacheTable - DB table where cache of trees (trees of parsed sentences) resides
     */
    public ReadTreeDataModule(String cacheTable) throws SQLException, ClassNotFoundException {
        cacheDao = new TreeCacheDao();
        this.cacheTable = cacheTable;

    }

    /**
     * Read tree from DataBase and put to treeMap
     * @param title
     * @param sentences list of sentences
     * @throws Exception
     */
    @Override
    public void readTreeCache(String title, List<Sentence> sentences, boolean isModCKY) throws Exception {
        if (treeMap == null) {
            //TODO!!! Here we can gat empty HashMap treeMap! We have to refactor getCachedTrees and this snippet as well!
            treeMap = cacheDao.getCachedTrees(cacheTable, title.hashCode());
            if (treeMap == null || treeMap.size() == 0) {
                // TODO: Maybe here we must use method from BasicBuilder, parse list of sentence
               // super.buildTreeCache(title,sentences,isModCKY);
                throw new BuilderException("Can't load trees for");
            }
        }
        //TODO!!! parse() implementation??? What does that "parse" do into readTreeCache() function? We have merely retrieve trees from dataModule! That's all!

        /**
         * Its implementation assumes: we will take trees from scratch into parse()-code in case we can't retrieve cached trees from DB
         * i.e. the treeMap will be populated into document.parse(parser) by parser
         * */
        document.parse(parser);
    }




}
