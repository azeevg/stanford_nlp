package grammarscope.parser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.BasicDocument;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.Options;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.process.WordToSentenceProcessor;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.ParseException;
//import edu.stanford.nlp.web.HTMLParser;

/**
 * Parser entity
 * 
 * @author Bernard Bou
 */
public class Parser
{
	/**
	 * Stanford Lexicalized Parser
	 */
	protected LexicalizedParser theParser;

	// C O N S T R U C T

	/**
	 * Constructor
	 * 
	 * @param thisInputStream
	 *            object stream for serialized grammar file
	 */
	public Parser(final ObjectInputStream thisInputStream)
	{
		doLoad(thisInputStream);
	}

	/**
	 * Constructor with options
	 * 
	 * @param thisInputStream
	 *            object stream for serialized grammar file
	 * @param flags
	 *            flags
	 */
	public Parser(final ObjectInputStream thisInputStream, final String... flags)
	{
		this(thisInputStream);
		if (isValid())
		{
			// do it individually
			for (final String flag : flags)
			{
				try
				{
					this.theParser.getOp().setOptions(flag);
				}
				catch (final IllegalArgumentException e)
				{
					System.err.println("While constructing parser: " + e.toString());
				}
			}
		}
	}

	// S Y N C H R O N I Z E D J O B S

	/**
	 * Load a grammar
	 * 
	 * @param thisInputStream
	 *            serialized grammar file input stream
	 */
	synchronized private void doLoad(final ObjectInputStream thisInputStream)
	{
		this.theParser = Parser.load(thisInputStream);
	}

	/**
	 * Parse document
	 * 
	 * @param thisDocumentPath
	 *            document path
	 */
	synchronized private List<Tree> doParse(final String thisDocumentPath)
	{
		return parseDocument(thisDocumentPath);
	}

	// A C T I O N S

	/**
	 * Load the grammar
	 * 
	 * @param thisInputStream
	 *            serialized grammar file input stream
	 * @return LexicalizedParser
	 */
	static protected LexicalizedParser load(final ObjectInputStream thisInputStream)
	{
		try
		{
			final LexicalizedParser thisParser = LexicalizedParser.loadModel(thisInputStream);
			return thisParser;
		}
		catch (final Exception e)
		{
			System.out.println(e.toString());
			return null;
		}
	}

	/**
	 * Parse
	 * 
	 * @param thisDocumentPath
	 *            document path
	 */
	private List<Tree> parse(final String thisDocumentPath)
	{
		return doParse(thisDocumentPath);
	}

	// D O C U M E N T F A C T O R Y

	/**
	 * Extract text from HTML
	 * 
	 * @param thisUrl
	 *            URL
	 * @return text
	 * @throws IOException
	 */
	/*static private String html2text(final URL thisUrl) throws IOException
	{
		final HTMLParser thisHMLParser = new HTMLParser();
		return thisHMLParser.parse(thisUrl);
	} */

	/**
	 * Extract text from HTML
	 * 
	 * @param thisFile
	 *            file
	 * @return text
	 * @throws IOException
	 */
	/*static private String html2text(final File thisFile) throws IOException
	{
		final HTMLParser thisHMLParser = new HTMLParser();
		return thisHMLParser.parse(new FileReader(thisFile));
	} */

	/**
	 * Get document from text
	 * 
	 * @param thisText
	 *            text
	 * @return document
	 */
	static public BasicDocument<HasWord> getDocument(final String thisText)
	{
		final BasicDocument<HasWord> thisDoc = BasicDocument.init(thisText);
		return thisDoc;
	}

	/**
	 * Get document from URL
	 * 
	 * @param thisUrl
	 *            URL
	 * @return document
	 * @throws IOException
	 */
	/*static public BasicDocument<HasWord> getDocument(final URL thisUrl) throws IOException
	{
		final String thisFilename = thisUrl.getFile();
		if (thisFilename.endsWith(".html") || thisFilename.endsWith(".htm"))
		{
			final String thisText = Parser.html2text(thisUrl);
			return Parser.getDocument(thisText);
		}
		final BasicDocument<HasWord> thisDoc = new BasicDocument<HasWord>().init(thisUrl);
		return thisDoc;
	} */

	/**
	 * Get document from file
	 * 
	 * @param thisFile
	 *            file
	 * @return document
	 * @throws IOException
	 */
	/*static public BasicDocument<HasWord> getDocument(final File thisFile) throws IOException
	{
		final String thisFilename = thisFile.getName();
		if (thisFilename.endsWith(".html") || thisFilename.endsWith(".htm"))
		{
			final String thisText = Parser.html2text(thisFile);
			return Parser.getDocument(thisText);
		}
		final BasicDocument<HasWord> thisDoc = new BasicDocument<HasWord>().init(thisFile);
		return thisDoc;
	} */

	// S E N T E N C E S

	/**
	 * Get sentences from URL
	 * 
	 * @param thisUrl
	 *            URL
	 * @return list of sentences
	 * @throws IOException
	 */
	/*static public List<Sentence> getSentences(final URL thisUrl) throws IOException
	{
		return Parser.getSentences(Parser.getDocument(thisUrl));
	} */

	/**
	 * Get sentences from file
	 * 
	 * @param thisFile
	 *            file
	 * @return list of sentences
	 * @throws IOException
	 */
	/*static public List<Sentence> getSentences(final File thisFile) throws IOException
	{
		return Parser.getSentences(Parser.getDocument(thisFile));
	} */

	/**
	 * Get sentences from text
	 * 
	 * @param thisText
	 *            texttheParser
	 * @return list of sentences
	 */
	static public List<Sentence> getSentences(final String thisText)
	{
		return Parser.getSentences(Parser.getDocument(thisText));
	}

	/**
	 * Get sentences from words
	 * 
	 * @param theseWords
	 *            words
	 * @return list of sentences
	 */
	static public List<Sentence> getSentences(final List<Word> theseWords)
	{
		final WordToSentenceProcessor<Word> thisSentenceProcessor = new WordToSentenceProcessor<Word>();
		final List<List<Word>> theseProtoSentences = thisSentenceProcessor.process(theseWords);
		final List<Sentence> theseSentences = new ArrayList<Sentence>();
		for (final List<Word> thisProtoSentence : theseProtoSentences)
		{
			theseSentences.add(new Sentence(thisProtoSentence));
		}
		return theseSentences;
	}

	// P A R S E

	/**
	 * Parse sentence
	 * 
	 * @param thisSentence
	 *            sentence to parse
	 * @return parsed tree
	 */
	public Tree parse(final Sentence thisSentence)
	{
		try
		{
			final Tree thisTree = this.theParser.parseTree(thisSentence);
			if (thisTree != null)
			{
				return thisTree;
			}
		}
		catch (final Throwable t)
		{
			System.err.println("While parsing sentence: " + t.toString());
		}
		return null;
	}

	/**
	 * Parse sentences
	 * 
	 * @param theseSentences
	 *            sentences to parse
	 * @return list of parse trees
	 */
	public List<Tree> parse(final List<Sentence> theseSentences)
	{
		final List<Tree> theseTrees = new ArrayList<Tree>();
		for (final Sentence thisSentence : theseSentences)
		{
			final Tree thisTree = parse(thisSentence);
			theseTrees.add(thisTree);
		}
		return theseTrees;
	}

	/**
	 * Parse document
	 * 
	 * @param thisDocumentPath
	 *            document path
	 * @return list of parse trees
	 */
	public List<Tree> parseDocument(final String thisDocumentPath)
	{
		final List<Sentence> theseSentences = Parser.getSentences(thisDocumentPath);
		return parse(theseSentences);
	}

	// H E L P E R

	/**
	 * Whether this parser is valid
	 * 
	 * @return true if this parser is valid
	 */
	public boolean isValid()
	{
		return this.theParser != null;
	}

	/**
	 * Get morphology base
	 * 
	 * @param thisWord
	 *            word
	 * @return morphology base
	 */
	static public String morphology(final Word thisWord)
	{
		try
		{
			final Morphology thisMorphology = new Morphology();
			final Word thisBase = thisMorphology.stem(thisWord);
			return thisBase.word();
		}
		catch (final Throwable e)
		{
			return null;
		}
	}

	/**
	 * Get morphology base
	 * 
	 * @param thisString
	 *            string
	 * @return morphology base
	 */
	static public String morphology(final String thisString)
	{
		final Word thisWord = new Word(thisString);
		return Parser.morphology(thisWord);
	}

	/**
	 * Get options
	 * 
	 * @return options as string
	 */
	public String getOptions()
	{
		final Options theseOptions = this.theParser.getOp();
		final StringWriter sw = new StringWriter();
		theseOptions.writeData(new PrintWriter(sw));
		return sw.toString();
	}

	// M A I N

	/**
	 * Parse argument string
	 * 
	 * @param theseArgs
	 *            arg1 string to parse
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	static public void main(final String[] theseArgs) throws ParseException, InterruptedException, IOException
	{
		final ObjectInputStream thisInputStream = IOUtils.readStreamFromString("grammar/englishPCFG.ser.gz");
		final Parser thisParser = new Parser(thisInputStream);
		for (final String thisArg : theseArgs)
		{
			final List<Tree> theseTrees = thisParser.parse(thisArg);
			for (final Tree thisTree : theseTrees)
			{
				System.out.println(thisTree);
			}
		}
	}
}