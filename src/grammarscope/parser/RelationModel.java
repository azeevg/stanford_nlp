package grammarscope.parser;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import edu.stanford.nlp.trees.tregex.TregexPattern;

/**
 * Relation model
 * 
 * @author Bernard Bou
 */
public class RelationModel
{
	/**
	 * Root relation
	 */
	public MutableGrammaticalRelation theRoot;

	/**
	 * Relation array
	 */
	public List<MutableGrammaticalRelation> theRelations;

	/**
	 * Constructor
	 * 
	 * @param thisRoot
	 *            root relation
	 * @param theseRelations
	 *            array list of relations
	 */
	public RelationModel(final MutableGrammaticalRelation thisRoot, final List<MutableGrammaticalRelation> theseRelations)
	{
		this.theRoot = thisRoot;
		this.theRelations = theseRelations;
	}

	/**
	 * Make gov/dep model
	 * 
	 * @return minimal model
	 */
	static public RelationModel makeGovDepModel()
	{
		final MutableGrammaticalRelation thisGovernor = new MutableGrammaticalRelation("GOVERNOR", "gov", "governor", null, null, new String[] {}, "The \"governor\" grammatical relation, which is the inverse of \"dependent\"", new String[][] { {
				"the red car", "gov(red, car)" } });

		final MutableGrammaticalRelation thisDependent = new MutableGrammaticalRelation("DEPENDENT", "dep", "dependent", null, null, new String[] {}, "The \"dependent\" grammatical relation, which is the inverse of \"governor\"", new String[][] { {
				"the red car", "dep(car, red)" } });
		final List<MutableGrammaticalRelation> theseRelations = new ArrayList<MutableGrammaticalRelation>();
		theseRelations.add(thisGovernor);
		theseRelations.add(thisDependent);
		return new RelationModel(thisDependent, theseRelations);
	}

	/**
	 * Constructor
	 * 
	 * @param thisRoot
	 *            root relation
	 * @param theseRelations
	 *            array list of relations
	 */
	public RelationModel(final MutableGrammaticalRelation thisRoot, final MutableGrammaticalRelation[] theseRelations)
	{
		this(thisRoot, new ArrayList<MutableGrammaticalRelation>(Arrays.asList(theseRelations)));
	}

	// M A P S / V E C T O R S
	/**
	 * Get relation from id
	 * 
	 * @param thisId
	 *            id
	 * @return relation
	 */
	public MutableGrammaticalRelation fromId(final String thisId)
	{
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
			if (thisRelation.getId().equals(thisId))
				return thisRelation;
		return null;
	}

	/**
	 * Get vector of ids
	 * 
	 * @return vector of ids
	 */
	public Vector<String> toIds()
	{
		final Vector<String> thisList = new Vector<String>();
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			thisList.add(thisRelation.getId());
		}
		return thisList;
	}

	/**
	 * Make id-relation map
	 * 
	 * @return id-relation map
	 */
	public Map<String, MutableGrammaticalRelation> makeIdToRelationMap()
	{
		final Map<String, MutableGrammaticalRelation> thisMap = new HashMap<String, MutableGrammaticalRelation>();
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			final String thisKey = thisRelation.getId();
			if (thisMap.containsKey(thisKey))
			{
				System.out.println("duplicate key:" + thisKey);
			}
			if (thisMap.containsValue(thisRelation))
			{
				System.out.println("duplicate value:" + thisRelation);
			}
			thisMap.put(thisKey, thisRelation);
		}
		return thisMap;
	}

	/**
	 * Make short name-relation map
	 * 
	 * @return short name-relation map
	 */
	public Map<String, MutableGrammaticalRelation> makeShortNameToRelationMap()
	{
		final Map<String, MutableGrammaticalRelation> thisMap = new HashMap<String, MutableGrammaticalRelation>();
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			final String thisKey = thisRelation.getShortName();
			if (thisMap.containsKey(thisKey))
			{
				System.out.println("duplicate key:" + thisKey);
			}
			if (thisMap.containsValue(thisRelation))
			{
				System.out.println("duplicate value:" + thisRelation);
			}
			thisMap.put(thisKey, thisRelation);
		}
		return thisMap;
	}

	/**
	 * Make name-relation map
	 * 
	 * @return name-relation map
	 */
	public Map<String, MutableGrammaticalRelation> makeNameToRelationMap()
	{
		final Map<String, MutableGrammaticalRelation> thisMap = new HashMap<String, MutableGrammaticalRelation>();
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			final String thisKey = thisRelation.getName();
			if (thisMap.containsKey(thisKey))
			{
				System.out.println("duplicate key:" + thisKey);
			}
			if (thisMap.containsValue(thisRelation))
			{
				System.out.println("duplicate value:" + thisRelation);
			}
			thisMap.put(thisKey, thisRelation);
		}
		return thisMap;
	}

	// A D D / R E M O V E

	/**
	 * Add relation to model
	 * 
	 * @param thisRelation
	 *            relation to add
	 */
	synchronized public void add(final MutableGrammaticalRelation thisRelation)
	{
		if (thisRelation != null)
		{
			// array
			this.theRelations.add(thisRelation);

			// hierarchy (may be added on construction)
			final MutableGrammaticalRelation thisParent = thisRelation.getParent();
			if (thisParent != null && thisParent.getChildren().indexOf(thisRelation) == -1)
			{
				thisParent.addChild(thisRelation);
			}

			// hierarchy uplink (on construction)
			// do nothing
		}
	}

	/**
	 * Remove relation from model
	 * 
	 * @param thisRelation
	 *            relation to remove
	 */
	synchronized public boolean remove(final MutableGrammaticalRelation thisRelation)
	{
		// remove only childless relation
		if (thisRelation != null && thisRelation.getChildren().size() == 0)
		{
			// array
			this.theRelations.remove(thisRelation);

			// hierarchy downlink
			final MutableGrammaticalRelation thisParent = thisRelation.getParent();
			if (thisParent != null)
			{
				thisParent.removeChild(thisRelation);
			}

			// hierarchy uplink
			// do nothing
			return true;
		}
		return false;
	}

	/**
	 * Move relation in model
	 * 
	 * @param thisRelation
	 *            relation to move
	 * @param thisParentRelation
	 *            parent relation to move relation to
	 * @return true if successful
	 */
	synchronized public boolean move(final MutableGrammaticalRelation thisRelation, final MutableGrammaticalRelation thisParentRelation)
	{
		// do not have a child as a parent
		if (thisRelation != null && !thisRelation.equals(thisParentRelation) && !thisRelation.isAncestor(thisParentRelation))
		{
			// array
			this.theRelations.remove(thisRelation);

			// hierarchy downlink
			final MutableGrammaticalRelation thisParent = thisRelation.getParent();
			if (thisParent != null)
			{
				thisParent.removeChild(thisRelation);
			}

			// hierarchy uplink
			// do nothing

			// add
			thisRelation.setParent(thisParentRelation);
			add(thisRelation);
			return true;
		}
		return false;
	}

	// S T R I N G

	/**
	 * Model to XML string
	 * 
	 * @return string
	 */
	public String toXmlString()
	{
		final StringBuffer thisBuffer = new StringBuffer();
		thisBuffer.append("<model>\n");
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			thisBuffer.append("<relation>");
			thisBuffer.append("<name>" + thisRelation.getName() + "</name>");
			thisBuffer.append("<shortname>" + thisRelation.getShortName() + "</shortname>");
			thisBuffer.append("<longname>" + thisRelation.getLongName() + "</longname>");
			thisBuffer.append("<parent>" + thisRelation.getTreeName(false, false) + "</parent>");

			final Pattern thisSourcePattern = thisRelation.getSourcePattern();
			if (thisSourcePattern != null)
			{
				thisBuffer.append("<sourcepattern>" + thisRelation.getSourcePattern().toString() + "</sourcepattern>");
			}
			for (final TregexPattern thisPattern : thisRelation.getTargetPatterns())
			{
				thisBuffer.append("<targetpattern>");
				// thisBuffer.append("<![CDATA[");
				final String thisString = MutableGrammaticalRelation.patternToString(thisPattern);
				thisBuffer.append(thisString);
				// thisBuffer.append("]]>");
				thisBuffer.append("</targetpattern>");
			}

			thisBuffer.append("<description>" + thisRelation.getDescription() + "</description>");
			final String[][] theseCodedExamples = thisRelation.getCodedExamples();
			for (final String[] theseCodedExample : theseCodedExamples)
			{
				final String thisExample = theseCodedExample[0];
				final String thisCoding = theseCodedExample[1];
				thisBuffer.append("<example>");
				thisBuffer.append(thisExample);
				thisBuffer.append("→");
				thisBuffer.append(thisCoding);
				thisBuffer.append("</example>");
			}

			thisBuffer.append("</relation>\n");
		}
		thisBuffer.append("</model>\n");
		return thisBuffer.toString();
	}

	/**
	 * Model to simple string
	 * 
	 * @return string
	 */
	public String toSimpleString()
	{
		final StringBuffer thisBuffer = new StringBuffer();
		thisBuffer.append("<model>\n");
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			thisBuffer.append(thisRelation.getName());
			thisBuffer.append(" < ");
			thisBuffer.append(thisRelation.getTreeName(false, false));
			thisBuffer.append("\n");
		}
		thisBuffer.append("</model>\n");
		return thisBuffer.toString();
	}

	/**
	 * Model to tree string
	 * 
	 * @return string
	 */
	public String toTreeString()
	{
		final StringBuffer theBuffer = new StringBuffer();
		this.theRoot.walkTree(new RelationProcessor()
		{
			public void process(final MutableGrammaticalRelation thisRelation)
			{
				theBuffer.append(thisRelation.getShortName());
				theBuffer.append(" [");
				theBuffer.append(thisRelation.getTreeName(false, false));
				theBuffer.append("]\n");
			}
		});
		return theBuffer.toString();
	}

	// E X P O R T

	/**
	 * Export model to writer
	 * 
	 * @param thisWriter
	 *            writer
	 */
	public void export(final PrintWriter thisWriter)
	{
		thisWriter.print(toXmlString());
		thisWriter.close();
	}

	/**
	 * Export model examples to writer
	 * 
	 * @param thisWriter
	 *            writer
	 */
	public void exportExamples(final PrintWriter thisWriter)
	{
		final StringBuffer thisBuffer = new StringBuffer();
		for (final MutableGrammaticalRelation thisRelation : this.theRelations)
		{
			final String[][] theseCodedExamples = thisRelation.getCodedExamples();
			for (final String[] theseCodedExample : theseCodedExamples)
			{
				final String thisText = theseCodedExample[0];
				thisBuffer.append(thisText);
				if (!thisText.endsWith(".") && !thisText.endsWith("?"))
				{
					thisBuffer.append(".");
				}
				thisBuffer.append("\n");
			}
		}
		thisWriter.print(thisBuffer.toString());
		thisWriter.close();
	}

	/**
	 * Export model to files
	 * 
	 * @param theseArgs
	 * @throws FileNotFoundException
	 */
	public static void main(final String[] theseArgs) throws FileNotFoundException
	{
		final RelationModel thisModel = DefaultMutableGrammaticalRelations.makeDefaultModel();

		thisModel.export(new PrintWriter(System.out));

		final String thisRelationsFile = "relations-dump.txt";
		final PrintWriter thisRelationsWriter = new PrintWriter(new FileOutputStream(thisRelationsFile, false));
		thisModel.export(thisRelationsWriter);
		System.err.println("OUTPUT:" + thisRelationsFile);

		final String thisExamplesFile = "relations-examples.txt";
		final PrintWriter thisExamplesWriter = new PrintWriter(new FileOutputStream(thisExamplesFile, false));
		thisModel.exportExamples(thisExamplesWriter);
		System.err.println("OUTPUT:" + thisExamplesFile);
	}
}
