package sesa.common.model.processing.sentence;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents a triplets for the sentence .
 * Triplets is a triple: subject, predicate and object
 */
public class SentenceTriplet {
    private String subject;
    private String predicate;
    private String object;
    private String sentence;

    public SentenceTriplet(String subject, String predicate, String object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public void setSentence(String sentencePart) {
        this.sentence = sentencePart;
    }

    public String getSentence() {
        return sentence;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }


    @Override
    public String toString() {
        return subject + ";" + predicate + ";" + object;
    }

    /**
     * ������� ���������� ������ ������� ������� �����������
     *
     * @return ������ ������ (token)
     */
    public List<String> getTokens() {
        List<String> list = new LinkedList<String>();
        if (object != null)
            list.add(object);
        if (predicate != null)
            list.add(predicate);
        if (subject != null)
            list.add(subject);
        return list;
    }

    public void printTriplet() {
        System.out.println("Triplet subj - pred - obj : " + subject + " - " + predicate + " - " + object);

    }

}
