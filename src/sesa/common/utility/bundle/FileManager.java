package sesa.common.utility.bundle;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by Ira on 24.02.2016.
 */
public class FileManager {

    private ResourceBundle bundle;
    public FileManager(String bundleName) {
        init(bundleName);
    }
    public boolean init(String bundleName){
        try{
            bundle = ResourceBundle.getBundle(bundleName);
            return true;
        }catch(MissingResourceException e){
            System.out.println(e.getMessage());

        }catch(NullPointerException e){
            System.out.println(e.getMessage());
        }

        return false;
    }

    public String getValue(String key) {
        try{
            String strKey = bundle.getString(key);
            return strKey;
        }catch (MissingResourceException e){
            System.out.println("Key does not found");
        }catch(NullPointerException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
