package sesa.common.semantics;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sesa.common.model.processing.sentence.SentenceTriplet;
import sesa.common.utility.logging.LoggingUtility;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.*;


//TODO: It's not a good algorithm to find triplet

/**
 * The class contains methods for finding the triplets using the algorithm Delia Rusu
 */
public class TripletFinder {
    private String[] NOUN_SUB_TREES = new String[]{"NN", "NNP", "NNPS", "NNS","COLLOCATION"};
    private String[] VERB_SUB_TREES = new String[]{"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};
    private String[] ADJECTIVE_SUB_TREES = new String[]{"JJ", "JJR", "JJS" , "COLLOCATION"};

    private String[] NOUN_PREPOSITION_PHRASES = new String[]{"NP", "PP"};
    private String[] VERB_PHRASES = new String[]{"VP"};
    private String[] NOUN_PHRASES = new String[]{"NP"};
    private String[] ADJECTIVE_PHRASES = new String[]{"ADJP"};
    private Document document = null;

    /**
     * Constructor
     * @param xmlTree parse tree sentence in xml format
     */
    public TripletFinder(String xmlTree) {
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        f.setValidating(false);
        DocumentBuilder builder = null;
        try {
            builder = f.newDocumentBuilder();
            document = builder.parse(new ByteArrayInputStream(xmlTree.getBytes()));
        } catch (Exception e) {
            LoggingUtility.error(e.getMessage());
        }
    }

    /**
     * Method finds the triple (subject, predicate , object) in parse tree sentence
     * Using the algorithm Delia Rusu
     * @param xmlTree parse tree sentence in xml format
     * @return triplet
     */
    public static SentenceTriplet findSentenceTriplet(String xmlTree) {
        TripletFinder f = new TripletFinder(xmlTree);
        String subject = f.getNodeValue(f.findSubjectNode());
        Node predicateNode = f.findPredicateNode();
        String predicate = f.getNodeValue(predicateNode);
        String object = f.getNodeValue(f.findObjectNode(predicateNode));
        return new SentenceTriplet(subject, predicate, object);
    }

    /**
     * Finding a node in the tree using the width search
     */
    public Node findSubjectNode() {
        Map<String[], String[]> subjectTreeMap = new HashMap<String[], String[]>();
        subjectTreeMap.put(NOUN_SUB_TREES, NOUN_PHRASES);
        return widthSearch(document.getDocumentElement(), subjectTreeMap);
    }

    /**
     * ����� ����������
     * @return
     */
    private Node findPredicateNode() {
        return recurrentDepthSearch(document.getDocumentElement(), VERB_SUB_TREES, VERB_PHRASES);
    }

    /**
     * ����� ����������
     * @param predicateNode
     * @return
     */
    private Node findObjectNode(Node predicateNode) {

        if (predicateNode == null)
            return null;

        Map<String[], String[]> predicateTreeMap = new HashMap<String[], String[]>();
        predicateTreeMap.put(NOUN_SUB_TREES, NOUN_PREPOSITION_PHRASES);
        predicateTreeMap.put(ADJECTIVE_SUB_TREES, ADJECTIVE_PHRASES);
        Node predicateParentNode = predicateNode.getParentNode();
        return widthSearch(predicateParentNode, predicateTreeMap);
    }

    /**
     * ����� � ������
     * @param rootNode
     * @param treeMap
     * @return
     */
    private Node widthSearch(Node rootNode, Map<String[], String[]> treeMap) {
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(rootNode);

        while (queue.size() > 0) {
            Node node = queue.poll();
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Node childNode = children.item(i);
                if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals("n")) {
                    String nodeName = findFirstNodeByName(childNode, "v").getTextContent();
                    String parentNodeName = findFirstNodeByName(node, "v").getTextContent();
                    for (String[] key : treeMap.keySet()) {
                        if (isSubTree(nodeName, key) && isPhrase(parentNodeName, treeMap.get(key))) {
                            return childNode;
                        }
                    }
                    queue.add(childNode);
                }
            }
        }
        return null;
    }

    private Node recurrentDepthSearch(Node rootNode, String[] nodeNames, String[] phraseNames) {
        Integer maxDepthIndex = 0, depthIndex = 0;
        Node deepestNode = null;
        Map<Node, Integer> nodeCandidates = new HashMap<Node, Integer>();
        findCandidates(rootNode, depthIndex, nodeCandidates, nodeNames, phraseNames);
        for (Node key : nodeCandidates.keySet()) {
            if (nodeCandidates.get(key) > maxDepthIndex) {
                maxDepthIndex = nodeCandidates.get(key);
                deepestNode = key;
            }
        }
        return deepestNode;
    }

    private String findCandidates(Node node, Integer depthIndex, Map<Node, Integer> nodeCandidates, String[] nodeNames, String[] parentNames) {
        depthIndex++;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node childNode = children.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals("n")) {
                String nodeName = findFirstNodeByName(childNode, "v").getTextContent();
                String parentNodeName = findFirstNodeByName(node, "v").getTextContent();
                if (isSubTree(nodeName, nodeNames) && isPhrase(parentNodeName, parentNames)) {
                    nodeCandidates.put(childNode, depthIndex);
                }
                findCandidates(childNode, depthIndex, nodeCandidates, nodeNames, parentNames);
            }
        }
        return null;
    }


    private boolean isSubTree(String name, String[] nodeNames) {
        for (String treeName : nodeNames)
            if (name.equals(treeName))
                return true;
        return false;
    }

    private boolean isPhrase(String name, String[] parentNames) {
        for (String parentTree : parentNames)
            if (name.equals(parentTree))
                return true;
        return false;
    }


    public Node findFirstNodeByName(Node node, String nodeName) {
        if (node == null)
            return null;

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node childNode = children.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals(nodeName)) {
                return childNode;
            }
        }
        return null;
    }

    public String getNodeValue(Node node) {
        if (node == null)
            return null;
        return findFirstNodeByName(findFirstNodeByName(node, "n"), "v").getTextContent();
    }
}
