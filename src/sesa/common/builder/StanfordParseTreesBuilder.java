package sesa.common.builder;

import edu.spbstu.appmath.collocmatch.Collocation;
import edu.spbstu.appmath.collocmatch.CollocationDictionary;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.trees.Tree;
import grammarscope.parser.Document;
import grammarscope.parser.Parser;
import grammarscope.parser.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import sesa.common.utility.FileReaderWriter;
import sesa.common.utility.bundle.FileManager;
import sesa.common.utility.collocation.CollocationUtility;
import sesa.common.utility.semantic.SentenceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Ira on 09.05.2016.
 */
public class StanfordParseTreesBuilder {
    public final Logger log = LogManager.getLogger(StanfordParseTreesBuilder.class);
    private boolean isModCKY = false;
    private Parser parser;
    private String fileName = "./data/wikiParseTree.txt";
    private String fileParseTree = "./data/parseTreePenn.txt";
    private String collocationsFileName = "./data/wikiCollocations.txt";
    private String fileCollocationsInTree = "./data/collocationsInTree.txt";
    private String fileCollocationsInText = "./data/collocationsInText.txt";
    CollocationDictionary dictionary;
    Set<String> collocationForParseTree;
    public int numCollocationsInText = 0;
    private String languageModel = "./data/grammar/englishFactored.ser.gz";
    private String smallLanguageModel = "./data/grammar/englishPCFG.ser.gz";
    private FileManager file;

    public StanfordParseTreesBuilder(){
        this.file = new FileManager("path");
        try {
            ObjectInputStream inputStream = IOUtils.readStreamFromString(smallLanguageModel);
            parser = new Parser(inputStream);
            dictionary = new CollocationDictionary(new String[]{"./data/dictionary/collocations/subsetCollocations.txt"});
            getCollocationForParseTree();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseHtmlDocument(String text) throws BuilderException, SQLException, ClassNotFoundException {
        numCollocationsInText = 0;
        org.jsoup.nodes.Document parsed = parseDocument(text);
        buildDocumentTree(parsed);
        System.out.println("---:" + numCollocationsInText + "\n-------------\n");
    }

    protected org.jsoup.nodes.Document parseDocument(String text) {
        return Jsoup.parse(text);
    }

    protected void buildDocumentTree(org.jsoup.nodes.Document document) throws BuilderException, SQLException, ClassNotFoundException {
        int collocationsNumber = 0;
        String title = document.title(); // page header
        collocationsNumber += buildParseTrees(title);
        for (Node child : document.body().childNodes()) {
            if (child instanceof TextNode) {
                String paragraph = ((TextNode) child).text();
                collocationsNumber += buildParseTrees(paragraph);

            } else if (child instanceof Element) {
                Element header = (Element) child;
                String tagName = header.tagName();
                if (isHeaderElement(tagName)) {
                    collocationsNumber += buildParseTrees(header.text());
                }
            }
        }
        log.info("collocationsNumber:" + collocationsNumber);
        log.info("collocationsInTextNumber:" + numCollocationsInText);
        FileReaderWriter.write(collocationsFileName, "---:" + collocationsNumber + "\n-------------\n");
        log.info("difference:" + (numCollocationsInText - collocationsNumber));
        log.info("end of buildDocumentTree");

    }

    public void parseText(String text) throws SQLException, ClassNotFoundException {
        int allFoundCollocationsInTree = buildParseTrees(text);
        log.info("collocationsNumber:" + allFoundCollocationsInTree);
        log.info("collocationsInTextNumber:" + numCollocationsInText);
        log.info("Number of collocations were not found:" + (numCollocationsInText - allFoundCollocationsInTree));
        log.info("end of parse text");

    }

    public void parseListString(List<String> text) throws SQLException, ClassNotFoundException {
        int allFoundCollocationsInTree = 0;
        for (String sentence : text) {
            allFoundCollocationsInTree += buildParseSentence(sentence);
        }

        log.info("collocationsNumber:" + allFoundCollocationsInTree);
        log.info("collocationsInTextNumber:" + numCollocationsInText);
        log.info("Number of collocations were not found:" + (numCollocationsInText - allFoundCollocationsInTree));
        log.info("end of parse text");

    }


    public int buildParseSentence(String text) throws SQLException, ClassNotFoundException {

        Document document = new Document();
        document.create(text);

        Sentence sentence = SentenceUtils.convertStringToSentence(text);

        Tree tree = parser.parse(sentence, true);

        List<String> collocationInTree = CollocationUtility.getCollocationInTree(collocationForParseTree, tree);
        int numCollocationInTree = collocationInTree.size();

       // FileReaderWriter.write(fileCollocationsInText, collocationInSentence.toString());
        FileReaderWriter.write(fileCollocationsInTree, collocationInTree.toString());
        FileReaderWriter.write(fileParseTree, tree.pennString());
        FileReaderWriter.write(fileName, tree.toString());

        log.info("In sentence:" + sentence.toString() + "\nCollocations in parse tree:" + numCollocationInTree);
        return numCollocationInTree;

    }

    public int buildParseTrees(String text) throws SQLException, ClassNotFoundException {
        Map<Sentence, Tree> parsedTrees = new HashMap<Sentence, Tree>();
        Document document = new Document();
        document.create(text);
        int allFoundCollocationsInTree = 0;
        int numSentence = 0;
        List<String> list = new ArrayList<String>();
        for (Sentence sentence : document.theSentences) {
            if (sentence.toString().length() < 3)
                continue;

            list.add(sentence.toString());
            log.info("Parse sentence number: " + numSentence);
            Tree tree = parser.parse(sentence, true);
            parsedTrees.put(sentence, tree);

            List<String> collocationInSentence = CollocationUtility.getCollocationInSentence(dictionary, sentence.toString());
            List<String> collocationInTree = CollocationUtility.getCollocationInTree(collocationForParseTree, tree);

            int numCollocationInText = collocationInSentence.size();
            int numCollocationInTree = collocationInTree.size();

            FileReaderWriter.write(fileCollocationsInText, collocationInSentence.toString());
            FileReaderWriter.write(fileCollocationsInTree, collocationInTree.toString());

            allFoundCollocationsInTree += numCollocationInTree;
            log.info("In sentence:" + sentence.toString() + "\nCollocations in sentence:" + numCollocationInText + "\nCollocations in parse tree:" + numCollocationInTree);

            FileReaderWriter.write(fileParseTree, tree.pennString());
            FileReaderWriter.write(fileName, tree.toString());

            numSentence++;
            this.numCollocationsInText += numCollocationInText;
        }
        // System.out.println(calcNumCollocationInText(list));
        System.out.println(numSentence);
        return allFoundCollocationsInTree;
    }


    private void getCollocationForParseTree() {
        collocationForParseTree = new HashSet<String>();
        for (Collocation collocation : dictionary) {
            StringBuilder sb = new StringBuilder();
            for (String w : collocation.words()) {
                sb.append(w);
            }
            collocationForParseTree.add(sb.toString());
        }
    }


    public boolean isModCKY() {
        return isModCKY;
    }

    /**
     * Get header level
     *
     * @param headerTag header string
     * @return level
     */
    protected int getHeaderLevel(String headerTag) {
        return new Integer(headerTag.replaceAll("[^\\d]", ""));
    }

    /**
     * Is the title bar? * Does the header Tag string regular expression h [1-9] +
     *
     * @param headerTag
     * @return true if it's header element
     */
    protected boolean isHeaderElement(String headerTag) {
        return headerTag.matches("h[1-9]+"); // check regular
    }

    /**
     * Building of the parse trees, semantic groups for suggestions in one paragraph
     * @param text input text
     * @param isTitle flag
     * @param level paragraph level
     * @return list of sentence forest
     * @throws BuilderException
     */
}
