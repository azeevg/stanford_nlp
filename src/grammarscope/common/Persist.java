package grammarscope.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Properties;

/**
 * Persist
 * 
 * @author Bernard Bou
 */
public class Persist
{
	/**
	 * Obtain settings (handles initial state)
	 * 
	 * @param thisPersistFile
	 *            persist file
	 * @return properties
	 */
/*	static public Properties makeSettings(final String thisPersistFile)
	{
		final Properties theseSettings = Persist.loadSettings(thisPersistFile);
		if (theseSettings.size() == 0)
		{
			// no grammar has ever been referenced
			final URI thisUri = FileUtils.findArchive("englishPCFG.ser.gz");
			if (thisUri != null)
			{
				try
				{
					final String thisPath = thisUri.toURL().toString();
					theseSettings.setProperty("grammar", thisPath);
				}
				catch (final MalformedURLException e)
				{
					System.err.println(e.toString());
				}
			}
		}
		return theseSettings;
	} */

	/**
	 * Load properties from file
	 * 
	 * @param thisFilePath
	 *            persist file
	 * @return properties
	 */
	static public Properties loadSettings(final String thisFilePath)
	{
		final Properties theseSettings = new Properties();

		try
		{
			final InputStream thisPropStream = new FileInputStream(thisFilePath);
			theseSettings.load(thisPropStream);
			return theseSettings;
		}
		catch (final Exception e)
		{
			// do nothing
		}
		return theseSettings;
	}

	/**
	 * Save persist data
	 * 
	 * @param thisFilePath
	 *            persist file
	 * @param theseSettings
	 *            settings to persist
	 * @return true if successful
	 */
	static public boolean saveSettings(final String thisFilePath, final Properties theseSettings)
	{
		try
		{
			final OutputStream thisPropStream = new FileOutputStream(thisFilePath);
			theseSettings.store(thisPropStream, "grammarscope");
			return true;
		}
		catch (final Exception e)
		{
			System.out.println("Cannot save persist file :" + e);
		}
		return false;
	}

	/**
	 * Get default persist file
	 * 
	 * @param thisPersistFile
	 *            persist file name
	 * @return path to persist file
	 */
	static public String getDefault(final String thisPersistFile)
	{
		return System.getProperty("user.home") + File.separator + "." + thisPersistFile;
	}
}
